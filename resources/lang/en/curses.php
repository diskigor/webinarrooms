<?php
    return [
        'curses_title'=>'Curses SDO',
        'add_curses_button'=>'Add course',
        'title_from_created_curses'=>'The form for adding an SDO course',
        'curse_image'=>'Course images',
        'name_curse'=>'Name course',
        'placeholder_name_course'=>'Enter a name for the course',
        'desc_course'=>'Description of the SDO course',
        'desc_course_placeholder'=>'Enter a description of the SDO course',
        'button_save_course'=>'Save course',
        'short_description'=>'Short Course Description',
        'goto_course'   => 'Go to in course',
    ];