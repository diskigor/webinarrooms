<?php
return [
    'news'=>'News SDO',
    'add_news'=>'Add News',
    'view'=>'View',
    'created_news'=>'Published',
    'form_created_news'=>'The form of adding News SDO',
    'news_image'=>'News image',
    'title_news'=>'Title Article',
    'placeholder_title'=>'Enter title article',
    'enter_article'=>'Enter you article',
    'save_button'=>'Save article',

];