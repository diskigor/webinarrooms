<?php
/**
 * author Michael Terpelyvets
 */

return [
    'title_language'=>'List of languages ​​on SDO',
    'title_add_language'=>'Form of adding a language',
    'title_edit_language'=>'Language edit form',
    'code'=>'Code',
    'name'=>'Name',
    'country'=>'Country',
    'logo'=>'Logo',
    'status'=>'Status',
    'position'=>'Position',
    'action'=>'Actions',
    'select_position'=>'Select a position',
    'change_logo_language'=>'Change logo',
    'message_add_language_logo'=>'The language logo was successfully added.',
    'message_edit_language_logo'=>'The logo of the language has been successfully changed.',
];