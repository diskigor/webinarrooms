<?php
return [
    'delete_user'=>'Profile successfully deleted!',
    'update_user'=>'User data changed successfully!',
    'hack_message'=>'You do not have the right to perform these actions! We informed the Site Administrator.',
];