<?php
return [
    'headertitle'=>'Блог',
    'alllikes'=>'Понравилось',
    'allcomments' => 'Комментариев',
    'author'=>'Автор',
    'create'=>'Создана',
    'backtolist'=>'Все статьи',
    'readmore'=>'Читать далее...',
    'like'=>'Понравилось',
    'dislike'=>'Не понравилось',
    'reply'=>'Ответить',
    'lasttimecomment'=>'только что добавлен',
    'addfirstcomment'=>'',
    'addcomment'=>'Написать комментарий',
    'cancelcomment'=>'Отменить ответ',
    'yourcomment'=>'Ваш комментарий',
    'sendcomment'=>'Отправить',
];