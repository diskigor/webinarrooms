<?php
return [
    'news'=>'Новости SDO',
    'add_news'=>'Добавить новость',
    'view'=>'Просмотреть',
    'created_news'=>'Опубликована',
    'form_created_news'=>'Форма добавления новости SDO',
    'news_image'=>'Изображение новости',
    'title_news'=>'Название новости',
    'placeholder_title'=>'Введите название новости',
    'enter_article'=>'Введите статью',
    'save_button'=>'Сохранить новость',

];