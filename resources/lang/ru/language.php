<?php
return [
    'title_language'=> 'Список языков на SDO',
    'title_add_language'=>'Форма добавления языка',
    'title_edit_language'=>'Форма редактирования языка',
    'code'=>'Код',
    'name'=>'Название',
    'country'=>'Страна',
    'logo'=>'Логотип',
    'status'=>'Статус',
    'position'=>'Позиция',
    'action'=>'Действия',
    'select_position'=>'Выберите позицию',
    'button_add_language'=>'Добавить язык',
    'change_logo_language'=>'Изменить логотип',
    'message_add_language_logo'=>'Логотип языка успешно добавлен',
    'message_edit_language_logo'=>'Логотип языка успешно изменен',
];