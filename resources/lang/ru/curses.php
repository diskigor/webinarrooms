<?php
    return [
        'curses_title'=>'Курсы SDO',
        'add_curses_button'=>'Добавить курс',
        'title_from_created_curses'=>'Форма добавления курса SDO',
        'curse_image'=>'Изображения курса',
        'name_curse'=>'Название курса',
        'placeholder_name_course'=>'Введите название курса',
        'desc_course'=>'Описание курса SDO',
        'desc_course_placeholder'=>'Введите описание курса SDO',
        'button_save_course'=>'Сохранить курс',
        'short_description'=>'Краткое описание курса',
        'goto_course'   => 'Перейти в курс',
    ];