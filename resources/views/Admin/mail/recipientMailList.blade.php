@extends('layouts.admin')
@section('content')
    <div class="col-md-12">
        <h3 class="text-center">Входящие сообщения</h3>
        <div class="row">
            <div class="panel">
                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <th>Отправитель</th>
                        <th>Сообщения</th>
                        <th>Статус</th>
                        <th>Действие</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($messages->count() === 0)
                        <tr>
                            <td>У вас нет входящих сообщения.</td>
                        </tr>
                    @else
                        @foreach($messages as  $message)
                            <tr>
                                <td>{{$message->sender->name}}</td>
                                <td>{{mb_strimwidth(strip_tags($message->message),0,100,'...')}}</td>
                                <td>@if($message->status === 0)
                                            Новое
                                        @else
                                            Прочитано
                                    @endif
                                </td>
                                <td>
                                    <a href="{{route('more_mail',$message->id)}}" class="btn btn-success">Подробнее</a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection