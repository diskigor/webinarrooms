@extends('layouts.admin')
@section('content')
    <div class="col-md-12 mail-support">
        <?php $title_page='Обращения в службу поддержки'; ?>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Тема</th>
                        <th>Статус</th>
                        <th>Создана</th>
                        <th>Изменена</th>
                        <th>Действие</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!$callbacks)
                        <tr>
                            <td>Вы ещё не создавали тем для службы поддержки.</td>
                        </tr>
                    @else
                        @foreach($callbacks as  $message)
                        <tr class="{{ $message->isAdmin?'text-danger':'' }}">
                                <td>{{ $loop->iteration }}</td>
                                <td>{{mb_strimwidth(strip_tags($message->subject),0,120,'...')}}</td>
                                <td>
                                    @if($message->UserStatusMessage == 'send')
                                        <i class="fa fa-paper-plane fa-2x" aria-hidden="true" title="Отправлено"></i>
                                    @elseif($message->UserStatusMessage == 'read')
                                        <i class="far fa-envelope-open fa-2x" aria-hidden="true" title="Прочитано"></i>
                                    @elseif($message->UserStatusMessage == 'new')
                                        <i class="far fa-envelope fa-2x text-success" aria-hidden="true" title="Новое сообщение"></i>
                                    @endif
                                </td>
                                <td>
                                    {{ $message->created_at->format('d.m.Y') }}
                                </td>
                                <td>
                                    {{ $message->updated_at->format('d.m.Y') }}
                                </td>
                                <td>
                                    <a href="{{route('user_callback_chats.show',$message->id)}}" class="btn btn-success">Подробнее</a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="col-md-12 block-admin-mail">
                <a class="btn-admin-mail" href="{{route('callback.create')}}">Написать администратору</a>
            </div>
        </div>
    </div>
@endsection