@extends('layouts.admin')
@section('content')
    <div class="col-md-12">
        <h3 class="text-center">Отправленые сообщения</h3>
        <div class="row">
            <div class="panel">
                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <th>Тема</th>
                        <th>Текст</th>
                        <th>Статус</th>
                        <th>Дата</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($send_messages->count() === 0)
                        <tr>
                            <td>У вас нет отправленых сообщений сообщения.</td>
                        </tr>
                    @else
                        @foreach($send_messages as  $message)
                            <tr>
                                <td>{{$message->subject}}</td>
                                <td>{{$message->message}}</td>
                                <td>@if($message->status === 1)
                                        Просмотренно
                                    @else
                                        Ожидает
                                    @endif
                                </td>
                                <td>{{$message->created_at}}</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection