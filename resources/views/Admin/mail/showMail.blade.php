@extends('layouts.admin')
@section('content')
    <div class="col-md-12">
        <h3 class="text-center">Ответ администратора сайта</h3>
    </div>
    <div class="container">
        <div class="row">
            <div class="panel">
                {!! $message->message !!}
            </div>
            <a href="{{redirect()->back()->getTargetUrl()}}" class="btn btn-blue">Назад</a>
        </div>
    </div>
@endsection