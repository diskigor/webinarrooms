@extends('layouts.admin')
@section('content')
    <?php $title_page='Афиша вебинаров'; ?>
<div class="col-md-12 first-admin-home-block">
        @if(Auth::user()->isAdmin())
            
            <? //Только для админа ?>
            
        @endif  
        @if(Auth::user()->isCadet() OR Auth::user()->isAdmin() OR Auth::user()->isLecturer())
        <div class="row">
            @foreach ($posters as $val)
                <div class="block-one">
                    <a href="/admin/catalog_webinars"><img src="http://<?=$url;?>/{{ $val->image }}" class="img-poster"></a>
                    <div class="block-info">
                        <div class="title-block">{!! $val->description !!}</div>
                        
                        <div class="count-webinars">Количество вебинаров: {{ $webinars }}</div>
                        <a href="/admin/catalog_webinars">Смотреть</a>
                    </div>
                </div>
            @endforeach
        </div>
        @endif
    </div> 
@endsection


