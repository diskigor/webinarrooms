@extends('layouts.admin')
@section('content')
    <?php $title_page='Создание вебинара'; ?>
<div class="col-md-12 first-admin-home-block">
        @if(Auth::user()->isAdmin())
            
            <? //Только для админа ?>
            
        @endif
        @if(Auth::user()->isLecturer() OR Auth::user()->isAdmin())
        <div class="row">
            <!--<div class="content-create-webinar">
                <div class="col-md-3 menu-tab-webinar active" data-tab="1" id="m_tab_1">
                    <span>Основное</span>
                </div>
                <div class="col-md-3 menu-tab-webinar" data-tab="2" id="m_tab_2">
                    <span>Участники</span>
                </div>
                <div class="col-md-3 menu-tab-webinar" data-tab="3" id="m_tab_3">
                    <span>Тесты/Опросы</span>
                </div>
                <div class="col-md-3 menu-tab-webinar" data-tab="4" id="m_tab_4">
                    <span>Материалы</span>
                </div>
            </div>-->

            <div class="block-basik">
                <div class="col-md-12 info-block-webinar actives" id="tab_1">
                    <div class="clock-bulb"><i class="fa fa-lightbulb-o" aria-hidden="true"></i></div><div class="info-bulb-block">Основная информация</div>
                    <form method="POST" action="{{ route('articleAdd') }}">
                        <input type="text" name="name_webinar" placeholder="Название вебинара" class="form-control" style="margin-top: 25px;">
                        <label class="name-lb-vebinar">Описание вебинара</label>
                        <textarea name="description" placeholder="Описание вебинара"></textarea>
                        <div class="row">
                            <div class="col-sm-4">
                                <p class="btst-label-p">Дата</p>
                                <div class="form-group">
                                  <div class="input-group" id="datetimepicker2">
                                    <input type="data" name="date_webinar" class="form-control" />
                                    <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                  </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <p class="btst-label-p">Время начала</p>
                                <div class="form-group">
                                  <div class="input-group glyphicons-clock" id="datetimepicker4">
                                    <input type="time" name="time_start" class="form-control" />
                                    <span class="input-group-addon">
                                      <span class="glyphicons glyphicons-clock"></span>
                                    </span>
                                  </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <p class="btst-label-p">Время окончания</p>
                                <div class="form-group">
                                  <div class="input-group glyphicons-clock" id="datetimepicker4_2">
                                    <input type="time" name="time_stop" class="form-control" />
                                    <span class="input-group-addon">
                                      <span class="glyphicons glyphicons-clock"></span>
                                    </span>
                                  </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <p class="btst-label-p" style="color: #00a9fd;">Часовой пояс</p>
                                <select name="timezone" class="timezone_select">
                                    <option value="Киев">Киев</option>
                                    <option value="Москва">Москва</option>
                                </select>
                            </div>
                        </div>
                        <div class="row cls-margin">
                            <div class="col-sm-6">
                                <div class="full-width">
                                    <input type="checkbox" name="chat"><label>Включить чат</label>
                                </div>
                                <div class="full-width">
                                    <input type="checkbox" name="question"><label>Задать вопрос в чате</label>
                                </div>
                                <div class="full-width">
                                    <input type="checkbox" name="question_sound"><label>Задать вопрос голосом</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="full-width">
                                    <input type="checkbox" name="type_message"><label>Сообщения в чате видны только ведущему</label>
                                </div>
                                <div class="full-width">
                                    <input type="checkbox" name="list_people"><label>Скрыть список участников</label>
                                </div>
                                <div class="full-width">
                                    <input type="checkbox" name="sum_people"><label>Скрыть количество участников</label>
                                </div>
                            </div>
                        </div>

                        <div class="full-width">
                            <p class="btst-label-p">Цена вебинара КЭЛ</p>
                            <input type="number" name="price" placeholder="Цена" class="form-control" style="margin-top: 25px;">
                        </div>
                        <input type="hidden" name="id_user" value="<?=Auth::user()->id;?>">
                        <input type="submit" name="" value="Добавить" class="btn-crt-webinar">

                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>






        <script>
            $(function () {
                $('#datetimepicker2').datetimepicker(
                    {format: 'DD-MM-YYYY', locale: 'ru'}
                );
            });
        </script>
        <script>
            $(function () {
                $('#datetimepicker4').datetimepicker(
                    {format: 'HH:mm', locale: 'ru'}
                );
                $('#datetimepicker4_2').datetimepicker(
                    {format: 'HH:mm', locale: 'ru'}
                );
            });
        </script>
        <script type="text/javascript">
            $('.menu-tab-webinar').click(function() {
                var tab = $(this).attr('data-tab');

                $('.menu-tab-webinar').removeClass('active');
                $('.info-block-webinar').removeClass('actives');
                $('#m_tab_' + tab).addClass('active');
                $('#tab_' + tab).addClass('actives');
            });
        </script>  
        @endif
    </div>
        
        
@endsection


