@extends('layouts.admin')
@section('content')
    <?php $title_page='Архив'; ?>
<div class="col-md-12 first-admin-home-block">
        @if(Auth::user()->isAdmin())
            
        @endif
        @if(Auth::user()->isCadet() OR Auth::user()->isAdmin() OR Auth::user()->isLecturer())
        <div class="row">
            <div class="content-create-webinar">
                <div class="col-md-3 menu-tab-webinar" data-tab="1" id="m_tab_1">
                    <a href="{{route('catalog_webinars')}}?tab=1" class="a-href"><span>Предстоящие</span></a>
                </div>
                <div class="col-md-3 menu-tab-webinar" data-tab="2" id="m_tab_2">
                    <a href="{{route('catalog_webinars')}}?tab=2" class="a-href"><span>Приобретенные</span></a>
                </div>
                <div class="col-md-3 menu-tab-webinar active" data-tab="2" id="m_tab_2">
                    <span>Прошедшие</span>
                </div>
            </div>

            <div class="info-block-webinar actives" id="tab_1">
                @foreach ($webinars as $webinar)
                    @foreach ($posters as $poster)
                    <? $counts = 0; ?>
                        @foreach ($count as $val)
                            <?php if($webinar->id == $val->webinar_id){
                                $counts++;
                            } ?>
                        @if($webinar->id == $val->webinar_id)
                        <div class="block-webinar">
                            <div class="col-md-3">
                                <a href="{{ route('MywebinarShow', ['id' => $webinar->id]) }}"><img src="http://{{ $_SERVER['SERVER_NAME'] }}{{ $poster }}"></a>
                                <div class="date-time">Дата вебинара: {{ $webinar->date_webinar }} <br>Время вебинара: {{ $webinar->time_start }}</div>
                            </div>
                            <div class="col-md-9 position-none">
                                <div class="title"><h4>{{ $webinar->name_webinar }}</h4></div>
                                <div class="description">{!! $webinar->description !!}</div>
                                <div class="tree-block">
                                    <div class="avtor-webinar-prev">
                                        <img src="http://<?=$_SERVER['SERVER_NAME'];?>/{{ $webinar->avatar_link }}">
                                        <div class="name-user-prev">{{ $webinar->name }}</div>
                                        <div class="count-users-webinar"><i class="fa fa-users" aria-hidden="true"></i>{{ $counts }}</div>
                                        <div class="link-webinar"><a href="#">Просмотр</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        @endforeach
                    @endforeach
                @endforeach
            </div>
            <div class="info-block-webinar" id="tab_2">
                2
            </div>
        </div>

        @endif
    </div>
        
<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
<script type="text/javascript">
    /*$('.menu-tab-webinar').click(function() {
        var tab = $(this).attr('data-tab');

        $('.menu-tab-webinar').removeClass('active');
        $('.info-block-webinar').removeClass('actives');
        $('#m_tab_' + tab).addClass('active');
        $('#tab_' + tab).addClass('actives');
    });*/
</script>   
@endsection





