@extends('layouts.admin')
@section('content')
    <?php $title_page='Мои вебинары'; ?>
<div class="col-md-12 first-admin-home-block">
        @if(Auth::user()->isAdmin())
        <div class="row">
            <!--<div class="content-create-webinar">
                <div class="col-md-3 menu-tab-webinar active" data-tab="1" id="m_tab_1">
                    <span>Предстоящие</span>
                </div>
                <div class="col-md-3 menu-tab-webinar" data-tab="2" id="m_tab_2">
                    <span>Приобретенные</span>
                </div>
            </div>-->

            <div class="info-block-webinar actives" id="tab_1">
                @foreach ($webinars as $webinar)
                    @foreach ($posters as $poster)
                    @if($webinar->arhiv != 1)
                    <?php  
                    if($webinar->image == null){
                        $webinar_img = $poster;
                    }else{
                        $webinar_img = $webinar->image;
                    }
                    ?>
                        <div class="block-webinar">
                            <div class="col-md-3">
                                <img src="http://{{ $_SERVER['SERVER_NAME'] }}{{ $webinar_img }}">
                                <div class="date-time">Дата вебинара: {{ $webinar->date_webinar }} <br>Время вебинара: {{ $webinar->time_start }}</div>
                            </div>
                            <div class="col-md-9 position-none">
                                <div class="title"><h4>{{ $webinar->name_webinar }}</h4></div>
                                <div class="description">{!! $webinar->description !!}</div>
                                <div class="tree-block">
                                    <div class="avtor-webinar-prev">
                                        <img src="http://<?=$_SERVER['SERVER_NAME'];?>/{{ $webinar->avatar_link }}">
                                        <div class="count-users-webinar"><i class="fa fa-users" aria-hidden="true"></i>{{ $webinar->count_user }}</div>
                                        <div class="link-webinar"><a href="{{ route('EditWebinarAdmin', ['id' => $webinar->id]) }}">Редактировать</a></div>
                                    </div>
                                    <div class="name-user-prev">{{ $webinar->name }}</div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @endforeach
                @endforeach
            </div>
            <div class="info-block-webinar" id="tab_2">
                2
            </div>
        </div>

        @endif
    </div>
        
<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
<script type="text/javascript">
    $('.menu-tab-webinar').click(function() {
        var tab = $(this).attr('data-tab');

        $('.menu-tab-webinar').removeClass('active');
        $('.info-block-webinar').removeClass('actives');
        $('#m_tab_' + tab).addClass('active');
        $('#tab_' + tab).addClass('actives');
    });
</script>   
@endsection





