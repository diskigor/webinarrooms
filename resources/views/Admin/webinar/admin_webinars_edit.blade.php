@extends('layouts.admin')
@section('content')
    <?php $title_page=$webinar->name_webinar; ?>
<div class="col-md-12 first-admin-home-block">
        @if(Auth::user()->isAdmin())
            
            <? //Только для админа ?>
            
        @endif
        @if(Auth::user()->isAdmin())
        <form method="POST" action="{{ route('SavedEditWebinar') }}">
        <div class="row">
            <div class="content-create-webinar">
                <div class="col-md-3 menu-tab-webinar active" data-tab="1" id="m_tab_1">
                    <span>Основное</span>
                </div>
                <div class="col-md-3 menu-tab-webinar" data-tab="2" id="m_tab_2">
                    <span>Участники</span>
                </div>
                <div class="col-md-3 menu-tab-webinar" data-tab="3" id="m_tab_3">
                    <span>Тесты/Опросы</span>
                </div>
                <div class="col-md-3 menu-tab-webinar" data-tab="4" id="m_tab_4">
                    <span>Материалы</span>
                </div>
            </div>

            <div class="block-basik">
                <div class="col-md-12 info-block-webinar actives" id="tab_1">
                    <div class="clock-bulb"><i class="fa fa-lightbulb-o" aria-hidden="true"></i></div><div class="info-bulb-block">Основная информация</div>
                        <label class="name-lb-vebinar">Название вебинара</label>
                        <input type="text" name="name" class="form-control" placeholder="Название вебинара" value="{{ $webinar->name_webinar }}">
                        <textarea name="description" placeholder="описание вебинара" value="">{{ $webinar->description }}</textarea>
                        <div class="row">
                            <div class="col-sm-4">
                                <p class="btst-label-p">Дата</p>
                                <div class="form-group">
                                  <div class="input-group" id="datetimepicker2">
                                    <input type="data" name="date_webinar" class="form-control" value="{{ $webinar->date_webinar }}" />
                                    <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                  </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <p class="btst-label-p">Время начала</p>
                                <div class="form-group">
                                  <div class="input-group glyphicons-clock" id="datetimepicker4">
                                    <input type="time" name="time_start" class="form-control" value="{{ $webinar->time_start }}" />
                                    <span class="input-group-addon">
                                      <span class="glyphicons glyphicons-clock"></span>
                                    </span>
                                  </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <p class="btst-label-p">Время окончания</p>
                                <div class="form-group">
                                  <div class="input-group glyphicons-clock" id="datetimepicker4_2">
                                    <input type="time" name="time_stop" class="form-control" value="{{ $webinar->time_stop }}" />
                                    <span class="input-group-addon">
                                      <span class="glyphicons glyphicons-clock"></span>
                                    </span>
                                  </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <p class="btst-label-p" style="color: #00a9fd;">Часовой пояс</p>
                                <select name="timezone" class="timezone_select">
                                    <option value="Киев">Киев</option>
                                    <option value="Москва">Москва</option>
                                </select>
                            </div>
                        </div>
                        <?
                            if($webinar->chat == 'on'){
                                $echo = 'checked';
                            }else{
                                $echo = ' ';
                            }
                            if($webinar->question == 'on'){
                                $per_2 = 'checked';
                            }else{
                                $per_2 = ' ';
                            }
                            if($webinar->question_sound == 'on'){
                                $par_3 = 'checked';
                            }else{
                                $par_3 = ' ';
                            }
                            if($webinar->type_message == 'on'){
                                $echo_4 = 'checked';
                            }else{
                                $echo_4 = ' ';
                            }
                            if($webinar->list_people == 'on'){
                                $echo_5 = 'checked';
                            }else{
                                $echo_5 = ' ';
                            }
                            if($webinar->sum_people == 'on'){
                                $echo_6 = 'checked';
                            }else{
                                $echo_6 = ' ';
                            }
                        ?>
                        <div class="row cls-margin">
                            <div class="col-sm-6">
                                <div class="full-width">
                                    <input type="checkbox" name="chat" <?=$echo;?>><label>Включить чат</label>
                                </div>
                                <div class="full-width">
                                    <input type="checkbox" name="question" <?=$per_2;?>><label>Задать вопрос в чате</label>
                                </div>
                                <div class="full-width">
                                    <input type="checkbox" name="question_sound" <?=$par_3;?>><label>Задать вопрос голосом</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="full-width">
                                    <input type="checkbox" name="type_message" <?=$echo_4;?>><label>Сообщения в чате видны только ведущему</label>
                                </div>
                                <div class="full-width">
                                    <input type="checkbox" name="list_people" <?=$echo_5;?>><label>Скрыть список участников</label>
                                </div>
                                <div class="full-width">
                                    <input type="checkbox" name="sum_people" <?=$echo_6;?>><label>Скрыть количество участников</label>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="{{ $webinar->id }}">
                        <input type="submit" name="btn_one" value="Обновить" class="btn-crt-webinar">

                        {{ csrf_field() }}
                    </form>
                </div>

                <div class="col-md-12 info-block-webinar" id="tab_2">
                    <label class="lab-vebinars">Добавьте участника</label>
                    <input list="users" name="list_users" class="list_users" placeholder="Добавьте участника (email, ник)">
                    <datalist id="users">
                        @foreach($users as $user)
                            <option data-id="{{ $user->id }}" value="{{ $user->email }}">{{ $user->name }}<option>
                        @endforeach
                    </datalist>
                    <div class="add_user"><i class="fa fa-share" aria-hidden="true"></i></div>
                    <div class="lists_users" style="display: none;"></div>
                    <ol class="ol_list"></ol>
                    <label class="lab-vebinars">Добавить модераторов</label>
                    <input type="hidden" name="list_users_add" class="add_list_users">

                    <input list="users_lektor" name="list_users_lektor" class="list_users_lektor" placeholder="Вебинар не выбран">
                    <datalist id="users_lektor">
                        @foreach($users as $user)
                            <option data-id="{{ $user->id }}" value="{{ $user->email }}">{{ $user->name }}<option>
                        @endforeach
                    </datalist>
                    <div class="add_user_lektor">Добавить</div>
                    <div class="lists_users_lektor" style="display: none;"></div>
                    <ol class="ol_list_lektor"></ol>
                    <input type="hidden" name="list_users_add_lektor" class="add_list_users_lektor">


                    <label class="lab-vebinars">Отправить напоминание участнику</label>

                    <div class="lb-min-desc">
                        <label class="lab-vebinars-desc">Письмо 1:</label>
                        <select name="message_1">
                            <option>Выберите время</option>
                            <option value="00:00">00:00</option>
                            <option value="01:00">01:00</option>
                            <option value="02:00">02:00</option>
                            <option value="03:00">03:00</option>
                            <option value="04:00">04:00</option>
                            <option value="05:00">05:00</option>
                            <option value="06:00">06:00</option>
                            <option value="07:00">07:00</option>
                            <option value="08:00">08:00</option>
                            <option value="09:00">09:00</option>
                            <option value="10:00">10:00</option>
                            <option value="11:00">11:00</option>
                            <option value="12:00">12:00</option>
                            <option value="13:00">13:00</option>
                            <option value="14:00">14:00</option>
                            <option value="15:00">15:00</option>
                            <option value="16:00">16:00</option>
                            <option value="17:00">17:00</option>
                            <option value="18:00">18:00</option>
                            <option value="19:00">19:00</option>
                            <option value="20:00">20:00</option>
                            <option value="21:00">21:00</option>
                            <option value="22:00">22:00</option>
                            <option value="23:00">23:00</option>
                        </select>
                    </div>

                    <div class="lb-min-desc">
                        <label class="lab-vebinars-desc">Письмо 2:</label>
                        <select name="message_2">
                            <option>Выберите время</option>
                            <option value="00:00">00:00</option>
                            <option value="01:00">01:00</option>
                            <option value="02:00">02:00</option>
                            <option value="03:00">03:00</option>
                            <option value="04:00">04:00</option>
                            <option value="05:00">05:00</option>
                            <option value="06:00">06:00</option>
                            <option value="07:00">07:00</option>
                            <option value="08:00">08:00</option>
                            <option value="09:00">09:00</option>
                            <option value="10:00">10:00</option>
                            <option value="11:00">11:00</option>
                            <option value="12:00">12:00</option>
                            <option value="13:00">13:00</option>
                            <option value="14:00">14:00</option>
                            <option value="15:00">15:00</option>
                            <option value="16:00">16:00</option>
                            <option value="17:00">17:00</option>
                            <option value="18:00">18:00</option>
                            <option value="19:00">19:00</option>
                            <option value="20:00">20:00</option>
                            <option value="21:00">21:00</option>
                            <option value="22:00">22:00</option>
                            <option value="23:00">23:00</option>
                        </select>
                    </div>
                    <textarea name="text_message"></textarea>
                    <input type="submit" name="btn_list" value="Добавить" class="input-sbm">
                    
                </div>
            </div>
        </div>
        </form>


        <script>
            $(function () {
                $('#datetimepicker2').datetimepicker(
                    {format: 'DD-MM-YYYY', locale: 'ru'}
                );
            });
        </script>
        <script>
            $(function () {
                $('#datetimepicker4').datetimepicker(
                    {format: 'HH:mm', locale: 'ru'}
                );
                $('#datetimepicker4_2').datetimepicker(
                    {format: 'HH:mm', locale: 'ru'}
                );
            });
        </script>
        <script type="text/javascript">
            $('.menu-tab-webinar').click(function() {
                var tab = $(this).attr('data-tab');

                $('.menu-tab-webinar').removeClass('active');
                $('.info-block-webinar').removeClass('actives');
                $('#m_tab_' + tab).addClass('active');
                $('#tab_' + tab).addClass('actives');
            });
            $('.add_user').click(function(){
                var user = $('.list_users').val();
                $('.lists_users').append(user + ",");
                $(".ol_list").append("<li>" + user + "</li>");
                var users = $('.lists_users').text();
                $('.add_list_users').val(users);
            });

            $('.add_user_lektor').click(function(){
                var user = $('.list_users_lektor').val();
                $('.lists_users_lektor').append(user + ",");
                $(".ol_list_lektor").append("<li>" + user + "</li>");
                var users = $('.lists_users_lektor').text();
                $('.add_list_users_lektor').val(users);
            });
        </script> 
        @endif
    </div>
        
        
@endsection


