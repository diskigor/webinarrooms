@extends('layouts.admin')
@section('content')
    <?php $title_page='Афиша предстоящих вебинаров'; ?>
<div class="col-md-12 first-admin-home-block">
        @if(Auth::user()->isAdmin())
            
        @endif
        @if(Auth::user()->isCadet() OR Auth::user()->isAdmin() OR Auth::user()->isLecturer())
        <div class="row">
            <div class="content-create-webinar">
                <div class="col-md-3 menu-tab-webinar active" data-tab="1" id="m_tab_1">
                    <span>Предстоящие</span>
                </div>
                <div class="col-md-3 menu-tab-webinar" data-tab="2" id="m_tab_2">
                    <span>Приобретенные</span>
                </div>
                <div class="col-md-3 menu-tab-webinar" data-tab="3" id="m_tab_3">
                    <a href="{{route('arhiv')}}" class="a-href"><span>Прошедшие</span></a>
                </div>
            </div>

            <div class="info-block-webinar actives" id="tab_1">
                @foreach ($webinars as $webinar)
                    @foreach ($posters as $poster)
                    <? if($webinar->arhiv != 1){ ?>
                        <div class="block-webinar">
                            <div class="col-md-3">
                                <?php 
                                if($webinar->image == null){
                                    $webinar_img = $poster;
                                }else{
                                    $webinar_img = $webinar->image;
                                }
                                ?>
                                <a href="{{ route('webinarShow', ['id' => $webinar->id]) }}"><img src="http://{{ $_SERVER['SERVER_NAME'] }}{{ $webinar_img }}"></a>

                                <div class="date-time">Дата вебинара: {{ $webinar->date_webinar }} <br>Время вебинара: {{ $webinar->time_start }}</div>
                            </div>
                            <div class="col-md-9 position-none">
                                <div class="title"><h4>{{ $webinar->name_webinar }}</h4></div>
                                <div class="description">{!! $webinar->description !!}</div>
                                <div class="tree-block">
                                    <div class="avtor-webinar-prev">
                                        <img src="http://<?=$_SERVER['SERVER_NAME'];?>/{{ $webinar->avatar_link }}">
                                        <div class="name-user-prev">{{ $webinar->name }}</div>
                                    	<div class="count-users-webinar"><i class="fa fa-users" aria-hidden="true"></i>{{ $webinar->count_user }}</div>
                                        <div class="link-webinar"><a href="{{ route('webinarShow', ['id' => $webinar->id]) }}">Подробнее</a></div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    <? } ?>
                    @endforeach
                @endforeach
            </div>
            <div class="info-block-webinar" id="tab_2">
                @foreach ($webinars_wallet as $val)
                    @foreach ($posterss as $post)
                    <?php 
                    if($val->image == null){
                        $webinar_img = $post;
                    }else{
                        $webinar_img = $val->image;
                    }
                    ?>
                    @if($val->arhiv == 1)
                        <div class="block-webinar">
                            <div class="col-md-3">
                                <a href="#"><img src="http://{{ $_SERVER['SERVER_NAME'] }}{{ $webinar_img }}"></a>
                                <div class="date-time">{{ $val->date_webinar }} {{ $val->time_start }}</div>
                            </div>
                            <div class="col-md-9 position-none">
                                <div class="title"><h4>{{ $val->name_webinar }}</h4></div>
                                <div class="description">{!! $val->description !!}</div>
                                <div class="tree-block">
                                    <div class="avtor-webinar-prev">
                                        <img src="http://<?=$_SERVER['SERVER_NAME'];?>/{{ $val->avatar_link }}">
                                        <div class="name-user-prev">{{ $val->name }}</div>
                                        <div class="count-users-webinar"><i class="fa fa-users" aria-hidden="true"></i>{{ $val->count_user }}</div>
                                        <div class="link-webinar"><a href="#">Просмотреть</a></div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($val->arhiv == 0)
                        <div class="block-webinar">
                            <div class="col-md-3">
                                <a href="{{ route('webinarShow', ['id' => $webinar->id]) }}"><img src="http://{{ $_SERVER['SERVER_NAME'] }}{{ $webinar_img }}"></a>
                                <div class="date-time">{{ $val->date_webinar }} {{ $val->time_start }}</div>
                            </div>
                            <div class="col-md-9 position-none">
                                <div class="title"><h4>{{ $val->name_webinar }}</h4></div>
                                <div class="description">{!! $val->description !!}</div>
                                <div class="tree-block">
                                    <div class="avtor-webinar-prev">
                                        <img src="http://<?=$_SERVER['SERVER_NAME'];?>/{{ $val->avatar_link }}">
                                        <div class="name-user-prev">{{ $val->name }}</div>
                                        <div class="count-users-webinar"><i class="fa fa-users" aria-hidden="true"></i>{{ $val->count_user }}</div>
                                        <div class="link-webinar"><a href="{{ route('webinarShow', ['id' => $val->webinar_id]) }}">Подробнее</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @endforeach
                @endforeach
            </div>
        </div>

        @endif
    </div>
        
<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
<script type="text/javascript">
    function $_GET(key) {
        var s = window.location.search;
        s = s.match(new RegExp(key + '=([^&=]+)'));
        return s ? s[1] : false;
    }

    $('.menu-tab-webinar').removeClass('active');
    $('.info-block-webinar').removeClass('actives');
    $('#m_tab_' + $_GET('tab')).addClass('active');
    $('#tab_' + $_GET('tab')).addClass('actives');

    $('.menu-tab-webinar').click(function() {
        var tab = $(this).attr('data-tab');

        $('.menu-tab-webinar').removeClass('active');
        $('.info-block-webinar').removeClass('actives');
        $('#m_tab_' + tab).addClass('active');
        $('#tab_' + tab).addClass('actives');
    });
</script>   
@endsection





