@extends('layouts.admin')
@section('content')
<div class="col-md-12 first-admin-home-block">
    @if(Auth::user()->isAdmin())
        
    @endif

    @foreach ($webinar as $webinars)
    <?php $title_page = $webinars->name_webinar; 
    ?>

    <script type="text/javascript">
        //Get API instance
        var f = Flashphoner.getInstance();
        var clientId;
        var login;
        var roomName;
        $(document).ready(function () {
            initOnLoad();
        });

        window.my_mute = false;

        function retrieveRoomName() {
            var address = window.location.toString();
            var pattern = /https?:\/\/.*\?roomName\=(.*)/;
            var match = address.match(pattern);
            if (match) {
                return match[1];
            } else {
                return null;
            }
        }

        function initOnLoad() {
            if (detectBrowser() == "Android" || detectBrowser() == "iOS") {
                for (var i = 0; i < 3; i++) {
                    $("#mobile").append("<div class=\"row-space\">&nbsp</div>");
                }
            }

            $(".connectBtn").click(function () {
                if ($(this).text() == "Подключиться к вебинару") {
                    console.log('Connect');
                    if (connect()) {
                        $(this).prop('disabled',false);
                    }
                    $('#connectBtn').remove();
                } 
                if ($("#connectBtn").text() == "Пауза"){
                    $('.video-remote').addClass('addClass');
                    $('#connectBtn').addClass('pause');

                }else{
                    console.log('Remove');
                    $('#connectBtn').html('Продолжить');
                }
                if($(this).text() == "Начать"){
                    if (connect()) {
                        $(this).prop('disabled',false);
                    }
                }
            });

            $('.red-square').click(function(){
                unPublishStream();
                $('#visitors').val(0);
            })

            $("#publishBtn").prop('disabled', false).click(function () {
                if($('.time').val() == '00:00:00.00'){
                    StartStop();
                }
                    var state = $("#publishBtn").text();
                    $("#publishBtn").removeClass('start');
                    if (state == "Начать") {
                        publishStream();
                        //$('#connectionStatus').html('Трансляция в эфире');
                        $('#visitors').val(0);
                    } else {
                        $('#connectionStatus').html('Пауза');
                        $('#publishBtn').html('Продолжить');
                        mute();
                        muteVideo();
                    }
                    if(state == 'Продолжить'){
                        $('#publishBtn').html('Пауза');
                        unmute();
                        unmuteVideo();
                    }
                    $(this).prop('disabled', false);
                }
            );

            // Set websocket URL
            setURL();

            //add listeners
            f.addListener(WCSEvent.ErrorStatusEvent, errorEvent);
            f.addListener(WCSEvent.ConnectionStatusEvent, connectionStatusListener);
            f.addListener(WCSEvent.StreamStatusEvent, streamStatusListener);

            if (detectIE()) {
                detectFlash();
            }
            // Configure remote and local video elements
            var configuration = new Configuration();
            configuration.remoteMediaElementId = 'remoteVideo';
            configuration.localMediaElementId = 'localVideo';
            configuration.elementIdForSWF = "flashVideoDiv";
            configuration.pathToSWF = "../../../dependencies/flash/MediaManager.swf";
            configuration.flashBufferTime = 0.0;

            f.init(configuration);

            // Hide WebRTC elements for IE and Flash based browsers. Hide flash elements for WebRTC based browsers.
            if (webrtcDetectedBrowser) {
                document.getElementById('remoteVideo').style.visibility = "visible";
                document.getElementById('flashVideoWrapper').style.visibility = "hidden";
                document.getElementById('flashVideoDiv').style.visibility = "hidden";
            } else {
                document.getElementById('remoteVideo').style.visibility = "hidden";
                document.getElementById('flashVideoWrapper').style.visibility = "visible";
                document.getElementById('localVideo').style.visibility = "hidden";
            }
        }


        function connect() {
            if (!checkForEmptyField('#login', '#loginForm')) {
                return false;
            }
            login = field("login").replace(/\s+/g, '');
            clientId = createUUID();
            f.connect({
                clientId: clientId,
                login: login,
                urlServer: field("urlServer"),
                appKey: 'defaultApp',
                width:0,
                height:0
            });
        }

        var currentStream;

        //Publish stream
        function publishStream() {
            $("#downloadDiv").hide();
            var streamName = "stream-{{ $webinars->id }}";
            currentStream = {name: streamName};
            f.publishStream(currentStream);
        }

        //Stop stream publishing
        function unPublishStream() {
            var streamName = "stream-{{ $webinars->id }}";
            f.unPublishStream(currentStream);
            currentStream = null;
        }


        function disconnect() {
            f.disconnect();
        }

        function mute() {
            if (currentStream) {
                f.mute(currentStream.mediaProvider);
            }
        }

        // Unmute audio in the call
        function unmute() {
            if (currentStream) {
                f.unmute(currentStream.mediaProvider);
            }
        }

        // Mute video in the call
        function muteVideo() {
            if (currentStream) {
                f.muteVideo(currentStream.mediaProvider);
            }
        }

        // Unmute video in the call
        function unmuteVideo() {
            if (currentStream) {
                f.unmuteVideo(currentStream.mediaProvider);
            }
        }

        ///////////////////////////////////////////
        //////////////Listeners////////////////////
        function connectionStatusListener(event) {
            if (event.status == ConnectionStatus.Established) {
                console.log('Connection has been established.');
                $("#connectBtn").text("Пауза").prop('disabled', false);
                $("#publishBtn").prop('disabled', false);
                roomName = 'stream{{ $webinars->id }}';
                if (!roomName) {
                    roomName = createUUID();
                }
                setInviteLink();

                f.subscribeRoom(roomName, roomStatusEventListener, this);
            } else if (event.status == ConnectionStatus.Disconnected || event.status == ConnectionStatus.Failed) {
                $("#connectBtn").text("Подключиться к вебинару").prop('disabled', false);
                $("#publishBtn").text("Начать").prop('disabled', false);
                setPublishStatus("");
                $("#muteAudioToggle").prop('checked',false).attr('disabled','disabled').trigger('change');
                $("#muteVideoToggle").prop('checked',false).attr('disabled','disabled').trigger('change');
                currentStream = null;
                var participantEl = $(".participant").first();
                if (participantEl) {
                    participantEl.find(".p-login").text("Offline");
                    participantEl.find(".fp-userState").removeClass("online").removeClass("streaming");
                    participantEl.addClass("free").removeAttr("login").removeAttr("stream");
                    var video = participantEl.find("video").get(0);
                    video.pause();
                    video.src = '';
                }
            }
            if (event.status == ConnectionStatus.Failed) {

                f.disconnect();
            }
            setConnectionStatus(event.status);
        }

        function streamStatusListener(event) {
            if (event.published) {
                trace("streamStatusListener >> " + event.status);
                switch (event.status) {
                    case StreamStatus.Publishing:
                        setPublishStatus(event.status);
                        $("#publishBtn").text("Пауза").prop('disabled', false);
                        var $muteAudioToggle = $("#muteAudioToggle");
                        var $muteVideoToggle = $("#muteVideoToggle");
                        $muteAudioToggle.removeAttr("disabled");
                        $muteAudioToggle.trigger('change');
                        $muteVideoToggle.removeAttr("disabled");
                        $muteVideoToggle.trigger('change');
                        break;
                    case StreamStatus.Unpublished:
                    case StreamStatus.Failed:
                        setPublishStatus(event.status);
                        $("#publishBtn").text("Начать").prop('disabled', false);
                        $("#muteAudioToggle").prop('checked',false).attr('disabled','disabled').trigger('change');
                        $("#muteVideoToggle").prop('checked',false).attr('disabled','disabled').trigger('change');
                        break;
                    default:
                        break;
                }
            }
        }
        //Error
        function errorEvent(event) {
            console.log(event.info);
        }

        function writeInfo(str) {
            console.log(str);
        }

        function sendMessage() {
            var date = new Date();
            var time = date.getHours() + ":" + (date.getMinutes()<10?'0':'') + date.getMinutes();
            f.sendRoomData(roomName, {
                operationId: createUUID(),
                payload: field("message")
            });
            var newMessage = time + " " + field("login") + " - " + field("message").split('\n').join('<br/>') + '<br/>';
            var chat = document.getElementById("chat");
            chat.innerHTML += newMessage ;
            $("#chat").scrollTop(chat.scrollHeight);
            document.getElementById("message").value = "";
        }


        function roomStatusEventListener(event) {
            var date = new Date();
            var time = date.getHours() + ":" + (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
            var message = event;
            var participantEl;
            if (message.owner.clientId != clientId) {
                switch (message.status) {

                    case "JOINED":
                        addMessage(time + " " + "<i>" + message.owner.login + " подключился" + '<br/></i>');
                        participantEl = $(".participant.free").first();
                        var visitors = +$('#visitors').val() + 1;
                        $('#visitors').val(visitors);
                        $('#list-visitors').append('<li id="'+message.owner.login+'" class="visitor">'+message.owner.login+'</li>');
                        var vis = $('.visitor').length;
                        $('.visitors').html(vis);
                        if (participantEl) {
                            participantEl.find(".p-login").text(message.owner.login);
                            participantEl.find(".fp-userState").addClass("online");
                            participantEl.removeClass("free").attr("login", message.owner.login);
                        }
                        break;
                    case "DISCONNECTED":
                        addMessage(time + " " + "<i>" + message.owner.login + " отключился" + '<br/></i>');
                        participantEl = $(".participant[login='" + message.owner.login +"']").first();
                        var visitors = +$('#visitors').val() - 1;
                        $('#visitors').val(visitors);
                        $('.visitors').html(visitors);
                        $('#' + message.owner.login).remove();
                        var vis = $('.visitor').length;
                        $('.visitors').html(vis);
                        if (participantEl) {
                            participantEl.find(".p-login").text("Offline");
                            participantEl.find(".fp-userState").removeClass("online").removeClass("streaming");
                            participantEl.addClass("free").removeAttr("login").removeAttr("stream");
                            var video = participantEl.find("video").get(0);
                            video.pause();
                            video.src = '';
                        }
                        break;
                    case "PUBLISHING":
                        addMessage(time + " " + "<i>" + message.owner.login + " начал трансляцию" + '<br/></i>');
                        participantEl = $(".participant[login='" + message.owner.login +"']").first();
                        $('.connect-users').html('Трансляция в эфире');
                        if (participantEl) {
                            var remoteMediaElementId = participantEl.find("video").attr("id");
                            f.playStream({name: message.streamName, remoteMediaElementId: remoteMediaElementId});
                            participantEl.find(".fp-userState").addClass("streaming");
                            participantEl.attr("stream", message.streamName);
                        }
                        break;
                    case "UNPUBLISHED":
                        addMessage(time + " " + "<i>" + message.owner.login + " закончил трансляцию" + '<br/></i>');
                        f.stopStream({name: message.streamName});
                        $('.connect-users').html('Проблемы со связью');
                        participantEl = $(".participant[login='" + message.owner.login +"']").first();
                        if (participantEl) {
                            var video = participantEl.find("video").get(0);
                            video.pause();
                            video.src = '';
                            participantEl.find(".fp-userState").removeClass("streaming");
                            participantEl.removeAttr("stream");
                        }
                        break;
                    case "SENT_DATA":
                    <? if($webinars->id_user == Auth::user()->id OR Auth::user()->role_id == 1){ ?>
                        addMessage(time + " " + message.owner.login + " - " + message.data.payload.split('\n').join('<br/>') + '<br/>');
                        console.log('true admin');
                        break;
                    <? }else{ ?>
                        addMessage(time + " " + message.owner.login + " - " + message.data.payload.split('\n').join('<br/>') + '<br/>');
                        console.log('false user');
                    <? } ?>
                    default:
                        break;

                }
            }

        }

        function addMessage(msg) {
            var chat = document.getElementById("chat");
            chat.innerHTML += msg;
            $("#chat").scrollTop(chat.scrollHeight);
        }

        // Set connection status and display corresponding view
        function setConnectionStatus(status) {
            if (status == "ESTABLISHED") {
                $("#connectionStatus").text('Трансляциия в эфире').removeClass().attr("class", "text-success");
            }

            if (status == "DISCONNECTED") {
                $("#connectionStatus").text(status).removeClass().attr("class", "text-muted");
            }

            if (status == "FAILED") {
                $("#connectionStatus").text(status).removeClass().attr("class", "text-danger");
            }
        }

        // Set Stream Status
        function setPublishStatus(status) {

            $("#publishStatus").className = '';

            if (status == "PUBLISHING") {
                $("#publishStatus").attr("class", "text-success");
            }

            if (status == "UNPUBLISHED") {
                $("#publishStatus").attr("class", "text-muted");
            }

            if (status == "FAILED") {
                $("#publishStatus").attr("class", "text-danger");
            }

            $("#publishStatus").text(status);
        }

        //Set WCS URL
        function setURL() {
            var proto;
            var url;
            var port;
            if (window.location.protocol == "http:") {
                proto = "ws://";
                port = "8080";
            } else {
                proto = "wss://";
                port = "8443";
            }

            url = proto + window.location.hostname + ":" + port;
            document.getElementById("urlServer").value = 'wss://94.130.37.75:8443';
        }

        function setInviteLink() {
            url = window.location.href + "?roomName=" + roomName;
            document.getElementById("inviteLink").value = url;
        }

        //Get field
        function field(name) {
            return document.getElementById(name).value;
        }

        // Check field for empty string
        function checkForEmptyField(checkField, alertDiv) {
            if (!$(checkField).val()) {
                $(alertDiv).addClass("has-error");
                return false;
            } else {
                $(alertDiv).removeClass("has-error");
                return true;
            }
        }
    </script>
    <? if($webinars->id_user == Auth::user()->id OR Auth::user()->role_id == 1){ ?>
    <div class="row">
        <div class="col-md-7">
            <div class="time-and-date">{{ $webinars->date_webinar }} {{ $webinars->time_start }}</div>
            <div class="desc-webinar"><img src="http://<?=$_SERVER['SERVER_NAME'];?>/{{ $webinars->avatar_link }}" class="img-author-web">
            <div class="name-user-prev">{{ $webinars->name }}</div>
            <div style="text-align: center; display: flex; justify-content: space-evenly; padding-top: 10px; font-size: 20px;">{{ $webinars->name_webinar }}</div>
            </div>
            <!-- Video block -->
            <div class="my-video">

                <input type="hidden" value="0" id="visitors">

                <!-- 320x240 Flash Video Block -->
                <div id="flashVideoWrapper" class="fp-my-videio">
                    <div id="flashVideoDiv">
                    </div>
                </div>

                <!-- 80x60 WebRTC Self captured video block -->
                <video id="localVideo" autoplay="" class="my-video-vide"></video>

                <div class="fp-my-videio-control" id="videoControls">
                    <div class="row" style="margin-top: 20px">
                        <div class="col-sm-12">
                            <form method="POST" action="{{ route('AddWebinarArgiv') }}" class="form-with-red-square" name = 'MyForm'>
                                <?php if ($webinars->arhiv == 0){ ?>
                                    <button id="publishBtn" type="button" class="btn btn-default connectBtn start" style="display: block !important;">Начать</button>
                                    <input type="hidden" name="webinar_id" value="{{ $webinars->id }}">
                                    <button type="submit" name="square" class="red-square"><i class="fa fa-square" aria-hidden="true"></i></button>
                                
                                    <input id="muteAudioToggle" type="checkbox" disabled/>
                                    <div class="ey-numb">
                                        <i class="fa fa-eye" aria-hidden="true"></i><div class="visitors"> 0</div>
                                    </div>
                                    <span id="connectionStatus" class="ln-height-span" style="top: 0px;"></span>  
                                    <input name=stopwatch size=10 value="00:00:00.00" class="time" style="border: 0px; margin-left: 15px;">
                                    <input type="submit" name="end" value="Завершить вебинар" class="btn-crt-webinar btn-add-arhiv">
                                    {{ csrf_field() }}
                                <?php }else{ ?>
                                    <button type="button" class="btn btn-default">Вебинар закончился</button>
                                <? } ?>
                            </form>
                        </div>
                    </div>
                </div>

                <script>

                    var $muteAudioToggle = $("#muteAudioToggle");
                    $muteAudioToggle.change(function () {
                        if (this.checked) {
                            mute();
                        } else {
                            unmute();
                        }
                    });

                    $muteAudioToggle.bootstrapSwitch({
                        on: '',
                        off: '',
                        size: 'md'
                    });

                    var $muteVideoToggle = $("#muteVideoToggle");
                    $muteVideoToggle.change(function () {
                        if (this.checked) {
                            muteVideo();
                        } else {
                            unmuteVideo();
                        }
                    });

                    $muteVideoToggle.bootstrapSwitch({
                        on: 'on',
                        off: 'off',
                        size: 'md'
                    });

                    //объявляем переменные
                    var base = 60; 
                    var clocktimer,dateObj,dh,dm,ds,ms; 
                    var readout=''; 
                    var h=1,m=1,tm=1,s=0,ts=0,ms=0,init=0; 
                    //функция для очистки поля
                    function ClearСlock() { 
                        clearTimeout(clocktimer); 
                        h=1;m=1;tm=1;s=0;ts=0;ms=0; 
                        init=0;
                        readout='00:01:00.00'; 
                        document.MyForm.stopwatch.value=readout; 
                    } 
                    //функция для старта секундомера
                    function StartTIME() { 
                        var cdateObj = new Date(); 
                        var t = (cdateObj.getTime() - dateObj.getTime())-(s*1000); 
                        if (t>999) { s++; } 
                        if (s>=(m*base)) { 
                            ts=0; 
                            m++; 
                        } else { 
                            ts=parseInt((ms/100)+s); 
                            if(ts>=base) { ts=ts-((m-1)*base); } 
                        } 
                        if (m>(h*base)) { 
                            tm=1; 
                            h++; 
                        } else { 
                            tm=parseInt((ms/100)+m); 
                            if(tm>=base) { tm=tm-((h-1)*base); } 
                        } 
                        ms = Math.round(t/10); 
                        if (ms>99) {ms=0;} 
                        if (ms==0) {ms='00';} 
                        if (ms>0&&ms<=9) { ms = '0'+ms; } 
                        if (ts>0) { ds = ts; if (ts<10) { ds = '0'+ts; }} else { ds = '00'; } 
                        dm=tm-1; 
                        if (dm>0) { if (dm<10) { dm = '0'+dm; }} else { dm = '00'; } 
                        dh=h-1; 
                        if (dh>0) { if (dh<10) { dh = '0'+dh; }} else { dh = '00'; } 
                        readout = dh + ':' + dm + ':' + ds + '.' + ms; 
                        document.MyForm.stopwatch.value = readout; 
                        clocktimer = setTimeout("StartTIME()",1); 
                    } 
                    //Функция запуска и остановки
                    function StartStop() {
                            ClearСlock();
                            dateObj = new Date(); 
                            StartTIME(); 
                            init=1;
                    } 
                </script>

                <div class="col-sm-9">
                    <input type="hidden" class="form-control" id="inviteLink"/>
                </div>

                
            </div>
            <!-- Connection fields -->
            <form id="formConnection" class="form-horizontal" role="form">
                <div id="urlServerForm" class="form-group">

                    <div class="col-sm-8">
                        <input type="hidden" class="form-control" id="urlServer" placeholder="WCS Server URL"/>
                    </div>
                </div>
                <div id="loginForm" class="form-group">

                    <div class="col-sm-4">
                        <input type="hidden" class="form-control" id="login" placeholder="Login" value="👑{{ Auth::user()->name }}">
                    </div>
                </div>
            </form>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <input type="text" id="message" class="form-control" style="resize: none;">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5 col-sm-offset-7">
                    <div class="pull-right">
                        <button id="sendBtn" type="button" class="btn btn-default" onclick="sendMessage(); return false;">Отправить в чат</button>
                    </div>
                </div>
            </div>
            <div class="list-visitors">
                <ol id="list-visitors"></ol>
            </div>
            
        </div>

        <div class="col-md-5">
            <div class="form-group">
                <h5 class="title-chatw">Онлайн чат</h5>
                <div id="chat" style="overflow-y: scroll; height: 600px;" class="text-left form-control"></div>
            </div>
        </div>
    </div>
    <? }else{ ?>
    <?php $access = true; ?>
    @foreach($pay as $pays)
        @if(Auth::user()->id != $pays->user_id)
            <div class="erorr-pay">Вы не преобрели доступ к данному вебинару.<br>Вы можете это сделать перейдя по <a href="https://webinar.dev-expo.top/admin/poster_webinars">данной ссылке</a></div>
            <?php $access = false; break; ?>
        @endif 
    @endforeach
    @if($access != false)
    <div class="row">
        <div class="col-md-7">
            <div class="time-and-date">{{ $webinars->date_webinar }} {{ $webinars->time_start }}</div>
            <div class="desc-webinar"><img src="http://<?=$_SERVER['SERVER_NAME'];?>/{{ $webinars->avatar_link }}" class="img-author-web">
            <div class="name-user-prev">{{ $webinars->name }}</div>
            <div style="text-align: center; display: flex; justify-content: space-evenly; padding-top: 10px; font-size: 20px;">{{ $webinars->name_webinar }}</div>
            </div>
            <!-- Video block -->
            <div class="my-video">
                <input type="hidden" value="0" id="visitors">
                <!-- 320x240 Flash Video Block -->
                <div id="flashVideoWrapper" class="fp-my-videio">
                    <div id="flashVideoDiv">
                    </div>
                </div>

                <!-- 320x240 WebRTC video block for remote video -->
                <div class="video-remote">
                    <video id="remoteVideo" autoplay="" class="my-video-vide" controls="controls"></video>
                </div>

                <div class="fp-my-videio-control" id="videoControls">
                    <div class="row" style="margin-top: 20px">
                        <div class="col-sm-12">
                            <button id="connectBtn" type="button" class="btn btn-default connectBtn">Подключиться к вебинару</button>
                            <div class="ey-numb">
                                @if($webinars->sum_people == 'on')
                                <i class="fa fa-eye" aria-hidden="true"></i><div class="visitors"> 0</div> 
                                @endif
                                <span id="connectionStatus" class="connect-users" style="top: 0;"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-9">
                    <input type="hidden" class="form-control" id="inviteLink"/>
                </div>
            </div>
            <!-- Connection fields -->
            <form id="formConnection" class="form-horizontal" role="form">
                <div id="urlServerForm" class="form-group">

                    <div class="col-sm-8">
                        <input type="hidden" class="form-control" id="urlServer" placeholder="WCS Server URL"/>
                    </div>
                </div>
                <div id="loginForm" class="form-group">

                    <div class="col-sm-4">
                        <input type="hidden" class="form-control" id="login" placeholder="Login" value="{{ Auth::user()->name }}">
                    </div>
                </div>
            </form>

            <? if($webinars->question == 'on'){ ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <input type="text" id="message" class="form-control" style="resize: none;">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5 col-sm-offset-7">
                    <div class="pull-right">
                        <button id="sendBtn" type="button" class="btn btn-default" onclick="sendMessage(); return false;">Отправить в чат</button>
                    </div>
                </div>
            </div>
            <? } ?>
            <? if($webinars->list_people == 'off'){
                $display = 'block';
            }else{
                $display = 'none';
            } ?>
            <div class="list-visitors" style="display: <?=$display;?>;">
                <ol id="list-visitors"></ol>
            </div>
        </div>
        <div class="col-md-5">
            <div class="form-group">
                <? if($webinars->chat == 'on' OR $webinars->type_message == 'off'){ ?>
                <h5 class="title-chatw">Онлайн чат</h5>
                <div id="chat" style="overflow-y: scroll; height: 600px;" class="text-left form-control"></div>
                <? } ?>
            </div>
        </div>
    </div>
    @endif
    <? } ?>
    @endforeach
</div>  
@endsection