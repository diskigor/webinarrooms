@extends('layouts.admin')
@section('content')
<div class="col-md-12 first-admin-home-block">
    @foreach ($webinar as $webinars)
    @if(Auth::user()->isAdmin())
        
    @endif
    <? 
    $button = '<input type="submit" name="" value="Оплатить">';
    $href = false;
    $succes_pay = false;
    $pay = null;
    ?>
    @foreach ($wallets as $wallet)
        <?php if($wallet->user_id == Auth::user()->id){
            $button = '';
            $href = true;
            $pay = 'Ждём Вас на вебинаре ' . $webinars->date_webinar . ' ' . $webinars->time_start . ' который Вы можете найти в разделе "Вебинары", подраздел "Афиша вебинаров", подраздел <a href="../catalog_webinars">"Предстоящие"</a>';
            $succes_pay = true;
        }
        if($wallet->user_id != Auth::user()->id){
            $button = '<input type="submit" name="" value="Оплатить">';
            $href = false;
            $pay = 'Для бронирования места на вебинара, Вам необходимо оплатить ' . $webinars->price . ' КЭЛ';
            $succes_pay = false;
        }
        ?>
    @endforeach

    <?php $title_page = $webinars->name_webinar; ?>
    <?php
    if($pay == null){
        $pay = 'Для бронирования места на вебинара, Вам необходимо оплатить ' . $webinars->price . ' КЭЛ';
    }
    ?>
    <div class="row">
    	<div class="col-md-7">
    		<div class="time-and-date"><span class="data-web-icon"><i class="fa fa-calendar" aria-hidden="true"></i>{{ $webinars->date_webinar }} </span> <i class="fa fa-clock-o" aria-hidden="true"></i>{{ $webinars->time_start }}</div>
    		<div class="desc-webinar">{!! $webinars->description !!}</div>
            <?php
            if($href == true){
            ?>
                
            <? }else{ ?>
                <div>Для участия в вебинари, нужно преобрести доступ</div>
            <? } ?>
    	</div>
        <div class="col-md-5">
            <?
            if($webinars->image != null){
                $webinar_img = $webinars->image;
            }else{
                foreach ($posters as $key) {
                    $webinar_img = $key;
                }
            }
            ?>
            <div id="countdown">
                <span class="text-timer">До начала вебинара: </span><span id="day">00</span><span> : </span><span id="hour">00</span><span> : </span><span id="minute">00</span><span> : </span><span id="second">00</span>
            </div>
            <?php
            $countdown_setting = array("visible" => array("day" => array("block","дней:"), "hour" => array("block","часов:"), "minute" => array("block","минут:"), "second" => array("block","секунд:")));

            $script = '';
            $countdown_txt = '';
            $block_count = 0;
            /* Генерация html кода таймера */
            $block = 0;
            foreach($countdown_setting['visible'] AS $i => $v) {
                $countdown_txt .= '<div id="'.$i.'" style="display:'.$v[0].';">'.$v[1].' <span>00</span></div>';
                $script .= '<script type="text/javascript">var countdown_'.$i.' = "'.$v[0].'";</script>';
                if($v[0] == 'block') $block++;
            }
            $strTime = strtotime($webinars->date_webinar . ' ' . $webinars->time_start);

            $times = $strTime - time();

            $script .= '<script type="text/javascript">var timeleft='. $times .';</script>';
            echo $script;
            ?>
            <img src="http://<?=$_SERVER['SERVER_NAME'];?>/{{ $webinar_img }}">
            <div class="count-users-webinar dop-class-count"><i class="fa fa-users" aria-hidden="true"></i>{{ $count }}</div>
            @if($href == false)
                <div class="price" style="display: inline-block; margin-right: 25px;">Стоимость: {{ $webinars->price }} КЭЛ</div><a href="#" class="link-modal-wind link-reg-cont">Учавствовать</a>
            @endif
            @if($href == true)
                <div class="price" style="display: inline-block; margin-right: 25px;">Стоимость: {{ $webinars->price }} КЭЛ</div><a href="{{ route('MywebinarShow', ['id' => $webinars->id]) }}" class="link-reg-cont">Участвовать</a>
            @endif
            
        </div>
        <div class="col-md-12">
            <div class="full-width-button">
                <p class="author-p"><b>Автор:</b></p>
            <img src="http://<?=$_SERVER['SERVER_NAME'];?>/{{ $webinars->avatar_link }}" class="img-author-web">
            <div class="name-user-prev">{{ $webinars->name }}</div>
            </div>
            @foreach ($lektor_webinar as $val)
            @foreach ($posters as $poster)
            @if($val->arhiv == 0)
                <?  if($val->id_user == $webinars->id_user){ 
                    if($val->image == null){
                        $webinar_img = $poster;
                    }else{
                        $webinar_img = $val->image;
                    }
                ?>

                    <div class="fl-wd">
                        <a href="{{ route('webinarShow', ['id' => $val->id]) }}"><img src="https://{{ $_SERVER['SERVER_NAME'] }}/{{ $webinar_img }}" class="img-min-pr">
                        <div>{{ $val->name_webinar }}</div></a>
                    </div>
                <? } ?>
            @endif
            @endforeach    
            @endforeach
        </div>
    </div>
    <div id="modal_form_w">
        <span id="modal_close"><img src="../../../file/crest.png"></span>
        <div class="auth activite" id="form_2">
            <?php
            if($webinars->price > Auth::user()->balance){
                $user_pay = '<a href="https://webinar.dev-expo.top/admin/mybalance">Управления финансами</a>';
                $but = false;
            }else{
                $user_pay = 'Ждём Вас на вебинаре ' . $webinars->date_webinar . ' ' . $webinars->time_start . ' который Вы можете найти в разделе "Вебинары", подраздел "Афиша вебинаров", подраздел <a href="../catalog_webinars">"Предстоящие"</a>';
                $but = true;
            }
            ?>
            <div>{!! $pay !!}</div>
            @if($succes_pay == false)
            <div>{!! $user_pay !!}</div>
            @endif
            <form method="POST" action="{{ route('AddUserWebinar') }}" class="webinar-content-tr-fl">
                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                <input type="hidden" name="webinar_id" value="{{ $webinars->id }}">
                <input type="hidden" name="price" value="{{ $webinars->price }}">
                @if($but == true)
                <?=$button;?>
                @endif
                {{ csrf_field() }}
            </form>
        </div>
    </div>
    <div id="overlay"></div>
    <script type="text/javascript">
    function countdown_go() {
        timeleft_func = timeleft;
        if(countdown_day=='block') {
            timevalue = Math.floor(timeleft_func/(24*60*60));
            timeleft_func -= timevalue*24*60*60;
            if(timevalue<10) timevalue = '0'+timevalue;
            $("#day").html(timevalue);
        }
        if(countdown_hour=='block') {
            timevalue = Math.floor(timeleft_func/(60*60));
            timeleft_func -= timevalue*60*60;
            if(timevalue<10) timevalue = '0'+timevalue;
            $("#hour").html(timevalue);
        }
        if(countdown_minute=='block') {
            timevalue = Math.floor(timeleft_func/(60));
            timeleft_func -= timevalue*60;
            if(timevalue<10) timevalue = '0'+timevalue;
            $("#minute").html(timevalue);
        }
        if(countdown_second=='block') {
            timevalue = Math.floor(timeleft_func/1);
            timeleft_func -= timevalue*1;
            if(timevalue<10) timevalue = '0'+timevalue;
            $("#second").html(timevalue);
        }
        timeleft-=1;
        return false;
    }

    $(document).ready(function() {
        setInterval(countdown_go,1000);
        $("#countdown").css('width',(block_count*98)+'px');
        return false;
    })
    </script>
    @endforeach
</div>  
@endsection


