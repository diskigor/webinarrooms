@extends('layouts.admin')

@section('content')
    @if($page->banner_image)
        <section class="top-banner">
            <img src="{{asset($page->banner_image)}}" alt="banner">
        </section>
    @endif
    <div id="content">
        <div class="col-md-12">
            {!! $page->content !!}
            <div class="courses-slider static" id="courses-slider-big">
                <div class="row">
                    @if(isset($free_course))
                        @foreach($free_course as $cours)
                            <div class="col-md-4">
                                <a href="{{route('free_materials.show',$cours->course->slug)}}" target="_blank" class="owl-slide">
                                    <img class="image" src="{{asset($cours->course->image)}}" alt="">
                                    <div class="caption">
                                        <header>{{$cours->course->trans_curse[0]->name_trans}}</header>
                                        <div class="description hidden">{!! $cours->course->trans_curse[0]->short_desc_trans !!}</div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    @endif

                </div>
            </div>
        </div>
    </div>
@endsection
