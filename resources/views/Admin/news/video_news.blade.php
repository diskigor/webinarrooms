@extends('layouts.admin')

@section('content')
    <div class="col-md-12">
        <header class="big-header text-center">Видео от Гильбо</header>
        <div class="form-group">
            <a href="{{route('video_news.create')}}" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>
                Добавить видео от Гильбо</a>
        </div>
        <div class="row">
            @foreach($video_news as $video_new)
                <figure class="col-sm-4">
                    <div class="thumbnail post_news">
                        <figcaption>
                            <h3>{{mb_strimwidth($video_new->short_desc,0,60,'...')}}</h3>
                            <p><label>{{trans('news.created_news')}}:</label> {{$video_new->created_at}}
                                <span class="pull-right">{!! $video_new->status?'<i class="fa fa-eye" aria-hidden="true"></i>':'<i class="fa fa-eye-slash" aria-hidden="true"></i>' !!}</span>
                            </p>
                        </figcaption>
                        <div class="hover-nav">
                            <a href="#" class="btn btn-success" data-toggle="modal"
                               data-target="{{'#modal-'.$video_new->id}}" title="{{trans('news.view')}}"><i class="fa fa-eye"></i></a>
                            {!! Form::open(array('route'=>['video_news.edit',$video_new->id],'method'=>'GET','class'=>'inline-block')) !!}
                            {!! Form::button('<i class="fas fa-pencil-alt"></i>',['class'=>'btn btn-info','type'=>'submit']) !!}
                            {!! Form::close() !!}
                            {!! Form::open(array('route'=>['video_news.destroy',$video_new->id], 'method'=>'DELETE','class'=>'inline-block')) !!}
                            {!! Form::button(trans('<i class="fa fa-times"></i>'),['class'=>'btn btn-danger','type'=>'submit','onclick'=>'return checkDelete()']) !!}
                            {!! Form::close() !!}
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="{{'modal-'.$video_new->id}}" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">{{mb_strimwidth($video_new->short_desc,0,60,'...')}}</h4>
                                    </div>
                                    <div class="modal-body">
                                        <video controls controlsList="nodownload" width="500" height="300">
                                        <source src="{{ $video_new->video }}">
                                    </video>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </figure>
            @endforeach
        </div>
    </div>
@endsection