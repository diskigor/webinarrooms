@extends('layouts.admin')

@section('content')
    <div class="col-md-12">
        <header class="big-header text-center">{{trans('news.news')}}</header>
        <div class="form-group">
            <a href="{{route('news.create')}}" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>
                {{trans('news.add_news')}}</a>
        </div>
        <div class="row">
            @foreach($news as $article)
                        <figure class="col-sm-4">
                            <div class="thumbnail post_news">
                            <img class="image" src="{{ $article->image }}">
                            <figcaption>
                                <h3>{{ mb_strimwidth($article->lang?$article->lang->title:'',0,60,'...') }}</h3>
                                <p><label>{{trans('news.created_news')}}:</label> {{ $article->created_at }}</p>
                                <p>{!! mb_strimwidth($article->lang?$article->lang->article:'',0,100,'...') !!}</p>
                            </figcaption>
                            <div class="hover-nav">
                                <a href="#" class="btn btn-success" data-toggle="modal" data-target="#modal-news-{{ $article->id }}">
                                    <i class="fa fa-eye"></i>
                                    <span class="hidden">{{trans('news.view')}}</span>
                                </a>

                                {!! Form::open(array('route'=>['news.edit',$article->id],'method'=>'GET','class'=>'inline-block')) !!}
                                {!! Form::button('<i class="fas fa-pencil-alt"></i>',['class'=>'btn btn-info','type'=>'submit']) !!}
                                {!! Form::close() !!}

                                {!! Form::open(array('route'=>['news.destroy',$article->id], 'method'=>'DELETE','class'=>'inline-block')) !!}
                                {!! Form::button('<i class="fa fa-times"></i>',['class'=>'btn btn-danger','type'=>'submit','onclick'=>'return checkDelete()']) !!}
                                {!! Form::close() !!}
                            </div>

                                <!-- Modal -->
                                <div class="modal fade" id="modal-news-{{ $article->id }}" tabindex="-1" role="dialog" >
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title">{{ $article->lang?$article->lang->title:'' }}</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="image">
                                                    <img width="900" height="500" src="{{ $article->image }}"></i>
                                                </div>
                                                {!! $article->lang?$article->lang->article:'' !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </figure>
            @endforeach
        </div>
         {{$news->links()}}
    </div>
    <script language="JavaScript" type="text/javascript">
        function checkDelete() {
            return confirm('Вы уверены что хотите удалить данную новость?');
        }
    </script>
@endsection