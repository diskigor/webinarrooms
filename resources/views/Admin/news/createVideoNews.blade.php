@extends('layouts.admin')

@section('content')
    <div class="col-md-12">
        <header class="big-header text-center">Форма добаления видео от Гильбо</header>
        <div class="panel">
            {!! Form::open(array('route'=>'video_news.store', 'method'=>'POST','enctype'=>'multipart/form-data')) !!}


            
            <div class="form-group">
                {!! Form::label('short_desc','Название',['class'=>'control-label']) !!}
                {!! Form::text('short_desc',null,['class'=>'form-control','required','placeholder'=>'Введите название или краткое описание видео']) !!}
            </div>
            
            <div class="form-group">
                <label for="video" class="control-label">Загрузить видео на сервер</label>
                {!! Form::file('video',['accept'=>"image/jpeg,image/png,image/gif",'class'=>''])!!}
            </div>
            
            <div class="form-group">
                {!! Form::label('status','Отображать',['class'=>'control-label']) !!}
                {!! Form::checkbox('status', 1) !!}
            </div>
            
            <a href="{{ route('video_news.index') }}" class="btn btn-info">Назад</a>
            {!! Form::submit('Сохранить',array_merge(['class'=>'btn btn-success'])) !!}
            {!! Form::close() !!}
        </div>
        </div>
@endsection

@push('stack_scripts')
<script type="text/javascript">

</script>
@endpush