@extends('layouts.admin')

@section('content')
<div class="col-md-12">
    <header class="big-header text-center">Форма добаления видео новости от Гильбо</header>
    <div class="panel">
        {!! Form::open(array('route'=>['video_news.update',$edit_video_news->id], 'method'=>'PUT','enctype'=>'multipart/form-data')) !!}

        <div class="form-group">
            {!! Form::label('short_desc','Название',['class'=>'control-label']) !!}
            {!! Form::text('short_desc',$edit_video_news->short_desc,['class'=>'form-control','required','placeholder'=>'Введите название или краткое описание новости']) !!}
        </div>

        <div class="form-group">
            <video controls controlsList="nodownload" width="500" height="300">
                <source src="{{ $edit_video_news->video }}">
            </video>
        </div> 
        <div class="form-group">   
            <label for="video" class="control-label">Заменить видео на сервере</label>
            {!! Form::file('video',['accept'=>"image/jpeg,image/png,image/gif",'class'=>''])!!}
        </div>    
        
        <div class="form-group">
            {!! Form::label('status','Отображать',['class'=>'control-label']) !!}
            {!! Form::checkbox('status', 1,$edit_video_news->status) !!}
        </div>

        <div class="form-group">
            <a href="{{ route('video_news.index') }}" class="btn btn-info">Назад</a>
            {!! Form::submit('Обновить',array_merge(['class'=>'btn btn-success'])) !!}
            {!! Form::close() !!}
        </div>


    </div>
    @endsection