@extends('layouts.admin')

@section('content')
<div class="col-md-12">
    <header class="big-header text-center">Редактирование новости</header>
    <div class="panel">
        {!! Form::open(array('route'=>['news.update',$articles->id], 'method'=>'PUT','enctype'=>'multipart/form-data')) !!}
        <div class="form-group">
            <img class="pre" src="{{ $articles->image}}">
        </div>
        <div class="form-group">
            <div class="preview">
                <div id="preview1"></div>
                <input type="button" id="reset1" value="Сбросить" class="btn btn-info reset"/>
            </div>
        </div>
        <div class="form-group">
            <label for="image_link" class="control-label">{{trans('news.news_image')}} <small>(1140х400)</small></label>
            {!! Form::file('image')!!}
        </div>
        <div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                @foreach($articles->relationForEdit->reverse() as $article)
                    @if($loop->first)
                    <li role="presentation" class="active">
                        <a href="#{{$article->lang_local.'-first'}}"
                           aria-controls="{{$article->lang_local.'-first'}}"
                           role="tab"
                           data-toggle="tab">{{$article->lang_local}}</a>
                    </li>
                    @else
                    <li role="presentation">
                        <a href="#{{$article->lang_local}}"
                           aria-controls="{{$article->lang_local}}"
                           role="tab" data-toggle="tab">{{$article->lang_local}}</a>
                    </li>
                    @endif
                @endforeach
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                @foreach($articles->relationForEdit->reverse() as $article_cont)
                    @if($loop->first)
                        <div role="tabpanel" class="tab-pane active fade in" id="{{$article_cont->lang_local.'-first'}}">
                            <div class="form-group">
                                {!! Form::label('title_'.$article_cont->lang_local,trans('news.title_news'),['class'=>'control-label']) !!}
                                {!! Form::text('title_'.$article_cont->lang_local,$article_cont->title,array_merge([
                                'class'=>'form-control',
                                'placeholder'=>trans('news.placeholder_title'),
                                'required']))!!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('article_'.$article_cont->lang_local,trans('news.enter_article'),['class'=>'control-label']) !!}
                                {!! Form::textarea('article_'.$article_cont->lang_local,$article_cont->article,['class'=>'form-control']) !!}
                            </div>
                        </div>
                    @else
                        <div role="tabpanel" class="tab-pane fade" id="{{$article_cont->lang_local}}">
                            <div class="form-group">
                                {!! Form::label('title_'.$article_cont->lang_local,trans('news.title_news').'  '.$article_cont->lang_local,['class'=>'control-label']) !!}
                                {!! Form::text('title_'.$article_cont->lang_local,$article_cont->title,array_merge([
                                'class'=>'form-control',
                                'placeholder'=>trans('news.placeholder_title'),
                                'required']))!!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('article_'.$article_cont->lang_local,trans('news.enter_article').'  '.$article_cont->lang_local,['class'=>'control-label']) !!}
                                {!! Form::textarea('article_'.$article_cont->lang_local,$article_cont->article,['class'=>'form-control']) !!}
                            </div>

                        </div>
                    @endif
                @endforeach

                <div class="form-group">
                    {!! Form::submit(trans('news.save_button'),['class'=>'btn btn-success']) !!}
                    <a href="{{route('news.index')}}" class="btn btn-info">{{trans('content.back')}}</a>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
@endsection

@push('stack_scripts')
<script src="{{asset('js/imagepreview.js')}} "></script>
<script type="text/javascript">
$(function () {
    $('#preview1').imagepreview({
        input: '[name="ilink"]',
        reset: '#reset1',
        preview: '#preview1'
    });
});
</script>
@endpush