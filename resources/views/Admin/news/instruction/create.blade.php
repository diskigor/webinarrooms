@extends('layouts.admin')

@section('content')
<div class="col-md-12">
    <header class="big-header text-center">Форма добаления видеоинструкции</header>
    <div class="panel">
        {!! Form::open(array('route'=>'video_instruction.store', 'method'=>'POST','enctype'=>'multipart/form-data')) !!}



        <div class="form-group">
            {!! Form::label('name','Название',['class'=>'control-label']) !!}
            {!! Form::text('name',null,['class'=>'form-control','required','placeholder'=>'Введите название']) !!}
        </div>
        
        <div class="form-group">
            {!! Form::label('short','Описание видеоинструкции',array_merge(['class'=>'control-label'])) !!}
            {!! Form::text('short',null,array_merge(['class'=>'form-control','required','placeholder'=>'Описание видеоинструкции'])) !!}
        </div>
        
        <div class="form-group">
            {!! Form::label('status','Отображать',['class'=>'control-label']) !!}
            {!! Form::checkbox('status', 1) !!}
        </div>

        <div class="form-group">
            {!! Form::label('order','Сортировка',['class'=>'control-label']) !!}
            {!! Form::number('order',0,['class'=>'form-control','min'=>0, 'max'=>100]) !!}
        </div>

        <div class="form-group">
            <label for="video" class="control-label">Загрузить видео на сервер</label>
            {!! Form::file('video',['accept'=>"image/jpeg,image/png,image/gif",'class'=>''])!!}
        </div>



        <a href="{{ route('video_instruction.index') }}" class="btn btn-info">Назад</a>
        {!! Form::submit('Сохранить',array_merge(['class'=>'btn btn-success'])) !!}
        {!! Form::close() !!}
    </div>
</div>
@endsection

@push('stack_scripts')
<script type="text/javascript">

</script>
@endpush