@extends('layouts.admin')

@section('content')
<div class="col-md-12">
    <header class="big-header text-center">Форма редактирования видеоинструкции</header>
    <div class="panel">
        {!! Form::open(array('route'=>['video_instruction.update',$instruction->id], 'method'=>'PUT','enctype'=>'multipart/form-data')) !!}

        <div class="form-group">
            {!! Form::label('name','Название',['class'=>'control-label']) !!}
            {!! Form::text('name',$instruction->name,['class'=>'form-control','required','placeholder'=>'Введите название или краткое описание новости']) !!}
        </div>
        
        <div class="form-group">
            {!! Form::label('short','Описание видеоинструкции',array_merge(['class'=>'control-label'])) !!}
            {!! Form::text('short',$instruction->short,array_merge(['class'=>'form-control','required','placeholder'=>'Описание видеоинструкции'])) !!}
        </div>
        
        <div class="form-group">
            <video controls controlsList="nodownload" width="500" height="300">
                <source src="{{ $instruction->video }}">
            </video>
        </div> 
        <div class="form-group">   
            <label for="video" class="control-label">Заменить видео на сервере</label>
            {!! Form::file('video',['accept'=>"image/jpeg,image/png,image/gif",'class'=>''])!!}
        </div>    
        
        <div class="form-group">
            {!! Form::label('status','Отображать',['class'=>'control-label']) !!}
            {!! Form::checkbox('status', 1,$instruction->status) !!}
        </div>
        
        <div class="form-group">
            {!! Form::label('order','Сортировка',['class'=>'control-label']) !!}
            {!! Form::number('order',$instruction->order,['class'=>'form-control','min'=>0, 'max'=>100]) !!}
        </div>
        
        <div class="form-group">
            <a href="{{ route('video_instruction.index') }}" class="btn btn-info">Назад</a>
            {!! Form::submit('Обновить',array_merge(['class'=>'btn btn-success'])) !!}
            {!! Form::close() !!}
        </div>


    </div>
    @endsection