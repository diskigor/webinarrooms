@extends('layouts.admin')

@section('content')
    <div class="col-md-12">
        <header class="big-header text-center">Видеоинструкции</header>
        <div class="form-group">
            <a href="{{route('video_instruction.create')}}" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>
                Добавить видеоинструкцию</a>
        </div>
        <div class="row">
            @foreach($instructions as $video)
                <figure class="col-sm-4">
                    <div class="thumbnail post_news">
                        <figcaption>
                            <h3>{{ mb_strimwidth($video->name,0,60,'...') }}</h3>
                            <p><label>{{trans('news.created_news')}}:</label> {{$video->created_at}}
                                <span class="btn-success pull-right">{{ $video->order }}</span>
                                <span class="pull-right">{!! $video->status?'<i class="fa fa-eye" aria-hidden="true"></i>':'<i class="fa fa-eye-slash" aria-hidden="true"></i>' !!}</span>
                            </p>
                            <p>{{ mb_strimwidth($video->short,0,60,'...') }}</p>
                        </figcaption>
                        <div class="hover-nav">
                            <a href="#" class="btn btn-success" data-toggle="modal"
                               data-target="{{'#modal-'.$video->id}}" title="{{trans('news.view')}}"><i class="fa fa-eye"></i></a>
                            {!! Form::open(array('route'=>['video_instruction.edit',$video->id],'method'=>'GET','class'=>'inline-block')) !!}
                            {!! Form::button('<i class="fas fa-pencil-alt"></i>',['class'=>'btn btn-info','type'=>'submit']) !!}
                            {!! Form::close() !!}
                            {!! Form::open(array('route'=>['video_instruction.destroy',$video->id], 'method'=>'DELETE','class'=>'inline-block')) !!}
                            {!! Form::button(trans('<i class="fa fa-times"></i>'),['class'=>'btn btn-danger','type'=>'submit','onclick'=>'return checkDelete()']) !!}
                            {!! Form::close() !!}
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="{{'modal-'.$video->id}}" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">{{mb_strimwidth($video->name,0,60,'...')}}</h4>
                                    </div>
                                    <div class="modal-body">
                                        <video controls controlsList="nodownload" width="500" height="300">
                                        <source src="{{ $video->video }}">
                                    </video>
                                    </div>
                                    <div class="modal-footer">
                                        <p>{{ $video->short }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </figure>
            @endforeach
        </div>
        <div class="col-md-12">{{ $instructions->links() }}</div>
    </div>
@endsection