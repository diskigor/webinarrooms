@extends('layouts.admin')

@section('content')
<div class="col-md-12">
    <header class="big-header text-center">{{trans('news.form_created_news')}}</header>
    <div class="panel">
        {!! Form::open(array('route'=>'news.store', 'method'=>'POST','enctype'=>'multipart/form-data')) !!}
        <div class="form-group">
            <div class="preview">
                <div id="preview1"></div>
                <input type="button" id="reset1" value="Сбросить" class="btn btn-info reset"/>
            </div>
        </div>
        <div class="form-group">
            <label for="image_link" class="control-label">{{trans('news.news_image')}} <small>(1140х400)</small></label>
            {!! Form::file('image',['accept'=>"image/jpeg,image/png,image/gif",'class'=>''])!!}
        </div>
        <div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <?php $i = 0; ?>
                @foreach($arr_local as $lang)
                <?php $i++; ?>
                @if($i===1)
                <li role="presentation" class="active">
                    <a href="#{{$lang.'-first'}}" aria-controls="{{$lang.'-first'}}" role="tab"
                       data-toggle="tab">{{$lang}}</a>
                </li>
                @else
                <li role="presentation">
                    <a href="#{{$lang}}" aria-controls="{{$lang}}" role="tab" data-toggle="tab">{{$lang}}</a>
                </li>
                @endif
                @endforeach
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                @foreach($arr_local as $key=>$lang_content)
                    @if($loop->first)
                        <div role="tabpanel" class="tab-pane active fade in" id="{{$lang_content.'-first'}}">
                            <div class="form-group">
                                {!! Form::label('title_'.$key,trans('news.title_news'),['class'=>'control-label']) !!}
                                {!! Form::text('title_'.$key,null,array_merge([
                                'class'=>'form-control',
                                'placeholder'=>trans('news.placeholder_title'),
                                'required']))!!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('article_'.$key,trans('news.enter_article'),['class'=>'control-label']) !!}
                                {!! Form::textarea('article_'.$key,null,['class'=>'form-control']) !!}
                            </div>
                        </div>
                    @else
                        <div role="tabpanel" class="tab-pane fade" id="{{$lang_content}}">
                            <div class="form-group">
                                {!! Form::label('title_'.$key,trans('news.title_news').'  '.$lang_content,['class'=>'control-label']) !!}
                                {!! Form::text('title_'.$key,null,array_merge([
                                'class'=>'form-control',
                                'placeholder'=>trans('news.placeholder_title'),
                                'required']))!!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('article_'.$key,trans('news.enter_article').'  '.$lang_content,['class'=>'control-label']) !!}
                                {!! Form::textarea('article_'.$key,null,['class'=>'form-control']) !!}
                            </div>
                        </div>
                    @endif
                @endforeach

                <div class="form-group">
                    {!! Form::submit(trans('news.save_button'),['class'=>'btn btn-success']) !!}
                    <a href="{{route('news.index')}}" class="btn btn-info">{{trans('content.back')}}</a>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
@endsection

@push('stack_scripts')
<script src="{{asset('js/imagepreview.js')}} "></script>
<script type="text/javascript">
$(function () {
    $('#preview1').imagepreview({
        input: '[name="ilink"]',
        reset: '#reset1',
        preview: '#preview1'
    });
});
</script>
@endpush