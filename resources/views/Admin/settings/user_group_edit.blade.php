@extends('layouts.admin')
@section('content')
    <div class="col-md-12">
        <div class="body">
            <header class="big-header text-center">Группa SDO {{ $group->name }}</header>
            <div class="panel">
                {!! Form::open(array('route'=>['group_user.update',$group->id],'method'=>'PUT')) !!}
                <div class="form-group">
                    {!! Form::label('name','Название',['class'=>'control-label']) !!}
                    {!! Form::text('name',$group->name,['class'=>'form-control','required','maxlength'=>'190']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('description','Описание',['class'=>'control-label']) !!}
                    {!! Form::text('description',$group->description,['class'=>'form-control','required','maxlength'=>'190']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('rule_id','Применяемое правило',['class'=>'control-label']) !!}
                    {!! Form::select('rule_id',$rules,$group->RuleIds,['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    <a href="{{ route('group_user.index') }}" class="btn btn-warning" data-dismiss="modal">Назад к списку групп</a>
                    {!! Form::submit('Сохранить',['class'=>'btn btn-success']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection