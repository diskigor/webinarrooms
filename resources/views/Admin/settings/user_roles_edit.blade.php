@extends('layouts.admin')
@section('content')
    <div class="col-md-12">
        <div class="body">
            <header class="big-header text-center">Роль SDO {{ $role->name }}</header>
            <div class="panel">
                {!! Form::open(array('route'=>['roles_user.update',$role->id],'method'=>'PUT')) !!}
                <div class="form-group">
                    {!! Form::label('name','Название',['class'=>'control-label']) !!}
                    <p>{{ $role->name }}</p>
                </div>
                <div class="form-group">
                    {!! Form::label('description','Описание',['class'=>'control-label']) !!}
                    {!! Form::text('description',$role->description,['class'=>'form-control','required','maxlength'=>'190']) !!}
                </div>
                <div class="form-group">
                    <a href="{{ route('roles_user.index') }}" class="btn btn-warning" data-dismiss="modal">Назад к списку Ролей</a>
                    {!! Form::submit('Сохранить',['class'=>'btn btn-success']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection