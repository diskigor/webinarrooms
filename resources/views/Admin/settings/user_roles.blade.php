@extends('layouts.admin')
@section('content')
    <div class="col-md-12">
        <div class="body">
            <header class="big-header text-center">Роли системы SDO</header>
            <div class="panel">
                <div class="panel">
                    <table class="table table-responsive">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Название</th>
                            <th>Описание</th>
                            <th>Пользователей</th>
                            <th>{{trans('content.actions')}}</th>
                        </tr>
                        </thead>
                        <tbody>

                            @foreach($roles as $role)
                                <tr>
                                    <td>{{ $role->id }}</td>
                                    <td>{{ $role->name }}</td>
                                    <td>{{ $role->description }}</td>
                                    <td>{{ $role->CountUsers }}</td>
                                    <td>
                                        <a href="{{ route('roles_user.edit',$role->id)}}" 
                                           class="btn btn-success"
                                           title="Редактировать Роль"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
@endsection