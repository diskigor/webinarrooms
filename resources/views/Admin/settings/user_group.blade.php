@extends('layouts.admin')
@section('content')
    <div class="col-md-12">
        <div class="body">
            <header class="big-header text-center">Группы SDO</header>
            <div class="panel">
                <!-- Начало модального окна -->
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalAddAdvantages">
                    <i class="fa fa-plus"></i> Добавить группу
                </button>

                <div class="modal fade" id="modalAddAdvantages" tabindex="-1" role="dialog"
                     aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Форма добавления группы</h4>
                            </div>
                            <div class="modal-body">
                                {!! Form::open(array('route'=>'group_user.store','method'=>'POST')) !!}
                                <div class="form-group">
                                    {!! Form::label('name','Название',['class'=>'control-label']) !!}
                                    {!! Form::text('name',null,['class'=>'form-control','required','maxlength'=>'190']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('description','Описание',['class'=>'control-label']) !!}
                                    {!! Form::text('description',null,['class'=>'form-control','required','maxlength'=>'190']) !!}
                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn-warning" data-dismiss="modal">Закрыть</button>
                                    {!! Form::submit('Сохранить',['class'=>'btn btn-success']) !!}
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel">
                    <table class="table table-responsive">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>Название</th>
                            <th>Описание</th>
                            <th>Правило</th>
                            <th>Пользователей</th>
                            <th>{{trans('content.actions')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($groups->count())
                            @foreach($groups as $group)
                                <tr>
                                    <td>{{ $group->id }}</td>
                                    <td>{{ $group->name }}</td>
                                    <td>{{ $group->description }}</td>
                                    <td>{{ $rules->getName($group->RuleIds) }}</td>
                                    <td>{{ $group->CountUsers }}</td>
                                    <td>
                                        <a href="{{ route('group_user.edit',$group->id)}}" 
                                           class="btn btn-success"
                                           title="Редактировать группу"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                        {!! Form::open(array('route'=>['group_user.destroy',$group->id], 'method'=>'DELETE','class'=>'inline-block')) !!}
                                        {!! Form::button('Удалить',['class'=>'btn btn-danger','type'=>'submit']) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            
                        @else
                            <tr>
                                <td>На данный момент нет групп пользователей</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
@endsection