<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="shortcut icon" href="{{asset('images/mane/favicon.png')}}" type="image/png"/>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{--<title>{{ config('app.name', 'Laravel') }}</title>--}}
    <title>Школа эффективных лидеров</title>
    <!-- Google Fonts -->
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <!-- Search -->
    <!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css"/-->
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}"/>
    <!-- -->
    <script src="{{asset('js/jquery-3.1.1.min.js')}} "></script>
    <link rel="stylesheet" href="{{asset('css/jquery.formstyler.css')}}">
    <link rel="stylesheet" href="{{asset('css/jquery.formstyler.theme.css')}}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style-s.css') }}" rel="stylesheet">
    <!-- Scripts -->

    <script src="{{asset('js/sweetalert.min.js')}}"></script>
    <!-- Sweet Alert -->
    <link href="{{asset('css/sweetalert.css')}}" rel="stylesheet">
    <!------------------>
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
<div id="app" class="wrap">
    @include('layouts.partials.nav')
    @if(Auth::check())
        @if(Auth::user()->isAdmin())
            @include('Admin.admin.leftSiteBarAdmin')
        @elseif(Auth::user()->isCadet())
            @include('Admin.cadet.leftSiteBarCadet')
        @elseif(Auth::user()->isСurator())
            @include('Admin.curator.leftSiteBarCurator')
        @elseif(Auth::user()->isLecturer())
            @include('Admin.lecturer.leftSiteBarLecturer')
        @endif
    @endif

    <div class="wrap-cabinet-content">
        @yield('content')
    </div>
</div>
<!-- OPEN FOOTER  -->
@include('layouts.partials.footer')
<!--END FOOTER-->

@include('sweet::alert')
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{URL::to('src/js/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script src="{{asset('js/jquery.formstyler.min.js')}}"></script>
<!--script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script-->
<script src="{{asset('js/jquery-ui.min.js')}}"></script>
<script>
    var editor_config = {
        path_absolute: "{{URL::to('/')}}/",
        language: "ru",
        theme : "advanced",
        selector: "textarea",
        selector : "textarea:not(.mceNoEditor)",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern","code"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
        relative_urls: false,
        file_browser_callback: function (field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight;

            var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
            if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }
            tinyMCE.activeEditor.windowManager.open({
                file: cmsURL,
                title: 'Filemanager',
                width: x * 0.8,
                height: y * 0.8,
                resizable: 'yes',
                close_previous: 'no'
            });
        }
    };
    tinymce.init(editor_config);
    $('input[type="checkbox"],input[type="radio"],input[type="file"]').styler();
</script>
@yield('script')

@stack('stack_scripts')

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KN57QVJ');</script>
<!-- End Google Tag Manager -->

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KN57QVJ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
</body>
</html>
