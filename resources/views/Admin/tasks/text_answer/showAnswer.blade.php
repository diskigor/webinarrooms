@extends('layouts.admin')
@section('content')
    <div class="col-md-12">
        <div class="row">
            <h2 class="text-center">Текстовый ответ на задание пользователя</h2>
        </div>
        <div class="panel">
            <h3 class="text-center">Пользователь :</h3>
            <img class="user-photo" src="{{ $text_answer->student->avatar }}">
            <p> {{$text_answer->student->name}}</p>

            <h3>Степень посвящения:</h3>
            <p>@if($text_answer->student->role->name==='Cadet')
                    Курсант
                @elseif($text_answer->student->role->name==='Сurator')
                    Эксперт
                @endif
            </p>
        </div>
        <div class="panel">
            <h3 class="text-center">{{ $text_answer->text_task->prelection->course->trans_curse->first()->name_trans }}</h3>
            <h4 class="text-center">Лекция: {{ $text_answer->text_task->prelection->sdoLectureTrans()->name_trans }}</h4>
            <h4 class="text-center">{{$text_answer->text_task->textTasksTrans()->name_trans}}</h4>
            <p>{!! $text_answer->text_task->textTasksTrans()->content_trans !!}</p>
        </div>
        <div class="panel">
            <h3 class="text-center">Ответ пользователя :</h3>
            <article>
                {!! $text_answer->answer !!}
            </article>
        </div>
        <div class="panel">
            <h3 class="text-center"> Комментарии :</h3>
            <div>
                @if($text_answer->comments->count() != 0)
                    @foreach($text_answer->comments as $comment)
                        <div class="panel">
                            <span>Эксперт: {{$comment->author_comment->name}}</span>
                            {!! $comment->comment !!}
                            @if(isset($comment->like_com->text_answer_comment_id) && $comment->like_com->text_answer_comment_id === $comment->id)
                                <i class="fa fa-trophy" aria-hidden="true"></i>
                            @endif
                        </div>
                    @endforeach
                @else
                    <p>Оставьте первый комментарий и получите бонус.</p>
                @endif
            </div>
            @if($text_answer->coutn_comment != 10)
                <div>
                    {!! Form::open(array('route'=>['add_comment_text_answer',$text_answer->id],'method'=>'POST')) !!}
                    <div class="form-group">
                        {!! Form::label('comment','Оставить комментарий. Комментарий должен содержать не менее 100 символов',['class'=>'control-label']) !!}
                        {!! Form::textarea('comment',null,['class'=>'form-control']) !!}
                    </div>
                    <button type="submit" class="btn btn-blue">Оставить комментарий</button>
                    {!! Form::close() !!}
                </div>
            @endif
        </div>
    </div>
@endsection