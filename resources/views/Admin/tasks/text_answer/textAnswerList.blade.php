@extends('layouts.admin')
@section('content')
<?php $title_page='Таблица ответов пользоватлей на текстовые задания'; ?>
    <div class="col-md-12">
        <!--p class="podskazki">Этот раздел содержит Ваши развернуты ответы, отчеты и сочинения, которые Вы будете писать в процессе освоения материала. 
            Здесь Вы сможете прочитать пять первых комментариев (мнений) более опытных пользователей, которые достигли статуса экспертов, касающиеся предмета обсуждения, 
            и выбрать лучший из них, взяв на вооружение полезную информацию для дальнейшего применения.
        </p-->
        <div class="row">
        <div class="col-md-12">
            <table class="table table-responsive">
                <thead>
                <tr>
                    <th>№</th>
                    <th>Пользователь</th>
                    <th>Тест</th>
                    <!--th>Роль</th>
                    <th>Уровен</th-->
                    
                    
                    <th>Курс</th>
                    <!--th>Лекция</th-->
                    
                    
                    <th>Комментарии</th>
                    <!--th>Дата</th-->
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>
                @foreach($text_answers as $text_answer)
                    <tr>
                        <td>{{ $loop->iteration + ($text_answers->currentPage()-1)*$text_answers->perPage() }}</td>
                        <td>{{$text_answer->student->name}}</td>
                        <td>{{ $text_answer->text_task->textTasksTrans()->name_trans}}</td>
                        <!--td>{{ $text_answer->student->role?$text_answer->student->role->description:'' }}</td>
                        <td class="text-center">{{$text_answer->student->level_point}}</td-->

                        <td>{{ $text_answer->text_task->prelection->course->block?$text_answer->text_task->prelection->course->block->Name:'' }}
                            ->
                            {{ $text_answer->text_task->prelection->course->LangName }}
                             ->
                            {{ $text_answer->text_task->prelection->Name }}
                            : 
                            {{ $text_answer->text_task->Name }}
                        
                        </td>
                        <!--td>{{ $text_answer->text_task->prelection->sdoLectureTrans()->name_trans }}</td-->
                        
                        
                        <td class="text-center">{{ $text_answer->comments_count }}</td>
                        
                        <!--td>{{$text_answer->created_at}}</td-->
                        <td>
                            @if(Auth::user()->favoriteUser($text_answer->student->id))
                                <a href="{{route('delete_favorite',$text_answer->student->id)}}" class="btn btn-warning">Убрать</a>
                            @else
                                <a href="{{route('add_favorit',$text_answer->student->id)}}" class="btn btn-info">Избранное</a>
                            @endif
                            <a href="{{route('checking_text_tasks.edit',$text_answer->id)}}" class="btn btn-success">Подробнее</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="col-md-12 text-center">
            {{ $text_answers->links() }}
        </div>
        </div>
        </div>
    </div>
@endsection