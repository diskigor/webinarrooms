@extends('layouts.admin')
@section('content')
    <div class="col-md-12">
        <header class="big-header text-center">Таблица избранных пользоватлей.</header>
        <div class="panel">
            <table class="table table-responsive">
                <thead>
                <tr>
                    <th>№</th>
                    <th>Пользователь</th>
                    <th>Роль</th>
                    <th>Уровень пользователя</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>
                <?php $i=1;?>
                @foreach($favorites as $favorite)
                <tr>
                    <td>{{$i++}}</td>
                    <td>{{$favorite->favoritUser->name}}</td>
                    <td>{{$favorite->favoritUser->role->name}}</td>
                    <td>{{$favorite->favoritUser->level_point}}</td>
                    <td>
                        <a href="{{route('delete_favorite',$favorite->favoritUser->id)}}" class="btn btn-warning">Удалить с избраных</a>
                        <a href="{{route('show_favorite_work',$favorite->favoritUser->id)}}" class="btn btn-success">Работы</a>
                    </td>
                </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection