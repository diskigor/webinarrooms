@extends('layouts.admin')
@section('content')
<link rel="stylesheet" href="{{ asset('css/datatables.css') }}">
    <div class="col-md-12">
        <h1 class="text-center">Список тестов</h1>
        <button class="btn btn-success" type="button" data-toggle="collapse" data-target="#add-form-test"
                aria-expanded="false" aria-controls="add-form-test">
            <i class="fa fa-plus"></i> Добавить тест
        </button>
        <div class="collapse" id="add-form-test">
            <h3>Форма добавления тестов</h3>
            <div>
                <select name="course_id" id="course" required>
                    <option selected="selected">Сортировка по курсу</option>
                    @foreach($course_chearsh as $key=>$value)
                        <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                </select>
            </div>
            {!! Form::open(array('route'=>'test.store','method'=>'POST')) !!}
            <div class="form-group">
                <div>
                    {!! Form::label('prelection_id','Выберите лекцию',['class'=>'control-label']) !!}
                    <select name="prelection_id" id="lectures">

                    </select>
                </div>
            </div>
            <!-- Выборе вид теста -->
            <div class="form-group">
                {!! Form::label('type','Выберите тип теста',['class'=>'control-label']) !!}
                {!! Form::select('type',$type_arr,['class'=>'form-control']) !!}
            </div>
            <!-- -->
            <div class="form-group">
                {!! Form::label('lang_local','Выберите язык',['class'=>'control-label']) !!}
                {!! Form::select('lang_local',$arr_local,['required']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('point','Кол-во баллов за прохождение:',['class'=>'control-label']) !!}
                {!! Form::number('point',null,['class'=>'form-control','min'=>'0','required']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('name','Название теста',['class'=>'control-label']) !!}
                {!! Form::text('name',null,array_merge([
                'class'=>'form-control',
                'placeholder'=>'Введите название теста',
                'required']))!!}
            </div>
            <div class="form-group">
                {!! Form::label('desc','Описание теста',['class'=>'control-label']) !!}
                {!! Form::textarea('desc',null,array_merge(['class'=>'form-control'])) !!}
            </div>
            {!! Form::submit('Сохранить тест',array_merge(['class'=>'btn btn-success'])) !!}
            {!! Form::close() !!}
        </div>
    </div>
    <hr>
    <div class="col-md-12">
        <div class="panel">
            <table id='listtestformy' class="table table-responsive">
                <thead>
                <tr>
                    <th>№</th>
                    <th>Курс</th>
                    <th>Лекция</th>
                    <th style="width: 50px;">Тип</th>
                    <th>Название</th>
                    <th>Описание</th>
                    <th>Добавил</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>
                @foreach($tests as $test)
                    <tr>
                        <th>{{$test->id}}</th>
                        <td>{{$test->prelection->course->trans_curse[0]->name_trans}}</td>
                        <td>{{$test->prelection->sdoLectureTrans()->name_trans}}</td>
                        <td style="color: #2a88bd">{{$test->typeTest->name}}</td>
                        <td>{{$test->name}}</td>
                        <td>{{  mb_strimwidth(strip_tags ($test->description),0,150,'...') }}</td>
                        <td>{{$test->author->name}}</td>
                        <td>
                            @if($test->type_id != 3)
                            <a href="{{route('test.edit',$test->id)}}" 
                               class="btn btn-success" title="Редактировать"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            @else
                            <a href="{{route('fill_test',$test->id)}}" 
                               class="btn btn-success"><i class="fa fa-check-square-o"></i> Заполнить тест</a>
                            @endif

                            <a href="{{route('explanation_list',$test->id)}}"
                               class="btn btn-warning"
                               title="Результаты теста"><i class="fa fa-object-group" aria-hidden="true"></i></a>
                            
                            
                            {!! Form::open(array('route'=>['test.destroy',$test->id], 'method'=>'DELETE','class'=>'center-block')) !!}
                            {!! Form::button('<i class="fa fa-times"></i>удалить',['class'=>'btn btn-danger','title'=>'Удалить','type'=>'submit','onclick'=>'return checkDelete()']) !!}
                            {!! Form::close() !!}
                            
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
@section('script')

    <script src="{{asset('js/sort-course.js')}}"></script>
    <script language="JavaScript" type="text/javascript">
        function checkDelete() {
            return confirm('Вы уверены что хотите удалить данный тест?');
        }
    </script>
<script src="{{ asset('js/datatables/datatables.js') }}"></script>
<script language="JavaScript" type="text/javascript">
    function checkDelete() {
        return confirm('Вы уверены что хотите удалить данное текстовое задание?');
    }
   $(document).ready(function() {
    $('#listtestformy').DataTable();
} );
</script>

@endsection