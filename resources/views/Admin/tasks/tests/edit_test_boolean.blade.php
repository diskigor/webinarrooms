@extends('layouts.admin')
@section('content')

        <h1 class="text-center">Редактирование Тест "{{$test->name}}"</h1>
        <p>Тип теста: {{$test->typeTest->name}}</p>
        <p>КУРС: <b>{{ $test->prelection->course->lang->name_trans }}</b></p>
        <p>ЛЕКЦИЯ:<b>{{ $test->prelection->sdoLectureTrans()->name_trans }}</b></p>
        <p>{{$test->typeTest->description}}</p>
        

    <hr>
    <div class="col-md-12">
            {!! Form::open(array('route'=>['test.update',$test->id], 'method'=>'PUT')) !!}
            
            <div class="row">
                <div class="col-md-12">
                    {!! Form::label('prelection_id','Сменить привязку к лекции ?') !!}
                    <select name="prelection_id">
                        <option value="0" selected="selected">Не менять привязку к лекции </option>
                        @foreach($lectures as $key=>$value)
                            <option value="{{$key}}">{{$value}}</option>
                        @endforeach
                    </select>
                </div>
                
            </div>
            <hr>
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        {!! Form::label('name','Название теста',['class'=>'control-label']) !!}
                        {!! Form::text('name',$test->name,array_merge(['class'=>'form-control','placeholder'=>'Введите название теста'])) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('point','Баллы теста',['class'=>'control-label']) !!}
                        {!! Form::text('point',$test->point,array_merge(['class'=>'form-control','placeholder'=>'Введите количество баллов за прохождение'])) !!}
                    </div>
                </div>
            </div>


                <div class="form-group">
                    {!! Form::label('description','Описание',['class'=>'control-label']) !!}
                    {!! Form::textarea('description',$test->description,array_merge([
                    'class'=>'form-control',
                    'placeholder'=>'Введите описание теста']))!!}
                </div>
                <!--div class="form-group">
                    <div id="answers"></div>
                </div-->
                <div class="form-group center-block">
                    {!! Form::submit('Обновить описание',array_merge(['class'=>'btn btn-success'])) !!}
                </div>
            {!! Form::close() !!}
        
    </div>
    <hr>
    <div class="container">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>№</th>
                <th>Вопрос</th>
                <th>Ответ</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>

            @foreach($test->testQuestions as $test_question)
                <tr>
                    <td>{{$test_question->number}}</td>
                    <td>{!!$test_question->question !!}</td>
                    <td>@foreach($test_question->testAnswerQuestion as $answers)
                            <p>{{ $answers->answer }}
                                @if($answers->is_right)
                                    <i class="fa fa-check-circle" style="color: #2ab27b" aria-hidden="true"></i> ({{$answers->is_right}})
                                @endif
                            </p>
                        @endforeach
                    </td>
                    <td>
                        <button class="btn btn-primary center-block" type="button" data-toggle="collapse" data-target="#edit_form_qestion_{{$test_question->id}}"
                                aria-expanded="false" title="edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                        {!! Form::open(array('route'=>['delete_question',$test_question->id],'method'=>'GET','class'=>'inline-block')) !!}
                        {!! Form::button('<i class="fa fa-times"></i>',['class'=>'btn btn-danger','type'=>'submit','onclick'=>'return checkDelete()']) !!}
                        {!! Form::close() !!}
                        
                    </td>
                </tr>
                <tr class="collapse" id="edit_form_qestion_{{$test_question->id}}">
                    <td>edit</td>
                    <td colspan="10">
                        {!! Form::open(array('route'=>'update_question_answer','method'=>'POST')) !!}
                            <input type="hidden" name="test_id" value="{{$test->id}}">
                            <input type="hidden" name="question_id" value="{{$test_question->id}}">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        {!! Form::label('number','Номер вопроса',['class'=>'control-label']) !!}
                                        {!! Form::text('number',$test_question->number,array_merge(['class'=>'form-control','placeholder'=>'Сортировка для вывода, 0 - по умолчанию'])) !!}
                                    </div>
                                </div>
                                {{--
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('ordered','Сортировка',['class'=>'control-label']) !!}
                                        {!! Form::text('ordered',$test_question->ordered,array_merge(['placeholder'=>'Сортировка для вывода, 0 - по умолчанию'])) !!}
                                    </div>
                                </div>
                                --}}
                                <div class="col-md-7">
                                    <div class="form-group"> 
                                        {!! Form::radio('button_type', 'radio', $test_question->button_type == 'radio'?:false ) !!}
                                        {!! Form::label('button_type', 'Один вариант (Radio)') !!}

                                    </div>
                                    <div class="form-group">                         
                                        {!! Form::radio('button_type', 'checkbox', $test_question->button_type == 'checkbox'?:false ) !!}
                                        {!! Form::label('button_type', 'Несколько вариантов (Checkbox)') !!}
                                    </div>
                                </div>
                            </div>

                            
                            
                            <div class="form-group">
                                {!! Form::label('question','Вопрос',['class'=>'control-label']) !!}
                                {!! Form::textarea('question',$test_question->question,array_merge([
                                'class'=>'form-control',
                                'placeholder'=>'Введите вопрос теста']))!!}
                            </div>
                            <div class="form-group">
                                @foreach($test_question->testAnswerQuestion as $answer)
                                    <label for="answer" class="label-control">Ответ номер</label>
                                    <input name="answer[{{$answer->id}}][number]" type="number" min="0" max="200" value='{{$answer->number}}'>&nbsp;&nbsp;&nbsp; && &nbsp;&nbsp;&nbsp;
                                    <input name="answer[{{$answer->id}}][is_right]" type="number" min="0" max="200" value='{{$answer->is_right}}'> балл/ов
                                    <input name="answer[{{$answer->id}}][answer]" type="text" class="form-control" value='{{$answer->answer}}'>
                                    <br>
                                @endforeach
                            </div>
                            <div class="form-group">
                                {!! Form::submit('Обновить, Текущий номер #'.$test_question->number,array_merge(['class'=>'btn btn-success'])) !!}
                            </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    
    <div class="container">
        <button class="btn btn-primary center-block" type="button" data-toggle="collapse" data-target="#add-form-test"
                aria-expanded="false" aria-controls="add-form-test">
            Добавить вопрос и ответы теста
        </button>
        <div class="collapse" id="add-form-test">
            <h3>Форма добавления вопроса к тесту  "{{$test->name}}"</h3>
            {!! Form::open(array('route'=>'add_question_answer','method'=>'POST')) !!}
                <input type="hidden" name="test_id" value="{{$test->id}}">
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        {!! Form::label('number','Номер вопроса',['class'=>'control-label']) !!}
                        {!! Form::text('number',$test->testQuestions->count()+1,array_merge(['class'=>'form-control','placeholder'=>'Номер вопроса'])) !!}
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group"> 
                        {!! Form::radio('button_type', 'radio', true ) !!}
                        {!! Form::label('button_type', 'Один вариант из нескольких (Radio)') !!}

                    </div>
                    <div class="form-group">                         
                        {!! Form::radio('button_type', 'checkbox') !!}
                        {!! Form::label('button_type', 'Несколько вариантов из нескольких (Checkbox)') !!}
                    </div>
                </div>
            </div>
                    
                <div class="form-group">
                    {!! Form::label('question','Вопрос',['class'=>'control-label']) !!}
                    {!! Form::textarea('question',null,array_merge([
                    'class'=>'form-control',
                    'placeholder'=>'Введите вопрос теста']))!!}
                </div>
                <div class="form-group">
                    <div id="answers"></div>
                </div>
                <div class="form-group">
                    <input class="btn btn-blue pull-right" type="button" value="Добавить ответ" id="variant_add" placeholder="">
                </div>
                
                <div class="clearfix"></div>
                <hr>
                <div class="form-group text-center">
                    {!! Form::submit('Сохранить новый вопрос с ответами',array_merge(['class'=>'btn btn-success'])) !!}
                </div>
                <hr>
            {!! Form::close() !!}
            
        </div>
    </div>
        
    
    <div class="container">
        <a href="{{route('test.index')}}" class="btn btn-info">Назад</a>
    </div>
    <script>
        $(document).ready(function () {
            var ind_ans = 0;
            var number_qwestion = {{$test->testQuestions->count()+1}};
            console.log(number_qwestion);
            $('#variant_add').click(function (event) {
                ind_ans++;
                event.preventDefault();
                var html = '';
                
                html+='<div id="new_row_' + ind_ans + '" class="row">';
                html+='<div class="col-md-10">';
                html+='<div class="form-group">';
                html+='<label for="answer" class="label-control">Ответ номер</label>';
                html+='<input name="answer[' + ind_ans + '][number]" type="number" min="0" max="1000" value="' + ind_ans + '">&nbsp;&nbsp;&nbsp; && &nbsp;&nbsp;&nbsp;';
                html+='<input name="answer[' + ind_ans + '][is_right]" type="number" min="0" max="1000" value="0"> балл/ов';
                html+='<input name="answer[' + ind_ans + '][answer]" type="text" class="form-control" placeholder="Введите описание ответа">';
                html+='</div>';
                html+='</div>';
                html+='<div class="col-md-2">';
                html+='<button name="remove" data-id="' + ind_ans + '" class="btn btn-danger btn_remove" title="Удалить"><i class="fa fa-trash-o" aria-hidden="true"></i></button>';
                html+='</div>';
                html+='</div>';
                
                $('#answers').append(html);
                /*
                $('#answers').append('<div id="row' + ind_ans + '" class="row form-group">' +
                    '<div class="col-md-2"><label for="answer" class="label-control">Ответ</label></div>' +
                    '<div class="col-md-5"><label for="namber[' + ind_ans + ']">№</label><input name="namber[' + ind_ans + ']" min="0" max="200"><input name="answer[' + ind_ans + ']" type="text" class="form-control"></div>' +
                    '<div class="col-md-5"><label for="bal[' + ind_ans + ']">Бал</label><input name="bal[' + ind_ans + ']"></div><button name="remove" id="' + ind_ans + '" class="btn btn-danger btn_remove">X</button></div>');
                */
            });
            $(document).on('click', '.btn_remove', function () {
                event.preventDefault();
                var button_id = $(this).data('id');
                $('#new_row_' + button_id + '').remove();
            });
        });
        function checkDelete() {
            return confirm('Вы уверены что хотите удалить данный вопрос с ответами?');
        }
    </script>
@endsection

{{--'<div class="col-md-2"><label for="answer" class="label-control">Ответ</label></div>' +--}}
{{--'<div class="col-md-5"><input name="answer[' + ind_ans + ']" type="text" class="form-control"></div>' +--}}
{{--'<div class="col-md-5"><label for="is_right">Правильно</label>' +--}}
{{--'<input type="radio" name="is_right[' + ind_ans + ']" value="1">' +--}}
{{--'<label for="is_right">Не правильно</label>' +--}}
{{--'<input type="radio" name="is_right[' + ind_ans + ']" value="0" checked></div><button name="remove" id="' + ind_ans + '" class="btn btn-danger btn_remove">X</button></div>'--}}