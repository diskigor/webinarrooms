@extends('layouts.admin')
@section('content')
    <div class="col-md-12">
        <h1 class="text-center">Наполнение теста {{$test->name}}</h1>
        <span>Тип теста: {{$test->typeTest->name}}</span>
        <p>{{$test->typeTest->description}}
        </p>
        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#add-form-test"
                aria-expanded="false" aria-controls="add-form-test">
            Наполнить тест
        </button>
        <div class="collapse" id="add-form-test">
            <h3>Форма наполнения теста {{$test->name}}</h3>
            {!! Form::open(array('route'=>'add_question_answer','method'=>'POST')) !!}
            <input type="hidden" name="test_id" value="{{$test->id}}">
            <div class="form-group">
                {!! Form::label('group','Группа (Данное поле не обязательное если у Вас тип теста не "group")',['class'=>'control-label']) !!}
                {!! Form::selectRange('group',0,20,['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('number','Номер вопроса. (Данное поле важно при создании групового теста)',['class'=>'control-label']) !!}
                {!! Form::selectRange('number',0,100,['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('question','Вопрос',['class'=>'control-label']) !!}
                {!! Form::textarea('question',null,array_merge([
                'class'=>'form-control',
                'placeholder'=>'Введите вопрос теста']))!!}
            </div>
            <div class="form-group">
                <div id="answers">

                </div>
            </div>
            <input class="btn btn-blue" type="button" value="Добавить ответ" id="variant_add">
            <hr>
            <div class="form-group">
                {!! Form::submit('Сохранить',array_merge(['class'=>'btn btn-success'])) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <hr>
    <div class="container">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>№</th>
                <th>Вопрос</th>
                <th>Ответ</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 1;?>
            @foreach($test_questions as $test_question)
                <tr>
                    <td>{{$i++}}</td>
                    <td>{!!$test_question->question !!}</td>
                    <td>@foreach($test_question->testAnswerQuestion as $answers)
                            <p>{{ $answers->answer }} @if( $test_question->test->typeTest->name === "boolean" and $answers->is_right)
                                    <i class="fa fa-check-circle" style="color: #2ab27b" aria-hidden="true"></i> @endif
                                @if($test_question->test->typeTest->name === "group")
                                    - {{$answers->is_right}}
                                    @endif
                            </p>
                        @endforeach
                    </td>
                    <td>
                        {{--{!! Form::open(array('route'=>['prelection.edit',$prelection->sdoLectureTrans()->sdoLectures->id],'method'=>'GET','class'=>'inline-block')) !!}--}}
                        {{--{!! Form::button('<i class="fas fa-pencil-alt"></i>',['class'=>'btn btn-info','type'=>'submit']) !!}--}}
                        {{--{!! Form::close() !!}--}}
                        {!! Form::open(array('route'=>['delete_question',$test_question->id],'method'=>'GET','class'=>'inline-block')) !!}
                        {!! Form::button('<i class="fa fa-times"></i>',['class'=>'btn btn-danger','type'=>'submit','onclick'=>'return checkDelete()']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <a href="{{redirect()->back()->getTargetUrl()}}" class="btn btn-info">Назад</a>
    </div>
    <script>
        $(document).ready(function () {
            var ind_ans = 0;
            $('#variant_add').click(function (event) {
                ind_ans++;
                event.preventDefault();
                $('#answers').append('<div id="row' + ind_ans + '" class="row form-group">' +
                    '<div class="col-md-2"><label for="answer" class="label-control">Ответ</label></div>' +
                    '<div class="col-md-5"><label for="namber[' + ind_ans + ']">№</label><input name="namber[' + ind_ans + ']" min="0" max="200"><input name="answer[' + ind_ans + ']" type="text" class="form-control"></div>' +
                    '<div class="col-md-5"><label for="bal[' + ind_ans + ']">Бал</label><input name="bal[' + ind_ans + ']"></div><button name="remove" id="' + ind_ans + '" class="btn btn-danger btn_remove">X</button></div>');
            });
            $(document).on('click', '.btn_remove', function () {
                var button_id = $(this).attr("id");
                $('#row' + button_id + '').remove();
            });
        });
        function checkDelete() {
            return confirm('Вы уверены что хотите удалить данный вопрос с ответами?');
        }
    </script>
@endsection

{{--'<div class="col-md-2"><label for="answer" class="label-control">Ответ</label></div>' +--}}
{{--'<div class="col-md-5"><input name="answer[' + ind_ans + ']" type="text" class="form-control"></div>' +--}}
{{--'<div class="col-md-5"><label for="is_right">Правильно</label>' +--}}
{{--'<input type="radio" name="is_right[' + ind_ans + ']" value="1">' +--}}
{{--'<label for="is_right">Не правильно</label>' +--}}
{{--'<input type="radio" name="is_right[' + ind_ans + ']" value="0" checked></div><button name="remove" id="' + ind_ans + '" class="btn btn-danger btn_remove">X</button></div>'--}}