@extends('layouts.admin')
@section('content')
    <div class="col-md-12">
        <h1 class="text-center">Результаты теста {{$test->name}}</h1>
        <h4 class="text-center">Тип теста: {{$test->typeTest->name}}</h4>

        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#add-form-test"
                aria-expanded="false" aria-controls="add-form-test">
            Добавить результат
        </button>
        <div class="collapse" id="add-form-test">
            <h3>Форма добавления результата теста {{$test->name}}</h3>
            {!! Form::open(array('route'=>['explanation_add',$test->id],'method'=>'POST')) !!}
            <div class="form-group">
                {!! Form::label('key_test','Ключ',['class'=>'control-label']) !!}
                {!! Form::text('key_test',null,['class'=>'form-control','required']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('alias','Абривеатура',['class'=>'control-label']) !!}
                {!! Form::text('alias',null,['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('description','Описание',['class'=>'control-label']) !!}
                {!! Form::textarea('description',null,array_merge([
                'class'=>'form-control',
                'placeholder'=>'Введите обьяснени теста']))!!}
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-blue">Сохранить</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <hr>
    <div class="container">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>№</th>

                <th>Ключ</th>
                <th>Абривеатура</th>
                <th>Описание</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 1?>
            @foreach($explanations as  $explanation)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$explanation->key_test}}</td>
                    <td>{{$explanation->alias}}</td>
                    <td>{!! $explanation->description !!}</td>
                    <td>
                        {!! Form::open(array('route'=>['explanation_delete',$explanation->id],'method'=>'GET','class'=>'inline-block'))!!}
                            <button class="btn btn-danger" type="submit">Удалить</button>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="container">
        <a href="{{route('test.index')}}" class="btn btn-info">Назад</a>
    </div>
    <br>
    <div class="container bg-info">
            <h4>Данный тип теста поддерживает алгоритм</h4>
            <hr>
            @if($test->type_id==1)
                <div class="col-md-offset-1">
                    <h4>1.Диапазон баллов</h4>
                    <h5>1;  - обязательный указатель на применяемый алгоритм, заканчивается точкой с запятой</h5>          
                    <p>1;0-5   при наборе от 0 до 5 включительно,баллов, на ответы, первое описание </p>
                    <p>1;6-20  при наборе от 6 до 20 включительно,баллов, на ответы, второе описание </p>
                    <p>1;21    при наборе от 21 и больше, на ответы, третье описание </p>
                    <p>*обратить внимание, чтобы диапазоны баллов в пределах одного теста не пересекались.</p>
                </div>
                <hr>
                <div class="col-md-offset-1">
                    <h4>2.Диапазон баллов, с указанием номеров вопросов которые будут учитываться, по группам ответов</h4>
                    <h5>2;  - обязательный указатель на применяемый алгоритм, заканчивается точкой с запятой</h5>   
                    <p>2;1:0-7:1,2,7,15...x  - алгоритм<b>[ ; ]</b> группа ответов<b>[ : ]</b> диапазон балов<b>[ : ]</b> номера вопросов ч/з запятую</p>
                    <p>2;1:0-7:1,2,3,4,5,6,7,8,9,10  первая группа ответов, при наборе от 0 до 7 включительно,баллов, на ответы № 1,2,3,4,5,6,7,8,9,10, описание 1.1 </p>
                    <p>2;1:8-15:1,2,3,4,5,6,7,8,9,10  первая группа ответов, при наборе от 8 до 15 включительно,баллов, на ответы № 1,2,3,4,5,6,7,8,9,10, описание 1.2</p>
                    <p></p>
                    <p>2;2:0-7:1,2,3,4,5,6,7,8,9,10  вторая группа ответов, при наборе от 0 до 7 включительно,баллов, на ответы № 1,2,3,4,5,6,7,8,9,10, описание 2.1</p>
                    <p>2;2:8-15:1,2,3,4,5,6,7,8,9,10  вторая группа ответов, при наборе от 8 до 15 включительно,баллов, на ответы № 1,2,3,4,5,6,7,8,9,10, описание 2.2</p>
                    <p></p>
                    <p>*обратить внимание, чтобы диапазоны баллов в пределах одной группы не пересекались.</p>
                    <p>*Выводится несколько групп ответов, если сумма баллов перечисленных ответов, попадает в диапазоны</p>
                </div>
            
            
            
            
            @elseif($test->type_id==2)
                <div class="col-md-offset-1">
                    <h4>10</h4>
                    <p>
                        10;  - обязательный указатель на применяемый алгоритм, заканчивается точкой с запятой<br>
                        [1:1] - первая цифра номер группы, вторая цифра номер вопроса - для которого нужно набрать больше баллов<br>
                    </p>
                    <p>для примера ключи к тесту "Тест Изабеллы Майерс-Бригс"</p>            
                    <p>10;[1:1],[2:1],[3:1],[4:1] - Ключь 1. Рациональный, Экстравертный, Сенсорный, Логический - ЛСЭ - Штирлиц </p> 
                    <p>10;[1:1],[2:1],[3:1],[4:2] - Ключь 2. Рациональный, Экстравертный, Сенсорный, Этический - ЭСЭ - Гюго  </p> 
                    <p>10;[1:1],[2:1],[3:2],[4:1] - Ключь 3. Рациональный, Экстравертный, Интуитивный, Логический - ЛИЭ - Джек </p> 
                    <p>*обратить внимание, на указание номеров групп и номеров ответов.</p> 
                </div>
            @endif
        </div>
@endsection