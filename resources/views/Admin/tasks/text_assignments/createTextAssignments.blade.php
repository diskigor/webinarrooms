@extends('layouts.admin')
@section('content')
    <div class="col-md-12">
        <header class="big-header">Форма добавления тесктового задания</header>
        <div class="panel">
            <div class="form-group">
                <select name="course_id" id="course" required>
                    <option selected="selected">Сортировка по курсу</option>
                    @foreach($course_chearsh as $key=>$value)
                        <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                </select>
            </div>
            {!! Form::open(array('route'=>'text_assignments.store','method'=>'POST')) !!}
            <div class="form-group">
                {!! Form::label('lectures_id','Выберите лекцию',['class'=>'control-label']) !!}
                <select name="lectures_id" id="lectures">

                </select>
            </div>
            <ul class="nav nav-tabs" role="tablist">
                @foreach($arr_local as $lang)
                    @if($loop->first)
                        <li role="presentation" class="active">
                            <a href="#{{$lang.'-first'}}" aria-controls="{{$lang.'-first'}}" role="tab"
                               data-toggle="tab">{{$lang}}</a>
                        </li>
                    @else
                        <li role="presentation">
                            <a href="#{{$lang}}" aria-controls="{{$lang}}" role="tab" data-toggle="tab">{{$lang}}</a>
                        </li>
                    @endif
                @endforeach
            </ul>
            <div class="tab-content">
                @foreach($arr_local as $key => $lang_content)
                    @if($loop->first)
                        <div role="tabpanel" class="tab-pane active fade in" id="{{$lang_content.'-first'}}">
                            <div class="form-group">
                                {!! Form::label('name_'.$key,'Название теста',['class'=>'control-label']) !!}
                                {!! Form::text('name_'.$key,null,array_merge(['class'=>'form-control',
                                                    'placeholder'=>'Введите название текстового задания',
                                                    'required'])) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('text_'.$key,'Введите текстовое задание',['class'=>'control-label']) !!}
                                {!! Form::textarea('text_'.$key,null,array_merge(['class'=>'form-control'])) !!}
                            </div>
                        </div>
                    @else
                        <div role="tabpanel" class="tab-pane fade" id="{{$lang_content}}">
                            <div class="form-group">
                                {!! Form::label('name_'.$key,'Название теста'.'  '.$lang_content,['class'=>'control-label']) !!}
                                {!! Form::text('name_'.$key,null,array_merge(['class'=>'form-control',
                                                    'placeholder'=>'Введите название текстового задания',
                                                    'required'])) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('text_'.$key,'Введите текстовое задание'.'  '.$lang_content,['class'=>'control-label']) !!}
                                {!! Form::textarea('text_'.$key,null,array_merge(['class'=>'form-control'])) !!}
                            </div>

                        </div>
                    @endif
                @endforeach    
                <div class="form-group">
                    {!! Form::submit('Создать',['class'=>'btn btn-success']) !!}
                    <a href="{{route('text_assignments.index')}}" class="btn btn-info">Назад</a>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
    <script src="{{asset('js/sort-course.js')}}"></script>
@endsection