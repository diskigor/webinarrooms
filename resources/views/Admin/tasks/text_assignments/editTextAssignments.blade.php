@extends('layouts.admin')
@section('content')
<div class="col-md-12">
    <h2>Форма редактирования тесктового задания</h2>
    {!! Form::open(array('route'=>['text_assignments.update',$text_tasks->id],'method'=>'PUT')) !!}
    <div class="form-group">
        {!! Form::label('lectures_id','Выберите лекцию',['class'=>'control-label']) !!}
        {!! Form::select('lectures_id',$arr_lectures,$text_tasks->lecture_id,['class'=>'form-control',
        'placeholder' => 'Выберите лекцию к торой относится задание']) !!}
    </div>

    <ul class="nav nav-tabs" role="tablist">
        @foreach($text_tasks->getTextTasksTransEdit->reverse() as $text_task)
            @if($loop->first)
                <li role="presentation" class="active">
                    <a href="#{{$text_task->lang_local.'-first'}}" aria-controls="{{$text_task->lang_local.'-first'}}" role="tab"
                       data-toggle="tab">{{$text_task->lang_local}}</a>
                </li>
            @else
                <li role="presentation">
                    <a href="#{{$text_task->lang_local}}" aria-controls="{{$text_task->lang_local}}" role="tab" data-toggle="tab">{{$text_task->lang_local}}</a>
                </li>
            @endif
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach($text_tasks->getTextTasksTransEdit->reverse() as $text_task)
            @if($loop->first)
                <div role="tabpanel" class="tab-pane active fade in" id="{{$text_task->lang_local.'-first'}}">
                    <div class="form-group">
                        {!! Form::label('name_'.$text_task->lang_local,'Название теста',['class'=>'control-label']) !!}
                        {!! Form::text('name_'.$text_task->lang_local,$text_task->name_trans,array_merge(['class'=>'form-control',
                        'placeholder'=>'Введите название текстового задания',
                        'required'])) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('text_'.$text_task->lang_local,'Введите текстовое задание',['class'=>'control-label']) !!}
                        {!! Form::textarea('text_'.$text_task->lang_local,$text_task->content_trans,array_merge(['class'=>'form-control'])) !!}
                    </div>
                </div>
            @else
                <div role="tabpanel" class="tab-pane fade" id="{{$text_task->lang_local}}">
                    <div class="form-group">
                        {!! Form::label('name_'.$text_task->lang_local,'Название теста'.'  '.$text_task->lang_local,['class'=>'control-label']) !!}
                        {!! Form::text('name_'.$text_task->lang_local,$text_task->name_trans,array_merge(['class'=>'form-control',
                        'placeholder'=>'Введите название текстового задания',
                        'required'])) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('text_'.$text_task->lang_local,'Введите текстовое задание'.'  '.$text_task->lang_local,['class'=>'control-label']) !!}
                        {!! Form::textarea('text_'.$text_task->lang_local,$text_task->content_trans,array_merge(['class'=>'form-control'])) !!}
                    </div>
                </div>
            @endif
        @endforeach    
        <div class="form-group">
            {!! Form::submit('Сохранить изменения',['class'=>'btn btn-success']) !!}
        </div>
        {!! Form::close() !!}
        <a href="{{route('text_assignments.index')}}" class="btn btn-info">Назад</a>
    </div>
</div>
@endsection