@extends('layouts.admin')

@section('content')
<link rel="stylesheet" href="{{ asset('css/datatables.css') }}">
    <div class="col-md-12">
        <header class="big-header">Список Текстовых заданий</header>
        <div class="panel">
        <a href="{{route('text_assignments.create')}}" class="btn btn-success">
            <i class="fa fa-plus" aria-hidden="true"></i>
            Добавить задание
        </a>
        <hr>

            <table id='listTEXTassignments' class="table table-responsive">
                <thead>
                <tr>
                    <th>№</th>
                    <th class="col-md-3">Название</th>
                    <th class="col-md-3">Текст</th>
                    <th>Курс</th>
                    <th>Лекция</th>
                    <th class="text-center">Действия</th>
                </tr>
                </thead>
                <tbody>
                @foreach($text_tasks as $text_task)
                    <tr>
                        <td>{{ $text_task->id }}</td>
                        <td>{{ $text_task->lang?$text_task->lang->name_trans:'' }}</td>
                        <td>{{ mb_strimwidth(strip_tags ($text_task->lang?$text_task->lang->content_trans:''),0,100,'...') }}</td>
                        <td>{{ $text_task->prelection->course->lang->name_trans }}</td>
                        <td>{{ $text_task->prelection->lang->name_trans}}</td>
                        <td>
                            {!! Form::open(array('route'=>['text_assignments.edit',$text_task->id],'method'=>'GET','class'=>'inline-block')) !!}
                            {!! Form::button('<i class="fas fa-pencil-alt"></i>',['class'=>'btn btn-info','type'=>'submit','title'=>'edit']) !!}
                            {!! Form::close() !!}

                            {!! Form::open(array('route'=>['text_assignments.destroy',$text_task->id], 'method'=>'DELETE','class'=>'inline-block')) !!}
                            {!! Form::button('<i class="fa fa-times"></i>',['class'=>'btn btn-danger','title'=>'delete','type'=>'submit','onclick'=>'return checkDelete()']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>



@endsection

@section('script')

<script src="{{ asset('js/datatables/datatables.js') }}"></script>
<script language="JavaScript" type="text/javascript">
    function checkDelete() {
        return confirm('Вы уверены что хотите удалить данное текстовое задание?');
    }
   $(document).ready(function() {
    $('#listTEXTassignments').DataTable();
} );
</script>

@endsection