@extends('layouts.admin')
@section('content')
<?php $title_page='Последние выпуски рассылки'; ?>
<div class="col-md-12 nauka-main bg_white ">
    
    <div class="chat">
    <!-- Nav tabs -->
        <ul class="chat-left col-md-2" role="tablist">
            <?php $i = 1;?>
            @foreach($mediascans as $mediascan)
                @if($i === 1)
                    <li role="presentation" class="active"><a href="{{'#tab-'.$mediascan->id}}"
                                                              aria-controls="{{'tab-'.$mediascan->id}}"
                                                              role="tab"
                                                              data-toggle="tab">{{$mediascan->mediascan_date}}</a>
                    </li>
                @else
                    <li role="presentation"><a href="{{'#tab-'.$mediascan->id}}"
                                               aria-controls="{{'tab-'.$mediascan->id}}" role="tab"
                                               data-toggle="tab">{{$mediascan->mediascan_date}}</a>
                    </li>
                @endif
                    <?php $i++ ?>
            @endforeach
            
        </ul>

        <!-- Tab panes -->
        <div class="chat-right col-md-10">
            <div class="tab-content">
                <?php $n = 1;?>
                @foreach($mediascans as $mediascan)
                    @if($n === 1)
                        <div role="tabpanel" class="tab-pane active fade in" id="{{'tab-'.$mediascan->id}}">
                            <div class="chat-item left">
                                {!! $mediascan->mediascan_body !!}
                                <p>
                                </p>
                            </div>
                        </div>
                    @else
                        <div role="tabpanel" class="tab-pane fade" id="{{'tab-'.$mediascan->id}}">
                            <div class="chat-item left">
                                {!! $mediascan->mediascan_body !!}
                            </div>
                        </div>
                    @endif
                    <?php $n++ ?>
                @endforeach
            </div>
        </div>
        
    </div>
    
</div>
<div class="col-md-12" style="text-align:center;">
    <a class="btn allarchivebutton" href="{{route('admin_all_archive.index')}}">{{trans('home.allarchive')}}</a>
</div>
@endsection