@extends('Admin.app')
@section('content')
    <div class="col-md-12">
        <h1 style="text-align: center;">Наука Лидерства</h1>
        <section class="bg_white">
            <p>Рассылка "Наука Лидерства" является частью образовательного проекта Школы Эффективных Лидеров. В ее
                рамках автор проекта отвечает на вопросы подписчиков, осуществляя открытое дистанционное
                консультирование (публичный коучинг) по вопросам, связанным с функционированием проекта, с тематикой
                курсов, со смежными вопросами.</p>
            <p>Содержание рассылки само по себе составляет существенную часть дистанционного образования в рамках нашего
                проекта. Знакомство с ее содержанием (ознакомьтесь с архивом предыдущих выпусков) позволит Вам по-новому
                взглянуть на свой образ жизни и образ действий, изменить некоторые элементы мировоззрения,
                препятствующие жизненному успеху. Живая ткань рассылки "Наука Лидерства" позволит понять и историю
                проекта "Школа эффективных лидеров", и его содержание, и его дух.</p>
            <p>Ознакомление с материалами этого раздела позволит Вам составить себе точное представление о проекте и
                принять осознанное решение - стоит ли Вам становиться курсантом Школы эффективных лидеров и членом Клуба
                эффективных лидеров.</p>
            <p>Успехов Вам на этом пути!</p>
            <div class="text-center">
                <a class="btn" data-toggle="modal" data-target="#question-modal">Задать свой вопрос Е.В.Гильбо</a>
            </div>
            <div class="text-center">
                <a class="" href="https://play.google.com/store/apps/details?id=ru.gilbo.leadersschoolapp"
                   target="_blank">
                    Приложение для Android <img height="55px" src="{{asset('images/gplay.png')}}">
                </a>
            </div>
        </section>
        <table class="table table-archive">
            @foreach($count_sends_year as $key=>$value)
                @if($key > intval($year))
                    <tr>
                        <th colspan="6" class="text-left"><a
                                    href="{{route('admin_show_years_archive',$key)}}">Год: {{$key}}</a>
                        </th>
                        <th colspan="6" class="text-right">Выпусков: {{$value}}</th>
                    </tr>
                @endif
            @endforeach
        </table>
        <h2 class="text-center">Архив за {{$year}}</h2>
        <table class="table table-archive">
            <tr class="month">
                <th>Январь</th>
                <th>Февраль</th>
                <th>Март</th>
                <th>Апрель</th>
                <th>Май</th>
                <th>Июнь</th>
                <th>Июль</th>
                <th>Август</th>
                <th>Сентябрь</th>
                <th>Октябрь</th>
                <th>Ноябрь</th>
                <th>Декабрь</th>
            </tr>
            <tr>
                @foreach($arr_month as $month)
                    <td valign="top">
                        @if($count_archive_year->getMonthDay($year,$month)->count() != 0)
                            @foreach($count_archive_year->getMonthDay($year,$month) as $day)
                                <div>
                                    <a href="{{route('admin_show_post',$day->id)}}">{{$day->day}}</a>
                                </div>
                            @endforeach
                        @else
                            <span></span>
                        @endif
                    </td>
                @endforeach
            </tr>
        </table>
        <table class="table table-archive">
            @foreach($count_sends_year as $key=>$value)
                @if($key < intval($year))
                    <tr>
                        <th colspan="6" class="text-left"><a
                                    href="{{route('admin_show_years_archive',$key)}}">Год: {{$key}}</a>
                        </th>
                        <th colspan="6" class="text-right">Выпусков: {{$value}}</th>
                    </tr>
                @endif
            @endforeach
        </table>
        <div class="text-center">
            <a href="{{route('admin_all_archive.index')}}" class="btn btn-success">Назад</a>
        </div>
    </div>
@endsection