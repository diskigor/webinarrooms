@extends('layouts.admin')
@section('content')
<?php $title_page='Поиск по рассылке'; ?>
<div class="col-md-12 nauka-main bg_white ">
    
    <div class="chat">
    <!-- Nav tabs -->
        <ul class="chat-left col-md-2" role="tablist">

            @foreach($mediascans as $mediascan)
                @if($loop->first)
                    <li role="presentation" class="active"><a href="{{'#tab-'.$mediascan->id}}"
                                                              aria-controls="{{'tab-'.$mediascan->id}}"
                                                              role="tab"
                                                              data-toggle="tab">{{$mediascan->mediascan_date}}</a>
                    </li>
                @else
                    <li role="presentation"><a href="{{'#tab-'.$mediascan->id}}"
                                               aria-controls="{{'tab-'.$mediascan->id}}" role="tab"
                                               data-toggle="tab">{{$mediascan->mediascan_date}}</a>
                    </li>
                @endif

            @endforeach
            
        </ul>

        <!-- Tab panes -->
        <div class="chat-right col-md-10">
            <div class="tab-content">
                
                
                
                
                @foreach($mediascans as $mediascan)
                    @if($loop->first)
                        <div role="tabpanel" class="tab-pane active fade in" id="{{'tab-'.$mediascan->id}}">
                            <div class="chat-item left">
                                {!! $mediascan->mediascan_body !!}
                                    <?
//                                    $text = $mediascan->mediascan_body;
//                                    $highlight = request()->input("textsearch");
//                                    $highlight_word = preg_replace('/' . $highlight . '/i', '<b style="color: red">\\0</b>', $text);
//                                    echo $highlight_word;
                                    ?>
                                <p>
                                </p>
                            </div>
                        </div>
                   
                    @else
                        <div role="tabpanel" class="tab-pane fade" id="{{'tab-'.$mediascan->id}}">
                            <div class="chat-item left">
                                {!! $mediascan->mediascan_body !!}
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
        
    </div>
    
</div>
{{ $mediascans->links() }}
<div class="col-md-12" style="text-align:center;">
    <a class="btn allarchivebutton" href="{{route('admin_all_archive.index')}}">{{trans('home.allarchive')}}</a>
</div>
@endsection