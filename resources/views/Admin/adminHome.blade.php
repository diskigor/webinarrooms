@extends('layouts.admin')

@push('stack_head_script')
    <script type="text/javascript" src="{{ asset('js/moment/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/datarangepicker/daterangepicker.js') }}"></script>
    <script src="{{asset('js/jquery.circliful.min.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('js/datarangepicker/daterangepicker.css') }}" />
    <script src="https://cdn.anychart.com/releases/8.3.0/js/anychart-base.min.js" type="text/javascript"></script>
@endpush

@section('content')
    <?php $title_page='Личный кабинет'; ?>

<div class="col-md-12 first-admin-home-block">
        @if(Auth::user()->isAdmin())
            <div class="col-lg-4 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-envelope fa-3x" aria-hidden="true"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{count(Auth::user()->getMessage())}}</div>
                                <div>Новых сообщения</div>
                            </div>
                        </div>
                    </div>
                    <a href="{{route('message.index')}}">
                        <div class="panel-footer">
                            <span class="pull-left">Посмотреть</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-users fa-3x" aria-hidden="true"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ $ref_orders }}</div>
                                <div>Рефералов</div>
                            </div>
                        </div>
                    </div>
                    <a href="{{route('refsystem.index')}}">
                        <div class="panel-footer">
                            <span class="pull-left">Весь список</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="clearfix visible-md"></div>
            <div class="col-lg-4 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-book fa-3x" aria-hidden="true"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ $orders_count }}</div>
                                <div>Купленых курсов</div>
                            </div>
                        </div>
                    </div>
                    <a href="{{route('orders_statistics.index')}}">
                        <div class="panel-footer">
                            <span class="pull-left">Архив курсов</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>

            </div>
            
            <div class="clearfix"></div>
            <div class="col-md-12 admin-charts-block1">
                @include('Admin.personal_area.block_admin_users_courses',[ 'block_users_courses'=>$block_users_courses ])
            </div>
            
            <div class="clearfix"></div>
            <div class="col-md-12" id="block_referalsActivityController"></div>
            
            <div class="clearfix"></div>
            <div class="col-md-12" id="block_financeActivityController"></div>
            
            @push('stack_scripts')

                <script>
                    $('#block_referalsActivityController').load("{{ route('all_admin_referalsActivity.index') }}");
                    $('#block_financeActivityController').load("{{ route('all_admin_financeActivity.index') }}");
                </script>
            @endpush
            
            
            
        @endif
        
        @if(Auth::user()->isCadet()  || Auth::user()->isСurator())
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="col-md-12 user-home-status box-shadow">
                    <div class="col-md-5 col-xs-5" style="padding: 0">
                        <p>Ваш статус</p>
                        <img class="img-responsive" src="{{asset('images/status-icon.png')}}" />
                        <p>{{Auth::user()->role->description}}</p>
                        <!-- {{ Auth::user()->role->PseudoName }} -->
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                    </div>
                    <div class="col-md-5 col-xs-5" style="padding: 0">
                        <p>Следующий статус</p>
                        <img class="img-responsive" src="{{asset('images/status-icon.png')}}" />
                        <p>{{Auth::user()->role->description}}</p>
                    </div>
                    
                    <!--new>
                    <div class="col-md-4 col-xs-4 {{ Auth::user()->role->PseudoName=='Cadet'?'acttive':'' }}" style="padding: 0">
                        <!--p>Ваш статус</p>
                        <img class="img-responsive" src="{{asset('images/status-icon.png')}}" />
                        <p>Курсант</p>
                        <!-- {{ Auth::user()->role->PseudoName }} >
                    </div>
                    <div class="col-md-4 col-xs-4 {{ Auth::user()->role->PseudoName=='Pretendent'?'acttive':'' }}" style="padding: 0">
                        <!--p>Ваш статус</p>
                        <img class="img-responsive" src="{{asset('images/status-icon.png')}}" />
                        <p>Продвинутый</p>
                        <!-- {{ Auth::user()->role->PseudoName }} >
                    </div>
                    
                    <div class="col-md-4 col-xs-4 {{ Auth::user()->role->PseudoName=='Сurator'?'acttive':'' }}"  style="padding: 0">
                        <!--p>Следующий статус</p>
                        <img class="img-responsive" src="{{asset('images/status-icon.png')}}" />
                        <p>Эксперт</p>
                    </div>
                    <!--new-->
                    
                    <div class="col-md-12 user-home-line"></div>
                    <div class="col-md-5 col-xs-5" style="padding: 0">
                        <p>Ваши баллы</p>
                        <img class="img-responsive" src="{{asset('images/bonus-icon.png')}}" />
                        <p>{{Auth::user()->level_point}} Балл(-ов-а)</p>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                    </div>
                    <div class="col-md-5 col-xs-5" style="padding: 0">
                        <p>Ваш баланс</p>
                        <img class="img-responsive" src="{{asset('images/wallet-icon.png')}}" />
                        <p>{{Auth::user()->balance}} КЭЛ</p>
                    </div>
                    <i class="fas fa-info-circle" aria-hidden="true" data-toggle-home="tooltiphomeone"></i>
                </div>
                
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="col-md-12 user-home-referal box-shadow">
                    <div class="col-md-5 col-xs-5" style="padding: 0">
                        <p>Ваши рефералы</p>
                        <img class="img-responsive" src="{{asset('images/referals-icon.png')}}" />
                        <p>Рефералов {{Auth::user()->getMyReferalsLevel(3)->collapse()->count()}}</p>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                    </div>
                    <div class="col-md-5 col-xs-5"  style="padding: 0">
                        <p class="referal-send-name">Пригласить по ссылке</p>
                        <input type="text" class="ref-link" value="{{Auth::user()->getReferalLink() }}" disabled=""/>
                        <p class="referal-send">Отправить: 
                            <a href="mailto:?subject=Реферальная ссылка на сайт lsg.ru&body=Перейдите по ссылке для регистрациии получения бонуса на сайте lsg.ru {{Auth::user()->getReferalLink() }}" target="_blank">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </a>
                        </p>
                    </div>
                    <div class="col-md-12 user-home-line"></div>
                    <div class="col-md-5 col-xs-5" style="padding: 0">
                        <p>Ваш доход</p>
                        <img class="img-responsive" src="{{asset('images/refpay-icon.png')}}" />
                        <p>{{Auth::user()->getMyReferalsAllBonus()}} КЭЛ</p>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                    </div>
                    <div class="col-md-5 col-xs-5" style="padding: 0">
                        <p>О реферальной системе</p>
                        <a class="btn-blue" href="{{ route('refsystem.index') }}">Подробнее</a>
                    </div>
                    <i class="fas fa-info-circle" aria-hidden="true" data-toggle-home="tooltiphometwo"></i>
                </div>
                
            </div>
        </div>
        @endif
    </div>
        @if(Auth::user()->isCadet() || Auth::user()->isСurator())
        <div class="col-md-12">
            @if(!empty($bay_all_curses))
                {!! $bay_all_curses !!}
            @endif
        </div>
        <div class="col-md-12 progress-block">
            <h3>Прогресс обучения</h3>
            <span class="line-popup"></span>
            @foreach($user_courses as $course)
                <div class=" col-md-10">
                    <p style="text-transform: uppercase;">{{$course->course->LangName}}</p>
                    <div class="progress">
                        <!--span>Прогресс {!! $course->currentProcent() !!} %</span-->
                        <div class="progress-bar" 
                             role="progressbar" 
                             aria-valuenow="{{ $course->currentProcent() }}"
                             aria-valuemin="0" 
                             aria-valuemax="100" 
                             style="min-width: 2em;width: {{ $course->currentProcent() }}%;">
                            {{ $course->currentProcent() }}%
                        </div>
                    </div>
                </div>
                <div class="col-md-2" style="text-align: center;">
                    <?php
                        $xr = route('main_studies.show',$course->course->id);
                        
                        if ($course->course->author && $course->course->author->isLecturer()) {
                            $xr = route('lecture_studies.show',$course->course->id);
                        }   
                        
                        if ($course->course->IsFree) {
                            $xr = route('free_studies.show',$course->course->id);
                        }
                 
                    ?>
                    
                    <a class="btn-charts" href="{{ $xr }}">
                        @if($course->currentProcent()== 0)
                            Начать
                        @endif
                        @if($course->currentProcent()== 100)
                            Повторить
                        @endif
                        @if(0<$course->currentProcent() && $course->currentProcent()<100)
                            Продолжить
                        @endif
                        <br>обучение
                    </a>
                </div>
            @endforeach()
            <i class="fas fa-info-circle" aria-hidden="true" data-toggle-home="tooltiphomethree"></i>
        </div>
        <div class="row hidden">
            @if(isset($video_news))
                    <div class="col-md-6">

                            <iframe style="width: 100%" height="350" src="{{$video_news->href}}" frameborder="0" allowfullscreen></iframe>
                            <div class="video-description">
                                    {{$video_news->short_desc}}
                            </div>

                    </div>
            @endif
        </div>
        @endif
        
        


@endsection


