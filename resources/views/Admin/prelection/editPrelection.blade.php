@extends('layouts.admin')
@section('content')
    <div class="col-md-12">
        <header class="big-header text-center">Форма редактирование лекции SDO</header>
        <div class="panel">
            {!! Form::open(array('route'=>['prelection.update',$prelection->id],'method'=>'PUT','enctype'=>'multipart/form-data')) !!}
            <div class="form-group">
                {!! Form::label('curse','Курс которому принадлежит лекция',['class'=>'control-label']) !!}
                {!! Form::select('curse_id',$arr_curses,$prelection->curse_id,array_merge(['class'=>'form-control'])) !!}
            </div>
            <div class="form-group">
                {!! Form::label('number','Номер лекции :' ,['class'=>'control-label']) !!}<br>
                {!! Form::selectRange('number',1,200,$prelection->number,array_merge(['class'=>'form-control'])) !!}
            </div>
            <div class="form-group">
                {!! Form::label('block_number','Номер блока: ' .$prelection->number ,['class'=>'control-label']) !!}<br>
                <small>Данное поле не обязательно если Вам не нужно группировать лекции по блокам. Если внесли вывод будет группироваться по блокам. По умолчанию должно быть 0</small>
                {!! Form::number('block_number',$prelection->block_number,array_merge(['class'=>'form-control','min'=>0])) !!}
            </div>
            <div class="form-group">
                {!! Form::label('price','Кол-во баллов получаемых пользователей за прохождение лекции:',['class'=>'control-label']) !!}
                {!! Form::number('price',$prelection->price,array_merge(['class'=>'form-control'])) !!}
            </div>
            <div>
                <ul class="nav nav-tabs" role="tablist">
                    <?php $i = 0;?>
                    @foreach($prelection->sdoLectureTransEdit as $prelection_trans)
                        <?php $i++;?>
                        @if($i===1)
                            <li role="presentation" class="active">
                                <a href="#{{$prelection_trans->lang_local.'-first'}}" aria-controls="{{$prelection_trans->lang_local.'-first'}}" role="tab"
                                   data-toggle="tab">{{$prelection_trans->lang_local}}</a>
                            </li>
                        @else
                            <li role="presentation">
                                <a href="#{{$prelection_trans->lang_local}}" aria-controls="{{$prelection_trans->lang_local}}" role="tab" data-toggle="tab">{{$prelection_trans->lang_local}}</a>
                            </li>
                        @endif
                    @endforeach
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <?php $c = 0;?>
                    @foreach($prelection->sdoLectureTransEdit as $prelection_trans_content)
                        <?php $c++;?>
                        @if($c===1)
                            <div role="tabpanel" class="tab-pane active fade in" id="{{$prelection_trans_content->lang_local.'-first'}}">
                                <div class="form-group">
                                    {!! Form::label('name_trans','Название лекции',['class'=>'control-label']) !!}
                                    {!! Form::text('name_trans_'.$prelection_trans_content->lang_local,$prelection_trans_content->name_trans,array_merge([
                                    'class'=>'form-control',
                                    'placeholder'=>'Название лекции',
                                    'required']))!!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('short_desc_','Короткое описание лекции',['class'=>'control-label']) !!}
                                    {!! Form::textarea('short_desc_'.$prelection_trans_content->lang_local,$prelection_trans_content->short_desc,array_merge([
                                    'class'=>'form-control']))!!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('content_trans','Лекция',['class'=>'control-label']) !!}
                                    {!! Form::textarea('content_trans_'.$prelection_trans_content->lang_local,$prelection_trans_content->content_trans,['class'=>'form-control']) !!}
                                </div>
                                @else
                                    <div role="tabpanel" class="tab-pane fade" id="{{$prelection_trans_content->lang_local}}">
                                        <div class="form-group">
                                            {!! Form::label('name_trans','Название лекции'.'  '.$prelection_trans_content->lang_local,['class'=>'control-label']) !!}
                                            {!! Form::text('name_trans_'.$prelection_trans_content->lang_local,$prelection_trans_content->name_trans,array_merge([
                                            'class'=>'form-control',
                                            'placeholder'=>'Название лекции',
                                            'required']))!!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('short_desc_','Короткое описание лекции'.'  '.$prelection_trans_content->lang_local,['class'=>'control-label']) !!}
                                            {!! Form::textarea('short_desc_'.$prelection_trans_content->lang_local,$prelection_trans_content->short_desc,array_merge([
                                   'class'=>'form-control']))!!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('content_trans','Лекция'.'  '.$prelection_trans_content->lang_local,['class'=>'control-label']) !!}
                                            {!! Form::textarea('content_trans_'.$prelection_trans_content->lang_local,$prelection_trans_content->content_trans,['class'=>'form-control']) !!}
                                        </div>
                                        @endif
                                    </div>
                                    @endforeach
                            </div>
                            <div class="form-group">
                                {!! Form::submit(trans('news.save_button'),['class'=>'btn btn-info']) !!}
                            </div>
                            {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
