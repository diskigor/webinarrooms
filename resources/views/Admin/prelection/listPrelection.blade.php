@extends('layouts.admin')
@section('content')
<div class="col-md-12">
    <header class="big-header text-center">Лекции SDO</header>
    <div class="panel">
        <div class="row">
            <div class="col-md-4">
                <a href="{{route('prelection.create')}}" class="btn btn-success">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                    Добавить лекцию
                </a>
            </div>


            <div class="col-md-8 text-right">
                <form action="{{route('prelection.index')}}" method="GET">
                    <div class="form-group">
                        <select name="course_id" class="form-control" required style="width: 300px; float: right;">
                            <option value="-1">Сортировка по курсу</option>
                            @foreach($course_chearsh as $key=>$value)
                            <option 
                                {{ $curse_id==$key?'selected="selected"':''}}
                                value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                        <a href="{{route('prelection.index')}}" class="btn btn-warning">сбросить</a>
                        <button type="submit" class="btn btn-info">Сортировать</button>
                    </div>
                </form>
            </div>
        </div>
        <form action="{{route('multiple_delete_prelection')}}" method="POST">
            {{csrf_field()}}
            <button type="submit" class="btn btn-danger" onclick='return checkDelete()'><i class="fa fa-trash"></i> Удалить выбранные</button>
            <hr>
            <div class="row" >
                @foreach($prelections as $prelection)
                <figure class="col-sm-4">
                    <div class="thumbnail prelection">
                        <img class="image" src="{{ $prelection->course->imagex }}">
                        <figcaption>
                            <div class="choser">
                                <input type="checkbox" name="prelections_id[]" value="{{$prelection->id}}">
                            </div>
                            <h3>{{$prelection->sdoLectureTrans()->name_trans}}</h3>
                            <div><label>Создан :</label> {{$prelection->sdoLectureTrans()->created_at}}</div>
                            <div><label>Автор :</label> {{$prelection->sdoLectureTrans()->sdoLectures->author->fullFIO}}
                            </div>
                            <div><label>{{trans('content.role_user')}}
                                    :</label> {{$prelection->sdoLectureTrans()->sdoLectures->author->role->name}}</div>
                            <div><label>Кол-во текстовых заданий :</label> {{count($prelection->text_tasks)}}</div>
                            <div><label>Цена :</label> {{$prelection->sdoLectureTrans()->sdoLectures->price}} $</div>
                        </figcaption>
                        <div class="hover-nav">
                            <a href="{{route('prelection.show',$prelection->sdoLectureTrans()->sdoLectures->id)}}" class="btn btn-success"><i class="fa fa-eye"></i></a>
                            <a href="{{route('prelection.edit',$prelection->sdoLectureTrans()->sdoLectures->id)}}" class="btn btn-info"><i class="fas fa-pencil-alt"></i></a>
                            {!! Form::open(array('route'=>['prelection_destr',$prelection->sdoLectureTrans()->sdoLectures->id],'class'=>'inline-block')) !!}
                            {!! Form::button('<i class="fa fa-times"></i>',['class'=>'btn btn-danger','type'=>'submit','onclick'=>'return checkDelete()']) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </figure>
                @endforeach
            </div>
        </form>
        {{$prelections->links()}}
    </div>
</div>
<script language="JavaScript" type="text/javascript">
    function checkDelete() {
        return confirm('Вы уверены?');
    }
</script>
@endsection
