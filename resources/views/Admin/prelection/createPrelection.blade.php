@extends('layouts.admin')
@section('content')
    <div class="col-md-12">
        <h1 class="text-center">Форма добавления лекции SDO</h1>
        <hr>
        {!! Form::open(array('route'=>'prelection.store','method'=>'POST','enctype'=>'multipart/form-data')) !!}
        <div class="form-group">
            {!! Form::label('curse','Курс которому принадлежит лекция',['class'=>'control-label']) !!}
            {!! Form::select('curse_id',$arr_curses,null,array_merge(['class'=>'form-control'])) !!}
        </div>
        <div class="form-group">
            {!! Form::label('number','Номер лекции',['class'=>'control-label']) !!}
            {!! Form::selectRange('number',1,200,array_merge(['class'=>'form-control'])) !!}
        </div>
        <div class="form-group">
            {!! Form::label('block_number','Номер блока: ',['class'=>'control-label']) !!}<br>
            <small>Данное поле не обязательно если Вам не нужно группировать лекции по блокам. Если внесли вывод будет группироваться по блокам. По умолчанию должно быть 0</small>
            {!! Form::number('block_number',0,array_merge(['class'=>'form-control','min'=>0])) !!}
        </div>
        <div class="form-group">
            {!! Form::label('price','Кол-во баллов получаемых пользователей за прохождение лекции:',['class'=>'control-label']) !!}
            {!! Form::number('price',null,array_merge(['class'=>'form-control'])) !!}
        </div>
        <div>
            <ul class="nav nav-tabs" role="tablist">
                <?php $i = 0;?>
                @foreach($arr_local as $lang)
                    <?php $i++;?>
                    @if($i===1)
                        <li role="presentation" class="active">
                            <a href="#{{$lang.'-first'}}" aria-controls="{{$lang.'-first'}}" role="tab"
                               data-toggle="tab">{{$lang}}</a>
                        </li>
                    @else
                        <li role="presentation">
                            <a href="#{{$lang}}" aria-controls="{{$lang}}" role="tab" data-toggle="tab">{{$lang}}</a>
                        </li>
                    @endif
                @endforeach
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <?php $c = 0;?>
                @foreach($arr_local as $key=>$lang_content)
                    <?php $c++;?>
                    @if($c===1)
                        <div role="tabpanel" class="tab-pane active fade in" id="{{$lang_content.'-first'}}">
                            <div class="form-group">
                                {!! Form::label('name_trans_'.$key,'Название лекции',['class'=>'control-label']) !!}
                                {!! Form::text('name_trans_'.$key,null,array_merge([
                                'class'=>'form-control',
                                'placeholder'=>'Название лекции',
                                'maxlength'=>'240',
                                'required']))!!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('short_desc_'.$key,'Короткое описание лекции',['class'=>'control-label']) !!}
                                {!! Form::textarea('short_desc_'.$key,null,array_merge(['class'=>'form-control']))!!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('content_trans_'.$key,'Лекция',['class'=>'control-label']) !!}
                                {!! Form::textarea('content_trans_'.$key,null,['class'=>'form-control']) !!}
                            </div>
                            @else
                                <div role="tabpanel" class="tab-pane fade" id="{{$lang_content}}">
                                    <div class="form-group">
                                        {!! Form::label('name_trans_'.$key,'Название лекции'.'  '.$lang_content,['class'=>'control-label']) !!}
                                        {!! Form::text('name_trans_'.$key,null,array_merge([
                                        'class'=>'form-control',
                                        'placeholder'=>'Название лекции',
                                        'maxlength'=>'240',
                                        'required']))!!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('short_desc_'.$key,'Короткое описание лекции'.'  '.$lang_content,['class'=>'control-label']) !!}
                                        {!! Form::textarea('short_desc_'.$key,null,array_merge(['class'=>'form-control']))!!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('content_trans_'.$key,'Лекция'.'  '.$lang_content,['class'=>'control-label']) !!}
                                        {!! Form::textarea('content_trans_'.$key,null,['class'=>'form-control']) !!}
                                    </div>
                                    @endif
                                </div>
                                @endforeach
                        </div>
                        <div class="form-group">
                            {!! Form::submit(trans('news.save_button'),['class'=>'btn btn-success']) !!}
                        </div>
                        {!! Form::close() !!}
                        <a href="{{route('prelection.index')}}" class="btn btn-info">Назад</a>
            </div>
        </div>
    </div>
@endsection
