@extends('layouts.admin')
@section('content')
    <div class="col-md-12">
        <header class="big-header text-center">Лекция № {{$prelection->sdoLectures->number}}. Курс
            : {{$prelection->sdoLectures->course->trans_curse[0]->name_trans}}</header>
        <div class="panel">
            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal"
                    data-target="{{'#modal-'.$prelection->sdo_lectures_id}}">
                Добавить доп. материал
            </button>
            <a href="{{route('prelection.index')}}" class="btn btn-warning btn-lg">Назад</a>
            <hr>
            <!-- Modal -->
            <div class="modal fade" id="{{'modal-'.$prelection->sdo_lectures_id}}" tabindex="-1" role="dialog"
                 aria-labelledby="my{{'modal-'.$prelection->sdo_lectures_id}}">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="my{{'modal-'.$prelection->sdo_lectures_id}}">Форма добавления
                                дополнительного материала</h4>
                        </div>
                        {!! Form::open(array('route'=>['prelection_material',$prelection->sdo_lectures_id],'method'=>'POST','class'=>'inline-block')) !!}
                        <div class="modal-body">
                            <div class="form-group">
                                {!! Form::label('material','Выберите дополнительный материал',['class'=>'control-label']) !!}
                                <br>
                                {!! Form::select('material',$add_materials, null, array_merge(['class'=>'form-control','placeholder'=>'Выберите материал'])) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('sort','Выберите номер сортировки дополнительного материала',['class'=>'control-label']) !!}
                                {!! Form::selectRange('sort',1,100,array_merge(['class'=>'form-control'])) !!}
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                            {!! Form::submit('Сохранить',array_merge(['class'=>'btn btn-primary'])) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <!-- Table for add_materials -->
            @if($prelection->sdoLectures->materials->count() != 0)
            <table class="table table-condensed">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Название</th>
                    <th>Тип</th>
                    <th>№ Сортировки</th>
                    <th>{{trans('content.actions')}}</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1;?>
                @foreach($prelection->sdoLectures->materials as $add_material)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$add_material->material->name}}</td>
                        <td class="text-center">
                            @if($add_material->material->type==='audio')
                                Аудио
                                @elseif($add_material->material->type==='pdf')
                                PDF
                                @elseif($add_material->material->type==='text')
                                Текст
                                @elseif($add_material->material->type==='video_link')
                                Видео
                                @elseif($add_material->material->type==='archives')
                                Архив
                                @else
                                Неизвестный тип
                                @endif
                        </td>
                        <td class="text-center">{{$add_material->sort}}</td>
                        <td class="text-center">
                            {!! Form::open(array('route'=>['delete_prelection_materail',$add_material->id],'method'=>'GET','class'=>'inline-block')) !!}
                            {!! Form::button(trans('content.btn_delete'),array_merge(['class'=>'btn btn-danger','type'=>'submit'])) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <!-- ///////////////// -->
            @endif
            <div class="text-center">
                <img class="pre" src="{{ $prelection->sdoLectures->course->imagex }}" alt="...">
            </div>
            <h3>{{$prelection->name_trans}}</h3>
            <div>
                {!! $prelection->content_trans !!}
            </div>
        </div>
    </div>
@endsection