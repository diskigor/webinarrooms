{{-- на входе $block_users_courses --}}
<div class="row">
<div class="col-md-6">
    <div class="panel panel-primary">
        <div class="panel-heading">Курсы в системе</div>
        <div class="panel-content">
            <div class="col-md-12">
                <div class="progress">
                    <div class="progress-bar bg-success" 
                         role="progressbar" 
                         style="width: {{ 100 / ($block_users_courses->courses_free + $block_users_courses->courses_lecturer + $block_users_courses->courses_gilbo) * $block_users_courses->courses_free }}%" 
                         aria-valuenow="{{ 100 / ($block_users_courses->courses_free + $block_users_courses->courses_lecturer + $block_users_courses->courses_gilbo) * $block_users_courses->courses_free }}" 
                         aria-valuemin="0" aria-valuemax="100">
                        <span class="progress-text-admin">Открытая школа: <b>{{ $block_users_courses->courses_free }}</b></span>
                    </div>
                </div>
                <div class="progress">
                    <div class="progress-bar bg-success" 
                         role="progressbar" 
                         style="width: {{ 100 / ($block_users_courses->courses_free + $block_users_courses->courses_lecturer + $block_users_courses->courses_gilbo) * $block_users_courses->courses_lecturer }}%" 
                         aria-valuenow="{{ 100 / ($block_users_courses->courses_free + $block_users_courses->courses_lecturer + $block_users_courses->courses_gilbo) * $block_users_courses->courses_lecturer }}" 
                         aria-valuemin="0" aria-valuemax="100">
                        <span class="progress-text-admin">Курсы партнеров: <b>{{ $block_users_courses->courses_lecturer }}</b></span>
                    </div>
                </div>
                <div class="progress">
                    <div class="progress-bar bg-success" 
                         role="progressbar" 
                         style="width: {{ 100 / ($block_users_courses->courses_free + $block_users_courses->courses_lecturer + $block_users_courses->courses_gilbo) * $block_users_courses->courses_gilbo }}%" 
                         aria-valuenow="{{ 100 / ($block_users_courses->courses_free + $block_users_courses->courses_lecturer + $block_users_courses->courses_gilbo) * $block_users_courses->courses_gilbo }}" 
                         aria-valuemin="0" aria-valuemax="100">
                        <span class="progress-text-admin">Основные курсы: <b>{{ $block_users_courses->courses_gilbo }}</b></span>
                    </div>
                </div>
           </div>
        </div>
        <div class="panel-footer">сумма всех курсов: {{ $block_users_courses->cadet_price_all }} КЄЛ</div>
    </div>
</div>
<div class="col-md-6">
    <div class="panel panel-primary">
        <div class="panel-heading">Пользователи</div>

        <div class="panel-content">
            <div class="col-md-12">
                @foreach($block_users_courses->roles as $role)
                    @if($role->id != 1)
                    
                    <div class=" col-md-4">
                        <div class="col-md-12 tasks-chart-txt">{{ $role->description }}</div>
                        <div class="col-md-12 tasks-chart-data"><p>{{ $role->CountUsers }}</p></div>
                    </div>
                    @endif
                @endforeach
            </div>

            <div class="clearfix"></div>
        </div>


        <div class="panel-footer">
        </div>
    </div>
</div>
</div>
<div class="clearfix"></div>