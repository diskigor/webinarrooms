{{-- на входе $block_finance_activity --}}

<div class="panel panel-primary">
    <div class="panel-heading">Финансовая активность</div>
    
    <div class="panel-content" id="panel_content_daterange_block_finance_activity">
        <!--
        Пополнений пользователями :{{-- $block_finance_activity->add --}}<br/>
        
        Пополнений админом :{{-- $block_finance_activity->admin --}}<br/>
        Всего :{{-- $block_finance_activity->sum --}}<br/>
        <br/><br/>
        Покупки :{{-- $block_finance_activity->bay --}}<br/>
        -->
        <meta
            data-meta_add="{{ $block_finance_activity->add }}"
            data-meta_admin="{{ $block_finance_activity->admin }}"
            data-meta_sum="{{ $block_finance_activity->sum }}"
            data-meta_bay="{{ $block_finance_activity->bay }}"
            id="admin_finance_chart"
            />

    </div>
    
    
<div id="finance_container" style="height: 500px"></div>    
    
<script>

anychart.onDocumentReady(function () {
    redraw_finance_charts();
});

function redraw_finance_charts() {
    // set the chart type
    var chart = anychart.line();

    // create data
    var set_new_data = [
      ["Пользователи", $('#admin_finance_chart').data('meta_add')],
      ["Админ", $('#admin_finance_chart').data('meta_admin')],
      ["Всего", $('#admin_finance_chart').data('meta_sum')],
      ["Покупки", $('#admin_finance_chart').data('meta_bay')]
    ];
    
    // create a series, set the data and name
    var series = chart.column(set_new_data);
    series.name("Sales");

    // enable and configure labels on the series
    series.labels(true);
    series.labels().fontColor("green");
    series.labels().fontWeight(900);
    series.labels().format("{%value}");

    // set the chart title
    //chart.title("Labels (Series)");

    // set the titles of the axes
    chart.xAxis().title("");
    chart.yAxis().title("Euro");

    // set the container id
    chart.container("finance_container");

    // initiate drawing the chart
    chart.draw();
}
</script>


    <div class="panel-footer">
        <div class="calendarMain">
                <input type="text" id="daterange_block_finance_activity"/>
        </div>
    </div>
    
    
</div>

<style>
#finance_container {
    width: 100%;
    height: 100%;
    margin: 0;
    padding: 0;
}
</style>







                                        
<script type="text/javascript">
    $( window ).ready(function() {
        $('#daterange_block_finance_activity').daterangepicker({
            "applyClass": "btn-apply",
            "startDate": "{{ $start_day }}",
            "endDate": "{{ $end_day }}",
            "minDate": "01/01/2017",
            "autoUpdateInput": true,
            "alwaysShowCalendars": true,
            "showCustomRangeLabel": false,
            "linkedCalendars": false,
            "locale": {
                "format": "DD-MM-YYYY",
                "separator": " - ",
                "applyLabel": "Применить",
                "cancelLabel": "Отменить",
                "fromLabel": "от",
                "toLabel": "до",
                "customRangeLabel": "Другой диапазон",
                "weekLabel": "Неделя",
                "daysOfWeek": ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
                "monthNames": ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Остябрь", "Ноябрь", "Декабрь"],
                "firstDay": 1
            },
            "ranges": {
                'За сегодня': [moment(), moment()],
                'За вчера': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'За последние 7 дней': [moment().subtract(6, 'days'), moment()],
                'За последние 30 дней': [moment().subtract(29, 'days'), moment()],
                'За этот месяц': [moment().startOf('month'), moment().endOf('month')],
                'За предыдущий месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        });
    });
    
$('#daterange_block_finance_activity').on('apply.daterangepicker', function(ev, picker) {

    var drp = picker;
    var dstart  = drp.startDate.format('YYYY-MM-DD');
    var dend    = drp.endDate.format('YYYY-MM-DD');
    
    
    console.log(drp);
    console.log(dstart);
    console.log(dend);
    
    $('#panel_content_daterange_block_finance_activity')
            .load("{{ route('all_admin_financeActivity.index') }}" + "?start_day=" + dstart + "&end_day=" + dend + ' #panel_content_daterange_block_finance_activity');
    
    setTimeout(function()
   { 
                $("#finance_container").html('');
                $("#finance_container").empty();
                console.log('clear container');
                redraw_dima_charts();  
                $("#finance_container").html('');
                $("#finance_container").empty();
                console.log('clear container 2');
                redraw_finance_charts();  
            },1000);
    
});
</script>                                        