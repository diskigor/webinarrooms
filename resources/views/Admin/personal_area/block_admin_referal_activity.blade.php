{{-- на входе $block_referal_activity --}}

<div class="panel panel-primary">
    <div class="panel-heading">График реферальной активности. Сумма заработынх реферальных КЭЛ: {{ $block_referal_activity->sum }}</div>
    
    <div class="panel-content" id="panel_content_daterange_block_referal_activity">

        
        <meta
            data-meta_all_new="{{ $block_referal_activity->all }}"
            
            data-meta_first_pay="{{ $block_referal_activity->vstup }}"
            data-meta_no_pay="{{ $block_referal_activity->pret }}"
            id="admin_referal_chart"
            />
        
        
    </div>
    <div id="referal_container" style="height: 500px"></div> 
    <script>

anychart.onDocumentReady(function () {
    redraw_referal_charts();
});

function redraw_referal_charts() {
    // set the chart type
    var chart = anychart.line();

    // create data
    var set_new_data = [
      ["Всего реферелов", $('#admin_referal_chart').data('meta_all_new')],
      //["Всего сумаа рефералов, eur", $('#admin_referal_chart').data('meta_all_summ')],
      ["Оплатили вступительный взнос", $('#admin_referal_chart').data('meta_first_pay')],
      ["Не проводили оплату", $('#admin_referal_chart').data('meta_no_pay')]
    ];
    
    // create a series, set the data and name
    var series = chart.column(set_new_data);
    series.name("Sales");

    // enable and configure labels on the series
    series.labels(true);
    series.labels().fontColor("green");
    series.labels().fontWeight(900);
    series.labels().format("{%value}");

    // set the chart title
    //chart.title("Labels (Series)");

    // set the titles of the axes
    chart.xAxis().title("");
    chart.yAxis().title("Пользователей");

    // set the container id
    chart.container("referal_container");

    // initiate drawing the chart
    chart.draw();
}
</script>
    <div class="panel-footer">


        <div class="calendarMain">
                <input type="text" id="daterange_block_referal_activity"/>
        </div>
        
    </div>
    
    
</div>
<style>
#referal_container {
    width: 100%;
    height: 100%;
    margin: 0;
    padding: 0;
}
</style>
                                        
<script type="text/javascript">
    $( window ).ready(function() {
        $('#daterange_block_referal_activity').daterangepicker({
            "applyClass": "btn-apply",
            "startDate": "{{ $start_day }}",
            "endDate": "{{ $end_day }}",
            "minDate": "01/01/2017",
            "autoUpdateInput": true,
            "alwaysShowCalendars": true,
            "showCustomRangeLabel": false,
            "linkedCalendars": false,
            "locale": {
                "format": "DD-MM-YYYY",
                "separator": " - ",
                "applyLabel": "Применить",
                "cancelLabel": "Отменить",
                "fromLabel": "от",
                "toLabel": "до",
                "customRangeLabel": "Другой диапазон",
                "weekLabel": "Неделя",
                "daysOfWeek": ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
                "monthNames": ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Остябрь", "Ноябрь", "Декабрь"],
                "firstDay": 1
            },
            "ranges": {
                'За сегодня': [moment(), moment()],
                'За вчера': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'За последние 7 дней': [moment().subtract(6, 'days'), moment()],
                'За последние 30 дней': [moment().subtract(29, 'days'), moment()],
                'За этот месяц': [moment().startOf('month'), moment().endOf('month')],
                'За предыдущий месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        });
    });
    
$('#daterange_block_referal_activity').on('apply.daterangepicker', function(ev, picker) {

    var drp = picker;
    var dstart  = drp.startDate.format('YYYY-MM-DD');
    var dend    = drp.endDate.format('YYYY-MM-DD');
    
    
    console.log(drp);
    console.log(dstart);
    console.log(dend);
    
    $('#panel_content_daterange_block_referal_activity')
            .load("{{ route('all_admin_referalsActivity.index') }}" + "?start_day=" + dstart + "&end_day=" + dend + ' #panel_content_daterange_block_referal_activity');
    setTimeout(function()
   { 
                $("#referal_container").html('');
                $("#referal_container").empty();
                console.log('clear container');
                redraw_dima_charts();  
                $("#referal_container").html('');
                $("#referal_container").empty();
                console.log('clear container 2');
                redraw_referal_charts();  
            },1000);
});
</script>                                        