{{-- на входе $statistic_curses , $statistic_sum --}}

<div class="panel panel-primary">
    <div class="panel-heading">Детальная татистика по продаже курсов</div>
    
    <div class="panel-content" id="panel_content_daterange_block_fullfinance_activity">
        
        @foreach($statistic_curses as $course)
            Название курса :{{ $course->name }}<br/>
            Роялти :{{ $course->sum }}<br/>
            <br/>
        
        @endforeach
        
        <hr/>
        
        Сумма :{{ $statistic_sum }}<br/>
        


    </div>
    


    <div class="panel-footer">
        <div class="calendarMain">
                <input type="text" id="daterange_block_fullfinance_activity"/>
        </div>
    </div>
    
    
</div>
                                   
<script type="text/javascript">
    $( window ).ready(function() {
        $('#daterange_block_fullfinance_activity').daterangepicker({
            "applyClass": "btn-apply",
            "startDate": "{{ $start_day }}",
            "endDate": "{{ $end_day }}",
            "minDate": "01/01/2017",
            "autoUpdateInput": true,
            "alwaysShowCalendars": true,
            "showCustomRangeLabel": false,
            "linkedCalendars": false,
            "locale": {
                "format": "DD-MM-YYYY",
                "separator": " - ",
                "applyLabel": "Применить",
                "cancelLabel": "Отменить",
                "fromLabel": "от",
                "toLabel": "до",
                "customRangeLabel": "Другой диапазон",
                "weekLabel": "Неделя",
                "daysOfWeek": ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
                "monthNames": ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Остябрь", "Ноябрь", "Декабрь"],
                "firstDay": 1
            },
            "ranges": {
                'За сегодня': [moment(), moment()],
                'За вчера': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'За последние 7 дней': [moment().subtract(6, 'days'), moment()],
                'За последние 30 дней': [moment().subtract(29, 'days'), moment()],
                'За этот месяц': [moment().startOf('month'), moment().endOf('month')],
                'За предыдущий месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        });
    });
    
$('#daterange_block_fullfinance_activity').on('apply.daterangepicker', function(ev, picker) {

    var drp = picker;
    var dstart  = drp.startDate.format('YYYY-MM-DD');
    var dend    = drp.endDate.format('YYYY-MM-DD');
    
    
    console.log(drp);
    console.log(dstart);
    console.log(dend);
    
    $('#panel_content_daterange_block_fullfinance_activity')
            .load("{{ route('all_lecturer_financeActivity.create') }}" + "?start_day=" + dstart + "&end_day=" + dend + ' #panel_content_daterange_block_fullfinance_activity');
    
});
</script>                                        