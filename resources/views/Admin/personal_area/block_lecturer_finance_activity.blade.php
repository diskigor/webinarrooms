{{-- на входе $block_finance_activity --}}

<div class="panel panel-primary">
    <div class="panel-heading">Общая финансовая статистика</div>
    
    <div class="panel-content" id="panel_content_daterange_block_finance_activity">
        <!--
        
        
        Роялти от курсов :{{-- $block_finance_activity->curses --}}<br/>
        
        Роялти от вебинаров :{{-- $block_finance_activity->vebinars --}}<br/>
        Всего :{{-- $block_finance_activity->sum --}}<br/>
        <br/><br/>
        -->
        <meta
            data-meta_curses="{{ $block_finance_activity->curses }}"
            data-meta_vebinars="{{ $block_finance_activity->vebinars }}"
            data-meta_sum="{{ $block_finance_activity->sum }}"
            id="only_block_lecturer_finance_activity"
            />

    </div>
    
    
<div id="container" style="height: 500px"></div>    
    
<script>

anychart.onDocumentReady(function () {
    redraw_dima_charts();
});

function redraw_dima_charts() {
    // set the chart type
    var chart = anychart.line();

    // create data
    var set_new_data = [
      ["Курсы", $('#only_block_lecturer_finance_activity').data('meta_curses')],
      ["Вебинары", $('#only_block_lecturer_finance_activity').data('meta_vebinars')],
      ["Всего", $('#only_block_lecturer_finance_activity').data('meta_sum')],    ];
    
    // create a series, set the data and name
    var series = chart.column(set_new_data);
    series.name("Sales");

    // enable and configure labels on the series
    series.labels(true);
    series.labels().fontColor("green");
    series.labels().fontWeight(900);
    series.labels().format("{%value}");

    // set the chart title
    chart.title("");

    // set the titles of the axes
    chart.xAxis().title("");
    chart.yAxis().title("КЭЛ");

    // set the container id
    chart.container("container");

    // initiate drawing the chart
    chart.draw();
}
</script>


    <div class="panel-footer">
        <div class="calendarMain">
                <input type="text" id="daterange_block_finance_activity"/>
        </div>
    </div>
    
    
</div>

<style>
#container {
    width: 100%;
    height: 100%;
    margin: 0;
    padding: 0;
}
</style>







                                        
<script type="text/javascript">
    $( window ).ready(function() {
        $('#daterange_block_finance_activity').daterangepicker({
            "applyClass": "btn-apply",
            "startDate": "{{ $start_day }}",
            "endDate": "{{ $end_day }}",
            "minDate": "01/01/2017",
            "autoUpdateInput": true,
            "alwaysShowCalendars": true,
            "showCustomRangeLabel": false,
            "linkedCalendars": false,
            "locale": {
                "format": "DD-MM-YYYY",
                "separator": " - ",
                "applyLabel": "Применить",
                "cancelLabel": "Отменить",
                "fromLabel": "от",
                "toLabel": "до",
                "customRangeLabel": "Другой диапазон",
                "weekLabel": "Неделя",
                "daysOfWeek": ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
                "monthNames": ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Остябрь", "Ноябрь", "Декабрь"],
                "firstDay": 1
            },
            "ranges": {
                'За сегодня': [moment(), moment()],
                'За вчера': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'За последние 7 дней': [moment().subtract(6, 'days'), moment()],
                'За последние 30 дней': [moment().subtract(29, 'days'), moment()],
                'За этот месяц': [moment().startOf('month'), moment().endOf('month')],
                'За предыдущий месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        });
    });
    
$('#daterange_block_finance_activity').on('apply.daterangepicker', function(ev, picker) {

    var drp = picker;
    var dstart  = drp.startDate.format('YYYY-MM-DD');
    var dend    = drp.endDate.format('YYYY-MM-DD');
    
    
    console.log(drp);
    console.log(dstart);
    console.log(dend);
    
    $('#panel_content_daterange_block_finance_activity')
            .load("{{ route('all_lecturer_financeActivity.index') }}" + "?start_day=" + dstart + "&end_day=" + dend + ' #panel_content_daterange_block_finance_activity');
    
    setTimeout(function()
   { 
                $("#container").html('');
                $("#container").empty();
                console.log('clear container');
                redraw_dima_charts();  
                $("#container").html('');
                $("#container").empty();
                console.log('clear container 2');
                redraw_dima_charts();  
            },1000);
    
});
</script>                                        