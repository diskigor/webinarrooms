@extends('layouts.admin')
@section('content')
<div class="col-md-12">
    <header class="big-header text-center">Форма редактирования курса</header>
    {!! Form::open(array('route'=>['curses.update',$edit_curse->id], 'method'=>'PUT','enctype'=>'multipart/form-data')) !!}
    <div class="panel">
        <div class="form-group">
            <img class="pre" src="{{ $edit_curse->imagex }}">
        </div>
        <div class="form-group">
            <div class="preview">
                <div id="preview1"></div>
                <input type="button" id="reset1" value="Сбросить" class="btn btn-info reset"/>
            </div>
        </div>
        <label for="image" class="control-label">{{trans('curses.curse_image')}} <small>(1140х400)</small></label>
        {!! Form::file('image',['accept'=>"image/jpeg,image/png,image/gif"])!!}

        <div class="form-group">
            {!! Form::label('status','Включено/выключено?',['class'=>'control-label']) !!}
            {!! Form::checkbox('status', 1,$edit_curse->status) !!}
        </div>
        
        <div class="form-group">
            {!! Form::label('ordered','Сортировка/последовательность вывода курсов',['class'=>'control-label']) !!}
            {!! Form::number('ordered',$edit_curse->ordered,['class'=>'form-control','min'=>0, 'max'=>10000]) !!}
        </div>

        <div class="form-group">
            {!! Form::label('discount','Скидка %',['class'=>'control-label']) !!}
            {!! Form::number('discount',$edit_curse->discount,['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('slug_curse','Slug для SEO',['class'=>'control-label']) !!}
            {!! Form::text('slug_curse',$edit_curse->slug,array_merge([
            'class'=>'form-control']))!!}
        </div>
        <div class="form-group">
            {!! Form::label('parent_buy_id','Зависимость покупки данного курса от покупки другого курса',['class'=>'control-label']) !!}
            <select name="parent_buy_id" class="form-control">
                <option value="">-- независимая покупка --</option>
                @foreach ($list_curs as $parent_curs)
                <option 
                    style="color: black;"
                    value="{{ $parent_curs->id }}" {{ $parent_curs->id == $edit_curse->parent_buy_id ?'selected="selected"':''}}>{{ $parent_curs->trans_curse->first()->name_trans }}</option>
                @endforeach
            </select>
        </div>


        @foreach($groups as $group)
        @if($edit_curse->groupPrice($group->id) != NULL)
        <div class="form-group">
            {!! Form::label('group_price['.$edit_curse->groupPrice($group->id)->group_id.']','Цена для пользователей группы '.$edit_curse->groupPrice($group->id)->group->name,['class'=>'control-label']) !!}
            {!! Form::number('group_price['.$edit_curse->groupPrice($group->id)->group_id.']',$edit_curse->groupPrice($group->id)->price,['class'=>'form-control','required']) !!}
        </div>
        @else
        <div class="form-group">
            {!! Form::label('group_price['.$group->id.']','Цена для пользователей группы '.$group->name,['class'=>'control-label']) !!}
            {!! Form::number('group_price['.$group->id.']',null,['class'=>'form-control','required']) !!}
        </div>
        @endif
        @endforeach
        
        @if(Auth::user()->isAdmin())
            <hr>
            <div class="form-group">
                {!! Form::label('front_status','Отображать на фронте?',['class'=>'control-label']) !!}
                {!! Form::checkbox('front_status', 1,$edit_curse->front_status) !!}
            </div>
            <div class="form-group">
                {!! Form::label('front_hight','Высота для фронта',['class'=>'control-label']) !!}
                {!! Form::number('front_hight',$edit_curse->front_hight,['class'=>'form-control']) !!}
            </div>
            <hr>    
        @endif
        
        
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            @foreach($edit_curse->getTransCurseEdit->reverse() as $curse)
            @if($loop->first)
            <li role="presentation" class="active">
                <a href="#{{$curse->lang_local.'-first'}}" aria-controls="{{$curse->lang_local.'-first'}}"
                   role="tab"
                   data-toggle="tab">{{$curse->lang_local}}</a>
            </li>
            @else
            <li role="presentation">
                <a href="#{{$curse->lang_local}}" aria-controls="{{$curse->lang_local}}" role="tab"
                   data-toggle="tab">{{$curse->lang_local}}</a>
            </li>
            @endif
            @endforeach
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            @foreach($edit_curse->getTransCurseEdit->reverse() as $curse )
            @if($loop->first)
            <div role="tabpanel" class="tab-pane active fade in" id="{{$curse->lang_local.'-first'}}">
                <div class="form-group">
                    {!! Form::label('name_'.$curse->lang_local,trans('curses.name_curse'),['class'=>'control-label']) !!}
                    {!! Form::text('name_'.$curse->lang_local,$curse->name_trans,array_merge([
                    'class'=>'form-control',
                    'required']))!!}
                </div>
                <div class="form-group">
                    {!! Form::label('short_desc_'.$curse->lang_local,trans('curses.short_description'),['class'=>'control-label']) !!}
                    {!! Form::text('short_desc_'.$curse->lang_local,$curse->short_desc_trans,array_merge([
                    'class'=>'form-control',
                    'maxlength'=>'190',
                    'required']))!!}
                </div>
                <div class="form-group">
                    {!! Form::label('description_'.$curse->lang_local,trans('curses.desc_course'),['class'=>'control-label']) !!}
                    {!! Form::textarea('description_'.$curse->lang_local,$curse->description_trans,['class'=>'form-control']) !!}
                </div>
                
                @if(Auth::user()->isAdmin())
                    <div class="form-group">
                        {!! Form::label('front_descr_'.$curse->lang_local,'Описание для фронтаqwerty',['class'=>'control-label']) !!}
                        {!! Form::textarea('front_descr_'.$curse->lang_local,$curse->front_descr,['class'=>'form-control editable']) !!}
                    </div>
                @endif
            </div>
            @else
            <div role="tabpanel" class="tab-pane fade" id="{{$curse->lang_local}}">
                <div class="form-group">
                    {!! Form::label('name_'.$curse->lang_local,trans('curses.name_curse').'  '.$curse->lang_local,['class'=>'control-label']) !!}
                    {!! Form::text('name_'.$curse->lang_local,$curse->name_trans,array_merge([
                    'class'=>'form-control',
                    'required']))!!}
                </div>
                <div class="form-group">
                    {!! Form::label('short_desc_'.$curse->lang_local,trans('curses.short_description').'  '.$curse->lang_local,['class'=>'control-label']) !!}
                    {!! Form::text('short_desc_'.$curse->lang_local,$curse->short_desc_trans,array_merge([
                    'class'=>'form-control',
                    'maxlength'=>'190',
                    'required']))!!}
                </div>
                <div class="form-group">
                    {!! Form::label('description_'.$curse->lang_local,trans('curses.desc_course').'  '.$curse->lang_local,['class'=>'control-label']) !!}
                    {!! Form::textarea('description_'.$curse->lang_local,$curse->description_trans,['class'=>'form-control']) !!}
                </div>
                
                @if(Auth::user()->isAdmin())
                    <div class="form-group">
                        {!! Form::label('front_descr_'.$curse->lang_local,'Описание для фронта',['class'=>'control-label']) !!}
                        {!! Form::textarea('front_descr_'.$curse->lang_local,$curse->front_descr,['class'=>'form-control']) !!}
                    </div>
                @endif
            </div>
            @endif
            @endforeach

            <div class="form-group">
                {!! Form::submit(trans('curses.button_save_course'),['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
            <a href="{{route('curses.index')}}" class="btn btn-info">Назад</a>
        </div>
    </div>
</div>
@endsection

@push('stack_scripts')
<script src="{{asset('js/imagepreview.js')}} "></script>
<script type="text/javascript">
$(function () {
    $('#preview1').imagepreview({
        input: '[name="image"]',
        reset: '#reset1',
        preview: '#preview1'
    });
});
</script>
@endpush