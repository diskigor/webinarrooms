@extends('layouts.admin')
@section('content')
    <div class="col-md-12">
        <header class="big-header text-center">{{trans('curses.curses_title')}}</header>
        <a href="{{route('curses.create')}}" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>
            {{trans('curses.add_curses_button')}}</a>
        <hr>
        <div class="row">
            @foreach($courses as $cours)
                @foreach($cours->trans_curse as $lang_course)
                        <figure class="col-md-4">
                            <div class="thumbnail courses">
                            
                                <img class="image" src="{{ $lang_course->curse->imagex }}">
                                <div>Кол - во лекций в курсе: <label class="label label-success">{{count($lang_course->curse->lectures)}}</label></div>
                                <figcaption>
                                    <h3>{{mb_strimwidth($lang_course->name_trans,0,60,'...')}}</h3>
                             
                                    <p>Скидка : <label class=" label label-warning">{{$lang_course->curse->discount}}</label>
                                        @if (Auth::user()->isAdmin())
                                            <span class="pull-right">Сортировка : <label class=" label label-info">{{ $cours->ordered }}</label></span>
                                        @endif
                                    </p>
                                    <p class="hidden"> <label>Создан :</label> {{$lang_course->created_at}}</p>
                                    <div class="short">{{mb_strimwidth($lang_course->short_desc_trans,0,100,'...')}}</div>
                                </figcaption>
                                <div class="hover-nav">
                                    <a href="#" class="btn btn-success" data-toggle="modal"
                                       data-target="{{'#modal-'.$lang_course->curse->id}}">
                                        <i class="fa fa-eye"></i>
                                        <span class="hidden">{{trans('news.view')}}</span>
                                    </a>
                                    @if($lang_course->curse->id!=1)
                                        <!-- привентивная защита хранилища -->
                                        {!! Form::open(array('route'=>['curses.edit',$lang_course->curse->id],'method'=>'GET','class'=>'inline-block')) !!}
                                        {!! Form::button('<i class="fas fa-pencil-alt"></i>',['class'=>'btn btn-info','type'=>'submit']) !!}
                                        {!! Form::close() !!}

                                        {!! Form::open(array('route'=>['curses.destroy',$lang_course->curse->id],'method'=>'DELETE','class'=>'inline-block')) !!}
                                        {!! Form::button('<i class="fa fa-times"></i>',['class'=>'btn btn-danger','type'=>'submit','onclick'=>'return checkDelete()']) !!}
                                        {!! Form::close() !!}
                                    @endif
                                </div>
                                <!-- Modal -->
                                <div class="modal fade" id="{{'modal-'.$lang_course->curse->id}}" tabindex="-1"
                                     role="dialog">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close"><span aria-hidden="true">&times;</span>
                                                </button>
                                                <h4 class="modal-title">{{$lang_course->name_trans}}</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="image">
                                                    <img width="900" height="500"
                                                         src="{{ $lang_course->curse->imagex }}"></i>
                                                </div>
                                                {!!  $lang_course->description_trans!!}
                                                <div>
                                                    @if(count($lang_course->curse->lectures)!=0)
                                                    <span>Лекции</span>
                                                    <ol>
                                                        @foreach($lang_course->curse->lectures as $lecture)
                                                            <li>
                                                                @if($lecture->sdoLectureTrans())
                                                                <a href="{{route('prelection.show',$lecture->id)}}">{{$lecture->sdoLectureTrans()->name_trans}}</a>
                                                                    @endif
                                                            </li>
                                                        @endforeach
                                                    @else
                                                        <div>В данном курсе нет лекци</div>
                                                    @endif
                                                    </ol>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- / Modal -->
                            </div>
                        </figure>
                @endforeach
            @endforeach
        </div>
        {{$courses->links()}}
    </div>
@endsection

@section('script')
    <script language="JavaScript" type="text/javascript">
        function checkDelete() {
            return confirm('Вы уверены что хотите удалить данный курс?');
        }
    </script>
@endsection


