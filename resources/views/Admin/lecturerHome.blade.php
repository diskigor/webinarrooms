@extends('layouts.admin')

@push('stack_head_script')
    <script type="text/javascript" src="{{ asset('js/moment/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/datarangepicker/daterangepicker.js') }}"></script>
    <script src="{{asset('js/jquery.circliful.min.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('js/datarangepicker/daterangepicker.css') }}" />
    <script src="https://cdn.anychart.com/releases/8.3.0/js/anychart-base.min.js" type="text/javascript"></script>
@endpush

@section('content')
    <?php $title_page='Личный кабинет'; ?>

<div class="col-md-12 first-admin-home-block">

            <div class="col-lg-4 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-envelope fa-3x" aria-hidden="true"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{count(Auth::user()->getMessage())}}</div>
                                <div>Новых сообщения</div>
                            </div>
                        </div>
                    </div>
                    <a href="{{route('message.index')}}">
                        <div class="panel-footer">
                            <span class="pull-left">Посмотреть</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-users fa-3x" aria-hidden="true"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">-------</div>
                                <div>Рефералов</div>
                            </div>
                        </div>
                    </div>
                    <a href="{{route('refsystem.index')}}">
                        <div class="panel-footer">
                            <span class="pull-left">Весь список</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="clearfix visible-md"></div>
            <div class="col-lg-4 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-book fa-3x" aria-hidden="true"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">----</div>
                                <div>Купленых курсов</div>
                            </div>
                        </div>
                    </div>
                    <a href="{{route('orders_statistics.index')}}">
                        <div class="panel-footer">
                            <span class="pull-left">Архив курсов</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>

            </div>
                     
            <div class="clearfix"></div>
            <div class="col-md-12" id="block_financeActivityController"></div>
            
            <div class="clearfix"></div>
            <div class="col-md-12" id="block_fullFinanceActivityController"></div>
            

            


    </div>
@endsection

@push('stack_scripts')
        <script>
            $('#block_financeActivityController').load("{{ route('all_lecturer_financeActivity.index') }}");
            $('#block_fullFinanceActivityController').load("{{ route('all_lecturer_financeActivity.create') }}");
        </script>
@endpush


