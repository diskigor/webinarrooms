@extends('Admin.app')
@section('content')
    <div class="col-md-12">
        <div class="row">
@if(isset($language_logo))
            <h1>{{trans('language.title_edit_language')}}</h1>

            <div class="panel">
            {!! Form::open(array('route'=>['language.update',$language_logo->id],'method'=>'PUT','enctype'=>'multipart/form-data')) !!}
            <div class="input-group">
                <img src="{{asset($language_logo->image)}}">
                {!! Form::label('image',trans('language.logo').'(20x16)',['class'=>'control-label']) !!}
                {!! Form::file('image') !!}
            </div>
            <br>
            {!! Form::button(trans('content.save'),['class'=>'btn btn-primary','type'=>'submit']) !!}
            {!! Form::close() !!}
            </div>
    @else
            <h1>{{trans('language.title_edit_language')}}</h1>
            <div class="panel">
            {!! Form::open(array('route'=>['language.store'],'method'=>'POST','enctype'=>'multipart/form-data')) !!}
            <div class="input-group">
                {!! Form::label(trans('language.logo'),null,['class'=>'control-label']) !!}
                {!! Form::file('image') !!}
            </div>
            <input type="hidden" name="name" value="{{$language}}">
            <br>
                {!! Form::button(trans('content.save'),['class'=>'btn btn-primary','type'=>'submit']) !!}
                {!! Form::close() !!}
            </div>
            @endif
        </div>
    </div>

@endsection