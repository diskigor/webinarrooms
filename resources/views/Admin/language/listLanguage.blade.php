@extends('Admin.app')
@section('content')
    @if(isset($languages))
        <!-- Contextual Classes -->
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2 class="text-center">
                                {{trans('language.title_language')}}
                            </h2>
                            <hr>
                        </div>
                        <div class="body panel">
                            <table class="table table-responsive">
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>{{trans('language.name')}}</th>
                                    <th>{{trans('language.code')}}</th>
                                    <th>{{trans('language.logo')}}</th>
                                    <th>{{trans('language.action')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1;?>
                                @foreach($languages as $language)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$language['name']}}</td>
                                        <td>{{$language['regional']}}</td>
                                        <td>
                                            @if(isset($lang_logo))
                                                @foreach($lang_logo as $logo)
                                                    @if($logo->name === $language['name'])
                                                        <img src="{{asset($logo->image)}}">
                                                    @endif
                                                @endforeach
                                            @endif
                                        </td>
                                        <td>
                                            {!! Form::open(array('route'=>['language.edit',$language['name']],'method'=>'GET')) !!}
                                            {!! Form::button('<i class="fas fa-pencil-alt"></i>',['class'=>'btn btn-info','type'=>'submit']) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Contextual Classes -->
    @endif
@endsection