@extends('layouts.admin')
@section('content')
    <div class="col-md-12">
        <h1 class="text-center">Форма создания мероприятия или семинара</h1>
        {!! Form::open(array('route'=>'admin_events.store', 'method'=>'POST','enctype'=>'multipart/form-data')) !!}
        <div class="form-group">
            <div class="preview">
                <div id="preview1"></div>
                <input type="button" id="reset1" value="Сбросить" class="btn btn-info reset"/>
            </div>
            <label for="image_link" class="control-label">{{trans('news.news_image')}} <small>(1140х400)</small></label>
            {!! Form::file('image_link',['accept'=>"image/jpeg,image/png,image/gif",'class'=>''])!!}
        </div>
        <div class="form-group">
            {!! Form::label('slug',"Slug",['class'=>'control-label']) !!}
            {!! Form::text('slug',null,['class'=>'form-control','placeholder'=>'slug_prednaznashen_dlia_seo','required']) !!}
        </div>
        <div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <?php $i = 0;?>
                @foreach($arr_local as $lang)
                    <?php $i++;?>
                    @if($i===1)
                        <li role="presentation" class="active">
                            <a href="#{{$lang.'-first'}}" aria-controls="{{$lang.'-first'}}" role="tab"
                               data-toggle="tab">{{$lang}}</a>
                        </li>
                    @else
                        <li role="presentation">
                            <a href="#{{$lang}}" aria-controls="{{$lang}}" role="tab" data-toggle="tab">{{$lang}}</a>
                        </li>
                    @endif
                @endforeach
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <?php $c = 0;?>
                @foreach($arr_local as $key=>$lang_content)
                    <?php $c++;?>
                    @if($c===1)
                        <div role="tabpanel" class="tab-pane active fade in" id="{{$lang_content.'-first'}}">
                            <div class="form-group">
                                {!! Form::label('name_'.$key,trans('news.title_news'),['class'=>'control-label']) !!}
                                {!! Form::text('name_'.$key,null,array_merge([
                                'class'=>'form-control',
                                'placeholder'=>trans('news.placeholder_title'),
                                'required']))!!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('description_'.$key,"Краткое описание",['class'=>'control-label']) !!}
                                {!! Form::textarea('description_'.$key,null,['class'=>'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('text_'.$key,trans('news.enter_article'),['class'=>'control-label']) !!}
                                {!! Form::textarea('text_'.$key,null,['class'=>'form-control']) !!}
                            </div>
                            @else
                                <div role="tabpanel" class="tab-pane fade" id="{{$lang_content}}">

                                    <div class="form-group">
                                        {!! Form::label('name_'.$key,trans('news.title_news').'  '.$lang_content,['class'=>'control-label']) !!}
                                        {!! Form::text('name_'.$key,null,array_merge([
                                        'class'=>'form-control',
                                        'placeholder'=>trans('news.placeholder_title'),
                                        'required']))!!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('description_'.$key,"Краткое описание".'  '.$lang_content,['class'=>'control-label']) !!}
                                        {!! Form::textarea('description_'.$key,null,['class'=>'form-control']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('text_'.$key,trans('news.enter_article').'  '.$lang_content,['class'=>'control-label']) !!}
                                        {!! Form::textarea('text_'.$key,null,['class'=>'form-control']) !!}
                                    </div>
                                    @endif
                                </div>
                                @endforeach
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Сохранить мероприятие',['class'=>'btn btn-success']) !!}
                        </div>
                        {!! Form::close() !!}
                        <a href="{{route('admin_events.index')}}" class="btn btn-info">{{trans('content.back')}}</a>
            </div>
        </div>
    </div>
@section('script')
    <script src="{{asset('js/imagepreview.js')}} "></script>
    <script type="text/javascript">
        $(function () {
            $('#preview1').imagepreview({
                input: '[name="image_link"]',
                reset: '#reset1',
                preview: '#preview1'
            });
        });
    </script>
@endsection
@endsection