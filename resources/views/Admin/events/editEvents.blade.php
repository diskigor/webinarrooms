@extends('layouts.admin')
@section('content')
<div class="col-md-12">
    <header class="big-header text-center">Форма редактирования мероприятия или семинара</header>
    <div class="panel">
        {!! Form::open(array('route'=>['admin_events.update',$event->id], 'method'=>'PUT','enctype'=>'multipart/form-data')) !!}
        <div class="form-group">
            <img class="pre" src="{{ $event->image }}">
        </div>
        <div class="form-group">
            <div class="preview">
                <div id="preview1"></div>
                <input type="button" id="reset1" value="Сбросить" class="btn btn-info reset"/>
            </div>
        </div>
        <div class="form-group">
            <label for="image_link" class="control-label">{{trans('news.news_image')}} <small>(1140х400)</small></label>
            {!! Form::file('image_link',['class'=>''])!!}
        </div>
        <div class="form-group">
            {!! Form::label('slug',"Slug",['class'=>'control-label']) !!}
            {!! Form::text('slug',$event->slug,['class'=>'form-control','required']) !!}
        </div>
        <div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                @foreach($event->relationForEditEvent->reverse() as $trans_event)
                @if($loop->first)
                <li role="presentation" class="active">
                    <a href="#{{$trans_event->lang_local.'-first'}}"
                       aria-controls="{{$trans_event->lang_local.'-first'}}"
                       role="tab"
                       data-toggle="tab">{{$trans_event->lang_local}}</a>
                </li>
                @else
                <li role="presentation">
                    <a href="#{{$trans_event->lang_local}}"
                       aria-controls="{{$trans_event->lang_local}}"
                       role="tab" data-toggle="tab">{{$trans_event->lang_local}}</a>
                </li>
                @endif
                @endforeach
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                @foreach($event->relationForEditEvent->reverse() as $trans_event)
                    @if($loop->first)
                    <div role="tabpanel" class="tab-pane active fade in" id="{{$trans_event->lang_local.'-first'}}">
                        <div class="form-group">
                            {!! Form::label('name_'.$trans_event->lang_local,trans('news.title_news'),['class'=>'control-label']) !!}
                            {!! Form::text('name_'.$trans_event->lang_local,$trans_event->name,array_merge([
                            'class'=>'form-control',
                            'placeholder'=>trans('news.placeholder_title'),
                            'required']))!!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('description_'.$trans_event->lang_local,"Краткое описание",['class'=>'control-label']) !!}
                            {!! Form::textarea('description_'.$trans_event->lang_local,$trans_event->description,['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('text_'.$trans_event->lang_local,trans('news.enter_article'),['class'=>'control-label']) !!}
                            {!! Form::textarea('text_'.$trans_event->lang_local,$trans_event->text,['class'=>'form-control']) !!}
                        </div>
                    </div>
                    @else
                    <div role="tabpanel" class="tab-pane fade" id="{{$trans_event->lang_local}}">
                        <div class="form-group">
                            {!! Form::label('name_'.$trans_event->lang_local,trans('news.title_news').'  '.$trans_event->lang_local,['class'=>'control-label']) !!}
                            {!! Form::text('name_'.$trans_event->lang_local,$trans_event->name,array_merge([
                            'class'=>'form-control',
                            'placeholder'=>trans('news.placeholder_title'),
                            'required']))!!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('description_'.$trans_event->lang_local,"Краткое описание".'  '.$trans_event->lang_local,['class'=>'control-label']) !!}
                            {!! Form::textarea('description_'.$trans_event->lang_local,$trans_event->description,['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('text_'.$trans_event->lang_local,trans('news.enter_article').'  '.$trans_event->lang_local,['class'=>'control-label']) !!}
                            {!! Form::textarea('text_'.$trans_event->lang_local,$trans_event->text,['class'=>'form-control']) !!}
                        </div>
                    </div>
                    @endif
                @endforeach
                
                <div class="form-group">
                    {!! Form::submit(trans('news.save_button'),['class'=>'btn btn-success']) !!}
                    <a href="{{route('admin_events.index')}}" class="btn btn-info">{{trans('content.back')}}</a>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('stack_scripts')
<script src="{{asset('js/imagepreview.js')}} "></script>
<script type="text/javascript">
$(function () {
$('#preview1').imagepreview({
    input: '[name="image_link"]',
    reset: '#reset1',
    preview: '#preview1'
});
});
</script>
@endpush