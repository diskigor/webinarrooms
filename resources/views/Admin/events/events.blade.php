@extends('layouts.admin')
@section('content')
    <div class="col-md-12">
        <header class="big-header text-center">Мероприятия и семинары</header>

        <a href="{{route('admin_events.create')}}" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>
            Добавить мероприятие </a>
<br><br>
        <div class="row">
            @if(!$events->isEmpty())
                @foreach($events as $doings)
                    <figure class="col-sm-4">
                        <div class="thumbnail post_news">
                            <img class="image" src="{{ $doings->image }}">
                            <figcaption>
                                <h3>{{ $doings->lang?$doings->lang->name:'' }}</h3>
                                <p><label>{{trans('news.created_news')}}:</label> {{$doings->created_at}}</p>
                                <p>{{ mb_strimwidth (strip_tags($doings->lang?$doings->lang->description:''),0,100,'...') }}</p>
                            </figcaption>
                            <div class="hover-nav">
                                <a href="#" class="btn btn-success" data-toggle="modal"
                                   data-target="{{'#modal-'.$doings->id}}">
                                    <i class="fa fa-eye"></i>
                                    <span class="hidden">{{trans('news.view')}}</span>
                                </a>
                                {!! Form::open(array('route'=>['admin_events.edit',$doings->id],'method'=>'GET','class'=>'inline-block')) !!}
                                {!! Form::button('<i class="fas fa-pencil-alt"></i>',['class'=>'btn btn-info','type'=>'submit']) !!}
                                {!! Form::close() !!}
                                {!! Form::open(array('route'=>['admin_events.destroy',$doings->id], 'method'=>'DELETE','class'=>'inline-block')) !!}
                                {!! Form::button('<i class="fa fa-times"></i>',['class'=>'btn btn-danger','type'=>'submit']) !!}
                                {!! Form::close() !!}
                            </div>
                            {{--<!-- Modal -->--}}
                            <div class="modal fade" id="{{'modal-'.$doings->id}}" tabindex="-1" role="dialog">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">{{ $doings->lang?$doings->lang->name:'' }}</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="image">
                                                <img width="900" height="500" src="{{ $doings->image }}">
                                            </div>
                                            {!! $doings->lang?$doings->lang->text:'' !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </figure>
                @endforeach
            @endif
        </div>
    </div>
@endsection