@extends('Admin.app')
@section('content')
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                <div class="panel text-center">
                    <h1 class="text-center">Пополнить баланс</h1>
                    <p>Добро пожаловать {{Auth::user()->name}}</p>
                    <div id="body_user_add_balance"></div>

                    
                    
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
<script>
    $(document).ready(function ($) {
        $('#body_user_add_balance').load('{{ route("get_add_balance_form")}}');
    });
    
    $(document).on('submit', '#form_add_balance', function (e) {
        e.preventDefault();
        var tforma = this;
        $.ajax({
            type: "GET",
            url: "{{ route('get_add_balance_form') }}",
            data: $(tforma).serialize(),
            success: function (msg) {
                if (msg.errors !== undefined) {
                    swal(msg.errors);
                    return;
                }
                if (msg.forms !== undefined) {
                    $(msg.forms).submit();
                    return;
                }
                $('#body_user_add_balance').html(msg);
            }
        });
    });

</script>
@endsection

