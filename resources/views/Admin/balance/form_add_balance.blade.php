{{--  $need_summ, $ers  --}}
<form class="inline-block" action="#" method="POST" id="form_add_balance">
    <input type="hidden" name="action" value="get_payment_form"/>
    <div class="form-group">
        <label>Введите сумму</label> <i class="fa fa-eur" aria-hidden="true"></i>
        <input type="number" 
               value="{{$need_summ}}" 
               name="summa" 
               minlength="0"
               min="1"
               class="form-control"  required>
        
        @if($ers)
            <p style="color: red">* {{ $ers }}</p>
        @endif
        
        @if($need_summ) 
            <p>
                @if(Auth::user()->checkEntrance())
                    Нужно внести от : {{ $need_summ }} <i class="fa fa-eur" aria-hidden="true"></i>
                @else
                    Для первого взноса нужно внести от : {{ $need_summ }} <i class="fa fa-eur" aria-hidden="true"></i>
                @endif
            </p>
        @endif

    </div>
    <button type="submit" class="btn btn-success">Перейти к оплате</button>
</form>