                    <form class="inline-block" id="payment" name="payment" method="post" action="https://sci.interkassa.com/"
                          enctype="utf-8">
                        <input type="hidden" name="ik_co_id" value="{{ env('INTERKASSA_ID', NULL) }}"/>
                        <input type="hidden"  name="ik_pm_no" value="{{Auth::user()->id}}"/>
                        <div class="form-group">
                            <label>Введите сумму</label> <i class="fa fa-eur" aria-hidden="true"></i>
                            <input type="number" value="{{$need_summ}}"  name="ik_am" minlength="0" class="form-control"  required>
                            @if($need_summ) 
                            <p>
                                @if(Auth::user()->checkEntrance())
                                    Нужно внести от : {{ $need_summ }} <i class="fa fa-eur" aria-hidden="true"></i>
                                @else
                                    Для первого взноса нужно внести от : {{ $need_summ }} <i class="fa fa-eur" aria-hidden="true"></i>
                                @endif
                            </p>
                            
                            @endif
                                   
                        </div>
                        <input type="hidden" name="ik_cur" value="{{ env('INTERKASSA_CURRENCY', 'EUR') }}"/>
                        <input type="hidden" name="ik_desc"
                               value="Пополнение баланса курсанта - {{Auth::user()->name.' '.Auth::user()->email}}"/>
                        <button type="submit" class="btn btn-success">Перейти к оплате</button>
                    </form>