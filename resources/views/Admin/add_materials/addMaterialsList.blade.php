@extends('layouts.admin')
@section('content')
    <div class="col-md-12">
        <header class="big-header">Дополнительные материалы SDO</header>
        <div class="panel">

                <div class="col-md-5">
                    <form action="" method="GET">
                        <div class="form-group">
                            <button type="submit" title="Выбрать" class="btn btn-danger"><i class="fa fa-search" aria-hidden="true"></i></button>
                            <select name="course_id" class="form-control" style="max-width: 320px; float: right;">
                                <option value="">Выбрать по курсу</option>
                                @foreach($courses as $cours)
                                    <option value="{{ $cours->id }}" {{ request('course_id',0)>0?request('course_id')==$cours->id?'selected':'':'' }}>{{ $cours->trans_curse->first()->name_trans }}</option>
                                @endforeach
                            </select>
                        </div>
                    </form>
                </div>
            
                    <a class="btn btn-success" href="{{route('text_materials.create')}}"><i class="fa fa-plus" aria-hidden="true"></i>
                        Доб. текстовый  материал</a></td>

                    <a class="btn btn-success" href="{{route('file_materials.create')}}"><i class="fa fa-plus" aria-hidden="true"></i>
                        Доб. Аудио/PDF/Видео материал </a>



                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Курс</th>
                        <th>Лекция</th>
                        <th>{{trans('language.name')}}</th>
                        <th></th>
                        <th>Тип</th>
                        <th>{{trans('content.actions')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($materials as $material)
                        <tr>
                            <th>{{$material->id}}</th>
                            <td>
                                @foreach($material->prelection as $pr)
                                    @if (!$loop->first)
                                        <br/><small style="color: red; font-weight: bolder;">&& &nbsp;&nbsp;</small><br/>
                                    @endif
                                    {{ $pr->course->trans_curse->first()->name_trans }}                                
                                @endforeach
                            </td>
                            <td>
                                @foreach($material->prelection as $pr)
                                    @if (!$loop->first)
                                        <br/><small style="color: red; font-weight: bolder;">&& &nbsp;&nbsp;</small><br/>
                                    @endif
                                    {{ $pr->sdoLectureTrans()->name_trans}}
                                @endforeach
                            </td>
                            
                            
                            <td>
                                @if($material->type==='text' or $material->type==='video_link')
                                    {{--<a href="{{route('text_materials.show',$material->id)}}">{{$material->name}}</a>--}}
                                    {{$material->name}}
                                @else
                                    {{--<a href="{{route('file_materials.show',$material->id)}}">{{$material->name}}</a>--}}
                                    {{$material->name}}
                                @endif
                            </td>
                            <td>{{$material->lang_local}}</td>
                            <td class="table-video" style="max-width: 320px;">

                                @if($material->type==="audio")
                                    <audio controls>
                                    <source src="{{ getUrlMaterialId($material->id) }}"  type="audio/mp3">
                                    </audio>
                                    Audio
                                @elseif($material->type==="pdf")
                                    <a href="{{ getUrlMaterialId($material->id) }}" target="_blank">Open PDF</a>
                                @elseif($material->type==="video_link")
                                    <video controls controlsList="nodownload" width="300" height="190">
                                        <source src="{{ getUrlMaterialId($material->id) }}">
                                    </video>
                                @else
                                    Text
                                @endif
                            </td>
                            <td>
                                @if($material->type==='text')
                                    {!! Form::open(array('route'=>['text_materials.edit',$material->id],'method'=>'GET','class'=>'inline-block')) !!}
                                    {!! Form::button('<i class="fas fa-pencil-alt"></i>',['class'=>'btn btn-info','type'=>'submit']) !!}
                                    {!! Form::close() !!}
                                @else
                                    {!! Form::open(array('route'=>['file_materials.edit',$material->id],'method'=>'GET','class'=>'inline-block')) !!}
                                    {!! Form::button('<i class="fas fa-pencil-alt"></i>',['class'=>'btn btn-info','type'=>'submit']) !!}
                                    {!! Form::close() !!}
                                @endif
                                {!! Form::open(array('route'=>['materials.destroy',$material->id], 'method'=>'DELETE','class'=>'inline-block')) !!}
                                {!! Form::button('<i class="fa fa-times"></i>',['class'=>'btn btn-danger','type'=>'submit','onclick'=>'return checkDelete()']) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                        {{ $materials->render() }}

        </div>
    </div>
    <script language="JavaScript" type="text/javascript">
        function checkDelete() {
            return confirm('Вы уверены что хотите удалить данный дополнительный материал?');
        }
    </script>
@endsection
