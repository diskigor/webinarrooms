@extends('layouts.admin')
@section('content')
    <div class="col-md-12">
        <header class="big-header">Форма добавления  Файлового материала</header>
        <div class="panel">
        {!! Form::open(array('route'=>'file_materials.store','method'=>'POST','files' => true,'enctype'=>'multipart/form-data')) !!}

            <div class="form-group">
                {!! Form::label('name','Название материала',['class'=>'control-label']) !!}
                {!! Form::text('name',null,array_merge([ 'class'=>'form-control','placeholder'=>'Введите название материала','required'=>'required'])) !!}
            </div>
            <div class="form-group">
                {!! Form::label('lang','Выберите язык дополнительного материала',['class'=>'control-label']) !!}
                {!! Form::select('lang',$arr_local,null,['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('type','Выберите тип дополнительного материала',['class'=>'control-label']) !!}
                {!! Form::select('type',$arr_type,null,['class'=>'form-control','placeholder'=>'Выберите тип дополнительного материала']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('file','Файл',['class'=>'control-label']) !!}
                {!! Form::file('file',null,['class'=>'']) !!}
            </div>
            <div class="form-group">
            {!! Form::submit('Добавить материал',['class'=>'btn btn-success']) !!}
            <a href="{{route('materials.index')}}" class="btn btn-info">Назад</a>
            </div>
            {!! Form::close()  !!}

        </div>

    </div>
@endsection