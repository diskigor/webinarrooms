@extends('layouts.admin')
@section('content')
<div class="col-md-12">
    <header class="big-header">Форма редактирования дополнительного материала</header>
    <div class="panel">
        {!! Form::open(array('route'=>['file_materials.update',$material->id],'method'=>'PUT','files' => true,'enctype'=>'multipart/form-data')) !!}
        <div class="form-group">
            {!! Form::label('name','Название материала',['class'=>'control-label']) !!}
            {!! Form::text('name',$material->name,array_merge([ 'class'=>'form-control','placeholder'=>'Введите название материала','required'=>'required'])) !!}
        </div>
        <div class="form-group">
            {!! Form::label('lang','Выберите язык дополнительного материала',['class'=>'control-label']) !!}
            {!! Form::select('lang',$arr_local,$material->lang_local,['class'=>'form-control']) !!}
        </div>
        
        @if($material->type==='pdf')
            <a href="{{ getUrlMaterialId($material->id) }}" target="_blank">Open PDF</a>
        @elseif($material->type==='audio')
            <audio controls="controls">
                <source src="{{ getUrlMaterialId($material->id) }}" type="audio/mp3">
            </audio>
        @elseif($material->type==='video_link')
            <video controls controlsList="nodownload" width="560" height="190">
                <source src="{{ getUrlMaterialId($material->id) }}">
            </video>
        @elseif($material->type==='archives')
            <a href="{{ getUrlMaterialId($material->id) }}" target="_blank">скачать архив</a>
        @endif
        
        <div class="form-group">
            <p>* перезалить контент</p>
            {!! Form::label('file','Файл '.$material->nameType,['class'=>'control-label']) !!}
            {!! Form::file('file',null,['class'=>'']) !!}
        </div>            
        {!! Form::submit('Обновить материал',['class'=>'btn btn-success']) !!}
        {!! Form::close()  !!}
        <a href="{{route('materials.index')}}" class="btn btn-info">Назад</a>
    </div>
</div>
@endsection