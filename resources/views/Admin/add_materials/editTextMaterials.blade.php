@extends('layouts.admin')
@section('content')
    <div class="col-md-12">
        <header class="big-header">Форма редактирования текстового материала</header>
        <div class="panel">
            {!! Form::open(array('route'=>['text_materials.update',$material->id],'method'=>'PUT')) !!}
            <div class="form-group">
                {!! Form::label('name','Название материала',['class'=>'control-label']) !!}
                {!! Form::text('name',$material->name,array_merge([ 'class'=>'form-control','placeholder'=>'Введите название материала','required'=>'required'])) !!}
            </div>
            <div class="form-group">
                {!! Form::label('lang','Выберите язык дополнительного материала',['class'=>'control-label']) !!}
                {!! Form::select('lang',$arr_local,$material->lang_local,['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('desc','Контент',['class'=>'control-label']) !!}
                {!! Form::textarea('desc',$material->content,['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
            {!! Form::submit('Сохранить',['class'=>'btn btn-success']) !!}

            <a href="{{route('materials.index')}}" class="btn btn-info">Назад</a>
            {!! Form::close()  !!}
            </div>
        </div>
    </div>
@endsection