@extends('layouts.admin')
@section('content')
@if(isset($cadets))
<!-- Contextual Classes -->
<div class="col-md-12">

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <header class="big-header text-center">
                        {{trans('content.list_cadets')}}
                    </header>

                </div>
                <div class="body">
                    <div class="panel">
                        <!--div class="row">
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="item" id="searchItem" placeholder="Поиск">
                                    <a type="submit" class="btn btn-blue input-group-btn" href="{{route('cadet.index')}}">Сбросить</a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="item" id="searchEmail" placeholder="Поиск по почте">
                                    <a type="submit" class="btn btn-blue input-group-btn" href="{{route('cadet.index')}}">Сбросить</a>
                                </div>
                            </div>
                        </div-->
                        <table class="table table-responsive" id="js_users_table">
                            <thead>
                                <tr>
                                    <th>№</th>
                                    <th>{{trans('content.name')}}</th>
                                    <th>Email</th>
                                    <th>{{trans('content.role_user')}}</th>
                                    <th>Группа</th>
                                    <th>{{trans('content.level')}}</th>
                                    <th>{{trans('content.balance')}}</th>
                                    <th>Зарегистрирован</th>
                                    <th>{{trans('content.actions')}}</th>
                                </tr>
                            </thead>
                            <tbody class="cadets">
                                @foreach($cadets as $cadet)
                                @if(is_null($cadet->blackUser))
                                <tr id="{{$cadet->name}}">
                                    <th>{{$cadet->id}}</th>
                                    <td>{{$cadet->name}}</td>
                                    <td>{{$cadet->email}}</td>
                                    <td>Курсант</td>
                                    <td>@if($cadet->group_id != NULL){{$cadet->group->name}}@else Нет @endif</td>
                                    <td>{{$cadet->level_point}}</td>
                                    <td>{{$cadet->balance}}</td>
                                    <td>{{ $cadet->created_at }}</td>
                                    <td>
                                        @if(Auth::user()->isСurator() or Auth::user()->isLecturer())
                                        {!! Form::open(array('route'=>['add_connect_mentor',$cadet->id],'method'=>'GET')) !!}
                                        {!! Form::submit('Стать наставником',['class'=>'btn btn-blue']) !!}
                                        {!! Form::close() !!}
                                        @endif
                                        @if(Auth::user()->isAdmin())
                                        {!! Form::open(array('route'=>['cadet.edit',$cadet->id],'method'=>'GET','class'=>'inline-block')) !!}
                                        {!! Form::button('<i class="fas fa-pencil-alt"></i>',['class'=>'btn btn-info','type'=>'submit']) !!}
                                        {!! Form::close() !!}

                                        {!! Form::open(array('route'=>['cadet.destroy',$cadet->id], 'method'=>'DELETE','class'=>'inline-block')) !!}
                                        {!! Form::button('<i class="fa fa-times"></i>',['class'=>'btn btn-danger','type'=>'submit','onclick'=>'return checkDelete()']) !!}
                                        {!! Form::close() !!}
                                        <button type="button" class="btn btn-black btn-lg"
                                                data-toggle="modal" data-target="{{'#modalBlackList_'.$cadet->id}}">
                                            <i class="fa fa-file" aria-hidden="true"></i>
                                        </button>
                                        <div class="modal fade" id="{{'modalBlackList_'.$cadet->id}}"
                                             role="dialog">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close"
                                                                data-dismiss="modal" aria-label="Close"><span
                                                                aria-hidden="true">&times;</span>
                                                        </button>
                                                        <h4 class="modal-title" id="myModalLabel">
                                                            Добавление в черный список</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        {!! Form::open(array('route'=>'black_list.store','method'=>'POST')) !!}
                                                        <input type="hidden" name="user_id"
                                                               value="{{$cadet->id}}">
                                                        <div class="form-group">
                                                            {!! Form::label('reason','Введите причину.',['class'=>'control-label']) !!}
                                                            {!! Form::text('reason',null,['class'=>'form-control','required','maxlength'=>'190']) !!}
                                                        </div>
                                                        <button type="button" class="btn btn-default"
                                                                data-dismiss="modal">Закрыть
                                                        </button>
                                                        {!! Form::submit('Заблокировать',['class'=>'btn btn-success']) !!}
                                                        {!! Form::close() !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    </td>
                                </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END# Contextual Classes -->
@endif
<!--script language="JavaScript" type="text/javascript">
    function checkDelete() {
        return confirm('Вы уверены что хотите удалить данного пользователя?');
    }
    $(function () {
        $("#searchItem").autocomplete({
            source: '{!! url('admin/search/4') !!}',
            minlenght: 2,
            autoFocus: true,
            select: function (i, user) {
                $('.cadets tr[id != "' + user.item.value + '"]').hide();
                $('.pagin').hide();
            }

        });
    });
    $(function () {
        $("#searchEmail").autocomplete({
            source: '{!! url('admin/searchEmail/4') !!}',
            minlenght: 4,
            autoFocus: true,
            select: function (i, user) {
                $('.cadets tr[id != "' + user.item.value + '"]').hide();
                $('.pagin').hide();
            }
        })
    });
</script-->
@endsection
@section('script')
<script src="{{ asset('js/datatables/datatables.min.js') }}"></script>
<script>
function checkDelete() {
    return confirm('Вы уверены что хотите удалить данного пользователя?');
}
$(document).ready(function () {

    $('#js_users_table').DataTable();
});

</script>
@endsection