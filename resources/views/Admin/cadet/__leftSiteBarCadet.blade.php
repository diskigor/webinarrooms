<aside class="left-sidebar">
    <div>
        <div class="side-nav-toggle"></div>
        <!-- User Info -->
        <div class="user">

            <div class="info-container menu">
                <header>
                    <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Tooltip on left"></i>
                    <a href="{{route('profile.index')}}" class="menu-toggle"> <i class="fa fa-cog" aria-hidden="true"></i></a>
                </header>
                <div class="user-photo" style="background-image: url({{ Auth::user()->avatar }})">

                        <!-- Branding Image -->
                    <a class="logo" href="{{ url('/') }}">
                            <img src="{{asset('images/mane/logo.png')}}" width="32" height="32" alt="SDO">
                        </a>
                    </div>
                  
                <div class="">
                <div class="user-name">{{Auth::user()->name}}</div>
                <div class="email">{{Auth::user()->email}}</div>
                <!-- Account navs -->
                <div class="statusball">{{trans('site_bar.yuorscore')}}: <span style="color: #fff; font-weight: bold;">{{Auth::user()->level_point}}</span></div>
                <div class="balance">{{trans('site_bar.yourballance')}}, {{trans('home.kel')}}: <span style="color: #fff; font-weight: bold;">{{Auth::user()->balance}}</span></div>
                <div class="statusball">{{trans('site_bar.yourstatus')}}: <span style="color: #fff; font-weight: bold;">{{Auth::user()->role->description}}</span></div>
                @if(!Auth::user()->isAdmin())
                <div class="container-fluid">
                    <a class="btn btn-success" href="{{route('mybalance.index')}}"><i class="fa fa-plus"
                                                                                        aria-hidden="true"></i>
                        Пополнить баланс</a>
                </div>
                @endif
                </div>
            <!-- END account navs -->
            
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul>
                <li>
                    <a href="{{route('/personal_area')}}">
                        <i class="fa fa-user"></i> {{trans('content.personal_area')}}
                    </a>
                </li>
                <li>
                    <a href="{{route('studies.index')}}" class="menu-toggle">
                        <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                        <span>Учебная аудитория</span>
                    </a>
                </li>
                <li>
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false"
                       aria-controls="collapseOne">
                        <i class="fa fa-book" aria-hidden="true"></i>
                        <span>Каталог курсов</span>
                    </a>
                    <ul id="collapseOne" class="ml-menu panel-collapse collapse" role="tabpanel">
                        <li>
                            <a href="{{route('free_materials.index')}}">
                                <span>Открытая школа</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('user_curses.index')}}">
                                <span>Основные курсы</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('partner_curses')}}">
                                <span>Партнёрские курсы</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="{{route('my_works')}}" class="menu-toggle">
                        <i class="fa fa-file-word-o" aria-hidden="true"></i>
                        <span>Мои работы</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('ablog.index')}}">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                        <span>Мой Блог</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('refsystem.index')}}">
                        <i class="fa fa-share-alt" aria-hidden="true"></i>
                        <span>Реферальная с-ма</span>
                    </a>
                </li>
                <li>
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseStatistics" aria-expanded="false"
                       aria-controls="collapseStatistics">
                        <i class="fa fa-line-chart" aria-hidden="true"></i>
                        <span>Статистика</span>
                    </a>
                    <ul id="collapseStatistics" class="ml-menu panel-collapse collapse" role="tabpanel">
                        <li>
                            <a href="{{route('user_statistics.index')}}">
                                <span>Статистика покупок</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('replenishment_user_statistic')}}">
                                <span>Статистика пополнений</span>
                            </a>
                        </li>
                    </ul>

                </li>
                <li>
                    <a href="{{route('profile.index')}}" class="menu-toggle">
                        <i class="fa fa-cog" aria-hidden="true"></i>
                        <span>{{trans('content.profile_setting')}}</span>
                    </a>
                </li>
                <li>
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse9" aria-expanded="false"
                       aria-controls="collapse9">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                        <span>Тех.Поддержка</span>
                    </a>
                    <ul id="collapse9" class="ml-menu panel-collapse collapse" role="tabpanel">
                        <li>
                            <a href="{{route('callback.create')}}">
                                <span>Создать новый запрос</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('user_callback_chats.index')}}">
                                <span>Список запросов</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="/page/rules">
                        <i class="fa fa-bullhorn" aria-hidden="true"></i>
                        <span> {{trans('home.rules')}}</span>
                    </a>
                </li>
                
                <span class="visible-xs">
                <hr>
                <li>
                            <a href="{{route('admin_all_archive.index')}}/">{{trans('home.leaderscience')}}</a>
                        </li>
                        <li>
                            <a href="{{ route('blog.index') }}">{{trans('home.blog')}}</a>
                        </li>
                        <li>
                            <a href="#"   data-toggle="modal" data-target="#webRoom">{{trans('home.webinar')}}</a>
                        </li>
                        <li>
                            <!-- Button trigger modal -->
                            <a  href="#" data-toggle="modal" data-target="#forumSDO">{{trans('home.forum')}}</a>
                        </li>
                        <li>
                            <a href="#" data-toggle="modal" data-target="#shopSDO">{{trans('home.magaz')}}</a>
                        </li>
                <hr>
                </span>
                <li>
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                        <i class="fa fa-power-off"></i> {{trans('content.logout')}}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </div>
        <!-- #Menu -->
        </div>
</aside>
<div class="modal fade" id="questionuser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{!!trans('home.tooltipzag')!!}</h4>
            </div>
            <div class="modal-body">
                {!!trans('home.tooltip')!!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
            </div>
        </div>
    </div>
</div>
@section('script')
    <script>
        // MOBILE MENU TOGGLE
        $('.side-nav-toggle').on('click', function () {
            $('.left-sidebar').toggleClass('open');
        });

    </script>
@endsection