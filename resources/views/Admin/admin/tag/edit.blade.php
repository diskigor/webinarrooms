@extends('layouts.admin')

@section('content')
    <div class="col-md-12">
        <header class="big-header text-center">Форма рдекатирования тега</header>
        <div class="panel">
            {!! Form::open(array('route'=>['atags.update',$tag->id], 'method'=>'POST','enctype'=>'multipart/form-data'))!!}
            {{ method_field('PUT') }}

            <div class="form-group">
                {!! Form::label('tag','Название тега',array_merge(['class'=>'control-label'])) !!}
                {!! Form::text('tag',$tag->tag,array_merge(['class'=>'form-control','required','placeholder'=>'Название тега'])) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Сохранить',['class'=>'btn btn-success']) !!}
                <a href="{{route('atags.index')}}" class="btn btn-info">Назад</a>
            </div>
            {!! Form::close() !!}

        </div>
    </div>
@endsection
