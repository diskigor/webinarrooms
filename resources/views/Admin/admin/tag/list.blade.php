@extends('layouts.admin')

@section('content')
    <div class="col-md-12">
        <header class="big-header text-center">Теги SDO</header>
        <div class="form-group">
            <a href="{{route('atags.create')}}" class="btn btn-success"><i class="fa fa-plus"></i> Добавить тег</a>
        </div>
        <div class="row">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Тег</th>
                        <th>slug</th>
                        <th>suggest</th>
                        <th>{{trans('content.actions')}}</th>
                    </tr>
                </thead>
            @foreach($tags as $tag)
                <tr>
                  <td>{{ $tag->id }}</td>
                  <td>{{ $tag->tag }}</td>
                  <td>{{ $tag->slug }}</td>
                  <td>{{ $tag->suggest }}</td>
                  <td><a href="{{route('atags.edit',$tag->id)}}" class="btn btn-info" title="Редактировать статью">
                                <i class="fas fa-pencil-alt"></i></a>
                            {!! Form::open(array('route'=>['atags.destroy',$tag->id],'method'=>'DELETE','class'=>'inline-block')) !!}
                            {!! Form::button('<i class="fa fa-times"></i>',['class'=>'btn btn-danger','type'=>'submit','onclick'=>'return checkDelete()','title'=>'Удалить']) !!}
                            {!! Form::close() !!}
                  
                  </td>
                </tr>
            @endforeach
            </table>
        </div>
        <div class="row">
            {{ $tags->links() }}
        </div>
    </div>
@endsection