@extends('layouts.admin')
@section('content')
    <div class="col-md-12">
        <header class="big-header text-center">Список комментариев к блогу</header>
        <center><h3>"{{ $blog->title }}"</h3></center>
        <div class="row">
            @if ($comments->isEmpty())
            <figure class="thumbnail courses">
                Ещё не было комментариев пользователей
            </figure>
            @endif
            @foreach($comments as $comment)
                <figure class="thumbnail courses">
                    <div class="col-md-2">
                        <img class="image" 
                                 src="{{ $comment->user->avatar }}"
                                 height="75" width="75">
                    </div>
                    <div class="col-md-9">
                        <div class="commentDate">{{ is_object($comment->created_at) ? $comment->created_at->format('d.m.Y в H:i') : ''}} &nbsp;&nbsp;&nbsp; {{ $comment->user->name }} </div>
                        <div class="comment">{{ $comment->text }}</div>
                    </div>    
                    <div class="hover-nav">
                        {!! Form::open(array('route'=>['comment_delete',$blog->id,$comment->id],'method'=>'POST','class'=>'inline-block')) !!}
                        {!! Form::button('<i class="fa fa-times"></i>',['class'=>'btn btn-danger','title'=>'Удалить комментарий','type'=>'submit','onclick'=>'return checkDelete()']) !!}
                        {!! Form::close() !!}
                    </div>

                </figure>
                <div class="clearfix"></div>
            @endforeach
        </div>
        <div class="row">
            {{ $comments->links() }}
        </div>
        <div class="row">
            <a href="{{route('ablog.index')}}" class="btn btn-info">Назад</a>
        </div>
    </div>
    <script language="JavaScript" type="text/javascript">
        function checkDelete() {
            return confirm('Вы уверены что хотите удалить данный комментарий?');
        }
    </script>
@endsection