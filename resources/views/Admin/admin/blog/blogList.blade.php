{{-- $blogs --}}
@extends('layouts.admin')
@section('content')
<div class="col-md-12">
    <?php $title_page=$blogs->nameBlog; ?>
    <div class="form-group">
        <a href="{{route('ablog.create')}}" class="btn btn-success"><i class="fa fa-plus"></i> Добавить статью</a>
    </div>
    <div class="row">
        @foreach($blogs as $article)
        <figure class="col-sm-4">
            <div class="thumbnail courses">
                <img class="image" src="{{ $article->image }}">
                <h3>{{ $article->title }}
                    @if($article->isNew)
                    <span class="badge pull-right" title="это новый блог">new</span>
                    @endif
                </h3>
                <p>{{ $article->authorFIO }}</p>
                <p>
                    <span class="bg-info">Всего <i class="fa fa-thumbs-up" aria-hidden="true"></i> : {{ $article->likes()->count() }}</span>
                    &nbsp;&nbsp;<span class="bg-info">Всего <i class="fa fa-comments" aria-hidden="true"></i> : {{ $article->comments()->count() }}</span>
                </p>
                <div class="hover-nav">
                    <a href="{{route('comment_list',$article->id)}}" class="btn btn-success" title="Модерировать комментарии">
                        <i class="fa fa-comments" aria-hidden="true"></i>
                    </a>
                    <a href="{{route('ablog.edit',$article->id)}}" class="btn btn-info" title="Редактировать статью">
                        <i class="fas fa-pencil-alt"></i>
                    </a>
                    {!! Form::open(array('route'=>['ablog.destroy',$article->id],'method'=>'DELETE','class'=>'inline-block')) !!}
                    {!! Form::button('<i class="fa fa-times"></i>',['class'=>'btn btn-danger','type'=>'submit','onclick'=>'return checkDelete()','title'=>'Удалить']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </figure>
        @endforeach
    </div>
    <div class="row">
        {{ $blogs->links() }}
    </div>
</div>
@endsection