@extends('layouts.admin')
@section('content')
    <div class="col-md-12">
        <?php $title_page='Форма добавления ' . $nameBlog; ?>
        <div class="panel">
            <div class="form-group">
                <div class="preview">
                    <div id="preview1"></div>
                    <input type="button" id="reset1" value="Сбросить" class="btn btn-info reset"/>
                </div>
            </div>
            {!! Form::open(array('route'=>'ablog.store', 'method'=>'POST','enctype'=>'multipart/form-data'))!!}
            <div class="form-group">
                <label for="image_link" class="control-label">Изображение <small>(1100x400)</small></label>
                {!! Form::file('image_link',['accept'=>"image/jpeg,image/png",'required'])!!}
            </div>
            <div class="form-group">
                {!! Form::label('lang','Язык',array_merge(['class'=>'control-label'])) !!}
                {!! Form::select('lang',$arr_local,array_merge(['class'=>'form-control','placeholder'=>'Выберите язык'])) !!}
            </div>
            
            <div class="form-group">
                {!! Form::label('tags','Теги',array_merge(['class'=>'control-label'])) !!}
                {!! Form::select('tags[]',$all_tags,null,['class'=>'chosen-select hidden','data-placeholder'=>'Выберите теги', 'multiple' => true]) !!}
            </div>
            
            <div class="form-group">
                {!! Form::label('slug','Slug проекта для SEO',array_merge(['class'=>'control-label'])) !!}
                {!! Form::text('slug',null,array_merge(['class'=>'form-control','required','placeholder'=>'Пример: ponedelnik_segodnya_vse_xorosho'])) !!}
            </div>
            <div class="form-group">
                {!! Form::label('title','Название проекта',array_merge(['class'=>'control-label'])) !!}
                {!! Form::text('title',null,array_merge(['class'=>'form-control','required','placeholder'=>'Название статьи'])) !!}
            </div>
            <div class="form-group">
                {!! Form::label('mainauthor','Автор проекта',array_merge(['class'=>'control-label'])) !!}
                <p class="form-control disabled">{{ request()->user()->FullFIO }}</p>
            </div>
            
            <div class="form-group">
                {!! Form::label('sd_article','Статья',array_merge(['class'=>'control-label'])) !!}
                {!! Form::textarea('sd_article',null,array_merge(['class'=>'form-control'])) !!}
            </div>
            <div class="form-group">
                {!! Form::submit('Сохранить',['class'=>'btn btn-success']) !!}
                <a href="{{route('ablog.index')}}" class="btn btn-info">Назад</a>
            </div>
            {!! Form::close() !!}

        </div>
    </div>
@endsection
@push('stack_scripts')
    <script src="{{asset('js/imagepreview.js')}} "></script>
    <script type="text/javascript">
        $(function () {
            $('#preview1').imagepreview({
                input: '[name="image_link"]',
                reset: '#reset1',
                preview: '#preview1'
            });
        });
    </script>
    
    <script src="{{asset('js/chosen.jquery.min.js')}} "></script>
    <link href="{{ asset('css/chosen.min.css') }}" rel="stylesheet">
    <script type="text/javascript">
        $(".chosen-select").chosen({width: "95%"});
    </script>
@endpush