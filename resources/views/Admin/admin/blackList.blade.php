@extends('layouts.admin')
@section('content')
    <div class="col-md-12">

            <div class="body">
                <header class="big-header text-center">Список заблокированых пользователей</header>
                <div class="panel">
                    <table class="table table-responsive">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Пользователь</th>
                            <th>Роль</th>
                            <th>Email</th>
                            <th>Причина</th>
                            <th>Дата</th>
                            <th>{{trans('content.actions')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($black_lists as $black_list)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $black_list->user->name }}</td>
                                <td>
                                    @if($black_list->user->role->name === 'Cadet')
                                        Курсант
                                    @elseif($black_list->user->role->name === 'Сurator')
                                        Куратор
                                    @else
                                        Лектор
                                    @endif
                                </td>
                                <td>{{$black_list->user->email}}</td>
                                <td>{{$black_list->reason}}</td>
                                <td>{{$black_list->created_at}}</td>
                                <td>
                                    {!! Form::open(array('route'=>['black_list.destroy',$black_list->id], 'method'=>'DELETE','class'=>'inline-block')) !!}
                                    {!! Form::button('Разблокирвать',['class'=>'btn btn-success','type'=>'submit']) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
@endsection