@extends('Admin.app')
@section('content')
    <div class="col-md-12">
        <header class="big-header text-center">Таблица связей кураторов и курсантов.</header>
    </div>
    <div class="container">

            <div class="panel">
                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Курсант</th>
                        <th>Куратор</th>
                        <th>Статус</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($connection->count() !=0 )
                        <?php $i = 1;?>
                        @foreach($connection as $item)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$item->cadet->name}}</td>
                                <td>{{$item->curator->name}}</td>
                                <td>@if($item->status === 1) <i class="fa fa-check-circle" style="color: #2ab27b"
                                                                aria-hidden="true"></i>  @else <i class="fa fa-times"
                                                                                                  style="color: #FF0000"
                                                                                                  aria-hidden="true"></i> @endif
                                </td>
                                <td>
                                    Удалить
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td>На данный момент нет связей</td>
                        </tr>
                    @endif
                    </tbody>

                </table>
            </div>

    </div>
@endsection