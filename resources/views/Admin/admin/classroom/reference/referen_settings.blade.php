@extends('Admin.app')
@section('content')
    <div class="col-md-12">
        <header class="big-header text-center">Настройка оценки реферальной ссылки.</header>
        <div class="panel">
            <div class="container">
                <div class="row">

                    <table class="table table-responsive">
                        <thead>
                        <tr>
                            <th>Кол-во денег начисляемых при первой покупке</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="text-center">{{$referen->bonus}}</td>
                            <td>
                                <button type="button" class="btn btn-primary btn-lg" data-toggle="modal"
                                        data-target="#myModal">
                                    Редактировать
                                </button>
                                <div class="modal fade" id="myModal" tabindex="-1" role="dialog"
                                     aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close"><span aria-hidden="true">&times;</span>
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel">Форма редактирования кол-во
                                                    баллов</h4>
                                            </div>
                                            {!! Form::open(array('route'=>['reference.update',$referen->id],'method'=>'PUT')) !!}
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    {!! Form::label('bonus','Преимущество',['class'=>'control-label']) !!}
                                                    {!! Form::number('bonus',$referen->bonus,['class'=>'form-control','min'=>1]) !!}
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                                    Закрыть
                                                </button>
                                                {!! Form::submit('Сохранить',['class'=>'btn btn-success']) !!}

                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection