@extends('layouts.admin')

@section('content')
    <div class="col-md-12">
        <header class="big-header text-center">Настройка оценки кураторов</header>
        <div class="panel">
            <p>В данной таблице хранятся данные, которыми будет пользоваться система при просьбе куратора взять себе
                курсанта.</p>
            <div class="form-group">
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalAddSettings">
                    <i class="fa fa-plus"></i> Добавить настройку
                </button>
            </div>
            <div class="modal fade" id="modalAddSettings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center" id="myModalLabel">Форма добавления настройки</h4>
                        </div>
                        <div class="modal-body">
                            {!! Form::open(array('route'=>'mentor_settings.store','method'=>'POST')) !!}
                            <div class="form-group">
                                {!! Form::label('count_point','Нужное количество очков',['class'=>'control-label']) !!}
                                {!! Form::number('count_point',null,['class'=>'form-control','required','min'=>'0','required']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('count_cadets','Количество допустимых курсантов',['class'=>'control-label']) !!}
                                {!! Form::number('count_cadets',null,['class'=>'form-control','required','min'=>'0','required']) !!}
                            </div>

                            {!! Form::submit('Сохранить',['class'=>'btn btn-success']) !!}
                            <button type="button" class="btn btn-warning" data-dismiss="modal">Закрыть</button>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>


            <div class="container">
                <div class="row">

                    <table class="table table-responsive">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Кол-во нужных баллов</th>
                            <th>Кол-во допустимых курсантов</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($mentors_settings->count() != 0)
                            <?php $i = 1;?>
                            @foreach($mentors_settings as $setting)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$setting->count_point}}</td>
                                    <td>{{$setting->count_cadets}}</td>
                                    <td>
                                        {!! Form::open(array('route'=>['mentor_settings.destroy',$setting->id],'method'=>'DELETE')) !!}
                                        {!! Form::submit('Удалить',['class'=>'btn btn-danger']) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <td class="text-center">Таблица пустая</td>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection