@extends('layouts.admin')

@section('content')
    <div class="col-md-12">
        <header class="big-header text-center">Вывод списко наших проектов.</header>
        <div class="form-group">
            <a href="{{route('else_project.create')}}" class="btn btn-success"><i class="fa fa-plus"></i> Добавить проект</a>
        </div>
            <div class="row">
                @foreach($projects as $project)
                    <figure class="col-sm-4">
                        <div class="thumbnail courses">
                            <img class="image" src="{{ $project->image }}">
                            <h3>{{$project->name}}</h3>
                            {!! $project->forSide?'<span class="pull-right">для рекламы</span>':'' !!}
                            <div class="hover-nav">
                                <a href="{{route('else_project.edit',$project->id)}}" class="btn btn-info">
                                    <i class="fas fa-pencil-alt"></i>
                                </a>
                                {!! Form::open(array('route'=>['else_project.destroy',$project->id],'method'=>'DELETE','class'=>'inline-block')) !!}
                                {!! Form::button('<i class="fa fa-times"></i>',['class'=>'btn btn-danger','type'=>'submit','onclick'=>'return checkDelete()']) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </figure>
                @endforeach
            </div>

    </div>
@endsection