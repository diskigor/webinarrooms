@extends('layouts.admin')

@section('content')
<div class="col-md-12">
    <header class="big-header text-center">Форма редактирования проекта на сайт</header>
    <div class="panel">
        <div class="form-group">
            <img class="pre" src="{{ $project->image }}" alt="">
        </div>
        <div class="form-group">
            <div class="preview">
                <div id="preview1"></div>
                <input type="button" id="reset1" value="Сбросить" class="btn btn-info reset"/>
            </div>
        </div>
        {!! Form::open(array('route'=>['else_project.update',$project->id], 'method'=>'PUT','enctype'=>'multipart/form-data'))!!}
        <div class="form-group">
            <label for="image" class="control-label">Изображение <small>(1140x400)</small></label>
            {!! Form::file('image',['accept'=>"image/jpeg,image/png"])!!}
        </div>

        <div class="form-group">
            {!! Form::label('main','Сортировка',['class'=>'control-label']) !!}
            {!! Form::number('main',$project->main_div,['class'=>'form-control','min'=>0, 'max'=>100]) !!}
        </div>
        
        <div class="form-group">
            {!! Form::label('side_bar','Только для рекламы',['class'=>'control-label']) !!}
            {!! Form::checkbox('side_bar', 1,$project->side_bar) !!}
        </div>

        <div class="form-group">
            {!! Form::label('lang','Язык',array_merge(['class'=>'control-label'])) !!}
            {!! Form::select('lang',$arr_local,['class'=>'form-control','placeholder'=>'Выберите язык'],[$project->lang_local]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('name_project','Название проекта',array_merge(['class'=>'control-label'])) !!}
            {!! Form::text('name_project',$project->name,array_merge(['class'=>'form-control','required','placeholder'=>'Название и краткое описание проекта'])) !!}
        </div>
        <div class="form-group">
            {!! Form::label('project_description','Краткое описание проекта',array_merge(['class'=>'control-label'])) !!}
            {!! Form::textarea('project_description',$project->short_desc,array_merge(['class'=>'form-control','placeholder'=>'Название и краткое описание проекта'])) !!}
        </div>
        <div class="form-group">
            {!! Form::label('href_project','Ссылка на проект',array_merge(['class'=>'control-label'])) !!}
            {!! Form::text('href_project',$project->href,array_merge(['class'=>'form-control','placeholder'=>"https://"])) !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Сохранить',['class'=>'btn btn-success']) !!}
            <a href="{{ route('else_project.index') }}" class="btn btn-info">Назад</a>
        </div>
        {!! Form::close() !!}

    </div>
</div>
@endsection

@push('stack_scripts')
<script src="{{asset('js/imagepreview.js')}} "></script>
<script type="text/javascript">
$(function () {
$('#preview1').imagepreview({
    input: '[name="image"]',
    reset: '#reset1',
    preview: '#preview1'
});
});
</script>
@endpush