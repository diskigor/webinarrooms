@extends('layouts.admin')

@section('content')
    <div class="col-md-12">
        <h3 class="text-center">Форма редактирования статической страницы</h3>
        {!! Form::open(array('route'=>['static_pages.update',$page->id],'method'=>'PUT','enctype'=>'multipart/form-data')) !!}
        <div class="form-group">
            {!! Form::label('sort','Номер страницы : '.$page->sort,['class'=>'control-label']) !!}
            {!! Form::selectRange('sort',0, 20)!!}
        </div>
        <!-- Картинка баннера vs text -->
        <img src="{{ $page->image }}" alt="">
        <div class="form-group">
            {!! Form::label('banner_image','Изображение баннера 1920х380',['class'=>'control-label']) !!}
            {!! Form::file('banner_image',['accept'=>"image/jpeg,image/png,image/gif"])!!}
        </div>
        <div class="form-group">
            {!! Form::label('banner_text','Текст баннера',['class'=>'control-label']) !!}
            {!! Form::text('banner_text',$page->banner_text,['class'=>'form-control']) !!}
        </div>
        <!-- -->
        <div class="form-group">
            {!! Form::label('type_href','Тип ссылки внешная',['class'=>'control-label']) !!}
            @if($page->type_href===0)
                {!! Form::checkbox('type_href','1',false) !!}
            @else
                {!! Form::checkbox('type_href','1',true) !!}
            @endif
        </div>
        <div class="form-group">
            {!! Form::label('show_menu','Отображать страницу',['class'=>'control-label']) !!}
            @if($page->show_menu === 1)
                {!! Form::checkbox('show_menu','1',true) !!}
            @else
                {!! Form::checkbox('show_menu','1',false) !!}
            @endif
        </div>
        <div class="form-group">
            {!! Form::label('href','Внешная ссылка',['class'=>'control-label']) !!}
            {!! Form::text('href',$page->href,['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('slug','Slug страницы',['class'=>'control-label']) !!}
            {!! Form::text('slug',$page->slug,['class'=>'form-control','required']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('name','Имя страницы',['class'=>'control-label']) !!}
            {!! Form::text('name',$page->name,['class'=>'form-control','required']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('content_page','Контент',['class'=>'control-label']) !!}
            {!! Form::textarea('content_page',$page->content,['class'=>'form-control']) !!}
        </div>
        <a href="{{redirect()->back()->getTargetUrl()}}" class="btn btn-default">Назад</a>
        {!! Form::submit('Сохранить',['class'=>'btn btn-success']) !!}
        {!! Form::close() !!}
    </div>
@endsection