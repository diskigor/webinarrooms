@extends('layouts.admin')

@section('content')
    <div class="col-md-12">
        <header class="big-header text-center">Статические страницы</header>
        <div class="panel">
            <!-- Начало модального окна -->
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalAddAdvantages">
                <i class="fa fa-plus"></i> Добавить страницу
            </button>
            <div class="modal fade" id="modalAddAdvantages" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Форма добавления страницы</h4>
                        </div>
                        <div class="modal-body">
                            {!! Form::open(array('route'=>'static_pages.store','method'=>'POST','enctype'=>'multipart/form-data')) !!}
                            <div class="form-group">
                                {!! Form::label('sort','Номер страницы',['class'=>'control-label']) !!}
                                {!! Form::selectRange('sort', 0, 20)!!}
                            </div>
                            <!-- Картинка баннера vs text -->
                            <div class="form-group">
                                {!! Form::label('banner_image','Изображение баннера 1920х380',['class'=>'control-label']) !!}
                                {!! Form::file('banner_image',['accept'=>"image/jpeg,image/png,image/gif"])!!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('banner_text','Текст баннера',['class'=>'control-label']) !!}
                                {!! Form::text('banner_text',null,['class'=>'form-control']) !!}
                            </div>
                            <!-- -->
                            <div class="form-group">
                                {!! Form::label('type_href','Тип ссылки внешная',['class'=>'control-label']) !!}
                                {!! Form::checkbox('type_href','1',false) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('show_menu','Отображать страницу',['class'=>'control-label']) !!}
                                {!! Form::checkbox('show_menu','1',false) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('href','Внешная ссылка',['class'=>'control-label']) !!}
                                {!! Form::text('href',null,['class'=>'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('slug','Slug страницы',['class'=>'control-label']) !!}
                                {!! Form::text('slug',null,['class'=>'form-control','required']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('name','Имя страницы',['class'=>'control-label']) !!}
                                {!! Form::text('name',null,['class'=>'form-control','required']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('content_page','Контент',['class'=>'control-label']) !!}
                                {!! Form::textarea('content_page',null,['class'=>'form-control']) !!}
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-warning" data-dismiss="modal">Закрыть</button>
                                {!! Form::submit('Сохранить',['class'=>'btn btn-success']) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Конец модалльного окна добавления страниц -->
            <h4 class="text-center">Список статических страниц</h4>
            <div class="benefits">

                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Номер сортировки</th>
                        <th>Slug</th>
                        <th>Отображать</th>
                        <th>Название</th>
                        <th>Дата</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody class="billing">
                    @if($static_pages->count() != 0)
                        <?php $i = 1;?>
                        @foreach($static_pages as $page)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$page->sort}}</td>
                                <td>{{$page->slug}}</td>
                                <td>@if($page->show_menu === 1 )Да @else Нет @endif</td>
                                <td>{{$page->name}}</td>
                                <td>{{$page->created_at}}</td>
                                <td>
                                    <div class="benefit">
                                        {!! Form::open(array('route'=>['static_pages.destroy',$page->id],'method'=>'DELETE','class'=>'inline-block')) !!}
                                        {!! Form::submit('Удалить',array_merge(['class'=>'btn btn-danger'])) !!}
                                        {!! Form::close() !!}
                                        <a href="{{route('static_pages.edit',$page->id)}}" class="btn btn-info">Редактировать</a>
                                    </div>

                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td>
                                Таблица пустая
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection