{{-- $subscribers --}}

<div class="row">
    @foreach($subscribers as $sub)
            <div class="form-group col-md-4">
                <label class="">
                    {!! Form::checkbox('set_subscribe_'.$sub->get('id'), 1,$sub->get('status'),array_merge(['data-id'=>$sub->get('id')])) !!}&nbsp;&nbsp;{{ $sub->get('uType') }}
                </label>
            </div>
    @endforeach
</div>
