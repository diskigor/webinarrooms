@extends('layouts.admin')

@section('content')
<link rel="stylesheet" href="{{ asset('css/datatables.css') }}">

    <div class="col-md-12">
        <header class="big-header text-center">Список подписчики SDO.  </header>
        <div class="panel">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Фильтровать по дате</h3>
                </div>
                <div class="panel-body">
                    <form method="POST" id="search-form" class="form-inline" role="form">

                        <div class="form-group">
                            <label for="start_date">С даты</label>
                            <input type="date" 
                                   class="form-control" 
                                   name="start_date" 
                                   id="start_date" 
                                   placeholder="С даты">
                        </div>
                        <div class="form-group">
                            <label for="end_date">По дату</label>
                            <input type="date" 
                                   class="form-control" 
                                   name="end_date" 
                                   id="end_date" 
                                   placeholder="По дату"
                                   >
                        </div>

                        <button type="submit" class="btn btn-primary">Search</button>
                        <button class="btn btn-grey" 
                                id="fa-eraser"
                                onclick="init_date();return false;"
                                ><i class="fa fa-eraser" aria-hidden="true"></i></button>
                    </form>
                </div>
            </div>
            
            <table class="table-responsive table" id="users_table_subscibers">
                    <thead>
                        <tr>
                            <th>Номер</th>
                            <th>Пользователь</th>
                            <th>Статус</th>
                            <th>Email</th>
                            <th>Тип подписки</th>
                            <th>Дата подписки</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
            </table>
        </div>

    </div>
@endsection

@section('script')
<script src="{{ asset('js/datatables/datatables.js') }}"></script>
<script src="{{ asset('js/datatables/dataTables.buttons.js') }}"></script>
<script src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script>
    <script>
      $(document).ready(function() {           
            var table = $('#users_table_subscibers').DataTable({
                processing: true,
                serverSide: true,
                displayLength: 10,
                ajax: {
                    url: '{!! route("subscribers.getAjaxData") !!}',
                    data: function (d) {
                        d.start_date = $('input[name=start_date]').val();
                        d.end_date = $('input[name=end_date]').val();
                        d._token = '{{ csrf_token() }}';
                    }
                },
                order: [[ 0, "desc" ]],
                columnDefs: [ {
                    "targets": [ 0,1,2],
                    "data": null,
                    "defaultContent": ""
                  } ],
                columns: [
                    {data: 'id', name: 'id', searchable: false},
                    {data: 'name'},
                    {data: 'status'},
                    {data: 'email'},
                    {data: 'type'},
                    {data: 'created_at', name: 'created_at'},
                ],
                initComplete: function () {
                    this.api().columns().columns([1,3]).every(function () {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).appendTo($(column.footer()).empty())
                        .on('change', function () {
                            column.search($(this).val(), false, false, true).draw();
                        });
                    });
                    
                    this.api().columns([2]).every( function () {
                        var column = this;
                        var select = $('<select><option value=""></option></select>')
                            .appendTo( $(column.footer()).empty() )
                            .on( 'change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search( val ? '^'+val+'$' : '', true, false )
                                    .draw();
                            } );
                            select.append( '<option value="0">Отписался</option>' );
                            select.append( '<option value="1">Подписан</option>' );
                    } );
                    
                    this.api().columns([4]).every( function () {
                        var column = this;
                        var select = $('<select><option value=""></option></select>')
                            .appendTo( $(column.footer()).empty() )
                            .on( 'change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search( val ? '^'+val+'$' : '', true, false )
                                    .draw();
                            } );

                        select.append( '<option value="10">Подписка на рассылку</option>' );
                        select.append( '<option value="30">Подписка на блоги</option>' );
                    } );
                    
                },
                language: {
                    processing: "Поиск в базе подписчиков",
                    info:       "показано с _START_ по _END_ подписчиков из _TOTAL_",
                    infoEmpty:  "нет одписчиков",
                    infoFiltered: "(фильтр по _MAX_ подписчиках)",
                    lengthMenu: "По _MENU_ подписчиках",
                },
            });
            
            new $.fn.dataTable.Buttons( table, {
                buttons: [
                    'postCsv'
                ]
            } );
            
            table.buttons().container().prependTo( $('.col-sm-6:eq(0)', table.table().container() ) );

            $('#search-form').on('submit', function(e) {
                table.draw();
                e.preventDefault();
            });

        });
        
        function init_date() {
            $('#start_date').val('').change();
            $('#end_date').val('').change();
            $('#search-form').submit();
        }
        
        function checkDelete() {
            return confirm('Вы уверены что хотите удалить данного подписчика ?');
        }
        
        

    </script>
@endsection

