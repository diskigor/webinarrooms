@extends('Admin.app')
@section('content')
    <div class="col-md-12">
        <header class="big-header text-center">Преимущества</header>
        <div class="panel">
            <!-- Начало модального окна -->
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalAddAdvantages">
                <i class="fa fa-plus"></i> Добавить преимущество
            </button>
            <div class="modal fade" id="modalAddAdvantages" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Форма добавления преимущества</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="preview">
                                    <div id="preview1"></div>
                                    <input type="button" id="reset1" value="Сбросить" class="btn btn-info reset"/>
                                </div>
                            </div>
                            {!! Form::open(array('route'=>'advantages.store','method'=>'POST','enctype'=>'multipart/form-data')) !!}
                            <div class="form-group">
                                {!! Form::label('image_link','Иконка',['class'=>'control-label']) !!}
                                {!! Form::file('image_link',['class'=>'','required']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('advantage','Преимущество',['class'=>'control-label']) !!}
                                {!! Form::text('advantage',null,['class'=>'form-control','required','maxlength'=>'190']) !!}
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-warning" data-dismiss="modal">Закрыть</button>
                                {!! Form::submit('Сохранить',['class'=>'btn btn-success']) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Конец модалльного окна добавления преимущества -->
            <h4 class="text-center">Что дает Вам наше обучение?</h4>
            <div class="benefits">
                @if(!is_null($advantages))
                    @foreach($advantages as $advantage)
                        <div class="benefit">
                            <button type="button" class="btn btn-warning btn-icon" data-toggle="modal"
                                    data-target="{{'#modal_'.$advantage->id}}">
                                <i class="fas fa-pencil-alt"></i>
                            </button>
                            {!! Form::open(array('route'=>['advantages.destroy',$advantage->id],'method'=>'DELETE','class'=>'inline-block')) !!}
                            {!! Form::submit('Удалить',array_merge(['class'=>'btn btn-danger'])) !!}
                            {!! Form::close() !!}
                            <img width="50px" height="50px" src="{{asset($advantage->image_link)}}" alt=""/>
                            {{$advantage->advantage}}
                        </div>
                        <div class="modal fade" id="{{'modal_'.$advantage->id}}" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close"><span
                                                    aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Форма редактирования преимущества</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <img width="200px" height="200px" src="{{asset($advantage->image_link)}}"alt=""/>
                                        </div>
                                        <div class="form-group">
                                        {!! Form::open(array('route'=>['advantages.update',$advantage->id],'method'=>'PUT','enctype'=>'multipart/form-data')) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('new_image','Иконка',['class'=>'control-label']) !!}
                                            {!! Form::file('new_image',['class'=>'']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('advantage','Преимущество',['class'=>'control-label']) !!}
                                            {!! Form::text('advantage',$advantage->advantage,['class'=>'form-control','required','maxlength'=>'190']) !!}
                                        </div>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть
                                        </button>
                                        {!! Form::submit('Сохранить',['class'=>'btn btn-success']) !!}
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>


                    @endforeach
                @else
                    <div class="benefit">
                        Таблица пустая
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('js/imagepreview.js')}} "></script>
    <script type="text/javascript">
        $(function () {
            $('#preview1').imagepreview({
                input: '[name="image_link"]',
                reset: '#reset1',
                preview: '#preview1'
            });
        });
    </script>
@endsection