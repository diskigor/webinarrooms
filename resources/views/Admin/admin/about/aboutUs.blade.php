@extends('Admin.app')
@section('content')
    <div class="col-md-12">
        <header class="big-header text-center">О нас</header>
        @if(isset($about_us))
            <div class="panel">
                <img class="pre" src="{{asset($about_us->image_logo)}}" alt="">
                <div class="preview">
                    <div id="preview1"></div>
                    <br>
                    <input type="button" id="reset1" value="Сбросить" class="btn btn-info reset"/>

                </div>
                <br>
                {!! Form::open(array('route'=>['admin_about_us.update',$about_us->id],'method'=>'PUT','enctype'=>'multipart/form-data')) !!}
                <div class="form-group">
                    {!! Form::label('image_logo','Изображение',array_merge(['class'=>'control-label'])) !!}
                </div>
                <div class="form-group">
                    {!! Form::file('image_logo',['accept'=>"image/jpeg,image/png",'class'=>''])!!}
                </div>
                <div class="form-group">
                    {!! Form::label('about_us','О нас',array_merge(['class'=>'control-label'])) !!}
                    {!! Form::textarea('about_us',$about_us->about_us,['class'=>'form-control']) !!}
                </div>
                {!! Form::submit('Сохранить',['class'=>'btn btn-success']) !!}
                {!! Form::close() !!}
            </div>
        @else
            <div class="panel">
                <div class="preview">
                    <div id="preview1"></div>
                    <input type="button" id="reset1" value="Сбросить" class="btn btn-info reset"/>
                </div>
                {!! Form::open(array('route'=>['admin_about_us.store'],'method'=>'POST','enctype'=>'multipart/form-data')) !!}
                <div class="form-group">
                    {!! Form::label('image_logo','Изображение',array_merge(['class'=>'control-label'])) !!}
                    {!! Form::file('image_logo',['accept'=>"image/jpeg,image/png",'class'=>''])!!}
                </div>
                <div class="form-group">
                    {!! Form::label('about_us','О нас',array_merge(['class'=>'control-label'])) !!}
                    {!! Form::textarea('about_us',null,['class'=>'form-control']) !!}
                </div>
                {!! Form::submit('Сохранить',['class'=>'btn btn-success']) !!}
                {!! Form::close() !!}
            </div>
        @endif
    </div>
@endsection
@section('script')
    <script src="{{asset('js/imagepreview.js')}} "></script>
    <script type="text/javascript">
        $(function () {
            $('#preview1').imagepreview({
                input: '[name="image_logo"]',
                reset: '#reset1',
                preview: '#preview1'
            });
        });
    </script>
@endsection