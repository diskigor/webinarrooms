@extends('layouts.admin')

@section('content')
<div class="col-md-12">
    <div class="panel">
        <h3 class="text-center">Администратор пишет пользователям сайта</h3>
        {!! Form::open(array('route'=>['message_admin'],'method'=>'POST')) !!}
        <div class="form-group">
            {!! Form::label('subject','Создать новую тему')!!}
            {!! Form::text('subject',null,array_merge(['class'=>'form-control','required','maxlength'=>'140'])) !!}
        </div>

        <!--div class="form-group">
            {!! Form::label('all_users','Всем пользователям',array_merge(['class'=>'control-label'])) !!}
            {!! Form::checkbox('all_users', 1) !!}
        </div-->


        <div class="form-group">
            {!! Form::label('select_userss','Кому создаётся сообщение?',array_merge(['class'=>'control-label'])) !!}
            <p><input name="select_userss" type="radio" value="all_users"> Всем пользователям?</p>
            <p><input name="select_userss" type="radio" value="group_users"> Группе пользователей?</p>
        </div>

        <div class="form-group select_userss group_users hide">
            {!! Form::label('group_users','Группа пользователей',array_merge(['class'=>'control-label'])) !!}
            {!! Form::select('group_users',$ugroups->pluck('name','id'),null,['class'=>'form-control','placeholder'=>'Выберите группу пользователей']) !!}
        </div>
        
        <div class="form-group">
            <p><input name="select_userss" type="radio" value="roles_users"> По роли пользователей?</p>
        </div>
        <div class="form-group select_userss roles_users hide">
            {!! Form::label('roles_users','Роль пользователей',array_merge(['class'=>'control-label'])) !!}
            {!! Form::select('roles_users',$uroles->pluck('name','id'),null,['class'=>'form-control','placeholder'=>'Выберите роль пользователей']) !!}
        </div>
        
        <div class="form-group">
            <p><input name="select_userss" type="radio" value="one_user"> Одному пользователю?</p>
        </div>
        <div class="form-group select_userss one_user hide">
            {!! Form::label('one_user','Один из пользователей',array_merge(['class'=>'control-label'])) !!}
            {!! Form::select('one_user',$uusers->pluck('idFullNameEmail','id'),null,['class'=>'form-control','placeholder'=>'Выберите пользователя']) !!}
        </div>

        
        <div class="form-group">
            {!! Form::label('message','Сообщение') !!}
            {!! Form::textarea('message',null,array_merge(['class'=>'form-control'])) !!}
        </div>
        {!! Form::submit('Отправить',array_merge(['class'=>'btn btn-success'])) !!}
        {!! Form::close() !!}
        <br/>
        <a href="{{ route('message.index') }}" class="btn btn-black">Назад к списку сообщений</a>
    </div>
</div>
@endsection

@push('stack_scripts')
<script type="text/javascript">
$('input[name="select_userss"]').on('change',function(e){
   console.log(this.value); 
   $('.select_userss').addClass('hide');
   $('.'+this.value).removeClass('hide');
   
   
});

</script>
@endpush