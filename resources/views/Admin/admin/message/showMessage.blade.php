@extends('layouts.admin')

@section('content')
    <div class="messenger">

        <h3 style="text-align:center;">Сообщения от польлзователя {{$user->name}}</h3>
        <div id="updateble_list">
        @if($new_collects)
            @foreach($new_collects as $message)
                @if(array_key_exists('subject',$message))
                    <!-- ===== чужое сообщение ===== -->
                        <div class="single-message">
                            <div class="user-image">
                            {{--<a href="#"><!-- ссылка на собеседника -->--}}
                            <!-- аватар собеседника -->
                                    <img src="{{ $user->avatar }}" alt="{{$user->name}}"/>
                                {{--</a>--}}
                            </div>
                            <div class="message-text">
                                <div class="msg-box">
                                    <div>{{$user->name}}</div><!-- имя собеседника -->
                                    <div>{{$message['subject']}}</div><!-- имя собеседника -->
                                    <div>{{$message['message']}}</div><!-- тектст сообщения -->
                                    <div class="date">{{$message['created_at']}}</div>
                                </div>
                            </div>
                        </div>
                @else
                    <!-- ===== моё сообщение ===== -->
                        <div class="single-message my">
                            <div class="user-image">
                                <img src="{{ Auth::user()->avatar }}" alt="{{Auth::user()->name}}"/>
                                <!-- собственный аватар пользователя -->
                            </div>
                            <div class="message-text">
                                <div class="msg-box">
                                    <div>{{Auth::user()->name}}</div><!-- собственное имя пользователя -->
                                    <div>{!! $message['message'] !!}<!-- тектст сообщения -->
                                        <div class="date">{{$message['created_at']}}</div>
                                    </div>
                                    @if($message['status'] == 1)
                                        <div class="readed">Прочитано</div>
                                @endif
                                <!-- статус сообщения - появляется когда абонент прочитал это сообщение -->
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            @endif
        </div>
        <!-- ===== форма отправки новых сообщений ===== -->
        {!! Form::open(array('route'=>'answer_users','method'=>'POST')) !!}
        <input type="hidden" name="user_id" value="{{$user->id}}">
        <div class="form-group no-editor">
            {!! Form::label('text_message','Ответить пользователю '.$user->name,['class'=>'label-control']) !!}
            {!! Form::textarea('text_message',null,array_merge(['class'=>'form-control '])) !!}
        </div>
        <a href="{{route('message.index')}}" class="btn btn-default">Закрыть</a>
        <button type="submit" class="btn btn-primary">Отправит</button>
        {!! Form::close() !!}
    </div>

@endsection

@push('stack_scripts')
    <script>
        $('#updateble_list').scrollTop($('#updateble_list')[0].scrollHeight);
    </script>
@endpush