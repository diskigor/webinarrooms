@extends('layouts.admin')

@section('content')
    <div class="col-md-12">

        <header class="big-header text-center">Сообщения администратору</header>
        <div class="panel">

            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover"
                       id="dataTables-example">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ID</th>
                        <th>Пользователь</th>
                        <th>Email</th>
                        <th>Роль</th>
                        <th>Teма</th>
                        <th>{{trans('content.actions')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($messages as $message)
                        <tr class="odd gradeX">
                            <td>{{ $loop->iteration }}</td>
                            <td>{{$message->id}}
                                @if($message->status==0)
                                <img style="max-height: 50px" src="{{asset('images/new_message.gif')}}" alt="">
                                @endif
                            </td>
                            <td>{{$message->author->name}}</td>
                            <td>{{$message->author->email}}</td>
                            <td>
                                @if($message->author->role->name==='Cadet')
                                    Курсант
                                @elseif($message->author->role->name==='Сurator')
                                    Куратор
                                @else
                                    Лектор
                                @endif
                            </td>
                            <td class="center">{{$message->subject}}</td>
                            <td>
                                {!! Form::open(array('route'=>['message.show',$message->id], 'method'=>'GET','class'=>'inline-block')) !!}
                                {!! Form::button('<i class="fa fa-eye"></i>',['class'=>'btn btn-info','type'=>'submit']) !!}
                                {!! Form::close() !!}

                                {!! Form::open(array('route'=>['message.destroy',$message->id],'method'=>'DELETE','class'=>'inline-block')) !!}
                                {!! Form::button('<i class="fa fa-times"></i>',['class'=>'btn btn-danger','type'=>'submit','onclick'=>'return checkDelete()']) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $messages->links() }}
            </div>
        </div>

    </div>

@endsection

@push('stack_scripts')
    <script language="JavaScript" type="text/javascript">
        function checkDelete() {
            return confirm('Вы уверены что хотите удалить тему сообщений ?');
        }
    </script>
@endpush