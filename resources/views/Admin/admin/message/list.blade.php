@extends('layouts.admin')

@section('content')
    <div class="col-md-12">

        <header class="big-header text-center">Сообщения администратору</header>
        <div class="panel">

            <div class="panel-body">
                
                <button id="delete_multi_message" 
                        class="btn btn-danger pull-left"
                        data-action="{{route('delete_multi_message')}}"><i class="fa fa-trash"></i> Удалить выбранные</button>
                
                <a href="{{ route('message.create') }}" class="btn btn-success pull-right">Создать сообщение</a>
                <table width="100%" class="table table-striped table-bordered table-hover"
                       id="dataTables-example">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th><input type="checkbox" name="check_all"></th>
                        <th></th>
                        <th>Пользователь</th>
                        <th>Email</th>
                        <th>Роль</th>
                        <th>Teма</th>
                        <th>{{trans('content.actions')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($messages as $message)

                        <tr class="odd gradeX {{ $message->isAdmin?'text-danger':'' }}">
                            <td>{{ $loop->iteration }}</td>
                            <td style="width: 10px;">
                                <input type="checkbox" name="messagies_id[]" value="{{$message->id}}">
                            </td>
                            <td class="text-center">
                                @if($message->AdminStatusMessage == 'send')
                                    <i class="fa fa-paper-plane fa-2x" aria-hidden="true" title="Отправлено"></i>
                                @elseif($message->AdminStatusMessage == 'read')
                                    <i class="far fa-envelope-open fa-1x" aria-hidden="true" title="Прочитано"></i>
                                @elseif($message->AdminStatusMessage == 'new')
                                    <i class="far fa-envelope fa-2x text-success" aria-hidden="true" title="Новое сообщение"></i>
                                @endif
                            </td>
                            <td>{{$message->author->name}}</td>
                            <td>{{$message->author->email}}</td>
                            <td>
                                @if($message->author->role->name==='Cadet')
                                    Курсант
                                @elseif($message->author->role->name==='Сurator')
                                    Куратор
                                @else
                                    Лектор
                                @endif
                            </td>
                            <td class="center">{{$message->subject}}</td>
                            <td>
                                {!! Form::open(array('route'=>['message.show',$message->id], 'method'=>'GET','class'=>'inline-block')) !!}
                                {!! Form::button('<i class="fa fa-eye"></i>',['class'=>'btn btn-info','type'=>'submit']) !!}
                                {!! Form::close() !!}

                                {!! Form::open(array('route'=>['message.destroy',$message->id],'method'=>'DELETE','class'=>'inline-block')) !!}
                                {!! Form::button('<i class="fa fa-times"></i>',['class'=>'btn btn-danger','type'=>'submit','onclick'=>'return checkDelete()']) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $messages->links() }}
            </div>
        </div>

    </div>

@endsection

@push('stack_scripts')
    <script language="JavaScript" type="text/javascript">
        function checkDelete() {
            return confirm('Вы уверены что хотите удалить тему сообщений ?');
        }
        function checkDeleteSelect() {
            return confirm('Вы уверены что хотите удалить выбранные сообщения ?');
        }
        $('input[name = "check_all"]').on('click',function(){
            var check_all = !($(this).prop("checked"));
            
            if (check_all) {
                $('input[name = "messagies_id[]"]').each(function(){
                    var clikeds = !($(this).prop("checked"));
                    if (clikeds) {
                        $(this).parent().click();
                    }
                    
                });
            }
        });
        
        $('input[name = "messagies_id[]"]').on('click',function(){
            var messagies_id = $(this).prop("checked");
            //console.log('messagies_id',messagies_id);
            if (messagies_id) {
                var check_all = ($('input[name = "check_all"]').first().prop("checked"));
                //console.log('check_all',check_all);
                if (check_all) {$('input[name = "check_all"]').first().parent().click();}
            }
        });
        
        $('#delete_multi_message').on('click',function(){
           var l_action = $(this).data('action');
           var l_chek = $('input[name = "messagies_id[]"]:checked').serialize();
           if(checkDeleteSelect()) {
                $.ajax({
                    url: l_action,
                    type: "POST",
                    dataType : "json",
                    data:l_chek,
                    success: function (data) { 
                        if (data.error){
                            swal(data.error);
                        }
                        if (data.success){
                            swal(data.success);
                            setTimeout(function() { window.location.reload(); }, 3000);
                        }
                    }               
                });
            }
           return false;
        });
        
        
    </script>
@endpush