@extends('layouts.admin')

@section('content')
<div class="col-md-12">
    <div class="messenger">
        <h3 style="text-align:center;">Сообщения от пользователя {{$callback->author->name}}</h3>
        <h4 style="text-align:center;">Тема: {{ $callback->subject }}</h4>
        <div id="updateble_list">
        @if ($callback->chats->isEmpty())
            <div class="single-message">
                Ещё нет сообщений от пользователя.
            </div>
        @else
            @foreach($callback->chats as $message)
                @if(!$message->sender->isAdmin())
                    <!-- ===== чужое сообщение ===== -->
                        <div class="single-message">
                            <div class="user-image">
                            {{--<a href="#"><!-- ссылка на собеседника -->--}}
                            <!-- аватар собеседника -->
                                    <img src="{{ $message->sender->avatar }}" alt="{{$message->sender->name}}"/>
                            </div>
                            <div class="message-text">
                                <div class="msg-box">
                                    <div>{{ $message->sender->name }}</div><!-- имя собеседника -->
                                    <div>{{ $message->message }}</div><!-- тектст сообщения -->
                                    <div class="date">{{ $message->created_at }}</div>
                                </div>
                            </div>
                        </div>
                @else
                    <!-- ===== моё сообщение ===== -->
                        <div class="single-message my">
                            <div class="user-image">
                                    <img src="{{ Auth::user()->avatar }}" alt="{{Auth::user()->name}}"/>
                            </div>
                            <div class="message-text">
                                <div class="msg-box">
                                    <div>{{ Auth::user()->name }}</div><!-- собственное имя пользователя -->
                                    <div>{{ $message->message}}<!-- тектст сообщения -->
                                        <div class="date">{{ $message->created_at }}</div>
                                    </div>
                                    @if( $message->status == 1)
                                        <div class="readed">Прочитано</div>
                                    @else
                                        <div class="readed">Отправлено</div>
                                    @endif
                                <!-- статус сообщения - появляется когда абонент прочитал это сообщение -->
                                </div>
                            </div>
                        </div>
                @endif
            @endforeach
        @endif
        </div>
        <!-- ===== форма отправки новых сообщений ===== -->
       
        {!! Form::open(array('route'=>'message.store','method'=>'POST')) !!}
            <input type="hidden" name="callback_id" value="{{ $callback->id }}">
            <div class="form-group no-editor">
                {!! Form::label('text_message','Ответить пользователю '.$callback->author->name,['class'=>'label-control']) !!}
                {!! Form::textarea('text_message',null,array_merge(['class'=>'form-control '])) !!}
            </div>
            
            <button type="submit" class="btn btn-primary">Отправить сообщение</button>
        {!! Form::close() !!}
    </div>
    <hr>
    <a href="{{route('message.index')}}" class="btn btn-default">Вернуться к списку сообщений</a>
</div>
@endsection

@push('stack_scripts')
    <script>
        $('#updateble_list').scrollTop($('#updateble_list')[0].scrollHeight);
    </script>
@endpush