@extends('layouts.admin')

@section('content')
    <div class="col-md-12">
        <header class="big-header text-center">Наши контакты</header>
        @if(isset($contacts))
            <div class="panel">
                {!! Form::open(array('route'=>['admin_contacts.update',$contacts->id],'method'=>'PUT')) !!}
                <div class="form-group">
                    {!! Form::label('number_one','Номер 1',['class'=>'control-label']) !!}
                    {!! Form::text('number_one',$contacts->number_one,['class'=>'form-control','type'=>'tel']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('number_two','Номер 2',['class'=>'control-label']) !!}
                    {!! Form::text('number_two',$contacts->number_two,['class'=>'form-control','type'=>'tel']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('email_one','Почта 1',['class'=>'control-label']) !!}
                    {!! Form::email('email_one',$contacts->email_one,['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('email_two','Почта 2',['class'=>'control-label']) !!}
                    {!! Form::email('email_two',$contacts->email_two,['class'=>'form-control']) !!}
                </div>
                {!! Form::submit('Сохранить',['class'=>'btn btn-blue']) !!}
                {!! Form::close() !!}
            </div>
        @else
            <div class="panel">
                {!! Form::open(array('route'=>'admin_contacts.store','method'=>'POST')) !!}
                <div class="form-group">
                    {!! Form::label('number_one','Номер 1',['class'=>'control-label']) !!}
                    {!! Form::text('number_one',null,['class'=>'form-control','type'=>'tel']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('number_two','Номер 2',['class'=>'control-label']) !!}
                    {!! Form::text('number_two',null,['class'=>'form-control','type'=>'tel']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('email_one','Почта 1',['class'=>'control-label']) !!}
                    {!! Form::email('email_one',null,['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('email_two','Почта 2',['class'=>'control-label']) !!}
                    {!! Form::email('email_two',null,['class'=>'form-control']) !!}
                </div>
                {!! Form::submit('Сохранить',['class'=>'btn btn-blue']) !!}
                {!! Form::close() !!}
            </div>
        @endif
    </div>
@endsection