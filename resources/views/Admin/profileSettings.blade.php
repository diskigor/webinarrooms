@extends('layouts.admin')
@section('content')
<div class="col-md-12 profilesettings">
    <?php $title_page = 'Настройки профиля'; ?>
        <div class="row">
            <div class=" col-md-11">
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="70"
                    aria-valuemin="0" aria-valuemax="100" style="width:{{ $full_profile_procent }}%;">
                        <span class="progress-complete">{{ $full_profile_procent }}% Заполнено</span>
                    </div>
                </div>
            </div>
            <div class="col-md-1 user-progress-profile">
                <i class="fa fa-info-circle" aria-hidden="true" data-toggle-progress="tooltipprogress"></i>
            </div>
            <div class="col-md-6">
                <h4 class="">Настройки профиля</h4>
                <div class="user-photo" style="background-image: url('{{ $profile->avatar }}')">
                    <div id="preview1"><button type="button" id="reset1" title="Отмена" class="btn btn-danger btn-icon">
                            Отмена
                        </button></div>
                    <form action="{{route('delete_avatar')}}" method="post">
                            {{csrf_field()}}
                            <button type="submit" title="Удалить Фото профиля" class="btn btn-danger del-ava"><i class="fa fa-times" aria-hidden="true"></i></button>
                        </form>
                    {!! Form::open(array('route'=>['change_profile_avatar',$profile->id],'method'=>'POST','enctype'=>'multipart/form-data')) !!}
                    <div class="file-upload">
                        <label>
                             <input type="file" name="avatar">
                             <span title="Загрузить Фото профиля"><i class="fa fa-upload" aria-hidden="true"></i></span>
                        </label>
                   </div>
                    <script>
                        $(document).ready( function() {
                            $(".file-upload input[type=file]").change(function(){
                                 var filename = $(this).val().replace(/.*\\/, "");
                                 $("#filename").val(filename);
                            });
                        });
                    </script>
                </div>
                <div class="form-group text-center">
                    <button type="submit" title="Сохранить Фото профиля" class="btn btn-blue" style="margin-top: 22px;">Сохранить Фото профиля</button>
                </div>
                {!! Form::close() !!}
                {!! Form::open(array('route'=>['change_prof',$profile->id],'method'=>'POST')) !!}
                <div class="row">
                    <div class="form-group col-md-4">
                        {!! Form::label('name','Имя',array_merge(['class'=>'form-label'])) !!}
                        {!! Form::text('name',$profile->name,array_merge(['class'=>'form-control','maxlength'=>'15','minlength'=>'4','required'])) !!}
                    </div>
                    <!-- Начало дополнительных полей -->
                    <div class="form-group col-md-4">
                        <label for="surname">Фамилия</label>
                        <input type="text" class="form-control" value="{{$profile->surname}}" name="surname">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="middle_name">Отчество</label>
                        <input type="text" name="middle_name" value="{{$profile->middle_name}}"
                               class="form-control">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="nickname">Телефон</label>
                        <input type="text" class="form-control" value="{{$profile->nickname}}" name="nickname">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="gender">Пол</label>
                        <div class="col-md-12 radio-gender-block">
                        @if($profile->gender==='man')
                        <label class="radio-gender">
                            <input type="radio" name="gender" value="man" checked>
                            <div class="radio__text">Мужчина</div>
                        </label>
                        <label class="radio-gender">
                            <input type="radio" name="gender" value="woman" >
                            <div class="radio__text">Женщина</div>
                        </label>
                        @else
                        <label class="radio-gender">
                            <input type="radio" name="gender" value="man" >
                            <div class="radio__text">Мужчина</div>
                        </label>
                        <label class="radio-gender">
                            <input type="radio" name="gender" value="woman"  checked>
                            <div class="radio__text">Женщина</div>
                        </label>
                        @endif
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    {!! Form::submit('Сохранить',['class'=>'btn btn-blue']) !!}
                </div>
                
                {!! Form::close() !!}
            </div>
            <div class="col-md-6">
                <h4 class="">Форма изменения пароля</h4>
                {!! Form::open(array('route'=>['profile.update',$profile->id],'method'=>'PUT')) !!}
                <div class="form-group">
                    {!! Form::label('old_pass','Старый пароль',['class'=>'control-label'])!!}
                    {!! Form::password('old_pass',array_merge(['class'=>'form-control','required'])) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('new_pass','Новый пароль',['class'=>'control-label']) !!}
                    {!! Form::password('new_pass',array_merge(['class'=>'form-control','required','minlength'=>'6'])) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('confirm_pass','Подтвердите новый пароль',['class'=>'control-label']) !!}
                    {!! Form::password('confirm_pass',array_merge(['class'=>'form-control','required','minlength'=>'6'])) !!}
                </div>
                <div class="form-group text-center">
                    {!! Form::submit('Сменить пароль',['class'=>'btn btn-blue']) !!}
                </div>
                {!! Form::close() !!}
                {!! Form::open(array('route'=>['change_user_email',$profile->id],'method'=>'POST')) !!}
                <div class="form-group">
                    <label for="email">Текущий e-mail</label>
                    <input type="text" value="{{$profile->email}}"
                           class="form-control" disabled>
                </div>
                <div class="form-group">
                    <label for="email">Новый e-mail</label>
                    <input type="text" name="email" value=""
                           class="form-control">
                </div>
                <div class="text-center">
                    {!! Form::submit('Сохранить',['class'=>'btn btn-blue']) !!}
                </div>
                
                {!! Form::close() !!}
                
            </div>
        </div>
        <hr>
        
        <div class="row">
            
            <div class="col-md-6">
                <h4 class="">Личные данные</h4>
                {!! Form::open(array('route'=>['change_user_data',$profile->id],'method'=>'POST')) !!}
                <div class="row">
                    <div class="form-group col-md-4">
                        <label for="date_of_birth">Дата рождения</label>
                        <input type="date" name="date_of_birth" value="{{$profile->date_of_birth}}" class="form-control">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="country">Страна</label>
                        <input type="text" value="{{$profile->country}}" class="form-control" name="country">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="city">Город</label>
                        <input type="text" name="city" value="{{$profile->city}}" class="form-control">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-4">
                        <label for="education">Образование</label>
                        <input type="text" name="education" value="{{$profile->education}}" class="form-control">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="profession">Профессия</label>
                        <input type="text" name="profession" value="{{$profile->profession}}" class="form-control">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="m_income">Доход</label>
                        <input type="number" class="form-control" value="{{$profile->m_income}}" name="m_income"
                               min="0">
                    </div>
                </div>
                <div class="text-center">
                    {!! Form::submit('Сохранить',['class'=>'btn btn-blue']) !!}
                </div>
                
                {!! Form::close() !!}
            </div>
            <div class="col-md-6">
                <h4>Уведомления</h4>
                @if(Auth::user())
                <div class="form-group">
                    <label for="m_income">Реферальная ссылка</label>
                    <input type="text" class="form-control" value="{{Auth::user()->getReferalLink() }}" disabled="">
                </div>
                @endif
                <label>Доступные подписки</label>
                <div id="js_loader_subscribers"></div>
                {!! Form::open(array('route'=>['profile.destroy',$profile->id], 'method'=>'DELETE','class'=>'delete-profile')) !!}
                {!! Form::button('<!--i class="fa fa-times"></i--> Удалить профиль',['class'=>'btn btn-blue','type'=>'submit','onclick'=>'return checkDelete()']) !!}
                {!! Form::close() !!}
            </div>
        </div>
       

    
    

</div>
@endsection
@section('script')
<script src="{{asset('js/imagepreviewprof.js')}} "></script>
<script type="text/javascript">
$(function () {
    $('#preview1').imagepreview({
        input: '[name="avatar"]',
        reset: '#reset1',
        preview: '#preview1'
    });

    $('#js_loader_subscribers').on('change', "input[name ^= set_]", function (e) {
        var x_id = $(this).data('id');
        var c_ch = this.checked;
        if (!x_id) { return; }
        $.ajax({
            type: "PUT",
            url: "{{ route('subscribers.update',Auth::user()->id) }}",
            data: "x_id=" + x_id + "&c_ch=" + c_ch,
            success: function (msg) {
                if (msg.success) { swal(msg.success); }
            },
            error: function (msg) {
                console.log('error', msg);
            },
            complete: function () {
                reload_subscribers_block();
            }
        });
    })

    
    function reload_subscribers_block() {
        var block = $('#js_loader_subscribers');
        block.load("{{ route('subscribers.edit',Auth::user()->id) }}", function () {
            $(this).find('input[type="checkbox"],input[type="radio"]').styler();
        })
    }
    
    reload_subscribers_block();
});

function checkDelete() {
    return confirm('Вы уверены что хотите удалить свой профиль?');
}

</script>
@endsection