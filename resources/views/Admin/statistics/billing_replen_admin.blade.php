@extends('layouts.admin')

@section('content')
    <div class="col-md-12">
        <header class="big-header text-center">Статистика пополнений администратором SDO</header>
        <div class="panel">
            <h4 class="text-center">Статистика</h4>
            <div class="row">
                <div>
                    <form action="{{route('search_email_repl_admin')}}" method="post">
                        <div class="input-group">
                        <span class="input-group-btn">
                            <a type="submit" class="btn btn-danger" href="{{route('admin_repl_statistics')}}">Сбросить</a>
                        </span>
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="email" class="form-control" name="email_user" id="searchEmail"
                                   placeholder="Поиск по почте">
                            <span class="input-group-btn">
                            <button class="btn btn-blue" type="submit">Поиск</button>
                        </span>
                        </div>
                    </form>
                </div>
            </div>
            <table class="table table-responsive">
                <thead>
                <tr>
                    <th>№</th>
                    <th>Пользователь</th>
                    <th>Email</th>
                    <th>Сумма</th>
                    <th>Валюта</th>
                    <th>Описание</th>
                    <th>Админ</th>
                    <th>Дата</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1;?>
                @if(count($billing_orders)!= 0)
                    @foreach($billing_orders as $order)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{ isset($order->user)?$order->user->name:$order->user_id }}</td>
                            <td>{{ isset($order->user)?$order->user->email:$order->user_id }}</td>
                            <td>{{$order->transfer_sum}}</td>
                            <td>{{$order->currency}}</td>
                            <td>{{$order->desc}}</td>
                            @if( isset($order->admin) )
                                <td>{{$order->admin->name}}</td>
                            @else
                                <td></td>
                            @endif
                            <td>{{$order->created_at}}</td>
                            <td>
                                {!! Form::open(array('route'=>['del_order_admin_prel',$order->id],'method'=>'post')) !!}
                                <button type="submit" class="btn btn-danger">Удалить</button>
                                {!! Form::close()!!}
                            </td>

                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
            {{ $billing_orders->links() }}
        </div>
    </div>
@endsection