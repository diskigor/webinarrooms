@extends('layouts.admin')

@section('content')

<link rel="stylesheet" href="{{ asset('css/datatables.css') }}">


    <div class="col-md-12">
        <header class="big-header text-center">Статистика пополнений</header>        
        <div class="row">
            <div class="row">
                <table class="table table-responsive"  id="users-table-orders">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Пользователь</th>
                            <th>Email</th>
                            <th width="70">Сумма</th>
                            <th>Номер платежа</th>
                            <th>Описание</th>
                            <th>Админ</th>
                            <th>Дата</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        
    </div>



@endsection

@section('script')
<script src="{{ asset('js/datatables/datatables.js') }}"></script>
<script src="{{ asset('js/datatables/dataTables.buttons.js') }}"></script>
<script src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script>
    <script>
      $(document).ready(function() {           
            var table = $('#users-table-orders').DataTable({
                processing: true,
                serverSide: true,
                displayLength: 10,
                ajax: '{!! route("biling_statistics.getAjaxData") !!}',
                order: [[ 0, "desc" ]],
                columnDefs: [ {
                    "targets": [ 0,1,2,3,4,5,6,7],
                    "data": null,
                    "defaultContent": ""
                  } ],
                columns: [
                    {data: 'id', name: 'id', searchable: false},
                    {data: 'user.name'},
                    {data: 'user.email'},
                    {data: 'transfer_sum'},
                    {data: 'number_inv', name: 'number_inv'},
                    {data: 'desc', name: 'desc'},
                    {data: 'admin.name', name: 'desc'},
                    {data: 'created_at', name: 'created_at'},
                ],
                initComplete: function () {
                    this.api().columns().columns([1,2,5,6,7]).every(function () {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).appendTo($(column.footer()).empty())
                        .on('change', function () {
                            column.search($(this).val(), false, false, true).draw();
                        });
                    });
                },
                language: {
                    processing: "Поиск в базе",
                    info:       "показано с _START_ по _END_ пополнений из _TOTAL_",
                    infoEmpty:  "нет пополнений",
                    infoFiltered: "(фильтр по _MAX_ пополнений)",
                    lengthMenu: "По _MENU_ пополнений",
                },
            });
            
            new $.fn.dataTable.Buttons( table, {
                buttons: [
                    'postExcel', 'postCsv'
                ]
            } );
            
            table.buttons().container().prependTo( $('.col-sm-6:eq(0)', table.table().container() ) );
        });

    </script>
@endsection



