@extends('layouts.admin')

@section('content')
    <div class="col-md-12">
        <header class="big-header">Статистика пополнений счетов пользователей</header>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <h4 class="text-center">Статистика</h4>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <form action="{{route('search_email_billig')}}" method="GET" class="input-group">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="text" class="form-control" name="email"
                                       placeholder="Сортировка по email" required>
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-blue  btn-icon"
                                    ><i class="fa fa-search"
                                        aria-hidden="true"></i></button>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <form action="{{route('search_inv_billig')}}" method="GET" class="input-group">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="text" class="form-control" name="inv"
                                       placeholder="Сортировка по номеру платежа" required>
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-blue  btn-icon"><i class="fa fa-search"
                                                                                            aria-hidden="true"></i>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <table class="table table-responsive">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Пользователь</th>
                            <th>Email</th>
                            <th>Сумма</th>
                            <th>Валюта</th>
                            <th>Номер платежа</th>
                            <th>Описание</th>
                            <th>Дата</th>
                        </tr>
                        </thead>
                        <tbody class="billing">
                        <?php $i = 1;?>
                        @if(count($billing_orders)!= 0)
                            @foreach($billing_orders as $order)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{ isset($order->user)?$order->user->name:$order->user_id }}</td>
                                    <td>{{ isset($order->user)?$order->user->email:$order->user_id }}</td>
                                    <td>{{$order->transfer_sum}}</td>
                                    <td>{{$order->currency}}</td>
                                    <td>{{$order->number_inv}}</td>
                                    <td>{{mb_strimwidth($order->desc,0,50,'...')}}</td>
                                    <td>{{date('d.m.Y', strtotime($order->created_at))}}</td>
                                    @if(Auth::user()->isAdmin())
                                        <td><i class="fa fa-lock" aria-hidden="true"></i> 
                                            <!-- 
                                            {!! Form::open(array('route'=>['biling_statistics.destroy',$order->id],'method'=>'DELETE')) !!}
                                            <button type="submit" class="btn btn-danger">Удалить</button>
                                            {!! Form::close()!!}
                                            -->
                                        </td>
                                    @else
                                        <td><i class="fa fa-lock" aria-hidden="true"></i></td>
                                    @endif
                                </tr>
                            @endforeach
                        @else
                            На данный момент нет пополнений счетов.
                        @endif
                        </tbody>
                    </table>

                    {{ $billing_orders->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection