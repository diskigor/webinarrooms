{{-- $user, $refferals--}}
@extends('layouts.admin')
@section('content')
<?php $title_page='Реферальная система'; ?>
<div class="col-md-12 referal-system-page">
    <div class="row">
        <div class="col-md-6">
            <div class=" referal-system-fblock">
                <h4>Зарабатывайте от 15 КЭЛ приводя друзей в ШЭЛ</h4>
                <div class="col-md-6">
                    <span class="">{{ $refferals->count() }}</span>
                    <p>Всего рефералов</p>
                </div>
                <div class="col-md-6">
                    <span class="">{{ Auth::user()->getMyReferalsAllBonus() }}</span>
                    <p>Ваш доход</p>
                </div>
                <i class="fas fa-info-circle" aria-hidden="true" data-toggle-referal="tooltiprefpay"></i>
            </div>
        </div>
        <div class="col-md-6">
            <div class="referal-system-sblock">
                <h4>Реферальная схема</h4>
                <img src="{{asset('images/referals-scheme.jpg')}}" class="img-responsive"/>
                <a href="" data-toggle="modal" data-target="#refModal"><i class="fas fa-info-circle" aria-hidden="true" data-toggle-referal="tooltiprefscheme"></i></a>
            </div>
        </div>
        <!-- refModal -->
        <div class="modal fade" id="refModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Реферальная система ШЭЛ</h4>
                    </div>
                    <div class="modal-body">
                        <p>
                            Реферальная система позволяет получать КЭЛЫ от приведенных вами людей, которые зарегистрировались по вашей реферальной ссылке и внесли вступительный взнос.
                        </p>
                        <p>
                            Реферальная система многоуровневая. Всего 3 уровня:
                            <ul>
                                <li>1 - 5%</li>
                                <li>2 - 2%</li>
                                <li>3 - 0,5</li>
                            </ul>
                        </p>
                        <p>
                            <ul>
                                <li>Когда Вы приглашаете пользователей по своей реферальной ссылке, для Вас это будут рефералы 1-ого уровня, за их работу начисляется 5% от оплаты вступительного взноса.</li>
                                <li>Когда Ваши рефералы 1-ого уровня, начинают приглашать уже для себя рефералов, то для Вас это будут рефералы 2-го уровня и за их взнос Вам начисляется 2% от суммы.</li>
                                <li>И соответственно, когда рефералы 2-ого уровня приглашают себе рефералов, для Вас они становятся рефералами 3-его уровня и за их работу Вы получаете 0,5 % от суммы.</li>
                            </ul>
                        </p>
                        <p>
                            Пример: Вы привели 2 человека и каждый из них привел еще по одному человеку. Все внесли вступительный взнос 350 евро. В итоге Вы получаете по 5% от 1-го уровня, сто составит по 18 евро и по 2% от второго уровня, что составит 7,2 евро. Всего Вы получите себе на счет 
                        </p>
                        <p>
                            Реферальная система доступна всем пользователям (особенно не членам клуба)!
                        </p>
                        <p>
                            Приведи друга и пусть друг приведет друга.
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- refModal -->
        <div class="col-md-12 referal-link">
            <form class="form-inline">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-files-o" aria-hidden="true"></i></div>
                        <input type="text" class="form-control" id="code" value="{{Auth::user()->getReferalLink() }}" placeholder="{{Auth::user()->getReferalLink() }}" readonly="">
                    </div>
                </div>
                <input type="button" class="btn btn-blue btn-copy" id="copy-skidka" value="Скопировать">
            </form>
        </div>
        <div class="col-md-12 referal-table">
            <h4>Реферальная статистика</h4>
            <!--div id="users-table-referals"-->
                <table class="table table-striped table-responsive">
                    @if($refferals->isEmpty())
                        <tbody>
                            <tr>
                                <td><h4 class="text-center">У Вас ещё нет рефералов</h4></td>
                            </tr>
                        </tbody>
                    @else
                    <thead>
                        <tr>
                            <th>№</th>
                            <th>Пользователь</th>
                            <th>Дата регистрации</th>
                            <th>Уровень</th>
                            <th>Внесена сумма</th>
                            <th>Доход</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($refferals as $ref)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $ref->fullFIO }}</td>
                            <td>{{ $ref->created_at }}</td>
                            <td>{{ $ref->x_referal_level }}</td>
                            <td>{{ $ref->x_referal_summa }}</td>
                            <td>{{ $ref->x_referal_bonus }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
            <!--/div-->
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="{{ asset('js/datatables/datatables.min.js') }}"></script>
<script>
  $(document).ready(function() {

        $('#users-table-referals').DataTable();
    });
//кнопка копировать
 $(function() {
    $('#copy-skidka').click(function() {
        $('#code')[0].select(); 
        document.execCommand('copy');
        $('#code').append(' ');
        $('#code').val().slice(0, -1);
        return swal({
            title: "Реферальная ссылка скопирован!", 
            html:  true,  
            text:  "Ваша ссылка: " + $('#code').val(),
            confirmButtonText: "Код сохранен", 
        });
    });
  });
</script>
@endsection
