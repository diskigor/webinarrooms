{{-- $user, $history--}}
@extends('layouts.admin')
@section('content')
<div class="col-md-12">
    <header class="big-header text-center">РЕФЕРАЛЬНЫЕ НАЧИСЛЕНИЯ, {{ $user->isAdmin()?' ВСЕ':$user->fullFIO }}</header>

    

    <div class="panel">        
        @if($history->isEmpty())
            <h3 class="text-center">Ещё не было реферальных начислений</h3>
        @elseif($user->isAdmin())
            <table class="table table-responsive" id="users-table-referals">
                <thead>
                    <tr>
                        <th>№</th>
                        <th>от кого</th>
                        <th>кому</th>
                        <th>Уровень</th>
                        <th>Описание</th>
                        <th>Дата начисления</th>
                        <th>Тест.приход</th>
                        <th>Доход</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($history as $ref)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $ref->user->idFullNameEmail }}</td>
                        <td>{{ $ref->uparent->idFullNameEmail }}</td>
                        <td>{{ $ref->rule?$ref->rule->level:'' }}</td>
                        <td>{{ $ref->nameRule }}</td>
                        <td>{{ $ref->created_at }}</td>
                        <td>{{ $ref->operationValueCurrency }}</td>
                        <td>{{ $ref->valueCurrency }}</td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        @else
            <table class="table table-responsive" id="users-table-referals">
                <thead>
                    <tr>
                        <th>№</th>
                        <th>от кого</th>
                        <th>Уровень</th>
                        <th>Описание</th>
                        <th>Дата начисления</th>
                        <th>Тест.приход</th>
                        <th>Доход</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($history as $ref)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $ref->user->fullFIO }}</td>
                        <td>{{ $ref->rule?$ref->rule->level:'' }}</td>
                        <td>{{ $ref->nameRule }}</td>
                        <td>{{ $ref->created_at }}</td>
                        <td>{{ $ref->operationValueCurrency }}</td>
                        <td>{{ $ref->valueCurrency }}</td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        @endif

    </div>
</div>
@endsection

@section('script')
<script src="{{ asset('js/datatables/datatables.min.js') }}"></script>
<script>
  $(document).ready(function() {

        $('#users-table-referals').DataTable();
    });

</script>
@endsection