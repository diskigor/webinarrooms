{{-- $refferals--}}
@extends('layouts.admin')
@section('content')
<div class="col-md-12">
    <header class="big-header text-center">РЕФЕРАЛЬНАЯ СИСТЕМА, ВСЕ пользователи</header>

    <div class="panel">
        @if($refferals->isEmpty())
            <h3 class="text-center">В системе ещё нет рефералов</h3>
        @else
        <table class="table table-responsive" id="users-table-referals">
            <thead>
                <tr>
                    <th>№</th>
                    <th>Пользователь</th>
                    <th>Дата регистрации</th>
                    <th>По приглашению</th>
                    <th>Баланс</th>
                    <th>Реф. доход</th>
                </tr>
            </thead>
            <tbody>

                @foreach($refferals as $ref)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $ref->user?$ref->user->idFullNameEmail:'-?-' }}</td>
                    <td>{{ $ref->user?$ref->user->created_at:'-?-' }}</td>
                    <td>{{ $ref->uparent?$ref->uparent->idFullNameEmail:'' }}</td>
                    <td>{{ $ref->user?$ref->user->balance:'' }}</td>
                    <td>{{ $ref->user?$ref->user->getMyReferalsAllBonus():'' }}</td>
                </tr>
                @endforeach
                
            </tbody>
        </table>
        @endif

    </div>
</div>
@endsection

@section('script')
<script src="{{ asset('js/datatables/datatables.min.js') }}"></script>
<script>
  $(document).ready(function() {

        $('#users-table-referals').DataTable();
    });

</script>
@endsection