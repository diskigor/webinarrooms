{{--  $levels--}}
@extends('layouts.admin')
@section('content')
<div class="col-md-12">
    <header class="big-header text-center">РЕФЕРАЛЬНАЯ СИСТЕМА, УРОВНИ</header>

    <div class="panel">
        <table class="table table-responsive" id="users-table-referals">
            <thead>
                <tr>
                    <th>Уровень</th>
                    <th>Статус</th>
                    <th>Описание</th>
                    <th>Начисляется</th>
                </tr>
            </thead>
            <tbody>
                @foreach($levels as $ref)
                <tr>
                    <td>{{ $ref->level }}</td>
                    <td>{{ $ref->status?'включен':'выключен' }}</td>
                    <td>{{ $ref->description }}</td>
                    <td>{{ $ref->value }} {{ ($ref->type == 'procent')?'%':'КЭЛ'}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
