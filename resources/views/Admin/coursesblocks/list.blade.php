@extends('layouts.admin')

@section('content')
<div class="col-md-12">
    <header class="big-header text-center">Блоки курсов</header>
    <a href="{{route('admin_block.create')}}" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>Добавить блок</a>
    <hr>
    <div class="row">
        @if($blocks->total())
        @foreach($blocks as $bl)
        <figure class="col-md-4">
            <div class="thumbnail courses">

                <!--img class="image" src=""-->
                <div>Кол - во курсов в блоке: <label class="label label-success">{{ $bl->coursesCount }}</label>
                    <span class="pull-right">Скидка : <label class=" label label-primary">{{ $bl->discount }} %</label></span>
                </div>
                <figcaption>
                    <h3>{{ mb_strimwidth($bl->name,0,60,'...') }}</h3>

                    <p><span class="pull-right">Сортировка : <label class=" label label-info">{{ $bl->order_block }}</label></span></p>
                    <p class="hidden"> <label>Создан :</label> {{$bl->created_at}}</p>
                    <div class="short">{{mb_strimwidth($bl->shortD,0,100,'...')}}</div>
                </figcaption>
                <div class="hover-nav">

                    {!! Form::open(array('route'=>['admin_block.edit',$bl->id],'method'=>'GET','class'=>'inline-block')) !!}
                    {!! Form::button('<i class="fas fa-pencil-alt"></i>',['class'=>'btn btn-info','type'=>'submit']) !!}
                    {!! Form::close() !!}
                    {!! Form::open(array('route'=>['admin_block.destroy',$bl->id],'method'=>'DELETE','class'=>'inline-block')) !!}
                    {!! Form::button('<i class="fa fa-times"></i>',['class'=>'btn btn-danger','type'=>'submit','onclick'=>'return checkDelete()']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </figure>
        @endforeach
        @else
            <center>
                <h2 class="bg-info">Нет блоков для отображения</h2>
            </center>
        @endif
        
    </div>
    {{$blocks->links()}}
</div>
@endsection

@push('stack_scripts')
    <script language="JavaScript" type="text/javascript">
        function checkDelete() {
            return confirm('Вы уверены что хотите удалить данный блок?');
        }
    </script>
@endpush