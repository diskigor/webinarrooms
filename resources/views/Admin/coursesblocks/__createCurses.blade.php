@extends('Admin.app')
@section('content')
    <div class="col-md-12">
        <h1 class="text-center">{{trans('curses.title_from_created_curses')}}</h1>
        {!! Form::open(array('route'=>'curses.store', 'method'=>'POST','enctype'=>'multipart/form-data')) !!}
        <div class="form-group">
            <div class="preview">
                <div id="preview1"></div>
                <input type="button" id="reset1" value="Сбросить" class="btn btn-info reset"/>
            </div>
            <label for="image" class="control-label">{{trans('curses.curse_image')}} <small>(1140х400)</small></label>
            {!! Form::file('image',['accept'=>"image/jpeg,image/png,image/gif"])!!}
        </div>

        <div class="form-group">
            {!! Form::label('ordered','Сортировка/последовательность вывода курсов',['class'=>'control-label']) !!}
            {!! Form::number('ordered',0,['class'=>'form-control','min'=>0, 'max'=>10000]) !!}
        </div>
        
        <div class="form-group">
            {!! Form::label('discount','Скидка %',['class'=>'control-label']) !!}
            {!! Form::number('discount',null,['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('slug_curse','Slug для SEO',['class'=>'control-label']) !!}
            {!! Form::text('slug_curse',null,array_merge([
            'class'=>'form-control',
            'required']))!!}
        </div>
        @foreach($groups as $group)
            <div class="form-group">
                {!! Form::label('group_price['.$group->id.']','Цена для пользователей группы '.$group->name,['class'=>'control-label']) !!}
                {!! Form::number('group_price['.$group->id.']',null,['class'=>'form-control','required']) !!}
            </div>
        @endforeach
        <div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <?php $i = 0;?>
                @foreach($arr_local as $key=>$lang)
                    <?php $i++;?>
                    @if($i===1)
                        <li role="presentation" class="active">
                            <a href="#{{$key.'-first'}}" aria-controls="{{$key.'-first'}}" role="tab"
                               data-toggle="tab">{{$lang}}</a>
                        </li>
                    @else
                        <li role="presentation">
                            <a href="#{{$key}}" aria-controls="{{$key}}" role="tab" data-toggle="tab">{{$lang}}</a>
                        </li>
                    @endif
                @endforeach
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <?php $c = 0;?>
                @foreach($arr_local as $key=>$lang_content)
                    <?php $c++;?>
                    @if($c===1)
                        <div role="tabpanel" class="tab-pane active fade in" id="{{$key.'-first'}}">
                            <div class="form-group">
                                {!! Form::label('name_'.$key,trans('curses.name_curse'),['class'=>'control-label']) !!}
                                {!! Form::text('name_'.$key,null,array_merge([
                                'class'=>'form-control',
                                'placeholder'=>trans('curses.placeholder_name_course'),
                                'required']))!!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('short_desc_'.$key,trans('curses.short_description'),['class'=>'control-label']) !!}
                                {!! Form::text('short_desc_'.$key,null,array_merge([
                                'class'=>'form-control',
                                'placeholder'=>trans('curses.desc_course_placeholder'),
                                'maxlength'=>'190',
                                'required']))!!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('description_'.$key,trans('curses.desc_course'),['class'=>'control-label']) !!}
                                {!! Form::textarea('description_'.$key,null,['class'=>'form-control']) !!}
                            </div>
                            @else
                                <div role="tabpanel" class="tab-pane fade" id="{{$key}}">
                                    <div class="form-group">
                                        {!! Form::label('name_'.$key,trans('curses.name_curse').'  '.$lang_content,['class'=>'control-label']) !!}
                                        {!! Form::text('name_'.$key,null,array_merge([
                                        'class'=>'form-control',
                                        'placeholder'=>trans('curses.placeholder_name_course'),
                                        'required']))!!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('short_desc_'.$key,trans('curses.short_description').'  '.$lang_content,['class'=>'control-label']) !!}
                                        {!! Form::text('short_desc_'.$key,null,array_merge([
                                        'class'=>'form-control',
                                        'placeholder'=>trans('curses.desc_course_placeholder'),
                                        'maxlength'=>'190',
                                        'required']))!!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('description_'.$key,trans('curses.desc_course').'  '.$lang_content,['class'=>'control-label']) !!}
                                        {!! Form::textarea('description_'.$key,null,['class'=>'form-control']) !!}
                                    </div>
                                    @endif
                                </div>
                                @endforeach
                                <div class="form-group">
                                    {!! Form::submit(trans('curses.button_save_course'),['class'=>'btn btn-success']) !!}
                                </div>
                                {!! Form::close() !!}
                                <a href="{{route('curses.index')}}" class="btn btn-info">Назад</a>
                        </div>
            </div>
        </div>
    </div>
@endsection

@push('stack_scripts')
<script src="{{asset('js/imagepreview.js')}} "></script>
<script type="text/javascript">
$(function () {
    $('#preview1').imagepreview({
        input: '[name="ilink"]',
        reset: '#reset1',
        preview: '#preview1'
    });
});
</script>
@endpush