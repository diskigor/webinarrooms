@extends('layouts.admin')

@section('content')
<div class="col-md-12">
    <header class="big-header text-center">Форма редактирования Блока</header>
    {!! Form::open(array('route'=>['admin_block.update',$block->id], 'method'=>'PUT','enctype'=>'multipart/form-data')) !!}
    <div class="panel">

        <div class="form-group">
            {!! Form::label('status','Включен/выключен',['class'=>'control-label']) !!}
            {!! Form::checkbox('status', 1,$block->status) !!}
        </div>
        
        <div class="form-group">
            {!! Form::label('order_block','Сортировка/последовательность вывода Блоков',['class'=>'control-label']) !!}
            {!! Form::number('order_block',$block->order_block,['class'=>'form-control','min'=>0, 'max'=>10000]) !!}
        </div>
        
        <div class="form-group">
            {!! Form::label('discount','Скидка на курсы, при покупке блоком',['class'=>'control-label']) !!}
            {!! Form::number('discount',$block->discount,['class'=>'form-control','min'=>0, 'max'=>100]) !!}
        </div>

        <div class="form-group">
            {!! Form::label('front_status','отображать на главной, и его высота ',['class'=>'control-label']) !!}
            {!! Form::checkbox('front_status', 1,$block->front_status) !!}
            {!! Form::number('front_hight',$block->front_hight,['min'=>0, 'max'=>10000,'placeholder'=>'Высота блока на главной, если включено']) !!}
        </div>
        
        <div class="form-group">
            {!! Form::label('ilink','Название для главной',['class'=>'control-label']) !!}
            {!! Form::text('ilink',$block->ilink,array_merge(['class'=>'form-control']))!!}
        </div>

        
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">

            @foreach(LaravelLocalization::getSupportedLocales() as $key=>$lang)
            <li role="presentation" class="{{ ($loop->iteration == 1)?'active':''}}">
                <a href="#language_{{ $key }}" aria-controls="language_{{ $key }}"
                   role="tab"
                   data-toggle="tab">{{$lang['native']}}</a>
            </li>
            @endforeach
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            @foreach(LaravelLocalization::getSupportedLocales() as $key=>$lang)
                        <?php
                            $block_xxx          = $block->lang->where('language', $key)->first();   //работаем с коллекцией, без запросов в базу
                            
                            $block_name         = $block_xxx?$block_xxx->name:'';                   //если добавился язык и описания нет
                            $block_shortd       = $block_xxx?$block_xxx->shortd:'';                 //если добавился язык и описания нет
                            $block_description  = $block_xxx?$block_xxx->description:'';            //если добавился язык и описания нет
                            
                        ?>
            <div role="tabpanel" class="tab-pane active fade {{ ($loop->iteration == 1)?'in':'' }}" id="language_{{ $key }}">
                <div class="form-group">
                    {!! Form::label('name['.$key.']','Название блока',['class'=>'control-label']) !!}
                    {!! Form::text('name['.$key.']',$block_name,array_merge([
                    'class'=>'form-control',
                    'required']))!!}
                </div>
                <div class="form-group">
                    {!! Form::label('shortd['.$key.']','Краткое описание блока',['class'=>'control-label']) !!}
                    {!! Form::text('shortd['.$key.']',$block_shortd,array_merge([
                    'class'=>'form-control',
                    'maxlength'=>'190',
                    'required']))!!}
                </div>
                <div class="form-group">
                    {!! Form::label('description['.$key.']','Полное описание блока',['class'=>'control-label']) !!}
                    {!! Form::textarea('description['.$key.']',$block_description,['class'=>'form-control']) !!}
                </div>

            </div>
            @endforeach
        </div>
        <div class="form-group">
            {!! Form::submit('Обновить описание блок',['class'=>'btn btn-success pull-right']) !!}
        </div>
        {!! Form::close() !!}
        <div class="clearfix">
            <hr/>
        </div>
        
    <hr>
    <div class="container">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>№</th>
                <th>Курс</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>

            @foreach($block->courses as $cours)
                <tr>
                    <td>{{ $cours->sdo_courses_block_ordred }}</td>
                    <td>{{ $cours->lang?$cours->lang->name_trans:'' }}</td>
                    <td>
                        {!! Form::open(array('route'=>['del_course_to_block',$block->id],'method'=>'POST','class'=>'inline-block')) !!}
                        <input type="hidden" name='course_id' value="{{ $cours->id }}">
                        {!! Form::button('Удалить курс из блока',['class'=>'btn btn-danger',
                        'type'=>'submit',
                        'title'=>'Удалить курс из блока',
                        'onclick'=>'return checkDelete()']) !!}
                        {!! Form::close() !!}
                        
                    </td>
                </tr>
            @endforeach
            <tr>
                @if($available->count())
                    <td></td>

                    <td colspan="2">
                            {!! Form::open(array('route'=>['add_course_to_block',$block->id],'method'=>'POST','class'=>'inline-block')) !!}

                            {!! Form::select('course_id', $available->pluck('trans_name','id'), null, ['placeholder' => 'Выбирите курс для добавление к этому блоку...']) !!}

                            {!! Form::button('Добавить курс к блоку',['class'=>'btn btn-success','type'=>'submit']) !!}
                            {!! Form::close() !!}
                    </td>
                @else
                    <td colspan="10">Нет доступных курсов, все распределены по блокам</td>
                @endif
            </tr>
            
            </tbody>
        </table>
    </div>
    
        
        
        <a href="{{route('admin_block.index')}}" class="btn btn-info">Назад</a>
    </div>
</div>
</div>
@endsection

@push('stack_scripts')
<script src="{{asset('js/imagepreview.js')}} "></script>
<script type="text/javascript">
$(function () {
    $('#preview1').imagepreview({
        input: '[name="ilink"]',
        reset: '#reset1',
        preview: '#preview1'
    });
});
</script>
@endpush