@extends('layouts.admin')

@section('content')
    <div class="col-md-12">
        <header class="big-header text-center">
            Список лекторов на сайте
        </header>
        <div class="">

            {!! Form::open(array('route'=>['lectors.update',$lecturer->id],
            'method'=>'PUT',
            'enctype'=>'multipart/form-data',
            'class'=>'panel')) !!}
            <div>{{trans('content.date_created_account').' '.$lecturer->created_at}}</div>
            <div class="form-group">
                <img class="pre" src="{{ $lecturer->avatar }}" class="">
            </div>
            <div class="form-group">
                <div class="preview">
                    <div id="preview1"></div>
                    <input type="button" id="reset1" value="Сбросить" class="btn btn-info reset"/>
                </div>
            </div>

            <div class="form-group">
                <label for="avatar" class="control-label">Выберите автарку
                    <small>(500х500)</small>
                </label>
                {!! Form::file('avatar_link') !!}
            </div>
            
            <div class="form-group">
                {!! Form::label('name',trans('content.name'),['class'=>'control-label']) !!}
                {!! Form::text('name',$lecturer->name,array_merge(['class'=>'form-control'])) !!}
            </div>
            
            <div class="form-group">
                {!! Form::label('email','Email',['class'=>'control-label']) !!}
                {!! Form::email('email',$lecturer->email,array_merge(['class'=>'form-control'])) !!}
            </div>
            
            <div class="form-group">
                {!! Form::label('lecture_percent','Процент лектора',['class'=>'control-label']) !!}
                {!! Form::number('lecture_percent',$lecturer->lecture_percent,array_merge(['class'=>'form-control'])) !!}
            </div>
            
            <div class="form-group">
                {!! Form::label('role','Роль пользователя',['class'=>'control-label']) !!}
                {!! Form::select('role',$arr_role,$lecturer->role->id,['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('group_id','Группа пользователя',['class'=>'control-label']) !!}
                {!! Form::select('group_id',$groups,$lecturer->group_id,['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('level_point',trans('content.level').': '.$lecturer->level_point,['class'=>'control-label']) !!}
                {!! Form::number('level_point',null,array_merge(['class'=>'form-control'])) !!}
            </div>
            <div class="form-group">
                {!! Form::label('balance',trans('content.balance').': '.$lecturer->balance,['class'=>'control-label']) !!}
                {!! Form::number('balance',null,array_merge(['class'=>'form-control'])) !!}
            </div>
            <div class="form-group">
                {!! Form::label('text_admin','Описание',['class'=>'control-label']) !!}
                {!! Form::text('text_admin',null,array_merge(['class'=>'form-control'])) !!}
            </div>
            <div class="form-group">
                {!! Form::button(trans('content.save'),['class'=>'btn btn-primary','type'=>'submit']) !!}
            </div>
            <div class="form-group">
                {{ link_to_route('lectors.show',trans('content.back'),null,['class'=>'btn btn-info']) }}
            </div>
            {!! Form::close() !!}


        </div>
    </div>
@endsection

@push('stack_scripts')
    <script src="{{asset('js/imagepreview.js')}} "></script>
    <script type="text/javascript">
        $(function () {
            $('#preview1').imagepreview({
                input: '[name="avatar_link"]',
                reset: '#reset1',
                preview: '#preview1'
            });
        });
    </script>
@endpush
