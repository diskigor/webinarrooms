@extends('layouts.admin')

@section('content')
<!-- Contextual Classes -->
<div class="col-md-12">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <header class="big-header text-center">
                        Список лекторов на сайте
                    </header>

                </div>
                <div class="body panel">
                    <!--div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" class="form-control" name="item" id="searchItem" placeholder="Поиск">
                                <a type="submit" class="btn btn-blue input-group-btn" href="{{route('lectors.index')}}">Сбросить</a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" class="form-control" name="item" id="searchEmail" placeholder="Поиск по почте">
                                <a type="submit" class="btn btn-blue input-group-btn" href="{{route('lectors.index')}}">Сбросить</a>
                            </div>
                        </div>
                    </div-->
                    <table class="table table-responsive" id="js_users_table">
                        <thead>
                            <tr>
                                <th>№</th>
                                <th>{{trans('content.name')}}</th>
                                <th>Email</th>
                                <th>{{trans('content.role_user')}}</th>
                                <th>Группа</th>
                                <th>{{trans('content.level')}}</th>
                                <th>{{trans('content.balance')}}</th>
                                <th>%</th>
                                <th>Зарегистрирован</th>
                                @if(Auth::user()->isAdmin())
                                <th>{{trans('content.actions')}}</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody class="lectures">
                            @foreach($lectors as $lector)
                            @if(is_null($lector->blackUser))
                            <tr id="{{$lector->name}}">
                                <th>{{$lector->id}}</th>
                                <td>{{$lector->name}}</td>
                                <td>{{$lector->email}}</td>
                                <td>{{$lector->role->name}}</td>
                                <td>@if($lector->group_id != NULL){{$lector->group->name}}@else Нет @endif</td>
                                <td>{{$lector->level_point}}</td>
                                <td>{{$lector->balance}}</td>
                                <td>{{$lector->lecture_percent}}</td>
                                <td>{{ $lector->created_at }}</td>
                                @if(Auth::user()->isAdmin())
                                <td>
                                    {!! Form::open(array('route'=>['lectors.edit',$lector->id],'method'=>'GET','class'=>'inline-block')) !!}
                                    {!! Form::button('<i class="fas fa-pencil-alt"></i>',['class'=>'btn btn-info','type'=>'submit']) !!}
                                    {!! Form::close() !!}
                                    <button type="button" class="btn btn-black btn-lg"
                                            data-toggle="modal" data-target="#modalBlackList">
                                        <i class="fa fa-file" aria-hidden="true"></i>
                                    </button>
                                    <div class="modal fade" id="modalBlackList" tabindex="-1"
                                         role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close"
                                                            data-dismiss="modal"
                                                            aria-label="Close"><span
                                                            aria-hidden="true">&times;</span>
                                                    </button>
                                                    <h4 class="modal-title" id="myModalLabel">Добавление
                                                        в черный список</h4>
                                                </div>
                                                <div class="modal-body">
                                                    {!! Form::open(array('route'=>'black_list.store','method'=>'POST')) !!}
                                                    <input type="hidden" name="user_id"
                                                           value="{{$lector->id}}">
                                                    <div class="form-group">
                                                        {!! Form::label('reason','Введите причину.',['class'=>'control-label']) !!}
                                                        {!! Form::text('reason',null,['class'=>'form-control','required','maxlength'=>'190']) !!}
                                                    </div>
                                                    <button type="button" class="btn btn-default"
                                                            data-dismiss="modal">Закрыть
                                                    </button>
                                                    {!! Form::submit('Заблокировать',['class'=>'btn btn-success']) !!}
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                @else
                                <td>
                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                </td>
                                @endif
                            </tr>
                            @endif
                            @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END# Contextual Classes -->
@endsection

@section('script')
<script src="{{ asset('js/datatables/datatables.min.js') }}"></script>
<script>
$(document).ready(function () {

    $('#js_users_table').DataTable();
});

</script>
@endsection