<aside class="left-sidebar">
    <div>

        <div class="side-nav-toggle"></div>
        <!-- User Info -->
        <div class="user">

            <div class="info-container menu">
                <header>
                    <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Tooltip on left"></i>
                    <a href="{{route('profile.index')}}" class="menu-toggle"> <i class="fa fa-cog" aria-hidden="true"></i></a>
                </header>

                <div class="user-photo" style="background-image: url({{ Auth::user()->avatar }})">

                        <!-- Branding Image -->
                        <a class="logo" href="{{ url('/') }}">
                            <img src="{{asset('images/mane/logo.png')}}" width="32" height="32" alt="SDO">
                        </a>
                    </div>
                  
                <div class="">
                <div class="user-name">{{Auth::user()->name}}</div>
                <div class="email">{{Auth::user()->email}}</div>
                <!-- Account navs -->
                <div class="statusball">{{trans('site_bar.yuorscore')}}: <span style="color: #fff; font-weight: bold;">{{Auth::user()->level_point}}</span></div>
                <div class="balance">{{trans('site_bar.yourballance')}}, {{trans('home.kel')}}: <span style="color: #fff; font-weight: bold;">{{Auth::user()->balance}}</span></div>
                <div class="statusball">{{trans('site_bar.yourstatus')}}: <span style="color: #fff; font-weight: bold;">{{Auth::user()->role->description}}</span></div>
                @if(!Auth::user()->isAdmin())
                <div class="container-fluid">
                    <a class="btn btn-success" href="{{route('mybalance.index')}}"><i class="fa fa-plus"
                                                                                        aria-hidden="true"></i>
                        Пополнить баланс</a>
                </div>
                @endif
                </div>
            <!-- END account navs -->
            
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">

            <ul>
                <li>
                    <a href="{{route('/personal_area')}}">
                        <i class="fa fa-user"></i> {{trans('content.personal_area')}}
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                        <span>{{trans('content.cadets')}}</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('studies.index')}}" class="menu-toggle">
                        <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                        <span>Учебная аудитория</span>
                    </a>
                </li>
                <li>
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false"
                       aria-controls="collapseOne">
                        <i class="fa fa-book" aria-hidden="true"></i>
                        <span>Каталог курсов</span>
                    </a>
                    <ul id="collapseOne" class="ml-menu panel-collapse collapse" role="tabpanel">
                        <li>
                            <a href="{{route('free_materials.index')}}">
                                <span>Открытая школа</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('user_curses.index')}}">
                                <span>Основные курсы</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('partner_curses')}}">
                                <span>Партнёрские курсы</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false"
                       aria-controls="collapseTwo">
                        <i class="fa fa-book" aria-hidden="true"></i>
                        <span>Домашняя работа</span>
                    </a>
                    <ul id="collapseFive" class="ml-menu panel-collapse collapse" role="tabpanel">
                        <li>
                            <a href="{{route('checking_text_tasks.index')}}">
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                <span>Текстовые ответы</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('favorite_list')}}">
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                <span>Избранные</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="{{route('ablog.index')}}">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                        <span>Мой Блог</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('refsystem.index')}}">
                        <i class="fa fa-share-alt" aria-hidden="true"></i>
                        <span>Реферальная с-ма</span>
                    </a>
                </li>
                <li>
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseStatistics" aria-expanded="false"
                       aria-controls="collapseStatistics">
                        <i class="fa fa-line-chart" aria-hidden="true"></i>
                        <span>Статистика</span>
                    </a>
                    <ul id="collapseStatistics" class="ml-menu panel-collapse collapse" role="tabpanel">
                        <li>
                            <a href="{{route('user_statistics.index')}}">
                                <span>Статистика покупок</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('replenishment_user_statistic')}}">
                                <span>Статистика пополнений</span>
                            </a>
                        </li>
                    </ul>

                </li>
                <li>
                    <a href="{{route('profile.index')}}" class="menu-toggle">
                        <i class="fa fa-cog" aria-hidden="true"></i>
                        <span>{{trans('content.profile_setting')}}</span>
                    </a>
                </li>
                <li>
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse9" aria-expanded="false"
                       aria-controls="collapse9">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                        <span>Тех.Поддержка</span>
                    </a>
                    <ul id="collapse9" class="ml-menu panel-collapse collapse" role="tabpanel">
                        <li>
                            <a href="{{route('callback.create')}}">
                                <span>Создать новый запрос</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('user_callback_chats.index')}}">
                                <span>Список запросов</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="/page/rules">
                        <i class="fa fa-bullhorn" aria-hidden="true"></i>
                        <span> {{trans('home.rules')}}</span>
                    </a>
                </li>
                <span class="visible-xs">
                <hr>
                <li>
                            <a href="{{route('admin_all_archive.index')}}/">{{trans('home.leaderscience')}}</a>
                        </li>
                        <li>
                            <a href="{{ route('blog.index') }}">{{trans('home.blog')}}</a>
                        </li>
                        <li>
                            <a href="#"   data-toggle="modal" data-target="#webRoom">{{trans('home.webinar')}}</a>
                        </li>
                        <li>
                            <!-- Button trigger modal -->
                            <a  href="#" data-toggle="modal" data-target="#forumSDO">{{trans('home.forum')}}</a>
                        </li>
                        <li>
                            <a href="#" data-toggle="modal" data-target="#shopSDO">{{trans('home.magaz')}}</a>
                        </li>
                <hr>
                </span>
                <li>
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                        <i class="fa fa-power-off"></i> {{trans('content.logout')}}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>

        </div>

        <!-- #Menu -->
        <!-- Menu2 -->
    {{--<div class="menu">--}}
    {{--<header>Блок меню <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Tooltip on left"></i></header>--}}

    {{--<ul>--}}
    {{--<li class="visible-xs">--}}
    {{--<a href="/">Главная</a>--}}
    {{--</li>--}}
    {{--@if(isset($pages) and $pages->count() !=0)--}}
    {{--@foreach($pages as $page)--}}
    {{--<li>--}}
    {{--<a href="{{route('page',$page->slug)}}" role="button">{{$page->name}}</a>--}}
    {{--</li>--}}
    {{--@endforeach--}}
    {{--@endif--}}
    {{--<li>--}}
    {{--<a target="_blank" href="http://forum.gilbo.ru/">Форум</a>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--<a target="_blank" href="http://insiderclub.ru/">Магазин</a>--}}
    {{--</li>--}}
    {{--</ul>--}}
    {{--</div>--}}
    <!-- #Menu2 -->
        </div>
</aside>
@section('script')
    <script>
        // MOBILE MENU TOGGLE
        $('.side-nav-toggle').on('click', function () {
            $('.left-sidebar').toggleClass('open');
        });

    </script>
@endsection