@extends('layouts.admin')

@section('content')
    <div class="col-md-12">
        <header class="big-header text-center">Редактирование куратора</header>
        <div class="">


            {!! Form::open(array('route'=>['curator.update',$curator->id],
            'method'=>'PUT','enctype'=>'multipart/form-data',
            'class'=>'panel')) !!}

            <div>{{trans('content.date_created_account').' '.$curator->created_at}}</div>
            <div class="form-group">
                <img class="pre" src="{{ $curator->avatar }}" class="">
            </div>
            <div class="form-group">
                <div class="preview">
                    <div id="preview1"></div>
                    <input type="button" id="reset1" value="Сбросить" class="btn btn-info reset"/>
                </div>
            </div>
            <div class="form-group">
                <label for="avatar" class="control-label">Выберите автарку
                    <small>(500х500)</small>
                </label>
                {!! Form::file('avatar_link') !!}
            </div>

            <div class="form-group">
                {!! Form::label('name',trans('content.name'),['class'=>'control-label']) !!}
                {!! Form::text('name',$curator->name,array_merge(['class'=>'form-control'])) !!}
            </div>
            <div class="form-group">
                {!! Form::label('email','Email',['class'=>'control-label']) !!}
                {!! Form::email('email',$curator->email,array_merge(['class'=>'form-control'])) !!}
            </div>
            <div class="form-group">
                {!! Form::label('role',trans('content.role_user'),['class'=>'control-label']) !!}
                {!! Form::select('role',$arr_role,$curator->role->id,['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('group_id','Группа пользователя',['class'=>'control-label']) !!}
                {!! Form::select('group_id',$groups,$curator->group_id,['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('level_point',trans('content.level').': '.$curator->level_point,['class'=>'control-label']) !!}
                {!! Form::number('level_point',null,array_merge(['class'=>'form-control'])) !!}
            </div>
            <div class="form-group">
                {!! Form::label('balance',trans('content.balance').': '.$curator->balance,['class'=>'control-label']) !!}
                {!! Form::number('balance',null,array_merge(['class'=>'form-control'])) !!}
            </div>

            <div class="form-group">
                {!! Form::label('text_admin','Описание',['class'=>'control-label']) !!}
                {!! Form::text('text_admin',null,array_merge(['class'=>'form-control'])) !!}
            </div>
            <div class="form-group">
                {!! Form::button(trans('content.save'),['class'=>'btn btn-primary','type'=>'submit']) !!}
            </div>
            <div class="form-group">
                {{ link_to_route('curator.show',trans('content.back'),null,['class'=>'btn btn-info']) }}
            </div>
            {!! Form::close() !!}

        </div>
    </div>

@endsection

@push('stack_scripts')
    <script src="{{asset('js/imagepreview.js')}} "></script>
    <script type="text/javascript">
        $(function () {
            $('#preview1').imagepreview({
                input: '[name="avatar_link"]',
                reset: '#reset1',
                preview: '#preview1'
            });
        });
    </script>
@endpush