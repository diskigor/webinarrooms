@extends('layouts.admin')
@section('content')
<!-- Contextual Classes -->
<div class="col-md-12">
    @if(Session::has('message'))
    <div class="alert alert-success ">
        {{Session::get('message')}}
    </div>
    @endif
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <header class="big-header text-center">
                        {{trans('content.list_curators')}}
                    </header>

                </div>
                <div class="body panel">
                    <table class="table table-responsive" id="js_users_table">
                        <thead>
                            <tr>
                                <th>№</th>
                                <th>{{trans('content.name')}}</th>
                                <th>Email</th>
                                <th>{{trans('content.role_user')}}</th>
                                <th>Группа</th>
                                <th>{{trans('content.level')}}</th>
                                <th>{{trans('content.balance')}}</th>
                                <th>Зарегистрирован</th>
                                @if(Auth::user()->isAdmin())
                                <th>{{trans('content.actions')}}</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody class="curators">
                            @foreach($curators as $curator)
                            @if(is_null($curator->blackUser))
                            <tr id='{{$curator->name}}'>
                                <td>{{$curator->id}}</td>
                                <td>{{$curator->name}}</td>
                                <td>{{$curator->email}}</td>
                                <td>{{$curator->role->name}}</td>
                                <td>@if($curator->group_id != NULL){{$curator->group->name}}@else Нет @endif</td>
                                <td>{{$curator->level_point}}</td>
                                <td>{{$curator->balance}}</td>
                                <td>{{ $curator->created_at }}</td>
                                <td>

                                    @if(Auth::user()->isAdmin())
                                    {!! Form::open(array('route'=>['curator.edit',$curator->id],'method'=>'GET','class'=>'inline-block')) !!}
                                    {!! Form::button('<i class="fas fa-pencil-alt"></i>',['class'=>'btn btn-info','type'=>'submit']) !!}
                                    {!! Form::close() !!}

                                    <button type="button" class="btn btn-black btn-lg"
                                            data-toggle="modal" data-target="#modalBlackList">
                                        <i class="fa fa-file" aria-hidden="true"></i>
                                    </button>
                                    <div class="modal fade" id="modalBlackList" tabindex="-1"
                                         role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close"
                                                            data-dismiss="modal"
                                                            aria-label="Close"><span
                                                            aria-hidden="true">&times;</span>
                                                    </button>
                                                    <h4 class="modal-title" id="myModalLabel">Добавление
                                                        в черный список</h4>
                                                </div>
                                                <div class="modal-body">
                                                    {!! Form::open(array('route'=>'black_list.store','method'=>'POST')) !!}
                                                    <input type="hidden" name="user_id"
                                                           value="{{$curator->id}}">
                                                    <div class="form-group">
                                                        {!! Form::label('reason','Введите причину.',['class'=>'control-label']) !!}
                                                        {!! Form::text('reason',null,['class'=>'form-control','required','maxlength'=>'190']) !!}
                                                    </div>
                                                    <button type="button" class="btn btn-default"
                                                            data-dismiss="modal">Закрыть
                                                    </button>
                                                    {!! Form::submit('Заблокировать',['class'=>'btn btn-success']) !!}
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </td>
                            </tr>
                            @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END# Contextual Classes -->
@endsection

@section('script')
<script src="{{ asset('js/datatables/datatables.min.js') }}"></script>
<script>
function checkDelete() {
    return confirm('Вы уверены что хотите удалить данного пользователя?');
}
$(document).ready(function () {

    $('#js_users_table').DataTable();
});

</script>
@endsection