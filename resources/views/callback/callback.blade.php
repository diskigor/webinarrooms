@extends('layouts.admin')
@section('content')
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <?php $title_page='Форма обратной связи с администратором сайт'; ?>
                    {!! Form::open(array('route'=>'callback.store','method'=>'POST')) !!}
                    <div class="form-group">
                        {!! Form::label('subject','Создать новую тему')!!}
                        {!! Form::text('subject',null,array_merge(['class'=>'form-control','required','maxlength'=>'140'])) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('message','Сообщение') !!}
                        {!! Form::textarea('message',null,array_merge(['class'=>'form-control'])) !!}
                    </div>
                    {!! Form::submit('Отправить',array_merge(['class'=>'btn btn-success'])) !!}
                    {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection