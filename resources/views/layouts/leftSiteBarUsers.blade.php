<aside class="left-sidebar">
    <!--div class="header_menu">Мои возможности</div-->
        <div class="side-nav-toggle"></div>
        <!-- User Info -->
        <div class="user">
            <!-- Menu -->
            <div class="menu">
                <ul class="collapse in">
                    <li data-route='/personal_area'>
                        <a href="{{route('/personal_area')}}"><i class="fas fa-solar-panel"></i> РАБОЧИЙ СТОЛ</a>
                    </li>
                    
                    @if(Auth::user()->isLecturer())
                    <li>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"
                           aria-expanded="false"
                           aria-controls="collapseTwo">
                            <i class="fa fa-book" aria-hidden="true"></i>
                            <span>{{trans('content.materials')}}</span>
                        </a>
                        <ul id="collapseTwo" class=" panel-collapse collapse" role="tabpanel">

                            <li data-route='curses'>
                                <a href="{{route('curses.index')}}">- {{trans('content.courses')}}</a>
                            </li>
                            
                            <li data-route='prelection'>
                                <a href="{{route('prelection.index')}}">- {{trans('content.lectures')}}</a>
                            </li>
                            
                            <li data-route='materials'>
                                <a href="{{route('materials.index')}}">- {{trans('content.additional_materials')}}</a>
                            </li>
                            
                            <li data-route='test'>
                                <a href="{{route('test.index')}}">- {{trans('content.tests')}}</a>
                            </li>
                            
                            <li data-route='text_assignments'>
                                <a href="{{route('text_assignments.index')}}">- {{trans('content.text_assignments')}}</a>
                            </li>

                        </ul>
                    </li>
                    <!--  Мой Блог  -->
                    <li data-route='ablog comment_list'>
                        <a href="{{ route('ablog.index') }}">
                            <i class="fa fa-wheelchair-alt" aria-hidden="true"></i>
                            <span>МОЙ БЛОГ</span>
                        </a>
                    </li>

                    <li>
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseWebinar"
                                   aria-expanded="false"
                                   aria-controls="collapseTwo">
                                    <i class="fa fa-blind" aria-hidden="true"></i>
                                    <span>Вебинарная комната</span>
                                </a>
                                <ul id="collapseWebinar" class="ml-menu panel-collapse collapse" role="tabpanel">
                                    <li data-route='create_webinar'>
                                        <a href="{{route('create_webinar')}}">
                                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                            <span>Создать вебинар</span>
                                        </a>
                                    </li>
                                    <li>
                                         <a data-toggle="collapse" data-parent="#accordion" href="#collapseWebinar_2"
                                           aria-expanded="false"
                                           aria-controls="collapseTwo">
                                            <i class="fa fa-blind" aria-hidden="true"></i>
                                            <span>Мои вебинары</span>
                                        </a>
                                        <ul id="collapseWebinar_2" class="ml-menu panel-collapse collapse" role="tabpanel">
                                            <li data-route='my_webinars'>
                                                <a href="{{route('my_webinars')}}">
                                                    <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                                    <span>Предстоящие</span>
                                                </a>
                                            </li>
                                            <li data-route='arhiv'>
                                                <a href="{{route('arhiv')}}">
                                                    <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                                    <span>Прошедшие</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                    <!-- / Мой Блог  -->   
                    @endif

                    @if(Auth::user()->isCadet() || Auth::user()->isСurator())
                   
                    <li data-route='educational_audience'>
                        <a  href="{{route('educational_audience.index')}}"><i class="fas fa-user-graduate"></i> <span>УЧЕБНАЯ АУДИТОРИЯ</span></a>
                        <ul id="educational_audience_block" class="second-drop panel-collapse collapse" role="tabpanel">
                            <li data-route='educational_audience' class="hidden"></li>
                            <li data-route='free_studies'>
                                <a href="{{route('free_studies.index')}}"><i class="fas fa-archway"></i> <span>Открытая школа</span></a>
                            </li>
                            <li data-route='main_studies'>
                                <a href="{{route('main_studies.index')}}"><i class="fas fa-chalkboard-teacher"></i> <span>Курсы ШЭЛ</span></a>
                            </li>
                            
                            <li data-route='lecture_studies'>
                                <a href="{{route('lecture_studies.index')}}"><i class="far fa-handshake"></i> <span>Курсы Партнеров</span></a>
                            </li>
                            
                            <!--li><a class="disabled">- Партнерские курсы</a> </li-->
                            <li data-route='my_works'>
                                <a href="{{route('my_works.index')}}" ><i class="fas fa-clipboard-check"></i> <span>Мои задания</span></a>
                            </li>
                            <!--li data-route='webinars_room'>
                                <a href="{{route('webinars_room.index')}}">- Вебинарная комната</a>
                            </li-->
                            <li data-route='advice_room'>
                                <a href="{{route('advice_room.index')}}"><i class="far fa-comments"></i> <span>Консультационная комната</span></a>
                            </li>
                            <li>
                                <a href="https://insiderclub.ru/" target="_blank"><i class="fas fa-book-open"></i> <span>Каталог семинаров</span></a>
                            </li>
                        </ul>
                    </li>
                    <li>
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseWebinar"
                                   aria-expanded="false"
                                   aria-controls="collapseTwo">
                                    <i class="fa fa-blind" aria-hidden="true"></i>
                                    <span>Вебинарная комната</span>
                                </a>
                                <ul id="collapseWebinar" class="ml-menu panel-collapse collapse" role="tabpanel">
                                    <!--<li>
                                        <a href="{{route('catalog_webinars')}}">
                                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                            <span>Каталог вебинаров</span>
                                        </a>
                                    </li>-->
                                    <li data-route='poster_webinars'>
                                        <a href="{{route('catalog_webinars')}}?tab=1">
                                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                            <span>Предстоящие</span>
                                        </a>
                                    </li>
                                    <li data-route='poster_webinars'>
                                        <a href="{{route('catalog_webinars')}}?tab=2">
                                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                            <span>Приобретенные</span>
                                        </a>
                                    </li>
                                    <li data-route='arhiv'>
                                        <a href="{{route('arhiv')}}">
                                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                            <span>Прошедшие</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                    <li data-route='lectures_catalog'>
                        <a href="{{route('lectures_catalog.index')}}"><i class="fas fa-folder-open"></i> <span>КАТАЛОГ КУРСОВ</span> <span class="new-count-badge badge">{{ $courses_counts->get('new_all')?' +'.$courses_counts->get('new_all'):'' }}</span></a>
                        
                        <ul id="course_catalog_block" class="second-drop panel-collapse collapse" role="tabpanel">
                            <li data-route='lectures_catalog' class="hidden"></li>
                            <li data-route='user_curses'>
                                <a href="{{route('user_curses.index')}}"><i class="fas fa-chalkboard-teacher"></i> <span>Курсы ШЭЛ</span> <span class="new-count-badge badge">{{ $courses_counts->get('new_gilbo')?' +'.$courses_counts->get('new_gilbo'):'' }}</span></a>
                            </li>
                            <li data-route='partner_curses'>
                                <a href="{{route('partner_curses')}}"><i class="far fa-handshake"></i> <span>Курсы партнеров</span> <span class="new-count-badge badge">{{ $courses_counts->get('new_lecturer')?' +'.$courses_counts->get('new_lecturer'):'' }}</span></a>
                            </li>
                            <!--li data-route='webinars_room'>
                                <a href="{{route('webinars_room.index')}}">- Вебинарная комната</a>
                            </li-->
                            <li>
                                <a href="https://insiderclub.ru/" target="_blank"><i class="fas fa-book-open"></i> <span>Каталог семинаров</span></a>
                            </li>
                        </ul>
                    </li>
                    @endif
                    
                    @if(!Auth::user()->isCadet())
                    <!--  Работы курсантов  -->
                        <li>
                            <a data-toggle="collapse" 
                               data-parent="#accordion" 
                               href="#cursant_works" 
                               aria-expanded="false"
                               aria-controls="cursant_works">
                                <i class="fas fa-book" aria-hidden="true"></i>
                                <span>РАБОТЫ КУРСАНТОВ</span>
                            </a>
                            <ul id="cursant_works" class=" panel-collapse collapse" role="tabpanel">
                                <li data-route='checking_text_tasks'>
                                    <a href="{{route('checking_text_tasks.index')}}">
                                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                        <span>Текстовые ответы</span>
                                    </a>
                                </li>
                                <li data-route='favorite_list show_favorite_work'>
                                    <a href="{{route('favorite_list')}}">
                                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                        <span>Избранные</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    @endif
                    
                    
                    <!-- Все блоги -->
                    <li data-route='blog'>
                        <a href="{{route('blog.index')}}">
                            <i class="fab fa-blogger-b"></i>
                            БЛОГИ <span class="new-count-badge badge">{{ $blogs_counts->get('new')?' +'.$blogs_counts->get('new'):'' }}</span>
                        </a>
                    </li>
                    <!-- / Все блоги -->
                    
                     
                    
                    <!-- Рассылка от Гильбо -->
                    <li data-route='admin_all_archive all_archive_first_show'>
                        <a href="{{route('all_archive_first_show')}}/">
                            <i class="fas fa-at"></i>
                            <span>РАССЫЛКА</span>
                        </a>
                    </li>
                    <!-- / Рассылка от Гильбо -->
                    
                    <li>
                        <a data-toggle="collapse" 
                           data-parent="#accordion" 
                           href="#user_finance_block" 
                           aria-expanded="false"
                           aria-controls="user_finance_block"><i class="fas fa-wallet"></i> <span>УПРАВЛЕНИЕ ФИНАНСАМИ</span></a>
                        <ul id="user_finance_block" class="second-drop panel-collapse collapse" role="tabpanel">
                            <li data-route='mybalance'>
                                <a href="{{ route('mybalance.index') }}"><i class="fas fa-coins"></i> <span>Пополнение счёта и баланс</span></a>
                            </li>
                            <li data-route='refsystem'>
                                <a href="{{ route('refsystem.index') }}"><i class="fas fa-sitemap"></i> <span>Реферальная система</span></a>
                            </li>
                        </ul>
                    </li>
                    <li data-route='profile'>
                        <a href="{{route('profile.index')}}"><i class="fas fa-user-cog"></i> <span>Настройки профиля</span></a>
                    </li>
                    <li>
                        <a data-toggle="collapse" 
                           data-parent="#accordion" 
                           href="#support_centr" 
                           aria-expanded="false"
                           aria-controls="support_centr"><i class="fas fa-info"></i> <span>ЦЕНТР ПОДДЕРЖКИ</span></a>
                        <ul id="support_centr" class="second-drop panel-collapse collapse" role="tabpanel">
                            <li data-route='user_callback_chats'>
                                <a href="{{route('user_callback_chats.index')}}"><i class="far fa-envelope"></i> <span>Мои обращения</span> <span class="new-count-badge badge">{{ $new_message_from_admin }}</span></a>
                            </li>
                            <li data-route='video_instruction'>
                                <a href="{{ route('video_instruction.index') }}"><i class="fas fa-film"></i> <span>Видеоинструкции</span></a>
                            </li>

                            
                            <li data-route='faq'>
                                <a href="{{ route('faq.index') }}"><i class="fas fa-file-alt"></i> <span>Справка</span></a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- #Menu -->
</aside>
@push('stack_scripts')
    <script>
        // MOBILE MENU TOGGLE
        $('.side-nav-toggle').on('click', function () {
            $('.left-sidebar').toggleClass('open');
        });

        //подсветим активный пункт меню ))
        var current_route = '{{ explode(".",request()->route()->getName())?explode(".",request()->route()->getName())[0]:"" }}';
        $('li[data-route ~= "' + current_route + '"]').addClass('active').parents("ul").collapse('show');
    </script>
@endpush
