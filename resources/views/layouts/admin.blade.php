@extends('layouts.app')

@section('main_body')
    <!--HEADER-->
        @include('layouts.partials.header_admin',['title_page'=>isset($title_page)?$title_page:NULL])
    <!--END HEADER-->


<div class="content admin-content">
    <div  class="container-fluid">
        <div class="col-md-2 col-xs-12 left-admin-bar">
            @if(Auth::check())
                @if(Auth::user()->isAdmin())
                    @include('Admin.admin.leftSiteBarAdmin')
                @else
                    @include('layouts.leftSiteBarUsers')
                @endif
            @endif
        </div>
        @if ( 
                in_array('admin_all_archive', request()->segments()) || 
                in_array('blog', request()->segments()) ||
                in_array('ablog', request()->segments()) ||
                in_array('blog_gilbo', request()->segments()) || 
                in_array('blog_lecturer', request()->segments()) || 
                in_array('blog_gaidpark', request()->segments()) ||
                request()->route()->getName()=='all_archive_first_show' ||
                request()->route()->getName()=='front_blog_gilbo' || 
                request()->route()->getName()=='front_blog_gilbo_show' || 
                request()->route()->getName()=='front_blog_gilbo_search' ||
                request()->route()->getName()=='front_blog_lecturer' || 
                request()->route()->getName()=='front_blog_lecturer_show' || 
                request()->route()->getName()=='front_blog_lecturer_search' ||
                request()->route()->getName()=='front_blog_gaypark' || 
                request()->route()->getName()=='front_blog_gaypark_show' || 
                request()->route()->getName()=='front_blog_gaypark_search' ||
                request()->route()->getName()=='front_blog_search' ||
                request()->route()->getName()=='/personal_area' || 
                request()->route()->getName()=='mybalance.index'
            )
             <div class="col-md-8 center-content col-xs-12">
                @yield('content')
            </div>
            <div class="col-md-2 right-admin-bar col-xs-12">    
                @include('layouts.rightSideBarUsers')
            </div>
            @else
            <div class="col-md-10 center-content col-xs-12">
                @yield('content')
            </div>
            @endif
    </div>
</div>
    
    
    <!-- OPEN FOOTER  -->
        @include('layouts.partials.footer')
    <!--END FOOTER-->
@endsection

@section('script')
<script src="{{ asset('src/js/vendor/tinymce/js/tinymce/tinymce.min.js') }}"></script>
<script>
    $('input[type="checkbox"],input[type="radio"],input[type="file"]').styler();
</script>
<script>
    var editor_config = {
        path_absolute: "{{URL::to('/')}}/",
        
        language: "ru",
        selector: "textarea",
        selector : "textarea:not(.mceNoEditor)",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern","code"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | fontselect fontsizeselect forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
        relative_urls: false,
        file_browser_callback: function (field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight;

            var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
            if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }
            tinyMCE.activeEditor.windowManager.open({
                file: cmsURL,
                title: 'Filemanager',
                width: x * 0.8,
                height: y * 0.8,
                resizable: 'yes',
                close_previous: 'no'
            });
        }
    };
    tinymce.init(editor_config);
    $('input[type="checkbox"],input[type="radio"],input[type="file"]').styler();
    // General options
        //mode : "textareas",
       // editor_deselector : "mceNoEditor", // этот кусочек добавляем

</script>
@endsection