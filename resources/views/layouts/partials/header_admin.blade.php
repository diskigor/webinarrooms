<nav class="navbar navbar-default  navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{asset('images/mane/logo-star.png')}}" alt="LSG">
                <span class="slogan">{{trans('home.shelzag')}}</span>
            </a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="navbar-nav navbar-left page-title-menu hidden-xs">
                <li style="list-style: none;"><span class="nav-link">{!!$title_page!!}</span></li>
            </ul>
            <ul class="nav navbar-nav navbar-right hidden-xs">
                @if(!Auth::user()->isAdmin() && Auth::user()->level_point>=1000)
                    <li>
                        <a href="{{ route('mybalance.index') }}" data-toggle-header="tooltipalertballs">
                            <i class="fa fa-refresh fa-spin fa-fw" style="color: greenyellow; cursor: pointer;"></i>
                        </a>
                    </li>
                @endif
                
                @if(!Auth::user()->isAdmin() && ($minum_kel_balance_alert?Auth::user()->balance<$minum_kel_balance_alert:FALSE))
                    <li>
                        <a href="{{ route('mybalance.index') }}" data-toggle-header="tooltipalertbalance">
                            <i class="fa fa-bell" aria-hidden="true" style="color: red; cursor: pointer;"></i>
                        </a>
                    </li>
                @endif
                
                <li><a href="{{route('mybalance.index')}}" data-toggle-header="tooltipmoney"><i class="fas fa-wallet"><span>{{Auth::user()->balance}}</span></i></a></li>
                @if(Auth::user()->isAdmin())
                    <li><a href="{{ route('message.index') }}">
                            <i class="fas fa-envelope"></i>
                            <span class="new-count-badge badge heder-badge">{{ $new_message_from_users }}</span>
                        </a>
                    </li>
                @else
                    <li><a href="{{ route('user_callback_chats.index') }}"><i class="fas fa-envelope"></i><span class="new-count-badge badge heder-badge">{{ $new_message_from_admin }}</span></a></li>
                @endif                
                <li><a href="{{route('faq.index')}}"><i class="fas fa-question-circle"></i></a></li>
                <li><a href="{{route('profile.index')}}"><i class="fas fa-cog"></i></a></li>
                <li class="dropdown">
                    <a class="nav-link dropdown-toggle avapaneldrop" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <span class="avatar-bg-panel" style="background-image: url({{ Auth::user()->avatar }}); background-size: cover;"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <div class="infopanelcabinet">
                            <span class="avatar-bg-panel col-md-3 pull-left" style="background: url({{ Auth::user()->avatar }}); background-size: cover;"></span>
                            <!--img class="img-responsive  avapaneldrop " src="{{ Auth::user()->avatar }}" /-->
                            <div class="user-name col-md-9 pull-right">
                                <a href="{{ route('/personal_area') }}">{{Auth::user()->fullFIO}}</a>
                            </div>
                            <div class="email col-md-9 pull-right"><b>{{Auth::user()->email}}</b></div>
                            <div class="col-md-9 pull-right"><span class="width-one">{{trans('site_bar.yourballance')}}, {{trans('home.kel')}}:</span> <b>{{Auth::user()->balance}}</b></div>
                            <div class="col-md-9 pull-right"><span class="width-one">{{trans('site_bar.yuorscore')}}:</span> <b>{{Auth::user()->level_point}}</b></div>
                            <div class="col-md-9 pull-right"><span class="width-one">{{trans('site_bar.yourstatus')}}:</span> <b>{{Auth::user()->role->PseudoDescription}}</b></div>
                            <span class="line-popup"></span>
                            <div class="col-md-12">
                                
                                @if( request()->route()->getName() == '/personal_area' )
                                    <a class="btn-blue" href="{{ route('main_front_page') }}">Перейти на главную</a>
                                @else
                                    <a class="btn-blue" href="{{ route('/personal_area') }}">Перейти в кабинет</a>
                                @endif

                                <a class="btn-blue pull-right" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{trans('content.logout')}}</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                            </div>
                            
                        </div>
                    </ul>
                </li>
            </ul>
            <ul class="dropdown-menu visible-xs">
                <div class="infopanelcabinet">
                    <span class="avatar-bg-panel col-xs-3 pull-left" style="background: url({{ Auth::user()->avatar }}); background-size: cover;"></span>
                    <!--img class="img-responsive  avapaneldrop " src="{{ Auth::user()->avatar }}" /-->
                    <div class="user-name col-xs-9 pull-right">
                        <a href="{{ route('/personal_area') }}">{{Auth::user()->fullFIO}}</a>
                    </div>
                    <div class="email col-xs-9 pull-right">{{Auth::user()->email}}</div>
                    <div class="col-xs-9 pull-right">{{trans('site_bar.yourballance')}}, {{trans('home.kel')}}: <b>{{Auth::user()->balance}}</b></div>
                    <div class="col-xs-9 pull-right">{{trans('site_bar.yuorscore')}}: <b>{{Auth::user()->level_point}}</b></div>
                    <div class="col-xs-9 pull-right">{{trans('site_bar.yourstatus')}}: <b>{{Auth::user()->role->description}}</b></div>
                    <span class="line-popup"></span>
                    
                    @if( request()->route()->getName() == '/personal_area' )
                        <a class="col-xs-6" href="{{ route('main_front_page') }}">Перейти на главную</a>
                    @else
                        <a class="col-xs-6" href="{{ route('/personal_area') }}">Перейти в кабинет</a>
                    @endif

                    <a class="col-xs-6 header-link-right" style="text-align: right;" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{trans('content.logout')}}</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                </div>
            </ul>
        </div>
    </div>
</nav>