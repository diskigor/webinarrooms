<aside class="left-sidebar hidden">
        <div class="side-nav-toggle"></div>
        <!--MENU-->
        @if (Auth::check())
            <div class="user-photo" style="background-image: url({{ Auth::user()->avatar }})">
                <!-- Branding Image -->
                <a class="logo" href="{{ url('/') }}">
                    <img src="{{asset('images/mane/logo.png')}}" width="32" height="32" alt="SDO">
                </a>
            </div>
            <div class="container-fluid text-center">
                <div class="user-name">{{Auth::user()->name}}</div>
            </div>
            <div class="container-fluid">
                <a class="btn" href="{{ route('/personal_area') }}">{{trans('home.ouncabinet')}}</a>
            </div>
        @else
            <div class="brand-name">{{trans('home.shelzag')}}</div>
            <a class="logo" href="/">
                <img src="{{asset('/images/mane/logo.png')}}" alt="SDO logo"/>
            </a>

            <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <div class="container-fluid">
                    <div class="{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input id="email" type="email" placeholder="Email" class="form-control" name="email"
                               value="{{ old('email') }}" required autofocus>
                        @if ($errors->has('email'))
                            <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input id="password" type="password" placeholder="Пароль" class="form-control" name="password"
                               required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="container-fluid text-center">
                    <button type="submit" class="btn">
                        {{trans('home.shelzag')}}
                    </button>
                </div>
                <div class="container-fluid text-center">
                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                </div>
                <div class="container-fluid">
                    <a class="forgot" href="{{ route('password.request') }}">{{trans('home.forgotpass')}}</a>
                    <a class="reg" href="{{ url('/register') }}">{{trans('home.register')}}</a>
                </div>
            </form>
    @endif
    <!-- END MENU -->
        <div id="side-events">
            @if(!is_null($video_news))
                <a class="item" href="{{route('video_show',$video_news->id)}}">
                    <div class="image" style="background-image: url('{{asset($video_news->image_link)}}')"
                         title="{{$video_news->short_desc}}"></div>
                    <div class="caption">
                        <header>{{$video_news->short_desc}}</header>
                    </div>
                </a>
            @endif
            @if(isset($events))
                @foreach($events as $event)
                    <a class="item" href="{{route('show_event',$event->id)}}">
                        <div class="image" style="background-image: url('{{asset($event->image_link)}}')"></div>
                        <div class="caption">
                            <header>{{$event->event_trans()->name}}</header>
                        </div>
                    </a>
                @endforeach
            @endif
        </div>
        <div class="container-fluid">
            <a href="http://insiderclub.ru/product/pervaya-lgotnaya-konsultaciya-dlya-chlenov-kluba-u-evgilbo"
               target="_blank" class="btn">
                {{trans('home.askconsultation')}}
            </a>
        </div>
        <div class="container-fluid">
            <!-- Button trigger modal -->
            <button type="button" class="btn" data-toggle="modal" data-target="#modalAsk">
                {{trans('home.askgilbo')}}
            </button>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="modalAsk" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">{{trans('home.askgilbo')}}</h4>
                    </div>
                    <div class="modal-body">
                        <form action="" method="POST">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group">
                                <input type="text" name="themes" placeholder="Введите тему" class="form-control">
                                <textarea name="question" placeholder="Введите вопрос" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" name="agree" required>
                                С
                                <a class="" role="button" data-toggle="collapse" href="#collapseRules"
                                   aria-expanded="false" aria-controls="collapseRules"><u>правилами подписки</u></a>
                                озyакомлен
                                <div class="collapse" id="collapseRules">
                                    <div class="well">
                                        <ul>
                                            <li>Содержание рассылки не является информацией. Выпуски рассылки содержат
                                                исключительно личное мнение автора, Евгения Витальевича Гильбо.
                                            </li>
                                            <li>Автор рассылки высказывает свое мнение в ответ на задаваемые вопросы.
                                            </li>
                                            <li>Автор специально требует от подписчиков не принимать на веру его мнение.
                                                Ценностью обладают только выработанные Вами самостоятельно мнения. Любое
                                                другое мнение полезно только в той мере, в какой наталкивает Вас на
                                                размышления.
                                            </li>
                                            <li>Каждый подписчик рассылки берет на себя обязательство не принимать на
                                                веру мнение автора, но вырабатывать свое мнение на основе
                                                самостоятельного анализа информации по каждому вопросу. Цель автора
                                                заключается исключительно в привлечении внимания к поднимаемым вопросам.
                                            </li>
                                            <li>Принятие любого чужого мнения – шаг к психологической зависимости.
                                                Будьте независимы. Не верьте сказанному. Проверяйте все, что Вам
                                                говорится.
                                            </li>
                                            <li>Подписчики рассылки имеют право задавать любые вопросы. Подписчики
                                                стараются воздерживаться от вопросов, связанных с короткоживущими
                                                текущими событиями. Ответ на вопрос, как правило, появляется не ранее,
                                                чем через месяц после того, как он задан. Исключение делается только для
                                                вопросов, связанных с ближайшими предстоящими событиями в жизни Клуба.
                                            </li>
                                            <li>Рассылка не является средством массовой информации. Она нигде не
                                                зарегистрирована как средство массовой информации, не функционирует в
                                                таком качестве, не размещает рекламы.
                                            </li>
                                            <li>При чтении текста рассылки, а также любого другого текста, читатели
                                                обязуются стараться придерживаться следующих правил анализа:
                                            </li>
                                        </ul>
                                        <ol>

                                            <li>Никогда не верьте словам, принимайте к сведению только конкретные
                                                факты.
                                            </li>
                                            <li>Все что говорится и пишется, обязательно имеет целью сокрытие истины.
                                                Сокрытие истины является одним из важнейших мотивов любой человеческой
                                                деятельности, включая вербальную. Этот мотив, часто наряду с другими,
                                                присутствует у автора любого текста ВСЕГДА. В большинстве случаев любой
                                                автор желает скрыть информацию не только от Вас, но и от себя - то есть
                                                его бессознательное осуществляет неуправляемое цензурирование
                                                формулируемых им положений.
                                            </li>
                                            <li>Никогда не принимайте к сведению информацию, содержащуюся в любом
                                                тексте. Мысль изреченная есть ложь.
                                            </li>
                                            <li>Основным источником информации в любом тексте являются оговорки, ошибки
                                                и описки автора. Всегда обращайте на них внимание и старайтесь
                                                интерпретировать их на основе известных Вам элементов и свойств
                                                контекстов, имеющих отношение к обсуждаемому предмету.
                                            </li>
                                            <li>Сопоставляйте источники. Обращайте внимание на все логические
                                                противоречия внутри текстов и между текстами различных источников.
                                                Старайтесь интерпретировать противоречия на основе известных Вам
                                                элементов и свойств контекстов, имеющих отношение к обсуждаемому
                                                предмету.
                                            </li>
                                            <li>Читая любой текст, все время держите в уме вопрос, кому выгодна
                                                публикация излагаемых фактов и предлагаемая их интерпретация. Насколько
                                                эти интересы близки Вашим?
                                            </li>
                                            <li>При анализе аргументации всегда старайтесь классифицировать аргументы по
                                                признаку "кому это выгодно", по характеру стоящих за ними интересов.
                                                Смело переходите от анализа аргументов к анализу самих этих интересов.
                                                Только после их прояснения возвращайтесь к анализу собственно
                                                аргументации.
                                            </li>
                                            <li>При анализе частных вопросов практикуйте переход на метауровень, к
                                                анализу более общих противоречий. Возвращайтесь к анализу собственно
                                                аргументации по обсуждаемому вопросу после прояснения общих принципов.
                                                Без решения общих вопросов Вы будете неизбежно возвращаться к ним, решая
                                                частные.
                                            </li>
                                        </ol>
                                        <ul>
                                            <li>Подписка на рассылку "Наука Лидерства" означает принятие Вами настоящих
                                                правил, полное согласие с ними, готовность их выполнять.
                                            </li>
                                            <li>Евгений Витальевич не врач и не даёт медицинских советов. По вопросам
                                                лечения конкретных заболеваний задавайте вопросы
                                                доктору О.В.Ковалёвой <a href="http://www.kovaleva.ru">www.kovaleva.ru</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('home.cancel')}}</button>
                                <button type="submit" class="btn btn-primary">{{trans('home.send')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script>
            jQuery('document').ready(function ($) {
                // MOBILE MENU TOGGLE
                $('.side-nav-toggle').on('click', function () {
                    $('.left-sidebar').toggleClass('open');
                });
            });
        </script>
</aside>