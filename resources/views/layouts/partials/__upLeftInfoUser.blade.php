<header>
    <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Tooltip on left"></i>
    <a href="{{route('profile.index')}}" class="menu-toggle"> <i class="fa fa-cog" aria-hidden="true"></i></a>
</header>

<div class="user-photo" style="background-image: url({{ Auth::user()->avatar }})">
    <!-- Branding Image -->
    <a class="logo" href="{{ url('/') }}">
        <img src="{{asset('images/mane/logo.png')}}" width="32" height="32" alt="SDO">
    </a>
</div>

<div>
    <div class="user-name">{{Auth::user()->fullFIO}}</div>
    <div class="email">{{Auth::user()->email}}</div>
    <!-- Account navs -->
    <div class="statusball">{{trans('site_bar.yuorscore')}}: <span style="color: #fff; font-weight: bold;">{{Auth::user()->level_point}}</span></div>
    <div class="balance">{{trans('site_bar.yourballance')}}, {{trans('home.kel')}}: <span style="color: #fff; font-weight: bold;">{{Auth::user()->balance}}</span></div>
    <div class="statusball">{{trans('site_bar.yourstatus')}}: <span style="color: #fff; font-weight: bold;">{{Auth::user()->role->description}}</span></div>
    
    @if(Auth::user()->isCadet() || Auth::user()->isСurator())
    <div class="container-fluid">
        <a class="btn btn-success" 
           href="{{route('mybalance.index')}}"><i class="fa fa-plus" aria-hidden="true"></i>Пополнить баланс</a>
    </div>
    @endif
    
</div>
<!-- END account navs -->
            
       