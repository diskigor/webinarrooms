
<!-- close wrap -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <p class="footerheader">Школа Эффективных Лидеров 1989 - 2018</p>
                <p><a href="#" data-toggle="modal" data-target="#oferta">Публичная оферта</a></p>
                <p><a href="#" data-toggle="modal" data-target="#rules">Политика конфидинциальности</a></p>
            </div>
            <div class="col-md-4 col-sm-4">
                <p class="footerheader">Контактная информация</p>
                <p><a href="#" data-toggle="modal" data-target="#question-modal">Обратная связь</a></p>
                <p><a href="#" data-toggle="modal" data-target="#workewithus">Сотрудничество с нами</a></p>
            </div>
            <div class="col-md-4 col-sm-4">
                <p class="footerheader">Рассылка "Наука лидерства"</p>
                <form class="form-inline" 
                      action="{{ route('add_new_subscribers') }}"
                      method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="type" value="10">
                    <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon hidden-sm">@</div>
                                <input type="text" class="form-control" name="email" placeholder="Введите e-mail">
                            </div>
                        <button type="submit" class="btn btn-primary">Подписаться</button>
                    </div>
                    
                </form>
            </div>
            <div class="clearfix"></div>
            <hr>
            <div class="col-md-4 col-sm-4 col-xs-12" style="margin: 5px auto;">
                <a class="mobileapp" href="https://play.google.com/store/apps/details?id=ru.gilbo.leadersschoolapp">
                    <img class="col-md-6 col-xs-6 img-responsive" src="{{asset('images/appmobileg.png')}}">
                </a>
                <a class="mobileapp" href="#">
                    <img class="col-md-6 col-xs-6 img-responsive" src="{{asset('images/appmobilei.png')}}" style="float:right;">
                </a>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12" style="margin: 5px auto;">
                <img src="{{asset('images/paysystems.png')}}" class="img-responsive money-tr-img">
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 footer-socials" style="margin: 5px auto;">
                <a href="https://vk.com/gilboru" target="_blank"><i class="fab fa-vk"></i></a>
                <a href="https://www.facebook.com/gilbo.ru/" target="_blank"><i class="fab fa-facebook-f"></i></a>
                <a href="https://t.me/gilbo_ev" target="_blank"><i class="fas fa-paper-plane"></i></a>
                <a href="https://www.instagram.com/eugengilbo" target="_blank"><i class="fab fa-instagram"></i></a>
                <a href="https://ru.pinterest.com/shelgilbo/" target="_blank"><i class="fab fa-pinterest"></i></a>
                <a href="https://twitter.com/GilboShel" target="_blank"><i class="fab fa-twitter"></i></a>
            </div>
            
            
            
        </div>
    </div>
</footer>
<!-- Modal question -->
<div class="modal fade" id="question-modal" tabindex="-1" role="dialog"
     aria-labelledby="question-modalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="question-modallLabel">Задать свой вопрос</h4>
            </div>
            <div class="modal-body">
                <form action="{{route('question_user')}}" method="POST">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="form-group">
                        <input type="text" name="name" placeholder="Ваше имя" value="@if (Auth::check()){{Auth::user()->fullFIO}}@endif" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" placeholder="Ваш email" value="@if (Auth::check()){{Auth::user()->email}}@endif" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <textarea placeholder="Ваш вопрос" name="ask_user" class="form-control mceNoEditor" style="max-width: 570px"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="rules" required>
                            {!!trans('home.rulesleader')!!}
                    </div>
                    <div class="modal-footer" style="text-align: center">
                        <button type="submit" class="btn-submit-modal">{{trans('home.send')}}</button>
                    </div>
                </form>
                <div style="text-align: center; margin-top: 20px;">
                    <h5>Контактные данные:</h5>
                    <p>Моб.тел.: +38 095 6737073</p>
                    <p>Адрес: ул.Земляной Вал, 33, Москва, Россия, 105064</p>
                </div>
            </div>
            
        </div>
    </div>
</div>
<!-- Modal question -->

<!-- Modal question -->
<div class="modal fade" id="question-gilbo" tabindex="-1" role="dialog"
     aria-labelledby="question-modal-gilbo">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="question-modal-gilbo">Индивидуальные консультации от Е.В. Гильбо</h4>
            </div>
            <div class="modal-body">
                <form action="{{route('question_user')}}" method="POST">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="form-group">
                        <input type="text" name="name" placeholder="Ваше имя" value="@if (Auth::check()){{Auth::user()->fullFIO}}@endif" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" placeholder="Ваш email" value="@if (Auth::check()){{Auth::user()->email}}@endif" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <textarea placeholder="Ваш вопрос" name="ask_user" class="form-control mceNoEditor" style="max-width: 570px"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="rules" required>
                            {!!trans('home.rulesleader')!!}
                    </div>
                    <div class="modal-footer" style="text-align: center">
                        <button type="submit" class="btn-submit-modal">{{trans('home.send')}}</button>
                    </div>
                </form>
            </div>
            
        </div>
    </div>
</div>
<!-- Modal question -->


<!-- Modal subscribe-blog -->
<div class="modal fade" id="subscribe-blog" tabindex="-1" role="dialog"
     aria-labelledby="question-modalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="question-modallLabel">Подписаться</h4>
            </div>
            <div class="modal-body">
                <form action="{{ route('subscribers.store') }}" 
                      method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon hidden-sm">@</div>
                            <input type="text" class="form-control" name="email" placeholder="Введите e-mail">
                        </div>
                    </div>
                    <div class="modal-footer" style="text-align: center">
                        <button type="submit" class="btn-submit-modal">Подписаться</button>
                    </div>
                </form>
            </div>
            
        </div>
    </div>
</div>
<!-- Modal subscribe-blog -->
<!-- Modal consultation-->
                    <div class="modal fade" id="support-modal" tabindex="-1" role="dialog"
                         aria-labelledby="support-modalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header text-center">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="support-modallLabel"  style="text-transform: initial;">{{trans('home.support')}}</h4>
                                </div>
                                <div class="modal-body">
                                <div class="row">
                                    <?php 
                                    $sup_fase = array(
                                        0 => array(
                                            'name' => 'Наталья',
                                            'foto' => 'images/front/girl-support1.jpg'
                                        ),
                                        1 => array(
                                            'name' => 'Мария',
                                            'foto' => 'images/front/girl-support2.jpg'
                                        ),
                                        2 => array(
                                            'name' => 'Юлия',
                                            'foto' => 'images/front/girl-support3.jpg'
                                        ),
                                        3 => array(
                                            'name' => 'Анастасия',
                                            'foto' => 'images/front/girl-support4.jpg'
                                        ),
//                                        4 => array(
//                                            'name' => 'Василиса',
//                                            'foto' => 'images/mane/support-portrait.png'
//                                        ),
//                                        5 => array(
//                                            'name' => 'Маргарита',
//                                            'foto' => 'images/mane/support-portrait.png'
//                                        ),
//                                        6 => array(
//                                            'name' => 'Ирина',
//                                            'foto' => 'images/mane/support-portrait.png'
//                                        ),
//                                        7 => array(
//                                            'name' => 'Ксения',
//                                            'foto' => 'images/mane/support-portrait.png'
//                                        )
                                    );
                                    shuffle($sup_fase);
                                    $sup_fase = $sup_fase[0];
                                    ?>
                                    <div class="col-md-3">
                                        <img src="{{asset($sup_fase['foto'])}}" class="img-responsive" alt=""/>
                                    </div>
                                    <div class="col-md-9">
                                        <div><b>{{trans('home.yourmanager')}} {{ $sup_fase['name'] }}</b></div>
                                        <div>{{trans('home.supportmail')}}</div>
                                    </div>
                                </div>
                                    <form action="{{route('support_send_email')}}" method="POST" class="text-center">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <div class="form-group">
                                            <input type="text" name="name" placeholder="Ваше имя" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" name="email" placeholder="Ваш email" class="form-control">
                                        </div>
                                        <!--div class="form-group">
                                            <input type="text" name="phone" placeholder="Ваш номер телефона" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="skype" placeholder="Ваше имя Skype" class="form-control">
                                        </div-->
                                        <div class="form-group">
                                            <select name="subject" class="form-control" >
                                                <option value="Вопросы оплаты">{{trans('home.payq')}}</option>
                                                <option value="Техническая проблема">{{trans('home.techq')}}</option>
                                                <option value="Общие вопросы">{{trans('home.allq')}}</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <textarea type="text" name="question" placeholder="Ваш вопрос" class="form-control" style="max-width: 570px"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-blue">{{trans('home.send')}}</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
<!-- Modal workewithus -->
<div class="modal fade" id="workewithus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel"><strong>{{trans('oferta.workewithushead')}}</strong></h4>
            </div>
            <div class="modal-body">
                {!!trans('oferta.workewithus-text')!!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('home.close')}}</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal workewithus -->
<!-- Modal rules -->
<div class="modal fade" id="rules" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel"><strong>{{trans('home.rulesshel')}}</strong></h4>
            </div>
            <div class="modal-body">
                {!!trans('oferta.privacy-text')!!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('home.close')}}</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal rules -->
<!-- Modal oferta -->
<div class="modal fade" id="oferta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel"><strong>{{trans('oferta.header_oferta')}}</strong></h4>
            </div>
            <div class="modal-body">
                {!!trans('oferta.text_oferta')!!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('home.close')}}</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal oferta -->
<!-- Modal login-->
<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="login-modalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id=login-modallLabel" style="text-align: center;">{{trans('home.entrancecabinet')}}</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}
                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                        <input id="email" type="email" placeholder="Email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                        @if ($errors->has('email'))
                            <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                        <input id="password" type="password" placeholder="Пароль" class="form-control" name="password" required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="checkbox">
                        <label class="checkboxlogin">
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{trans('home.rememberme')}}
                        </label>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn-submit-modal">{{trans('home.entrancecabinet')}}</button>
                    </div>
                    
                </form>
                <div class="clearfix"></div>
                <hr>
                <div class="container-fluid">
                    <a class="forgot pull-left" href="{{ route('password.request') }}">{{trans('home.forgotpass')}}</a>
                    <a class="reg pull-right" href="{{ url('/register') }}">{{trans('home.register')}}</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Modal login-->
<!-- bank-card -->
<div class="modal fade" id="bank-card" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Банковская карта:</h4>
      </div>
      <div class="modal-body">
        <strong>Карта ВТБ</strong>
        <p>5543 8633 4333 5299</p>
        <p>Гильбо Евгений Витальевич, evgeny gilbo</p>
        <p>Поле комментарий оставить пустым</p>
        <p><strong>Карта ТИНЬКОФФ</strong></p>
        <p><strong>Номер карты: <br>5536 9137 7168 6694</strong></p>
        <p>Банк-получатель:<br>
        АО «Тинькофф Банк»</p>
        <p>Корр. счет:<br>
        30101810145250000974</p>
        <p>БИК:<br>
        044525974</p>
        <p>Получатель:<br>
        Гильбо Евгений Витальевич</p>
        <p>Счет получателя:<br>
        40817810300005285877</p>
        <p>Назначение платежа: Перевод средств по договору № 5072373912 Гильбо Евгений Витальевич НДС не облагается</p>
        <p>ИНН:<br>
        Укажите ваш ИНН, если его нет, поле оставьте пустым (при строгой необходимости заполнять поле – укажите 7710140679)</p>
        <p>КПП:<br>
        Поле оставьте пустым (при строгой необходимости заполнять поле – укажите 773401001)</p>
        <p>номер договора - 5072373912</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
      </div>
    </div>
  </div>
</div>
<!-- bank-card -->
<!--eu-bank-->
<div class="modal fade" id="eu-bank" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Реквизиты р\с в ЕС:</h4>
      </div>
      <div class="modal-body">
        <p>Bank: CIM Banque</p>
        <p>Name: IEVGENII BATURA</p>
        <p>Account No: 104280095</p>
        <p>IBAN No: CH0808822104280095000</p>
        <p>SWIFT: CIMMCHGG</p>
        <p>Bank address: 16 Rue Merle d'Aubigné, 1207 Genève - Suisse</p>
        <p>Поле комментарий оставить пустым</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
      </div>
    </div>
  </div>
</div>
<!--eu-bank-->
<!--money-transfer-->
<div class="modal fade" id="money-transfer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Реквизиты для денежных переводов:</h4>
      </div>
      <div class="modal-body">
        <p>Получатель: Батура Евгений Константинович</p>
        <p>Адрес: Украина, Днепр</p>
        <p>Телефон: +38 067 564 52 64</p>
        <hr>
        <p>Recipient: Batura Ievgenii</p>
        <p>Adress: Ukraine, Dnipro</p>
        <p>Telephone: +38 067 564 52 64</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
      </div>
    </div>
  </div>
</div>
<!--money-transfer-->
<!-- tooltips -->
<script>
    $(function () {
        // инициализация всплывающих подсказок
        $('[data-toggle1="tooltipsupport"]').tooltip({
            title : '{{ trans('tooltips.tooltipsupport') }}',
            placement: 'bottom',
            delay: { show: 300, hide: 1000 }
        });
        $('[data-toggle2="tooltipenter"]').tooltip({
            title : '{{ trans('tooltips.tooltipenter') }}',
            placement: 'bottom',
            delay: { show: 300, hide: 300 }
        });
        $('[data-toggle-progress="tooltipprogress"]').tooltip({
            title : '{{ trans('tooltips.tooltipprogress') }}',
            placement: 'bottom',
            delay: { show: 300, hide: 300 }
        });
        $('[data-toggle-home="tooltiphomeone"]').tooltip({
            title : '{{ trans('tooltips.tooltiphomeone') }}',
            placement: 'bottom',
            delay: { show: 300, hide: 300 }
        });
        $('[data-toggle-home="tooltiphometwo"]').tooltip({
            title : '{{ trans('tooltips.tooltiphometwo') }}',
            placement: 'bottom',
            delay: { show: 300, hide: 300 }
        });
        $('[data-toggle-home="tooltiphomethree"]').tooltip({
            title : '{{ trans('tooltips.tooltiphomethree') }}',
            placement: 'bottom',
            delay: { show: 300, hide: 300 }
        });
        $('[data-toggle-referal="tooltiprefscheme"]').tooltip({
            title : 'Реферальная система ШЭЛ',
            placement: 'bottom',
            delay: { show: 300, hide: 300 }
        });
        $('[data-toggle-referal="tooltiprefpay"]').tooltip({
            title : 'Информация о заработке Вашей реферальной системы',
            placement: 'bottom',
            delay: { show: 300, hide: 300 }
        });
        $('[data-toggle-finance="tooltipfinance"]').tooltip({
            title : 'Информация о финансах',
            placement: 'bottom',
            delay: { show: 300, hide: 300 }
        });
        $('[data-toggle-ball="tooltipball"]').tooltip({
            title : 'Информация о баллах',
            placement: 'bottom',
            delay: { show: 300, hide: 300 }
        });
        
        @if (Auth::check())
        $('[data-toggle-header="tooltipalertballs"]').tooltip({
            title : 'У Вас {{Auth::user()->level_point}} баллов, успей обменять на КЭЛы',
            placement: 'bottom',
            html: true,
            template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner" style="min-width: 150px;"></div></div>',
            delay: { show: 300, hide: 300 }
        });
    
        $('[data-toggle-header="tooltipalertbalance"]').tooltip({
            title : 'Ваш баланс ( {{Auth::user()->balance}} ) скоро будет исчерпан, пополните',
            placement: 'bottom',
            html: true,
            template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner" style="min-width: 150px;"></div></div>',
            delay: { show: 300, hide: 300 }
        });
    
        $('[data-toggle-header="tooltipmoney"]').tooltip({
            title : '<span style="width: 50%;float: left;text-align: left;padding-left: 10px;">{{Auth::user()->balance}}</span><span style="width: 50%; float: left; text-align: left;padding-left: 10px;">КЭЛ</span><br><span style="width: 50%;float: left;text-align: left;padding-left: 10px;">{{Auth::user()->level_point}}</span><span style="width: 50%; float: left; text-align: left;padding-left: 10px;">баллов</span><br/>',
            placement: 'bottom',
            html: true,
            template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner" style="min-width: 150px;"></div></div>',
            delay: { show: 300, hide: 300 }
        });
        @endif
        $('[data-toggle-catalogue="seminars"]').tooltip({
            title : 'В данном разделе Вы сможете:<br>- ознакомиться с описанием и приобрести участие в семинарах<br>- ознакомиться с описанием и приобрести записи уже состоявшихся семинаров<br>- ознакомиться с описанием и приобрести записи уже состоявшихся вебинаров',
            html: true,
            placement: 'bottom',
            delay: { show: 300, hide: 300 }
        });
        $('[data-toggle-catalogue="webcourses"]').tooltip({
            title : 'В данном разделе Вы сможете:<br>- ознакомиться с афишей предстоящих вебинаров<br>- приобрести участие в заинтересовавших Вас вебинарах<br>- просмотреть архив уже состоявшихся',
            html: true,
            placement: 'bottom',
            delay: { show: 300, hide: 300 }
        });
        $('[data-toggle-catalogue="partnercourses"]').tooltip({
            title : 'В данном разделе Вы сможете:<br>ознакомиться с блоками курсов Партнеров Школы Эффективных Лидеров, их структурой,  стоимостью и  количеством баллов, которые можно получить за прохождение курса,  а также включить желаемые курсы в свой учебный план', 
            html: true,
            placement: 'bottom',
            delay: { show: 300, hide: 300 }
        });
        $('[data-toggle-catalogue="maincourses"]').tooltip({
            title : 'В данном разделе Вы сможете:<br>ознакомиться с блоками курсов Школы Эффективных Лидеров, их структурой,  стоимостью и  количеством баллов, которые можно получить за прохождение курса,  а также включить желаемые курсы в свой учебный план',
            html: true,
            placement: 'bottom',
            delay: { show: 300, hide: 300 }
        });
    });
    
    
    $('body').on('hidden.bs.modal', '.modal', function () {
$('video').trigger('pause');
$('audio').trigger('pause');
});

</script>

<!-- tooltips -->