<nav class="navbar navbar-default navbar-fixed-top header_area">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{asset('images/mane/logo-star.png')}}" alt="LSG">
                <span class="slogan">{{trans('home.shelzag')}}</span>
            </a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            @if ( request()->route()->getName()=='main_front_page')
            <ul class="nav navbar-nav">
                <li><a class="" href="#fifthsection">Программа обучения</a></li>
                <li><a class="" href="#naukasection">Наука Лидерства</a></li>
                <li><a class="" href="#ninethsection">Блоги</a></li>
            </ul>
            @else
            <ul class="navbar-nav navbar-left ml-auto" id="nav">
                <li  style="list-style: none;"><span class="nav-link">{!!$title_page!!}</span></li>
            </ul>
            @endif
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="#" class="nav-link" data-toggle="modal" data-target="#support-modal" data-toggle1="tooltipsupport">
                        <span class="visible-xs">Служба поддержки</span><i class="fa fa-envelope hidden-xs" aria-hidden="true"></i>
                    </a>
                </li>
                @if (Auth::check())
                    <li class="dropdown">
                        <a class="nav-link dropdown-toggle avapaneldrop" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            <span class="avatar-bg-panel" style="background-image: url({{ Auth::user()->avatar }}); background-size: cover;"></span>
                            <!--img class="img-responsive  avapanel" src="{{ Auth::user()->avatar }}" /-->
                        </a>
                        <ul class="dropdown-menu">
                            <div class="infopanelcabinet">
                                <span class="avatar-bg-panel col-md-3 pull-left" style="background: url({{ Auth::user()->avatar }}); background-size: cover;"></span>
                                <!--img class="img-responsive  avapaneldrop " src="{{ Auth::user()->avatar }}" /-->
                                <div class="user-name col-md-9 pull-right">
                                    <a href="{{ route('/personal_area') }}">{{Auth::user()->fullFIO}}</a>
                                </div>
                                <div class="email col-md-9 pull-right"><b>{{Auth::user()->email}}</b></div>
                                <div class="col-md-9 pull-right"><span class="width-one">{{trans('site_bar.yourballance')}}, {{trans('home.kel')}}:</span> <b>{{Auth::user()->balance}}</b></div>
                                <div class="col-md-9 pull-right"><span class="width-one">{{trans('site_bar.yuorscore')}}:</span> <b>{{Auth::user()->level_point}}</b></div>
                                <div class="col-md-9 pull-right"><span class="width-one">{{trans('site_bar.yourstatus')}}:</span> <b>{{Auth::user()->role->description}}</b></div>
                                <span class="line-popup"></span>
                                <div class="col-md-12">
                                    <a class="btn-blue" href="{{ route('/personal_area') }}">Перейти в кабинет</a>
                                    <a class="btn-blue pull-right" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{trans('content.logout')}}</a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                </div>

                            </div>
                        </ul>
                    </li>
                @else
                <li class="nav-item">
                    <a href="#" class="login nav-link" data-toggle="modal" data-target="#login-modal" data-toggle2="tooltipenter" >
                        <span class="visible-xs">Вход/Регистрация</span><i class="fas fa-sign-in-alt hidden-xs"></i> {{--trans('home.entrancecabinet')--}}
                    </a>
                </li>
                @endif
            </ul>
        </div>
    </div>
</nav>