<!--nav.blade.php-->
<div id="header">
    <div class="container-fluid">
        <div class="row">
            <!-- Collapsed Hamburger -->
            <button type="button" class="hidden navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="logo hidden-xs">
                <a href="{{ url('/') }}">
                    <img src="{{asset('images/mane/logo-star.png')}}" alt="LSG">
                    <span class="slogan">{{trans('home.shelzag')}}</span>
                </a>
            </div>
            <div class="login-drop" style="width: inherit;">
            @if (Auth::check())
                        <span style="color: white">Баланс: {{Auth::user()->balance}}</span>
                        <img class="img-circle" src='{{ Auth::user()->avatar }}'style="height: 50px; width: 50px;"/>
                        <a style="padding: 10px 12px; margin-right: 10px;" 
                           class="btn btn-blue"
                           href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                {{trans('content.logout')}}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form> 
            @endif
            </div>
            <nav class="navbar navbar-default navbar-static-top" style="width: 65%;">
                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <ul class="top-menu">
                        <li class="visible-xs">
                            <a href="/">{{trans('home.homepage')}}</a>
                        </li>
                        {{--@if($pages->count() !=0)--}}
                            {{--@foreach($pages as $page)--}}
                        {{--<li>--}}
                            {{--<a href="{{route('page_admin',$page->slug)}}"  role="button" >{{$page->name}}</a>--}}
                        {{--</li>--}}
                            {{--@endforeach--}}
                        {{--@endif--}}
                        <li>
                            <a href="{{route('admin_all_archive.index')}}/">{{trans('home.leaderscience')}}</a>
                        </li>
                        <li>
                            <a href="{{ route('blog.index') }}">{{trans('home.blog')}}
                                    @if($blogcounts->get('new'))
                                    <span class="badge" title="новых {{ $blogcounts->get('new') }}">{{ $blogcounts->get('new') }}</span>
                                    @endif
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('unews.index') }}">{{trans('home.webinar')}}</a>
                        </li>
                        <li>
                            <!-- Button trigger modal -->
                            <a  href="#" data-toggle="modal" data-target="#forumSDO">{{trans('home.forum')}}</a>
                        </li>
                        <li>
                            <a href="#" data-toggle="modal" data-target="#shopSDO">{{trans('home.magaz')}}</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="forumSDO" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans('home.forum')}}</h4>
            </div>
            <div class="modal-body">
                <h1>{{trans('home.obrzag')}}</h1>
               <p>{{trans('home.obrtextforum')}}</p>
               <p>{{trans('home.obrthanks')}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('home.cancel')}}</button>
                <a target="_blank" href="http://forum.gilbo.ru/" type="button" class="btn btn-blue" >{{trans('home.next')}}</a>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="shopSDO" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans('home.magaz')}}</h4>
            </div>
            <div class="modal-body">
                <h1>{{trans('home.obrzag')}}</h1>
                <p>{{trans('home.obrthanksmag')}}</p>
                <p>{{trans('home.obrthanks')}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('home.cancel')}}</button>
                <a target="_blank" href="http://insiderclub.ru/shop/" type="button" class="btn btn-blue" >{{trans('home.next')}}</a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="webRoom" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans('home.webinar')}}</h4>
            </div>
            <div class="modal-body">
                <h1>{{trans('home.obrzag')}}</h1>
                <p>{{trans('home.obrthankswebinar')}}</p>
                <p>{{trans('home.obrthanks')}}</p>
            </div>
            <div class="modal-footer">
                <a href="{{ route('unews.index') }}" class="btn btn-blue"> - NEWS - </a>
                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('home.cancel')}}</button>
            </div>
        </div>
    </div>
</div><div class="modal fade" id="blog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans('home.blog')}}</h4>
            </div>
            <div class="modal-body">
                <h1>{{trans('home.obrzag')}}</h1>
                <p>{{trans('home.obrthanksblog')}}</p>
                <p>{{trans('home.obrthanks')}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('home.cancel')}}</button>
            </div>
        </div>
    </div>
</div>