@extends('layouts.app')

@section('main_body')
    <!--HEADER-->
        @include('layouts.partials.header_front',['title_page'=>isset($title_page)?$title_page:NULL])
    <!--END HEADER-->

    
<div class="content">
    @yield('content')
</div>


    <!-- OPEN FOOTER  -->
        @include('layouts.partials.footer')
    <!--END FOOTER-->
@endsection