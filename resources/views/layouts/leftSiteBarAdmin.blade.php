<aside class="left-sidebar">
        <div class="side-nav-toggle"></div>
        <!-- User Info -->
        <div class="user">
            <div class="info-container menu">
                <!--header>
                    <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top"></i>
                </header-->

                    <div class="user-photo" style="background-image: url({{ Auth::user()->avatar }})">

                            <!-- Branding Image -->
                                <a class="logo" href="{{ url('/') }}">
                                    <img src="{{asset('images/mane/logo.png')}}" width="32" height="32" alt="SDO">
                                </a>
                            </div>
                            <div class="user-name">{{Auth::user()->name}}</div>
                            <div class="email">{{Auth::user()->email}}</div>

                            <!-- Account navs -->
                            <div class="balance">
                                На балансе: {{Auth::user()->balance}}
                            </div>
                        {{--<li>--}}
                        {{--<div class="language">--}}
                        {{--<button class="dropdown-toggle" type="button" id="dropdown-lang" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                        {{--<div>{{trans('site_bar.menu')}}</div>--}}
                        {{--<span class="caret"></span>--}}
                        {{--</button>--}}
                        {{--<ul class="dropdown-menu" aria-labelledby="dropdown-lang">--}}
                        {{--@foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)--}}
                        {{--<li>--}}
                        {{--<a rel="alternate" hreflang="{{$localeCode}}" href="{{LaravelLocalization::getLocalizedURL($localeCode) }}">--}}
                        {{--@foreach($lg_logo as $logo)--}}
                        {{--@if($properties['name']=== $logo->name)--}}
                        {{--<img src="{{asset($logo->image)}}" alt="{{trans('site_bar.menu')}}">--}}
                        {{--@endif--}}
                        {{--@endforeach--}}
                        {{--{{ $properties['native'] }}--}}
                        {{--</a>--}}
                        {{--</li>--}}
                        {{--@endforeach--}}
                        {{--</ul>--}}
                        {{--</div>--}}
                        {{--</li>--}}
                        <!-- END account navs -->
                    </div>
            </div>
        <!-- #User Info -->
            
                    <!-- Menu -->
                    <div class="menu">
                        <ul>

                            <li>
                                <a href="{{route('/personal_area')}}">
                                    <i class="fa fa-user"></i> {{trans('content.personal_area')}}
                                </a>
                            </li>
                            <li>
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                                   aria-expanded="false"
                                   aria-controls="collapseOne">
                                    <i class="fa fa-users" aria-hidden="true"></i>
                                    <span>{{trans('site_bar.users')}}</span>
                                </a>
                                <ul id="collapseOne" class="ml-menu panel-collapse collapse" role="tabpanel">
                                    <li>
                                        <a href="{{route('cadet.index')}}">
                                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                            <span>{{trans('content.cadets')}}</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('curator.index')}}">
                                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                            <span>{{trans('content.curators')}}</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('lectors.index')}}">
                                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                            <span>{{trans('content.lecturers')}}</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('black_list.index')}}">
                                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                            <span>Черный список</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('group_user.index')}}">
                                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                            <span>Группы пользователей</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('roles_user.index')}}">
                                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                            <span>Роли в системе</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"
                                   aria-expanded="false"
                                   aria-controls="collapseTwo">
                                    <i class="fa fa-book" aria-hidden="true"></i>
                                    <span>{{trans('content.materials')}}</span>
                                </a>
                                <ul id="collapseTwo" class="ml-menu panel-collapse collapse" role="tabpanel">
                                    
                                    <li>
                                        <a href="{{route('admin_block.index')}}">
                                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                            <span>БЛОКИ</span>
                                        </a>
                                    </li>
                                    
                                    <li>
                                        <a href="{{route('curses.index')}}">
                                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                            <span>{{trans('content.courses')}}</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('prelection.index')}}">
                                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                            <span>{{trans('content.lectures')}}</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('materials.index')}}">
                                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                            <span>{{trans('content.additional_materials')}}</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('test.index')}}">
                                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                            <span>{{trans('content.tests')}}</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('text_assignments.index')}}">
                                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                            <span>{{trans('content.text_assignments')}}</span>
                                        </a>
                                    </li>

                                </ul>
                            </li>
                            <li>
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive"
                                   aria-expanded="false"
                                   aria-controls="collapseTwo">
                                    <i class="fa fa-book" aria-hidden="true"></i>
                                    <span>Домашняя работа</span>
                                </a>
                                <ul id="collapseFive" class="ml-menu panel-collapse collapse" role="tabpanel">
                                    <li>
                                        <a href="{{route('checking_text_tasks.index')}}">
                                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                            <span>Текстовые ответы</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('favorite_list')}}">
                                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                            <span>Избранные</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="{{route('admin_events.index')}}" class="menu-toggle">
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                    <span>Мероприятия</span>
                                </a>
                            </li>
                            <li>
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"
                                   aria-expanded="false"
                                   aria-controls="collapseThree">
                                    <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                                    <span>{{trans('content.news')}}</span>
                                </a>
                                <ul id="collapseThree" class="ml-menu panel-collapse collapse" role="tabpanel">
                                    <li>
                                        <a href="{{route('news.index')}}">
                                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                            <span>Статьи</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('video_news.index')}}">
                                            <i class="fa fa-file-video-o" aria-hidden="true"></i>
                                            <span>Видео новости</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="{{route('ablog.index')}}">
                                    <i class="fab fa-blogger-b" aria-hidden="true"></i>
                                    <span>Блог </span>
                                    @if($blogcounts->get('new'))
                                    <span class="badge pull-right">{{ $blogcounts->get('new') }}</span>
                                    @endif
                                </a>
                            </li>
                            <li>
                                <a href="{{route('atags.index')}}">
                                    <i class="fa fa-tags" aria-hidden="true"></i>
                                    <span>ТЕГИ</span>
                                </a>
                            </li>
                            <li>
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"
                                   aria-expanded="false"
                                   aria-controls="collapseFour">
                                    <i class="fas fa-chart-line" aria-hidden="true"></i>
                                    <span>{{trans('content.statistic')}}</span>
                                </a>
                                <ul id="collapseFour" class="ml-menu panel-collapse collapse" role="tabpanel">
                                    <li>
                                        <a href="{{route('biling_statistics.index')}}">
                                            <i class="fa fa-money" aria-hidden="true"></i>
                                            <span>Пополнения</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('admin_repl_statistics')}}">
                                            <i class="fa fa-money" aria-hidden="true"></i>
                                            <span>Пополнения админом</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('orders_statistics.index')}}">
                                            <i class="fa fa-credit-card-alt" aria-hidden="true"></i>
                                            <span>Покупки</span>
                                        </a>
                                    </li>

                                </ul>
                            </li>
                            
                            <li>
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseREF"
                                   aria-expanded="false"
                                   aria-controls="collapseREF">
                                    <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                                    <span>Реферальная с-ма</span>
                                </a>
                                <ul id="collapseREF" class="ml-menu panel-collapse collapse" role="tabpanel">
                                    <li>
                                        <a href="{{route('refrules.index')}}">
                                            <i class="fa fa-share-alt" aria-hidden="true"></i>
                                            <span>Условия</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('refsystem.index')}}">
                                            <i class="fa fa-share-alt" aria-hidden="true"></i>
                                            <span>Рефералы</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('refhistory.index')}}">
                                            <i class="fa fa-share-alt" aria-hidden="true"></i>
                                            <span>История начислений</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                <a href="{{route('profile.index')}}" class="menu-toggle">
                                    <i class="fa fa-wrench" aria-hidden="true"></i>
                                    <span>{{trans('content.profile_setting')}}</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('message.index')}}" class="menu-toggle">
                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                    <span>{{trans('content.message')}}</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('subscribers.index')}}" class="menu-toggle">
                                    <i class="fa fa-globe" aria-hidden="true"></i>
                                    <span>Подписчики</span>
                                </a>
                            </li>
                            <li>
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix"
                                   aria-expanded="false"
                                   aria-controls="collapseSix">
                                    <i class="fa fa-desktop" aria-hidden="true"></i>
                                    <span>Фронт SDO</span>
                                </a>
                                <ul id="collapseSix" class="ml-menu panel-collapse collapse" role="tabpanel">
                                    <li>
                                        <a href="{{route('static_pages.index')}}">
                                            <i class="fa fa-product-hunt" aria-hidden="true"></i>
                                            <span>Статические страницы</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('else_project.index')}}">
                                            <i class="fa fa-product-hunt" aria-hidden="true"></i>
                                            <span>Наши проекты</span>
                                        </a>
                                    </li>
                                    {{--<li>--}}
                                    {{--<a href="{{route('admin_about_us.index')}}">--}}
                                    {{--<i class="fa fa-list-alt" aria-hidden="true"></i>--}}
                                    {{--<span>О нас</span>--}}
                                    {{--</a>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                    {{--<a href="{{route('advantages.index')}}">--}}
                                    {{--<i class="fa fa-qq" aria-hidden="true"></i>--}}
                                    {{--<span>Преимущества</span>--}}
                                    {{--</a>--}}
                                    {{--</li>--}}
                                    <li>
                                        <a href="{{route('admin_contacts.index')}}">
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                            <span>Контакты</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse8"
                                   aria-expanded="false"
                                   aria-controls="collapse8">
                                    <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                    <span>Учебная</span>
                                </a>
                                <ul id="collapse8" class="ml-menu panel-collapse collapse" role="tabpanel">
                                    <li>
                                        <a href="{{route('refrules.index')}}">
                                            <i class="fa fa-cog" aria-hidden="true"></i>
                                            <span>Настройка баллов рефералки</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('mentor_settings.index')}}">
                                            <i class="fa fa-cog" aria-hidden="true"></i>
                                            <span>Настройка баллов кураторства</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('connection_mentors.index')}}">
                                            <i class="fa fa-connectdevelop" aria-hidden="true"></i>
                                            <span>Связи</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            {{--<li>--}}
                                {{--<a href="#">--}}
                                    {{--<i class="fa fa-envelope-o" aria-hidden="true"></i>--}}
                                    {{--<span>Письма пользователей</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                            {{--<a href="{{route('language.index')}}" class="menu-toggle">--}}
                            {{--<i class="fa fa-globe" aria-hidden="true"></i>--}}
                            {{--<span>{{trans('content.language')}}</span>--}}
                            {{--</a>--}}
                            {{--</li>--}}
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                                    <i class="fa fa-power-off"></i> {{trans('content.logout')}}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>

                    </div>
                    <!-- #Menu -->        
</aside>
@push('stack_scripts')
    <script>
        // MOBILE MENU TOGGLE
        $('.side-nav-toggle').on('click', function () {
            $('.left-sidebar').toggleClass('open');
        });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })

    </script>
@endpush