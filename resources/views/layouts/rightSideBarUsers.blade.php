
    
    @if ( in_array('admin_all_archive', request()->segments()) || request()->route()->getName()=='all_archive_first_show' )
    <aside class="right-sidebar col-md-12">
        @include('layouts.partialsrightbar.nauka_liderstva')
    </aside>
    @endif
    @if (   in_array('blog', request()->segments()) ||
            request()->route()->getName()=='front_blog_gilbo' || 
            request()->route()->getName()=='front_blog_gilbo_show' || 
            request()->route()->getName()=='front_blog_gilbo_search' ||
            request()->route()->getName()=='front_blog_lecturer' || 
            request()->route()->getName()=='front_blog_lecturer_show' || 
            request()->route()->getName()=='front_blog_lecturer_search' ||
            request()->route()->getName()=='front_blog_gaypark' || 
            request()->route()->getName()=='front_blog_gaypark_show' || 
            request()->route()->getName()=='front_blog_gaypark_search' ||
            request()->route()->getName()=='front_blog_search' ||
            in_array('ablog', request()->segments()) ||
            in_array('blog_gilbo', request()->segments()) || 
            in_array('blog_lecturer', request()->segments()) || 
            in_array('blog_gaidpark', request()->segments())
        )
    <aside class="right-sidebar col-md-12">
    {{-- Для страниц блога --}}
        @include('layouts.partialsrightbar.blog_rightbar')
    {{-- Для страниц блога --}}
    </aside>
    @endif
    @if ( request()->route()->getName()=='/personal_area')
    <aside class="right-sidebar col-md-12">
    {{-- Для страртовой страниц рабочий стол --}}
        @include('layouts.partialsrightbar.main_rightbar')
    {{-- Для страртовой страниц рабочий стол --}}
    </aside>
    @endif
    @if ( request()->route()->getName()=='mybalance.index')
    <aside class="right-sidebar col-md-12">
    {{-- Для страртовой страниц рабочий стол --}}
        @include('layouts.partialsrightbar.balance_rightbar')
    {{-- Для страртовой страниц рабочий стол --}}
    </aside>
    @endif
    
</aside>