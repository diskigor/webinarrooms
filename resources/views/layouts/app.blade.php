<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <link rel="shortcut icon" href="{{asset('images/mane/favicon.png')}}" type="image/png"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="interkassa-verification" content="e901c1161467bf4b2041e0094ba3a0d4" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--meta name="viewport" content="width=3200, initial-scale=0.5, minimal-ui"-->
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Школа эффективных лидеров - @yield('title')</title>

        <!--script-->
        <script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
        <script src="{{ asset('js/jquery-migrate.min.js') }}"></script>
        <script src="{{ asset('js/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="{{asset('js/plagins.js')}}"></script>
        <script src="{{asset('js/active.js')}}"></script>
        <script src="{{asset('js/sweetalert.min.js')}}"></script>
        <script src="{{asset('js/jquery.formstyler.min.js')}}"></script>
        <script src="{{asset('js/pechenka.js')}}"></script>

        <!-- Webinar room Start -->
        <script src="{{asset('js/jquery.websocket.js')}}"></script>
        <script src="{{asset('js/jquery.json.js')}}"></script>
        <script src="{{asset('js/Flashphoners.js')}}"></script>
        <script src="{{asset('js/swfobject.js')}}"></script>
        <script src="{{asset('js/utils.js')}}"></script>
        <script src="{{asset('js/ConfigurationLoader.js')}}"></script>
        <script src="{{asset('js/switch.js')}}"></script>

        <script src="{{asset('js/moment-with-locales.min.js')}}"></script>
        <script src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script>

        <link rel="stylesheet" type="text/css" href="{{ asset('css/video-chat.css') }}">

        <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">
        <!-- Webinar room End -->

        <script>
            window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
            ]) !!};
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            });
        </script>
        
        @yield('head_script')
        @stack('stack_head_script')
        <!--script-->

        <!--style-->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}"/>
        <link rel="stylesheet" type="text/css" href="{{ asset('css/owl.carousel.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/owl.theme.default.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.formstyler.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.formstyler.theme.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/style-s.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/sweetalert.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

        @yield('head_style')
        @stack('stack_head_style')    
        <!--style-->

    </head>
    <body>
        @yield('main_body')

        <!-- alerts -->
        @include('sweet::alert')
        @if(Session::has('status'))
        
        <script>
            swal({
            title: '{!! Session::get("status") !!}', 
            html:  true,  
            text: '{!! Session::get("text") !!}' });
        </script>
        @endif
        

        
        <!-- end alerts -->

        <!-- For Vova -->
        @yield('script')
        @stack('stack_scripts')
        <!-- For Vova -->

    </body>
</html>
