<div class="header_menu col-md-12">Поиск</div>
<div class="sidebar_events col-md-12">
    <form class="form-inline search-blog"
          action="{{ isset($blogs->linktoroutesearch)?$blogs->linktoroutesearch:'' }}">
        <div class="form-group">
          <input type="text" 
                 class="form-control" 
                 id="textsearch" 
                 name="textsearch"
                 placeholder="Поиск"
                 value="{{ request()->input('textsearch') }}"
                 />
        </div>
        <button type="submit" class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i></button>
    </form>
</div>
<div class="header_menu col-md-12">Популярные статьи в блогах</div>
@foreach($right_dates->get('blogs') as $blog)
<div class="sidebar_blog col-md-12">
    <div class="col-md-12 sb_blog_img">
        <h5 class="sb_blog_header">
            <img class="img-right-bar" src="{{ $blog->image }}" />
            {{ $blog->title }}
        </h5>
        <p class="sb_blog_shortdesc col-md-12">{{ mb_strimwidth (strip_tags($blog->article),0,100,'...') }}</p>
        <div class="cleafix"></div>
        <p class="sb_blog_author col-md-5 col-xs-5 ">{{ $blog->authorFIO }}<br>
            {{-- $blog->created_at->toDateString() --}}
        </p>
        @if(Auth::check())
            <span class="sb_blog_more col-md-7 col-xs-7"><a href="{{ route('blog.show',$blog->slug) }}">Подробнее <i class="fa fa-arrow-circle-right"></i></a></span>
        @else
            <span class="sb_blog_more col-md-7 col-xs-7"><a href="{{ route('front_blog_show',$blog->slug) }}">Подробнее <i class="fa fa-arrow-circle-right"></i></a></span>
        @endif
        
    </div>
</div>
@endforeach
<div class="header_menu col-md-12">Рекомендуем</div>
@foreach($right_dates->get('projects') as $project)
<div class="sidebar_blog col-md-12">
    <div class="col-md-3 sb_blog_img">
        <img class="img-responsive" src="{{ $project->image }}" />
    </div>
    <div class="col-md-9">
        <span class="sb_blog_header col-md-12">{{ $project->name }}</span>
        <span class="sb_blog_shortdesc col-md-12">{{ mb_strimwidth (strip_tags($project->short_desc),0,100,'...') }}</span>
    </div>
    <span class="sb_blog_more col-md-7 col-xs-7"><a href="{{ $project->href }}">Подробнее <i class="fa fa-arrow-circle-right"></i></a></span>
</div>
@endforeach