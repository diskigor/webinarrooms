<div class="header_menu col-md-12">Конвертер валют</div>
<div class="sidebar_events col-md-12">
    <!--Currency Converter widget by FreeCurrencyRates.com -->
    <div id='gcw_mainFb3JcHNV2' class='gcw_mainFb3JcHNV2'></div>
    <a id='gcw_siteFb3JcHNV2' href='https://freecurrencyrates.com/ru/'>FreeCurrencyRates.com</a>
    <script>function reloadFb3JcHNV2(){ var sc = document.getElementById('scFb3JcHNV2');if (sc) sc.parentNode.removeChild(sc);sc = document.createElement('script');sc.type = 'text/javascript';sc.charset = 'UTF-8';sc.async = true;sc.id='scFb3JcHNV2';sc.src = 'https://freecurrencyrates.com/ru/widget-vertical?iso=RUBUAHUSDEUR&df=2&p=Fb3JcHNV2&v=fits&source=fcr&width=245&width_title=0&firstrowvalue=1&thm=A6C9E2,FCFDFD,4297D7,5C9CCC,FFFFFF,C5DBEC,FCFDFD,2E6E9E,000000&title=%D0%9A%D0%BE%D0%BD%D0%B2%D0%B5%D1%80%D1%82%D0%B5%D1%80%20%D0%B2%D0%B0%D0%BB%D1%8E%D1%82&tzo=-180';var div = document.getElementById('gcw_mainFb3JcHNV2');div.parentNode.insertBefore(sc, div);} reloadFb3JcHNV2(); </script>
    <!-- put custom styles here: .gcw_mainFb3JcHNV2{}, .gcw_headerFb3JcHNV2{}, .gcw_ratesFb3JcHNV2{}, .gcw_sourceFb3JcHNV2{} -->
    <!--End of Currency Converter widget by FreeCurrencyRates.com -->
</div>
<div class="header_menu col-md-12">Курс валют</div>

<div class="sidebar_blog col-md-12">
    <link rel="stylesheet" type="text/css" href="https://fortrader.org/informers/css?id=46949">
    <div class='FTinformersWrapper FT0a4927e864931ea6313fd9140416bef5 FT1369496724'>
        <script>document.querySelector(".FT0a4927e864931ea6313fd9140416bef5.FT1369496724").style.opacity=0;</script>
        <div class="informer_box informer_box_example clearfix alignCenter">
            <div class="inlineBlock">
                <table class="informer_table informer1">
                    <thead>
                        <tr>
                            <th colspan="2">
                                <a target="_blank" href="https://fortrader.org/currencyrates">Курсы валют ЦБ РФ</a>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="col1">
                            <th class="bd_l">Валюта</th>
                            <th class="bd_r">RUB</th>
                        </tr>
                        <tr class="col2 trClass " data-symbol="Доллар США" data-id="2">
                            <td class="icon_amount arrow"><a target="_blank" class="symbolContainer symbolContainer0a4927e864931ea6313fd9140416bef5" title="Доллар США" href="https://fortrader.org/currencyrates/usd">USD</a></td>
                            <td class="changeVal" data-column="todayCourse"></td>
                        </tr>
                        <tr class="col3 trClass " data-symbol="Евро" data-id="21">
                            <td class="icon_amount arrow"><a target="_blank" class="symbolContainer symbolContainer0a4927e864931ea6313fd9140416bef5" title="Евро" href="https://fortrader.org/currencyrates/eur">EUR</a></td>
                            <td class="changeVal" data-column="todayCourse"></td>
                        </tr>
                        <tr class="col3 trClass " data-symbol="Украинская гривна" data-id="49">
                            <td class="icon_amount arrow"><a target="_blank" class="symbolContainer symbolContainer0a4927e864931ea6313fd9140416bef5" title="Украинская гривна" href="https://fortrader.org/currencyrates/uah">UAH</a></td>
                            <td class="changeVal" data-column="todayCourse"></td>
                        </tr>
                        <tr class="col-data">
                            <td colspan="2">
                                <time class='ftDateTime' datetime="2018-07-02MSK10:30">Дата  <span class='ftDateTimeStr'>---</span> мск </time>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script async src="https://fortrader.org/informers/js?id=46949&m=1369496724" type="text/javascript"></script>
</div>

<div class="header_menu col-md-12">Рекомендуем</div>
@foreach($right_dates->get('projects') as $project)
<div class="sidebar_blog col-md-12">
    <div class="col-md-3 sb_blog_img">
        <img class="img-responsive" src="{{ $project->image }}" />
    </div>
    <div class="col-md-9">
        <span class="sb_blog_header col-md-12">{{ $project->name }}</span>
        <span class="sb_blog_shortdesc col-md-12">{{ mb_strimwidth (strip_tags($project->short_desc),0,100,'...') }}</span>
    </div>    
        <span class="sb_blog_more col-md-7 col-xs-7"><a href="{{ $project->href }}">Подробнее <i class="fa fa-arrow-circle-right"></i></a></span>
    
</div>
@endforeach