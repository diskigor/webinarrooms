<div class="header_menu col-md-12">Поиск</div>
<div class="sidebar_events col-md-12">
    <form class="form-inline search-blog"
          action="{{ route('admin_all_archive.search') }}">
        <div class="form-group">
          <input type="text" 
                 class="form-control" 
                 id="textsearch" 
                 name="textsearch"
                 placeholder="Поиск"
                 value="{{ request()->input('textsearch') }}"
                 />
        </div>
        <button type="submit" class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i></button>
    </form>
</div>
<div class="header_menu col-md-12">Рассылка</div>
<div class="sidebar_events col-md-12">
    <p class="rassilkarightbar text-center">Задать свой вопрос Е.В. Гильбо</p>
    <div class="text-center" style="margin-bottom: 15px">
    <a class="btn-blue-invers"  href="#" data-toggle="modal" data-target="#question-modal">Написать</a>
    </div>
    <p class="rassilkarightbar text-center">Индивидуальные консультации от Е.В. Гильбо</p>
    <div class="text-center" style="margin-bottom: 15px">
    <a class="btn-blue-invers" href="#" data-toggle="modal" data-target="#question-gilbo">Записаться</a>
    </div>
    <!--p><a class="mobileapp" href="https://play.google.com/store/apps/details?id=ru.gilbo.leadersschoolapp">
        <img class="col-md-8 col-md-offset-2 img-responsive" src="{{asset('images/appmobileg.png')}}">
    </a></p-->
</div>
