<div class="header_menu col-md-12">События</div>
@foreach($right_dates->get('events') as $event)
<div class="sidebar_events col-md-12">
    <div class="col-md-12 sb_events_img">
        <h5 class="sb_events_header">
            <img class="img-right-bar" src="{{ $event->image }}" />
            {{ $event->lang?$event->lang->name:'' }}
        </h5>
        <p class="sb_events_shortdesc">{{ mb_strimwidth (strip_tags($event->lang?$event->lang->description:''),0,100,'...') }}</p>
        <p class="col-md-5 col-xs-5 sb_events_date" >{{-- $event->created_at->toDateString() --}}</p>
        <p class="sb_events_more col-md-7 col-xs-7"><a href="{{ route('show_event',$event->slug) }}" target="blank">Подробнее <i class="fa fa-arrow-circle-right"></i></a></p>
    </div>
</div>
<div class="clearfix"></div>
@endforeach

<div class="header_menu col-md-12">Новые статьи в блогах</div>
@foreach($right_dates->get('blogs') as $blog)
<div class="sidebar_blog col-md-12">
    <div class="col-md-12 sb_blog_img">
        <h5 class="sb_blog_header">
            <img class="img-right-bar" src="{{ $blog->image }}" />
            {{ $blog->title }}
        </h5>
        <p class="sb_blog_shortdesc col-md-12">{{ mb_strimwidth (strip_tags($blog->article),0,100,'...') }}</p>
        <div class="cleafix"></div>
        <p class="sb_blog_author col-md-5 col-xs-5 ">{{ $blog->authorFIO }}<br>
            {{-- $blog->created_at->toDateString() --}}
        </p>
        <span class="sb_blog_more col-md-7 col-xs-7"><a href="{{ route('blog.show',$blog->slug) }}">Подробнее <i class="fa fa-arrow-circle-right"></i></a></span>
    </div>
</div>
@endforeach
<div class="header_menu col-md-12">Новые выпуски рассылки</div>
@foreach($right_dates->get('mediascans') as $mediascan)
<div class="sidebar_blog col-md-12">
    <p class="sb_blog_shortdesc col-md-12">{{ mb_strimwidth (strip_tags($mediascan->mediascan_body),0,100,'...') }}</p>
    <p class="sb_blog_author col-md-5 col-xs-5">{{ $mediascan->mediascan_date }}</p>
    @if(Auth::check())
        <p class="sb_blog_more col-md-7 col-xs-7"><a href="{{ route('admin_all_archive.edit',$mediascan->id) }}">Подробнее <i class="fa fa-arrow-circle-right"></i></a></p>
    @else
        <p class="sb_blog_more col-md-7 col-xs-7"><a href="{{ route('show_post',$mediascan->id) }}">Подробнее <i class="fa fa-arrow-circle-right"></i></a></p>
    @endif
</div>
@endforeach