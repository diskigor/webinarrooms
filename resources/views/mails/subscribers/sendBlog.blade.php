Здравствуйте, {{ $subscriber->UName }}.
<br>
<hr>
<h2>{{ $blog->title }}</h2>
<p>добавлено: {{ $blog->created_at }}</p>
@if($blog->author)
    <p>автор: {{ $blog->author->fullFIO }}</p>
@endif
<br>
<hr>
<p>{{ mb_strimwidth (strip_tags($blog->article),0,300,'...') }}</p>
<br>
<hr>

@if($blog->isLecturer)
<a href="{{ route('front_blog_lecturer_show',$blog->slug) }}">подробней</a>
@endif

@if($blog->isGilbo)
<a href="{{ route('front_blog_gilbo_show',$blog->slug) }}">подробней</a>
@endif

<p> Если из-за настроек Вашего почтового сервиса ссылка не активна, скопируйте ее и вставьте в поисковую строку браузера.</p>

<h4>Вы подписаны, на новые статьи блогов "ШЭЛ"</h4>