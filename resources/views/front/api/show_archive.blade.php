@extends('layouts.front')
@section('content')
    <div class="container">
        <h1 style="text-align: center;">Наука Лидерства</h1>
        <?php //$title_page='Наука Лидерства'; ?>
        <section class="bg_white text-justify">
            <p>Рассылка "Наука Лидерства" является частью образовательного проекта Школы Эффективных Лидеров. В ее
                рамках автор проекта отвечает на вопросы подписчиков, осуществляя открытое дистанционное
                консультирование (публичный коучинг) по вопросам, связанным с функционированием проекта, с тематикой
                курсов, со смежными вопросами.</p>
            <p>Содержание рассылки само по себе составляет существенную часть дистанционного образования в рамках нашего
                проекта. Знакомство с ее содержанием (ознакомьтесь с архивом предыдущих выпусков) позволит Вам по-новому
                взглянуть на свой образ жизни и образ действий, изменить некоторые элементы мировоззрения,
                препятствующие жизненному успеху. Живая ткань рассылки "Наука Лидерства" позволит понять и историю
                проекта "Школа эффективных лидеров", и его содержание, и его дух.</p>
            <p>Ознакомление с материалами этого раздела позволит Вам составить себе точное представление о проекте и
                принять осознанное решение - стоит ли Вам становиться курсантом Школы эффективных лидеров и членом Клуба
                эффективных лидеров.</p>
            <p>Успехов Вам на этом пути!</p>
            <div class="text-center">
                <a class="btn" data-toggle="modal" data-target="#question-modal">Задать свой вопрос Е.В.Гильбо</a>
            </div>
            <div class="text-center">
                <a class="" href="https://play.google.com/store/apps/details?id=ru.gilbo.leadersschoolapp"
                   target="_blank">
                    Приложение для Android <img height="55px" src="{{asset('images/gplay.png')}}">
                </a>
            </div>
        </section>
        <table class="table table-archive">
            @foreach($count_sends_year as $key=>$value)
                @if(isset($count_archive_year))
                    @if($key == $current_y)
                        <tr>
                            <th colspan="6" class="text-left">{{$key}}</th>
                            <th colspan="6" class="text-right">Всего выпусков: {{$value}}</th>
                        </tr>
                        <tr class="month">
                            <td>Январь</td>
                            <td>Февраль</td>
                            <td>Март</td>
                            <td>Апрель</td>
                            <td>Май</td>
                            <td>Июнь</td>
                            <td>Июль</td>
                            <td>Август</td>
                            <td>Сентябрь</td>
                            <td>Октябрь</td>
                            <td>Ноябрь</td>
                            <td>Декабрь</td>
                        </tr>
                        <tr>
                            @foreach($arr_month as $month)
                                <td valign="top">
                                    @if($count_archive_year->getMonthDay($current_y,$month)->count() != 0)
                                        @foreach($count_archive_year->getMonthDay($current_y,$month) as $day)
                                            <div>
                                                <a href="{{route('show_post',$day->id)}}">{{$day->day}}</a>
                                            </div>
                                        @endforeach
                                    @else
                                        <span></span>
                                    @endif
                                </td>
                            @endforeach
                        </tr>
                    @endif
                @endif
            @endforeach
            @foreach($count_sends_year as $key=>$value)
                @if($key != $current_y)
                    <tr>
                        <th colspan="6" class="text-left"><a
                                    href="{{route('show_years_archive',$key)}}">Год: {{$key}}</a>
                        </th>
                        <th colspan="6" class="text-right">выпусков: {{$value}}</th>
                    </tr>
                @endif
            @endforeach
        </table>
    </div>
@endsection