@extends('layouts.front')
@section('content')
<?php $title_page='Тест: насколько я успешен?'; ?>
    <div id="content">
        <div class="container">
            {!! Form::open(array('route'=>'front_test_answer','method'=>'POST')) !!}
            <hr>
            <h4>Вопрос № 1</h4>
            <p>Я не достиг пока успеха потому что - </p>
            <div class="form-group">
                {!! Form::radio('answer_1',5) !!}
                {!! Form::label('answer_1','Меня неправильно воспитали родители;') !!}
                <br>
                {!! Form::radio('answer_1',0) !!}
                {!! Form::label('answer_1','Я родился не в том социальном слое, в котором гарантировано социальное продвижение;') !!}
                <br>
                {!! Form::radio('answer_1',0) !!}
                {!! Form::label('answer_1','Социальные лифты мне закрыты в результате вражды к моей национальности;') !!}
                <br>
                {!! Form::radio('answer_1',5) !!}
                {!! Form::label('answer_1','Я родился в неподходящее время в неподходящей стране;') !!}
                <br>
                {!! Form::radio('answer_1',15) !!}
                {!! Form::label('answer_1','Я приложил недостаточно усилий или приложил в неверном направлении;') !!}
            </div>
            <hr>
            <h4>Вопрос № 2</h4>
            <p>Моя мать изменяла моему отцу </p>
            <div class="form-group">
                {!! Form::radio('answer_2',13) !!}
                {!! Form::label('answer_2','Я уверен в этом;') !!}

                <br>
                {!! Form::radio('answer_2',10) !!}
                {!! Form::label('answer_2','Я подозреваю это;') !!}

                <br>
                {!! Form::radio('answer_2',0) !!}
                {!! Form::label('answer_2','Этого не может быть, даже постановка вопроса для меня оскорбительна;') !!}

            </div>
            <hr>
            <h4>Вопрос № 3</h4>
            <p>Геополитические соперники ненавидят и пытаются уничтожить мою страну потому, что -</p>
            <div class="form-group">
                {!! Form::radio('answer_3',0) !!}
                {!! Form::label('answer_3','Они испытывают страх перед нашей мощью;') !!}

                <br>
                {!! Form::radio('answer_3',0) !!}
                {!! Form::label('answer_3','Они не выносят нашего морального превосходства;') !!}

                <br>
                {!! Form::radio('answer_3',0) !!}
                {!! Form::label('answer_3','Таковы их геополитические и экономические интересы;') !!}

                <br>
                {!! Form::radio('answer_3',15) !!}
                {!! Form::label('answer_3','Мне это кажется под воздействием пропаганды;') !!}

            </div>
            <hr>
            <h4>Вопрос № 4</h4>
            <p>Мне не везёт в жизни потому, что -</p>
            <div class="form-group">
                {!! Form::radio('answer_4',0) !!}
                {!! Form::label('answer_4','Окружающий мир ко мне настроен враждебно или безразлично;') !!}

                <br>
                {!! Form::radio('answer_4',0) !!}
                {!! Form::label('answer_4','Такая у меня карма;') !!}

                <br>
                {!! Form::radio('answer_4',10) !!}
                {!! Form::label('answer_4','В результате воспитания меня научили негодным методам решения проблем;') !!}

                <br>
                {!! Form::radio('answer_4',15) !!}
                {!! Form::label('answer_4','Мои представления о реальности неадекватны и нуждаются в коррекции;') !!}

                <br>
                {!! Form::radio('answer_4',5) !!}
                {!! Form::label('answer_4','С чего вы взяли, что мне не везёт в жизни?;') !!}

            </div>
            <hr>
            <h4>Вопрос № 5</h4>
            <p>Уверены ли Вы, что Вы – сын своего отца? </p>
            <div class="form-group">
                <br>
                {!! Form::radio('answer_5',10) !!}
                {!! Form::label('answer_5','Да, я сделал генетическую экспертизу;') !!}

                <br>
                {!! Form::radio('answer_5',15) !!}
                {!! Form::label('answer_5','Нет, не уверен,') !!}

                <br>
                {!! Form::radio('answer_5',0) !!}
                {!! Form::label('answer_5','Уверен и не собираюсь проверять;') !!}

            </div>
            <hr>
            <h4>Вопрос № 6</h4>
            <p>Женщина всегда треплет нервы мужчине потому что - </p>
            <div class="form-group">
                <br>
                {!! Form::radio('answer_6',5) !!}
                {!! Form::label('answer_6','Она бессознательно ненавидит всех мужчин;') !!}

                <br>
                {!! Form::radio('answer_6',10) !!}
                {!! Form::label('answer_6','Она хочет установить полный контроль над его деньгами и ресурсами;') !!}

                <br>
                {!! Form::radio('answer_6',15) !!}
                {!! Form::label('answer_6','У неё низкий тестостерон;') !!}

                <br>
                {!! Form::radio('answer_6',0) !!}
                {!! Form::label('answer_6','Она реализует свою генетическую программу, как самка богомола;') !!}

            </div>
            <hr>
            <h4>Вопрос № 7</h4>
            <p>Когда вам говорят «Этого нельзя сделать», как вы реагируете?</p>
            <div class="form-group">
                <br>
                {!! Form::radio('blue_7',0) !!}
                {!! Form::label('blue_7','Я сдаюсь и опускаю руки;') !!}

                <br>
                {!! Form::radio('blue_7',0) !!}
                {!! Form::label('blue_7','Я начинаю нервничать и выхожу из себя;') !!}

                <br>
                {!! Form::radio('blue_7',1) !!}
                {!! Form::label('blue_7','Я буду пробовать еще раз;') !!}

                <br>
                {!! Form::radio('blue_7',3) !!}
                {!! Form::label('blue_7','Это меня заводит и наполняет энергией. Я не сдамся, пока не добьюсь своей цели;') !!}

            </div>
            <hr>
            <h4>Вопрос № 8</h4>
            <p>Если Вы оказались вовлеченным в спор:</p>
            <div class="form-group">
                <br>
                {!! Form::radio('blue_8',0) !!}
                {!! Form::label('blue_8','Упорствуете в утверждении своей точки зрения до тех пор, пока оппоненты не отступятся;') !!}

                <br>
                {!! Form::radio('blue_8',2) !!}
                {!! Form::label('blue_8','Ищите компромиссное решение;') !!}

                <br>
                {!! Form::radio('blue_8',0) !!}
                {!! Form::label('blue_8','Уступаете перед упорством других;') !!}

            </div>
            <hr>
            <h4>Вопрос № 9</h4>
            <p>Если вам нужно принять важное решение и вы не знаете как поступить, что вы делаете? </p>
            <div class="form-group">
                <br>
                {!! Form::radio('blue_9',0) !!}
                {!! Form::label('blue_9','Лежу на диване и смотрю телевизор чтобы расслабиться;') !!}

                <br>
                {!! Form::radio('blue_9',1) !!}
                {!! Form::label('blue_9','Узнаю мнение друзей и родственников;') !!}

                <br>
                {!! Form::radio('blue_9',1) !!}
                {!! Form::label('blue_9','Делаю логический анализ ситуации и выбираю наиболее разумное решение;') !!}

                <br>
                {!! Form::radio('blue_9',4) !!}
                {!! Form::label('blue_9','Использовав варианты 2 и 3, доверяюсь своей интуиции;') !!}
            </div>
            <hr>
            <h4>Вопрос № 10</h4>
            <p>Если кто-то намеренно портит вашу репутацию, как вы поступите?</p>
            <div class="form-group">
                <br>
                {!! Form::radio('blue_10',0) !!}
                {!! Form::label('blue_10','Переживаю по этому поводу, занимаюсь самоедством, ищу причины, по которым меня не любят;') !!}
                <br>
                {!! Form::radio('blue_10',0) !!}
                {!! Form::label('blue_10','Не обращаю на это внимание и ухожу от конфликта;') !!}
                <br>
                {!! Form::radio('blue_10',4) !!}
                {!! Form::label('blue_10','Спрашиваю обидчика напрямую почему он это сделал;') !!}
                <br>
                {!! Form::radio('blue_10',1) !!}
                {!! Form::label('blue_10','Расплачиваюсь той же монетой, только в десять раз круче чтобы мало не показалось;') !!}
            </div>
            <hr>
            <h4>Вопрос № 11</h4>
            <p>Просыпаясь утром, Вы обычно чувствуете: </p>
            <div class="form-group">
                <br>
                {!! Form::radio('blue_11',1) !!}
                {!! Form::label('blue_11','Бодрость;') !!}
                <br>
                {!! Form::radio('blue_11',0) !!}
                {!! Form::label('blue_11','Зависть к тем, кто еще спит;') !!}
                <br>
                {!! Form::radio('blue_11',2) !!}
                {!! Form::label('blue_11','Готовность работать без особых усилий над собой;') !!}
            </div>
            <hr>
            <h4>Вопрос № 12</h4>
            <p>В случае экономического кризиса:</p>
            <div class="form-group">
                <br>
                {!! Form::radio('blue_12',4) !!}
                {!! Form::label('blue_12','Вы обдумываете какие из этого все-же можно извлечь выгоды;') !!}
                <br>
                {!! Form::radio('blue_12',0) !!}
                {!! Form::label('blue_12','Встревожены возможными социальными последствиями;') !!}
                <br>
                {!! Form::radio('blue_12',1) !!}
                {!! Form::label('blue_12','Отказываетесь от своих планов и ожидаете развития событий;') !!}
            </div>
            <hr>
            <h4>Вопрос № 13</h4>
            <p>Вы могли бы убить человека?</p>
            <div class="form-group">
                <br>
                {!! Form::radio('blue_13',0) !!}
                {!! Form::label('blue_13','Ни в коем случае;') !!}
                <br>
                {!! Form::radio('blue_13',3) !!}
                {!! Form::label('blue_13','Не задумываясь;') !!}
                <br>
                {!! Form::radio('blue_13',2) !!}
                {!! Form::label('blue_13','Защищая себя и близких;') !!}
            </div>
            <hr>
            <h4>Вопрос № 14</h4>
            <p>Есть ли что-то, что человек нравственный не сделает ни при каких обстоятельствах?</p>
            <div class="form-group">
                <br>
                {!! Form::radio('blue_14',0) !!}
                {!! Form::label('blue_14','Да, конечно, вспомните хотя бы заповеди;') !!}

                <br>
                {!! Form::radio('blue_14',1) !!}
                {!! Form::label('blue_14','Обстоятельства бывают разные… ;') !!}

                <br>
                {!! Form::radio('blue_14',3) !!}
                {!! Form::label('blue_14','Общественная мораль - это тоже обстоятельство; ') !!}

            </div>
            <hr>
            <h4>Вопрос № 15</h4>
            <p>Случалось ли Вам сдаваться в шаге от победы?</p>
            <div class="form-group">
                <br>
                {!! Form::radio('blue_15',2) !!}
                {!! Form::label('blue_15','Нет, раз уж поставил цель - нужно бороться до конца;') !!}

                <br>
                {!! Form::radio('blue_15',3) !!}
                {!! Form::label('blue_15','Я не жалею о цели, если сознательно от нее отказался; ') !!}

                <br>
                {!! Form::radio('blue_15',0) !!}
                {!! Form::label('blue_15','Да, но на это всегда были объективные причины;') !!}

            </div>
            <hr>
            <h4>Вопрос № 16</h4>
            <p>Бывает ли, что с Вами поступают несправедливо?</p>
            <div class="form-group">
                <br>
                {!! Form::radio('blue_16',3) !!}
                {!! Form::label('blue_16','Эти люди просто действуют в своих интересах;') !!}

                <br>
                {!! Form::radio('blue_16',0) !!}
                {!! Form::label('blue_16','Мир такой несправедливый;') !!}

                <br>
                {!! Form::radio('blue_16',2) !!}
                {!! Form::label('blue_16','Есть же какие-то неписаные законы;') !!}

            </div>
            <hr>
            <h4>Вопрос № 17</h4>
            <p>Решительный ли Вы человек?</p>
            <div class="form-group">
                <br>
                {!! Form::radio('blue_17',0) !!}
                {!! Form::label('blue_17','Что ни сделай - заденешь чьи-то интересы;') !!}

                <br>
                {!! Form::radio('blue_17',2) !!}
                {!! Form::label('blue_17','Сначала нужно прикинуть, что да как;') !!}

                <br>
                {!! Form::radio('blue_17',3) !!}
                {!! Form::label('blue_17','А чего бояться?') !!}

            </div>
            <hr>
            <h4>Вопрос № 18</h4>
            <p>Ищете ли Вы оправдание своим поступкам?</p>
            <div class="form-group">
                <br>
                {!! Form::radio('blue_18',1) !!}
                {!! Form::label('blue_18','Надо же объяснить людям, что это обстоятельства так сложились;') !!}

                <br>
                {!! Form::radio('blue_18',3 ) !!}
                {!! Form::label('blue_18','А перед кем оправдываться?') !!}

                <br>
                {!! Form::radio('blue_18',2) !!}
                {!! Form::label('blue_18','Иногда я озвучиваю свои мотивы.') !!}

            </div>
            <br>
            <div class="form-group">
                {!! Form::label('email_test','Чтобы ознакомиться с результатами введите Ваш электронный адрес.') !!}
                @if (Auth::check())
                {!! Form::email('email_test',Auth::user()->email,['class'=>'form-control','placeholder'=>'Email']) !!}
                @else
                {!! Form::email('email_test',null,['class'=>'form-control','placeholder'=>'Email']) !!}
                @endif
            </div>
            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-blue">Узнать результат</button>
            </div>
            
            {!! Form::close() !!}
        </div>
    </div>
@endsection