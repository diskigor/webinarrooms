@extends('layouts.front')
@section('content')
    <div id="content">
        <div class="container">
            <header class="panel big-header text-center">{{trans('home.allnews')}}</header>
            <div class="row">
                @if(isset($news))
                @foreach($news as $article)
                    <figure class="col-md-4">
                        <div class="post_news">
                            <a href="{{route('show_news',$article->id)}}">
                                <img src="{{asset($article->image_link)}}" class="image">
                            </a>
                            <figcaption class="panel">
                                <header><strong>{{mb_strimwidth($article->news_trans[0]->title,0,60,'...')}}</strong></header>
                                <p>{{mb_strimwidth($article->news_trans[0]->article,0,100,'...') }}</p>
                                <p class="date"><small>{{trans('news.created_news')}}: {{date('d.m.Y', strtotime($article->created_at))}}</small></p>
                                <div class="text-center">
                                    <a href="{{route('show_news',$article->id)}}" class="btn btn-blue">{{trans('home.more')}}</a>
                                </div>
                            </figcaption>
                        </div>
                    </figure>
                @endforeach
                    @endif
            </div>
        </div>
    </div>
@endsection