@extends('layouts.front')
@section('content')
    <div id="content">
        <div class="container">
            <header class="big-header">{{$news->news_trans[0]->title}}</header>
            <div class="panel">
                <img class="img-responsive" src="{{asset($news->image_link)}}" alt="{{$news->news_trans[0]->slug}}">
                <h4>{{trans('home.describenews')}}</h4>
                <div>{!! $news->news_trans[0]->article !!}</div>
            </div>
        </div>
    </div>
@endsection