@extends('layouts.front')
@section('content')
    <div id="content">
        <div class="container">
            <div class="panel">
                <h3 class="text-center">{{trans('home.accblock')}}</h3>
                @if(isset($message))  <p> {{trans('home.cause')}}Причина : {{$message}}</p>@endif
                <p>{{trans('home.unlock')}} </p>
            </div>

            {{--<div class="row">--}}
                {{--<div class="panel">--}}
                    {{--@if(!isset($confirm_send_message))--}}
                        {{--{!! Form::open(array('route'=>'block_user_message','method'=>'GET')) !!}--}}
                        {{--<div class="form-group">--}}
                            {{--{!! Form::label('first_name','Имя',['class'=>'label-control']) !!}--}}
                            {{--{!! Form::text('first_name',null,['class'=>'form-control','required','placeholder'=>'Введите Ваше имя']) !!}--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--{!! Form::label('last_name','Фамилия',['class'=>'label-control']) !!}--}}
                            {{--{!! Form::text('last_name',null,['class'=>'form-control','required','placeholder'=>'Введите Фамилию']) !!}--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--{!! Form::label('email','Email',['class'=>'label-control']) !!}--}}
                            {{--{!! Form::email('email',null,['class'=>'form-control','required','placeholder'=>'Введите почту с которой регистрировались.']) !!}--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--{!! Form::label('message','Сообщение',['class'=>'label-control']) !!}--}}
                            {{--{!! Form::textarea('message',null,['class'=>'form-control']) !!}--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--{!! Form::submit('Отправить',['class'=>'btn btn-blue']) !!}--}}
                        {{--</div>--}}
                        {{--{!! Form::close() !!}--}}
                    {{--@else--}}
                        {{--<h2 class="text-center">{{$confirm_send_message}}</h2>--}}
                    {{--@endif--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
@endsection