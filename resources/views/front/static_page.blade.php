@extends('layouts.front')
@section('content')
    @if($page->banner_image)
    <section class="top-banner">
        <img src="{{asset($page->banner_image)}}" alt="banner">
        <div class="description">
            <div class="container">
                <h1>{{$page->name}}</h1>
                <?php $title_page=$page->name; ?>
                <p>{{$page->banner_text}}</p>
                <a href="http://lsg.ru/register" type="button" class="btn">
                    {{trans('home.iamleader')}}
                </a>
            </div>
        </div>
    </section>
    @endif
    <div id="content">
        <div class="container">

                {!! $page->content !!}

                    <div class="courses-slider static" id="courses-slider-big">
                        <div class="row">
                        @if(isset($free_course))
                            @foreach($free_course as $cours)
                                <div class="col-md-3">
                                    <a href="{{route('free_materials.show',$cours->course->slug)}}" target="_blank" class="owl-slide">
                                        <img class="image" src="{{asset($cours->course->image)}}" alt="">
                                        <div class="caption">
                                            <header>{{$cours->course->trans_curse[0]->name_trans}}</header>
                                            <div class="description hidden">{!! $cours->course->trans_curse[0]->short_desc_trans !!}</div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        @endif

                </div>
            </div>
        </div>
    </div>
@endsection
@section('front_script')
    <script src="{{asset('js/static_main.js')}}"></script>

@endsection