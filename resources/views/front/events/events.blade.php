@extends('layouts.front')
@section('content')
    <div id="content">
        <div class="container sobitija">
            <header class="panel big-header text-center">Все События и Мероприятия</header>
            <div class="row">
                @if(isset($events))
                    @foreach($events as $event)
                        <figure class="col-md-4">
                            <div class="post_news">
                                <a class="post_news_img" href="{{route('show_event',$event->event_trans()->slug)}}">
                                    <img class="image" src="{{asset($event->image_link)}}">
                                </a>
                                <figcaption class="panel">
                                    <header><strong>{{str_limit($event->event_trans()->name,60)}}</strong></header>
                                    <!--div class="newsdescr">{!! str_limit($event->event_trans()->text,100) !!}</div-->
                                    {{--<p class="date"><small>{{trans('news.created_news')}}: {{date('d.m.Y', strtotime($event->created_at))}}</small></p>--}}
                                    <div class="text-center button_sobitija">
                                        <a href="{{route('show_event',$event->slug)}}" class="btn btn-blue">Подробнее</a>
                                    </div>
                                </figcaption>
                            </div>
                        </figure>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
@endsection