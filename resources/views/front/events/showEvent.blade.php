@extends('layouts.front')
@section('content')
    <div class="container">
        <header class="panel big-header text-center">{{$event->event_trans()->name}}</header>
        <div class="panel">
            <div class="row">
                <div class="col-sm-12">
                    <img class="img-responsive" src="{{asset($event->image_link)}}" alt="{{$event->slug}}">
                </div>
                <!--div class="col-sm-9">
                    <h4>Краткое описание:</h4>
                    <div class="intro">
                        description 
                    </div>
                </div-->
            </div>
            <!--h4>Описание:</h4-->
            <div>{!! $event->event_trans()->text !!}</div>
        </div>
    </div>
@endsection