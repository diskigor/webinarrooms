@extends('layouts.front')
@section('content')
<?php $title_page=trans('home.contactsour'); ?>
    <div id="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="bg_white">
                        <h4>{{trans('home.phoneour')}} </h4>
                        @if(!is_null($contacts))
                        <p><i class="fa fa-phone" aria-hidden="true"></i>      {{$contacts->number_one}}</p>
                        <p><i class="fa fa-phone" aria-hidden="true"></i>      {{$contacts->number_two}}</p>
                            @endif
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="bg_white">
                        <h4>{{trans('home.mailour')}}</h4>
                        @if(!is_null($contacts))
                        <p><i class="fa fa-envelope" aria-hidden="true"></i>     {{$contacts->email_one}}</p>
                        <p><i class="fa fa-envelope" aria-hidden="true"></i>     {{$contacts->email_two}}</p>
                            @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    
                        @if ($page)
                            {!! $page->content !!}
                        @endif
                    
                </div>
            </div>
        </div>
    </div>
@endsection