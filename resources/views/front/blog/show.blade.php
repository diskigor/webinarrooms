{{-- $blog, $linktolist, $linktoshow --}}
@extends('layouts.front')
@section('content')
<link rel="stylesheet" type="text/css" media="all" href="{{asset('comments/css')}}/comments.css" />
    



<div class="container-fluid blog-content">
        <div class="col-md-2 left-blog-content">
            
        </div>
        <div class="col-md-8 blog-one-item">
                <h1>{{$blog->title}}</h1>
                <article class="blogcontent col-md-12">  
                    <img class="image imgblog img-responsive" src="{{ $blog->image }}">
                    <p>{!! $blog->article !!}</p>
                    <hr class="blogsepar">
                    <p class="text-left col-md-4">{{trans('blog.author')}}: {{ $blog->authorFIO }}</p>
                    <p class="col-md-4 text-center fs18">
                    <span id="likes_block" class="bloglikes"> @include('users.blog.likes_block',['blog'=>$blog])</span>
                    <span class="blogcomments far fa-comments" aria-hidden="true">  {{ $blog->comments()->count() }}</i></span>
                    </p>
                    <p class="text-right col-md-4">{{trans('blog.create')}}: {{ $blog->created_at }}</p>
                    <hr class="blogsepar">
                    @include('front.blog.comments_block', ['essence' => $blog])
                <a href="{{ url('blog') }}" class="btn btn-blue">{{trans('blog.backtolist')}}</a>
                

                <a href="{{ $linktoshow }}" class="btn btn-blue pull-right">{{trans('blog.addcomment')}}</a>
                </article>
        </div>
        <div class="col-md-2 right-blog-content">
            @include('layouts.rightSideBarUsers')
        </div>
    </div>




<script>
    $(document).on('click','#i_am_like', function(e){
        e.preventDefault();
        window.location.href = '{{ $linktoshow }}';
        });
</script>
@endsection