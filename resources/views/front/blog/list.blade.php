@extends('layouts.front')
@section('content')
    <div class="container-fluid blog-content">
        <?php $title_page=trans('blog.headertitle'); ?>
        <div class="col-md-2 left-blog-content">
            
        </div>
        <div class="col-md-8 center-blog-content">
                @foreach($blogs as $kk=>$blogs_name)
                <div class="col-md-4">
                    <div class="header_menu col-md-12">{{ $blogs_name->textNameGroup }}</div>
                    @foreach($blogs_name as $article)
                    <div class="row">
                        <div class="col-md-12">
                            <div class="thumbnail">
                                <img src="{{ $article->image }}" alt="...">
                                <span class="dateblog">{{ $article->created_at->format('d.m.Y') }}</span>
                                <div class="caption">
                                    <h3>{{$article->title}}</h3>
                                    @if($article->isNew)
                                        <span class="badge blog-new-icon" title="это новый блог">new</span>
                                    @endif
                                    <p class="blog-description">{{ mb_strimwidth (strip_tags($article->article),0,300,'...') }}</p>
                                    <hr>
                                    <p class="" style="display: flow-root;">
                                    <span class="pull-left">  {{trans('blog.author')}}: {{ $article->authorFIO }}</span> 
                                    <span class="bloglikes pull-right"><i class="fa fa-heart" aria-hidden="true">  {{ $article->likes()->count() }}</i></span>
                                    <span class="blogcomments pull-right"><i class="fa fa-comments-o" aria-hidden="true">  {{ $article->comments()->count() }}</i></span>
                                    </p>
                                    @if(!$article->listTags()->isEmpty())
                                        <hr class="blogsepar">
                                        @foreach($article->listTags() as $tag)
                                            <span class="tags-search" title="тег: {{ $tag }}">{{ $tag }}</span>
                                        @endforeach
                                    @endif
                                    <hr>
                                    <p><a href="{{ $article->linktoroute }}" class="btn btn-blogread" role="button">{{trans('blog.readmore')}}</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <a href="{{ $blogs_name->linktoroute }}" class="btn btn-allblogs">{{ $blogs_name->textNameGroup }}
                        @if($blogs_name->blog_newcounts)
                            <span class="badge" title="новых {{ $blogs_name->blog_newcounts }}">{{ $blogs_name->blog_newcounts }}</span>
                        @endif
                    </a>
                </div>
                @endforeach
            
        </div>
        <div class="col-md-2 right-blog-content">
            @include('layouts.rightSideBarUsers')
        </div>
    </div>
@endsection