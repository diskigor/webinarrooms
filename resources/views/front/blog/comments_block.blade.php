@php

	if($page_comments && $all_comments){
		$comments = $all_comments;

		/*
		 * Группируем комментарии по полю parent_id. При этом данное поле становится ключами массива 
		 * коллекций содержащих модели комментариев
		 */
		$com = $comments->where('status', 1)->groupBy('parent_id');
	} else $com = null;

@endphp


<!--Блок для вывода сообщения про отправку комментария-->
<div class="wrap_result"></div>

<div id="js_block_load_comments_for_this_blog_pagination">
    {{ $page_comments->links() }}
</div>

<div id="comments">
    @if(!$page_comments->count())
        <p>{{trans('blog.addfirstcomment')}}</p>
    @endif
    

    <ol class="commentlist group">
            @if($com)
                @foreach($com as $k => $comments)
                        <!--Выводим только родительские комментарии parent_id = 0-->

                        @if($k)
                                @break
                        @endif

                        @include('comments.comment', ['items' => $comments])

                @endforeach                
            @endif
    </ol>

	
</div>

