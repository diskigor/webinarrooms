@extends('layouts.front')
@section('content')
<div class="container">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="row">
        <br/>
        <a href="{{ url('blog') }}" class="btn btn-blue">ВСЕ БЛОГИ</a>
        <br/>
        @if(isset($tags) && !empty($tags))
        <div class="bg-primary">
            <a class="js_tag {{ empty($tag)?'js_active':'' }} badge">ВСЕ</a>
            @foreach($tags as $onetag)
            <a data-tag="{{ $onetag }}" class="js_tag {{ (isset($tag) && $onetag==$tag)?'js_active':'' }} badge">{{ $onetag }}</a>
            @endforeach 
        </div>
        @endif
        <div class="bg-info">
            
            <a data-sort="sort_created" 
               class="js_sort {{ !request()->has('sort_created')?'js_active':'' }} badge bg-danger">Последние</a>
            <a data-sort="sort_likes" 
               class="js_sort {{ (request()->has('sort_likes') && request()->input('sort_likes')=='desc')?'js_active':'' }} badge bg-danger">Самые популярные</a>
        </div>
    </div>

    <div class="row">
        @foreach($blogs as $article)
        <div class="col-md-12 bg_white">
            <div class="col-md-3">
                <img class="image" src="{{Storage::url($article->image_link)}}">
            </div>
            <div class="col-md-9">
                <h3>{{$article->title}}</h3>
                <hr class="blogsepar">
                <p>{!! str_limit($article->article, 500) !!}</p>
                <hr class="blogsepar">
                <p class="text-left col-md-4 fs18">{{trans('blog.author')}}: {{ $article->mainauthor }}</p>
                <p class="col-md-4 text-center fs18">
                    <span class="bloglikes">{{--trans('blog.alllikes')--}} <i class="fa fa-heart" aria-hidden="true">  {{ $article->likes()->count() }}</i></span>
                    <span class="blogcomments">{{--trans('blog.allcomments')--}} <i class="fa fa-comments-o" aria-hidden="true">  {{ $article->comments()->count() }}</i></span>
                </p>
                <p class="col-md-4 text-right fs18">{{trans('blog.create')}}: {{ $article->created_at }}</p>
                @if(!$article->listTags()->isEmpty())
                <hr class="blogsepar">
                <span>Теги: </span>
                @foreach($article->listTags() as $tag)
                <span class="badge">{{ $tag }}</span>
                @endforeach

                @endif
                <hr class="blogsepar">
                <a href="{{ route('front_blog_gilbo_show',$article->slug) }}" class="btn btn-blue pull-right  fs18">{{trans('blog.readmore')}}</a>
            </div>

        </div>
        @endforeach
    </div>
    {{ $blogs->links() }}
</div>
@endsection
@section('front_script')
<style>
    .js_active {
        background: red;
    }
</style>
<script type="text/javascript" src="{{asset('js/QueryData.js')}}" /></script>
<script type="text/javascript" >
    var lroute = "{{ isset($blogs->linktoroute)?$blogs->linktoroute:'' }}"
    var lroutes = "{{ isset($blogs->linktoroutesearch)?$blogs->linktoroutesearch:'' }}"
</script>
<script>

    $(document).on('click','.js_sort', function(e){
        e.preventDefault();
        $(this).toggleClass('js_active');
        window.location.href = getPrefixSearch();
    });
    
    $(document).on('click','.js_tag', function(e){
        e.preventDefault();
        $('.js_tag').removeClass('js_active');
        $(this).addClass('js_active');
        window.location.href = getPrefixSearch();
    });
    
    
    function getPrefixSearch(){
        var res = {sort_created:'asc'};
        
        $('.js_sort.js_active').each(function(){
            if (!$(this).data('sort')) { return; }
            if ($(this).data('sort') === 'sort_created') {delete res.sort_created;}
            if ($(this).data('sort') === 'sort_likes') {
                res.sort_likes = 'desc';
            }
        });

        $('.js_tag.js_active').each(function(){
            if (!$(this).data('tag')) { return; }
            res.tag = $(this).data('tag');
        });

        var lcounter = 0;

        for (var key in res) {
          lcounter++;
        }
        if (lcounter) {
            return lroutes + '?' + $.param(res);
        } else {
            return lroute;
        }
    }
    


</script>
@endsection