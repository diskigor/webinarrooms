{{-- $blog, $linktolist, $linktoshow --}}
@extends('layouts.front')
@section('content')
<link rel="stylesheet" type="text/css" media="all" href="{{asset('comments/css')}}/comments.css" />
<?php $title_page=$blog->title ; ?>



<div class="container-fluid blog-content">
        
        <div class="col-md-10 blog-one-item">
                
                <article class="blogcontent col-md-12">
                    <!--h1>{{--$blog->title--}}</h1-->
                    <img class="image imgblog img-responsive" src="{{ $blog->image }}">
                    <p>{!! $blog->article !!}</p>
                    <hr class="blogsepar">
                    <p class="text-left col-md-4">{{trans('blog.author')}}: <b>{{ $blog->authorFIO }}</b></p>
                    <p class="col-md-4 text-center fs18">
                    <span id="likes_block" class="bloglikes"> @include('users.blog.likes_block',['blog'=>$blog])</span>
                    <span class="blogcomments far fa-comments" aria-hidden="true">  {{ $blog->comments()->count() }}</i></span>
                    @include('users.blog.view_block',['blog'=>$blog])
                    </p>
                    <p class="text-right col-md-4">{{ $blog->created_at->format('d.m.Y') }}</p>
                    <hr class="blogsepar">
                    <div id='js_block_load_comments_for_this_blog'> </div>
                    {{-- @include('front.blog.comments_block', ['essence' => $blog]) --}}
                <a href="{{ url('blog') }}" class="btn btn-blue">{{trans('blog.backtolist')}}</a>
                

                <a href="{{ $linktoshow }}" class="btn btn-blue pull-right">{{trans('blog.addcomment')}}</a>
                </article>
        </div>
        <div class="col-md-2 right-blog-content">
            @include('layouts.rightSideBarUsers')
        </div>
    </div>




<script>
    $(document).ready(function(){
      $('#js_block_load_comments_for_this_blog').load("{{ route('comments_load',['id_blog'=>$blog->id]) }}");
    });
    
    $(document).on('click','#js_block_load_comments_for_this_blog_pagination a',function(e){
        e.preventDefault();
        var href = this.href;
        if (href) {
            $('#js_block_load_comments_for_this_blog').load(href);
        }
    });
    

    $(document).on('click','.js_comments_collapse', function(e){
        e.preventDefault();
        var block = $(this).parents('li.comment');
        
        var children = block.find('ul.collapse');
        
        if (children.length === 0) {
            children = block.find('ul.children');
            children.addClass('collapse');
            return;
        }
        children.removeClass('collapse');
    });

    
    $(document).on('click','#i_am_like', function(e){
        e.preventDefault();
        window.location.href = '{{ $linktoshow }}';
        });
        
    $(document).on('click','.js_i_am_comment_like', function(e){
        e.preventDefault();
        window.location.href = '{{ $linktoshow }}';
    });
</script>
@endsection