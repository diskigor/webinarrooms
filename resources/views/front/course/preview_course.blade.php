@extends('layouts.front')
@section('content')
    <div id="content">
        <div class="container">
            <header class="big-header">{{$preview_course->name_trans}}</header>
            <div class="panel">
                <img class="img-responsive" src="{{asset($preview_course->curse->image)}}" alt="{{$preview_course->slug}}">
                <h4>Описание :</h4>
                <p>{!! $preview_course->short_desc_trans !!}</p>
                <p>{!! $preview_course->description_trans !!}</p>

                <br/>
                <div class="text-center">
                    <a class="btn btn-blue" title="Поступить в ШЭЛ" href="{{ url('/register') }}">Поступить в ШЭЛ</a>
                    <div style="margin-top: 20px;">
                        пройти курс "{{$preview_course->name_trans}}", цена 100 КЭЛ<br/>
                        *при условии внесения вступительного взноса<br/>
                        **для постоянных членов клуба условиях прохождения указаны в личном кабинете
                    </div>
                </div>
                
            </div>
        </div>
    </div>
@endsection