@extends('layouts.front')
@section('content')
    <div id="content">
        <div class="container">
            <header class="big-header">Все курсы ШЭЛ</header>
            <div class="row">
                @if(isset($courses))
                    @foreach($courses as $course)
                        <figure class="col-sm-4">
                            <div class="post_news">
                                <div class="image">
                                    <img src="{{asset($course->image)}}" alt="">
                                </div>
                                <figcaption>
                                    <h3>{{mb_strimwidth($course->trans_curse[0]->name_trans,0,60,'...')}}</h3>
                                    <p><label>{{trans('news.created_news')}}:</label> {{$course->created_at}}</p>
                                    <p>{!! mb_strimwidth($course->trans_curse[0]->description_trans,0,100,'...') !!}</p>
                                    <a href="{{route('show_preview_course',$course->trans_curse[0]->slug)}}" class="btn btn-blue">Подробнее</a>
                                </figcaption>
                            </div>
                        </figure>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
@endsection