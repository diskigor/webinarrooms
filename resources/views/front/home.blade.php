@extends('layouts.front')
@section('content')
    <!--section class="top-banner">
        <img src="{{asset('images/mane/top-banner.jpg')}}" alt="banner">
        <div class="description">
            <div class="container">
                <h1>{{trans('home.shelzag')}}</h1>
                <p>{!!trans('home.slogan')!!}</p>
                <a href="{{ url('/front_test') }}" type="button" class="btn">
                    {{trans('home.testmain')}}
                </a>
            </div>
        </div>
    </section-->
    
    <section class="hidden-xs" id="firstsection">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators hidden">
                @foreach($projects as $project)
                <li data-target="#carousel-example-generic" data-slide-to="{{$loop->index}}" class="{{$loop->first?'active':''}}"></li>
                @endforeach
            </ol>
            <div class="carousel-inner" role="listbox">
                @foreach($projects as $project)
                    <div class="item {{$loop->first?'active':''}}">
                        <img src="{{ $project->image }}" alt="...">
                        <div class="carousel-caption">
                            <div class="content-block">
                                <div class="header-slide">
                                    <h2>{{$project->name}}</h2>
                                </div>
                                <div class="caption-content">
                                    <div class="caption-content-p">
                                        {!! $project->short_desc !!}
                                    </div>
                                </div>
                                <div class="col-md-12 button-slider">
                                    <div class="button-slider-wrap">
                                        {!!$project->href!!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="fa fa-angle-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="fa fa-angle-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div> 
        <script>
            $('.carousel').carousel({
                interval: {{ env('speed_home_banner', 5000) }}
              });
        </script>
    </section>
    
    <div class="cleafix"></div>
    <section class="" id="secondsection">
        <div class="container">
            <div class="col-md-12">
                <h2>Система Дистанционного Образования</h2>
            </div>
            <div class="row">
                <div class="top-block-fs">
                    <img class="img-responsive  textblock2" src="{{asset('images/banner1block2.png')}}" />
                </div>
            </div>
            <div class="row">
                <div class="lef-block-fs">
                    <img class="img-responsive  textblock2" src="{{asset('images/banner5block2.png')}}" />
                    <img class="img-responsive  textblock2" src="{{asset('images/banner6block2.png')}}" />
                </div>
                <div class="center-block-fs">
                    <img class="img-responsive" src="{{asset('images/infografika.png')}}">
                </div>
                <div class="right-block-fs">
                    <img class="img-responsive textblock2" src="{{asset('images/banner2block2.png')}}" />
                    <img class="img-responsive  textblock2" src="{{asset('images/banner3block2.png')}}" />
                </div>
            </div>
            <div class="row">
                <div class="bottom-block-fs">
                    <img class="img-responsive" src="{{asset('images/banner4block2.png')}}" />
                </div>
            </div>
        </div>
    </section>
    <div class="cleafix"></div>
    <section class="" id="thirdsection">
        <div class="start">
            <div class="container">	
                <div class="start-bottom ">
                    <div class="col-md-2 col-xs-6 start-in">
                        <img src="{{asset('images/s1.png')}}" alt="">
                        <span> 18 лет успешной работы </span>
                    </div>
                    <div class="col-md-2 col-xs-6 start-in">
                        <img src="{{asset('images/s2.png')}}" alt="">
                        <span> 2000+ выпускников  </span>
                    </div>
                    <div class="col-md-2 col-xs-6 start-in">
                        <img src="{{asset('images/s3.png')}}" alt="">
                        <span> 10000+ кандидатов в ШЭЛ  </span>
                    </div>
                    <div class="col-md-2 col-xs-6 start-in">
                        <img src="{{asset('images/s4.png')}}" alt="">
                        <span> 500000+ подписчиков  </span>
                    </div>
                    <div class="col-md-2 col-xs-6 start-in">
                        <img src="{{asset('images/s5.png')}}" alt="">
                        <span> 500+ часов живых семинаров  </span>
                    </div>
                    <div class="col-md-2 col-xs-6 start-in">
                        <img src="{{asset('images/s6.png')}}" alt="">
                        <span> 35+ авторских курсов  </span>
                    </div>
                    
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
    </section>
    <div class="cleafix"></div>
    <section class="" id="fourthsection">
        <div class="about" id="about">
            <div class="container">
                <div class="about-main">
                    <h2>ПРЕИМУЩЕСТВА СИСТЕМЫ ОНЛАЙН ОБУЧЕНИЯ</h2>
                    <p>А также в чем отличие СДО 2.0 от СДО 3.0</p>
                </div>
                <div class="about-bottom">
                <div class="abt">
                    <div class="col-md-3 about-left active">
                        <div class="abt-one">
                            <div class="abt-left">
                                <span class="hnd"> </span>
                            </div>
                            <div class="abt-right">
                                <h4>Система накопления<br> баллов</h4>
                                <p>Бальная система позволяет зарабатывать, накапливать и конвертировать баллы в полезные скидки в процессе обучения</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-md-3 about-left">
                        <div class="abt-one">
                            <div class="abt-left">
                                <span class="d"> </span>
                            </div>
                            <div class="abt-right">
                                <h4>Внутренняя<br> валюта</h4>
                                <p>Собственный биллинг предоставляет безопасность и удобство взаиморасчетов</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-md-3 about-left">
                        <div class="abt-one">
                            <div class="abt-left">
                                <span class="ar"> </span>
                            </div>
                            <div class="abt-right">
                                <h4>Проверочные<br> задания</h4>
                                <p>Разноплановые проверочные задания повышают эффективность освоения материала в разы</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-md-3 about-left">
                        <div class="abt-one">
                            <div class="abt-left">
                                <span class="flg"> </span>
                            </div>
                            <div class="abt-right">
                                <h4>Многоуровневая<br> реферальная система</h4>
                                <p>Привести в проект друга или партнера теперь просто и выгодно всем сторонам</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    </div>
                    <div class="abt">
                    <div class="col-md-3 about-left">
                        <div class="abt-one">
                            <div class="abt-left">
                                <span class="flr"> </span>
                            </div>
                            <div class="abt-right">
                                <h4>Система<br> кураторства</h4>
                                <p>Экспертное оценивание дает существенный прогресс в процессе обучения</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-md-3 about-left">
                        <div class="abt-one">
                            <div class="abt-left">
                                <span class="ard"> </span>
                            </div>
                            <div class="abt-right">
                                <h4>Вебинарная<br> аудитория</h4>
                                <p>Взаимодействие с лекторами без выхода из личного кабинета</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-md-3 about-left">
                        <div class="abt-one">
                            <div class="abt-left">
                                <span class="plan"> </span>
                            </div>
                            <div class="abt-right">
                                <h4>Учебный<br> план</h4>
                                <p>Самостоятельное составление учебного плана позволяет выстраивать приоритеты в направлениях обучения</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-md-3 about-left">
                        <div class="abt-one">
                            <div class="abt-left">
                                <span class="kab"> </span>
                            </div>
                            <div class="abt-right">
                                <h4>Интерактивный<br> личный кабинет</h4>
                                <p>Совокупность систем учета, контроля и взаимодействия, ускоряющих процесс обучения и качество усвоения материала</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-12 rgisterbuttonhome"><a class="btn-blue" href="/register">Регистрация</a></div>
            </div>
        </div>
        
        
    </section>
    <div class="cleafix"></div>
    <section class="" id="fifthsection">
        <div class="container">
            <div class="col-md-12">
                <h2>Программа обучения</h2>
                <h5 style="text-align: center; color: white; font-size: 14px; text-transform: inherit;">Базовые курсы - включены во Вступительный взнос</h5>
            </div>
            <div class="row">
                @foreach($c_blocks as $block)
                    @if($block->order_block<1000)
                        <div class="col-md-4 coursesprview coursesprview{{ $block->id }}" style="min-height: {{ $block->front_hight }}px">
                            <h3><img src="{{asset('images/mane/logo-star.png')}}" style="width: 30px"><span style="padding: 10px">{{ $block->frontName }}</span></h3>
                            @if($block->courses_front_count)
                                <ul>
                                    @foreach($block->coursesFront as $cours)
                                        <li><i class="fa fa-chevron-right" aria-hidden="true"></i><a data-toggle="modal" data-target="#myModalCourses_{{$cours->id}}">{{ $cours->lang?$cours->lang->name_trans:'' }}</a></li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    
                   
                        @if($block->courses_front_count)
                            @foreach($block->coursesFront as $cours)
                                    <div class="modal fade" id="myModalCourses_{{$cours->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">{{ $cours->lang?$cours->lang->name_trans:'' }}</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <img src="{{ $cours->imagex }}"/>

                                                {!! $cours->lang?$cours->lang->front_descr:'' !!}
                                                </div>
                                                <div class="modal-footer" style="text-align: center">
                                                @if (Auth::check())
                                                <a href="/admin/user_curses" class="btn-blue">Перейти в каталог курсов</a>
                                                @else
                                                <a href="/register" class="btn-blue" data-toggle="modal" data-target="#login-modal">Поступить в ШЭЛ</a>
                                                @endif
                                                <button type="button" class="btn-blue-invers" data-dismiss="modal">Закрыть</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            @endforeach
                        @endif
                    @endif
                @endforeach
                
                <div class="col-md-12 home-cours-separ">
                    <h4>Партнерские курсы</h4>
                </div>
                @foreach($c_blocks as $block)
                    @if($block->order_block>=1000)
                        <div class="col-md-4 coursesprview coursesprview{{ $block->id }}" style="min-height: {{ $block->front_hight }}px">
                            <h3><img src="{{asset('images/mane/logo-star.png')}}" style="width: 30px"><span style="padding: 10px">{{ $block->frontName }}</span></h3>
                            @if($block->courses_front_count)
                                <ul>
                                    @foreach($block->coursesFront as $cours)
                                        <li><i class="fa fa-chevron-right" aria-hidden="true"></i><a data-toggle="modal" data-target="#myModalCourses_{{$cours->id}}">{{ $cours->lang?$cours->lang->name_trans:'' }}</a></li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                        @if($block->courses_front_count)
                            @foreach($block->coursesFront as $cours)
                                    <div class="modal fade" id="myModalCourses_{{$cours->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">{{ $cours->lang?$cours->lang->name_trans:'' }}</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <img src="{{ $cours->imagex }}"/>

                                                {!! $cours->lang?$cours->lang->front_descr:'' !!}
                                                </div>
                                                <div class="modal-footer" style="text-align: center">
                                                @if (Auth::check())
                                                <a href="/admin/user_curses" class="btn-courses-home">Перейти в каталог курсов</a>
                                                @else
                                                <a href="#" class="btn-courses-home" data-toggle="modal" data-target="#login-modal">Поступить в ШЭЛ</a>
                                                @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            @endforeach
                        @endif
                    @endif
                @endforeach                
            </div>
        </div>
        
        <div class="cleafix"></div>
    </section>
    <div class="cleafix"></div>
    
    <section class="" id="sixthsection">
        <div class="container">
            <div class="col-md-12">
                <h2>Результаты обучения</h2>
            </div>
            <div class="resultsteach col-md-12">
                <span class="col-md-6 col-xs-12">
                    <i class="far fa-check-square"></i>
                    <p>Повышение личной конкурентоспособности</p>
                </span>
                <span class="col-md-6 col-xs-12">
                    <i class="far fa-check-square"></i>
                    <p>Личностный рост</p>
                </span>
                <span class="col-md-6 col-xs-12">
                    <i class="far fa-check-square"></i>
                    <p>Снятие барьеров</p>
                </span>
                <span class="col-md-6 col-xs-12">
                    <i class="far fa-check-square"></i>
                    <p>Освоение техник саморегуляции</p>
                </span>
                <span class="col-md-6 col-xs-12">
                    <i class="far fa-check-square"></i>
                    <p>Освоение техник мотивирования партнера</p>
                </span>
                <span class="col-md-6 col-xs-12">
                    <i class="far fa-check-square"></i>
                    <p>Овладение технологией власти</p>
                </span>
                <span class="col-md-6 col-xs-12">
                    <i class="far fa-check-square"></i>
                    <p>Понимание социальной реальности</p>
                </span>
                <span class="col-md-6 col-xs-12">
                    <i class="far fa-check-square"></i>
                    <p>Новое понимание природы денег</p>
                </span>
            </div>
        </div>
    </section>
    <div class="cleafix"></div>
    <section class="" id="sevensection">
        <div class="container">
            <div class="col-md-12">
                <h2>Шаги к успеху</h2>
            </div>
            <div class="col-md-12 hidden-xs">
                <div class="mindshel">
                <img src="{{asset('images/minedshal.png')}}" />
                <p>Мышление ШЭЛ-овцев</p>
                </div>
            </div>
            <div class="col-md-12">
            <div class="stepto1">
                <p class="plusstep">
                
                    <span class="plussteptxt" style="padding: 35px 5px;">Важно иметь внутреннюю установку на реальность, ее восприятие. Тогда появляется способность этой реальностью управлять</span>
                <span class="plusstepbg"></span>
                </p>
                <p class="steppositive">АДЕКВАТНОСТЬ <br> ВОСПРИЯТИЯ РЕАЛЬНОСТИ</p>
                <span class="stepborder1"></span>
                <span class="stepborder2"></span>
                <p class="stepnegative">НЕАДЕКВАТНОСТЬ <br>И МИР ИЛЛЮЗИЙ</p>
                <p class="minusstep">
                <span class="minusstepbg"></span>
                <span class="minussteptxt" style="padding: 40px 5px;">Чтобы что-то менять, нужно иметь адекватное представление о предмете, который взялся менять</span>
                </p>
            </div>
            <div class="stepto2">
                <p class="plusstep">
                
                    <span class="plussteptxt" style="padding: 35px 5px;">Не ищите цель жизни. Ищите удовольствие. Делайте то, что вас прикалывает. Не делайте того, что не чувствуете своим. И жизнь наладится</span>
                <span class="plusstepbg"></span>
                </p>
                <p class="steppositive">СВОБОДА <br> И РАСКРЕПОЩЕНИЕ</p>
                <span class="stepborder1"></span>
                <span class="stepborder2"></span>
                <p class="stepnegative">ВНУТРЕННИЕ <br>БАРЬЕРЫ И ТОРМОЗА</p>
                <p class="minusstep">
                <span class="minusstepbg"></span>
                <span class="minussteptxt" style="padding: 20px 5px;">Нельзя достичь свободы при помощи “преодоления”. Она недостижима любыми способами, которые предполагают жесткую встряску психики</span>
                </p>
            </div>
            <div class="stepto3">
                <p class="plusstep">
                
                <span class="plussteptxt">В определенном положении позвоночника происходит активация его иннервации, энергетики, “разгонка”. Это влечет активизацию всего организма в целом, делает его готовым к активной деятельности</span>
                <span class="plusstepbg"></span>
                </p>
                <p class="steppositive">ЗДОРОВЬЕ <br> И ЭНЕРГИЯ</p>
                <span class="stepborder1"></span>
                <span class="stepborder2"></span>
                <p class="stepnegative">ОТСУТСТВИЕ <br>ЖИЗНЕННЫХ СИЛ</p>
                <p class="minusstep">
                <span class="minusstepbg"></span>
                <span class="minussteptxt">Проблема заключается в том, что люди часто не способны расслабляться и вообще управлять процессами в своем организме. Поэтому организм полон напряжений, зажимов, неврозов и блокировок</span>
                </p>
            </div>
            <div class="stepto4">
                <p class="plusstep">
                
                    <span class="plussteptxt" style="padding: 15px 5px;">Интуиция не есть проявление первичной, инстинктивной мотивации. Она настраивается в результате жизненного опыта и правильной работы с коллективным бессознательным</span>
                <span class="plusstepbg"></span>
                </p>
                <p class="steppositive">ДОВЕРИЕ СЕБЕ, <br> УДАЧА, МАГИЯ</p>
                <span class="stepborder1"></span>
                <span class="stepborder2"></span>
                <p class="stepnegative">НЕУДАЧИ <br>И ОШИБКИ</p>
                <p class="minusstep">
                <span class="minusstepbg"></span>
                <span class="minussteptxt" style="padding: 50px 5px;">На самом деле все просто – надо только поменять отношение к жизни</span>
                </p>
            </div>
            <div class="stepto5">
                <p class="plusstep">
                
                    <span class="plussteptxt" style="padding: 55px 5px;">Хочешь быть наверху – строй свою систему и поднимайся вместе с ней</span>
                <span class="plusstepbg"></span>
                </p>
                <p class="steppositive">УСПЕХ И ВЛАСТЬ <br> НАД КОНКУРЕНЦИЕЙ</p>
                <span class="stepborder1"></span>
                <span class="stepborder2"></span>
                <p class="stepnegative">НЕУСПЕХ <br>И НЕРЕАЛИЗОВАННОСТЬ</p>
                <p class="minusstep">
                <span class="minusstepbg"></span>
                <span class="minussteptxt" style="padding: 20px 5px;">В ШЭЛ мы учимся не искать себе хозяина, а самому руководить собой и своей судьбой. В ШЭЛ мы учимся действовать не по алгоритму, а по своей интуиции и произволу</span>
                </p>
            </div>
            </div>
            <div class="col-md-12 hidden-xs">
                <div class="mindall">
                <img src="{{asset('images/minedall.png')}}" />
                <p>Мышление большинства</p>
                </div>
            </div>
        </div>
    </section>
    <div class="cleafix"></div>
    
    <section class="" id="naukasection">
        <div class="container">
            <div class="col-md-12">
                <h2>Рассылка "Наука лидерства"</h2>
            </div>
        </div>
        <div class="container">
            <div class="col-md-12 nauka-main">
                <div class="chat">
                <!-- Nav tabs -->
                    <ul class="chat-left col-md-2" role="tablist">
                        <?php $i = 1;?>
                        @foreach($mediascans as $mediascan)
                            @if($i === 1)
                                <li role="presentation" class="active"><a href="{{'#tab-'.$mediascan->id}}"
                                                                          aria-controls="{{'tab-'.$mediascan->id}}"
                                                                          role="tab"
                                                                          data-toggle="tab">{{$mediascan->mediascan_date}}</a>
                                </li>
                            @else
                                <li role="presentation"><a href="{{'#tab-'.$mediascan->id}}"
                                                           aria-controls="{{'tab-'.$mediascan->id}}" role="tab"
                                                           data-toggle="tab">{{$mediascan->mediascan_date}}</a>
                                </li>
                            @endif
                                <?php $i++ ?>
                        @endforeach

                    </ul>

                    <!-- Tab panes -->
                    <div class="chat-right col-md-10">
                        <div class="tab-content">
                            <?php $n = 1;?>
                            @foreach($mediascans as $mediascan)
                                @if($n === 1)
                                    <div role="tabpanel" class="tab-pane active fade in" id="{{'tab-'.$mediascan->id}}">
                                        <div class="chat-item left">
                                            {!! $mediascan->mediascan_body !!}
                                            <p>
                                            </p>
                                        </div>
                                    </div>
                                @else
                                    <div role="tabpanel" class="tab-pane fade" id="{{'tab-'.$mediascan->id}}">
                                        <div class="chat-item left">
                                            {!! $mediascan->mediascan_body !!}
                                        </div>
                                    </div>
                                @endif
                                <?php $n++ ?>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="text-align:center; margin-top: 10px">
                <a class="btn-blue" href="{{ $all_nauka }}">{{trans('home.allarchive')}}</a>
            </div>
        </div>
    </section>
    <section class="" id="eightsection">
        <div class="container">
            <div class="col-md-7">
                <h2>Как начать обучение?</h2>
                <p>Пройдите тест и на основании его результатов Вы получите рекомендации по индивидуальному плану обучения</p>
                <span>*Среднее время прохождения теста 7 минут</span>
            </div>
            <div class="col-md-4 col-md-offset-1">
                <h4>Тест насколько я успешен</h4>
                <a href="{{ url('/front_test') }}" class="testcarusel">Пройти тест</a>
            </div>
        </div>
    </section>
    <div class="cleafix"></div>
    <section class="" id="ninethsection">
        <div class="container">
            <div class="col-md-12">
                <h2>Блоги</h2>
            </div>
        </div>
            <div class="container-fluid">
                <div class="owl-carousel owl-theme carousel-one">
                    
                    @foreach($blogs as $blog)
                        <div class="homeblogblock">
                            <a href="{{ $blog->linktoshow }}">
                            <div class="imagehomeblog" style="background-image: url({{ $blog->image }})">
                            <span class="datehomeblog">{{ $blog->created_at->format('d.m.Y') }}</span>
                            </div>
                            <div class="contenthomeblog">
                                <h4>{{mb_strimwidth($blog->title,0,40,'...') }}</h4>
                                <p>{{mb_strimwidth(strip_tags($blog->article),0,100,'...')}}</p>
                                <hr>
                            </div>
                            <div class="footerhomeblog">
                                <span class="author">
                                    <div class="col-md-4">
                                    <span class="author-avatar" style="background-image: url({{ $blog->author?$blog->author->avatar:asset('images/default-avatar.png') }})"></span>
                                    </div>
                                    <div class="col-md-8">
                                        <p style="width: 100%;">{{ $blog->authorFIO }}</p>
                                    <span class="view"><i class="fa fa-comments" aria-hidden="true"></i>{{ $blog->count_comments }}</span>
                                    <span class="likes"><i class="fa fa-thumbs-up" aria-hidden="true"></i>{{ $blog->count_likes }}</span>
                                    </div>
                                </span>

                                <span class="readmore pull-right">Читать дальше<i class="fa fa-arrow-circle-right" aria-hidden="true" style="padding-left: 10px;"></i></span>
                            </div>
                            </a>
                        </div>
                    @endforeach
                </div>
                
                <div class="col-md-12 allblogshome">
                    <div class="col-md-12 left-part">
                        <a class="btn-blue" href="{{ $all_blogs }}">Все блоги</a>
                    </div>
                    <div class="col-md-12 right-part">
                        <p>Больше интересных статей, применимых технологий и удивительных фактов еще впереди</p>
                        <form class="form-inline" 
                              action="{{ route('add_new_subscribers') }}"
                              method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="type" value="30">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon hidden-sm hidden-xs"><i class="fa fa-envelope"></i></div>
                                    <input type="text" class="form-control" name="name" placeholder="Введите имя">
                                </div>
                                <input type="text" class="form-control" name="email" placeholder="Введите e-mail">
                                <button type="submit" class="">Подписаться</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        
    </section>
    <div class="cleafix"></div>
    <section class="" id="tensection">
        <div class="container">
            <div class="col-md-12">
                <h2>Об основателе ШЭЛ</h2>
                <hr>
            </div>
            <div class="col-md-4">
                <img src="{{asset('images/gilbomain.jpg')}}" class="img-responsive" />
            </div>
            <div class="col-md-8">
                <!--p>
                    Е.В.Гильбо - Известный российский психолог, социолог, экономист. 
                    Доктор экономики honoris causa Дипломатической академии мира при ЮНЕСКО. 
                    Президент Международной Академии Гуманитарных Технологий (Вашингтон). 
                    Сопредседатель русско-германского философского сообщества. Руководитель консалтингового центра “Модернизация” (Москва). 
                    Вице-президент Международной интернатуры практической психологии (Берлин). Вице-президент СПб Психоаналитического Общества (Петербург). 
                    Руководитель системы дистанционного образования “Школа Эффективных Лидеров”. Президент информационого холдинга Liberty Space Group, LSG (Франкфурт/М). 
                    Автор уникальных методик развития лидерского потенциала, инструментария личной власти и личных конкурентных преимуществ. 
                    Автор уникальных методик тренингов для системы дистанционного образования.
                </p>
                <p>
                    Ведет тренинговую деятельность с 1984 года. С 1988 года руководил разработкой ряда АСУ для предприятий Ленинграда. 
                    С 1991 года - вице-президент Ленинградского психоаналитического общества. 
                    С 1992 года - руководитель группы экспертов Высшего Экономического Совета РФ, советник вице-президента РФ. 
                    С 1994 года занимается экономической публицистикой и консалтингом в сфере финансов, организационного развития, высшей социологии, антикризисного управления. 
                    С 2000 года руководит международным образовательным проектом "Школа эффективных лидеров". 
                    С 2004 года - Президент Международной Академии Гуманитарных Технологий. С 2006 года - сопредседатель Русско-Германского Философского Сообщества.
                </p-->
                <p>
                    <strong> Е.В.Гильбо - Известный российский психолог, социолог, экономист</strong>
                    <ul>
                        <span class="far fa-gem"></span><li>Доктор экономики honoris causa Дипломатической академии мира при ЮНЕСКО</li>
                        <span class="far fa-gem"></span><li>Президент Международной Академии Гуманитарных Технологий (Вашингтон)</li> 
                        <span class="far fa-gem"></span><li>Сопредседатель русско-германского философского сообщества</li>
                        <span class="far fa-gem"></span><li>Руководитель консалтингового центра “Модернизация” (Москва)</li>
                        <span class="far fa-gem"></span><li>Вице-президент Международной интернатуры практической психологии (Берлин)</li>
                        <span class="far fa-gem"></span><li>Вице-президент СПб Психоаналитического Общества (Петербург)</li>
                        <span class="far fa-gem"></span><li>Руководитель системы дистанционного образования “Школа Эффективных Лидеров”</li>
                        <span class="far fa-gem"></span><li>Президент информационого холдинга Liberty Space Group, LSG (Франкфурт/М)</li>
                    </ul>
                </p>
                <p>
                    Автор уникальных методик развития лидерского потенциала, инструментария личной власти и личных конкурентных преимуществ. 
                    Автор уникальных методик тренингов для системы дистанционного образования.
                </p>
                <p>
                    <strong>Деятельность</strong>
                    <ul>
                        <span class="fa fa-calendar-check-o"></span><li>С 1984 года ведет тренинговую деятельность </li>
                        <span class="fa fa-calendar-check-o"></span><li>С 1988 года руководил разработкой ряда АСУ для предприятий Ленинграда</li>
                        <span class="fa fa-calendar-check-o"></span><li>С 1991 года - вице-президент Ленинградского психоаналитического общества</li>
                        <span class="fa fa-calendar-check-o"></span><li>С 1992 года - руководитель группы экспертов Высшего Экономического Совета РФ, советник вице-президента РФ</li>
                        <span class="fa fa-calendar-check-o"></span><li>С 1994 года занимается экономической публицистикой и консалтингом в сфере финансов, организационного развития, высшей социологии, антикризисного управления</li>
                        <span class="fa fa-calendar-check-o"></span><li>С 2000 года руководит международным образовательным проектом "Школа эффективных лидеров"</li>
                        <span class="fa fa-calendar-check-o"></span><li>С 2004 года - Президент Международной Академии Гуманитарных Технологий</li>
                        <span class="fa fa-calendar-check-o"></span><li>С 2006 года - сопредседатель Русско-Германского Философского Сообщества</li>
                    </ul>
                </p>
            </div>
        </div>
        <hr>
        <div class="container-fluid" id="videblock">
            <div class="owl-carousel carousel-two owl-theme">
                @foreach($vidos as $vido)
                <div class="video-home-main">
                    <div class="video-home-block">
                        <!-- Video -->
                        <video id="video_{{ $vido->id }}" width="300" height="169" poster="{{asset('images/home-video-lable.jpg')}}">
                            <source src="{!! $vido->video !!}" type="video/webm">
                        </video>

                        <!-- Video Controls -->
                        <div id="video-controls">
                            <div class="col-md-5 col-sm-5 col-xs-5">
                                <button type="button" id="play-pause_{{ $vido->id }}" class="play"><i class="far fa-play-circle"></i></button>
                                <input type="range" id="seek-bar_{{ $vido->id }}" value="0">
                            </div>
                            <div class="col-md-5 col-sm-5 col-xs-5">
                                <button type="button" id="mute_{{ $vido->id }}"><i class="fas fa-volume-up"></i></button>
                            <input type="range" id="volume-bar_{{ $vido->id }}" min="0" max="1" step="0.1" value="1">
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-2">
                                <button type="button" id="full-screen_{{ $vido->id }}"><i class="fas fa-arrows-alt"></i></button>
                            </div>
                        </div>
                    </div>
                    <h4>{{mb_strimwidth($vido->short_desc,0,60,'...')}}</h4>
                    <script>
                       
         // Video
	var video_{{ $vido->id }} = document.getElementById("video_{{ $vido->id }}");

	// Buttons
	var playButton_{{ $vido->id }} = document.getElementById("play-pause_{{ $vido->id }}");
	var muteButton_{{ $vido->id }} = document.getElementById("mute_{{ $vido->id }}");
	var fullScreenButton_{{ $vido->id }} = document.getElementById("full-screen_{{ $vido->id }}");

	// Sliders
	var seekBar_{{ $vido->id }} = document.getElementById("seek-bar_{{ $vido->id }}");
	var volumeBar_{{ $vido->id }} = document.getElementById("volume-bar_{{ $vido->id }}");


	// Event listener for the play/pause button
	playButton_{{ $vido->id }}.addEventListener("click", function() {
		if (video_{{ $vido->id }}.paused == true) {
			// Play the video
			video_{{ $vido->id }}.play();

			// Update the button text to 'Pause'
			playButton_{{ $vido->id }}.innerHTML = '<i class="far fa-pause-circle"></i>';
		} else {
			// Pause the video
			video_{{ $vido->id }}.pause();

			// Update the button text to 'Play'
			playButton_{{ $vido->id }}.innerHTML = '<i class="far fa-play-circle"></i>';
		}
	});


	// Event listener for the mute button
	muteButton_{{ $vido->id }}.addEventListener("click", function() {
		if (video_{{ $vido->id }}.muted == false) {
			// Mute the video
			video_{{ $vido->id }}.muted = true;

			// Update the button text
			muteButton_{{ $vido->id }}.innerHTML = '<i class="fas fa-volume-up"></i>';
		} else {
			// Unmute the video
			video_{{ $vido->id }}.muted = false;

			// Update the button text
			muteButton_{{ $vido->id }}.innerHTML = '<i class="fas fa-volume-off"></i>';
		}
	});


	// Event listener for the full-screen button
	fullScreenButton_{{ $vido->id }}.addEventListener("click", function() {
		if (video_{{ $vido->id }}.requestFullscreen) {
			video_{{ $vido->id }}.requestFullscreen();
		} else if (video_{{ $vido->id }}.mozRequestFullScreen) {
			video_{{ $vido->id }}.mozRequestFullScreen(); // Firefox
		} else if (video_{{ $vido->id }}.webkitRequestFullscreen) {
			video_{{ $vido->id }}.webkitRequestFullscreen(); // Chrome and Safari
		}
	});


	// Event listener for the seek bar
	seekBar_{{ $vido->id }}.addEventListener("change", function() {
		// Calculate the new time
		var time = video_{{ $vido->id }}.duration * (seekBar_{{ $vido->id }}.value / 100);

		// Update the video time
		video_{{ $vido->id }}.currentTime = time;
	});

	
	// Update the seek bar as the video plays
	video_{{ $vido->id }}.addEventListener("timeupdate", function() {
		// Calculate the slider value
		var value = (100 / video_{{ $vido->id }}.duration) * video_{{ $vido->id }}.currentTime;

		// Update the slider value
		seekBar_{{ $vido->id }}.value = value;
	});

	// Pause the video when the seek handle is being dragged
	seekBar_{{ $vido->id }}.addEventListener("mousedown", function() {
		video_{{ $vido->id }}.pause();
	});

	// Play the video when the seek handle is dropped
	seekBar_{{ $vido->id }}.addEventListener("mouseup", function() {
		video_{{ $vido->id }}.play();
	});

	// Event listener for the volume bar
	volumeBar_{{ $vido->id }}.addEventListener("change", function() {
		// Update the video volume
		video_{{ $vido->id }}.volume = volumeBar_{{ $vido->id }}.value;
	});
        </script>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <div class="cleafix"></div>
    <script>
        $(document).ready(function(){
            $('.carousel-one').owlCarousel({
                nav:true,
                loop:true,
                items:6,
                margin:10,
                autoplay:false,
                navText:['<i class="fas fa-angle-left"></i>','<i class="fas fa-angle-right"></i>'],
                responsive: {
                    0: {
                            items:1
                    },
                    800: {
                            items: 2
                    },
                    1024: {
                            items: 4
                    },
                    1360: {
                            items: 4
                    },
                    1920: {
                            items: 6
                    },
                    2400: {
                            items: 8
                    },
                    4000: {
                            items: 10
                    }
            }});
        $('.carousel-two').owlCarousel({
                nav:true,
                loop:false,
                items:6,
                margin:10,
                autoplay:false,
                navText:['<i class="fas fa-angle-left"></i>','<i class="fas fa-angle-right"></i>'],
                responsive: {
                    0: {
                            items:1
                    },
                    800: {
                            items: 2
                    },
                    1024: {
                            items: 3
                    },
                    1360: {
                            items: 4
                    },
                    1900: {
                            items: 6
                    },
                    2400: {
                            items: 8
                    },
                    4000: {
                            items: 10
                    }
            }});
        });
    </script>
@endsection