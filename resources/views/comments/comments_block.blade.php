@php

	if($page_comments && $all_comments){
		$comments = $all_comments;

		/*
		 * Группируем комментарии по полю parent_id. При этом данное поле становится ключами массива 
		 * коллекций содержащих модели комментариев
		 */
		$com = $comments->where('status', 1)->groupBy('parent_id');
	} else $com = null;

@endphp


<!--Блок для вывода сообщения про отправку комментария-->
<div class="wrap_result"></div>



<div id="comments">
    @if(!$page_comments->count())
        <p>{{trans('blog.addfirstcomment')}}</p>
    @endif
    

    <ol class="commentlist group">
            @if($com)
                @foreach($com as $k => $comments)
                        <!--Выводим только родительские комментарии parent_id = 0-->

                        @if($k)
                                @break
                        @endif

                        @include('comments.comment', ['items' => $comments])

                @endforeach                
            @endif
    </ol>

<div id="js_block_load_comments_for_this_blog_pagination" class="col-md-12 text-center">
    {{ $page_comments->links() }}
</div>
    
    <div id="respond">
            <h3 id="reply-title"> {{trans('blog.addcomment')}}</h3><p><a rel="nofollow" id="cancel-comment-reply-link" href="#respond" style="display:none; float: right;">{{trans('blog.cancelcomment')}}</a></p>
            <!--параметр action используется ajax-->
            <form action="{{ route('comment')}}" method="post" id="commentform">
                    <p class="comment-form-comment"><label for="comment">{{--trans('blog.yourcomment')--}}</label>
                        <textarea class="form-control" id="comment" name="text" cols="45" rows="8" ></textarea>
                    </p>

                    <!--Данные поля так же нужны для работы JS - вставки формы сразу за комментарием на который нужно ответить--> 
                    <input type="hidden" id="comment_blog_ID" name="comment_blog_ID" value="{{ $page_comments->id_blog}}">
                    <input type="hidden" id="comment_parent" name="comment_parent" value="">

                    {{ csrf_field()}}

                    <div class="clear"></div>
                    <p class="form-submit">
                        <input class="btn btn-blue pull-right" name="submit" type="submit" id="submit" value="{{trans('blog.sendcomment')}}" />
                    </p>
            </form>
    </div>
	
</div>
