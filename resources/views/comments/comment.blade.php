@foreach($items as $item)

        <li id="li-comment-{{$item->id}}" class="comment">
	<div id="comment-{{$item->id}}" class="comment-container">
            <div class="commentblock">
                <div class="col-md-1">
                    <img alt="" src="{{ $item->user->avatar }}" class="avatar img-responsive" />
                </div>
                <div class="commentheader col-md-11">
                    <div class="info-user-comment col-md-6">
                        <span class="">{{$item->user->name}}</span>
                        <span class="">{{ is_object($item->created_at) ? $item->created_at->format('d.m.Y в H:i') : ''}}</span>
                    </div>
                    <div class="sub-comment-open  col-md-6" style="text-align: right;">
                        
                        @if($item->IsTop && $item->IsSetChildren)
                        <span class="js_comments_collapse">Смотреть все ответы <span class="far fa-comments" aria-hidden="true"></span></span>
                        @endif
                    </div>
                    <div class="col-md-12">
                        <span class="commentbody">
                            <p class="comment-content">{{ $item->text }}</p>
                            
                        </span>
                    </div>
                    <div class="col-md-6">
                        @if(Auth::check())
                            <p class="replycomment">
                                <a class="comment-reply-link" href="#respond" onclick="return addComment.moveForm(&quot;comment-{{$item->id}}&quot;, &quot;{{$item->id}}&quot;, &quot;respond&quot;, &quot;{{$item->article_id}}&quot;)">{{trans('blog.reply')}}</a>
                            </p>
                        @endif
                    </div>
                    <div class="col-md-6" style="text-align: right;">
                        @include('users.blog.likes_block_comment', ['comment' => $item])
                    </div>
                    
                </div>
                
            </div>
	</div>
	@if(isset($com[$item->id]))
	<ul class="children {{ $item->IsSetChildren?'collapse':'' }}">
		@include('comments.comment', ['items' => $com[$item->id]])
	</ul>
	@endif
    </li>

@endforeach
