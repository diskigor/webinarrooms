<li id="li-comment-{{$data['id']}}" class="comment">
    <div id="comment-{{$data['id']}}" class="comment-container new_comment">
        <div class="commentblock">
            <div class="col-md-1">
                <img alt="" src="{{ $data['avatar'] }}" class="avatar img-responsive" />
            </div>
            <div class="commentheader col-md-11">
                <div class="info-user-comment col-md-6">
                    <span class="">{{$data['name']}}</span>
                </div>
                <div class="info-user-comment col-md-6" style="text-align: right;">
                    <span class="">{{trans('blog.lasttimecomment')}}</span>
                </div>
                <div class="col-md-12">
                    <span class="commentbody">
                        <p class="comment-content">{{ $data['text'] }}</p>
                    </span>
                </div>
            </div>
        </div>
    </div>
</li>
