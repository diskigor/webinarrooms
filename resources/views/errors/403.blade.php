<table>
    <tr>
        <td>
            <h1>403</h1>
            <div>Ошибка: хватает прав доступа</div>
            <div><a href="/">Вернуться на главную</a></div>
        </td>
    </tr>

</table>
<style>
    body, html {
        height: 100%;
    }
    table {
        width: 100%; height: 100%; font-family: Arial, sans-serif; color:#336899; font-size: 20px;
    }
    td {
        vertical-align: middle; text-align: center
    }
    h1 {
        font-size: 170px;
        margin: 0;
        line-height: 1;
    }
    a {
        background: #4876a3;
        color: #fff;
        display: inline-block;
        vertical-align: middle;
        padding: 12px 30px;
        font-weight: 400;
        /* text-transform: uppercase; */
        text-decoration: none !important;
        text-align: center;
        font-size: 14px;
        line-height: 16px;
        letter-spacing: .05em;
        border-radius: 333px;
        box-shadow: 0 4px 8px rgba(0, 0, 0, 0.3), inset 0 10px 20px rgba(255,255,255,.2), inset 0 -10px 20px rgba(0,0,0,.1);
        text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.4);
        border: none;
        white-space: normal;
        transition: .3s;
        -webkit-transition: .3s;
        margin: 20px auto;
    }
    a:hover {
        background: #397cbd;
        color: #fff;
        box-shadow: 0 6px 12px rgba(0, 0, 0, 0.2), inset 0 10px 20px rgba(255,255,255,.2), inset 0 -10px 20px rgba(0,0,0,.1);
    }
</style>