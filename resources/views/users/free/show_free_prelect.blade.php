{{--  $free_lecture   --}}
@extends('layouts.admin')
@section('content')
    <div class="container">
        @if($free_lecture->course->trans_curse[0])
            <h1 class="text-center">{{$free_lecture->course->trans_curse[0]->name_trans}}</h1><h1 class="text-center">{{$free_lecture->sdoLectureTrans()->name_trans}}</h1>
        @endif
        <div style="width: 100%;     height: 200px;
                background-position: center; background-size: cover; background-image: url('{{asset($free_lecture->course->image)}}'); box-shadow: 0 4px 8px rgba(0,0,0,.1);"></div>
        <div class="panel">
            <h3>Краткое описание лекции</h3>
            <p> {!! $free_lecture->sdoLectureTrans()->short_desc !!}</p>
        </div>
    </div>
    <div class="container">
        <div class="panel">
            <!-- Modal Lecture -->
            <!-- Button trigger modal -->
            <div class="text-center">
                <button type="button" class="btn btn-primary btn-lg" data-toggle="modal"
                        data-target="{{'#s'.$free_lecture->id}}">
                    Содержание лекции
                </button>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="{{'s'.$free_lecture->id}}" tabindex="-1" role="dialog"
                 aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title"
                                id="myModalLabel">{{$free_lecture->sdoLectureTrans()->name_trans}}</h4>
                        </div>
                        <div class="modal-body">
                            {!! $free_lecture->sdoLectureTrans()->content_trans !!}
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <!--  -->
        </div>
        <!-- Вывод пополнительного материала  -->
        @if($free_lecture->materials->count() != 0)
            <div class="panel">
                <h3 class="text-center">Дополнительные материалы</h3>

            @foreach($free_lecture->materials as $material)
                @if($material->material->type === "audio")
                    <!-- Вывод дополнительного материала аудио -->
                        <div class="text-center">
                            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal"
                                    data-target="{{'#my_dopm_'.$material->material->id}}">
                                Аудио : {{$material->material->name}}
                            </button>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="{{'my_dopm_'.$material->material->id}}" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title"
                                            id="myModalLabel"> {{$material->material->name}}</h4>
                                    </div>
                                    <div class="modal-body">
                                        <audio controls>
                                            <source src="{{ getUrlMaterialId($material->material->id) }}"
                                                    type="audio/mp3">
                                        </audio>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                @elseif($material->material->type ==="text")
                    <!-- Вывод дополнительного материала текстового формата -->
                        <!-- Button trigger modal -->
                        <div class="text-center">
                            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal"
                                    data-target="{{'#t_dopm_'.$material->material->id}}">
                                {{$material->material->name}}
                            </button>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="t_dopm_{{$material->material->id}}" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel"> {{$material->material->name}}</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>{!! $material->material->content !!}</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                @elseif($material->material->type ==="pdf")
                    <!-- Вывод дополнительного материала pdf формата -->
                    {{--<h3>{{$material->material->name}}</h3>--}}
                    {{--<a href="{{route('getFile',$material->material->id)}}" target="_blank">Открыть PDF</a>--}}

                    <!-- Button trigger modal -->
                        <div class="text-center">
                            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal"
                                    data-target="{{'#pdf_dopm_'.$material->material->id}}">
                                {{$material->material->name}}
                            </button>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="pdf_dopm_{{$material->material->id}}" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">{{$material->material->name}}</h4>
                                    </div>
                                    <div class="modal-body">
                                        <a href="{{ getUrlMaterialId($material->material->id) }}" target="_blank">Открыть
                                            PDF</a>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                @else
                    <!-- Вывод дополнительного материала видео -->
                        <div class="text-center">
                            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal"
                                    data-target="{{'#my_video_dopm_'.$material->material->name}}">
                                Видео : {{$material->material->name}}
                            </button>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="{{'my_video_dopm_'.$material->material->name}}" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title"
                                            id="myModalLabel"> {{$material->material->name}}</h4>
                                    </div>
                                    <div class="modal-body">
                                        <video controls controlsList="nodownload" width="560" height="190">
                                            <source src="{{ getUrlMaterialId($material->material->id) }}">
                                        </video>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach

            </div>
        @endif
    <!-- Конец вывода дополнительного материала -->
        @if($free_lecture->text_tasks->count() != 0 )
        <!-- Вывод  -->
            <div class="panel">
                @foreach($free_lecture->text_tasks as $text_task)
                    <div class="text-center">
                        <button type="button" class="btn btn-primary btn-lg" data-toggle="modal"
                                data-target="{{'#'.$text_task->id}}">
                            {{$text_task->textTasksTrans()->name_trans}}
                        </button>
                    </div>
                    <div class="modal fade" id="{{$text_task->id}}" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title"
                                        id="myModalLabel">{{$text_task->textTasksTrans()->name_trans}}</h4>
                                </div>
                                <div class="modal-body">
                                    <p>{!! $text_task->textTasksTrans()->content_trans !!}</p>
                                    @if(is_null($text_task->answerUserText))
                                        {!! Form::open(array('route'=>['answer_text_tasks',$text_task->id],'method'=>'POST')) !!}
                                        {!! Form::label('answer_text_tasks','Ответ :',array_merge(['class'=>'control-label'])) !!}
                                        {!! Form::textarea('answer_text_tasks',null,array_merge(['class'=>'form-control'])) !!}
                                        {!! Form::submit('Ответить',array_merge(['class'=>'btn btn-success'])) !!}
                                        {!! Form::close() !!}
                                    @else
                                        @if($text_task->answerUserText->status === 0)
                                            <div class="panel">
                                                <p>Ваш ответ на проверке.Как только его проверят, Вам будет доступна
                                                    следующая
                                                    лекция.</p>
                                            </div>
                                        @elseif($text_task->answerUserText->status === 2)
                                            <div class="panel">
                                                <h3>Ваш ответ проверин и не принят,проверьте свой ответ и отредактируйте
                                                    его.</h3>
                                            </div>
                                            @if(!is_null($text_task->answerUserText->check_user))
                                                <h3>Проверил :</h3><p>{{$text_task->answerUserText->check_user}}</p>
                                            @endif
                                            {!! Form::open(array('route'=>['edit_answer',$text_task->answerUserText->id],'method'=>'POST','role'=>'form')) !!}
                                            <div class="form-group">
                                                {!! Form::label('edit_answer_text_tasks','Форма редактирования ответа',array_merge(['class'=>'control-label'])) !!}
                                                {!! Form::textarea('edit_answer_text_tasks',$text_task->answerUserText->answer,array_merge(['class'=>'form-control'])) !!}
                                            </div>
                                            {!! Form::submit('Сохранить изменения',array_merge(['class'=>'btn btn-success'])) !!}
                                            {!! Form::close() !!}
                                        @else
                                            <div class="panel">
                                                <h3>Ваш ответ: </h3>
                                                <p>{!! $text_task->answerUserText->answer !!}</p>
                                                @if(!is_null($text_task->answerUserText->price))
                                                    <h3>Вы получили :</h3><p>{{$text_task->answerUserText->price}}</p>
                                                @endif
                                                @if(!is_null($text_task->answerUserText->check_user))
                                                    <h3>Проверил :</h3>
                                                    <p>{{$text_task->answerUserText->checkUser->name}} -
                                                        "{{$text_task->answerUserText->checkUser->role->name}}"</p>
                                                @endif
                                            </div>
                                        @endif
                                    @endif
                                    
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                                </div>
                            </div>
                        </div>
                    </div>@endforeach
            </div>
        @endif
    <!--  вывод тестовых заданий -->
        @foreach($free_lecture->test_tasks as $test)
            @if($test->typeTest->name === 'boolean')
                <div class="panel">
                    <div class="text-center">
                        <button type="button" class="btn btn-primary btn-lg" data-toggle="modal"
                                data-target="{{'#m'.$test->id}}">
                            Тест - {{$test->name}}
                        </button>
                    </div>
                </div>
                <div class="modal fade" id="{{'m'.$test->id}}" tabindex="-1" role="dialog"
                     aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel"> Тест - {{$test->name}}</h4>
                            </div>
                            <div class="modal-body">
                                <div class="panel">
                                    <h3>Краткое описание теста: </h3>
                                    {!! $test->description !!}
                                    <hr>
                                    @if(isset($test->userTestAnswer) and $test->userTestAnswer->status === 1 )
                                        <h3 class="text-center">Вы успешно прошли данный тест</h3>
                                    @else
                                        {!! Form::open(array('route'=>['answer_test_task',$test->id],'method'=>'POST')) !!}
                                        <?php $i = 1;?>
                                        @foreach($test->testQuestions as $question)
                                            <h4>Вопрос №{{$i++}}</h4>
                                            <p>{!! $question->question !!}</p>
                                            <div class="form-group">
                                                @foreach($question->testAnswerQuestion as $answer)
                                                    {!! Form::label('answer',$answer->answer) !!}
                                                    {!! Form::radio('answer['.$question->id.']', $answer->answer ) !!}
                                                @endforeach
                                            </div>
                                        @endforeach
                                        <button type="submit" class="btn btn-blue">Отправить</button>
                                        {!! Form::close() !!}
                                    @endif
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            @if($test->typeTest->name === 'group')
                <div class="panel">
                    <div class="text-center">
                        <button type="button" class="btn btn-primary btn-lg" data-toggle="modal"
                                data-target="{{'#my'.$test->id}}">
                            {{$test->name}}
                        </button>
                    </div>
                </div>
                <div class="modal fade" id="{{'my'.$test->id}}" tabindex="-1" role="dialog"
                     aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                <h3 class="text-center">{{$test->name}}</h3>
                                <div class="panel">
                                    <h3>Краткое описание теста: </h3>
                                    {!! $test->description !!}
                                    <hr>
                                    @if(isset($test->userTestAnswer) and $test->userTestAnswer->status === 1 )
                                        <h3 class="text-center">Вы успешно прошли данный тест</h3>
                                        <span>Ответ:</span>
                                        <div style="color:#FF0000">{!!  $test->userTestAnswer->descr_answer !!}</div>
                                    @else
                                        <form action="{{route('test_group_answer',$test->id)}}" method="POST">
                                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                                            @foreach($test->questions_group as $question)
                                                <div class="row">
                                                    @foreach($question as $quest)
                                                        <div class="col-md-{{ceil(12/count($question))}}">
                                                            {!! $quest->question  !!}
                                                            @foreach($quest->testAnswerQuestion as $answer)
                                                                <div class="row">
                                                                    <div class="col-xs-1">
                                                                        <strong>{{$answer->number}}) </strong>
                                                                    </div>
                                                                    <div class="col-xs-10">
                                                                        {{$answer->answer}}
                                                                    </div>
                                                                    <div class="col-xs-1">
                                                                        <input type="checkbox"
                                                                               name="group_answer[{{$quest->group_number}}][{{$quest->number}}][{{$answer->number}}]">
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    @endforeach
                                                </div>
                                                <hr>
                                            @endforeach
                                            <button type="submit" class="btn btn-blue">Отправит</button>
                                        </form>
                                    @endif
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            @if($test->typeTest->name === 'concatenation')
                <div class="panel">
                    <div class="text-center">
                        <button type="button" class="btn btn-primary btn-lg" data-toggle="modal"
                                data-target="{{'#myM'.$test->id}}">
                            {{$test->name}}
                        </button>
                    </div>
                </div>
                <div class="modal fade" id="{{'myM'.$test->id}}" tabindex="-1" role="dialog"
                     aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                <h3>Краткое описание теста: </h3>
                                {!! $test->description !!}
                                <hr>
                                @if(isset($test->userTestAnswer) and $test->userTestAnswer->status === 1 )
                                    <header>Вы успешно прошли данный тест</header>

                                    <div class="panel">
                                        <span>Результат :</span>{!!  $test->userTestAnswer->descr_answer !!}</div>
                                @else
                                    {!! Form::open(array('route'=>['test_concat_answer',$test->id],'method'=>'POST')) !!}
                                    <?php $i = 1;?>
                                    @foreach($test->testQuestions as $question)
                                        <div class="col-md-{{ceil(12/count($question))}}">
                                            <h4>Вопрос №{{$i++}}</h4>
                                            <p>{!! $question->question !!}</p>
                                            <div class="form-group">
                                                @foreach($question->testAnswerQuestion as $answer)
                                                    <div class="row">
                                                        {!! Form::label('answer',$answer->answer) !!}
                                                        {!! Form::radio('answer['.$question->group_number.']['.$answer->number.']', $answer->answer ) !!}
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    @endforeach
                                    <button type="submit" class="btn btn-blue">Отправить</button>
                                    {!! Form::close() !!}
                                @endif
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
        <div class="panel">
            <div class="text-center"><a href="{{redirect()->back()->getTargetUrl()}}"
                                        class="btn btn-success">Назад</a>
            </div>
        </div>
    </div>
@endsection