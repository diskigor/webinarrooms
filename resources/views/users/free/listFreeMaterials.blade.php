{{--  $free_courses   --}}
@extends('layouts.admin')
@section('content')
<?php $title_page='Открытая школа'; ?>
    <div class="col-md-12 courses-block">
        <!--p class="podskazki">В этом разделе представлен ряд курсов, техник и лекций, доступных всем членам Клуба в открытом виде – КЭЛы за них не взымаются. 
            Представленные здесь материалы имеют практическую ценность и будут полезны Вам для приобретения правильных установок на жизнь.
        </p  -->
        <div class="row">
            @include('users._partials.education.edu-upmenu',['active_menu'=>'free_materials'])
            <div class="col-md-12">
            @foreach($free_courses as $cours)
                @foreach($cours->trans_curse as $lang_course)
                    @if( $lang_course->curse->groupPrice(Auth::user()->group_id) != NULL  and $lang_course->curse->groupPrice(Auth::user()->group_id)->price === 0)
                        <div class="col-md-12 course-list-item">
                            <div class="col-md-4">
                                <h3>{{mb_strimwidth($lang_course->name_trans,0,60,'...')}}</h3>
                                <img class="image" src="{{asset($lang_course->curse->image)}}">
                                <a class="btn-coursdetails" href="{{route('free_materials.show',$lang_course->curse->slug)}}">Начать обучение</a>
                            </div>
                            <div class="col-md-8 block-csd">
                                <p class="cours-short-desc">{{mb_strimwidth($lang_course->short_desc_trans,0,300,'...')}}</p>
                                <span class="col-md-6 long-term">Продолжительность :{{count($lang_course->curse->lectures)}} Недель</span>
                                <span class="col-md-6 max-bonus">Максимально 26 балл(-ов-а)</span>
                                <span class="col-md-6 cours-author">Автор курса</span>
                                <span class="col-md-6 cours-buy">Куплено 26 курсов</span>
                            </div>
                            <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                        </div>
                    @endif
                @endforeach
            @endforeach
            </div>
        </div>
    </div>
@endsection