{{--  $cours   --}}
@extends('layouts.admin')
@section('content')
    <div class="container">
        <h1 class="text-center">{{$cours->name_trans}}</h1>
        <div style="width: 100%; height: 400px; background-size: cover; background-image: url('{{asset($cours->curse->image)}}')"></div>
        <div class="panel">
            <h3>Краткое описание курса</h3>
            <p> {{$cours->short_desc_trans}}</p>
            <h3>Аннотация </h3>
            <p> {!! $cours->description_trans !!}</p>
        </div>
    </div>

    <div class="container">
        <div class="panel lections">
            <h3>Лекции курса:</h3>
            <hr>
            @if($cours->curse->lectures->groupBy('block_number')->count() === 1)
                @foreach($cours->curse->lectures as $lecture)
                    <a class="item" href="{{route('free_materials.prelection',['slug'=>$cours->curse->slug,'id'=>$lecture->id])}}">
                        <h4>{{$lecture->sdoLectureTrans()->name_trans}}</h4>
                        <div>{!!  $lecture->sdoLectureTrans()->short_desc !!}</div>
                    </a>
                @endforeach
            @else
                @foreach($cours->curse->lectures->groupBy('block_number') as $key => $value)
                    <div class="course-block">
                        <h4> <a data-toggle="collapse" data-parent="#accordion" href="{{'#block-'.$key}}" aria-expanded="false"
                                aria-controls="{{'block-'.$key}}">
                                <span class="number">{{$key}}</span> Блок лекций
                            </a></h4>
                        <div id="{{'block-'.$key}}" class="panel-collapse collapse" role="tabpanel">
                            @foreach($value as $lecture)
                                <a class="item" href="{{route('free_materials.prelection',['slug'=>$cours->curse->slug,'id'=>$lecture->id])}}">
                                    <h4>{{$lecture->sdoLectureTrans()->name_trans}}</h4>
                                    <div>{!!  $lecture->sdoLectureTrans()->short_desc !!}</div>
                                </a>
                            @endforeach
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
@endsection