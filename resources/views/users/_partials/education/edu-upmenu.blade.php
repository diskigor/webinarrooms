<div class="col-md-12 top-edu-menu hidden-xs">
    <ul class="nav nav-tabs nav-justified">
        <li class="{{ $active_menu=='educational_audience'?'active':'' }}">
            <a href="{{route('educational_audience.index')}}">Учебная аудитория</a>
        </li>
        <li class="{{ $active_menu=='free_studies'?'active':'' }}">
            <a href="{{route('free_studies.index')}}">Открытая школа</a>
        </li>

        {{-- <li data-route='studies'><a href="{{route('studies.index')}}">Основные курсы</a></li> --}}

        <li class="{{ $active_menu=='main_studies'?'active':'' }}">
            <a href="{{route('main_studies.index')}}">Курсы ШЭЛ</a>
        </li>
        
        <li class="{{ $active_menu=='lecture_studies'?'active':'' }}">
            <a href="{{route('lecture_studies.index')}}">Курсы партнеров</a>
        </li>
        
        <!--li> <a class="disabled">Курсы партнеров</a> </li-->
        
        <li class="{{ $active_menu=='my_works'?'active':'' }}">
            <a href="{{route('my_works.index')}}" >Мои задания</a>
        </li>
        <li class="{{ $active_menu=='webinars_room'?'active':'' }}">
            <a href="{{route('webinars_room.index')}}">Вебинарная комната</a>
        </li>
        <li class="{{ $active_menu=='advice_room'?'active':'' }}">
            <a href="{{route('advice_room.index')}}">Консультационная комната</a>
        </li>
        <li>
            <a href="https://insiderclub.ru/" target="_blank">Каталог семинаров</a>
        </li>
    </ul>
</div>