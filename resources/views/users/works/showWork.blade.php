{{--  $work   --}}
@extends('layouts.admin')
@section('content')
<?php $title_page='Текстовее задание'; ?>
    <div class="col-md-12 one-text-task">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center">{{$work->text_task->textTasksTrans()->name_trans}}</h3>
                <p>{!! $work->text_task->textTasksTrans()->content_trans !!}</p>
            </div>
            <div class="col-md-12">
                <h3 class="text-center">Ответ:</h3>
                <article class="answer-block">
                    {!! $work->answer !!}
                </article>
            </div>
            <div class="col-md-12">
                <h3 class="text-center"> Комментарии :</h3>
                <div class="comment-block">
                    @if($work->comments->count())
                        @foreach($work->comments as $comment)
                            <div class="panel">
                                <span>Автор: {{$comment->author_comment->name}}</span>
                                {!! $comment->comment !!}
                                @if($work->check_comment === NULL)
                                    <a href="{{route('my_works.edit',$comment->id)}}" class="btn btn-info">Нравится</a>
                                @endif
                                @if(isset($comment->like_com->text_answer_comment_id) && $comment->like_com->text_answer_comment_id === $comment->id)
                                    <i class="fa fa-trophy" aria-hidden="true"></i>
                                @endif
                            </div>
                        @endforeach
                    @else
                        <p>Комментариев нет</p>
                    @endif
                </div>
                <div class="col-md-12" style="margin: 10px auto; text-align: center;">
                    <a class="btn btn-blue" href="{{route('my_works.index')}}" >Назад</a>
                </div>
                
            </div>
        </div>
    </div>
@endsection