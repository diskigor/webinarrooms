{{--  $works   --}}
@extends('layouts.admin')
@section('content')
<?php $title_page='Таблица с ответами на текстовые задания'; ?>
    <div class="col-md-12">
        <!--p class="podskazki">
            Этот раздел содержит Ваши развернуты ответы, отчеты и сочинения, которые Вы будете писать в процессе освоения материала. 
            Здесь Вы сможете прочитать пять первых комментариев (мнений) более опытных пользователей, которые достигли статуса экспертов, 
            касающиеся предмета обсуждения, и выбрать лучший из них, взяв на вооружение полезную информацию для дальнейшего применения.
        </p-->
        <div class="row">
        @include('users._partials.education.edu-upmenu',['active_menu'=>'my_works'])
        <div class="col-md-12">
            <table class="table table-responsive">
                <thead>
                <tr>
                    <th>№</th>
                    <th>Тест</th>
                    <th>Курс</th>
                    <th>Комментарии экспертов</th>
                    <!--th>Дата написания</th>
                    <th>Действие</th-->
                </tr>
                </thead>
                <tbody>
                @foreach($works as $work)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td><a href="{{route('my_works.show',$work->id)}}">{{ $work->text_task->textTasksTrans()->name_trans }}</a></td>
                        <td>
                            {{ $work->text_task->prelection->course->block->Name }}
                            ->
                            {{ $work->text_task->prelection->course->LangName }}
                             ->
                            {{ $work->text_task->prelection->Name }}
                            : 
                            {{ $work->text_task->Name }}
                            
                            
                            <!-- Базовые курсы->Элементы психологии->Неделя1, день2: Фрейд -->
                        </td>
                        <td class="text-center"><a href="{{route('my_works.show',$work->id)}}"><i class="far fa-comments"></i> {{ $work->comments()->count() }}</i></a></td>
                        <!--td>{{-- $work->created_at --}}</td-->
                        <!--td>
                            <a href="{{--route('my_works.show',$work->id)--}}" class="btn btn-success">Подробнее</a>
                        </td-->
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="col-md-12 text-center">
                {{ $works->links()}}
            </div>
            
        </div>
        </div>
    </div>
@endsection