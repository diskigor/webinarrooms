{{-- $biling_user_orders --}}
@extends('Admin.app')
@section('content')
    <div class="container">
        <header class="big-header text-center">Пополнения , {{ Auth::user()->isAdmin()?' ВСЕ':Auth::user()->fullFIO }}</header>
        <div class="panel">

            @if($biling_user_orders->isEmpty())
                <p>Пополнений нет.</p>
            @else
                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Сумма</th>
                        <th>Описание платежа</th>
                        <th>Дата</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($biling_user_orders as $order)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $order->transfer_sum }} {{$order->currency}}</td>
                                <td>
                                    @if($order->isUser)
                                        {{ $order->number_inv }},
                                    @elseif($order->isReferal)
                                        Реферал ,
                                    @endif
                                    {{ $order->desc }}
                                </td>

                                <td>{{$order->created_at}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $biling_user_orders->links() }} 
            @endif
            
        </div>
    </div>
@endsection