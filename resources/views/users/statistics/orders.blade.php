@extends('Admin.app')
@section('content')
    <div class="container">
        <div class="panel">
            <h4 class="text-center">
                Статистика пользователя {{Auth::user()->name}}
            </h4>

            <table class="table table-responsive">
                <thead>
                <tr>
                    <th>№</th>
                    <th>Курс</th>
                    <th>Сумма</th>
                    <th>Дата</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1;?>
                    @if(isset($orders))
                        @foreach($orders as $order)
                        <tr>
                        <td>{{$i++}}</td>
                        <td>{{$order->course->trans_curse[0]->name_trans}}</td>
                        <td>{{$order->sum}}</td>
                        <td>{{$order->created_at}}</td>
                        </tr>
                        @endforeach
                    @else
                        У Вас нет покупок.
                    @endif
                </tbody>
            </table>
            {{ $orders->links() }}
        </div>
</div>
@endsection