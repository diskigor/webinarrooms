@extends('layouts.admin')
@section('content')
<script src="{{asset('js/jquery.circliful.min.js')}}"></script>
    <div class="col-md-12 educational">
        <?php $title_page='Учебная аудитория'; ?>
        <div class="row">
            @include('users._partials.education.edu-upmenu',['active_menu'=>'educational_audience'])               
            <div class="col-md-4">
                <h3>Открытые материалы</h3>
                <a href="{{route('free_studies.index')}}">
                <div class="bkock-educ">
                    <div class="col-md-4 col-sm-4 col-xs-4 result-edu">Пройдено: <b>{{ $open_user_courses_complite_count }}</b></div>
                    <div class="col-md-4 col-sm-4 col-xs-4 result-edu">В процессе: <b>{{ $open_user_courses_work_count}}</b></div>
                    <div class="col-md-4 col-sm-4 col-xs-4 result-edu" style="border: 0;">Всего: <b>{{ $open_user_courses_count }}</b></div>                    
                    @foreach($open_user_courses_last as $b_c)
                        <?php
                        
                        $koctil_last_bc = $b_c->course;
                        
                        ?>
                        <div class="charts-block">
                            <div class="col-md-8 col-sm-8 col-xs-8 charts-header">
                                <h3>{{ $koctil_last_bc->LangName }}</h3>
                                <p>Всего лекций: <b>{{ $koctil_last_bc->lectures->count() }}</b></p>
                                <p>Пройдено: <b>{{ $koctil_last_bc->progressCurse(Auth::id())?$koctil_last_bc->progressCurse(Auth::id())->fullStepsCount():0 }}</b></p>
                                <p>Осталось: <b>{{ $koctil_last_bc->lectures->count() - ($koctil_last_bc->progressCurse(Auth::id())?$koctil_last_bc->progressCurse(Auth::id())->fullStepsCount():0) }}</b></p>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 charts-diagramm">
                                <div id="main-material-id-open-{{ $b_c->id }}"></div>
                                <script>
                                    $( document ).ready(function() {
                                        $("#main-material-id-open-{{ $b_c->id }}").circliful({
                                            animation: 1,
                                            animationStep: 4,
                                            percent: {{ $koctil_last_bc->progressCurse(Auth::id())?$koctil_last_bc->progressCurse(Auth::id())->currentProcent():0 }},
                                        });
                                    });
                                </script>
                            </div>
                        </div>
                    @endforeach
                    <p class="more-educational">Подробнее...</p>
                </div>
                </a>
            </div>
            <div class="col-md-4">
                <h3>Основные курсы ШЭЛ</h3>
                <a href="{{route('main_studies.index')}}">
                <div class="bkock-educ">
                    <div class="col-md-4 col-sm-4 col-xs-4 result-edu">Пройдено: <b>{{ $main_user_courses_complite_count }}</b></div>
                    <div class="col-md-4 col-sm-4 col-xs-4 result-edu">В процессе: <b>{{ $main_user_courses_work_count}}</b></div>
                    <div class="col-md-4 col-sm-4 col-xs-4 result-edu" style="border: 0;">Всего: <b>{{ $main_user_courses_count }}</b></div>
                    @foreach($main_user_courses_last as $b_c)
                        <?php
                        
                        $koctil_last_bc = $b_c->course;
                        
                        ?>
                        <div class="charts-block">
                            <div class="col-md-8 col-sm-8 col-xs-8 charts-header">
                                <h3>{{ $koctil_last_bc->LangName }}</h3>
                                <p>Всего лекций: <b>{{ $koctil_last_bc->lectures->count() }}</b></p>
                                <p>Пройдено: <b>{{ $koctil_last_bc->progressCurse(Auth::id())?$koctil_last_bc->progressCurse(Auth::id())->fullStepsCount():0 }}</b></p>
                                <p>Осталось: <b>{{ $koctil_last_bc->lectures->count() - ($koctil_last_bc->progressCurse(Auth::id())?$koctil_last_bc->progressCurse(Auth::id())->fullStepsCount():0) }}</b></p>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 charts-diagramm">
                                <div id="main-material-id-main-{{ $b_c->id }}"></div>
                                <script>
                                    $( document ).ready(function() {
                                        $("#main-material-id-main-{{ $b_c->id }}").circliful({
                                            animation: 1,
                                            animationStep: 4,
                                            percent: {{ $koctil_last_bc->progressCurse(Auth::id())?$koctil_last_bc->progressCurse(Auth::id())->currentProcent():0 }},
                                        });
                                    });
                                </script>
                            </div>
                        </div>
                    @endforeach
                    <p class="more-educational">Подробнее...</p>
                </div>
                </a>
            </div>
            <div class="col-md-4">
                <h3>Курсы партнеров ШЭЛ</h3>
                <a href="{{route('lecture_studies.index')}}">
                <div class="bkock-educ">
                    <div class="col-md-4 col-sm-4 col-xs-4 result-edu">Пройдено: <b>{{ $lecture_user_courses_complite_count }}</b></div>
                    <div class="col-md-4 col-sm-4 col-xs-4 result-edu">В процессе: <b>{{ $lecture_user_courses_work_count}}</b></div>
                    <div class="col-md-4 col-sm-4 col-xs-4 result-edu" style="border: 0;">Всего: <b>{{ $lecture_user_courses_count }}</b></div>
                    @foreach($lecture_user_courses_last as $b_c)
                        <?php
                        
                        $koctil_last_bc = $b_c->course;
                        
                        ?>
                        <div class="charts-block">
                            <div class="col-md-8 col-sm-8 col-xs-8 charts-header">
                                <h3>{{ $koctil_last_bc->LangName }}</h3>
                                <p>Всего лекций: <b>{{ $koctil_last_bc->lectures->count() }}</b></p>
                                <p>Пройдено: <b>{{ $koctil_last_bc->progressCurse(Auth::id())?$koctil_last_bc->progressCurse(Auth::id())->fullStepsCount():0 }}</b></p>
                                <p>Осталось: <b>{{ $koctil_last_bc->lectures->count() - ($koctil_last_bc->progressCurse(Auth::id())?$koctil_last_bc->progressCurse(Auth::id())->fullStepsCount():0) }}</b></p>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 charts-diagramm">
                                <div id="main-material-id-lecturer-{{ $b_c->id }}"></div>
                                <script>
                                    $( document ).ready(function() {
                                        $("#main-material-id-lecturer-{{ $b_c->id }}").circliful({
                                            animation: 1,
                                            animationStep: 4,
                                            percent: {{ $koctil_last_bc->progressCurse(Auth::id())?$koctil_last_bc->progressCurse(Auth::id())->currentProcent():0 }},
                                        });
                                    });
                                </script>
                            </div>
                        </div>
                    @endforeach
                    <p class="more-educational">Подробнее...</p>
                </div>
                </a>
            </div>
            <div class="separator col-md-12"></div>
            <div class="col-md-4">
                <h3>Мои задания</h3>
                <a href="{{route('my_works.index')}}">
                <div class="bkock-educ">
                    <div class="tasks-chart">
                        <div class="col-md-8 col-sm-8 col-xs-8 tasks-chart-txt">Всего выполнено заданий</div>
                        <div class="col-md-4 col-sm-4 col-xs-4 tasks-chart-data"><p>{{ $count_text_answer }}</p></div>
                    </div>
                    <div class="tasks-chart">
                        <div class="col-md-8 col-sm-8 col-xs-8 tasks-chart-txt">Всего заданий на проверке</div>
                        <div class="col-md-4 col-sm-4 col-xs-4 tasks-chart-data"><p>{{ $count_text_answer }}</p></div>
                    </div>
                    <div class="tasks-chart">
                        <div class="col-md-8 col-sm-8 col-xs-8 tasks-chart-txt">Всего Новых комментариев</div>
                        <div class="col-md-4 col-sm-4 col-xs-4 tasks-chart-data"><p>{{ $count_text_answer_comment }}</p></div>
                    </div>
                    <p class="more-educational">Подробнее...</p>
                </div>
                </a>
            </div>
            <div class="col-md-4">
                <h3>Вебинары</h3>
                <a href="#">
                <div class="bkock-educ">
                    <div class="col-md-12 count-down-block">
                        <h3>До ближайшего вебинара</h3>
                        <div id="clockdiv">
                            <div>
                                <span class="days"></span>
                                <div class="smalltext">Дней</div>
                            </div>
                            <div>
                                <span class="hours"></span>
                                <div class="smalltext">Часов</div>
                            </div>
                            <div>
                                <span class="minutes"></span>
                                <div class="smalltext">Минут</div>
                            </div>
                            <div>
                                <span class="seconds"></span>
                                <div class="smalltext">Секунд</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 featured-web">
                        <h3>Всего предстоящих</h3>
                        <p>5</p>
                        <h3>Вебинаров</h3>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 my-web">
                        <h3>Всего пройденых</h3>
                        <p>25</p>
                        <h3>Вебинаров</h3>
                    </div>
                    <div class="clearfix"></div>
                    <p class="more-educational">Подробнее...</p>
                </div>
                </a>
            </div>
            <div class="col-md-4">
                <h3>Семинары</h3>
                <a href="https://insiderclub.ru/" target="_blanc">
                <div class="bkock-educ">
                    <div class="col-md-12 featured-sem">
                        <h3>Всего Записей</h3>
                        <p>50</p>
                        <h3>Семинаров</h3>
                    </div>
                    <p class="more-educational">Подробнее...</p>
                </div>
                </a>
            </div>
        </div>
        <script>
            function getTimeRemaining(endtime) {
            var t = Date.parse(endtime) - Date.parse(new Date());
            var seconds = Math.floor((t / 1000) % 60);
            var minutes = Math.floor((t / 1000 / 60) % 60);
            var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
            var days = Math.floor(t / (1000 * 60 * 60 * 24));
            return {
              'total': t,
              'days': days,
              'hours': hours,
              'minutes': minutes,
              'seconds': seconds
            };
          }

          function initializeClock(id, endtime) {
            var clock = document.getElementById(id);
            var daysSpan = clock.querySelector('.days');
            var hoursSpan = clock.querySelector('.hours');
            var minutesSpan = clock.querySelector('.minutes');
            var secondsSpan = clock.querySelector('.seconds');

            function updateClock() {
              var t = getTimeRemaining(endtime);

              daysSpan.innerHTML = t.days;
              hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
              minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
              secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

              if (t.total <= 0) {
                clearInterval(timeinterval);
              }
            }

            updateClock();
            var timeinterval = setInterval(updateClock, 1000);
          }

          var deadline="January 01 2019 00:00:00 GMT+0300"; //for Ukraine
          var deadline = new Date(Date.parse(new Date()) + 15 * 24 * 60 * 60 * 1000); // for endless timer
          initializeClock('clockdiv', deadline);
        </script>
    </div>
@endsection