{{--  $free_courses   --}}
@extends('layouts.admin')
@section('content')

<?php $title_page='Открытая школа'; ?>
    <div class="col-md-12 courses-block">
        <div class="row">
            @include('users._partials.education.edu-upmenu',['active_menu'=>'free_studies'])
            <div class="col-md-12 text-center">
            <a href="{{ url('/front_test') }}" type="button" class="btn btn-blue">
                {{trans('home.testmain')}}
            </a>
        </div>
            <div class="col-md-12">
            @foreach($free_courses as $course)
                <div class="col-md-12 course-list-item">
                    <div class="col-md-4">
                        <h3>{{ mb_strimwidth($course->langName,0,60,'...') }}</h3>
                        <img class="image" src="{{ $course->imagex }}">
                        <a class="btn-coursdetails" href="{{ route('free_studies.show',$course->id) }}">
                            @if($course->currentProcent(Auth::id())== 0)
                                Начать
                            @endif
                            @if($course->currentProcent(Auth::id())== 100)
                                Повторить
                            @endif
                            @if(0<$course->currentProcent(Auth::id()) && $course->currentProcent(Auth::id())<100)
                                Продолжить
                            @endif
                             обучение
                        </a>
                    </div>
                    <div class="col-md-8 block-csd">
                        <p class="cours-short-desc">{{ mb_strimwidth($course->langShortD,0,300,'...') }}</p>
                        <span class="col-md-6 long-term">Продолжительность недель: <b>{{ $course->lectures()->count() }}</b> </span>
                        <span class="col-md-6 max-bonus">Максимально баллов: <b>{{ $course->lecturesBallsCount }}</b></span>
                        <span class="col-md-6 cours-author">Автор курса: <b>{{ $course->author?$course->author->fullFIO:'' }}</b></span>
                        <span class="col-md-6 cours-buy">Приобретено студентами раз: <b>{{ $course->getUserBuy() }}</b></span>
                    </div>
                    <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                </div>
            @endforeach
            </div>
        </div>
    </div>
@endsection