{{--  $user_courses   --}}
@extends('layouts.admin')
@section('content')
<?php $title_page='Учебная аудитория, основные кусы'; ?>
    <div class="col-md-12">
        <div class="row">
            @include('users._partials.education.edu-upmenu',['active_menu'=>'main_studies'])
            <div class="col-md-12 buy-main-courses">
            @foreach($grouped as $group)
                <div class="col-md-12 buy-main-courses-block">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <div class="col-md-4">
                                    <h3>{{ $group->name }}</h3>
                                    <img class="image" src="{{asset('images/blog_pic_5.jpg')}}">
                                        <button title="Включено в учебный план ( {{ count($group->courses).' / '.$group->CoursesCount }} )"
                                                class="disabled">
                                                 Включено в учебный план ( {{ count($group->courses).' / '.$group->CoursesCount }} )
                                        </button>
                                </div>
                                <div class="col-md-8 block-csd">
                                    <p class="cours-short-desc">{!! $group->description !!}</p>
                                    <hr>
                                    @foreach($group->courses as $course)
                                        @include('users.courses._partials.short_figure_course',[ 'course'=>$course,'route_type' => $route_type ])
                                    @endforeach
                                    <div class="col-md-12" style="margin-bottom: 50px"></div>
                                </div>
                                <a role="button" 
                                    class="double-down"
                                    data-toggle="collapse" 
                                    data-parent="#accordion" 
                                    href="#collapseOne_{{$loop->iteration}}" 
                                    aria-expanded="true" 
                                    aria-controls="collapseOne"><i class="fa fa-angle-double-down foo_mrakis" aria-hidden="true"></i></a>
                                <a role="button" 
                                    class="show-all-courses"
                                    data-toggle="collapse" 
                                    data-parent="#accordion" 
                                    href="#collapseOne_{{$loop->iteration}}" 
                                    aria-expanded="true" 
                                    aria-controls="collapseOne">
                                    смотреть список включенных курсов<br>

                                </a>
                            </div>
                            <div id="collapseOne_{{$loop->iteration}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    @foreach($group->courses as $course)
                                        @include('users.courses._partials.figure_course',[ 'course'=>$course,'route_type' => $route_type ])
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
        

        
        {{-- список оплаченых курсов, не входящих в блоки курсов --}}
        <div class="row">
            <div class="col-md-11 col-md-offset-1">
            @foreach($user_courses as $cours)
                
            <div class="col-md-12 course-list-item">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <h3>{{ mb_strimwidth($cours->course->langName,0,60,'...') }}</h3>
                    <img class="image" src="{{ $cours->course->imagex }}">
                    <a class="btn-coursdetails" href="{{ route('main_studies.show',$cours->course->id) }}">Продолжить обучение</a>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12 block-csd">
                    <p class="cours-short-desc">{!! mb_strimwidth($cours->course->langShortD,0,300,'...') !!}</p>
                    <span class="col-md-6 col-sm-6 col-xs-12 long-term">Продолжительность дней: <b>{{ $cours->course->lectures()->count() }}</b></span>
                    <span class="col-md-6 col-sm-6 col-xs-12 max-bonus">Максимально баллов за курс: <b>{{ $cours->course->lecturesBallsCount }}</b></span>
                    <span class="col-md-6 col-sm-6 col-xs-12 cours-author">Автор курса: <b>{{ $cours->course->author?$cours->course->author->fullFIO:'' }}</b></span>
                    <span class="col-md-6 col-sm-6 col-xs-12 cours-buy">Приобретено студентами раз: <b>{{ $cours->course->getUserBuy() }}</b></span>
                </div>
                <i class="fa fa-angle-double-down" aria-hidden="true"></i>
            </div>
            @endforeach
            </div>
        </div>
        {{-- список оплаченых курсов, не входящих в блоки курсов --}}
    </div>
<script>
$('.foo_mrakis').on('click', function(){
                $(this).toggleClass('rotate');
        });
</script>
@endsection