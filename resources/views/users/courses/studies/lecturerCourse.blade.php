{{--  $user_courses   --}}
@extends('layouts.admin')
@section('content')
<?php $title_page='Учебная аудитория, основные кусы'; ?>
    <div class="col-md-12">
        <div class="row">
            @include('users._partials.education.edu-upmenu',['active_menu'=>'lecture_studies'])
            <div class="col-md-12">
            @foreach($user_courses as $cours)
                
            <div class="col-md-12 course-list-item">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <h3>{{ mb_strimwidth($cours->course->langName,0,60,'...') }}</h3>
                    <img class="image" src="{{ $cours->course->imagex }}">
                    <a class="btn-coursdetails" href="{{ route('lecture_studies.show',$cours->course->id) }}">
                            @if($cours->course->currentProcent(Auth::id())== 0)
                                Начать
                            @endif
                            @if($cours->course->currentProcent(Auth::id())== 100)
                                Повторить
                            @endif
                            @if(0<$cours->course->currentProcent(Auth::id()) && $cours->course->currentProcent(Auth::id())<100)
                                Продолжить
                            @endif
                             обучение
                    </a>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12 block-csd">
                    <p class="cours-short-desc">{!! mb_strimwidth($cours->course->langShortD,0,300,'...') !!}</p>
                    <span class="col-md-6 col-sm-6 col-xs-12 long-term">Продолжительность дней: <b>{{ $cours->course->lectures()->count() }}</b></span>
                    <span class="col-md-6 col-sm-6 col-xs-12 max-bonus">Максимально баллов за курс: <b>{{ $cours->course->lecturesBallsCount }}</b></span>
                    <span class="col-md-6 col-sm-6 col-xs-12 cours-author">Автор курса: <b>{{ $cours->course->author?$cours->course->author->fullFIO:'' }}</b></span>
                    <span class="col-md-6 col-sm-6 col-xs-12 cours-buy">Приобретено студентами раз: <b>{{ $cours->course->getUserBuy() }}</b></span>
                </div>
                <i class="fa fa-angle-double-down" aria-hidden="true"></i>
            </div>
            @endforeach
            </div>
        </div>
    </div>
@endsection