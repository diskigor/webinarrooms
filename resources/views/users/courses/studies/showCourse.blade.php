{{--  $course   --}}
@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-md-12 week-menu">
        <!-- Nav tabs -->
        <ul class="nav nav-pills" role="tablist">
            @foreach($course->lectures->groupBy('block_number') as $key => $value)
            <li role="presentation" class="js_synhro_tabs {{ $loop->first?'active':''}}"><a href="{{'#time_block_'.$key}}" aria-controls="home" role="tab" data-toggle="tab">Неделя <span class="number">{{$key}}</span></a></li>

            @endforeach
        </ul>
        <h1 class="text-center">{{ $course->langName }}</h1>
        <hr>
        <!--div style="width: 100%; height: 400px; background-size: cover; background-image: url('{{ $course->imagex }}')"></div>
        <div class="panel">
            <h3>Краткое описание курса</h3>
            <p> {{ $course->langShortD }}</p>
            <h3>Аннотация </h3>
            <p> {!! $course->langDescription !!}</p>
        </div-->
    </div>
    <div class="col-md-12">
        <div class="panel lections">
            <!--h3>Лекции курса:</h3-->
            <!-- Tab panes -->
            <div class="tab-content">
                @foreach($course->lectures->groupBy('block_number') as $key => $value)
                <div role="tabpanel" class="tab-pane {{ $loop->first?'active':''}}" id="{{'time_block_'.$key}}">
                    @foreach($value as $lecture)
                    @if (Auth::user()->progressCurse($course->id)->getAvailableLection($lecture->id))
                        <div class="{{ ($current_step == $lecture->id)?'course_current':'course_completed' }}" data-cs="{{ $current_step }} {{ $lecture->id }}">
                            @include('users.courses._partials.tabs_one_lection',['lecture'=>$lecture])
                        </div>
                    <?php $my_open = true; ?>
                    
                    @elseif(Auth::user()->progressCurse($course->id)->getAvailableLectionDate($lecture->id))
                    <div class="item js_open_lecture_ball_div course_can_be_opened" >
                        <h4 data-toggle="tooltip" 
                            data-placement="left"
                            data-balls="{{ $lecture->AllBallsCount }}"
                            title="Лекция станет доступна {{ Auth::user()->progressCurse($course->id)->getAvailableLectionDate($lecture->id) }}">
                            <i class="fa fa-lock" aria-hidden="true"></i>
                            {{ $lecture->name }}
                        </h4>
                        <!--div data-toggle="tooltip" data-placement="left"
                             title="Лекция станет доступна {{ Auth::user()->progressCurse($course->id)->getAvailableLectionDate($lecture->id) }}">
                            {!! mb_strimwidth(strip_tags($lecture->langShortD),0,240,'...')  !!}
                        </div-->
                        <div class="cleafix">
                            <div class="col-md-3">
                            <p class="balls-lections">MAX Баллов<br><span class="borderedinfo">{{ $lecture->AllBallsCount }}</span></p>
                            
                            </div>
                            <div class="col-md-5 open-text">
                                <p>
                                    *Эта лекция откроется автоматически через 24 часа<br>Также ее можно открыть досрочно за баллы
                                </p>
                                
                            </div>
                            <div class="col-md-4 open-text">
                                <p style="margin-bottom: 0;">
                                    <button class="js_open_lecture_ball btn btn-blue" 
                                        data-route="{{ route('openLectionFromBalls') }}"
                                        data-curse_id="{{ $course->id }}"
                                        data-lecture_id="{{ $lecture->id }}"
                                        data-text="Вы готовы открыть эту лекцию за <b>{{ round($lecture->AllBallsCount/100*config('vlavlat.ball_procent_open_lection'), 0, PHP_ROUND_HALF_UP) }}</b>  балл(-ов-а)?"
                                        >Открыть день</button>
                                </p>
                                <p>
                                    *c Вас спишется баллов: {{ round($lecture->AllBallsCount/100*config('vlavlat.ball_procent_open_lection'), 0, PHP_ROUND_HALF_UP) }}
                                </p>
                            </div>
                        </div>
                        
                        
                    </div>
                    @else
                    <div class="item course_can_not_be_opened">
                        <h4 data-toggle="tooltip" 
                            data-placement="left"
                            data-balls="{{ $lecture->AllBallsCount }}"
                            title="Вы не  прошли предыдущую лекцию!">
                            <i class="fa fa-lock" aria-hidden="true"></i>
                            {{ $lecture->name }}
                        </h4>
                        <div data-toggle="tooltip" data-placement="left"
                             title="Вы не  прошли предыдущую лекцию!">
                            {!! mb_strimwidth(strip_tags($lecture->langShortD),0,240,'...')  !!}
                        </div>
                    </div>
                    @endif
                    @endforeach
                </div>
                @endforeach
                <div class="col-md-12 week-menu" style="padding: 0; margin-top: 10px">
                    <!-- Nav tabs -->
                    <ul class="nav nav-pills" role="tablist">
                        @foreach($course->lectures->groupBy('block_number') as $key => $value)
                        <li role="presentation" class="js_synhro_tabs {{ $loop->first?'active':''}}"><a href="{{'#time_block_'.$key}}" aria-controls="home" role="tab" data-toggle="tab">Неделя <span class="number">{{$key}}</span></a></li>
                        @endforeach
                    </ul>
                </div>
               
            </div>
        </div>
    </div>
</div>
@endsection
@push('stack_head_script')
    
    <script>
        function isEmpty(obj) {
            for(var prop in obj) { if(obj.hasOwnProperty(prop)) { return false;} }
            return true;
        }
        
        function getSDOCourseCookie() {
            var in_c = getCookie('course_{{$course->id}}');
            try { in_c = JSON.parse(in_c); } catch (e) { in_c = {}; }
            return in_c;
        }

        var course_progress_sdo = getSDOCourseCookie();

        function updateSDOCourseCookie() { setCookie('course_{{$course->id}}',JSON.stringify(course_progress_sdo)); }

        function reinitCurrentLection(){
            course_progress_sdo.course_progress     = {!! json_encode($course->progressCurse(Auth::id())->currentSteps()) !!};
            course_progress_sdo.lection_current_id  = {!! $current_step !!},
            course_progress_sdo.lection_study_need  = {!! json_encode($lection_study_need) !!};
            course_progress_sdo.lection_studies     = {!! json_encode($lection_study_need) !!};
            updateSDOCourseCookie();
        }
        
        function updateCurrentLection($params) {
            if ($params === undefined) { return; }
            if (!$params.toUpperCase) { return; }

            for (var key in course_progress_sdo.lection_study_need) {
                if (course_progress_sdo.lection_study_need[key] == $params) {
                    course_progress_sdo.lection_study_need.splice(key, 1);
                    //testmode, TODO remove
                    //console.log('delete params ',$params,course_progress_sdo);
                    updateSDOCourseCookie();
                }
            }
        }
        
        function verifyCloseCurrentLection(){
            $.ajax({
                
                type: 'POST',
                url: "{{ $route_type?route($route_type.'.update',$current_step):'' }}",
                data: {"_method":"PUT","local_cook":course_progress_sdo},
                success: function (msg) {                    
                    if (msg.errors !== undefined) { }
                    
                    if (msg.success !== undefined && msg.text === undefined) {
                        swal(msg.success);
                    }
                    
                    if (msg.success !== undefined && msg.text !== undefined) { 
                        swal({
                            title: msg.success,
                            html: true,
                            text: msg.text + '',
                            });
                        }; 

                    if (msg.reinit && msg.reinit === 'cookies') { reinitCurrentLection(); }
                    if (msg.reinit && msg.reinit === 'page') { 
                        setTimeout(function() { window.location.reload(); }, 4000);
                        }
                }
            });
        }
        
        
        
        if (isEmpty(course_progress_sdo)) {//init
            course_progress_sdo = {};
            course_progress_sdo.name                = 'course_{{$course->id}}';
            course_progress_sdo.course_id           = {{ $course->id }};
            reinitCurrentLection();
        }
        
        if (course_progress_sdo.lection_current_id === undefined || course_progress_sdo.lection_current_id != {!! $current_step !!}) { reinitCurrentLection(); }
        
        //verifyCloseCurrentLection();
        //testmode, TODO remove
        console.log(course_progress_sdo);
        
        $(document).on('click','span.js_previous_material_in_lecture',function(){
            var lecture_id = $(this).data('lecture_id');
            if (lecture_id !== undefined) {
                $(this).siblings('button').click();
                var links = 'a[href="#lection_'+ lecture_id+'_home"]';
                $(links).click();
            }
//           console.log(this); 
//           console.log($(this).data('lecture_id'));
        });
        
        $(document).on('click','span.js_next_material_in_lecture',function(){
            var material_link = $(this).data('link');
            if (material_link !== undefined) {
                $(this).siblings('button').click();
                var links = 'a[href="#'+ material_link+'"]';
                $(links).click();
            }
        });
        

        setTimeout(function(){
          $('.js_lecture_content').each(function() {
                $( this ).height($(window).height()/4*3);
                
                
              });
          }, 1000);
    </script>
@endpush

@push('stack_scripts')
    <script src="{{asset('js/main_studies.js')}}"></script>
    <script>
        $('div.content').on('click.synhro','li.js_synhro_tabs',function(){
            var a_click = $(this).find('a').first();
            var a_href = $(a_click).attr('href');

            var need = $(document).find('a[href="' + a_href + '"]').not(a_click).parents('li.js_synhro_tabs');
            
            $(need).siblings('li.js_synhro_tabs').removeClass('active');
            $(need).addClass('active');
        });
    </script>
@endpush