{{--  $course   --}}
@extends('layouts.admin')
@section('content')
    <div class="container">
        @if($lecture->course->trans_curse[0])
            <h1 class="text-center">{{$lecture->course->trans_curse[0]->name_trans}}</h1><h1 class="text-center">{{$lecture->sdoLectureTrans()->name_trans}}</h1>
        @endif
        <div style="width: 100%;     height: 200px;
                background-position: center; background-size: cover; background-image: url('{{ $lecture->course->imagex }}');box-shadow: 0 4px 8px rgba(0,0,0,.1);"></div>
        <div class="panel">
            <h3>Ваше сегодняшнее задание</h3>
            <p> {!! $lecture->sdoLectureTrans()->short_desc !!}</p>
        </div>
    </div>

    <div class="container">
        <div class="panel">
            <!-- Modal Lecture -->
            <div class="text-center">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary btn-lg" data-toggle="modal"
                        data-target="#lecture_{{$lecture->id}}">
                    Лекция
                </button>
            </div>
            <!-- Modal -->

            <div class="modal fade" id="lecture_{{$lecture->id}}" 
                 tabindex="-1" 
                 role="dialog" 
                 data-backdrop="static"
                 aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <!--button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button-->
                            <h4 class="modal-title" id="myModalLabel">{{$lecture->sdoLectureTrans()->name_trans}}</h4>
                        </div>
                        <div class="modal-body">
                            {!! $lecture->sdoLectureTrans()->content_trans !!}
                        </div>
                        <div class="modal-footer">

                                <input type="checkbox"
                                       class="required_checked_element pull-left hidden"
                                       name="required_checked_element_lecture_{{$lecture->id}}"
                                       data-warning ="Нужно прочитать лекцию">

                            <button type="button" class="btn btn-default" data-dismiss="modal">Лекция изучена</button>
                        </div>
                    </div>
                </div>
            </div>
            <!--  -->
        </div>
        <!-- Вывод пополнительного материала  -->
        @if($lecture->materials->count() != 0)
        <div class="panel">
            <h3 class="text-center">Дополнительные материалы</h3>
            @foreach($lecture->materials as $material)
            
                @if($material->material->type === "audio")
                    <!-- Вывод дополнительного материала аудио -->

                        <p class="text-center">
                            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal"
                                    data-target="#audiomaterial_{{$material->material->id}}">
                                Аудио : {{$material->material->name}}
                            </button>
                        </p>
                        <!-- Modal -->
                        <div class="modal fade" id="audiomaterial_{{$material->material->id}}" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title"
                                            id="myModalLabel"> {{$material->material->name}}</h4>
                                    </div>
                                    <div class="modal-body">
                                        <audio controls controlsList="nodownload" >
                                            <source src="{{ getUrlMaterialId($material->material->id) }}"
                                                    type="audio/mp3">
                                        </audio>
                                    </div>
                                    <div class="modal-footer">
                                        
                                        <input type="checkbox"
                                               class="required_checked_element pull-left hidden"
                                               name="required_checked_element_material_audio_{{$material->material->id}}"
                                               data-warning ='Нужно прослушать "{{$material->material->name}}"'>
                                        
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                @elseif($material->material->type ==="text")
                    <!-- Вывод дополнительного материала текстового формата -->
                        <!-- Button trigger modal -->
                        <p class="text-center">
                            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal"
                                    data-target="#txtmaterial_{{$material->material->id}}">
                                {{$material->material->name}}
                            </button>
                        </p>
                        <!-- Modal -->
                        <div class="modal fade" id="txtmaterial_{{$material->material->id}}" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title"
                                            id="myModalLabel"> {{$material->material->name}}</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>{!! $material->material->content !!}</p>
                                    </div>
                                    <div class="modal-footer">
                                        <input type="checkbox"
                                               class="required_checked_element pull-left hidden"
                                               name="required_checked_element_material_text_{{$material->material->id}}"
                                               data-warning ='Нужно прочитать "{{$material->material->name}}"'>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                @elseif($material->material->type ==="pdf")
                    <!-- Вывод дополнительного материала pdf формата -->
                        <div class="text-center">
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal"
                                    data-target="#pdfmaterial_{{ $material->material->id }}">
                                {{$material->material->name}}
                            </button>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="pdfmaterial_{{$material->material->id}}" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel_pdfmaterial">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel_pdfmaterial">{{$material->material->name}}</h4>
                                    </div>
                                    <div class="modal-body">
                                        <a href="{{ getUrlMaterialId($material->material->id) }}" target="_blank">Открыть PDF</a>
                                    </div>
                                    <div class="modal-footer">
                                        <input type="checkbox"
                                               class="required_checked_element pull-left hidden"
                                               name="required_checked_element_material_pdf_{{$material->material->id}}"
                                               data-warning ='Нужно прочитать "{{$material->material->name}}"'>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                @elseif($material->material->type ==="video_link")
                        <div class="text-center">
                            <!-- Вывод дополнительного материала видео -->
                            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal"
                                    data-target="{{'#my'.$material->material->id}}">
                                Видео : {{$material->material->name}}
                            </button>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="{{'my'.$material->material->id}}" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title"
                                            id="myModalLabel"> {{$material->material->name}}</h4>
                                    </div>
                                    <div class="modal-body">
                                        <video controls controlsList="nodownload" width="560" height="190">
                                            <source src="{{ getUrlMaterialId($material->material->id) }}">
                                        </video>
                                    </div>
                                    <div class="modal-footer">
                                        <input type="checkbox"
                                               class="required_checked_element pull-left hidden"
                                               name="required_checked_element_material_video_{{$material->material->id}}"
                                               data-warning ='Нужно просмотреть "{{$material->material->name}}"'>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                @elseif($material->material->type ==="archives")
                    <!-- Вывод дополнительного материала pdf формата -->


                        <div class="text-center">
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal"
                                    data-target="#archivesmaterial_{{ $material->material->id }}">
                                {{$material->material->name}}
                            </button>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="archivesmaterial_{{$material->material->id}}" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel_archivesmaterial">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel_archivesmaterial">{{$material->material->name}}</h4>
                                    </div>
                                    <div class="modal-body">
                                        <a href="{{ getUrlMaterialId($material->material->id) }}" target="_blank">Скачать архив</a>
                                    </div>
                                    <div class="modal-footer">
                                        <input type="checkbox"
                                               class="required_checked_element pull-left hidden"
                                               name="required_checked_element_material_archives_{{$material->material->id}}"
                                               data-warning ='Нужно скачать "{{$material->material->name}}"'>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                 @endif
                <br/>
            @endforeach
        </div>
        @endif
        <!-- Конец вывода дополнительного материала -->
        
        @if($lecture->text_tasks->count() != 0 )
        <!-- Вывод  -->
            <div class="panel">
                @foreach($lecture->text_tasks as $text_task)
                    <div class="text-center texttaskbtn">
                        <button type="button" class="btn btn-primary btn-lg" data-toggle="modal"
                                data-target="{{'#'.$text_task->id}}">
                            {{$text_task->textTasksTrans()?$text_task->textTasksTrans()->name_trans:'-'}}
                        </button>
                    </div>
                
                    <div class="modal fade" id="{{$text_task->id}}" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title"
                                        id="myModalLabel">{{$text_task->textTasksTrans()->name_trans}}</h4>
                                </div>
                                <div class="modal-body">
                                    <p>{!! $text_task->textTasksTrans()->content_trans !!}</p>
                                    @if(is_null($text_task->answerUserText))
                                        {!! Form::open(array('route'=>['answer_text_tasks',$text_task->id],'method'=>'POST')) !!}
                                        {!! Form::label('answer_text_tasks','Ответ :',array_merge(['class'=>'control-label'])) !!}
                                    <div class="no-editor">
                                        {!! Form::textarea('answer_text_tasks',null,array_merge(['class'=>'form-control'])) !!}
                                    </div>
                                        {!! Form::submit('Ответить',array_merge(['class'=>'btn btn-success'])) !!}
                                        {!! Form::close() !!}
                                    @else
                                        <div class="panel">
                                            <h3>Ваш ответ: </h3>
                                            <p>{!! $text_task->answerUserText->answer !!}</p>
                                        </div>
                                    @endif
                                    
                                    
                                    
                                </div>
                                <div class="modal-footer">
                                        <input type="checkbox"
                                               class="required_checked_element pull-left hidden"
                                               disabled="disabled"
                                               name="required_checked_element_tasks_{{$text_task->id}}"
                                               data-warning ='Необходимо ответить на все вопросы задания'
                                               {{is_null($text_task->answerUserText)?'':'checked="checked"'}}
                                               ></input>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        <!-- Вывод закончен -->
        @endif

        @if($lecture->test_tasks->count() != 0)
        <!--  вывод тестовых заданий -->
            @foreach($lecture->test_tasks as $test)
            
                @if($test->type_id === 1) {{-- $test->typeTest->name === 'boolean' --}}
                    @include('users.courses.modal_test_boolean', ['test' => $test ])
                @endif
                
                @if($test->type_id === 2) {{-- $test->typeTest->name === 'group' --}}
                    @include('users.courses.modal_test_group', ['test' => $test ])
                @endif

                @if($test->type_id === 3) {{-- $test->typeTest->name === 'concatenation' --}}
                    @include('users.courses.modal_test_concatenation', ['test' => $test ])
                @endif
                
            @endforeach
        <!--  конец вывода тестовых заданий -->
        @endif
    
        {{-- @if(Auth::user()->progressCurse($lecture->curse_id)->step > $lecture->number ) --}}
        <?php
            $progress = Auth::user()->progressCurse($lecture->curse_id);

            $p_cur_lection  = in_array($lecture->id,$progress->fullSteps());
            $closed_curse   = $progress->closed;
            $last_lection   = $lecture->course->verifyLastLectionID($lecture->id);
        
        ?>
        
        

        @if(!$p_cur_lection)
        <!-- Проверка тестовых и текстовых пройденых заданий -->
            <div class="panel">
                <div class="text-center">
                    <a href="{{route('finish_prelection',$lecture->id)}}" id="end_of_game_zis" class=" btn btn-success">Завершить</a>
                </div>
            </div>
            <!-- -->
        @elseif ($p_cur_lection && !$last_lection)
            <div class="panel">
                <div class="text-center ">
                    <a href="{{route('studies.show',$lecture->curse_id)}}" class="btn btn-success">Пройдено</a>
                </div>
            </div>
        @elseif( $p_cur_lection && $last_lection && $closed_curse)
            <div class="panel">
                <div class="text-center ">
                    <a href="{{route('studies.show',$lecture->curse_id)}}" class="btn btn-success">Курс пройден</a>
                </div>
            </div>
        @endif
    </div>
@endsection
@section('script')
<script src="{{asset('js/cookie.js')}}"></script>
    <!--  STOP AUDIO  -->

    <script>
        $(function () {
            $('.modal').modal({
                show: false
            }).on('hidden.bs.modal', function () {

                var prau = $(this).find('audio');
                if (prau !== undefined && prau.length>0) {
                    prau[0].pause();
                }
            
                
                var prcheck = $(this).find('input.required_checked_element');
                if (prcheck !== undefined && prcheck.length>0) {
                    if (!prcheck.attr('disabled')) {
                         prcheck.attr('checked','checked');
                         verifyAllChecked(false);
                         //console.log(this);
                    }
                }
                

                
            });
            
            
            
            
        $('#end_of_game_zis').on('click',function(event){
            event.preventDefault();

            if (!countARR(getLectionCookie())) {
                window.location.href = this.href;
            } else {
                var all_check = $('input.required_checked_element:checkbox').not('input:checked');
                if (all_check !== undefined && all_check.length>0) {
                    swal(all_check.first().data('warning'));
                } else {
                    var errm = '{{Auth::id()}} / {{$lecture->user_id}} / {{$lecture->id}}, ' + getLectionCookie().join(' ');
                    swal('Ошибка,Скрин администратору',errm,'error');
                }
            }
        });
        

        
            verifyAllChecked(isFirstInitCook());
        });

        function verifyAllChecked(start){
            var all_checked = $('input.required_checked_element');
            var cookiess = getLectionCookie();
            
            all_checked.each(function(){
                var xxx = cookiess.indexOf(this.name);
                if (xxx > -1) {
                        if (this.checked) {
                            //console.log('this.checked && coockie ',this)
                            cookiess.splice(xxx, 1); 
                            } 
                            //////
                    } else {
                        if (!this.checked && start) {
                            cookiess.push(this.name); 
                            } else if (!start) {
                                $(this).attr('checked','checked');
                            }
                            //////
                    };
            });

            cookiess = jQuery.unique(cookiess);
            //console.log(cookiess,getLectionCookie());
            setLectionCookie(cookiess);
        } 

        function isFirstInitCook(){
            var c = getCookie('progress_lection_{{$lecture->id}}');
            return c === undefined;
        }

        function getLectionCookie(){
            var c = getCookie('progress_lection_{{$lecture->id}}');
            if (c === undefined || (typeof (JSON.parse(c)) !== 'object')){
                setLectionCookie();
                return new Array();
            } 
            return JSON.parse(getCookie('progress_lection_{{$lecture->id}}'));
        }
        
        function setLectionCookie(value){
            if (value === undefined) { value = new Array();}
            setCookie('progress_lection_{{$lecture->id}}',JSON.stringify(value));
        }
        
        function countARR(c){var a=0,b;for(b in c)b&&a++;return a}
        
    </script>
@endsection