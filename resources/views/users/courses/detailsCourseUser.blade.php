{{--  $course   --}}
@extends('layouts.admin')
@section('content')
    <div class="container">
        <h1 class="text-center">{{$course->trans_curse[0]->name_trans}}</h1>
        <div style="width: 100%; height: 400px; background-size: cover; background-image: url('{{asset($course->image)}}')"></div>
        <div class="panel">
            <h3>Краткое описание курса</h3>
            <p> {{$course->trans_curse[0]->short_desc_trans}}</p>
            <h3>Аннотация </h3>
            <p> {!! $course->trans_curse[0]->description_trans !!}</p>
        </div>
    </div>

    <div class="container">
        <div class="panel lections">
            <h3>Лекции курса:</h3>
            <hr>
            @if($course->lectures->groupBy('block_number')->count() === 1)
                @foreach($course->lectures as $lecture)
                
                    {{-- @if($lecture->number <= Auth::user()->progressCurse($course->id)->step) --}}
                    @if (in_array($lecture->id,Auth::user()->progressCurse($course->id)->currentSteps()))
                        <a class="item" href="{{route('studies.learn_prelection',$lecture->id)}}" data-test="{{$lecture->id}}">
                            <h4>

                                {{$lecture->sdoLectureTrans()->name_trans}}</h4>
                            <div>{!! mb_strimwidth(strip_tags($lecture->sdoLectureTrans()->short_desc),0,240,'...')  !!}</div>
                        </a>
                    @else
                        <div class="item">
                            <h4 data-toggle="tooltip" data-placement="left" title="Вы не  прошли предыдущую лекцию!" data-test="{{$lecture->id}}">
                                <i class="fa fa-lock" aria-hidden="true"></i>
                                {{$lecture->sdoLectureTrans()->name_trans}}
                            </h4>
                            <div data-toggle="tooltip" data-placement="left" title="Вы не  прошли предыдущую лекцию!" data-test="{{$lecture->id}}">
                                {!! mb_strimwidth(strip_tags($lecture->sdoLectureTrans()->short_desc),0,240,'...')  !!}
                            </div>
                        </div>
                    @endif
                @endforeach
            @else
                @foreach($course->lectures->groupBy('block_number') as $key => $value)
                    <div class="course-block">
                        <h4> <a data-toggle="collapse" data-parent="#accordion" href="{{'#block-'.$key}}" aria-expanded="false"
                       aria-controls="{{'block-'.$key}}">
                        <span class="number">{{$key}}</span> Блок лекций
                    </a></h4>
                    <div id="{{'block-'.$key}}" class="panel-collapse collapse" role="tabpanel">
                            @foreach($value as $lecture)
                                {{-- @if($lecture->number <= Auth::user()->progressCurse($course->id)->step) --}}
                                @if (in_array($lecture->id,Auth::user()->progressCurse($course->id)->currentSteps()))
                                    <a class="item" href="{{route('studies.learn_prelection',$lecture->id)}}">
                                        <h4>{{$lecture->sdoLectureTrans()->name_trans}}</h4>
                                        <div>{!! mb_strimwidth(strip_tags($lecture->sdoLectureTrans()->short_desc),0,240,'...')  !!}</div>
                                    </a>
                                @else
                                    <div class="item">
                                        <h4 data-toggle="tooltip" data-placement="left"
                                            title="Вы не  прошли предыдущую лекцию!">
                                            <i class="fa fa-lock" aria-hidden="true"></i>
                                            {{$lecture->sdoLectureTrans()->name_trans}}
                                        </h4>
                                        <div data-toggle="tooltip" data-placement="left"
                                             title="Вы не  прошли предыдущую лекцию!">
                                            {!! mb_strimwidth(strip_tags($lecture->sdoLectureTrans()->short_desc),0,240,'...')  !!}
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
@endsection