@extends('layouts.admin')
@section('content')
<?php $title_page='Партнерские курсы'; ?>
    <div class="col-md-12 buy-main-courses">
        <div class="row">
            <div class="col-md-12">
        @if((Auth::user()->isCadet() or Auth::user()->isСurator()) &&  !Auth::user()->checkEntrance())
            <div class="row alert alert-warning text-center">
                {!! Form::open(array('route'=>'mybalance.index','method'=>'GET'))!!}
                {!! Form::submit('Внести вступительный взнос',array_merge(['class'=>'btn btn-success','onclick'=>'return checkByu()'])) !!}
                {!! Form::close() !!}
            </div>
        @endif
        
        @if(!empty($bay_all_curses))
            {!! $bay_all_curses !!}
        @endif
        
        <!--список не оплаченных курсов -->
        <?php $ex_id_curses = Auth::user()->coursesByu->pluck('curses_id')->toArray();?>
        @foreach($courses as $cours)
            @if(!in_array($cours->id,$ex_id_curses))
                @foreach($cours->trans_curse as $lang_course)
                    @if( $lang_course->curse->groupPrice(Auth::user()->group_id) != NULL  and $lang_course->curse->groupPrice(Auth::user()->group_id)->price != 0)
                    <div class="col-md-12 course-list-item buy-main-courses-block-child">
                        <div class="col-md-4">
                            <h3>{{mb_strimwidth($lang_course->name_trans,0,60,'...')}}</h3>
                            <img class="image" src="{{ $lang_course->curse->imagex }}">
                            @if(Auth::user()->checkEntrance() && !$cours->xclosed)
                                {!! Form::open(array('route'=>['buy_curses',$lang_course->sdo_curses_id],'method'=>'GET'))!!}
                                <button type="submit" title="заказать курс" class="btn-coursdetails act_buyCours">
                                    <i class="fa fa-shopping-cart"></i>
                                    Приобрести курс за {{$lang_course->curse->groupPrice(Auth::user()->group_id)->price}} Кэл
                                </button>
                                {!! Form::close() !!}
                            @elseif(!Auth::user()->checkEntrance())
                                <button class="disabled"><i class="fa fa-lock" aria-hidden="true"></i></button>
                            @elseif($cours->xclosed)
                                <button class="disabled"><i class="fa fa-lock" title="Нужно изучить курс: {{ $cours->xname }}"aria-hidden="true"></i></button>
                            @endif
                        </div>
                        <div class="col-md-8 block-csd">
                            <p class="cours-short-desc">{{ mb_strimwidth($lang_course->langShortD,0,300,'...') }}</p>
                            <span class="col-md-6 long-term">Продолжительность дней: <b>{{ $cours->lectures()->count() }}</b></span>
                            <span class="col-md-6 max-bonus">Максимально баллов за курс: <b>{{ $cours->lecturesBallsCount }}</b></span>
                            <span class="col-md-4 cours-author">Ваша скидка: <b>{{$lang_course->curse->discount}}%</b></span>
                            <span class="col-md-4 cours-created">
                                <a href="{{$lang_course->slug}}" class="" title="смотреть программу" data-toggle="modal" data-target="{{'#modal-'.$lang_course->curse->id}}">
                                    Описание курса
                                </a>
                            </span>
                            <span class="col-md-4 cours-buy">Приобретено студентами раз: <b>{{ $cours->getUserBuy() }}</b></span>
                        </div>
                        <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                    </div>       
                    <!-- Modal -->
                    <div class="modal fade" id="{{'modal-'.$lang_course->curse->id}}" tabindex="-1" role="dialog">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">{{ $cours->langName }}</h4>
                                </div>
                                <div class="modal-body">{!! $cours->LangDescription !!}</div>
                                <div class="modal-footer"  style="text-align: center;">
                                    @if(Auth::user()->checkEntrance() && !$cours->xclosed)
                                        {!! Form::open(array('route'=>['buy_curses',$lang_course->sdo_curses_id],'method'=>'GET'))!!}
                                        <button type="submit" title="заказать курс" class="btn-coursdetails act_buyCours">
                                            <i class="fa fa-shopping-cart"></i>
                                            Приобрести курс за {{$lang_course->curse->groupPrice(Auth::user()->group_id)->price}} Кэл
                                        </button>
                                        {!! Form::close() !!}
                                    @elseif(!Auth::user()->checkEntrance())
                                        <button class="disabled"><i class="fa fa-lock" aria-hidden="true"></i></button>
                                    @elseif($cours->xclosed)
                                        <button class="disabled"><i class="fa fa-lock" title="Нужно изучить курс: {{ $cours->xname }}"aria-hidden="true"></i></button>
                                    @endif
                                        
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal -->
                    @endif
                @endforeach
            @endif
        @endforeach
        <!-- список оплаченых курсов партнёров -->
        @if(Auth::check())
            @foreach($courses as $cours)
                @if(in_array($cours->id,$ex_id_curses))
                <div class="col-md-12 course-list-item buy-main-courses-block-child">
                    <div class="col-md-4">
                        <h3>{{mb_strimwidth($cours->trans_curse[0]->name_trans,0,60,'...')}}</h3>
                        <img class="image" src="{{ $cours->imagex }}">
                        <a class="btn-coursdetails" href="{{route('lecture_studies.show',$cours->id)}}" title="{{trans('curses.goto_course')}}">Перейти к изучению</a>
                    </div>
                    <div class="col-md-8 block-csd">
                        <p class="cours-short-desc">{{mb_strimwidth($cours->langShortD,0,300,'...')}}</p>
                        <span class="col-md-6 long-term">Продолжительность дней: <b>{{ $cours->lectures()->count() }}</b></span>
                        <span class="col-md-6 max-bonus">Максимально баллов за курс: <b>{{ $cours->lecturesBallsCount }}</b></span>
                        <span class="col-md-4 cours-author">Автор курса: <b>{{ $cours->author?$cours->author->fullFIO:'' }}</b></span>
                        <span class="col-md-4 cours-created">
                            <a href="{{$cours->slug}}" class="" title="смотреть программу" data-toggle="modal" data-target="{{'#modal-'.$cours->id}}">
                                Описание курса
                            </a>
                        </span>
                        <span class="col-md-4 cours-buy">Приобретено студентами раз: <b>{{ $cours->getUserBuy() }}</b></span>
                    </div>
                    <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                </div>
                <!-- Modal -->
                <div class="modal fade" id="{{'modal-'.$cours->id}}" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">{{ $cours->langName }}</h4>
                            </div>
                            <div class="modal-body">
                                {!!  $cours->LangDescription !!}
                            </div>
                            <div class="modal-footer"  style="text-align: center;">
                                <a class="btn-coursdetails" href="{{route('lecture_studies.show',$cours->id)}}" title="{{trans('curses.goto_course')}}">Перейти к изучению</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal -->
                @endif
                <script>
                $(document).ready(function () {
                    if (window.location.hash === "#modal-{{ $cours->id }}") {
                      $("#modal-{{ $cours->id }}").modal('show');
                    }
                  });
                </script>
            @endforeach
        @endif
        </div>
    </div>
    </div>
    <script language="JavaScript" type="text/javascript">
        $(document).ready(function () {
            $('.act_buyCours').on('click', function (event) {
                event.preventDefault();
                var xe = event;
                swal({
                        title: 'Вы уверены, что хотите включить этот курс в Учебный план?',
                        /*text: "Подвердите сой выбор.",*/
                        type: "warning",
                        confirmButtonColor: "#336899",
                        confirmButtonText: "Да",
                        closeOnConfirm: true,
                        closeOnCancel: false,
                        closeOnClickOutside: false,
                        showCancelButton: true,
                        cancelButtonText: "Нет"
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            var xf = $(xe.currentTarget).parent('form');
                            $(xf).submit();
                        }
                        else {
                            swal("Вы отказались", "Включить этот курс в Учебный план", "error");
                        }
                    });
            });
        });
        
        
        

    </script>
@endsection
