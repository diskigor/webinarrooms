
@extends('layouts.admin')
@section('content')
    <div class="col-md-12">
        <?php $title_page='Партнерские курсы'; ?>
        <!--p class="podskazki">В этом разделе находятся курсы других лекторов Школы.Мы будем регулярно обновлять список партнерских курсов, расширяя авторское сотрудничество с нашими партнерами, поэтому не забывайте заглядывать в этот раздел – возможно в следующий раз мы добавим курс, который будет Вам полезен или даже необходим!
        </p-->
        @if((Auth::user()->isCadet() or Auth::user()->isСurator()) &&  !Auth::user()->checkEntrance())
            <div class="row alert alert-warning text-center">
                {!! Form::open(array('route'=>'mybalance.index','method'=>'GET'))!!}
                {!! Form::submit('Внести вступительный взнос',array_merge(['class'=>'btn btn-success','onclick'=>'return checkByu()'])) !!}
                {!! Form::close() !!}
            </div>
        @endif
        
        <div class="row">{{-- список ещё не оплаченых курсов --}}
            <?php 
                $ex_id_curses = Auth::user()->coursesByu->pluck('curses_id')->toArray();
            ?>

            @foreach($courses as $cours)
                @if(!in_array($cours->id,$ex_id_curses))
                    @foreach($cours->trans_curse as $lang_course)
                        @if( $lang_course->curse->groupPrice(Auth::user()->group_id) != NULL  and $lang_course->curse->groupPrice(Auth::user()->group_id)->price != 0)
                            <figure class="col-sm-4">
                                <div class="thumbnail text-center courses">
                                    <h3>{{mb_strimwidth($lang_course->name_trans,0,60,'...')}}</h3>
                                    <img class="image" src="{{ $lang_course->curse->imagex }}">
                                    <div>Кол-во учебных дней : {{count($lang_course->curse->lectures)}}
                                    </div>
                                    <figcaption>
                                        <h5>Ваша скидка : {{$lang_course->curse->discount}} %<br/> Стоимость курса
                                            : {{$lang_course->curse->groupPrice(Auth::user()->group_id)->price}}
                                            {{--<img width="10" src="{{asset('images/kel.png')}}" alt="КЭЛ">--}}
                                            <i class="fa fa-eur kel" aria-hidden="true"></i>
                                        </h5>
                                        <p class="hidden">{{mb_strimwidth($lang_course->short_desc_trans,0,100,'...')}}</p>
                                    </figcaption>
                                    <div class="hover-nav">
                                        @if(Auth::user()->checkEntrance() && !$cours->xclosed)
                                            {!! Form::open(array('route'=>['buy_curses',$lang_course->sdo_curses_id],'method'=>'GET'))!!}
                                            <button type="submit" title="заказать курс"
                                                    class="btn btn-success act_buyCours">
                                                <i class="fa fa-shopping-cart"></i>
                                            </button>
                                            {!! Form::close() !!}
                                        @elseif(!Auth::user()->checkEntrance())
                                            <i class="fa fa-lock" aria-hidden="true"></i>
                                        @elseif($cours->xclosed)
                                            <i class="fa fa-lock" title="Нужно изучить курс: {{ $cours->xname }}"aria-hidden="true"></i>
                                        @endif
                                    </div> 
                                    <a href="{{$lang_course->slug}}" class="btn btn-blue"
                                       title="смотреть программу"
                                       data-toggle="modal"
                                       data-target="{{'#modal-'.$lang_course->curse->id}}">
                                        <span>{{trans('news.view')}}</span>
                                    </a>
                                    <!-- Modal -->
                                    <div class="modal fade" id="{{'modal-'.$lang_course->curse->id}}" tabindex="-1"
                                         role="dialog">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <div class="image"
                                                         style="background-image: url('{{ $lang_course->curse->imagex }}')"></div>
                                                    <h4 class="modal-title">{{$lang_course->name_trans}}</h4>
                                                </div>
                                                <div class="modal-body">
                                                    {!!  $lang_course->description_trans!!}
                                                </div>
                                                <div class="modal-footer"  style="text-align: center;">
                                                    @if(Auth::user()->checkEntrance() && !$cours->xclosed)
                                                        {!! Form::open(array('route'=>['buy_curses',$lang_course->sdo_curses_id],'method'=>'GET'))!!}
                                                         <button type="submit" title="заказать курс"
                                                                 class="btn btn-blue act_buyCours">
                                                             <i class="fa fa-shopping-cart"></i> Включить этот курс в Учебный план
                                                         </button>
                                                        {!! Form::close() !!}
                                                        <br/>
                                                    @endif
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </figure>
                        @endif
                    @endforeach
                @endif
            @endforeach
        </div>
        <div class="row">{{-- список оплаченых курсов партнёров --}}
            @if(Auth::check())
            @foreach($courses as $cours)
                @if(in_array($cours->id,$ex_id_curses))
                    <figure class="col-sm-4">
                        <div class="thumbnail text-center courses">
                            <h3>{{mb_strimwidth($cours->trans_curse[0]->name_trans,0,60,'...')}}</h3>
                            <img class="image" src="{{ $cours->imagex }}">
                            <div>Кол-во учебных дней : {{count($cours->lectures)}}
                            </div>
                            <figcaption>
                                <h5>Ваша скидка : {{$cours->discount}} % <br/>Стоимость курса
                                    : {{$cours->groupPrice(Auth::user()->group_id)->price}}
                                    <i class="fa fa-eur kel" aria-hidden="true"></i>
                                </h5>
                            </figcaption>
                            <a href="{{route('studies.show',$cours->id)}}" class="btn btn-grey"
                               title="{{trans('curses.goto_course')}}">
                                <span>Включено в учебный план</span>
                            </a>
                            <!-- Modal -->
                            <div class="modal fade" id="{{'modal-'.$cours->id}}" tabindex="-1"
                                 role="dialog">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close"><span
                                                        aria-hidden="true">&times;</span>
                                            </button>
                                            <div class="image"
                                                 style="background-image: url('{{ $cours->imagex }}')"></div>
                                            <h4 class="modal-title">{{ $cours->trans_curse[0]->name_trans}}</h4>
                                        </div>
                                        <div class="modal-body">
                                            {!!  $cours->trans_curse[0]->description_trans!!}
                                            <div>
                                                @if(count($cours->lectures)!=0)
                                                    <span>В данный курс входят следующие лекции :</span>
                                                    @foreach($cours->lectures as $lecture)
                                                        @if($lecture->sdoLectureTrans())
                                                            <p>{{$lecture->sdoLectureTrans()->name_trans}}</p>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <span>В данном курсе нет лекци</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </figure>
                @endif
                @endforeach
            @endif
        </div>
    </div>
    <script language="JavaScript" type="text/javascript">
        $(document).ready(function () {
            $('.act_buyCours').on('click', function (event) {
                event.preventDefault();
                var xe = event;
                swal({
                        title: 'Вы уверены, что хотите включить этот курс в Учебный план?',
                        /*text: "Подвердите сой выбор.",*/
                        type: "warning",
                        confirmButtonColor: "#336899",
                        confirmButtonText: "Да",
                        closeOnConfirm: true,
                        closeOnCancel: false,
                        closeOnClickOutside: false,
                        showCancelButton: true,
                        cancelButtonText: "Нет"
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            var xf = $(xe.currentTarget).parent('form');
                            $(xf).submit();
                        }
                        else {
                            swal("Вы отказались", "Включить этот курс в Учебный план", "error");
                        }
                    });
            });
        });
    </script>
@endsection