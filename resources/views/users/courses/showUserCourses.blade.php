{{--  'courses','grouped','bay_curses','route_type','bay_all_curses'   --}}
@extends('layouts.admin')
@section('content')
<?php $title_page='Основные курсы'; ?>
    <div class="col-md-12 buy-main-courses">
        <!--p class="podskazki">
            В этом разделе представлены авторские курсы Е.В.Гильбо. Система курсов составлена таким образом, 
            чтобы помочь Вам легко усваивать материал, поэтому доступность курсов подчиняется определенной иерархической структуре, 
            когда вы получаете возможность поставить курс в учебный план только после приобретения его предшественников. 
            Таким образом, Вы приобретете и изучаете, к примеру, курс «Элементы психологии» и это автоматически делает для Вас доступными курсы «Элементы теории власти», 
            «Типологии Юнга» и т.д.
        </p-->
        @if((Auth::user()->isCadet() or Auth::user()->isСurator()) &&  !Auth::user()->checkEntrance())
            <div class="row alert alert-warning text-center">
                        {!! Form::open(array('route'=>'mybalance.index','method'=>'GET'))!!}
                        {!! Form::submit('Внести вступительный взнос',array_merge(['class'=>'btn btn-success','onclick'=>'return checkByu()'])) !!}
                        {!! Form::close() !!}
            </div>
        @endif
        
        @if(!empty($bay_all_curses))
            {!! $bay_all_curses !!}
        @endif
        
        <div class="row">
        @foreach($grouped as $group)
            <div class="col-lg-4 col-md-6 table-view-course">
                <div class="table-view-course-block">
                    <a href="#anchor_{{ $group->id }}"><h4><img src="{{asset('images/mane/logo-star-b.png')}}">{{ $group->name }}</h4></a>
                    @foreach($group->courses as $course)
                        <a href="#anchor_{{ $group->id }}">
                            <span class="col-xs-8">{{ mb_strimwidth($course->trans_curse->first()->name_trans,0,40,'...') }}</span>
                            <span class="col-xs-4" style="text-align: right;">{{ $course->groupPrice(Auth::user()->group_id)->price }} КЭЛ</span>
                        </a>
                    @endforeach
                </div>
            </div>
        @endforeach
        <div class="col-md-12">
            @foreach($grouped as $group)
            <div class="col-md-12 buy-main-courses-block" id="anchor_{{ $group->id }}">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <div class="col-md-4">
                                <h3>{{ $group->name }}</h3>
                                <img class="image" src="{{asset('images/block_pic_def.jpg')}}">
                                @if(Auth::user()->checkEntrance() && $group->price_buy)
                                    {!! Form::open(array('route'=>['buyBlockCourses',$group->id],'method'=>'POST'))!!}
                                    <button type="submit" title="заказать блок курсов"
                                            class="btn-coursdetails act_buyBlock">
                                        <i class="fa fa-shopping-cart"></i> Приобрести все за : <b>{{ $group->price_buy }} КЭЛ</b>
                                    </button>
                                    {!! Form::close() !!}
                                @elseif(!Auth::user()->checkEntrance())
                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                @else
                                    <button title="Блок курсов включен в учебный план"
                                            class="disabled">
                                             {{-- $group->name --}} Блок включен в учебный план
                                    </button>
                                @endif
                            </div>
                            <div class="col-md-8 block-csd">
                                <p class="cours-short-desc">{!! $group->description !!}</p>
                                <span class="">
                                    @if($group->price_buy)
                                        <span class="">Стоимость блока : <b>{{ $group->global_price_summ }} КЭЛ</b></span>
                                        @if($group->global_buy_summ)<br>
                                        <span class="">Уже приобретено на : <b>{{ $group->global_buy_summ }} КЭЛ</b></span>
                                        @endif
                                    @endif
                                </span>
                                <hr>
                                @foreach($group->courses as $course)
                                    @include('users.courses._partials.short_figure_course',[ 'course'=>$course,'route_type' => $route_type ])
                                @endforeach
                                <div class="col-md-12" style="margin-bottom: 50px"></div>
                            </div>
                            <a role="button" 
                                class="double-down"
                                data-toggle="collapse" 
                                data-parent="#accordion" 
                                href="#collapseOne_{{$loop->iteration}}" 
                                aria-expanded="true" 
                                aria-controls="collapseOne"><i class="fa fa-angle-double-down foo_mrakis" aria-hidden="true"></i></a>
                            <a role="button" 
                                class="show-all-courses"
                                data-toggle="collapse" 
                                data-parent="#accordion" 
                                href="#collapseOne_{{$loop->iteration}}" 
                                aria-expanded="true" 
                                aria-controls="collapseOne">
                                смотреть список курсов<br>

                            </a>
                        </div>
                        <div id="collapseOne_{{$loop->iteration}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                @foreach($group->courses as $course)
                                    @include('users.courses._partials.figure_course',[ 'course'=>$course,'route_type' => $route_type ])
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                
                @foreach($group->courses as $course)
                <div class="modal fade" id="{{'llllmodal-'.$course->id}}" tabindex="-1"
                    role="dialog">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <div class="image"
                                    style="background-image: url('{{ $course->imagex }}')"></div>
                                <h4 class="modal-title">{{ $course->trans_curse->first()->name_trans }}</h4>
                            </div>
                            <div class="modal-body">
                                {!!  $course->trans_curse->first()->description_trans !!}
                            </div>
                            <div class="modal-footer"  style="text-align: center;">
                                @if(Auth::user()->checkEntrance() && !$course->xclosed)
                                    {!! Form::open(array('route'=>['buy_curses',$course->id],'method'=>'GET'))!!}
                                    <button type="submit" title="заказать курс"
                                            class="btn btn-blue act_buyCours">
                                        <i class="fa fa-shopping-cart"></i> Включить этот курс в Учебный план
                                    </button>
                                    {!! Form::close() !!}
                                    <br/>
                                @endif
                                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                            </div>
                        </div>
                    </div>
                </div>
                <script>
                    $(document).ready(function () {
                        if (window.location.hash === "#modal-{{ $course->id }}") {
                            console.log("#llllmodal-{{ $course->id }}");
                          $("#llllmodal-{{ $course->id }}").modal('show');
                        }
                      });
                </script>
                @endforeach
            </div>
            @endforeach
            {{-- список ещё не оплаченых курсов, не входящих в блоки курсов --}}
                @foreach($courses as $cours)

                    @if (in_array($cours->id, $bay_curses))
                        @continue
                    @endif

                    @if( $cours->groupPrice(Auth::user()->group_id) != NULL  and $cours->groupPrice(Auth::user()->group_id)->price != 0)
                        @include('users.courses._partials.figure_course',[ 'course'=>$cours,'route_type' => $route_type ])
                    @endif

                @endforeach
            {{-- список ещё не оплаченых курсов, не входящих в блоки курсов --}}
            {{-- список оплаченых курсов, не входящих в блоки курсов --}}
                @if(Auth::check())
                    @foreach($courses as $cours)

                        @if (!in_array($cours->id, $bay_curses))
                            @continue
                        @endif

                        @if( $cours->groupPrice(Auth::user()->group_id) != NULL  and $cours->groupPrice(Auth::user()->group_id)->price != 0)
                            @include('users.courses._partials.figure_course',[ 'course'=>$cours,'route_type' => $route_type ])
                        @endif

                    @endforeach
                @endif
            {{-- список оплаченых курсов, не входящих в блоки курсов --}}
        </div>
        </div>

        

        

    </div>
    <script language="JavaScript" type="text/javascript">
        $(document).ready(function () {
            $('.act_buyCours').on('click', function (event) {
                event.preventDefault();
                var xe = event;
                swal({
                        title: 'Вы уверены, что хотите включить этот курс в Учебный план?',
                        /*text: "Подвердите свой выбор.",*/
                        type: "warning",
                        confirmButtonColor: "#336899",
                        confirmButtonText: "Да",
                        closeOnConfirm: true,
                        closeOnCancel: false,
                        closeOnClickOutside: false,
                        showCancelButton: true,
                        cancelButtonText: "Нет",
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            var xf = $(xe.currentTarget).parent('form');
                            $(xf).submit();
                        }
                        else {
                            swal("Вы отказались", "Включить этот курс в Учебный план", "error");
                        }
                    });
            });

            $('.act_buyBlock').on('click', function (event) {
                event.preventDefault();
                var xe = event;
                swal({
                        title: 'Вы уверены, что хотите включить этот блок курсов в Учебный план?',
                        /*text: "Подвердите свой выбор.",*/
                        type: "warning",
                        confirmButtonColor: "#336899",
                        confirmButtonText: "Да",
                        closeOnConfirm: true,
                        closeOnCancel: false,
                        closeOnClickOutside: false,
                        showCancelButton: true,
                        cancelButtonText: "Нет",
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            var xf = $(xe.currentTarget).parent('form');
                            $(xf).submit();
                        }
                        else {
                            swal("Вы отказались", "Включить этот блок курсов в Учебный план", "error");
                        }
                    });
            });
        });
        $('.foo_mrakis').on('click', function(){
                $(this).toggleClass('rotate');
        });
    </script>
@endsection