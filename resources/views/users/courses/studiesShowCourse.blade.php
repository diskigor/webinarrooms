{{--  $user_courses   --}}
@extends('layouts.admin')
@section('content')
    <div class="col-md-12">
        <?php $title_page='Учебная аудитория'; ?>
        <!--p class="podskazki">Вы находитесь в учебной аудитории. Здесь отображаются все курсы, стоящие в Вашем учебном плане и доступные к изучению. 
            Выберите нужный курс, зайдите в него и начните проходить учебный день. 
            Учебный день состоит из лекции и различных дополнительных материалов (тестов, письменных заданий, теоретических заданий, аудио и видео материалов и т.д.), 
            за изучение которых Вам начисляется определенное количество баллов. Следующий учебный день станет доступным после изучения всех материалов текущего учебного дня. 
            Подробнее с работой системы Вы можете ознакомиться в разделе <a href="/page/rules">«Руководство пользователя»</a> на главной странице Вашего личного кабинета.
        </p-->
        
        <div class="row">
            @foreach($user_courses as $cours)
                
            <div class="col-md-12 course-list-item">
                <div class="col-md-4">
                    <h3>{{ mb_strimwidth($cours->course->langName,0,60,'...') }}</h3>
                    <img class="image" src="{{ $cours->course->imagex }}">
                    <a class="btn-coursdetails" href="{{ route('studies.show',$cours->course->id) }}">Начать обучение</a>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12 block-csd">
                    <p class="cours-short-desc">{!! mb_strimwidth($cours->course->langShortD,0,300,'...') !!}</p>
                    <span class="col-md-6 col-sm-6 col-xs-12 long-term">Продолжительность дней: <b>{{ $cours->course->lectures()->count() }}</b></span>
                    <span class="col-md-6 col-sm-6 col-xs-12 max-bonus">Максимально баллов за курс: <b>{{ $cours->course->lecturesBallsCount }}</b></span>
                    <span class="col-md-6 col-sm-6 col-xs-12 cours-author">Автор курса: <b>{{ $cours->course->author?$cours->course->author->fullFIO:'' }}</b></span>
                    <span class="col-md-6 col-sm-6 col-xs-12 cours-buy">Приобретено студентами раз: <b>{{ $cours->course->getUserBuy() }}</b></span>
                </div>
                <i class="fa fa-angle-double-down" aria-hidden="true"></i>
            </div>
            @endforeach
        </div>
    </div>
@endsection