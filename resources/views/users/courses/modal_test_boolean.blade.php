{{--  на входе $test   --}}              

<div class="panel">
    @if(isset($test->userTestAnswer) && isset($test->userTestAnswer->jsonAnswer))
        <div class="text-center">
            <button type="button" 
                    class="btn btn-success btn-lg center" 
                    data-toggle="modal"
                    title="Предыдущие результаты"
                    data-target="{{'#m_prev'.$test->id}}">
                Тест - {{$test->name}} - результаты
            </button>
        </div>
        <div class="modal fade" 
             id="{{'m_prev'.$test->id}}" 
             tabindex="-1" 
             role="dialog"
             aria-labelledby="myModalLabel{{'m_prev'.$test->id}}">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-center" id="myModalLabel{{'m_prev'.$test->id}}">Вы прошли данный тест</h4>
                    </div>
                    <div class="modal-body">
                        <div class="panel">
                            <h3 class="text-center">Тест - {{$test->name}}</h3>

                            <?php $us_answer = $test->userTestAnswer->jsonAnswer; ?>
                            
                            @if(isset($us_answer['description']) && $us_answer['description'])
                                <hr>
                                <h4 class="text-center">Результаты теста</h4>
                                <div class="form-group bg-warning">
                                    {!! $us_answer['description'] !!}
                                </div>
                                <hr>
                            @endif
                            
                            @foreach($test->testQuestions as $question)
                                <h4>Вопрос №{{$loop->iteration}}</h4>
                                <p>{!! $question->question !!}</p>

                                @if($question->button_type == 'radio')
                                    @foreach($question->testAnswerQuestion as $answer)
                                        <?php 
                                            $class_s='';
                                            $check_s=false;
                                            if (isset($us_answer[$question->id]) && isset($us_answer[$question->id]['answer_id']) && $us_answer[$question->id]['answer_id'] == $answer->id ) {
                                                $class_s=' __id_'.$answer->id; 
                                                if(!isset($us_answer['description']) || isset($us_answer['description']) && !$us_answer['description']){
                                                    $class_s=' bg-danger __id_'.$answer->id; 
                                                }
                                                if (isset($us_answer[$question->id]['balls']) 
                                                        && $us_answer[$question->id]['balls']>0
                                                        && (!isset($us_answer['description']) || isset($us_answer['description']) && !$us_answer['description'])
                                                        ) { $class_s=' bg-success __id_'.$answer->id; }

                                                $check_s=true; 
                                            }
                                        ?>

                                        <div class="form-group{{ $class_s}}"> 

                                            {!! Form::radio('answer['.$question->id.']', $answer->answer, $check_s,['disabled' => 'disabled'] ) !!}
                                            {!! Form::label('answer',$answer->answer) !!}
                                        </div>
                                    @endforeach
                                @else
                                    @foreach($question->testAnswerQuestion as $answer)
                                        <?php 
                                            $class_s=' b';
                                            $check_s=false;
                                            if (isset($us_answer[$question->id]) && is_array($us_answer[$question->id]) ) {

                                                if (isset($us_answer[$question->id][$answer->id])) {
                                                    $class_s=' __id_'.$answer->id; 
                                                    if(!isset($us_answer['description']) || isset($us_answer['description']) && !$us_answer['description']){
                                                        $class_s=' bg-danger __id_'.$answer->id; 
                                                    }
                                                    if (isset($us_answer[$question->id][$answer->id]['balls'])
                                                            && $us_answer[$question->id][$answer->id]['balls']>0
                                                            && (!isset($us_answer['description']) || isset($us_answer['description']) && !$us_answer['description']))
                                                    { $class_s=' bg-success __id_'.$answer->id;  }
                                                    $check_s=true; 
                                                }
                                            }
                                        ?>

                                        <div class="form-group{{ $class_s}} "> 
                                            {!! Form::checkbox('answer['.$question->id.']', $answer->answer, $check_s,['disabled' => 'disabled'] ) !!}
                                            {!! Form::label('answer',$answer->answer) !!}
                                        </div>
                                    @endforeach
                                @endif

                            @endforeach

                        </div>
                    </div>
                    <div class="modal-footer mt30">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
        <br/>
        
        <input type="checkbox"
               class="required_checked_element hidden"
               disabled="disabled"
               name="required_checked_element_test_{{$test->id}}"
               checked="checked"/>
    @else
        <input type="checkbox"
               class="required_checked_element hidden"
               disabled="disabled"
               name="required_checked_element_test_{{$test->id}}"
               data-warning ='Нужно пройти тест "{{$test->name}}"'/>
                              
    @endif
        <div class="text-center">
            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal"
                    data-target="{{'#m_test'.$test->id}}">
                Тест - {{$test->name}} - пройти {{ isset($test->userTestAnswer)?'снова':'' }}
            </button>
        </div>
        <div class="modal fade" 
             id="{{'m_test'.$test->id}}" 
             tabindex="-1" 
             role="dialog"
             aria-labelledby="myModalLabel{{'m_test'.$test->id}}">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel{{'m_test'.$test->id}}"> Тест - {{$test->name}}</h4>
                    </div>
                    <div class="modal-body">
                        <div class="panel">
                            <!--h3>Описание теста:</h3-->
                            {!! $test->description !!}
                            <hr>
                            {!! Form::open(array('route'=>['answer_test_task',$test->id],'method'=>'POST')) !!}
                                @foreach($test->testQuestions as $question)
                                    <h4>Вопрос №{{$loop->iteration}}</h4>
                                    <p>{!! $question->question !!}</p>
                                    @if($question->button_type == 'radio')
                                        @foreach($question->testAnswerQuestion as $answer)
                                            <div class="form-group">
                                                {!! Form::radio('answer['.$question->id.']', $answer->id ) !!}
                                                {!! Form::label('answer',$answer->answer) !!}
                                            </div>
                                        @endforeach
                                    @else
                                        @foreach($question->testAnswerQuestion as $answer)
                                            <div class="form-group">
                                                {!! Form::checkbox('answer['.$question->id.']['.$answer->id.']', 1 ) !!}
                                                {!! Form::label('answer',$answer->answer) !!}
                                            </div>
                                        @endforeach

                                    @endif
                                @endforeach
                                <button type="submit" class="btn btn-blue">Отправить ответы</button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
</div>