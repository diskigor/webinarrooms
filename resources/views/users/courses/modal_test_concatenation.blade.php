{{--  на входе $test   --}} 
    <div class="panel text-center">
        <button type="button" class="btn btn-primary btn-lg" data-toggle="modal"
                data-target="{{'#myM'.$test->id}}">
            {{$test->name}}
        </button>
    </div>
    <div class="modal fade" id="{{'myM'.$test->id}}" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <h3>Краткое описание теста: </h3>
                    {!! $test->description !!}
                    <hr>
                    @if(isset($test->userTestAnswer))
                        <header>Вы успешно прошли данный тест</header>

                        <div class="panel">
                            <span>Результат :</span>{!!  $test->userTestAnswer->descr_answer !!}</div>
                    @else
                        {!! Form::open(array('route'=>['test_concat_answer',$test->id],'method'=>'POST')) !!}
                        <?php $i = 1;?>
                        @foreach($test->testQuestions as $question)
                            <div class="col-md-{{ceil(12/count($question))}}">
                                <h4>Вопрос №{{$i++}}</h4>
                                <p>{!! $question->question !!}</p>
                                <div class="form-group">
                                    @foreach($question->testAnswerQuestion as $answer)
                                        <div class="row">
                                            {!! Form::label('answer',$answer->answer) !!}
                                            {!! Form::radio('answer['.$question->group_number.']['.$answer->number.']', $answer->answer ) !!}
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                        <button type="submit" class="btn btn-blue">Отправить</button>
                        {!! Form::close() !!}
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
