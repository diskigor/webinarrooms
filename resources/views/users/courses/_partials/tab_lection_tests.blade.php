{{--  $lecture   --}}
<!--  вывод тестовых заданий -->
    @foreach($lecture->test_tasks as $test)

        @if($test->type_id === 1) {{-- $test->typeTest->name === 'boolean' --}}
            @include('users.courses._partials._new_modal_test_boolean', ['test' => $test ])
        @endif

        @if($test->type_id === 2) {{-- $test->typeTest->name === 'group' --}}
            @include('users.courses._partials._new_modal_test_group', ['test' => $test ])
        @endif

        @if($test->type_id === 3) {{-- $test->typeTest->name === 'concatenation' --}}
            @include('users.courses._partials._new_modal_test_concatenation', ['test' => $test ])
        @endif

    @endforeach
<!--  конец вывода тестовых заданий -->