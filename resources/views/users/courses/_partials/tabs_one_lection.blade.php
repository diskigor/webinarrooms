{{--  $lecture   --}}
<?php $kostil = Auth::user()->progressCurse($lecture->curse_id)->getTimeInfoLection($lecture->id); ?>
         @if($kostil && $kostil['created'])
            <!--p>Создана запись: {{ $kostil['created'] }}</p-->
         @endif
         @if($kostil && $kostil['available'])
            <!--p>Лекция стала доступна: {{ $kostil['available'] }}</p-->
         @endif
        

  <!-- Nav tabs -->
  <h3>{{ $lecture->name }}</h3>
  <ul id="main-tabs" class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active main-tab tab-item-1">
        <a href="#lection_{{ $lecture->id }}_home" aria-controls="home" role="tab" data-toggle="tab"> 
            @if($kostil && $kostil['completed'])
                {{ $kostil['completed'] }}
            @else
                {{ $kostil['available'] }}
            @endif
        </a>
    </li>
    <li role="presentation" class="main-tab tab-item-2"><a href="#lection_{{ $lecture->id }}_lection"  aria-controls="messages" role="tab" data-toggle="tab">Лекция</a></li>

    @if($lecture->materials->count())
    <li role="presentation" class="main-tab tab-item-3"><a href="#lection_{{ $lecture->id }}_multimedia_panel" aria-controls="messages" role="tab" data-toggle="tab">Дополнительные материалы</a></li>
    @else
        <li role="presentation" class="disabled main-tab tab-item-3"><a>Дополнительные материалы</a></li>
    @endif
    
    @if($lecture->text_tasks->count())
        <li role="presentation" class="main-tab tab-item-4"><a href="#lection_{{ $lecture->id }}_tasks_panel" aria-controls="settings" role="tab" data-toggle="tab">Упражнения</a></li>
    @else
        <li role="presentation" class="disabled main-tab tab-item-4"><a>Упражнения</a></li>
    @endif
    
    @if($lecture->test_tasks->count())
        <li role="presentation"  class="main-tab tab-item-5"><a href="#lection_{{ $lecture->id }}_tests_panel" aria-controls="settings" role="tab" data-toggle="tab">Проверочное задание</a></li>
    @else
        <li role="presentation" class="disabled main-tab tab-item-5"><a>Проверочное задание</a></li>
    @endif
    
  </ul>

  <!-- Tab panes -->
  <div class="tab-content tab-lections-block">
    <div role="tabpanel" 
         class="tab-pane active" 
         style="min-height: 50px;"
         id="lection_{{ $lecture->id }}_home">
        @if($kostil && $kostil['completed'])
        <p style="float: left;" class="icon-lections"><i class="fa fa-check-square" aria-hidden="true"></i></p>
            <p style="float: left;" class="dateago">
                Дней назад:
                <span class="borderedinfo">
                <?php 
                    $now = time(); // or your date as well
                    $your_date = strtotime($kostil['completed']);
                    $datediff = $now - $your_date;
                    echo floor($datediff / (60 * 60 * 24));
                ?>
            </span>
            </p>
            <p style="float: left;" class="balls-lections">MAX Баллов:<br><span class="borderedinfo">{{ $lecture->AllBallsCount }}</span></p>
            <p style="float: left;" class="balls-lections">Получено баллов:<span class="borderedinfo">{{ $lecture->AllBallsCount }}</span></p>
        @else
            <p style="float: left;" class="icon-lections"><i class="fa fa-square" aria-hidden="true"></i></p>
            <p style="float: left;" class="balls-lections">MAX Баллов:<br><span class="borderedinfo">{{ $lecture->AllBallsCount }}</span></p>
        @endif
    </div>
      
    <div role="tabpanel" class="tab-pane" id="lection_{{ $lecture->id }}_lection" style="min-height: 100px;">
        {!! $lecture->langShortD  !!}
        <p class="text-center">
            <a class="js_lection_modal btn btn-blue" href="#lection_{{ $lecture->id }}_lection" data-href="#js_lection_{{$lecture->id}}" role="tab" data-toggle="tab">Изучить</a>
        </p>
    </div>
      
    <div role="tabpanel" 
         class="tab-pane js_downloads_lection_material" 
         id="lection_{{ $lecture->id }}_multimedia_panel" 
         data-download="{{ $lecture->materials->isEmpty()?'':route('get_studies_materials',['id'=>$lecture->id]) }}" >
        {{--@include('users.courses._partials.tab_lection_multimedia',['lecture'=>$lecture])--}}
    </div>
    
    <div role="tabpanel" 
         class="tab-pane js_downloads_lection_material" 
         id="lection_{{ $lecture->id }}_tasks_panel" 
         data-download="{{ $lecture->text_tasks->isEmpty()?'':route('studies_texts.show',['id'=>$lecture->id]) }}">
        {{--@include('users.courses._partials.tab_lection_text_tasks',['lecture'=>$lecture])--}}
    </div>
    
    <div role="tabpanel" 
         class="tab-pane js_downloads_lection_material" 
         id="lection_{{ $lecture->id }}_tests_panel" 
         data-download="{{ $lecture->test_tasks->isEmpty()?'':route('studies_tests.show',['id'=>$lecture->id]) }}">
        {{--@include('users.courses._partials.tab_lection_tests',['lecture'=>$lecture])--}}
    </div>
    
  </div>




<!-- Modal -->
<div class="modal fade" 
     id="js_lection_{{$lecture->id}}" 
     tabindex="-1" 
     role="dialog" 
     data-backdrop="static"
     aria-labelledby="myModalLabel_js_lection_{{$lecture->id}}_lection">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel_js_lection_{{$lecture->id}}_lection">{{ $lecture->name }}</h4>
            </div>
            <div class="modal-body js_lecture_content" style="overflow-y: scroll;">
                {!! $lecture->langDescription !!}
            </div>
            <div class="modal-footer">

                <span class="pull-left js_previous_material_in_lecture"
                      data-lecture_id="{{ $lecture->id }}"
                      style="cursor: pointer;"><i class="fa fa-arrow-left" aria-hidden="true"></i> Назад</span>
                
                <?php 
                $next_m_link        = NULL;
                $next_m_link_text   = NULL;
                if(!$lecture->materials->isEmpty()){
                    $next_m_link        = "lection_" . $lecture->id ."_multimedia_panel";
                    $next_m_link_text   = "Дополнительные материалы";
                }
                elseif(!$lecture->text_tasks->isEmpty()){
                    $next_m_link        = "lection_" . $lecture->id ."_tasks_panel";
                    $next_m_link_text   = "Упражнения";
                }
                elseif(!$lecture->test_tasks->isEmpty()){
                    $next_m_link        = "lection_" . $lecture->id ."_tests_panel";
                    $next_m_link_text   = "Проверочное задание";
                }
                ?>
                 
                @if($next_m_link_text)
                    <span class="js_next_material_in_lecture"
                          style="cursor: pointer;"
                          data-link = "{{ $next_m_link }}"
                          >{{ $next_m_link_text }}&nbsp;<i class="fa fa-arrow-right" aria-hidden="true"></i></span>    
                @endif
                &nbsp;&nbsp;&nbsp;
                    <input type="checkbox"
                           class="required_checked_element pull-left hidden"
                           name="required_checked_element_lecture_{{$lecture->id}}"
                           data-warning ="Нужно прочитать лекцию">

                <button type="button" class="btn btn-default" data-dismiss="modal">Лекция изучена</button>
            </div>
        </div>
    </div>
</div>
<!--  -->


