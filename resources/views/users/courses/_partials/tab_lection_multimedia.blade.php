<!-- Вывод пополнительного материала  -->
@foreach($lecture->materials as $material)

@if($material->material->type === "audio")
<!-- Вывод дополнительного материала аудио -->

<div class="text-center" style="margin: 5px auto;">
    <button type="button" class="btn-blue" data-toggle="modal"
            data-target="#material_audio_{{$material->material->id}}">
        Аудио : {{$material->material->name}}
    </button>
</div>
<!-- Modal -->
<div class="modal fade" id="material_audio_{{$material->material->id}}" tabindex="-1" role="dialog"
     aria-labelledby="ML_material_audio_{{$material->material->id}}">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center"
                    id="ML_material_audio_{{$material->material->id}}"> {{$material->material->name}}</h4>
                    
            </div>
            <div class="modal-body">
                <audio controls controlsList="nodownload" >
                    <source src="{{ getUrlMaterialId($material->material->id) }}"
                            type="audio/mp3">
                </audio>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

@elseif($material->material->type ==="text")
<!-- Вывод дополнительного материала текстового формата -->
<!-- Button trigger modal -->
<div class="text-center" style="margin: 5px auto;">
    <button type="button" class="btn-blue" data-toggle="modal"
            data-target="#material_text_{{$material->material->id}}">
        {{$material->material->name}}
    </button>
</div>
<!-- Modal -->
<div class="modal fade" id="material_text_{{$material->material->id}}" tabindex="-1" role="dialog"
     aria-labelledby="ML_material_text_{{$material->material->id}}">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center"
                    id="ML_material_text_{{$material->material->id}}"> {{$material->material->name}}</h4>
                    
            </div>
            <div class="modal-body">
                <p>{!! $material->material->content !!}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

@elseif($material->material->type ==="pdf")
<!-- Вывод дополнительного материала pdf формата -->
    <div class="text-center" style="margin: 5px auto;">
        <!-- Button trigger modal -->
        <button type="button" class="btn-blue" data-toggle="modal"
                data-target="#material_pdf_{{ $material->material->id }}">
            {{$material->material->name}}
        </button>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="material_pdf_{{ $material->material->id }}" tabindex="-1" role="dialog"
         aria-labelledby="ML_material_pdf_{{ $material->material->id }}">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="ML_material_pdf_{{ $material->material->id }}">{{$material->material->name}}</h4>
                    
                </div>
                <div class="modal-body">
                    <a href="{{ getUrlMaterialId($material->material->id) }}" target="_blank">Открыть PDF</a>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>

@elseif($material->material->type ==="video_link")
    <div class="text-center" style="margin: 5px auto;">
        <!-- Вывод дополнительного материала видео -->
        <button type="button" class="btn-blue" data-toggle="modal"
                data-target="#material_video_link_{{ $material->material->id }}">
            Видео : {{$material->material->name}}
        </button>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="material_video_link_{{ $material->material->id }}" tabindex="-1" role="dialog"
         aria-labelledby="ML_material_video_link_{{ $material->material->id }}">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center"
                        id="ML_material_video_link_{{ $material->material->id }}"> {{$material->material->name}}</h4>
                        
                </div>
                <div class="modal-body">
                    <video controls controlsList="nodownload" width="560" height="190">
                        <source src="{{ getUrlMaterialId($material->material->id) }}">
                    </video>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>

@elseif($material->material->type ==="archives")
<!-- Вывод дополнительного материала pdf формата -->

    <div class="text-center" style="margin: 5px auto;">
        <!-- Button trigger modal -->
        <button type="button" class="btn-blue" data-toggle="modal"
                data-target="#material_archives_{{ $material->material->id }}">
            {{$material->material->name}}
        </button>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="material_archives_{{ $material->material->id }}" tabindex="-1" role="dialog"
         aria-labelledby="ML_material_archives_{{ $material->material->id }}">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="ML_material_archives_{{ $material->material->id }}">{{$material->material->name}}</h4>
                    
                </div>
                <div class="modal-body">
                    <a href="{{ getUrlMaterialId($material->material->id) }}" target="_blank">Скачать архив</a>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
    
@endif

@endforeach
<!-- Конец вывода дополнительного материала -->