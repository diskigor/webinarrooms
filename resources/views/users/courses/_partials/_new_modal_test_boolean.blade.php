{{--  на входе $test   --}}              

<div class="">
    <div class="text-center">
            <h3>{{--$test->name--}}</h3>
            <button type="button" class="btn-blue" data-toggle="modal"
                    data-target="#lection_bool_test_{{ $test->id }}_current">
                Пройти тест {{ isset($test->userTestAnswer)?'снова':'' }}
            </button>
            <p></p>
        </div>
        <div class="modal fade" 
             id="lection_bool_test_{{ $test->id }}_current" 
             tabindex="-1" 
             role="dialog"
             aria-labelledby="ML_lection_bool_test_{{ $test->id }}_current">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-center" id="ML_lection_bool_test_{{ $test->id }}_current"> {{$test->name}}</h4>
                    </div>
                    <div class="modal-body">
                        <div class="panel">
                            <!--h3>Описание теста:</h3-->
                            {!! $test->description !!}
                            <hr>
                            {!! Form::open(array('route'=>['studies_tests.store'],'method'=>'POST','class'=>'js_answer_test')) !!}
                            <input type="hidden" name="test_id" value="{{ $test->id }}"/>
                                @foreach($test->testQuestions as $question)
                                    <h4>Вопрос №{{$loop->iteration}}</h4>
                                    <h5>{!! $question->question !!}</h5>
                                    @if($question->button_type == 'radio')
                                        @foreach($question->testAnswerQuestion as $answer)
                                            <div class="form-group">
                                                {!! Form::radio('answer['.$question->id.']', $answer->id ) !!}
                                                {!! Form::label('answer',$answer->answer) !!}
                                            </div>
                                        @endforeach
                                    @else
                                        @foreach($question->testAnswerQuestion as $answer)
                                            <div class="form-group">
                                                {!! Form::checkbox('answer['.$question->id.']['.$answer->id.']', 1 ) !!}
                                                {!! Form::label('answer',$answer->answer) !!}
                                            </div>
                                        @endforeach

                                    @endif
                                @endforeach
                                <div class="text-center mt30">
                                    <button type="submit" class="btn btn-blue">Отправить ответы</button>
                                </div>
                                
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
    @if(isset($test->userTestAnswer) && isset($test->userTestAnswer->jsonAnswer))
        <div class="text-center">
            <!--h3>{{--$test->name--}}</h3-->
            <button type="button" 
                    class="btn-blue-invers" 
                    data-toggle="modal"
                    title="Предыдущие результаты"
                    data-target="#lection_bool_test_{{ $test->id }}_previous">
                    Результаты теста
            </button>
        </div>
        <div class="modal fade" 
             id="lection_bool_test_{{ $test->id }}_previous" 
             tabindex="-1" 
             role="dialog"
             aria-labelledby="ML_lection_bool_test_{{ $test->id }}_previous">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="ML_lection_bool_test_{{ $test->id }}_previous">Вы прошли данный тест</h4>
                    </div>
                    <div class="modal-body">
                        <div class="panel">
                            <h3 class="text-center">{{$test->name}}</h3>

                            <?php $us_answer = $test->userTestAnswer->jsonAnswer; ?>
                            
                            @if(isset($us_answer['description']) && $us_answer['description'])
                                <hr>
                                <h4 class="text-center">Результаты теста</h4>
                                <div class="form-group bg-warning">
                                    {!! $us_answer['description'] !!}
                                </div>
                                <hr>
                            @endif
                            
                            @foreach($test->testQuestions as $question)
                                <h4>Вопрос №{{$loop->iteration}}</h4>
                                <h5>{!! $question->question !!}</h5>

                                @if($question->button_type == 'radio')
                                    @foreach($question->testAnswerQuestion as $answer)
                                        <?php 
                                            $class_s='';
                                            $check_s=false;
                                            if (isset($us_answer[$question->id]) && isset($us_answer[$question->id]['answer_id']) && $us_answer[$question->id]['answer_id'] == $answer->id ) {
                                                $class_s=' __id_'.$answer->id; 
                                                if(!isset($us_answer['description']) || isset($us_answer['description']) && !$us_answer['description']){
                                                    $class_s=' bg-danger __id_'.$answer->id; 
                                                }
                                                if (isset($us_answer[$question->id]['balls']) 
                                                        && $us_answer[$question->id]['balls']>0
                                                        && (!isset($us_answer['description']) || isset($us_answer['description']) && !$us_answer['description'])
                                                        ) { $class_s=' bg-success __id_'.$answer->id; }

                                                $check_s=true; 
                                            }
                                        ?>

                                        <div class="form-group{{ $class_s}}"> 

                                            {!! Form::radio('answer['.$question->id.']', $answer->answer, $check_s,['disabled' => 'disabled'] ) !!}
                                            {!! Form::label('answer',$answer->answer) !!}
                                        </div>
                                    @endforeach
                                @else
                                    @foreach($question->testAnswerQuestion as $answer)
                                        <?php 
                                            $class_s=' b';
                                            $check_s=false;
                                            if (isset($us_answer[$question->id]) && is_array($us_answer[$question->id]) ) {

                                                if (isset($us_answer[$question->id][$answer->id])) {
                                                    $class_s=' __id_'.$answer->id; 
                                                    if(!isset($us_answer['description']) || isset($us_answer['description']) && !$us_answer['description']){
                                                        $class_s=' bg-danger __id_'.$answer->id; 
                                                    }
                                                    if (isset($us_answer[$question->id][$answer->id]['balls'])
                                                            && $us_answer[$question->id][$answer->id]['balls']>0
                                                            && (!isset($us_answer['description']) || isset($us_answer['description']) && !$us_answer['description']))
                                                    { $class_s=' bg-success __id_'.$answer->id;  }
                                                    $check_s=true; 
                                                }
                                            }
                                        ?>

                                        <div class="form-group{{ $class_s}} "> 
                                            {!! Form::checkbox('answer['.$question->id.']', $answer->answer, $check_s,['disabled' => 'disabled'] ) !!}
                                            {!! Form::label('answer',$answer->answer) !!}
                                        </div>
                                    @endforeach
                                @endif

                            @endforeach

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
        <br/>                 
    @endif
    <hr>     
</div>