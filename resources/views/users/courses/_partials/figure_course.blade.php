{{-- на входе $course , $route_type --}}
@if( Auth::user()->checkBuyCourse($course->id) )
    <div class="col-md-11 col-md-offset-1 course-list-item">
        <div class="col-md-4">
            <h3>{{ mb_strimwidth($course->trans_curse->first()->name_trans,0,60,'...') }}</h3>
            <img class="image" src="{{ $course->imagex }}">
            <a class="btn-coursdetails" href="{{ $route_type?route($route_type.'.show',$course->id):'' }}" title="{{ trans('curses.goto_course') }}">
                @if($course->currentProcent(Auth::id())== 0)
                    Начать
                @endif
                @if($course->currentProcent(Auth::id())== 100)
                    Повторить
                @endif
                @if(0<$course->currentProcent(Auth::id()) && $course->currentProcent(Auth::id())<100)
                    Продолжить
                @endif
                 обучение
            </a>
        </div>
       
        <div class="col-md-8 col-sm-8 col-xs-12 block-csd">
            <p class="cours-short-desc">{{mb_strimwidth($course->trans_curse->first()->short_desc_trans,0,300,'...')}}</p>
            <span class="col-md-6 col-sm-6 col-xs-12 long-term">Продолжительность дней: <b>{{ $course->lectures()->count() }}</b></span>
            <span class="col-md-6 col-sm-6 col-xs-12 max-bonus">Максимально баллов за курс: <b>{{ $course->lecturesBallsCount }}</b></span>
            <span class="col-md-6 col-sm-6 col-xs-12 cours-author">Автор курса: <b>{{ $course->author?$course->author->fullFIO:'' }}</b></span>
            <span class="col-md-6 col-sm-6 col-xs-12 cours-buy">Приобретено студентами раз: <b>{{ $course->getUserBuy() }}</b></span>
        </div>
        <i class="fa fa-angle-double-down" aria-hidden="true"></i>
    </div>
@else
    <div class="col-md-11 col-md-offset-1 course-list-item buy-main-courses-block-child">
        <div class="col-md-4">
            <h3>{{mb_strimwidth($course->trans_curse->first()->name_trans,0,60,'...')}}</h3>
            <img class="image" src="{{ $course->imagex }}">
            @if(Auth::user()->checkEntrance() && !$course->xclosed)
                {!! Form::open(array('route'=>['buy_curses',$course->id],'method'=>'GET'))!!}
                <button type="submit" title="заказать курс"
                        class="btn-coursdetails act_buyCours">
                    <i class="fa fa-shopping-cart"></i>
                    Приобрести курс за <b>{{ $course->groupPrice(Auth::user()->group_id)->price }} КЭЛ</b>
                </button>
                {!! Form::close() !!}
            @elseif(!Auth::user()->checkEntrance())
            <button class="disabled"><i class="fa fa-lock" aria-hidden="true"></i></button>
            @elseif($course->xclosed)
            <button class="disabled"><i class="fa fa-lock" title="Нужно изучить курс: {{ $course->xname }}"aria-hidden="true"></i></button>
            @endif
        </div>
        <div class="col-md-8 col-sm-8 col-xs-12 block-csd">
            <p class="cours-short-desc">{{mb_strimwidth($course->trans_curse->first()->short_desc_trans,0,300,'...')}}</p>
            <span class="col-md-6 col-sm-6 col-xs-12 long-term">Продолжительность дней: <b>{{ $course->lectures()->count() }}</b></span>
            <span class="col-md-6 col-sm-6 col-xs-12 max-bonus">Максимально баллов за курс: <b>{{ $course->lecturesBallsCount }}</b></span>
            <span class="col-md-6 col-sm-6 col-xs-12 cours-author">Ваша скидка: <b>{{ $course->discount }} %</b></span>
            <span class="col-md-6 col-sm-6 col-xs-12 cours-buy">
                <a href="#{{$course->slug}}" class="" title="смотреть программу" data-toggle="modal" data-target="{{'#modal-'.$course->id}}">
                 Описание курса
                </a>
            </span>
        </div>
        <i class="fa fa-angle-double-down" aria-hidden="true"></i>
    </div>




<!-- Modal лекции курса -->
<div class="modal fade" id="{{'modal_buy_'.$course->id}}" tabindex="-1"
     role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close"><span
                            aria-hidden="true">&times;</span>
                </button>
                <div class="image"
                     style="background-image: url('{{ $course->imagex }}')"></div>
                <h4 class="modal-title">{{ $course->trans_curse->first()->name_trans }}</h4>
            </div>
            <div class="modal-body">
                {!! $course->trans_curse->first()->description_trans !!}
                <div>
                    @if(count($course->lectures)!=0)
                        <span>В данный курс входят следующие лекции :</span>
                        @foreach($course->lectures as $lecture)
                            @if($lecture->sdoLectureTrans())
                                <p>{{$lecture->sdoLectureTrans()->name_trans}}</p>
                            @endif
                        @endforeach
                    @else
                        <span>В данном курсе нет лекци</span>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal лекции курса -->
<!-- Modal  описание курса-->
<div class="modal fade" id="{{'modal-'.$course->id}}" tabindex="-1"
     role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="image"
                     style="background-image: url('{{ $course->imagex }}')"></div>
                <h4 class="modal-title">{{ $course->trans_curse->first()->name_trans }}</h4>
            </div>
            <div class="modal-body">
                {!!  $course->trans_curse->first()->description_trans !!}
            </div>
            <div class="modal-footer"  style="text-align: center;">
                @if(Auth::user()->checkEntrance() && !$course->xclosed)
                    {!! Form::open(array('route'=>['buy_curses',$course->id],'method'=>'GET'))!!}
                    <button type="submit" title="заказать курс"
                            class="btn btn-blue act_buyCours">
                        <i class="fa fa-shopping-cart"></i> Включить этот курс в Учебный план
                    </button>
                    {!! Form::close() !!}
                    <br/>
                @endif
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal  описание курса-->
@endif
