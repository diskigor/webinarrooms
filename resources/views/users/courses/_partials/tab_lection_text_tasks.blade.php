<!-- Вывод  -->
@foreach($lecture->text_tasks as $text_task)
<div class="text-center texttaskbtn">
    <h3>{{ $text_task->name }}</h3>
    <button type="button" 
            class="btn-blue {{ is_null($text_task->answerUserText)?'btn-primary':''}}" 
            data-toggle="modal"
            data-target="#lection_text_tasks_{{ $text_task->id }}">
        Пройти упражнение
    </button>
</div>

<div class="modal fade" id="lection_text_tasks_{{ $text_task->id }}" tabindex="-1" role="dialog"
     aria-labelledby="ML_lection_text_tasks_{{ $text_task->id }}">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" 
                        class="close" 
                        data-dismiss="modal" 
                        aria-label="Close"><span
                        aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">{{ $text_task->name }}</h4>
            </div>
            <div class="modal-body">
                <p>{!! $text_task->description !!}</p>
                @if(is_null($text_task->answerUserText))
                {!! Form::open(array('route'=>['studies_texts.store'],'method'=>'POST','class'=>'answer_text_tasks')) !!}
                <input type="hidden" name='lecture_id' value="{{ $lecture->id }}"/>
                <input type="hidden" name='text_task_id' value="{{ $text_task->id }}"/>
                {!! Form::label('answer_text_tasks','Ответ :',array_merge(['class'=>'control-label'])) !!}
                <div class="no-editor">
                    {!! Form::textarea('answer_text_tasks',null,array_merge(['class'=>'form-control'])) !!}
                </div>
                {!! Form::submit('Ответить',array_merge(['class'=>'btn btn-success'])) !!}
                {!! Form::close() !!}
                @else
                <div class="panel">
                    <h3>Ваш ответ: </h3>
                    <p>{!! $text_task->answerUserText->answer !!}</p>
                </div>
                <script>
                    updateCurrentLection('text_task_' + {{ $text_task-> id }}); //global fn
                </script>
                @endif



            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
@endforeach
<!-- Вывод закончен -->
