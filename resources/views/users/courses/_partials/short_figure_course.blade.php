{{-- на входе $course , $route_type --}}
@if( Auth::user()->checkBuyCourse($course->id) )
<div class="col-md-12 short-course-figure">
    <div>
        <span class="short-course-name">{{ mb_strimwidth($course->trans_curse->first()->name_trans,0,40,'...') }}</span>
        <span class="short-course-ball">Баллов: <b>{{ $course->lecturesBallsCount }}</b></span>
        <a class="short-course-link" href="{{ $route_type?route($route_type.'.show',$course->id):'' }}" title="{{ trans('curses.goto_course') }}">
            @if($course->currentProcent(Auth::id())== 0)
                Начать
            @endif
            @if($course->currentProcent(Auth::id())== 100)
                Повторить
            @endif
            @if(0<$course->currentProcent(Auth::id()) && $course->currentProcent(Auth::id())<100)
                Продолжить
            @endif
             обучение
        </a>
    </div>
</div>
@else
<div class="col-md-12 short-course-figure">
    <div>
        <span class="short-course-name">{{ mb_strimwidth($course->trans_curse->first()->name_trans,0,50,'...') }}</span>
        <span class="short-course-ball">Баллов: <b>{{ $course->lecturesBallsCount }}</b></span>
        @if(Auth::user()->checkEntrance() && !$course->xclosed)
            {!! Form::open(array('route'=>['buy_curses',$course->id],'method'=>'GET'))!!}
            <button type="submit" class="short-course-link act_buyCours">Приобрести за {{ $course->groupPrice(Auth::user()->group_id)->price }} КЭЛ</button>
            {!! Form::close() !!}
        @elseif(!Auth::user()->checkEntrance())
            <button class="short-course-linkdes"><i class="fa fa-lock" aria-hidden="true"></i></button>
        @elseif($course->xclosed)
            <button class="short-course-linkdes"><i class="fa fa-lock" title="Нужно изучить курс: {{ $course->xname }}"aria-hidden="true"></i></button>
        @endif
    </div>
</div>
@endif
