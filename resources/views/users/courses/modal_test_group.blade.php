{{--  на входе $test  --}}

<div class="panel">
    @if(isset($test->userTestAnswer) && isset($test->userTestAnswer->jsonAnswer))
        <div class="text-center">
            <button type="button" 
                    class="btn btn-success btn-lg center" 
                    data-toggle="modal"
                    title="Предыдущие результаты"
                    data-target="{{'#my_group_test_res_'.$test->id}}">
                Тест - {{$test->name}} - результаты
            </button>
        </div>
        <div class="modal fade" id="{{'my_group_test_res_'.$test->id}}" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel{{'my_group_test_res_'.$test->id}}">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-center" id="myModalLabel{{'my_group_test_res_'.$test->id}}">Вы прошли данный тест. {{$test->userTestAnswer->updated_at}}</h4>
                    </div>
                    <div class="modal-body">
                        <div class="panel">
                            <h3 class="text-center">Тест - {{$test->name}}</h3>
                            <?php $us_answer = $test->userTestAnswer->jsonAnswer; 
                                  $us_description = isset($us_answer['description'])?$us_answer['description']:null;
                            
                            ?>
                            @if($us_description)
                                <hr>
                                <h4 class="text-center">Результаты теста</h4>
                                <div class="form-group bg-warning">
                                    {!! $us_answer['description'] !!}
                                </div>
                                <hr>
                            @endif
                            

                            @foreach($test->questions_group as $question)
                                <div class="row">
                                    @foreach($question as $quest)
                                        <div class="col-md-{{ceil(12/count($question))}}">
                                            {!! $quest->question  !!}
                                            @foreach($quest->testAnswerQuestion as $answer)
                                            
                                        <?php 
                                            $class_s='';
                                            $check_s='';
                                            if (isset($us_answer[$quest->group_number]) 
                                                    && isset($us_answer[$quest->group_number][$quest->number]) 
                                                    && isset($us_answer[$quest->group_number][$quest->number][$answer->id])
                                                    ) {
//                                                $class_s=' __id_'.$answer->id; 
//                                                if(!$us_description 
//                                                        && isset($us_answer[$quest->group_number][$quest->number][$answer->id]['balls'])
//                                                        && $us_answer[$quest->group_number][$quest->number][$answer->id]['balls']==0){
//                                                    $class_s=' bg-danger __id_'.$answer->id; 
//                                                } else if($us_description ) {
//                                                    $class_s=' bg-success __id_'.$answer->id; 
//                                                }

                                                $check_s='checked="checked"'; 
                                            }
                                        ?>
                                            
                                            
                                                <div class="row{{ $class_s}}">
                                                    <div class="col-xs-1">
                                                        <strong>{{$answer->number}}) </strong>
                                                    </div>
                                                    <div class="col-xs-1">
                                                        @if($quest->button_type == 'radio')
                                                        <input type="checkbox"
                                                               disabled="disabled"
                                                               {{ $check_s }}>
                                                        @else
                                                        <input type="checkbox"
                                                               disabled="disabled"
                                                               {{ $check_s }}>
                                                        @endif
                                                    </div>
                                                    <div class="col-xs-9">
                                                        {{$answer->answer}}
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                </div>
                                <hr>
                            @endforeach


                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
        <br/>

        <input type="checkbox"
               class="required_checked_element hidden"
               disabled="disabled"
               name="required_checked_element_test_{{$test->id}}"
               checked="checked"/>
   
    @else
    <input type="checkbox"
           class="required_checked_element hidden"
           disabled="disabled"
           name="required_checked_element_test_{{$test->id}}"
           data-warning ='Нужно пройти тест "{{$test->name}}"'/>
                              
    @endif
        <div class="text-center">
            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal"
                    data-target="{{'#my_group_test_new_'.$test->id}}">
                Тест - {{$test->name}} - пройти {{ isset($test->userTestAnswer)?'снова':'' }}
            </button>
        </div>
        <div class="modal fade" id="{{'my_group_test_new_'.$test->id}}" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel{{'my_group_test_new_'.$test->id}}">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <h3 class="text-center">{{$test->name}}</h3>
                        <div class="panel">
                            <!--h3>Описание теста:</h3-->
                            {!! $test->description !!}
                            <hr>
                            @if($test->questions_group)
                            <form action="{{route('test_group_answer',$test->id)}}" method="POST">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                @foreach($test->questions_group as $question)
                                    <div class="row">
                                        @foreach($question as $quest)
                                            <div class="col-md-{{ceil(12/count($question))}}">
                                                {!! $quest->question  !!}
                                                @foreach($quest->testAnswerQuestion as $answer)
                                                    <div class="row">
                                                        <div class="col-xs-1">
                                                            <strong>{{$answer->number}}) </strong>
                                                        </div>
                                                        <div class="col-xs-1">
                                                            @if($quest->button_type == 'radio33')
                                                                <input type="radio"
                                                                       name="group_answer[{{$quest->group_number}}][{{$quest->number}}]"
                                                                       value='{"{{$answer->number}}":"{{$answer->id}}"}'>
                                                            @else
                                                                <input type="checkbox"
                                                                   name="group_answer[{{$quest->group_number}}][{{$quest->number}}][{{$answer->id}}]">
                                                            @endif
                                                        </div>
                                                        <div class="col-xs-9">
                                                            {{$answer->answer}}
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endforeach
                                    </div>
                                    <hr>
                                @endforeach
                                <button type="submit" class="btn btn-blue">Отправить ответы</button>
                            </form>
                            @else
                                <h4>В тесте №-{{ $test->id }} нет вопросов</h4>
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
    
    
    

</div>




