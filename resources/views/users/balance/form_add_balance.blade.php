{{--  $need_summ, $need_text  --}}
<form class="form-inline form_add_balance">
    <div class="form-group">
        <label class="sr-only" for="exampleInputAmount">Введите сумму <i class="fa fa-eur" aria-hidden="true"></i></label>
        <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-eur" aria-hidden="true"></i></div>
            <input type="number" value="{{$need_summ}}" name="summa" minlength="0" min="{{$need_summ}}" id="exampleInputAmount" class="form-control"  required>
        </div>
    </div>
    <button type="submit" class="btn-blue btn-pay">Пополнить</button>
    @if($need_text)
        <p style="color: red">* {!! $need_text !!}</p>
    @endif
</form>