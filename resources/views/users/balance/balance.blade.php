{{--  $need_sum, $need_text   --}}
@extends('layouts.admin')
@section('content')
<link rel="stylesheet" href="{{ asset('css/datatables.css') }}">
<?php $title_page = 'Пополнение счета и Баланс'; ?>

<div class="col-md-12 user-balance-page">
    <div class="row">
        <div class="col-md-12 text-center">
            <h3>Операции с балансом</h3>
            <hr>
        </div>
        <div class="col-md-5 balance-block-one">
            <h4>Ваш баланс</h4>
            <form class="form-inline">
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" id="balanceview" placeholder="{{Auth::user()->balance}}" readonly>
                        <div class="input-group-addon">КЭЛ</div>
                    </div>
                </div>
            </form>
            <br><label for="balanceview">*1 КЭЛ = 1 EUR</label><br>
            <a class="history-transactions" href="#" data-toggle="modal" data-target="#modal_history_transaction_from_user">История транзакций</a>
        </div>
        <div class="col-md-7">
            <h4>Пополнить баланс</h4>
            <div id="body_user_add_balance" style="max-width: 615px; margin: 0 auto;"></div>
            <div class="separator"></div>
            <div class="text-center">
                @foreach([50,100,200,300] as $price)
                @include('users.balance.button_fix_add_balance',['fix_summa'=>$price])
                @endforeach
            </div>
            
            @if(!empty($bay_all_curses))
                {!! $bay_all_curses !!}
            @endif

            <h4>Другие методы оплаты</h4>
            <button type="button" class="btn btn-rekvisiti" data-toggle="modal" data-target="#bank-card">
                Реквизиты банковской карты
            </button>
            
            <button type="button" class="btn btn-rekvisiti" data-toggle="modal" data-target="#eu-bank">
                Реквизиты р/с в ЕС
            </button>
            
            <button type="button" class="btn btn-rekvisiti" data-toggle="modal" data-target="#money-transfer">
                Реквизиты денежные переводы
            </button>
        </div>
        <i class="fas fa-info-circle" aria-hidden="true" data-toggle-finance="tooltipfinance"></i>
    </div>
</div>
<div class="col-md-12 user-balance-page">

    <div class="row">
        <div class="col-md-12 text-center">
            <h3>Операции с баллами</h3>
            <hr>
        </div>
        <div class="col-md-5 balance-block-fourth">
            <h4>Ваши баллы</h4>
            <form class="form-inline">
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" id="levelpoint" placeholder="{{ Auth::user()->level_point}}">
                        <div class="input-group-addon">Баллов</div>
                    </div>
                </div>
            </form>
            <br><label for="levelpoint">*1 Балл = 0.1 EUR</label><br>
            <a class="history-transactions" href="#" data-toggle="modal" data-target="#modal_history_balls_from_user">История трансферов</a>
        </div>
        <div class="col-md-7">
            <h4>Конвертация баллов</h4>
            @include('users.converters.ball_to_kel.index')
            <!--h4>Получить скидку за баллы</h4-->
            <!--form id="" class="">
            <div class="form-group skidka-form">
                <select class="form-control">
                    <option selected disabled="" value="Выберите скидку">Выберите скидку</option>
                    <option>Скидка на семинар = 300 баллов</option>
                    <option>Скидка на вебинар = 200 баллов</option>
                    <option>Скидка на аудио запись = 100 баллов</option>
                </select>
                <button class="btn btn-blue btn-skidka" id="js_form_converter_ball_to_kell_success">Обменять</button>
            </div>
            <div class="form-group skidka-form-copy">
                <input class="form-control" id="code" type="text" readonly="" value="123456789456123" />
                <input type="button"  class="btn btn-blue btn-skidka" id="copy-skidka" value="Скопировать">
            </div>
        </form-->
        </div>
        <i class="fas fa-info-circle" aria-hidden="true" data-toggle-ball="tooltipball"></i>
    </div>
</div>

<div class="modal fade" id="modal_history_transaction_from_user" tabindex="-1" role="dialog"
     aria-labelledby="modal_history_transaction_from_user_Label">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3>История транзакций</h3>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                @include('users.balance.form_history_transactions')
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_history_balls_from_user" tabindex="-1" role="dialog"
     aria-labelledby="modal_history_balls_from_user_Label">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3>История баллов</h3>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                @include('users.balance.form_history_balls')
            </div>
        </div>
    </div>
</div>


@endsection

@push('stack_scripts')
<script src="{{ asset('js/datatables/datatables.js') }}"></script>
<script src="{{ asset('js/datatables/dataTables.buttons.js') }}"></script>
<script src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script>


<script>
$(document).ready(function ($) {
    //$('#body_user_add_balance').load('{{ route("get_add_balance_form",["need_sum"=>$need_sum,"need_text"=>$need_text])}}');
    var mypar = {
        'need_sum': '{{$need_sum}}',
        'need_text': '{{$need_text}}'
    };

    $.ajax({
        type: "GET",
        url: "{{ route('get_add_balance_form') }}",
        data: $.param(mypar),
        success: function (msg) {
            if (msg.errors !== undefined) {
                swal(msg.errors);
                return;
            }
            if (msg.forms !== undefined) {
                //$(msg.forms).submit();
                return;
            }
            $('#body_user_add_balance').html(msg);
        }
    });

});

$(document).on('submit', '.form_add_balance', function (e) {
    e.preventDefault();
    $(document).remove('form#interkassa_payment');
    var dataforma = $(this).serialize() + '&action=get_payment_form';
    $.ajax({
        type: "GET",
        url: "{{ route('get_add_balance_form') }}",
        data: dataforma,
        success: function (msg) {
            if (msg.errors !== undefined) {
                swal(msg.errors);
                return;
            }
            if (msg.forms !== undefined) {
                //console.log(msg.forms);
                //$(msg.forms).submit();
                $(document.body).append(msg.forms);
                $('form#interkassa_payment').submit();
                return;
            }
            $('#body_user_add_balance').html(msg);
        }
    });
});
//кнопка копировать
 $(function() {
    $('#copy-skidka').click(function() {
        $('#code')[0].select(); 
        document.execCommand('copy');
        $('#code').append(' ');
        $('#code').val().slice(0, -1);
        return swal({
            title: "Код скопирован в буфер обмена!", 
            html:  true,  
            text:  "Ваш код: " + $('#code').val(),
            confirmButtonText: "Код сохранен", 
        });
    });
  });
</script>
@endpush

