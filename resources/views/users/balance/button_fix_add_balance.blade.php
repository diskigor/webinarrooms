{{--  $fix_summa  --}}
<form class="inline-block form_add_balance">
        <input type="hidden" 
               value="{{$fix_summa}}" 
               name="summa" 
               required>
        <button type="submit" class="btn-blue-invers">Пополнить <span class="static-summ">{{$fix_summa}}</span> <i class="fa fa-eur" aria-hidden="true"></i></button>
</form>