@if((Auth::user()->isCadet() or Auth::user()->isСurator()) &&  $bay_all_curses->route)
    <div class="row alert alert-warning text-center">
                {!! Form::open(array('url'=>$bay_all_curses->route,'method'=>'POST'))!!}
                @if($bay_all_curses->text)
                    {!! Form::submit($bay_all_curses->text,array_merge(['class'=>'btn btn-success','onclick'=>'return checkByu()'])) !!}
                @endif
                {!! Form::close() !!}
    </div>
@endif