<table class="table table-responsive"  id="user-history-balls">
    <thead>
        <tr>
            <th class="hidden">Id</th>
            <th>Сумма баллов</th>
            <th>Описание</th>
            <th>Дата</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th class="hidden"></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
    </tfoot>
</table>
@push('stack_scripts')
    <script>
      $(document).ready(function() {

            var table = $('#user-history-balls').DataTable({
                processing: true,
                serverSide: true,
                displayLength: 10,
                order: [[ 3, "desc" ]],
                ajax: "{!! route('get_history_user_balls') !!}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'value', name: 'value'},
                    {data: 'description', name: 'description'},
                    {data: 'created_at', name: 'created_at'},
                ],
                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).appendTo($(column.footer()).empty())
                        .on('change', function () {
                            column.search($(this).val(), false, false, true).draw();
                        });
                    });
                },
                language: {
                    processing: "Поиск в базе",
                    info:       "показано с _START_ по _END_ записей из _TOTAL_",
                    infoEmpty:  "нет баллов",
                    infoFiltered: "(фильтр по _MAX_ записях)",
                    lengthMenu: "По _MENU_ записей",
                    search: "Поиск:",
                    "paginate": {
                        "first": "Первая",
                        "previous": "Предыдущая",
                        "next": "Следующая",
                        "last": "Последняя"
                      },
                },
            });
            
            new $.fn.dataTable.Buttons( table, {
                buttons: [
                    {
                        extend: 'print',
                        text: 'Печать',

                    }
                ]
            } );
            
           table.buttons().container().prependTo( $('.col-sm-6:eq(0)', table.table().container() ) );

        });

    </script>
@endpush