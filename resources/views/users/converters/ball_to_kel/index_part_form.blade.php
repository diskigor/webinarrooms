
<form id="js_form_converter_ball_to_kell" class="form-inline">
  <div class="form-group">
    
    <input class="form-control js_convert_balls" id="converts-ball" name="convert_balls" type="number" min="0" max="{{ Auth::user()->level_point}}" value="0" >
    <label for="converts-ball">Введите Ваши баллы</label>
  </div>
  <div class="form-group">
    
    <input class="form-control disabled" disabled="disabled" type="text" value="0" id="view_convert_kel">
    <label for="kel-convert">Получаете КЭЛ</label>
  </div>
  <button class="btn btn-blue btn-convert" id="js_form_converter_ball_to_kell_success">Обменять</button>
</form>
