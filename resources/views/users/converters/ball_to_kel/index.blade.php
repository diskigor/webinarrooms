<div class="converter_ball_to_kell" id="js_converter_ball_to_kell">
    @include('users.converters.ball_to_kel.index_part_form')
</div>

@push('stack_scripts')
    <script type="text/javascript">
        $('#js_converter_ball_to_kell').on('change','.js_convert_balls', function(e){
            e.preventDefault();
            view_calc_ball_to_kel(this.value);
        });
        
        $('#js_converter_ball_to_kell').on('keyup','.js_convert_balls', function(e){
            e.preventDefault();
            view_calc_ball_to_kel(this.value);
        });
        
        function view_calc_ball_to_kel(balls){
            if (balls==undefined || balls=='') {balls = 0;}
            var xn = parseInt(balls);
            if (xn>{{ Auth::user()->level_point}}) {
                $('.js_convert_balls').val({{ Auth::user()->level_point}});
                $('.js_convert_balls').change();
                return;
            }
            if(xn<0) {  xn=0; }
            $('#view_convert_kel').val((xn*{!! config('vlavlat.balls_to_kel_convert_curse') !!}).toFixed(2));
        }
        
        $('#js_converter_ball_to_kell').on('click','#js_form_converter_ball_to_kell_success', function(e){
            e.preventDefault();
            var forma = $('#js_form_converter_ball_to_kell').serialize();
            $.ajax({
                type: "POST",
                url: "{{ route('form_converter_ball_to_kell') }}",
                data: forma,
                success: function (msg) {
                    console.log(msg);
                    if (msg.errors !== undefined) {
                        swal(msg.errors);
                        view_calc_ball_to_kel(0);
                        return;
                    }
                    if (msg.forms !== undefined) {
                        $('#js_converter_ball_to_kell').html(msg.forms);
                        swal('Операция успешна');
                        setTimeout(function() { window.location.reload(); }, 1000);
                        
                        return;
                    }
                }
            });
            
            
        });
        
        
    </script>
@endpush

