@extends('layouts.admin')
@section('content')
    <div id="content">
        <div class="container">
            <header class="panel big-header text-center">{{trans('home.allnews')}}</header>
            <div class="row">
                @foreach($news as $article)
                    <figure class="col-md-4">
                        <div class="post_news">
                            <a class="post_news_img" href="{{route('unews.show',$article->id)}}">
                                <img src="{{asset($article->image_link)}}" class="image">
                            </a>
                            <figcaption class="panel">
                                <header><strong>{{str_limit($article->news_trans[0]->title,60)}}</strong></header>
                                <div class="newsdescr">{!! str_limit($article->news_trans[0]->article,100) !!}</div>
                                <p class="date"><small>{{trans('news.created_news')}}: {{date('d.m.Y', strtotime($article->created_at))}}</small></p>
                                <div class="text-center">
                                    <a href="{{route('unews.show',$article->id)}}" class="btn btn-blue">{{trans('home.more')}}</a>
                                </div>
                            </figcaption>
                        </div>
                    </figure>
                @endforeach
            </div>
            <div class="row">{{ $news->links() }}</div>
        </div>
    </div>
@endsection