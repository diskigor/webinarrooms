@extends('layouts.admin')
@section('content')
<?php $title_page = 'Видеоинструкции'; ?>
<div class="col-md-12 video-instruction">
<div class="row">
    @foreach($instructions as $video)
    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
        <div class="video-block">
            <!-- Video -->
            <video id="video_{{ $video->id }}" width="300" height="169" poster="{{asset('images/postervideoinst_1.jpg')}}">
                <source src="{{ $video->video }}" type="video/webm">
            </video>
            
            <!-- Video Controls -->
            <div id="video-controls">
                <div class="col-md-5 col-sm-5 col-xs-5">
                    <button type="button" id="play-pause_{{ $video->id }}" class="play"><i class="far fa-play-circle"></i></button>
                    <input type="range" id="seek-bar_{{ $video->id }}" value="0">
                </div>
                <div class="col-md-5 col-sm-5 col-xs-5">
                    <button type="button" id="mute_{{ $video->id }}"><i class="fas fa-volume-up"></i></button>
                <input type="range" id="volume-bar_{{ $video->id }}" min="0" max="1" step="0.1" value="1">
                </div>
                <div class="col-md-2 col-sm-2 col-xs-2">
                    <button type="button" id="full-screen_{{ $video->id }}"><i class="fas fa-arrows-alt"></i></button>
                </div>
            </div>
            <script>
//                document.getElementById('video_{{ $video->id }}').addEventListener('loadedmetadata', function() {
//                            this.currentTime = 8;
//                          }, false);
            // Video
	var video_{{ $video->id }} = document.getElementById("video_{{ $video->id }}");

	// Buttons
	var playButton_{{ $video->id }} = document.getElementById("play-pause_{{ $video->id }}");
	var muteButton_{{ $video->id }} = document.getElementById("mute_{{ $video->id }}");
	var fullScreenButton_{{ $video->id }} = document.getElementById("full-screen_{{ $video->id }}");

	// Sliders
	var seekBar_{{ $video->id }} = document.getElementById("seek-bar_{{ $video->id }}");
	var volumeBar_{{ $video->id }} = document.getElementById("volume-bar_{{ $video->id }}");


	// Event listener for the play/pause button
	playButton_{{ $video->id }}.addEventListener("click", function() {
		if (video_{{ $video->id }}.paused == true) {
			// Play the video
			video_{{ $video->id }}.play();

			// Update the button text to 'Pause'
			playButton_{{ $video->id }}.innerHTML = '<i class="far fa-pause-circle"></i>';
		} else {
			// Pause the video
			video_{{ $video->id }}.pause();

			// Update the button text to 'Play'
			playButton_{{ $video->id }}.innerHTML = '<i class="far fa-play-circle"></i>';
		}
	});


	// Event listener for the mute button
	muteButton_{{ $video->id }}.addEventListener("click", function() {
		if (video_{{ $video->id }}.muted == false) {
			// Mute the video
			video_{{ $video->id }}.muted = true;

			// Update the button text
			muteButton_{{ $video->id }}.innerHTML = '<i class="fas fa-volume-up"></i>';
		} else {
			// Unmute the video
			video_{{ $video->id }}.muted = false;

			// Update the button text
			muteButton_{{ $video->id }}.innerHTML = '<i class="fas fa-volume-off"></i>';
		}
	});


	// Event listener for the full-screen button
	fullScreenButton_{{ $video->id }}.addEventListener("click", function() {
		if (video_{{ $video->id }}.requestFullscreen) {
			video_{{ $video->id }}.requestFullscreen();
		} else if (video_{{ $video->id }}.mozRequestFullScreen) {
			video_{{ $video->id }}.mozRequestFullScreen(); // Firefox
		} else if (video_{{ $video->id }}.webkitRequestFullscreen) {
			video_{{ $video->id }}.webkitRequestFullscreen(); // Chrome and Safari
		}
	});


	// Event listener for the seek bar
	seekBar_{{ $video->id }}.addEventListener("change", function() {
		// Calculate the new time
		var time = video_{{ $video->id }}.duration * (seekBar_{{ $video->id }}.value / 100);

		// Update the video time
		video_{{ $video->id }}.currentTime = time;
	});

	
	// Update the seek bar as the video plays
	video_{{ $video->id }}.addEventListener("timeupdate", function() {
		// Calculate the slider value
		var value = (100 / video_{{ $video->id }}.duration) * video_{{ $video->id }}.currentTime;

		// Update the slider value
		seekBar_{{ $video->id }}.value = value;
	});

	// Pause the video when the seek handle is being dragged
	seekBar_{{ $video->id }}.addEventListener("mousedown", function() {
		video_{{ $video->id }}.pause();
	});

	// Play the video when the seek handle is dropped
	seekBar_{{ $video->id }}.addEventListener("mouseup", function() {
		video_{{ $video->id }}.play();
	});

	// Event listener for the volume bar
	volumeBar_{{ $video->id }}.addEventListener("change", function() {
		// Update the video volume
		video_{{ $video->id }}.volume = volumeBar_{{ $video->id }}.value;
	});
        </script>
	</div>
        <h3>{{$video->name}}</h3>
        <!--p>{{--$video->short--}}</p-->
    </div>
    @endforeach
</div>
<div class="col-md-12 text-center">{{ $instructions->links() }}</div>
</div>
@endsection


