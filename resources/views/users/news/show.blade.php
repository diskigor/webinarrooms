@extends('layouts.admin')
@section('content')
    <div id="content">
        <div class="container">
            <header class="panel big-header text-center">{{$news->news_trans->first()->title}}</header>
            <div class="panel">
                <img class="img-responsive" src="{{asset($news->image_link)}}" alt="{{$news->news_trans->first()->slug}}">
                <h4 style="text-align: center;">{{trans('home.describenews')}}</h4>
                <div>{!! $news->news_trans->first()->article !!}</div>
                <div class="row" style="text-align: center;"><a href="{{ route('unews.index') }}" class="btn btn-blue">Назад к списку</a></div>
            </div>
        </div>
    </div>
@endsection