@extends('layouts.admin')
@section('content')
    <div id="content">
        <div class="container">
            <header class="big-header">{{$video_news->short_desc}}</header>
            <div class="panel">
                <img class="img-responsive" src="{{asset($video_news->image_link)}}" alt="{{$video_news->short_desc}}">
                <div>{!! $video_news->href !!}</div>
                <div class="row"><a href="{{ route('unews.index') }}" class="btn btn-blue">Назад к списку</a></div>
            </div>
        </div>
    </div>
@endsection