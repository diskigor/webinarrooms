{{-- на входе параметр $blog, $class --}}

@if ($blog->count_view)
    <?php if (!isset($class) || !is_string($class)) { $class =''; } ?>
    <span style="padding: 12px 3px" class="bloglikes {{$class}} far fa-eye" aria-hidden="true">  {{ $blog->Counter }}</i></span>
@endif