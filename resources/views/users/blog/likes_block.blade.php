{{-- на входе параметр $blog --}}
@if($blog->isLiked)
    <a href="{{ route('blog.like', $blog->id) }}" id="i_am_like" title="{{trans('blog.dislike')}}">
        <i class="fa fa-heart" aria-hidden="true"></i>
    </a>
@else
    <a href="{{ route('blog.like', $blog->id) }}" id="i_am_like" title="{{trans('blog.like')}}">
        <i class="fa fa-heart" aria-hidden="true"></i>
    </a>
@endif
{{ $blog->likes()->count() }}
