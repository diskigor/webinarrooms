{{-- на входе $blog, $linktolist, $linktoshow --}}
@extends('layouts.admin')
@section('content')
<?php $title_page=trans('blog.headertitle').' - '. $blog->authorFIO.' - '.$blog->title ; ?>
<link rel="stylesheet" type="text/css" media="all" href="{{asset('comments/css')}}/comments.css" />
    <div class="col-md-12 blog-one-item-user">
        <!--h1 class="blogheader">{{--$blog->title--}}</h1-->


                <article class="blogcontent-user">  
                    <img class="image imgblog" src="{{ $blog->image }}">
                    <p>{!! $blog->article !!}</p>
                    <hr class="blogsepar">
                    <p class="text-left col-md-4"><img style="max-width: 40px;" src="{{ $blog->author->avatar }}" />  <b>{{ $blog->authorFIO }}</b></p>
                    <p class="col-md-4 text-center fs18" style="padding: 9px;">
                    <span id="likes_block" class="bloglikes"> @include('users.blog.likes_block',['blog'=>$blog])</span>
                    <span class="blogcomments far fa-comments" aria-hidden="true">  {{ $blog->comments()->count() }}</i></span>
                    @include('users.blog.view_block',['blog'=>$blog])
                    </p>
                    <p class="text-right col-md-4"style="padding: 9px;">{{ $blog->created_at->format('d.m.Y') }}</p>
                    
                    @if(!$blog->listTags()->isEmpty())
                        <hr class="blogsepar">
                        <span>Теги: </span>
                        @foreach($blog->listTags() as $tag)
                        <span class="tags-search">{{ $tag }}</span>
                        @endforeach
                    
                    @endif
                    <hr class="blogsepar">
                    <div id='js_block_load_comments_for_this_blog'> </div>
                    {{--@include('comments.comments_block', ['essence' => $blog])--}}
                    <a href="{{ $linktolist }}" class="btn btn-blue">{{trans('blog.backtolist') }}</a>
                </article>
                



    </div>
@endsection
@section('script')
<script>
    $(document).ready(function(){
      $('#js_block_load_comments_for_this_blog').load("{{ route('comments_load',['id_blog'=>$blog->id]) }}");
    });
    
    $(document).on('click','#js_block_load_comments_for_this_blog_pagination a',function(e){
        e.preventDefault();
        var href = this.href;
        if (href) {
            $('#js_block_load_comments_for_this_blog').load(href);
        }
    });
    
    $(document).on('click','.js_comments_collapse', function(e){
        e.preventDefault();
        var block = $(this).parents('li.comment');
        
        var children = block.find('ul.collapse');

        if (children.length === 0) {
            children = block.find('ul.children');
            children.addClass('collapse');
            return;
        }
        children.removeClass('collapse');
    });
    
    $(document).on('click','#i_am_like', function(e){
        e.preventDefault();
        var a = this.href;
        if (a === undefined) return false;        
        $.ajax({
            url: a,
            type: "POST",
            cache: false,
            success: function (data) {
                if (data.errors) {
                    console.log(data.errors);
                    return false;
                }
                if (data.likes_block) {
                    $('#likes_block').html(data.likes_block);
                }
            },
        });
    });
    
    $(document).on('click','.js_i_am_comment_like', function(e){
        e.preventDefault();
        var a = $(this).data('href');
        if (a === undefined) return false;   
        var block = $(this).parent('.js_block_coment_like');
        console.log(a,block);

        $.ajax({
            url: a,
            type: "POST",
            cache: false,
            success: function (data) {
                if (data.errors) {
                    console.log(data.errors);
                    return false;
                }
                if (data.likes_block_comment) {
                    block.replaceWith(data.likes_block_comment);
                }
            },
        });
    });
</script>
<script type="text/javascript" src="{{asset('comments/js')}}/comment-reply.js" /></script>
<script type="text/javascript" src="{{asset('comments/js')}}/comment-scripts.js" /></script>
@endsection