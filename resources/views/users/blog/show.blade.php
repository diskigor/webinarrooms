{{-- на входе $blog, $linktolist, $linktoshow --}}
@extends('layouts.admin')
@section('content')
<link rel="stylesheet" type="text/css" media="all" href="{{asset('comments/css')}}/comments.css" />
    <div class="col-md-12 blog-one-item">
        <h1 class="blogheader">{{$blog->title}}</h1>


                <article class="blogcontent">  
                    <img class="image imgblog" src="{{ $blog->image }}">
                    <p>{!! $blog->article !!}</p>
                    <hr class="blogsepar">
                    <p class="text-left col-md-4">{{trans('blog.author')}}: {{ $blog->authorFIO }}</p>
                    <p class="col-md-4 text-center fs18">
                    <span id="likes_block" class="bloglikes"> @include('users.blog.likes_block',['blog'=>$blog])</span>
                    <span class="blogcomments far fa-comments" aria-hidden="true">  {{ $blog->comments()->count() }}</i></span>
                    </p>
                    <p class="text-right col-md-4">{{trans('blog.create')}}: {{ $blog->created_at }}</p>
                    
                    @if(!$blog->listTags()->isEmpty())
                        <hr class="blogsepar">
                        <span>Теги: </span>
                        @foreach($blog->listTags() as $tag)
                        <span class="tags-search">{{ $tag }}</span>
                        @endforeach
                    
                    @endif
                    <hr class="blogsepar">
                    {{--@include('comments.comments_block', ['essence' => $blog])--}}
                    <a href="{{ $linktolist }}" class="btn btn-blue">{{trans('blog.backtolist') }}</a>
                </article>
                



    </div>
@endsection
@section('script')
<script>
    $(document).on('click','#i_am_like', function(e){
        e.preventDefault();
        var a = this.href;
        if (a === undefined) return false;        
        $.ajax({
            url: a,
            type: "POST",
            cache: false,
            data: '_token={{csrf_token()}}',
            success: function (data) {
                if (data.errors) {
                    console.log(data.errors);
                    return false;
                }
                if (data.likes_block) {
                    $('#likes_block').html(data.likes_block);
                }
            },
        });
    });
</script>
<script type="text/javascript" src="{{asset('comments/js')}}/comment-reply.js" /></script>
<script type="text/javascript" src="{{asset('comments/js')}}/comment-scripts.js" /></script>
@endsection