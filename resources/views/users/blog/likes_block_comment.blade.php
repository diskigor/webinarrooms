{{-- на входе параметр $comment,$class --}}

<?php if (!isset($class) || !is_string($class)) { $class =''; } ?>

<span class="js_block_coment_like {{ $class }}">
    @if($comment->isLiked)
    <a href="#" data-href="{{ route('comment.like', $comment->id) }}" class="js_i_am_comment_like" title="{{trans('comment.dislike')}}">
            <i class="fa fa-heart" aria-hidden="true"></i>
        </a>
    @else
        <a href="#" data-href="{{ route('comment.like', $comment->id) }}" class="js_i_am_comment_like" title="{{trans('comment.like')}}">
            <i class="fa fa-heart" aria-hidden="true"></i>
        </a>
    @endif
    {{ $comment->likes()->count() }}
</span>