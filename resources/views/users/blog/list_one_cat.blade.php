{{--  $blogs  --}}
@extends('layouts.admin')
@section('content')
<div class="col-md-12 blog-content">
    <?php $title_page=trans('blog.headertitle'); ?>
    <div class="admin-tag-filter col-md-12">
        <h3>Фильтр блога по тегам <i class="fa fa-filter" aria-hidden="true" style="float: right;"></i></h3>
        <!--a href="{{-- $blogs->linktoroute --}}" class="btn btn-blue">{{-- $blogs->textNameGroup --}}
                @if($blogs->blog_newcounts)
                    <span class="badge" title="новых {{ $blogs->blog_newcounts }}">{{ $blogs->blog_newcounts }}</span>
                @endif
        </a-->
        @if(isset($tags) && !empty($tags))
            <div class="col-md-12 admin-tags-block">
                <a class="js_tag {{ empty($tag)?'js_active':'' }} tags-search">ВСЕ</a>
                @foreach($tags as $onetag)
                <a data-tag="{{ $onetag }}" class="js_tag {{ (isset($tag) && $onetag==$tag)?'js_active':'' }} tags-search">{{ $onetag }}</a>
                @endforeach 
            </div>
        @endif
        <div class="btn-block-blog">
            <a href="{{ route('blog.index') }}" class="btn-sort-admin pull-left" style="margin-left: 0;">
                <span>ВСЕ БЛОГИ</span></a>
            <a data-sort="sort_created" 
               class="js_sort {{ !request()->has('sort_created')?'js_active':'' }} btn-sort-admin  pull-right">
                <span>Последние</span></a>
            <a data-sort="sort_likes" 
               class="js_sort {{ (request()->has('sort_likes') && request()->input('sort_likes')=='desc')?'js_active':'' }} btn-sort-admin pull-right">
                <span>Самые популярные</span></a>
        </div>
    </div>
    <div class="row center-blog-content">
        @foreach($blogs as $article)
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-12">
                        <div class="thumbnail">
                            <div class="blog-img" style="background-image: url({{ $article->image }});"></div>
                            
                            <span class="dateblog">{{ $article->created_at->format('d.m.Y') }}</span>
                            <div class="caption">
                                <h3>{{$article->title}}</h3>
                                @if($article->isNew)
                                    <span class="badge blog-new-icon" title="это новый блог">new</span>
                                @endif
                                <p class="blog-description">{{ mb_strimwidth (strip_tags($article->article),0,300,'...') }}</p>
                                <hr>
                                <p class="" style="display: flow-root;">
                                <span class="pull-left">  {{trans('blog.author')}}: {{ $article->authorFIO }}</span> 
                                <span class="bloglikes pull-right"><i class="fa fa-heart" aria-hidden="true">  {{ $article->likes()->count() }}</i></span>
                                <span class="blogcomments pull-right"><i class="fa fa-comments-o" aria-hidden="true">  {{ $article->comments()->count() }}</i></span>
                                </p>
                                <hr>
                                <p><a href="{{ $article->linktoshow }}" class="btn btn-blogread" role="button">{{trans('blog.readmore')}}</a></p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            @endforeach
            <div class="col-md-12" style="text-align: center;">{{ $blogs->links() }}</div>
    </div>
    {{ $blogs->links() }}
</div>
@endsection
@section('script')
<style>
    .js_active {
        background: red;
    }
</style>
<script type="text/javascript" src="{{asset('js/QueryData.js')}}" /></script>

<script type="text/javascript" >
    var lroute = "{{ isset($blogs->linktoroute)?$blogs->linktoroute:'' }}"
    var lroutes = "{{ isset($blogs->linktoroutesearch)?$blogs->linktoroutesearch:'' }}"
</script>

<script>

    $(document).on('click','.js_sort', function(e){
        e.preventDefault();
        $(this).toggleClass('js_active');
//        console.log(getPrefixSearch());
//        return;
        window.location.href = getPrefixSearch();
    });
    
    $(document).on('click','.js_tag', function(e){
        e.preventDefault();
        $('.js_tag').removeClass('js_active');
        $(this).addClass('js_active');
//        console.log(getPrefixSearch());
//        return;
        window.location.href = getPrefixSearch();
    });
    
    function getPrefixSearch(){
        var res = {sort_created:'asc'};
        
        $('.js_sort.js_active').each(function(){
            if (!$(this).data('sort')) { return; }
            if ($(this).data('sort') === 'sort_created') {delete res.sort_created;}
            if ($(this).data('sort') === 'sort_likes') {
                res.sort_likes = 'desc';
            }
        });

        $('.js_tag.js_active').each(function(){
            if (!$(this).data('tag')) { return; }
            res.tag = $(this).data('tag');
        });

        var lcounter = 0;

        for (var key in res) {
          lcounter++;
        }
        if (lcounter) {
            return lroutes + '?' + $.param(res);
        } else {
            return lroute;
        }
    }
    
</script>
@endsection