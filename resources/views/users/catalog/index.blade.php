@extends('layouts.admin')
@section('content')
<?php $title_page='Каталог обучающих материалов'; ?>
    <div class="row catalogue">
        <div class="col-md-6">
            <a href="{{route('user_curses.index')}}">
            <div class="catalog-lectures-block">
                <h2>Курсы ШЭЛ</h2>
                <p class="count-catalog-item"><span>Всего курсов {{ $main_curses->count() }}  </span><br><span>{{ $courses_counts->get('new_gilbo')?' Новых курсов +'.$courses_counts->get('new_gilbo'):'' }}</span></p>
            </div>
            <i class="fas fa-info-circle" aria-hidden="true" data-toggle-catalogue="maincourses"></i>
            </a>
            
        </div>
        <div class="col-md-6">
            <a href="{{route('partner_curses')}}">
            <div class="catalog-lectures-block">
                <h2>Курсы партнеров </h2>
                <p class="count-catalog-item"><span>Всего курсов {{ $lecturer_curses->count() }}  </span><br><span>{{ $courses_counts->get('new_lecturer')?' Новых курсов +'.$courses_counts->get('new_lecturer'):'' }}</span></p>
            </div>
            <i class="fas fa-info-circle" aria-hidden="true" data-toggle-catalogue="partnercourses"></i>
            </a>
            
        </div>
        <div class="col-md-6">
            <a href="{{route('webinars_room.index')}}">
            <div class="catalog-lectures-block">
                <h2>Каталог вебинаров</h2>
                <p class="count-catalog-item"><span>Всего курсов 25  </span><br><span>  Новых курсов +5</span></p>
            </div>
            <i class="fas fa-info-circle" aria-hidden="true" data-toggle-catalogue="webcourses"></i>
            </a>
            
        </div>
        <div class="col-md-6">
            <a href="https://insiderclub.ru/" target="_blanc">
            <div class="catalog-lectures-block">
                <h2>Каталог семинаров</h2>
                <p class="count-catalog-item"><span>Всего курсов 25  </span><br><span>  Новых курсов +5</span></p>
            </div>
            <i class="fas fa-info-circle" aria-hidden="true" data-toggle-catalogue="seminars"></i>
            </a>
            
        </div>
        
        
    </div>

        

@endsection