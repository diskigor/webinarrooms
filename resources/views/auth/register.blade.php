@extends('layouts.front')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <header class="big-header text-center">Быстрая регистрация</header>
                <div class="panel panel-default">

                    <div class="panel-body">
                        <form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST"
                              action="{{ route('register') }}">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Имя *</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name"
                                           value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block danger">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="surname" class="col-md-4 control-label">Реферальный ключ</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="ref_key" placeholder="Введите ключ по которому Вас пригласили.">
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail *</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email"
                                           value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block danger">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Пароль *</label>

                                <div class="col-md-6">
                                    <input type="password" 
                                           class="form-control form_password_eye" 
                                           name="password" required>
                                    
                                    <a tabindex="-1" 
                                            aria-hidden="true" 
                                            style="cursor: pointer;"
                                            class="password-reveal btn-grey"><i class="fa fa-eye-slash" aria-hidden="true"></i></a>

                                    @if ($errors->has('password'))
                                        <span class="help-block danger">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password-confirm" class="col-md-4 control-label">Подтверждение
                                    пароля *</label>

                                <div class="col-md-6">
                                    <input type="password" 
                                           class="form-control form_password_eye"
                                           name="password_confirmation" 
                                           required>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('rules') ? ' has-error' : '' }}">
                                <label for="rules" class="col-md-4 control-label">{{ $errors->has('rules') ? '***' : '   ' }}</label>
                                <div class="col-md-6">
                                <input type="checkbox" name="rules" required> <a href="#">





                                    <!-- Button trigger modal -->
                                    <a href="#" type="button" data-toggle="modal" data-target="#myModal">
                                        с правилами и обязанностями школы - ознакомлен
                                    </a>

                                    <!-- Modal -->
                                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title text-center" id="myModalLabel">
                                                        <b>ПРАВА И ОБЯЗАННОСТИ ЧЛЕНОВ КЛУБА</b>
                                                    </h4>
                                                </div>
                                                <div class="modal-body">

                                                    <p>Члены Клуба кроме права на доступ к средствам самообразования в объеме полного курса "Школы эффективных лидеров", обладают целым рядом дополнительных прав. Среди них право на получение информации, доступ к нашим базам данных. Члены Клуба пользуются правом на специфические консультации. Члены Клуба пользуются также правом скидки на участие в семинарах, организуемых при участии Клуба.</p>
                                                    <p>Обязанностью членов Клуба является сохранение тайны внутренней информации Клуба, содержания полученного специфического образования, используемых технологий, характера взаимодействия членов Клуба. Информационный ресурс Клуба не должен быть предметом свободного доступа.</p>
                                                    <p>В случае нарушения этого требования возможно исключение из членов Клуба по исполнении Клубом обязательств по обучению, взятых перед членом Клуба. Исключенный по этому основанию навсегда теряет право на членство в Клубе.</p>
                                                    <p>Членство в Клубе не накладывает на его членов никаких дополнительных обязательств перед каким-либо государством или сторонними организациями.</p>
                                                    <p>Член Клуба не имеет никаких других обязанностей перед Клубом. Применение полученных в Клубе знаний, использование получаемой информации не обусловлено никакими ограничениями.</p>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    @if ($errors->has('rules'))
                                        <span class="help-block danger">
                                        <strong>{{ $errors->first('rules') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Зарегистрироваться
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script>
        var inputs_eye = document.querySelectorAll('.form_password_eye');
        document.querySelector(".password-reveal").addEventListener("click", function () {
            El_eye = this;
            inputs_eye.forEach(function(item) {
                if (item.getAttribute('type') !== 'text') {
                    item.setAttribute('type', 'text');
                    El_eye.innerHTML = '<i class="fa fa-eye" aria-hidden="true"></i>';
                } else {
                    item.setAttribute('type', 'password');
                    El_eye.innerHTML = '<i class="fa fa-eye-slash" aria-hidden="true"></i>';
                }
            });

        });

</script>

@endsection
