<?php

return [
    'referalkey'    => env('referalkey', 'ref'),
    'ref_max_level' => env('refref_max_level', 3),
    
    'personal_IdGilbo' => env('personal_IdGilbo', 89),
    'limit_new_blog' => env('limit_new_blog', 10),
    'limit_new_course' => env('limit_new_course', 30),
    
    
    'telegram_api_send_message' => env('telegram_api_send_message',FALSE),
    
    'balls_for_best_comment_text_answer' => env('balls_for_best_comment_text_answer', 5),
    
    'balls_to_kel_convert_curse' => env('balls_to_kel_convert_curse', 0.1),
    
    'add_day_for_lection_complete' => env('add_day_for_lection_complete', 1),
    
    'add_ball_from_full_profile' => env('add_ball_from_full_profile', 100),
    
    'ball_procent_open_lection' => env('ball_procent_open_lection', 50),
    
    'minum_kel_balance_alert'   => env('minum_kel_balance_alert', 5),
    
    'minum_pay_add_balance'   => env('minum_pay_add_balance', 5),
    
    'standart_first_pay'  => env('standart_first_pay', 350),
    'admin_first_pay'  => env('admin_first_pay', 200),
    
    
    'balls_up_role_to_pretendent'  => env('balls_up_role_to_pretendent', 500),
    'balls_up_role_to_expert'  => env('balls_up_role_to_expert', 1000),
    
    'summa_auto_chlen_club' => env('summa_auto_chlen_club', 500),
    
    'discont_for_bay_all_curs' => env('discont_for_bay_all_curs', 20),
];
