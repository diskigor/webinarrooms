<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class testreset extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'testreset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset data to start test';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
    }
}
