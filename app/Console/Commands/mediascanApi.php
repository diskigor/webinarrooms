<?php

namespace App\Console\Commands;

use App\Http\Controllers\Api\apiMediascanController;
use Illuminate\Console\Command;

class mediascanApi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mediascan';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'get api mediascan';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $api = new apiMediascanController();
       $api->getNewID();
       $this->info('get new Api success');
    }
}
