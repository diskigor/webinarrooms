<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\User;
use App\Models\Subscribers\Subscribers;

class syncSubscribers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'syncSubscribers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Subscribers table and User table, from email || user_id';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (Subscribers::all() as $sub) {
            //обновляем только имя
            if ($sub->user && $sub->type) {  
                $sub->update([  'name'  => $sub->user->fullName]); 
                continue; }
            
            //обновить имя и тип
            if ($sub->user) {  
                $sub->update([  'name'  => $sub->user->fullName,
                                'type'  => Subscribers::$FOR_MEDIASCAN]); 
                continue; }
            
            $user = User::where('email',$sub->email)->first();
            if($user) {
                //добавить имя, тип, и ид
                $sub->update([  'name'      => $user->fullName,
                                'type'      => Subscribers::$FOR_MEDIASCAN,
                                'user_id'   => $user->id]); 
            } else {
                //обновить имя и тип
                $sub->update([  'name'  => '',
                                'type'  => Subscribers::$FOR_MEDIASCAN]);  
            }
        }
    }
}
