<?php

namespace App\DataTables;

use App\Models\Subscribers\Subscribers;
use Yajra\DataTables\Services\DataTable;

use Carbon\Carbon;

class SubscibersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->editColumn('created_at', function ($query) {
                return $query->created_at->format('d-m-Y');
            })
            ->editColumn('status', function ($query) {
                return $query->status?'Подписан':'Отписался';
            })
            ->editColumn('type', function ($query) {
                return $query->UType;
            })
            ->filter(function($qu) {
                if (request()->has('start_date')) {
                    $qu->where('created_at', '>=', request()->get('start_date'));
                }
                if (request()->has('end_date')) {
                    $qu->where('created_at', '<=', request()->get('end_date'));
                }

            })
            ->addColumn('action', 'subscribers.action');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Billing\Orders $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Subscribers $model)
    {       
        return $model->newQuery()->select('subscribers.*');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    //->columns($this->getColumns())
                    ->addColumn(['data' => 'id', 'name' => 'id', 'title' => 'Номер','width' => '40px'])
                    ->addColumn(['data' => 'status', 'name' => 'status', 'title' => 'Статус','width' => '40px'])
                    ->addColumn(['data' => 'name', 'name' => 'name', 'title' => 'Имя Пользователя'])
                    ->addColumn(['data' => 'email', 'name' => 'email', 'title' => 'E-mail'])
                    ->addColumn(['data' => 'type', 'name' => 'type', 'title' => 'Тип подписки'])
                    ->addColumn(['data' => 'created_at', 'name' => 'created_at', 'title' => 'Дата регистрации'])
                    ->minifiedAjax()
                    //->addAction(['width' => '80px','exportable'     => true])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'created_at'
            
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'subscribers_' . Carbon::now();
    }
}
