<?php

namespace App\DataTables;

use App\Models\Billing\Billing;
use Yajra\DataTables\Services\DataTable;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class UserHistoryTransactionsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', 'history.action')
                            ->editColumn('created_at', function ($query) {
                return $query->created_at->format('d m Y');
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Billing\Orders $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Billing $model)
    {

        return $model->newQuery()->getUserOf(Auth::user()->id)->select('id','transfer_sum','desc','created_at');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    //->columns($this->getColumns())
                    ->addColumn(['data' => 'id', 'name' => 'id', 'title' => 'Запись'])
                    ->addColumn(['data' => 'transfer_sum', 'name' => 'transfer_sum', 'title' => 'Сумма'])
                    ->addColumn(['data' => 'desc', 'name' => 'desc', 'title' => 'Описание платежа'])
                    ->addColumn(['data' => 'created_at', 'name' => 'created_at', 'title' => 'Дата операции'])
                    ->minifiedAjax()
                    //->addAction(['width' => '80px','exportable'     => true])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'transfer_sum',
            'desc',
            'created_at'
            
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'orders_' . Carbon::now();
    }
}
