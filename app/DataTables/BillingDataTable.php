<?php

namespace App\DataTables;

use App\Models\Billing\Billing;
use Yajra\DataTables\Services\DataTable;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class BillingDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $users = $query->newQuery()->get()->pluck('user')->filter()->unique();
        $admins = $query->newQuery()->get()->pluck('admin')->unique();
        //dd($ss);
        return datatables($query)
            ->addColumn('action', 'billings.action')
                ->with('users', $users)
                ->with('admins', $admins);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Billing\Orders $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Billing $model)
    {       
            if(Auth::user()->isAdmin()){
                return $model->newQuery()->with(['user','admin'])->select('sdo_billing_orders.*');
            } else {
                return $model->newQuery()->with(['admin'])->where('user_id',Auth::user()->id)->select('sdo_billing_orders.*');
            }
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    //->columns($this->getColumns())
                    ->addColumn(['data' => 'id', 'name' => 'id', 'title' => 'Запись'])
                    ->addColumn(['data' => 'user.name', 'name' => 'user.name', 'title' => 'Пользователь'])
                    ->addColumn(['data' => 'user.email', 'name' => 'user.email', 'title' => 'E-mail'])
//                    ->addColumn(['data' => 'course.lang.name_trans', 'name' => 'course.lang.name_trans', 'title' => 'Курс'])
                    ->addColumn(['data' => 'transfer_sum', 'name' => 'transfer_sum', 'title' => 'Сумма'])
                    ->addColumn(['data' => 'desc', 'name' => 'desc', 'title' => 'Описание'])
                    ->addColumn(['data' => 'created_at', 'name' => 'created_at', 'title' => 'Дата операции'])
                    ->minifiedAjax()
                    ->addAction(['width' => '80px','exportable'     => true])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'created_at'
            
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'billing_' . Carbon::now();
    }
}
