<?php

namespace App\DataTables;

use App\Models\Billing\Balls;
use Yajra\DataTables\Services\DataTable;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class AdminBallsDataTable extends DataTable {

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query) {
        return datatables($query)
                        ->addColumn('action', 'balls.action')
                            ->editColumn('created_at', function ($query) {
                return $query->created_at->format('d m Y');
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Billing\Orders $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Balls $model) {
        if (Auth::user()->isAdmin()) {
            return $model->newQuery()->with(['user'])->select('sdo_balls.*')->get();
        } 
        
        return $model->newQuery()->where('user_id', Auth::user()->id)->select('sdo_balls.*')->get();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() {

        if (Auth::user()->isAdmin()) {
            return $this->builder()
                            //->columns($this->getColumns())
                            ->addColumn(['data' => 'id', 'name' => 'id', 'title' => 'Запись'])
                            ->addColumn(['data' => 'user.name', 'name' => 'user.name', 'title' => 'Пользователь'])
                            ->addColumn(['data' => 'user.email', 'name' => 'user.email', 'title' => 'E-mail'])
                            ->addColumn(['data' => 'value', 'name' => 'value', 'title' => 'Количество'])
                            ->addColumn(['data' => 'description', 'name' => 'description', 'title' => 'Описание'])
                            ->addColumn(['data' => 'created_at', 'name' => 'created_at', 'title' => 'Дата операции'])
                            ->minifiedAjax()
                            //->addAction(['width' => '80px','exportable'     => true])
                            ->parameters($this->getBuilderParameters());
        } 
        
        return $this->builder()
                        ->addColumn(['data' => 'id', 'name' => 'id', 'title' => 'Запись'])
                        ->addColumn(['data' => 'value', 'name' => 'value', 'title' => 'Количество'])
                        ->addColumn(['data' => 'description', 'name' => 'description', 'title' => 'Описание'])
                        ->addColumn(['data' => 'created_at', 'name' => 'created_at', 'title' => 'Дата операции'])
                        ->minifiedAjax()
                        ->parameters($this->getBuilderParameters());

    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
        return [
            'id',
            'user.name',
            'user.email',
            'value',
            'description',
            'created_at'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() {
        return 'balls_' . Carbon::now();
    }

}
