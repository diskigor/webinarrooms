<?php

namespace App\DataTables;

use App\Models\Billing\Orders;
use Yajra\DataTables\Services\DataTable;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class OrdersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', 'orders.action');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Billing\Orders $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Orders $model)
    {
            if(Auth::user()->isLecturer()){
                return $model->newQuery()->with(['user','course.lang'])->where('lecture_id',Auth::user()->id)->select('sdo_orders.*');
            }elseif(Auth::user()->isAdmin()){
                return $model->newQuery()->with(['user','course.lang'])->select('sdo_orders.*');
            } else {
                return $model->newQuery()->with(['user','course.lang'])->where('id',0)->select('sdo_orders.*');
            }
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    //->columns($this->getColumns())
                    ->addColumn(['data' => 'id', 'name' => 'id', 'title' => 'Запись'])
                    ->addColumn(['data' => 'user.name', 'name' => 'user.name', 'title' => 'Пользователь'])
                    ->addColumn(['data' => 'user.email', 'name' => 'user.email', 'title' => 'E-mail'])
                    ->addColumn(['data' => 'course.lang.name_trans', 'name' => 'course.lang.name_trans', 'title' => 'Курс'])
                    ->addColumn(['data' => 'sum', 'name' => 'sum', 'title' => 'Сумма'])
                    ->addColumn(['data' => 'lecture_sum', 'name' => 'lecture_sum', 'title' => 'Бонус лектора'])
                    ->addColumn(['data' => 'created_at', 'name' => 'created_at', 'title' => 'Дата операции'])
                    ->minifiedAjax()
                    //->addAction(['width' => '80px','exportable'     => true])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'user.name',
            'user.email',
            'course.lang.name_trans',
            'sum',
            'lecture_sum',
            'created_at'
            
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'orders_' . Carbon::now();
    }
}
