<?php
namespace App\Observers;

use App\Models\Billing\Orders;
use App\Models\Billing\Billing;

class OrderObserver
{
    /**
     * Listen to the User created event.
     *
     * @param  Orders $order
     * @return void
     */
    public function created(Orders $order)
    {
        if ($order->sum == 0) { return; }
        
        if($order->user) {
            //ВАЖНО !!! 
            //Изменения в баланс только через наблюдатели
            //$order->user()->update(['balance'=>$order->user->balance - abs($order->sum) ]);
        }

//        if(!$order->user && $order->lecture_sum < 0 && $order->lecture_id) {//TODO это лектор забирает заработанное
//                $add_admin_replensh                     = new Billing();
//                $add_admin_replensh->user_id            = $order->lecture_id;
//                $add_admin_replensh->type_replenishment = Billing::TYPE_LECTURE;
//                $add_admin_replensh->transfer_sum       = abs($order->lecture_sum);
//                $add_admin_replensh->currency           = Billing::TYPE_CURRENCY;
//                $add_admin_replensh->desc = 'Заработано, на продаже лекций ';
//            
//        }
    }
    

    /**
     * Listen to the User deleting event.
     *
     * @param  Orders $order
     * @return void
     */
    public function deleting(Orders $order)
   {
//        //удаляем профиль пользователя
//        $user->profile()->delete();
//        //удаляем из профилей ссылки на пользователя если он кого-то приглашал
//        UserProfile::where('parrent_id',$user->id)
//                ->update(['parrent_id'=>NULL]);
    }
    

}