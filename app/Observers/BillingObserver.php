<?php
namespace App\Observers;

use App\Models\Billing\Billing;
use App\Http\Controllers\Rules\RulesAutoGroup;

class BillingObserver
{
    /**
     * Listen to the User created event.
     *
     * @param  Billing  $biling
     * @return void
     */
    public function created(Billing $biling)
    {
        if(!$biling->user) { return; }
        if ($biling->transfer_sum == 0) { return; }
        
        //админам неведомо начисление ;))
        if($biling->user->isAdmin()) { return; }
        
        //ВАЖНО !!! 
        //Изменения в баланс только через наблюдатели
        $new_balance = Billing::getUserOf($biling->user_id)->sum('transfer_sum');
        
        $biling->user()->update(['balance'=>$new_balance ]);

        
        RulesAutoGroup::getInstance()->execute($biling->user);
               
        //пусть работает работа ))
        if($biling->transfer_sum > 0 && !$biling->isReferal) { dispatch(new \App\Jobs\createReferalHistory($biling)); }
    }
    

    /**
     * Listen to the User deleting event.
     *
     * @param  Billing  $biling
     * @return void
     */
    public function deleting(Billing $biling)
   {
//        //удаляем профиль пользователя
//        $user->profile()->delete();
//        //удаляем из профилей ссылки на пользователя если он кого-то приглашал
//        UserProfile::where('parrent_id',$user->id)
//                ->update(['parrent_id'=>NULL]);
    }
    

}