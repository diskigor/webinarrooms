<?php
namespace App\Observers;

use GuzzleHttp\Client;
use App\Models\Blog\Blog;
//use Illuminate\Support\Facades\Log;

class BlogObserver
{
    /**
     * Listen to the Blog created event.
     *
     * @param  Blog  $blog
     * @return void
     */
    public function created(Blog $blog)
    {
        if(!$blog->author || $blog->isGaypark) { return;}

        

        //работа, отправка сообщения в телеграмм чат
        dispatch(new \App\Jobs\workSendToTelegramjob($blog));
        
        //работа, рассылка подписчикам
        dispatch(new \App\Jobs\workSendSubscibersEmailjob($blog));
    }
    

    /**
     * Listen to the Blog deleting event.
     *
     * @param  Blog  $blog
     * @return void
     */
    public function deleting(Blog $blog)
   {

    }
    

}