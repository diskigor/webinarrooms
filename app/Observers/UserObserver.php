<?php
namespace App\Observers;

use App\User;
use App\Models\Billing\RefKeys;
use App\Http\Controllers\Rules\RulesAutoGroup;

use Illuminate\Support\Facades\Cookie;

class UserObserver
{
    /**
     * Listen to the User created event.
     *
     * @param  User  $user
     * @return void
     */
    public function created(User $user)
    {
        //создание реферального ключа и т.п.
        $this->referals($user);
        
        //подписываем нового пользователя на всё автоматом
        dispatch(new \App\Jobs\addNewSubscribleAllFromEmailJob($user->email,$user->fullName)); 

        
    }
    
    /**
     * Listen to the User updated event.
     *
     * @param  User  $user
     * @return void
     */
    public function updated(User $user)
    {
        $this->referals($user);
    }

    
    
    /**
     * Listen to the User deleting event.
     *
     * @param  User  $user
     * @return void
     */
    public function deleting(User $user)
   {
//        //удаляем профиль пользователя
//        $user->profile()->delete();
//        //удаляем из профилей ссылки на пользователя если он кого-то приглашал
//        UserProfile::where('parrent_id',$user->id)
//                ->update(['parrent_id'=>NULL]);
    }
    
    /**
     * Создание реферального ключа
     * @param User $user
     * @return void
     */
    protected function referals(User $user) {
        //проверка существования пользователя в таблице реферальных ключей
        if (RefKeys::where('user_id',$user->id)->first()) { return; }
        
        //создание для пользователя свого реферального ключа
        $ref = new RefKeys();
        $ref->user_id   = $user->id;
        $ref->hashkey   = substr(str_shuffle(md5(time())),0,5).'00'.$user->id;
        
        //при наличии реферальной куки, подписываем пользователя, и удалаяем куку
        $cook           = request()->cookie(config('vlavlat.referalkey'));
        if ($cook) {
            Cookie::queue(Cookie::forget(config('vlavlat.referalkey')));
            $parent = RefKeys::where('hashkey',$cook)->first();
            if ($parent) { $ref->parent_id = $parent->user_id; }
        }
        
        //сохраняем результат ))
        $ref->save();
        
        //автороль
        RulesAutoGroup::getInstance()->execute($user);
    }
    

    
    
}