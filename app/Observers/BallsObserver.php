<?php
namespace App\Observers;

use App\Models\Billing\Balls;

class BallsObserver
{
    /**
     * Listen to the User created event.
     *
     * @param  Balls $balls
     * @return void
     */
    public function created(Balls $balls)
    {
        if(!$balls->user) { return; }
        if ($balls->value == 0) { return; }
        
        //админам неведомо начисление ;))
        if($balls->user->isAdmin()) { return; }
        
        //ВАЖНО !!! 
        //Изменения в балах только через наблюдатели
        $new_balls = Balls::GetUserOf($balls->user_id)->sum('value');
        
        $balls->user()->update(['level_point'=>$new_balls ]);
        
        //Лекторы не переключаются
        if($balls->user->isLecturer()) { return; }
        
        //переключатель роли на куратора
        if($new_balls > config('vlavlat.balls_up_role_to_expert')) {
            $balls->user()->update(['role_id' => 3 ]);
        }
    }
    

    /**
     * Listen to the User deleting event.
     *
     * @param  Balls $balls
     * @return void
     */
    public function deleting(Balls $balls)
   {
        if(!$balls->user) { return; }
        if ($balls->value == 0) { return; }
        
        //админам неведомо начисление ;))
        if($balls->user->isAdmin()) { return; }
        
        //ВАЖНО !!! 
        //Изменения в балах только через наблюдатели
        $new_balls = Balls::GetUserOf($balls->user_id)->sum('value');
        
        $balls->user()->update(['level_point'=>$new_balls ]);
    }
    

}