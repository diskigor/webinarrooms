<?php
namespace App\Observers;

use GuzzleHttp\Client;
use App\Models\Doings\DoingsTrans;
use Illuminate\Support\Facades\Log;

class EventDoingObserver
{
    /**
     * Listen to the User created event.
     *
     * @param  DoingsTrans $tr_event
     * @return void
     */
    public function creating(DoingsTrans $tr_event)
    {
        
        //работа, отправка сообщения в телеграмм чат
        dispatch(new \App\Jobs\workSendToTelegramjob($tr_event->name));
        
//        $url = config('vlavlat.telegram_api_send_message');
//        
//        $text = $tr_event->name;
//
//        $client = new Client();
//        $uri = $url.'&text='.$text;                 //Адрес запроса по api
//        $res = $client->get($uri)->getBody(); 
//        
//        Log::info('результат запроса на телеграм',['uri'=>$uri,'res'=>$res]);

        
        /*
        https://api.telegram.org/bot590140961:AAFceUnLRC1TR64VdosiD--tN-NZ_Xvg_jQ/sendMessage?chat_id=-1001150354768&text=Hello
        */
    }
    

    /**
     * Listen to the User deleting event.
     *
     * @param  DoingsTrans $tr_event
     * @return void
     */
    public function deleting(DoingsTrans $tr_event)
   {

    }
    

}