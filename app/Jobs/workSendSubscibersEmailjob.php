<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Subscribers\Subscribers;
use App\Models\Blog\Blog;
use App\Models\Api\apiMediascan;

use App\Mail\SendSubscriberEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class workSendSubscibersEmailjob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $obj;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($obj)
    {
        $this->obj = $obj;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
        if ($this->obj instanceof Blog) {
            if ($this->obj->isGaypark) { return; }
            $this->sendBlog($this->obj);
        }
        
    }
    
    /**
     * Рассылка блога, подписчикам
     * @param Blog $blog
     */
    protected function sendBlog(Blog $blog) {
        $listS = Subscribers::active()->where('type',  Subscribers::$FOR_BLOGS)->get();
        $t = 0;
        foreach ($listS as $ss) {
            Mail::send(new SendSubscriberEmail($ss, $blog));
            $t = $t +1;
        }
        Log::info('Рассылка блога "'.$blog->title.'" , подписчиков '.$t);
    }
    
    /**
     * Рассылка рассылки, подписчикам
     * @param apiMediascan $mediascan
     */
    protected function sendMediascan(apiMediascan $mediascan) {
        
    }
}
