<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\User;
use App\Models\Billing\Billing;

use App\Models\Billing\RefKeys;
use App\Models\Billing\RefRules;
use App\Models\Billing\RefHistory;

class createReferalHistory implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $billing;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Billing $bil)
    {
        $this->billing = $bil;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->referals($this->billing->user);
    }
    
    /**
     * Создание реферального начисления
     * @param User $user
     * @return void
     */
    protected function referals(User $user ) {
        //получаем список пригласивших для начислениия бонусов
        $listparents = $user->getMyParrents($user->id);
        
        if($listparents->isEmpty()) { return; }         //нет списка приглашенных
        
        foreach ($listparents as $level => $refkey) {
            $rule = RefRules::where('level',$level)->first();
            if(!$rule) { return; }
            
            $rf_bonus =RefRules::calculateOfRule($this->billing->transfer_sum, $level);
            
            //billing new bonus
            $bon_bill = new Billing();
            $bon_bill->user_id              = $refkey->user_id;
            $bon_bill->type_replenishment   = Billing::TYPE_REFERAL;
            $bon_bill->transfer_sum         = $rf_bonus;
            $bon_bill->currency             = Billing::TYPE_CURRENCY;
            $bon_bill->desc                 = $rule->description.' за пользователя '.$user->fullFIO;
            $bon_bill->save();            
            
            //создание для пользователя записи
            $uphistory = new RefHistory();
            $uphistory->user_id         = $user->id;
            $uphistory->parent_id       = $refkey->user_id;
            $uphistory->referal_level   = $level;
            
            $uphistory->operation_value = $this->billing->transfer_sum;
            $uphistory->value           = $rf_bonus;

            $uphistory->rules_id        = $rule->id;
            $uphistory->rules_desc      = $rule->toArray();

            $uphistory->billing_orders_id = $bon_bill->id;

            //сохраняем результат ))
            $uphistory->save();
        
        
        }
    }
}
