<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Subscribers\Subscribers;

class addNewSubscribleAllFromEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $name;
    

    
    /**
     * Create a new job instance.
     * @param type $email
     * @param type $name
     */
    public function __construct($email, $name = '')
    {
        $this->email = \trim(strip_tags($email));
        $this->name = \trim(strip_tags($name));
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if(empty($this->email)) { return;}
        
        foreach (array_keys(Subscribers::getAllType()) as $type) {
            \App\Http\Controllers\Admin\Subscribers\subscribersController::addSubcriberEmailNameType($this->email,$this->name,$type);
        }
        
    }
}
