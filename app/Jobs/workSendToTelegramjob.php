<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;


use App\Models\Blog\Blog;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class workSendToTelegramjob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $obj;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($obj)
    {
        $this->obj = $obj;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if(!config('vlavlat.telegram_api_send_message')) { return; }
        
        if ($this->obj instanceof Blog) {
            $this->sendToTelegram($this->obj->title);
        }
        
        if (is_string($this->obj)) {
            $this->sendToTelegram($this->obj);
        }
        
    }

    /**
     * Отправка сообщения в телеграмм
     * @param type $text
     */
    protected function sendToTelegram($text) {
        $url = config('vlavlat.telegram_api_send_message');

        $client = new Client();
        $uri = $url.'&text='.$text;                 //Адрес запроса по api
        $res = $client->get($uri)->getBody(); 
        
        Log::info('ВЫполнен запрос на телеграмм, адрес ',['uri'=>$uri]);
        /* https://api.telegram.org/bot590140961:AAFceUnLRC1TR64VdosiD--tN-NZ_Xvg_jQ/sendMessage?chat_id=-1001150354768&text=Hello */
    }
}
