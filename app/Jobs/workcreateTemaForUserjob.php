<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;


use App\Models\Callback\Callback;
use App\Models\Chats\Chats;
use App\User;

use Illuminate\Support\Facades\Log;

class workcreateTemaForUserjob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var User
     */
    protected $user;
    
    protected $sender_id;
    
    protected $param;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user,$sender_id,$param)
    {
        $this->user     = $user;
        $this->sender_id= $sender_id;
        $this->param    = $param;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
        $callback = new Callback();
        $callback->user_id  = $this->user->id;
        $callback->subject  = strip_tags($this->param['subject']);
        $callback->message  = strip_tags($this->param['message']);
        $callback->sadmin   = 1;
        $callback->save();
        
        $add_chat = new Chats();
        $add_chat->sender_id    = $this->sender_id;
        $add_chat->recipient_id = $this->user->id;
        $add_chat->status       = 0;
        $add_chat->sadmin       = 1;
        $add_chat->message      = strip_tags($this->param['message']);
        
        $callback->chats()->save($add_chat);
        
    }

    /**
     * Отправка сообщения в телеграмм
     * @param type $text
     */
    protected function sendToTelegram($text) {
        $url = config('vlavlat.telegram_api_send_message');

        $client = new Client();
        $uri = $url.'&text='.$text;                 //Адрес запроса по api
        $res = $client->get($uri)->getBody(); 
        
        Log::info('ВЫполнен запрос на телеграмм, адрес ',['uri'=>$uri]);
        /* https://api.telegram.org/bot590140961:AAFceUnLRC1TR64VdosiD--tN-NZ_Xvg_jQ/sendMessage?chat_id=-1001150354768&text=Hello */
    }
}
