<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class LecturerOrAdminMiddleware
{
    /**
     * Пропустим всех авторизированных пользователей, кроме кадетов
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if (Auth::check() && (Auth::user()->isCadet() || Auth::user()->isСurator())) {return back()->with('warning', trans('message.hack_message'));}
        
        if(Auth::check()){
            return $next($request);
        }else{
            return redirect( url('/'));
        }
    }
}
