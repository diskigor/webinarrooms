<?php

namespace App\Http\Middleware;

use App\Models\BlackList\BlackList;
use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        if (Auth::guard($guard)->check()) {
            return $next($request);

        }

        $check_blak_list = BlackList::get();
        $check = true;
        $reason = '';
        foreach ($check_blak_list as $user) {
            if ($user->user->email === $request->email) {
                $check = false;
                $reason = $user->reason;
            }
        }
        $confirm_check = User::where('email',$request->email)->first();
        if($confirm_check and $confirm_check->confirmed === 0){
            return  redirect('/')->with('status','Ваш профиль не активирован. Подвердите почту перейдя по ссылке в письме!');
        }
        if ($check) {
            return $next($request);
        } else {
            return redirect(route('block_user',$reason));
        }

    }
}
