<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cookie;
use App\Models\Billing\RefKeys;


class CheckReferalCook
{
    /**
     * Проверяем наличие параметра referal во всех запросах,
     * при наличии пишем в одноимённуюю куку.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $name_cook_ref = config('vlavlat.referalkey');
        if ($request->{$name_cook_ref}) {
            //проверка на существование такого реферального кода
            if(RefKeys::where('hashkey',$request->{$name_cook_ref})->first()) {
                if (!request()->cookie($name_cook_ref)) {
                    Cookie::queue(Cookie::make($name_cook_ref, $request->{$name_cook_ref}, 2628000));
                }
            } else {
                Cookie::queue(Cookie::forget($name_cook_ref));
            }
        }
        return $next($request);
    }
}
