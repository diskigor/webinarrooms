<?php
namespace App\Http\Controllers\Users\Cataloglectures;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Curses\Curse;

/**
 * Description of audienceController
 *
 * @author vlavlat
 */
class cataloglecturesController extends Controller 
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    
    
    /**
     * Метод вывода представления
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $main_curses = Curse::active()->gilbo()->get();
        
        $lecturer_curses = Curse::active()->Lecturer()->get();
        
        //dd($main_curses);
        
        return view('users.catalog.index',[
            'main_curses'       => $main_curses,
            'lecturer_curses'   => $lecturer_curses,
            
            
        ]);
    }
    
}
