<?php

namespace App\Http\Controllers\Users\Converters;

use App\Models\Billing\Balls;
use App\Models\Billing\Billing;
use App\User;

use App;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ballToKelController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');

    }


    public function forInclude(Request $request) {
        //проверил запрос на адекватность, нужное дело
        if(!$request->has('convert_balls')) { return response()->json(['errors'=>'Введите корректное значение Баллов для осуществления конвертации в КЭЛы']); }
        //приведение к целому числу
        $balls = (int)$request->input('convert_balls');
        //проверил диапазон допустимых значений
        if ($balls<=0 || $balls>$request->user()->level_point) { return response()->json(['errors'=>'Введите корректное значение Баллов для осуществления конвертации в КЭЛы']); }
        //вычислил кэлность запроса, бля что я тут делаю ?!
        $kel = round($balls*config('vlavlat.balls_to_kel_convert_curse'), 4);
        
        $ballance = $this->addBalance($request->user(), $balls, $kel);
        
        $this->addBalls($ballance, $balls, $kel);
        
        //и пусть все там охренеют ;)
        return response()->json([ 'forms' => view('users.converters.ball_to_kel.index_part_form')->render() ]);
    }

    
    /**
     * Запись в баланс пополнения от конвертации
     * @param User $user
     * @param type $balls
     * @param type $kel
     * @return Billing
     */
    protected function addBalance(User $user, $balls, $kel) {
        $ballance                     = new Billing();
        $ballance->user_id            = $user->id;
        $ballance->type_replenishment = Billing::TYPE_BALLS_TO_MONEY;
        $ballance->transfer_sum       = abs($kel);
        $ballance->currency           = Billing::TYPE_CURRENCY;
        $ballance->admin_id           = NULL;
        $ballance->desc   = 'Конвертация '.$balls.' баллов в '.$kel.' КЭЛ';
        $ballance->save();
        return $ballance;
    }
    
    /**
     * Снятие баллов после конвертации
     * @param Request $request
     * @param User $user
     * @return Balls|boolean
     */
    protected function addBalls(Billing $ballance, $balls, $kel){
        $new_ball           = new Balls();
        $new_ball->type     = Balls::$FOR_BALLS_TO_MONEY;
        $new_ball->user_id  = $ballance->user_id;
        $new_ball->value    = -abs($balls);
        $new_ball->description = 'Конвертация '.$balls.' баллов в '.$kel.' КЭЛ';
        $ballance->balls()->save($new_ball);
        return $new_ball;
    }
    
    
    /**
     * Метод вывода представления
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index(Request $request)
    {

    }

    
    /**
     * Метод обновления 
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }
    
    

    /**
     * Метод вывода представления
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function show($slug)
    {

    }
}
