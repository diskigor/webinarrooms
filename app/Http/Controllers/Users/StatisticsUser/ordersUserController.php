<?php

namespace App\Http\Controllers\Users\StatisticsUser;

use App\Models\Billing\Billing;
use App\Models\Billing\Orders;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

class ordersUserController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth'); 
    }
    
    /**
     * Метод вывода статистики пользователя
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Orders::where('user_id',Auth::user()->id)->orderBy('id','desc')->paginate(15);
        return view('users.statistics.orders',compact('orders'));
    }

    public function replenishmentUserStatistic(){
        $biling_user_orders = Billing::where('user_id',Auth::user()->id)->orderBy('id','desc')->paginate(15);
        return view('users.statistics.biling_user_statistic',compact('biling_user_orders'));

    }

}
