<?php
namespace App\Http\Controllers\Users\Educational;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Models\UserAnswer\TextAnswer;

use App\Models\Curses\Curse;
use App\Models\Prelections\Prelection;
use App\Models\Shop\buyUsersCourses;


/**
 * Description of audienceController
 *
 * @author vlavlat
 */
class audienceController extends Controller 
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    
    
    /**
     * Метод вывода представления
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        //текстовые ответы
        $count_t_a_comment = TextAnswer::withCount('comments')->where('user_id',$request->user()->id)->get();
        $count_text_answer = $count_t_a_comment->isNotEmpty()?$count_t_a_comment->count():0;
        $count_text_answer_comment = $count_t_a_comment->isNotEmpty()?$count_t_a_comment->sum('comments_count'):0;
        
        
        
        //Главные курсы
        $main_user_c_courses = buyUsersCourses::with('course')
                ->whereHas('course', function($q){ $q->gilbo(); })
                ->where('user_id', Auth::id())->get();   
                
        $main_user_courses_complite_count = 0;
        $main_user_courses_work_count = 0;
        
        foreach ($main_user_c_courses as $c) {
            if (($c->course->progressCurse($request->user()->id)?$c->course->progressCurse($request->user()->id)->Closed:FALSE)){
                ++$main_user_courses_complite_count;
            } else {
                ++$main_user_courses_work_count;
            }

        }
        $main_user_courses_last =  $main_user_c_courses->take(2);            
        $main_user_courses_count = $main_user_c_courses->count();
        
        
        //Лекторские курсы
        $lecture_user_c_courses = buyUsersCourses::with('course')
                ->whereHas('course', function($q){ $q->Lecturer(); })
                ->where('user_id', Auth::id())->get();   
                
        if($lecture_user_c_courses->isEmpty()) { $lecture_user_c_courses=$main_user_c_courses; }        
                
        $lecture_user_courses_complite_count = 0;
        $lecture_user_courses_work_count = 0;
        
        foreach ($lecture_user_c_courses as $c) {
            if (($c->course->progressCurse($request->user()->id)?$c->course->progressCurse($request->user()->id)->Closed:FALSE)){
                ++$lecture_user_courses_complite_count;
            } else {
                ++$lecture_user_courses_work_count;
            }

        }
        $lecture_user_courses_last =  $lecture_user_c_courses->take(2);            
        $lecture_user_courses_count = $lecture_user_c_courses->count();
        
        
        //открытые материалы
        $open_user_c_courses = buyUsersCourses::with('course')
                ->whereHas('course', function($q){ $q->Discount100(); })
                ->where('user_id', Auth::id())->get();   
                
        if($open_user_c_courses->isEmpty()) { $lecture_user_c_courses=$main_user_c_courses; }        
        
        $open_user_courses_complite_count = 0;
        $open_user_courses_work_count = 0;
        
        foreach ($open_user_c_courses as $c) {
            if (($c->course->progressCurse($request->user()->id)?$c->course->progressCurse($request->user()->id)->Closed:FALSE)){
                ++$open_user_courses_complite_count;
            } else {
                ++$open_user_courses_work_count;
            }

        }
        $open_user_courses_last =  $open_user_c_courses->take(2);            
        $open_user_courses_count = $open_user_c_courses->count();
        
        
        return view('users.educational.index',[
            'count_text_answer'             => $count_text_answer,
            'count_text_answer_comment'     => $count_text_answer_comment,
            
            'main_user_courses_count'           => $main_user_courses_count,
            'main_user_courses_complite_count'  => $main_user_courses_complite_count,
            'main_user_courses_work_count'      => $main_user_courses_work_count,
            'main_user_courses_last'            => $main_user_courses_last,
            
            'lecture_user_courses_count'            => $lecture_user_courses_count,
            'lecture_user_courses_complite_count'   => $lecture_user_courses_complite_count,
            'lecture_user_courses_work_count'       => $lecture_user_courses_work_count,
            'lecture_user_courses_last'             => $lecture_user_courses_last,
            
            'open_user_courses_count'            => $open_user_courses_count,
            'open_user_courses_complite_count'   => $open_user_courses_complite_count,
            'open_user_courses_work_count'       => $open_user_courses_work_count,
            'open_user_courses_last'             => $open_user_courses_last,
            
        ]);
    }
    
}
