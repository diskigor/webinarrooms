<?php

namespace App\Http\Controllers\Users;

use App\Models\Blog\Blog;
use Lecturize\Tags\Models\Tag;

use App;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class blogController extends Controller
{
    /**
     * @var array
     */
    protected $all_tags;
    
    /**
     * @var array
     */
    protected $all_authors;

    /**
     * @var string
     */
    protected $local;
    
    public function __construct()
    {
        $this->middleware('auth')->except('free_index','free_show','search');
        $this->local = App::getLocale();
        
        //$this->all_tags = Tag::all(['tag'])->pluck('tag')->toArray();
        $this->all_tags = Blog::LecturerGilbo()->with('tags')->has('tags')->get()->pluck('tags')->flatten()->pluck('tag')->unique()->toArray();
        
        $this->all_authors = Blog::LecturerGilbo()->get()->pluck('author')->unique();
    }

    
    /**
     * Метод вывода представления со всеми статьями фронте, без регистрации
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function free_index()
    {
//        $blogs = collect();
//        
//        //Блог Гильбо
//        $blogs['Блог Гильбо'] = Blog::where('lang_local',$this->local)->gilbo()->orderBy('created_at','desc')->limit(3)->get();
//        foreach($blogs['Блог Гильбо'] as $bb){ $bb->linktoroute = route('front_blog_gilbo_show',$bb->slug); }
//        $blogs['Блог Гильбо']->linktoroute = $blogs['Блог Гильбо']->isNotEmpty()?route('front_blog_gilbo'):false;
//        $blogs['Блог Гильбо']->textNameGroup       = 'Блог Гильбо';
//        $blogs['Блог Гильбо']->blog_newcounts      = Blog::CountNew('gilbo');
//        
//        //Блоги Лекторов
//        $blogs['Блоги Лекторов'] = Blog::where('lang_local',$this->local)->lecturer()->orderBy('created_at','desc')->limit(3)->get();
//        foreach($blogs['Блоги Лекторов'] as $bb){ $bb->linktoroute = route('front_blog_lecturer_show',$bb->slug); }
//        $blogs['Блоги Лекторов']->linktoroute = $blogs['Блоги Лекторов']->isNotEmpty()?route('front_blog_lecturer'):false;
//        $blogs['Блоги Лекторов']->textNameGroup       = 'Блоги Лекторов';
//        $blogs['Блоги Лекторов']->blog_newcounts      = Blog::CountNew('lecturer');
//        
//        //Гайдпарк
//        $blogs['Гайдпарк'] = Blog::where('lang_local',$this->local)->gaypark()->orderBy('created_at','desc')->limit(3)->get();
//        foreach($blogs['Гайдпарк'] as $bb){ $bb->linktoroute = route('front_blog_gaypark_show',$bb->slug); }
//        $blogs['Гайдпарк']->linktoroute = $blogs['Гайдпарк']->isNotEmpty()?route('front_blog_gaypark'):false;
//        $blogs['Гайдпарк']->textNameGroup       = 'Гайдпарк';
//        $blogs['Гайдпарк']->blog_newcounts      = Blog::CountNew('gaypark');
//        
//        return view('front.blog.list', ['blogs'=>$blogs,'tags'=>$this->all_tags]);
        
        $blogs = Blog::LecturerGilbo()->orderBy('created_at','desc')->paginate(9);
        foreach($blogs as $bb){ $bb->linktoshow = route('front_blog_show',['slug'=>$bb->slug]); }
        $blogs->linktoroute         = route('front_blog_index');
        $blogs->linktoroutesearch   = route('front_blog_search');
        $blogs->blog_newcounts      = Blog::CountNew();
        
        
        return view('front.blog.list_lecture_Gilbo', ['blogs'=>$blogs,'tags'=>$this->all_tags, 'authors' => $this->all_authors]);
        
    }

    /**
     * Метод вывода представления со всеми статьями
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index()
    {
//        $blogs = collect();
//        
//        //Блог Гильбо
//        $blogs['Блог Гильбо'] = Blog::where('lang_local',$this->local)->gilbo()->orderBy('created_at','desc')->limit(3)->get();
//        foreach($blogs['Блог Гильбо'] as $bb){ $bb->linktoroute = route('blog_gilbo.show',$bb->slug); }
//        $blogs['Блог Гильбо']->linktoroute = $blogs['Блог Гильбо']->isNotEmpty()?route('blog_gilbo.index'):false;
//        $blogs['Блог Гильбо']->textNameGroup       = 'Блог Гильбо';
//        $blogs['Блог Гильбо']->blog_newcounts      = Blog::CountNew('gilbo');
//        
//        //Блоги Лекторов
//        $blogs['Блоги Лекторов'] = Blog::where('lang_local',$this->local)->lecturer()->orderBy('created_at','desc')->limit(3)->get();
//        foreach($blogs['Блоги Лекторов'] as $bb){ $bb->linktoroute = route('blog_lecturer.show',$bb->slug); }
//        $blogs['Блоги Лекторов']->linktoroute = $blogs['Блоги Лекторов']->isNotEmpty()?route('blog_lecturer.index'):false;
//        $blogs['Блоги Лекторов']->textNameGroup       = 'Блоги Лекторов';
//        $blogs['Блоги Лекторов']->blog_newcounts      = Blog::CountNew('lecturer');
//        
//        //Гайдпарк
//        $blogs['Гайдпарк'] = Blog::where('lang_local',$this->local)->gaypark()->orderBy('created_at','desc')->limit(3)->get();
//        foreach($blogs['Гайдпарк'] as $bb){ $bb->linktoroute = route('blog_gaidpark.show',$bb->slug); }
//        $blogs['Гайдпарк']->linktoroute = $blogs['Гайдпарк']->isNotEmpty()?route('blog_gaidpark.index'):false;
//        $blogs['Гайдпарк']->textNameGroup       = 'Гайдпарк';
//        $blogs['Гайдпарк']->blog_newcounts      = Blog::CountNew('gaypark');
//        
//        //dd($blogs);
//        return view('users.blog.list', ['blogs'=>$blogs,'tags'=>$this->all_tags]);
        
        $blogs = Blog::LecturerGilbo()->orderBy('created_at','desc')->paginate(9);
        foreach($blogs as $bb){ $bb->linktoshow = route('blog.show',['slug'=>$bb->slug]); }
        $blogs->linktoroute         = route('blog.index');
        $blogs->linktoroutesearch   = route('ablog.search');
        $blogs->blog_newcounts      = Blog::CountNew();
        
        
        return view('users.blog.list_lecture_Gilbo', ['blogs'=>$blogs,'tags'=>$this->all_tags, 'authors' => $this->all_authors]);
    }

    /**
     * Метод вывода представления одной статьи блога
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function free_show($slug)
    {
        $local = App::getLocale();
        $blog = Blog::where('slug',$slug)->where('lang_local',$local)->first();
        
        if(!$blog) { return redirect()->back()->with('warning', 'Неизвестная статья блога'); }

        $blog->increment('count_view');
        
        return view('front.blog.show_lecture_Gilbo', [
            'blog'=>$blog,
            'linktolist'=> route('blog.index'),
            'linktoshow'=> route('blog.show',$blog->slug)]);
    }
    
    /**
     * Метод вывода представления одной статьи блога для админки
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function show($slug)
    {
        $blog = Blog::where('slug',$slug)->where('lang_local',App::getLocale())->first();
        
        if(!$blog) { return redirect()->back()->with('warning', 'Неизвестная статья блога'); }
//        
//        if($blog->isGilbo) {
//            return redirect()->route('blog_gilbo.show',$blog->slug);
//        }
//        if($blog->isLecturer) {
//            return redirect()->route('blog_lecturer.show',$blog->slug);
//        }
//        
//        if($blog->isGaypark) {
//            return redirect()->route('blog_gaidpark.show',$blog->slug);
//        }
        
        $blog->increment('count_view');
        
        return view('users.blog.show_lecture_Gilbo', [
            'blog'=>$blog,
            'linktolist'=> route('blog.index'),
            'linktoshow'=> route('blog.show',$blog->slug)]);
        
        //return redirect()->back()->with('warning', 'Неизвестный автор блога');
        
    }
    
    
    /**
     * 
     * @param Request $request
     * @return type
     */
    public function search(Request $request) {
        $blogs = Blog::with('likes')->where('lang_local',$this->local)->LecturerGilbo();
        
        //работаем с автором
        if($request->has('author_id') &&
                !in_array($request->input('author_id'), $this->all_authors->pluck('id')->toArray()) ) {
            return redirect()->back()->with('warning', 'Выбран не существующий автор');
        } elseif($request->has('author_id')) { 
            $author_id = $request->input('author_id'); 
            $blogs->where('author_id',$author_id);
        } else { $author_id = FALSE; }
        
        //работаем с тегом
        if($request->has('tag') && !in_array($request->input('tag'), $this->all_tags)) {
            return redirect()->back()->with('warning', 'Указан не существующий тег "' . $request->input('tag') . '"');
        } elseif($request->has('tag')) { $tag = $request->input('tag'); $blogs->WithTag($tag);
        } else { $tag = FALSE; }

        //работаем с сортировкой по лайкам
        if ($request->has('sort_likes') && $request->input('sort_likes') == 'desc') {   
            $blogs->withCount('likes')->orderBy('likes_count','desc');
        }
        
        //работаем с сортировкой по дате
        if ($request->has('sort_created') && $request->input('sort_created') == 'asc') {
            $blogs->orderBy('created_at','asc');
        } else { $blogs->orderBy('created_at','desc'); }
        
        //работаем со строкой поиска
        if ($request->has('textsearch')) {   
            $blogs->where('article','like','%'.$request->input('textsearch').'%');
        }
        
        $blogs = $blogs->paginate(9)->appends($request->input());

        
        if($blogs->isEmpty()) { return redirect()->back()->with('warning', 'Нет статей по данному запросу'); }
        
        $blogs->blog_newcounts      = Blog::CountNew();
        
        if($request->routeIs('front_blog_search')) {
            foreach($blogs as $bb){ $bb->linktoshow = route('front_blog_show',['slug'=>$bb->slug]); }
            $blogs->linktoroute = route('front_blog_index');
            $blogs->linktoroutesearch = route('front_blog_search');
            return view('front.blog.list_lecture_Gilbo', ['blogs'=>$blogs,'tag'=>$tag,'tags'=>$this->all_tags, 'authors' => $this->all_authors,'author_id'=>$author_id]);
        } else {
            foreach($blogs as $bb){ $bb->linktoshow = route('blog.show',['slug'=>$bb->slug]); }
            $blogs->linktoroute         = route('blog.index');
            $blogs->linktoroutesearch   = route('ablog.search');
            return view('users.blog.list_lecture_Gilbo', ['blogs'=>$blogs,'tag'=>$tag,'tags'=>$this->all_tags, 'authors' => $this->all_authors,'author_id'=>$author_id]);
        }
    }
}
