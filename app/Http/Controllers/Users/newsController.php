<?php

namespace App\Http\Controllers\Users;

use App\Models\News\News;
use App\Models\News\VideoNews;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class newsController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::latest()->paginate(12);
        return view('users.news.list',compact('news'));
    }


    /**
     * Метод вывода представления статьи подробней
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show($id)
    {
       $news = News::find($id);
       
       if(is_null($news)){ return redirect()->route('unews.index'); }
       
       return view('users.news.show',compact('news'));
    }

    /**
     * Метод вывода представления вывода видео новости
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function videoNews($id){
        $video_news = VideoNews::find($id);
        
        if(is_null($video_news)){ return redirect()->route('unews.index'); }
        
        return view('users.news.showVideoNews',compact('video_news'));
    }

}
