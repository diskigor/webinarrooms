<?php

namespace App\Http\Controllers\Users\Blogs;

use App\Models\Blog\Blog;
use Lecturize\Tags\Models\Tag;
use Lecturize\Tags\Models\Taggable;

use App;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class gayparkController extends Controller
{
    /**
     * @var array
     */
    protected $all_tags;
    
    /**
     * @var string
     */
    protected $local;


    protected $blogName = 'Блог Гайдпарк';


    public function __construct()
    {
        
        $this->middleware('admin')->except('free_index','free_show','search');
        
        $this->local = App::getLocale();
        //TODO
        $bs = Blog::where('lang_local',$this->local)->gaypark()->get(['id'])->pluck('id');
        $tag_ids = Taggable::whereIn('taggable_id', $bs)->where('taggable_type', 'App\Models\Blog\Blog')->pluck('tag_id')->unique();
        
        $this->all_tags = Tag::whereIn('id',$tag_ids)->pluck('tag')->toArray();

    }

    
    /**
     * Метод вывода представления со всеми статьями фронте, без регистрации
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function free_index()
    {
        $blogs = Blog::where('lang_local',$this->local)->gaypark()->orderBy('created_at','desc')->paginate(9);
        foreach($blogs as $bb){ $bb->linktoshow = route('front_blog_gaypark_show',['slug'=>$bb->slug]); }
        $blogs->linktoroute         = route('front_blog_gaypark');
        $blogs->linktoroutesearch   = route('front_blog_gaypark_search');
        $blogs->textNameGroup       = $this->blogName;
        $blogs->blog_newcounts      = Blog::CountNew('gaypark');
        return view('front.blog.list_one_cat', ['blogs'=>$blogs,'tags'=>$this->all_tags]);
    }

    /**
     * Метод вывода представления со всеми статьями
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index()
    {
        $blogs = Blog::where('lang_local',$this->local)->gaypark()->orderBy('created_at','desc')->paginate(9);
        foreach($blogs as $bb){ $bb->linktoshow = route('blog_gaidpark.show',['slug'=>$bb->slug]); }
        $blogs->linktoroute         = route('blog_gaidpark.index');
        $blogs->linktoroutesearch   = route('ablog_gaypark.search');
        $blogs->textNameGroup       = $this->blogName;
        $blogs->blog_newcounts      = Blog::CountNew('gaypark');
        return view('users.blog.list_one_cat', ['blogs'=>$blogs,'tags'=>$this->all_tags]);
    }

    /**
     * Метод вывода представления одной статьи блога
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function free_show($slug)
    {
        $blog = Blog::where('slug',$slug)->where('lang_local',$this->local)->gaypark()->first();
        
        if(!$blog) {
            return redirect()->back()->with('warning', 'Неизвестная статья блога');
        }
    $blog->textNameGroup = $this->blogName;
        return view('front.blog.show', [
            'blog'=>$blog,
            'linktolist'=> route('blog_gaidpark.index'),
            'linktoshow'=> route('blog_gaidpark.show',$blog->slug)]);
    }
    
    /**
     * Метод вывода представления одной статьи блога
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function show($slug)
    {
        $blog = Blog::where('slug',$slug)->where('lang_local',$this->local)->gaypark()->first();
        
        if(!$blog) {
            return redirect()->back()->with('warning', 'Неизвестная статья блога');
        }
        $blog->textNameGroup = $this->blogName;
        return view('users.blog.show', [
            'blog'=>$blog,
            'linktolist'=> route('blog_gaidpark.index'),
            'linktoshow'=> route('blog_gaidpark.show',$blog->slug)]);
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function search(Request $request) {
        $blogs = Blog::with('likes')->where('lang_local',$this->local)->gaypark();
        
        //работаем с тегом
        if($request->has('tag') && !in_array($request->input('tag'), $this->all_tags)) {
            return redirect()->back()->with('warning', 'Указан не существующий тег "' . $request->input('tag') . '"');
        } elseif($request->has('tag')) { $tag = $request->input('tag'); $blogs->WithTag($tag);
        } else { $tag = FALSE; }

        //работаем с сортировкой по лайкам
        if ($request->has('sort_likes') && $request->input('sort_likes') == 'desc') {   
            $blogs->withCount('likes')->orderBy('likes_count','desc');
        }
        
        //работаем с сортировкой по дате
        if ($request->has('sort_created') && $request->input('sort_created') == 'asc') {
            $blogs->orderBy('created_at','asc');
        } else { $blogs->orderBy('created_at','desc'); }
        
        //работаем со строкой поиска
        if ($request->has('textsearch')) {   
            $blogs->where('article','like','%'.$request->input('textsearch').'%');
        }
        
        $blogs = $blogs->paginate(9)->appends($request->input());

        
        if($blogs->isEmpty()) { return redirect()->back()->with('warning', 'Нет статей по данному запросу'); }
        
        $blogs->textNameGroup   = $this->blogName;
        $blogs->blog_newcounts  = Blog::CountNew('gaypark');
        
        if($request->routeIs('front_blog_gaypark_search')) {
            foreach($blogs as $bb){ $bb->linktoshow = route('front_blog_gaypark_show',['slug'=>$bb->slug]); }
            $blogs->linktoroute = route('front_blog_gaypark');
            $blogs->linktoroutesearch = route('front_blog_gaypark_search');
            return view('front.blog.list_one_cat', ['blogs'=>$blogs,'tag'=>$tag,'tags'=>$this->all_tags]);
        } else {
            foreach($blogs as $bb){ $bb->linktoshow = route('blog_gaidpark.show',['slug'=>$bb->slug]); }
            $blogs->linktoroute = route('blog_gaidpark.index');
            $blogs->linktoroutesearch = route('ablog_gaypark.search');
            return view('users.blog.list_one_cat', ['blogs'=>$blogs,'tag'=>$tag,'tags'=>$this->all_tags]);
        }
    }

}
