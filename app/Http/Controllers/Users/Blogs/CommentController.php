<?php

namespace App\Http\Controllers\Users\Blogs;

use Illuminate\Http\Request;
use Validator;
use Auth;
use App\Models\Comment;
use App\Models\Blog\Blog;


class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Метод, для администратора, список комментариев к конкретному блогу
     * @param int $id_blog
     * @return view
     */
    public function index($id_blog) {
        $blog = Blog::findOrFail($id_blog);
        
        if(!request()->user()->isAdmin() && $blog->author_id !=  request()->user()->id){
            return back()->with('warning','Вы не можете редактировать комментарии!');
        }
        
        $comments = Comment::where('blog_id',$id_blog)->orderBy('created_at','desc')->paginate(20);
        return view('Admin.admin.blog.blogListComments',  compact(['blog','comments']));
    }
    
    /**
     * Метод, для администратора, удаление комментария
     * @param int $id_blog
     * @param int $id_comment
     * @return redirect
     */
    public function destroy($id_blog,$id_comment) {
        $blog = Blog::findOrFail($id_blog);
        $comment = Comment::findOrFail($id_comment);
        if($comment->blog_id != $blog->id) { return back()->with('warning','Комментарий не к этой статье!'); }
        
        if(!request()->user()->isAdmin() && $blog->author_id !=  request()->user()->id){
            return back()->with('warning','Вы не можете удалить этот комментарий!');
        }
        
        $fix = Comment::where('parent_id',$comment->id)->update(['parent_id'=>null]);
        
        $comment->delete();
        return back()->with('success','Комментарий успешно удален!');
    }

    
    /**
     * Метод записи комментария к блогу, доступен всем зарегестрированным пользователям
     * @param Request $request
     * @return json
     */
    public function store(Request $request){


        $data = array();

        $data['blog_id'] = (int)$request->input('comment_blog_ID');
        $data['parent_id'] = ((int)$request->input('comment_parent',0))?(int)$request->input('comment_parent'):null;
        $data['text'] = $request->input('text');

        //устанавливаем статус в зависимости от настройки
        $data['status'] = config('comments.show_immediately');

        $data['user_id']    = Auth::user()->id;

        //Проверка
        $validator = Validator::make($data,[
                'blog_id' => 'integer|required',
                'text' => 'required',

        ]);

        //Ошибки
        if ($validator->fails()) {	
                /*
                 * Возвращаем ответ в формате json.
                 * Метод all() переводит в массив т.к. данный формат работает или
                 * с объектами или с массивами
                 */
                return \Response::json(['error'=>$validator->errors()->all()]);
        }


        //получаем модель записи к которой принадлежит комментарий
        $blog = Blog::find($data['blog_id']);
        if(!$blog) { return \Response::json(['error'=>'Не известный блог!']); }

        
        //Проверка: если блог Гильбо, то только члены клуба могут коментировать
        if($blog->isGilbo && !$request->user()->checkEntrance()) {
            return \Response::json(['error'=>['Тольк члены клуба, могут оставлять комментарии!']]);
        }
        

        /*
         * Создаем объект для сохранения, передаем ему массив данных
         */
        $comment = new Comment($data); 
        /*
         * Сохраняем данные в БД
         * Используем связывающий метод comments()
         * для того, чтобы автоматически заполнилось поле post_id
         */
        $blog->comments()->save($comment);


        /*
         * Формируем массив данных для вывода нового комментария с помощью AJAX
         * сразу после его добавления (без перезагрузки)
         */
        $data['id']     = $comment->id;
        $data['name']   = Auth::user()->name;
        $data['avatar'] = Auth::user()->avatar;
        $data['status'] = config('comments.show_immediately');

        //Вывод шаблона сохраняем в переменную
        $view_comment = view(env('THEME').'.comments.new_comment')->with('data', $data)->render();

        //Возвращаем AJAX вывод шаблона  с данными
        return \Response::json(['success'=>true, 'comment'=>$view_comment, 'data'=>$data]);

    }
}
