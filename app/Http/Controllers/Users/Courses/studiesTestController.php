<?php

namespace App\Http\Controllers\Users\Courses;

use App\Models\Tasks\Explanation;
use App\Models\Tasks\Test;
use App\Models\UserAnswer\TestUserAnswer;
use App\Models\Billing\Balls;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Users\Courses\mainStudiesController;

class studiesTestController extends Controller
{
    protected $steps_study_lection;
    protected $steps_study_lection_full;
    protected $steps_study_lection_need;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth'); 
    }
    

    /**
     * Список Тестов лекции
     * @param Request $request
     * @param $id Номер лекции
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show(Request $request,$id)
    {
        $lecture = mainStudiesController::valid_user_lection($request->user(),$id);
        if (is_string($lecture)) {
                if(request()->isXmlHttpRequest()) { return response()->json(['errors'=>$lecture]); }
                    return response()->back()->with('warning', $lecture);
            }
        
        if($lecture->test_tasks->isEmpty() && request()->isXmlHttpRequest()) { return request()->json(FALSE); }//TODO что делать если нет Тестов
       foreach ($lecture->test_tasks as $test) {
            if ($test->type_id == 2 
                    && isset($test->testQuestions) &&  !$test->testQuestions->isEmpty()){
                $test->questions_group = $test->testQuestions->groupBy('group_number');
            } elseif ($test->type_id == 2) {
                $test->questions_group = null;
            }
        }
        return view('users.courses._partials.tab_lection_tests',['lecture'=>$lecture]);
    }

    
    /**
     * Метод записи ответов пользователя на ТЕСТ
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        if (!$request->isXmlHttpRequest()) { return back()->with('warning', 'Ошибка!Неверный формат запроса!'); }
        
        $test = Test::find($request->input('test_id', 0));
        if (! $test) { return response()->json(['info'=>'Ошибка!Тест не найден, сообщите администратору сайта!']);}
        if (! $test->typeTest) { return response()->json(['info'=>'Ошибка! У теста не определён его ТИП']);}
        
        $lecture = mainStudiesController::valid_user_lection($request->user(),$test->lectures_id);
        if (is_string($lecture)) {return response()->json(['info'=>$lecture]);}
        
        if ($test->typeTest->name == 'boolean') { return $this->BooleanAnswerTestTaskUser($request, $test); }
        
        if ($test->typeTest->name == 'group') { return $this->GroupAnswerTestTaskUser($request, $test); }
        
        if ($test->typeTest->name == 'concatenation') { return $this->СoncatenationAnswerTestTaskUser($request, $test); }
        
        return response()->json(['info'=>'ТЕСТ, неизвестный статус/тип ((']);    
    }

    /**
     * Матод записи/обновления результатов прохождения теста, пользователем
     * @param Test $test
     * @param array $answer
     * @return string
     */
    protected function addOrUpdateUserAnswer(Test $test, array $answer) {
            $finish_test = TestUserAnswer::firstOrNew(['user_id' => Auth::user()->id, 'test_id' => $test->id,'prelection_id'=>$test->lectures_id]);
            $finish_test->status = 1;
            $finish_test->descr_answer = \GuzzleHttp\json_encode($answer);
            
            $text_balls = '';
            
            //только для первого прохождения теста начисляем баллы
            if (!isset($finish_test->id)) {
                $new_ball = new Balls();
                $new_ball->type     = Balls::$FOR_TEST;
                $new_ball->user_id  = Auth::user()->id;
                $new_ball->value    = abs($test->point);
                $new_ball->description    = 'Прохождение теста "'.$test->name.'"';
                $test->balls()->save($new_ball);
                $text_balls = 'Вы получили: <b>'. $test->point . '</b> балл(-ов-а)';
                }
                
            $finish_test->save();
            return $text_balls;
    }
    
    
    
    
    
    
    
    /**
     * Метод записи ответа на тест первого типа пользователя, boolean
     * @param Request $request
     * @param Test $test
     * @return string
     */
    protected function BooleanAnswerTestTaskUser(Request $request,Test $test)
    {

        if(!is_array($request->answer)) { return ['errors'=>'Ошибка, нет ответов.']; }

        $user_answer = $this->parseAnswerBooleanTest($request, $test);
        
        if (isset($user_answer['errors'])) { return ['errors'=>$user_answer['errors']]; }

        $description_UA = $this->explanationSearchDescription($test, $user_answer);
        
        $user_answer['description'] = null;
        
        if ($description_UA && $description_UA instanceof Explanation) {
            $user_answer['description'] = $description_UA->description;
        } elseif ($description_UA) {
            foreach ($description_UA as $expla) {
                $user_answer['description'] .= '<br/>'.$expla->description;
            }
            
        }
        
        //dd('$description_UA',$description_UA,$user_answer);
        $text_balls = $this->addOrUpdateUserAnswer($test, $user_answer);

        return ['success'=>'Тест пройден<br> Результаты в блоке лекции', 'text'=>$text_balls];
    }
    

    
    /**
     * Метод проверяет ответы пользователя, на тест первого типа
     * @param Request $request
     * @param Test $test
     * @return array
     */
    protected function parseAnswerBooleanTest(Request $request, Test $test) {
        $user_answer = []; $req_answer   = $request->input('answer',null);

        foreach ($test->testQuestions as $tquestion) {
            $user_answer[$tquestion->id]['balls'] = 0;

            foreach ($tquestion->testAnswerQuestion as $tanswer) {

                if(!isset($req_answer[$tquestion->id])) { $user_answer['errors'] = 'Необходимо ответить на все вопросы'; break; }

                if ($tquestion->button_type == 'radio' && $req_answer[$tquestion->id] == $tanswer->id) {
                    $user_answer[$tquestion->id] = [
                        
                        'balls'             => $tanswer->is_right,
                        'question_number'   => $tquestion->number,
                        'answer'            => strip_tags($tanswer->answer),
                        'answer_id'         => $tanswer->id,
                        'answer_number'     => $tanswer->number];
                }

                if ($tquestion->button_type == 'checkbox' && is_array($req_answer[$tquestion->id]) && isset($req_answer[$tquestion->id][$tanswer->id])) {
                    $user_answer[$tquestion->id][$tanswer->id] = ['balls' => $tanswer->is_right,'answer' => strip_tags($tanswer->answer),'answer_id' => $tanswer->id];
                    $user_answer[$tquestion->id]['balls'] +=$tanswer->is_right;
                }

            }

        }

//                if ($question->button_type == 'radio') {
//                    foreach ($question->testAnswerQuestion as $answer) {
//                        $user_answer[$question->id]['balls'] = 0;
//                        if (isset($request->answer[$question->id]) && $answer->id == $request->answer[$question->id]) {
//                            //$user_answer[$question->id]['answer']       = $answer->answer;
//                            //$user_answer[$question->id]['answer_id']    = $answer->id;
//                            //$user_answer[$question->id]['balls']        = $answer->is_right;
//                            $user_answer[$question->id] = ['balls' => $answer->is_right,'answer' => strip_tags($answer->answer),'answer_id' => $answer->id];
//                        }
//                    }
//                    
//                } else if (isset($request->answer[$question->id]) && is_array($request->answer[$question->id]) && $question->button_type === 'checkbox') {
//                    $user_answer[$question->id]['balls'] = 0;
//                    foreach ($question->testAnswerQuestion as $answer) {
//                        if (isset($request->answer[$question->id][$answer->id])) {
//                            //$user_answer[$question->id][$answer->id]['answer'] = $answer->answer;
//                            //$user_answer[$question->id][$answer->id]['balls'] = $answer->is_right;
//                            $user_answer[$question->id][$answer->id] = ['balls' => $answer->is_right,'answer' => strip_tags($answer->answer),'answer_id' => $answer->id];
//                            $user_answer[$question->id]['balls'] +=$answer->is_right;
//                        }
//                    }
//                } 

        $user_answer['balls'] = \array_sum (\array_column($user_answer, 'balls')); 
        //dd($test->testQuestions,'$req_answer',$req_answer,'$user_answer',$user_answer);
        return $user_answer;
    }


    
    
    
    
    /**
     * Метод проверки ответа пользователя на групповой тест и записи в БД
     * @param Request $request
     * @param Test $itest
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function GroupAnswerTestTaskUser(Request $request, Test $itest)
    {
        $test = Test::with(['testQuestions'=>function($query){
                $query->orderBy('group_number', 'asc')->orderBy('number', 'asc');
                $query->with(['testAnswerQuestion'=>function($q){
                    $q->orderBy('number', 'asc');
                }]);
            }])->find($itest->id);
        
        if (!$test) { return ['errors'=>'Возникла ошибка при поиске теста.']; }
        if(!is_array($request->input('group_answer'))) { return ['errors'=>'Ошибка, нет ответов']; }
        
        $test->questions_group = $test->testQuestions->groupBy('group_number');
        
        $user_answer = $this->parseAnswerGroupTest($request, $test);
        
        if (isset($user_answer['errors'])) { return ['errors'=>$user_answer['errors']]; }
        
        $description_UA = $this->explanationSearchDescription($test, $user_answer);
        
        //dd($user_answer,$description_UA);
        
        $user_answer['description'] = $description_UA?$description_UA->description:null;

        $text_balls = $this->addOrUpdateUserAnswer($test, $user_answer);
        
        return ['success'=>'Тест пройден<br> Результаты в блоке лекции', 'text'=>$text_balls];
        
        
//        $explan = Explanation::where('test_id', $id)->pluck('description', 'key_test');
//        $answer_str = "";
//        foreach ($request->group_answer as $items) {
//            $question_key = array_keys($items);
//            if (count($question_key) < 2) {
//                return back()->with('warning', 'Вы пропустили один блок ответов!');
//            }
//            if (count($items[$question_key[0]]) > count($items[$question_key[1]])) {
//                $answer_str .= $question_key[0];
//            } else {
//                $answer_str .= $question_key[1];
//            }
//        }
//        if (isset($explan[$answer_str])) {
//            $answer = $explan[$answer_str];
//        } else {
//            return back()->with('warning', 'Данно ответа нет в базе.');
//        }
//        $add_answer_user = new TestUserAnswer();
//        $add_answer_user->user_id = Auth::user()->id;
//        $add_answer_user->test_id = $test->id;
//        $add_answer_user->prelection_id = $test->lectures_id;
//        $add_answer_user->status = 1;
//        $add_answer_user->descr_answer = $answer;
//        $add_answer_user->save();
        
        
 //       return back()->with('success', 'Вы успешно прошли тест!');
    }

    /**
     * Метод исчет ключи для теста и проверяет ответы пользователя в завсисимости от типа ключей
     * @param Test $test
     * @param array $user_answer
     * @return Explanation || null
     */
    protected function explanationSearchDescription(Test $test, array $user_answer) {
        //посик ключей в ответах на тест
        $explans = Explanation::where('test_id', $test->id)->get();
        if ($explans->isEmpty()) return null;
        
        $result = null;
        foreach ($explans as $exp) {
            $type = explode(';', $exp->key_test);
            if (count($type) != 2) { continue; } //в условии нет ключа / ключ неверный
            
            switch ((int)$type[0]) {
                case 1:
                    $result = $this->explanationType_1($exp, $user_answer);
                    break;
                case 2:
                    $result_arr = $this->explanationType_2($exp, $user_answer);
                    if ($result_arr) { $result[]=$result_arr; }
                    break;
                case 10:
                    $result = $this->explanationType_10($exp, $user_answer );
                default :
                    break;
            }

            if ($result && ($result instanceof Explanation)) { break; }
        }
        return $result;
    }
    
    
    
    /**
     * Метод проверки ответа пользователя по 1 типу ключа, для тестов типа boolean
     * 1;0-20
     * 1;21
     * по диапазону суммы баллов на все ответы
     * @param Explanation $exp
     * @param array $user_answer
     * @return Explanation || null
     */
    protected function explanationType_1(Explanation $exp, array $user_answer ) {
        $type   = explode(';', $exp->key_test);
        $in_rules  = explode('-', $type[1]);
        
        $balls = array_sum (array_column($user_answer, 'balls'));
        //dd($user_answer,$balls);
        if (\count($in_rules) == 2) {
            if ((int)$in_rules[0]<=$balls && $balls<=(int)$in_rules[1]) {
                return $exp;
            }
        }
        
        if (\count($in_rules) == 1) {
            if ((int)$in_rules[0]<=$balls) {
                return $exp;
            }
        }

        return null;       
        //dd('explanationType10',$user_answer,$rules,$exp);
    }
    
    /**
     * Метод проверки ответа пользователя по 1 типу ключа, для тестов типа boolean
     * 1;0-20
     * 1;21
     * по диапазону суммы баллов на все ответы
     * @param Explanation $exp
     * @param array $user_answer
     * @return Explanation || null
     */
    protected function explanationType_2(Explanation $exp, array $user_answer ) {
        $type   = explode(';', $exp->key_test);
        $in_rules  = explode(':', $type[1]);
        if (\count($in_rules) !== 3) { return null; }
        
        list(,$fromto, $listan) = $in_rules;
        $min_max = explode('-', $fromto);
        $list_answer = explode(',', $listan);
        
        $balls = 0;
        foreach ($user_answer as $ua) {
            if (in_array($ua['question_number'], $list_answer)) {
               $balls += $ua['balls'];
            }
        }
        
        if ($min_max[0] <= $balls && $balls <= $min_max[1]) {
            //dd('return $exp',$exp);
            return $exp;
        }

        //dd('2 type',$group,$min_max, $list_answer,$user_answer,$balls);
                
        return null;
   
        //dd('explanationType10',$user_answer,$rules,$exp);
    }
    
    
    /**
     * Метод проверки ответа пользователя по 10 типу ключа
     * 10;[1:1],[2:2],[2:1],[1:1]
     * в первой группе больше баллов у вопроса 1
     * во второй группу больше баллов у вопроса 2
     * @param Explanation $exp
     * @param array $user_answer
     * @return Explanation || null
     */
    protected function explanationType_10(Explanation $exp, array $user_answer ) {
        $type   = explode(';', $exp->key_test);
        $in_rules  = explode(',', $type[1]);
        $rules = []; $oreder = ['[',']'];
        
        foreach ($in_rules as $rul) {
            $temp = explode(':', str_replace($oreder,'',$rul));
            $rules[] = ['group'=>isset($temp[0])?(int)$temp[0]:0,'question'=>isset($temp[1])?(int)$temp[1]:0];
        }
        
        foreach ($rules as $rul) {
            if (!isset($user_answer[$rul['group']])) { return null;}            //в ответе нет требуемой группы
            if (!isset($user_answer[$rul['group']][$rul['question']])) { return null;} //в ответе нет требуемого вопроса
            
            $maxquest = 0; $maxval  = 0;
            
            foreach ($user_answer[$rul['group']] as $k=>$qa) {
                if (!isset($qa['balls'])) { continue; }
                if ($qa['balls']>$maxval) {
                    $maxval = $qa['balls']; $maxquest = $k;
                } 
            }
            
            if ($maxquest != $rul['question'])     { return null;}              //не совпал номер вопроса с макисальным балом в группе
            
        }
        
        return $exp;//правило прошло все проверки
        //dd('explanationType10',$user_answer,$rules,$exp);
    }
    
    
    
    /**
     * Метод разбирает ответы пользователя, на тест второго типа
     * @param Request $request
     * @param Test $test
     * @return array
     */
    protected function parseAnswerGroupTest(Request $request, Test $test) {
        $user_answer    = []; $group_answer   = $request->input('group_answer');
        
        //dd($test->questions_group,$group_answer);
        
        foreach ($test->questions_group as $key=>$g_question) {
            if (!isset($group_answer[$key])) { $user_answer['errors'] = 'Нет ответов на группу вопросов'; break; }
            
            foreach ($g_question as $question) {
                if (!isset($group_answer[$key][$question->number])) {
                    $user_answer[$key][$question->number] = $question->button_type == 'radio'?null:['balls' => 0,'answer' => null,'answer_id' => null]; continue; }
                
                
                foreach ($question->testAnswerQuestion as $answer) {
                    if (isset($group_answer[$key][$question->number][$answer->id])){
                        $user_answer[$key][$question->number][$answer->id] = ['balls' => $answer->is_right,'answer' => strip_tags($answer->answer),'answer_id' => $answer->id];
                    }
                }
                
                $user_answer[$key][$question->number]['balls']      = array_sum (array_column($user_answer[$key][$question->number], 'balls'));
                $user_answer[$key][$question->number]['question']   = strip_tags($question->question);
                $user_answer[$key][$question->number]['question_id']= $question->id;
            }
            
            $user_answer[$key]['balls']      = array_sum (array_column($user_answer[$key], 'balls'));
            
        }
        $user_answer['balls'] = \array_sum (\array_column($user_answer, 'balls'));
        return $user_answer;
    }
    

    
    
    
    
    
    
    
    
    
    
    
    /**
     * ТЕСТ типа конкатенатион?
     * @param Request $request
     * @param Test $test
     * @return type
     */
    protected function СoncatenationAnswerTestTaskUser(Request $request,Test $test)
    //public function addAnswerConcatTest(Request $request, $id)
    {
        $explan = Explanation::where('test_id', $test->id)->pluck('description', 'key_test');
        if ($explan->count() === 0) { return ['errors'=>'Наданный момент нельзя пройти тест.']; }
        
        $answer_str = "";

        foreach ($request->answer as $value) {
            $question_key = array_keys($value);
            $answer_str .= $question_key[0];
        }
        
        if (isset($explan[$answer_str])) { $user_answer = $explan[$answer_str];
        } else { return ['errors'=>'Данного ответа нет в базе.'];  }

        
        $text_balls = $this->addOrUpdateUserAnswer($test, $user_answer);

        return ['success'=>'Тест пройден<br> Результаты в блоке лекции', 'text'=>$text_balls];
    }


}
