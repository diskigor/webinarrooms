<?php

namespace App\Http\Controllers\Users\Courses;


use App\Models\Tasks\TextTasks;
use App\Models\UserAnswer\TextAnswer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;


use Validator;

class studiesTextController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth'); 
    }
    

    /**
     * Список Текстовые задания лекции
     * @param Request $request
     * @param $id Номер лекции
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show(Request $request,$id)
    {
        $lecture = mainStudiesController::valid_user_lection($request->user(),$id);
        if (is_string($lecture)) {
            if(request()->isXmlHttpRequest()) { return request()->json(['errors'=>$lecture]); }
            return back()->with('warning', $lecture);}
        
        if($lecture->text_tasks->isEmpty() && request()->isXmlHttpRequest()) { return request()->json(FALSE); }//TODO что делать если нет Текстовых задания
        
        return view('users.courses._partials.tab_lection_text_tasks',['lecture'=>$lecture]);
    }
    
    

    /**
     * Метод записи ответа на письменный тест приложения
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $validator = Validator::make($request->all(), [ 'lecture_id'        => 'required',
                                                        'text_task_id'      => 'required',
                                                        'answer_text_tasks' => 'required',
        ], [ 'answer_text_tasks.required' => 'Вы не ответили на текстовое задание!' ]);
        
        if ($validator->fails()) { return response()->json(['errors' => $validator->errors()->first()]); }
      
        $lecture = mainStudiesController::valid_user_lection($request->user(),$request->get('lecture_id'));
        if (is_string($lecture)) {
            if($request->isXmlHttpRequest()) { return response()->json(['errors'=>$lecture]); }
            return back()->withInput()->with('warning', $lecture);}

        $text_tasks = TextTasks::find($request->get('text_task_id'));
        if(!$text_tasks) { 
            if($request->isXmlHttpRequest()) { return response()->json(['errors'=>'Не найдено текстовое задание, сообщите администратору.']); }
            return back()->withInput()->with('warning', 'Не найдено текстовое задание, сообщите администратору.'); }

        if (TextAnswer::where('task_id', $request->get('text_task_id'))->where('user_id', $request->user()->id)->first()) {
            if($request->isXmlHttpRequest()) { return response()->json(['errors'=>'Вы уже проходили данный тест!']); }
            return back()->with('warning', 'Вы уже проходили данный тест!');
        }
           
        $add_answer = new TextAnswer();
        $add_answer->user_id = $request->user()->id;
        $add_answer->task_id = $request->get('text_task_id');
        $add_answer->answer = stripcslashes(strip_tags($request->get('answer_text_tasks')));
        $add_answer->lang_local = App::getLocale();
        $add_answer->save();

        if($request->isXmlHttpRequest()) { return response()->json(['success'=>'Ответ отправлен', 'text'=>'*Комментарии экспертов Вы можете отслеживать в разделе <a href="'.route('my_works.index').'">"Мои задания"</a>']); }
        return back()->with('success', 'Ответ отправлен2');

    }

    /**
     * Метод редактирования ответа пользователем.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $edit_text_answer = TextAnswer::find($id);
        if ($edit_text_answer) {
            $this->validate($request, [
                'edit_answer_text_tasks' => 'required',
            ], [
                'edit_answer_text_tasks.required' => 'Вы не ответили на текстовое задание!'
            ]);
            $edit_text_answer->answer = stripcslashes(strip_tags($request->edit_answer_text_tasks));
            $edit_text_answer->status = 0;
            $edit_text_answer->save();
            return back()->with('success', 'Ваш ответ принят.Когда его проверять Вы сможете продолжить обучение.');
        }
        return back()->with('warning', 'Ошибка, редактирования ответа, сообщите администратору сайта!.');
    }


}
