<?php

namespace App\Http\Controllers\Users\Courses;

use App\Models\Curses\Curse;
use App\Models\Prelections\Prelection;
use App\Models\Shop\buyUsersCourses;
use App\Models\Shop\Progress;
use App\Models\Tasks\Explanation;
use App\Models\Tasks\Test;
use App\Models\Tasks\TextTasks;
use App\Models\UserAnswer\TestUserAnswer;
use App\Models\UserAnswer\TextAnswer;
use App\Models\Billing\Balls;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class studiesController extends Controller
{
    protected $steps_study_lection;
    protected $steps_study_lection_full;
    protected $steps_study_lection_need;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth'); 
    }
    
    /**
     * Список купленных пользователем курсов
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_courses = buyUsersCourses::where('user_id', Auth::id())->get();
        return view('users.courses.studiesShowCourse', compact('user_courses'));
    }

    /**
     * Метод вывода лекций купленого пользователем курса
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show($id)
    {
        $user_course_buy_check = buyUsersCourses::where('curses_id', $id)->where('user_id', Auth::id())->first();
        if ($user_course_buy_check) {
            $course = Curse::with('lectures')->find($id);
            if ($course) {
                //dd($course->lastLectionID());
                //dd(Auth::user()->progressCurse($course->id)->allStepsCount());
                return view('users.courses.detailsCourseUser', compact('course'));
            }
            return back()->with('warning', 'Ошибка! Курс не найден сообщите администратору сайта');
        }
        return redirect(route('user_curses.index'));
    }

    /**
     * Метод вывода лекции для прохождения пользователем-
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function showLecture($id)
    {
        //проверка наличия данной лекции
        $lecture = Prelection::find($id);
        if (!$lecture) { return back()->with('warning', 'Ошибка! Лекция не найдена сообщите администратору сайта!'); }
        
        //проверка принадлежность лекции к купленным курсам пользователя
        if(!Auth::user()->checkBuyCourse($lecture->curse_id)) { return back()->with('warning', 'Лекция не доступна!'); }
        
        //проверка последовательности прохождения лекций
        if(!in_array($id, Auth::user()->progressCurse($lecture->curse_id)->currentSteps())) {
             return back()->with('warning', 'Сейчас лекция не доступна! Важна последовательность в изучении метриала'); 
        }


        //проверка прохождения и инициализация
//        $this->recordStepsStudyLection($lecture,request()->get('steps_study_lection'));
//        $lecture->steps_study_lection       = $this->steps_study_lection;
//        $lecture->steps_study_lection_full  = $this->steps_study_lection_full;
//        $lecture->steps_study_lection_need  = $this->steps_study_lection_need;
//        
//        dd($lecture);
        
        $lecture->load(['test_tasks.testQuestions'=>function($query){
            $query->orderBy('group_number', 'asc')->orderBy('number', 'asc');
            $query->with(['testAnswerQuestion'=>function($q){
                $q->orderBy('number', 'asc');
            }]);
        }]);//вытянули все связи, если они есть
        
        
        foreach ($lecture->test_tasks as $test) {
            if ($test->type_id == 2 
                    && isset($test->testQuestions) &&  !$test->testQuestions->isEmpty()){
                $test->questions_group = $test->testQuestions->groupBy('group_number');
            } elseif ($test->type_id == 2) {
                $test->questions_group = null;
            }
        }

        //dd($lecture->test_tasks->first(),$lecture->test_tasks->first()->userTestAnswer->jsonAnswer);
        return view('users.courses.studiesShowPrelection', compact('lecture'));
    }


    /**
     * 
     * @param Prelection $lecture
     * @return void
     */
    protected function recordStepsStudyLection(Prelection $lecture, $approved_record = array()) {
        if (is_null($this->steps_study_lection_full)) { $this->steps_study_lection_full = array(); }
        if (is_null($this->steps_study_lection)) { $this->steps_study_lection = array(); }
        
        //найти все типы прикреплённых материалов к лекции и записать в массив требуемых
        $this->steps_study_lection_full['lecture'] = $lecture->id;
        
        foreach ($lecture->materials as $material){
            $this->steps_study_lection_full[$material->type][] = $material->id;
            }
        foreach ($lecture->text_tasks as $text_task){
            $this->steps_study_lection_full['task'][] = $text_task->id;
            if($text_task->answerUserText) { $this->steps_study_lection['task'][] = $text_task->id; }
            }
        foreach ($lecture->test_tasks as $test){
            $this->steps_study_lection_full['test'][] = $test->id;
            if(isset($test->userTestAnswer) && isset($test->userTestAnswer->jsonAnswer)){ $this->steps_study_lection['test'][] = $test->id; }
            }


            
        
    }
    
    
    /**
     * Метод записи ответа на письменный тест приложения
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function addAnswerTextTaskUser(Request $request, $id)
    {
        $text_tasks = TextTasks::find($id);
        if(is_null($text_tasks)) { return back()->withInput()->with('warning', 'Произошла ошибка в системе! Сообщите администратору'); }

            $this->validate($request, [
                'answer_text_tasks' => 'required',
            ], [
                'answer_text_tasks.required' => 'Вы не ответили на текстовое задание!'
            ]);
            $text_answer_user = stripcslashes(strip_tags($request->answer_text_tasks));
            $text_answer = TextAnswer::where('task_id', $id)->where('user_id', Auth::user()->id)->first();
            if ($text_answer) {
                return back()->with('warning', 'Вы уже проходили данный тест!');
            }
            $add_answer = new TextAnswer();
            $add_answer->user_id = Auth::user()->id;
            $add_answer->task_id = $id;
            $add_answer->answer = $text_answer_user;
            $add_answer->lang_local = App::getLocale();
            $add_answer->save();
            //$this->recordStepsStudyLection($text_tasks->prelection,$request()->get('steps_study_lection'));
            return back()->with('success', 'Ответ отправлен');

    }

    /**
     * Метод записи ответа на тест первого типа пользователя
     */
    public function addAnswerTestTaskUser(Request $request, $id)
    {
        $test = Test::with('testQuestions')->find($id);
        if (!$test) { return back()->with('warning', 'Возникла ошибка при поиске теста'); }
        if(!is_array($request->answer)) { return back()->with('warning', 'Ошибка, нет ответов'); }

        $user_answer = $this->parseAnswerBooleanTest($request, $test);
        
        if (isset($user_answer['errors'])) { return back()->with('warning', $user_answer['errors']); }

        $description_UA = $this->explanationSearchDescription($test, $user_answer);
        
        $user_answer['description'] = null;
        
        if ($description_UA && $description_UA instanceof Explanation) {
            $user_answer['description'] = $description_UA->description;
        } elseif ($description_UA) {
            foreach ($description_UA as $expla) {
                $user_answer['description'] .= '<br/>'.$expla->description;
            }
            
        }
        
        //dd('$description_UA',$description_UA,$user_answer);
        $text_balls = $this->addOrUpdateUserAnswer($test, $user_answer);

        return back()->with(['status'=>'Тест пройден. Результаты на странице учебного дня', 'text'=>$text_balls]);
    }
    
    /**
     * Матод записи/обновления результатов прохождения теста, пользователем
     * @param Test $test
     * @param array $answer
     * @return string
     */
    protected function addOrUpdateUserAnswer(Test $test, array $answer) {
            $finish_test = TestUserAnswer::firstOrNew(['user_id' => Auth::user()->id, 'test_id' => $test->id,'prelection_id'=>$test->lectures_id]);
            $finish_test->status = 1;
            $finish_test->descr_answer = \GuzzleHttp\json_encode($answer);
            
            $text_balls = '';
            
            //только для первого прохождения теста начисляем баллы
            if (!isset($finish_test->id)) {
                $new_ball = new Balls();
                $new_ball->type     = Balls::$FOR_TEST;
                $new_ball->user_id  = Auth::user()->id;
                $new_ball->value    = abs($test->point);
                $new_ball->description    = 'Прохождение теста "'.$test->name.'"';
                $test->balls()->save($new_ball);
                $text_balls = 'Вы получили: <b>'. $test->point . '</b> балл(-ов-а)';
                }
                
            $finish_test->save();
            return $text_balls;
    }
    
    /**
     * Метод проверяет ответы пользователя, на тест первого типа
     * @param Request $request
     * @param Test $test
     * @return array
     */
    protected function parseAnswerBooleanTest(Request $request, Test $test) {
        $user_answer = []; $req_answer   = $request->input('answer',null);

        foreach ($test->testQuestions as $tquestion) {
            $user_answer[$tquestion->id]['balls'] = 0;

            foreach ($tquestion->testAnswerQuestion as $tanswer) {

                if(!isset($req_answer[$tquestion->id])) { $user_answer['errors'] = 'Необходимо ответить на все вопросы'; break; }

                if ($tquestion->button_type == 'radio' && $req_answer[$tquestion->id] == $tanswer->id) {
                    $user_answer[$tquestion->id] = [
                        
                        'balls'             => $tanswer->is_right,
                        'question_number'   => $tquestion->number,
                        'answer'            => strip_tags($tanswer->answer),
                        'answer_id'         => $tanswer->id,
                        'answer_number'     => $tanswer->number];
                }

                if ($tquestion->button_type == 'checkbox' && is_array($req_answer[$tquestion->id]) && isset($req_answer[$tquestion->id][$tanswer->id])) {
                    $user_answer[$tquestion->id][$tanswer->id] = ['balls' => $tanswer->is_right,'answer' => strip_tags($tanswer->answer),'answer_id' => $tanswer->id];
                    $user_answer[$tquestion->id]['balls'] +=$tanswer->is_right;
                }

            }

        }

//                if ($question->button_type == 'radio') {
//                    foreach ($question->testAnswerQuestion as $answer) {
//                        $user_answer[$question->id]['balls'] = 0;
//                        if (isset($request->answer[$question->id]) && $answer->id == $request->answer[$question->id]) {
//                            //$user_answer[$question->id]['answer']       = $answer->answer;
//                            //$user_answer[$question->id]['answer_id']    = $answer->id;
//                            //$user_answer[$question->id]['balls']        = $answer->is_right;
//                            $user_answer[$question->id] = ['balls' => $answer->is_right,'answer' => strip_tags($answer->answer),'answer_id' => $answer->id];
//                        }
//                    }
//                    
//                } else if (isset($request->answer[$question->id]) && is_array($request->answer[$question->id]) && $question->button_type === 'checkbox') {
//                    $user_answer[$question->id]['balls'] = 0;
//                    foreach ($question->testAnswerQuestion as $answer) {
//                        if (isset($request->answer[$question->id][$answer->id])) {
//                            //$user_answer[$question->id][$answer->id]['answer'] = $answer->answer;
//                            //$user_answer[$question->id][$answer->id]['balls'] = $answer->is_right;
//                            $user_answer[$question->id][$answer->id] = ['balls' => $answer->is_right,'answer' => strip_tags($answer->answer),'answer_id' => $answer->id];
//                            $user_answer[$question->id]['balls'] +=$answer->is_right;
//                        }
//                    }
//                } 

        $user_answer['balls'] = \array_sum (\array_column($user_answer, 'balls')); 
        //dd($test->testQuestions,'$req_answer',$req_answer,'$user_answer',$user_answer);
        return $user_answer;
    }


    /**
     * Метод проверки ответа пользователя на групповой тест и записи в БД
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addAnswerGroupTest(Request $request, $id)
    {
        $test = Test::with(['testQuestions'=>function($query){
                $query->orderBy('group_number', 'asc')->orderBy('number', 'asc');
                $query->with(['testAnswerQuestion'=>function($q){
                    $q->orderBy('number', 'asc');
                }]);
            }])->find($id);
        
        if (!$test) { return back()->with('warning', 'Возникла ошибка при поиске теста'); }
        if(!is_array($request->input('group_answer'))) { return back()->with('warning', 'Ошибка, нет ответов'); }
        
        $test->questions_group = $test->testQuestions->groupBy('group_number');
        
        $user_answer = $this->parseAnswerGroupTest($request, $test);
        
        if (isset($user_answer['errors'])) { return back()->with('warning', $user_answer['errors']); }
        
        $description_UA = $this->explanationSearchDescription($test, $user_answer);
        
        //dd($user_answer,$description_UA);
        
        $user_answer['description'] = $description_UA?$description_UA->description:null;

        $text_balls = $this->addOrUpdateUserAnswer($test, $user_answer);
        
        return back()->with(['success'=>'Тест пройден. Результаты на странице учебного дня', 'text'=>$text_balls]);
        
//        $explan = Explanation::where('test_id', $id)->pluck('description', 'key_test');
//        $answer_str = "";
//        foreach ($request->group_answer as $items) {
//            $question_key = array_keys($items);
//            if (count($question_key) < 2) {
//                return back()->with('warning', 'Вы пропустили один блок ответов!');
//            }
//            if (count($items[$question_key[0]]) > count($items[$question_key[1]])) {
//                $answer_str .= $question_key[0];
//            } else {
//                $answer_str .= $question_key[1];
//            }
//        }
//        if (isset($explan[$answer_str])) {
//            $answer = $explan[$answer_str];
//        } else {
//            return back()->with('warning', 'Данно ответа нет в базе.');
//        }
//        $add_answer_user = new TestUserAnswer();
//        $add_answer_user->user_id = Auth::user()->id;
//        $add_answer_user->test_id = $test->id;
//        $add_answer_user->prelection_id = $test->lectures_id;
//        $add_answer_user->status = 1;
//        $add_answer_user->descr_answer = $answer;
//        $add_answer_user->save();
        
        
 //       return back()->with('success', 'Вы успешно прошли тест!');
    }

    /**
     * Метод исчет ключи для теста и проверяет ответы пользователя в завсисимости от типа ключей
     * @param Test $test
     * @param array $user_answer
     * @return Explanation || null
     */
    protected function explanationSearchDescription(Test $test, array $user_answer) {
        //посик ключей в ответах на тест
        $explans = Explanation::where('test_id', $test->id)->get();
        if ($explans->isEmpty()) return null;
        
        $result = null;
        foreach ($explans as $exp) {
            $type = explode(';', $exp->key_test);
            if (count($type) != 2) { continue; } //в условии нет ключа / ключ неверный
            
            switch ((int)$type[0]) {
                case 1:
                    $result = $this->explanationType_1($exp, $user_answer);
                    break;
                case 2:
                    $result_arr = $this->explanationType_2($exp, $user_answer);
                    if ($result_arr) { $result[]=$result_arr; }
                    break;
                case 10:
                    $result = $this->explanationType_10($exp, $user_answer );
                default :
                    break;
            }

            if ($result && ($result instanceof Explanation)) { break; }
        }
        return $result;
    }
    
    
    
    /**
     * Метод проверки ответа пользователя по 1 типу ключа, для тестов типа boolean
     * 1;0-20
     * 1;21
     * по диапазону суммы баллов на все ответы
     * @param Explanation $exp
     * @param array $user_answer
     * @return Explanation || null
     */
    protected function explanationType_1(Explanation $exp, array $user_answer ) {
        $type   = explode(';', $exp->key_test);
        $in_rules  = explode('-', $type[1]);
        
        $balls = array_sum (array_column($user_answer, 'balls'));
        //dd($user_answer,$balls);
        if (\count($in_rules) == 2) {
            if ((int)$in_rules[0]<=$balls && $balls<=(int)$in_rules[1]) {
                return $exp;
            }
        }
        
        if (\count($in_rules) == 1) {
            if ((int)$in_rules[0]<=$balls) {
                return $exp;
            }
        }

        return null;       
        //dd('explanationType10',$user_answer,$rules,$exp);
    }
    
    /**
     * Метод проверки ответа пользователя по 1 типу ключа, для тестов типа boolean
     * 1;0-20
     * 1;21
     * по диапазону суммы баллов на все ответы
     * @param Explanation $exp
     * @param array $user_answer
     * @return Explanation || null
     */
    protected function explanationType_2(Explanation $exp, array $user_answer ) {
        $type   = explode(';', $exp->key_test);
        $in_rules  = explode(':', $type[1]);
        if (\count($in_rules) !== 3) { return null; }
        
        list(,$fromto, $listan) = $in_rules;
        $min_max = explode('-', $fromto);
        $list_answer = explode(',', $listan);
        
        $balls = 0;
        foreach ($user_answer as $ua) {
            if (in_array($ua['question_number'], $list_answer)) {
               $balls += $ua['balls'];
            }
        }
        
        if ($min_max[0] <= $balls && $balls <= $min_max[1]) {
            //dd('return $exp',$exp);
            return $exp;
        }

        //dd('2 type',$group,$min_max, $list_answer,$user_answer,$balls);
                
        return null;
   
        //dd('explanationType10',$user_answer,$rules,$exp);
    }
    
    
    /**
     * Метод проверки ответа пользователя по 10 типу ключа
     * 10;[1:1],[2:2],[2:1],[1:1]
     * в первой группе больше баллов у вопроса 1
     * во второй группу больше баллов у вопроса 2
     * @param Explanation $exp
     * @param array $user_answer
     * @return Explanation || null
     */
    protected function explanationType_10(Explanation $exp, array $user_answer ) {
        $type   = explode(';', $exp->key_test);
        $in_rules  = explode(',', $type[1]);
        $rules = []; $oreder = ['[',']'];
        
        foreach ($in_rules as $rul) {
            $temp = explode(':', str_replace($oreder,'',$rul));
            $rules[] = ['group'=>isset($temp[0])?(int)$temp[0]:0,'question'=>isset($temp[1])?(int)$temp[1]:0];
        }
        
        foreach ($rules as $rul) {
            if (!isset($user_answer[$rul['group']])) { return null;}            //в ответе нет требуемой группы
            if (!isset($user_answer[$rul['group']][$rul['question']])) { return null;} //в ответе нет требуемого вопроса
            
            $maxquest = 0; $maxval  = 0;
            
            foreach ($user_answer[$rul['group']] as $k=>$qa) {
                if (!isset($qa['balls'])) { continue; }
                if ($qa['balls']>$maxval) {
                    $maxval = $qa['balls']; $maxquest = $k;
                } 
            }
            
            if ($maxquest != $rul['question'])     { return null;}              //не совпал номер вопроса с макисальным балом в группе
            
        }
        
        return $exp;//правило прошло все проверки
        //dd('explanationType10',$user_answer,$rules,$exp);
    }
    
    
    
    /**
     * Метод разбирает ответы пользователя, на тест второго типа
     * @param Request $request
     * @param Test $test
     * @return array
     */
    protected function parseAnswerGroupTest(Request $request, Test $test) {
        $user_answer    = []; $group_answer   = $request->input('group_answer');
        
        //dd($test->questions_group,$group_answer);
        
        foreach ($test->questions_group as $key=>$g_question) {
            if (!isset($group_answer[$key])) { $user_answer['errors'] = 'Нет ответов на группу вопросов'; break; }
            
            foreach ($g_question as $question) {
                if (!isset($group_answer[$key][$question->number])) {
                    $user_answer[$key][$question->number] = $question->button_type == 'radio'?null:['balls' => 0,'answer' => null,'answer_id' => null]; continue; }
                
                
                foreach ($question->testAnswerQuestion as $answer) {
                    if (isset($group_answer[$key][$question->number][$answer->id])){
                        $user_answer[$key][$question->number][$answer->id] = ['balls' => $answer->is_right,'answer' => strip_tags($answer->answer),'answer_id' => $answer->id];
                    }
                }
                
                $user_answer[$key][$question->number]['balls']      = array_sum (array_column($user_answer[$key][$question->number], 'balls'));
                $user_answer[$key][$question->number]['question']   = strip_tags($question->question);
                $user_answer[$key][$question->number]['question_id']= $question->id;
            }
            
            $user_answer[$key]['balls']      = array_sum (array_column($user_answer[$key], 'balls'));
            
        }
        $user_answer['balls'] = \array_sum (\array_column($user_answer, 'balls'));
        return $user_answer;
    }
    

    public function addAnswerConcatTest(Request $request, $id)
    {
        $test = Test::find($id);
        if (!$test) {
            return back()->with('warning', 'Возникла ошибка при поиске теста. Сообщите администратору сайта');
        }
        $explan = Explanation::where('test_id', $id)->pluck('description', 'key_test');
        if ($explan->count() === 0) {
            return back()->with('warning', 'Наданный момент нельзя пройти тест');
        }
        $answer_str = "";

        foreach ($request->answer as $value) {
            $question_key = array_keys($value);
            $answer_str .= $question_key[0];
        }
        if (isset($explan[$answer_str])) {
            $answer = $explan[$answer_str];
        } else {
            return back()->with('warning', 'Данного ответа нет в базе');
        }

        $add_answer_user = new TestUserAnswer();
        $add_answer_user->user_id = Auth::user()->id;
        $add_answer_user->test_id = $test->id;
        $add_answer_user->prelection_id = $test->lectures_id;
        $add_answer_user->status = 1;
        $add_answer_user->descr_answer = $answer;
        $add_answer_user->save();
        return back()->with('success', 'Вы успешно прошли тест');
    }

    /**
     * Метод редактирования ответа пользователем.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function editAnswerTextTaskUser(Request $request, $id)
    {
        $edit_text_answer = TextAnswer::find($id);
        if ($edit_text_answer) {
            $this->validate($request, [
                'edit_answer_text_tasks' => 'required',
            ], [
                'edit_answer_text_tasks.required' => 'Вы не ответили на текстовое задание!'
            ]);
            $edit_text_answer->answer = stripcslashes(strip_tags($request->edit_answer_text_tasks));
            $edit_text_answer->status = 0;
            $edit_text_answer->save();
            return back()->with('success', 'Ваш ответ принят.Когда его проверять Вы сможете продолжить обучение');
        }
        return back()->with('warning', 'Ошибка, редактирования ответа, сообщите администратору сайта!');
    }

    /**
     * Метод отвечающий за прохождения лекции и проверки на прохождения всех заданий лекции
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function finishPrelection($id)
    {
        //проверка наличия данной лекции
        $prelection = Prelection::find($id);
        if (!$prelection) { return back()->with('warning', 'Ошибка!Лекция не найдена сообщите администратору сайта!'); }
        
        //проверка принадлежность лекции к купленным курсам
        if(!Auth::user()->checkBuyCourse($prelection->curse_id)) { return back()->with('warning', 'Лекция не доступна!'); }
        
        //проверка последовательности прохождения лекций
        if(!in_array($id,Auth::user()->progressCurse($prelection->curse_id)->currentSteps())) {
             return back()->with('warning', 'Лекция не доступна! Важна последовательность в изучении метриала'); 
        }
        
        //$user = Auth::user();

        $test_tasks = Test::where('lectures_id', $prelection->id)->get();
        if ($test_tasks->count() != 0) {
            foreach ($test_tasks as $test_task) {
                $test_user_answers = TestUserAnswer::where('user_id', Auth::user()->id)
                    ->where('test_id', $test_task->id)
                    ->where('prelection_id', $prelection->id)
                    ->first();
                if ($test_user_answers) {
                    if ($test_user_answers->status != 1) {
                        return back()->with('warning', 'Вы не прошли тестовое задание');
                    }
                } else {return back()->with('warning', 'Вы не прошли тестовое задание');}
            }
        }
        
        $progress = Auth::user()->progressCurse($prelection->curse_id);

        //начисление баллов за прохождение лекции пользователем
        if(abs($prelection->price)>0) {
            $new_ball           = new Balls();
            $new_ball->type     = Balls::$FOR_LECTION;
            $new_ball->user_id  = Auth::user()->id;
            $new_ball->value    = abs($prelection->price);
            $new_ball->description    = 'Прохождение лекции "'.$prelection->name.'"';
            $prelection->balls()->save($new_ball);
        }
                
        $progress->pushFullStep($prelection->id);
        $next_lection = $progress->curse->nextLectionID($prelection->id);
        
        if ($next_lection) { $progress->pushNextStep($next_lection->id); }

        $progress->save();
        
        //--------Метод отправки сообщения на почту -----------------------        
        $this->prelectionMail(Auth::user(), $progress, $next_lection);
        
        if (!$next_lection && $progress->currentStepsCount()) {
            redirect(route('studies.show', $progress->curses_id))->with('success', 'Вы успешно завершили курс!');
        }
        return redirect(route('studies.show', $progress->curses_id))->with('success', 'Вы успешно завершили лекцию!');

    }

    /**
     * Метод отправки сообщения на почту об открытии лекции
     * @param User $user
     * @param $progress
     * @param $next_lection
     */
    public function prelectionMail(User $user, $progress, $next_lection )
    {
        $data['name'] = $user->name;
        $data['email'] = $user->email;
        
        if (!$next_lection && $progress->currentStepsCount()) {
            
            if($progress->curse->trans_curse[0]->short_desc_trans){
                $data['desc'] = $progress->curse->trans_curse[0]->name_trans;
            }else{
                $data['desc'] = ' ';
            }
            $data['id'] = $progress->curse->id;
            Mail::send('Admin.admin.mails.finish_prelection_mail', $data, function ($message) use ($data) {
                $message->to($data['email']);
                $message->subject('Успешное завершение курса!');
            });
            
        } else {

            if($next_lection->sdoLectureTrans()->short_desc){
                $data['desc'] = $next_lection->sdoLectureTrans()->short_desc;
            }else{
                $data['desc'] = ' ';
            }
            $data['id'] = $next_lection->id;
            Mail::send('Admin.admin.mails.next_prelection_mail', $data, function ($message) use ($data) {
                $message->to($data['email']);
                $message->subject('Доступна новая лекция!');
            });
        }

    }


    public  function testEmail()
    {
        $data['name'] = 'Test';
        $data['email'] = 'shadowgodsik@gmail.com';
        $data['desc'] = 'testing for email';
        $data['id'] = 'sdasd';
        Mail::send('Admin.admin.mails.next_prelection_mail', $data, function ($message) use ($data) {
            $message->to($data['email']);
            $message->subject('Результаты Вашего тестирования');
        });
    }

//    /**
//     * Update the specified resource in storage.
//     *
//     * @param  \Illuminate\Http\Request $request
//     * @param  int $id
//     * @return \Illuminate\Http\Response
//     */
//    public function update(Request $request, $id)
//    {
//        //
//    }


}
