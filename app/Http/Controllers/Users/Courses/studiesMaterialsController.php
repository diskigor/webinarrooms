<?php

namespace App\Http\Controllers\Users\Courses;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Controllers\Users\Courses\mainStudiesController;

/**
 * Дополнительные материалы для лекции
 */
class studiesMaterialsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth'); 
    }
    

    /**
     * Список Материалов лекции
     * @param Request $request
     * @param $id Номер лекции
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show(Request $request,$id)
    {
        $lecture = mainStudiesController::valid_user_lection($request->user(),$id);
        if (is_string($lecture)) {
            if(request()->isXmlHttpRequest()) { return response()->json(['errors'=>$lecture]); }
            return back()->with('warning', $lecture);}
        
        if($lecture->materials->isEmpty() && request()->isXmlHttpRequest()) { return request()->json(FALSE); }//TODO что делать если нет дополнительного материала
        
        return view('users.courses._partials.tab_lection_multimedia',['lecture'=>$lecture]);
    }

}
