<?php

namespace App\Http\Controllers\Users\Courses;

use App\Models\Curses\Curse;
use App\Models\Prelections\Prelection;
use App\Models\Shop\buyUsersCourses;

use App\Models\Billing\Balls;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Subscribers\Subscribers;
use App\Http\Controllers\Admin\Subscribers\subscribersController;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

use App\Http\Controllers\Users\Courses\mainStudiesController;

class lectureStudiesController extends Controller
{
    protected $steps_study_lection;
    protected $steps_study_lection_full;
    protected $steps_study_lection_need;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth'); 
    }
    
    /**
     * Список купленных пользователем ОСНОВНЫХ (ГИЛЬБО) курсов
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_courses = buyUsersCourses::with('course')
                ->whereHas('course', function($q){ $q->Lecturer(); })
                ->where('user_id', Auth::id())->get();        
        return view('users.courses.studies.lecturerCourse', compact('user_courses'));
    }

    /**
     * Метод вывода купленого пользователем курса и список лекций
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show($id)
    {
        $user_course_buy_check = buyUsersCourses::where('curses_id', $id)->where('user_id', Auth::id())->first();
        if(!$user_course_buy_check) { return back()->with('warning', 'Ошибка! Вы не покупали данный курс.'); }
        
        $course = Curse::with('lectures')->find($id);
        if(!$course) { return back()->with('warning', 'Ошибка! Курс не найден сообщите администратору сайта.'); }

        
        $current_step = $course->progressCurse(Auth::id())?$course->progressCurse(Auth::id())->currentStep():0;
        
        $current_lection = Prelection::find($current_step);

        $lection_study_need = $this->getLectionStudyNeed($current_lection);
        
        
        $route_type = 'lecture_studies';//fix
        
        //dd($current_step,$current_lection,$lection_study_need);
        //dd(Auth::user()->progressCurse($course->id)->allStepsCount());
        return view('users.courses.studies.showCourse', compact('course','current_step','lection_study_need','route_type'));

    }
    
    /**
     * 
     * @param Prelection $lecture
     * @return void
     */
    protected function getLectionStudyNeed(Prelection $lecture = NULL) {
        if(!$lecture) { return ['js_lection_0','material_0','task_0','test_0']; }
        
        
        $result = ['js_lection_'.$lecture->id];
        //найти все типы прикреплённых материалов к лекции и записать в массив требуемых
        foreach ($lecture->materials as $material){
            if ($material->material){ array_push($result, 'material_'.$material->material->type.'_'.$material->material->id); }
            }
        //foreach ($lecture->text_tasks as $text_task){ array_push($result, 'text_task_'.$text_task->id); }
        //foreach ($lecture->test_tasks as $test){ array_push($result, 'test_'.$test->id); }
        return $result;
            

    }





    /**
     * Метод отвечающий за прохождения лекции и проверки на прохождения всех заданий лекции
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request,$id)
    {

        if (!$request->isXmlHttpRequest()) { return back()->with('warning', 'Ошибка!Лекция не верный ти запроса!'); }
                
        $prelection = mainStudiesController::valid_user_lection($request->user(),$id);
        
        if (is_string($prelection)) { return response()->json(['errors'=>$prelection]); }
            
        //получит куку с фронта
        $local_cook = $request->get('local_cook',[]);
        if (    empty($local_cook) || 
                !isset($local_cook['course_id']) ||
                $local_cook['course_id'] != $prelection->curse_id || 
                !isset($local_cook['lection_current_id']) ||
                $local_cook['lection_current_id'] != $id   
            ){ return response()->json(['reinit'=>'cookies']); }//что-то пошло не так, переиницилизируем куку
        // проверка что-то не пройдено/просмотрено
        if (!empty($local_cook['lection_study_need'])) { return response()->json(['info'=>$local_cook['lection_study_need']]); }
        
        //проверка ответов на текстовые задания
        foreach ($prelection->text_tasks as $tt) {
            if (!$tt->answerUserText) { return response()->json(['info'=>'Не выполнено текстовое задание "'.$tt->Name.'"']); }
        }

        //проверка ответов на тесты
        foreach ($prelection->test_tasks as $test_task) {
            if (!$test_task->userTestAnswer || $test_task->userTestAnswer->status != 1) { return response()->json(['info'=>'Не выполнен тест "'.$tt->Name.'"']); }
        }

        $progress = Auth::user()->progressCurse($prelection->curse_id);

        //начисление баллов за прохождение лекции пользователем
        if(abs($prelection->price)>0) {
            $new_ball           = new Balls();
            $new_ball->type     = Balls::$FOR_LECTION;
            $new_ball->user_id  = Auth::user()->id;
            $new_ball->value    = abs($prelection->price);
            $new_ball->description    = 'Прохождение лекции "'.$prelection->name.'"';
            $prelection->balls()->save($new_ball);
        }

        $progress->pushFullStep($prelection->id);
        $next_lection = $progress->curse->nextLectionID($prelection->id);
        
        if ($next_lection) { $progress->pushNextStep($next_lection->id); }

        $progress->save();
        
        
        //--------Метод отправки сообщения на почту -----------------------        
        $this->prelectionMail(Auth::user(), $progress, $next_lection);
        
        if (!$next_lection && $progress->currentStepsCount()) {
            return response()->json(['success'=>'Вы успешно завершили курс!'.$prelection->course->langName]);
        }
        return response()->json(['success'=>'Вы успешно открыли лекцию '.$prelection->name,'reinit'=>'page']);
    }

    /**
     * Метод отправки сообщения на почту об открытии лекции
     * @param User $user
     * @param $progress
     * @param $next_lection
     */
    public function prelectionMail(User $user, $progress, $next_lection )
    {
        $sub = subscribersController::SearchOrNewUserFromTYPE($user, Subscribers::$FOR_SYSTEM_EMAIL);
        if (!$sub->status) { return; }
        
        $data['name'] = $user->name;
        $data['email'] = $user->email;
        
        if (!$next_lection && $progress->currentStepsCount()) {
            $data['desc']   = $progress->curse->langShortD;
            $data['id']     = $progress->curse->id;
            Mail::send('mails.studies.finish_lecturer_course', $data, function ($message) use ($data) {
                $message->to($data['email']);
                $message->subject('Успешное завершение курса!');
            });
            
        } else {
            $data['lection_name']   = $next_lection->name;
            $data['desc']           = $next_lection->langShortD;
            $data['id']             = $progress->curse->id;
            Mail::send('mails.studies.next_lecturer_lection', $data, function ($message) use ($data) {
                $message->to($data['email']);
                $message->subject('Доступна новая лекция!');
            });
        }

    }



}
