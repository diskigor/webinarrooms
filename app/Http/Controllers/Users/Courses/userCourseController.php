<?php

namespace App\Http\Controllers\Users\Courses;

use App\Http\Controllers\Admin\Users\Settings\balanceController;

use App\Models\Billing\Orders;
//use App\Models\Billing\RefOrders;
use App\Models\Callback\Callback;
use App\Models\Curses\Curse;
use App\Models\Curses\CurseBlock;

use App\Models\Roles\Role;

use App\Models\Billing\Billing;

//use App\Models\Settings\ReferencSettings;
use App\Models\Shop\buyUsersCourses;
use App\Models\Shop\Progress;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use UxWeb\SweetAlert\SweetAlert;

class userCourseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth'); 
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->verifyAllowed();
        
//        $admins = User::where('role_id',1)->get(['id'])->pluck('id');
//
//        
//        
//        $courses = Curse::active()->where(function($query) use ($admins) {
//                        $query->whereIn('user_id',$admins)->orWhereNull('user_id');    
//                    })->whereNull('sdo_courses_block_id')->orderBy('ordered')->get();       
                    
        $courses = Curse::active()->gilbo()->whereNull('sdo_courses_block_id')->orderBy('ordered')->get();            
                   
        $bay_curses = $ex_id_curses = Auth::user()->coursesByu->pluck('curses_id')->toArray();
        
        foreach ($courses as $cuors) {
            if ($cuors->parent_buy_id && !in_array($cuors->parent_buy_id, $bay_curses)) {
                $cuors->xclosed = true;
                $cuors->xname   = Curse::find($cuors->parent_buy_id)->trans_curse->first()->name_trans;
            } else {
                $cuors->xclosed = false;
            }
            
        }
        
        $grouped = CurseBlock::with(['courses'=> function($query){
            return $query->active()->gilbo()->orderBy('sdo_courses_block_ordred');
        }])->whereHas('courses', function($query){
            return $query->active()->gilbo();
        })->active()->orderBy('order_block')->get();

        foreach ($grouped as $key => $block) {
                $block->global_price_summ   = 0;
                $block->global_buy_summ     = 0;
                $block->price_buy           = 0;
                $block->real_buy_courses    = collect();

                foreach ($block->courses as $bcuors) {

                    if ($bcuors->parent_buy_id && !in_array($bcuors->parent_buy_id, $bay_curses)) {
                        $bcuors->xclosed = true;
                        $bcuors->xname   = Curse::find($bcuors->parent_buy_id)->trans_curse->first()->name_trans;
                    } else {
                        $bcuors->xclosed = false;
                    }

                    $bcuors->price_full     = $bcuors->groupPrice(Auth::user()->group_id)->price;
                    $bcuors->price_dicount  = ( $bcuors->discount / 100) * $bcuors->price_full;
                    $bcuors->price_buy      = $bcuors->price_full - $bcuors->price_dicount;

                    $block->global_price_summ += $bcuors->price_buy;

                    if (!in_array($bcuors->id,$bay_curses)) {
                        $block->real_buy_courses->push($bcuors);
                        $block->price_buy += $bcuors->price_buy;
                    } else {
                        $block->global_buy_summ += $bcuors->price_buy;
                    }
                }
        }
        
        $route_type = 'main_studies';
        
        $bay_all_curses = balanceController::getButtonBayAllCourses(Auth::user());

        return view('users.courses.showUserCourses', compact('courses','grouped','bay_curses','route_type','bay_all_curses'));
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexpartners()
    {
        $this->verifyAllowed();

        $courses = Curse::active()->lecturer()->orderBy('ordered')->get();
                   
        $bay_curses = Auth::user()->coursesByu->pluck('curses_id')->toArray();
        
        foreach ($courses as $cuors) {
            if ($cuors->parent_buy_id && !in_array($cuors->parent_buy_id, $bay_curses)) {
                $cuors->xclosed = true;
                $cuors->xname   = Curse::find($cuors->parent_buy_id)->trans_curse->first()->name_trans;
            } else {
                $cuors->xclosed = false;
            }
            
        }
        $route_type = 'main_studies';
        
        $bay_all_curses = balanceController::getButtonBayAllCourses(Auth::user());
        
        return view('users.courses.showPartnersCourses', compact('courses','route_type','bay_all_curses'));
    }
    
    
    /**
     * Метод покупки курса пользователями
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function buyCourse(Request $request, $id)
    {        
        $course = Curse::find($id);
        if (!$course) { return back()->with('warning', 'Неизвестный курс. Сообщите Администратору'); }


        if (!$request->user()->checkEntrance()){ return back()->with('warning','Для покупки курсов внесите вступительный взнос'); }
        if ($request->user()->checkBuyCourse($id)) {  return back()->with('warning', 'Данный курс Выми уже приобретен'); }

        $buy_user = $request->user();
        $my_buy_curse_ids = buyUsersCourses::where('user_id',$buy_user->id)->get()->pluck('curses_id')->toArray();
        
        if ($course->parent_buy_id && !in_array($course->parent_buy_id, $my_buy_curse_ids)) {
            $need_curse = Curse::find($course->parent_buy_id);
            return back()->with('warning', 'Сначала пройдите курс "'.$need_curse->trans_curse->first()->name_trans.'"');
        }


        if (count(collect($course->lectures)) === 0) { return back()->with('warning', 'Извините в данном курсе нет учебных дней. Сообщите об этом администратору сайта'); }

        if ($request->price == 0) { /*Решить если сумма равна 0;*/ }
        
        $discount = $course->discount;
        $res_with_discount = ($discount / 100) * $course->groupPrice($buy_user->group_id)->price;
        $byu_sum = $course->groupPrice($buy_user->group_id)->price - $res_with_discount;
        $user_balance = $request->user()->balance;
        if ($user_balance < $byu_sum) {
            $need_summ = $byu_sum - $user_balance;
            return redirect()->route('mybalance.index', ['need_summ'=>$need_summ,'need_text'=>'Для покупки нужно ещё внести '.$need_summ])->with('warning', 'Для покупки курса пополните баланс');
        }
        $this->orderOneCourse($course, $buy_user, $byu_sum);
        SweetAlert::success('Курс ' . $course->trans_curse[0]->name_trans . ' включен в учебный план');
        if($course->author && $course->author->isLecturer()) {
            return redirect()->route('lecture_studies.index');
        }
        return redirect()->route('main_studies.index');
    }

    /**
     * Метод покупки курса пользователями
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function buyBlockCourses(Request $request, $id)
    {
        if (!$request->user()->checkEntrance()){ return back()->with('warning','Для покупки курсов внесите вступительный взнос'); }

        $block = CurseBlock::with(['courses'=> function($query){
            return $query->active()->orderBy('sdo_courses_block_ordred');
        }])->whereHas('courses', function($query){
            return $query->active();
        })->active()->where('id',$id)->first();

        if (!$block) { return back()->with('warning', 'Неизвестный Блок курсов. Сообщите Администратору'); }

        $bay_curses = $request->user()->coursesByu->pluck('curses_id')->toArray();
        $real_buy_courses = collect();
        foreach ($block->courses as $bcuors) {
            if (!in_array($bcuors->id,$bay_curses)) {
                $bcuors->price_full     = $bcuors->groupPrice(Auth::user()->group_id)->price;
                $bcuors->price_dicount  = ( $bcuors->discount / 100) * $bcuors->price_full;
                $bcuors->price_buy      = $bcuors->price_full - $bcuors->price_dicount;
                $bcuors->price_block_buy = $bcuors->price_buy - ($bcuors->price_buy/100*$block->discount);
                $real_buy_courses->push($bcuors);
            }
        }

        if ($real_buy_courses->count() == 0) { return back()->with('warning', 'В этом блоке Вы оплатили все доступные курсы'); }

        $balance_need_summa = $real_buy_courses->sum('price_block_buy');

        $user_balance = $request->user()->balance;
        if ($user_balance < $balance_need_summa) {
            $need_summ = $balance_need_summa - $user_balance;
            return redirect()->route('mybalance.index', ['need_summ'=>$need_summ,'need_text'=>'Для покупки блока нужно ещё внести '.$need_summ])->with('warning', 'Для покупки блока курсов пополните баланс');
        }

        /* Покупка курсов из блока, согласно списка */
        foreach ($real_buy_courses as $cours){
            $this->orderOneCourse($cours, $request->user(), $cours->price_block_buy,'Блок "'.$block->name . '"');
        }

        SweetAlert::success('Блок "' . $block->name . '", со всеми курсами включен в учебный план');
        
        if($block->user && $block->user->isLecturer()) {
            return redirect()->route('lecture_studies.index');
        }
        return redirect()->route('main_studies.index');
    }


    
    /**
     * Метод покупки ВСЕХ курсо пользователями
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function buyAllCourses(Request $request)
    {
        if ($request->user()->countBuyCourse()){ return back()->with('warning','Предложение не актульно'); }

        $balance_need_summa = buyUsersCourses::getSummAllActiveCurses(config('vlavlat.discont_for_bay_all_curs'));

        $user_balance = $request->user()->balance;

        if ($user_balance < $balance_need_summa) {
            $need_summ = $balance_need_summa - $user_balance;
            return redirect()->route('mybalance.index', ['need_summ'=>$need_summ,'need_text'=>'Для покупки  всех курсов нужно ещё внести '.$need_summ])->with('warning', 'Для покупки всех курсов пополните баланс');
        }

        $role_cadet = Role::where('name','Cadet')->first();
        
        $all_active = Curse::active()->get();
        
        if ($all_active->count() == 0) { return back()->with('warning', 'Нет активных курсов'); }

        foreach ($all_active as $curs) {
                $bcuors = new \stdClass();
                $bcuors->price_full     = $curs->groupPrice($role_cadet->id)->price;
                $bcuors->price_dicount  = ( config('vlavlat.discont_for_bay_all_curs') / 100) * $bcuors->price_full;
                $bcuors->price_buy      = $bcuors->price_full - $bcuors->price_dicount;
                                
                $this->orderOneCourse($curs, $request->user(), $bcuors->price_buy);
        }

        SweetAlert::success('Все курсы включены в учебный план');
        return redirect()->route('main_studies.index');
    }
    

    
    /**
     * ОСНОВНАЯ Покупка одного курса
     * @param Curse $course
     * @param User $user
     * @param type $price
     * @return Orders
     */
    protected function orderOneCourse(Curse $course, User $user, $price = 0, $on_desc = null) {

        //запись в таблицу покупок
        $order = new Orders();
        $order->user_id = $user->id;
        $order->course_id = $course->id;
        $order->lecture_sum = $this->addLecturerBonus($course, $user, $price);
        $order->lecture_id = $course->user_id;
        $order->sum = $price;
        $order->save();

        //запись в таблицу купленных пользователем курсов
        $this->buyUserCours($course, $user);
        
        $billing    = new Billing();
        $billing->user_id            = $user->id;
        $billing->type_replenishment = Billing::TYPE_BUY_COURSE;
        $billing->transfer_sum       = -abs($price);
        $billing->currency           = Billing::TYPE_CURRENCY;
        $billing->admin_id           = NULL;
        if($on_desc) {
            $billing->desc = $on_desc.', покупка курса "'.$course->langName.'"';
        } else {
            $billing->desc = 'Покупка курса "'.$course->langName.'"';
        }
        
        $billing->save();

        return $order;
    }
    

    /**
     * Инициализация прохождения курса пользователем
     * @param Curse $course
     * @param User $user
     * @return Progress
     */
    protected function initProgress(Curse $course, User $user) {
        $progress = new Progress();
        $progress->user_id = $user->id;
        $progress->curses_id = $course->id;
        $progress->step = 0;
        $progress->save();
        return $progress;
    }
    
    /**
     * Запись в таблицу купленных пользователем курсов и доступных для изучения
     * @param Curse $course
     * @param User $user
     * @return buyUsersCourses
     */
    protected function buyUserCours(Curse $course, User $user) {
        $add_course_user = new buyUsersCourses();
        $add_course_user->user_id = $user->id;
        $add_course_user->curses_id = $course->id;
        $add_course_user->save();
        
        //инициализируем прохождение, купленного курса
        $this->initProgress($course, $user);
        
        return $add_course_user;
    }

    /**
     * Начисление лектору (автору курса), при продаже его курса
     * @param Curse $course
     * @param float $price
     * @return type
     */
    protected function addLecturerBonus(Curse $course, User $user, $price = 0) {
        //нет автора, будет Гильбо
        if(!$course->author){
            $course->update(['user_id'=>config('vlavlat.personal_IdGilbo')]);
            return 0;
        }
        
        //Гильбо не начисляем
        if($course->author->isGilbo()) { return 0; }
        
        //вычисляем сумму для автора курса
        $res = ($course->author->lecture_percent/100)*$price;
        
        //нет начислений
        if($res<=0) {return 0;}
        
        //Начисление автору при продаже его курса
        $billing    = new Billing();
        $billing->user_id            = $course->author->id;
        $billing->type_replenishment = Billing::TYPE_LECTURE;
        $billing->transfer_sum       = abs($res);
        $billing->currency           = Billing::TYPE_CURRENCY;
        $billing->admin_id           = NULL;
        $billing->desc = 'Покупка курса "'.$course->langName.'" пользователем "'.$user->name.'".';
        $billing->save();
        return $res;
    }
    
    

    

    
    protected function verifyAllowed(){
        $user = Auth::user();
        
        if( $user->checkEntrance()) { return \TRUE;}
        
        if($user->balance < $user->MinPay) { return \FALSE; }
        
        $user->update(['first_contribution'=>1]);

        return \TRUE;
    }
}
