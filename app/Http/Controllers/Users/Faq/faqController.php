<?php
namespace App\Http\Controllers\Users\Faq;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * Description of audienceController
 *
 * @author vlavlat
 */
class faqController extends Controller 
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    
    
    /**
     * Метод вывода представления
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('users.faq.index');
    }
    
}
