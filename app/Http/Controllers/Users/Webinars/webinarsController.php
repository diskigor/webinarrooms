<?php
namespace App\Http\Controllers\Users\Webinars;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * Description of audienceController
 *
 * @author vlavlat
 */
class webinarsController extends Controller 
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    
    
    /**
     * Метод вывода представления
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('users.webinars.index');
    }
    
}
