<?php
namespace App\Http\Controllers\Users\Advice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * Description of audienceController
 *
 * @author vlavlat
 */
class adviceController extends Controller 
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    
    
    /**
     * Метод вывода представления
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('users.advice.index');
    }
    
}
