<?php
namespace App\Http\Controllers\Rules;

use App\User;
use App\Models\Settings\UserGroup;

use App\Models\Billing\RefKeys;
use App\Models\Billing\Billing;
use App\Models\Curses\Curse;

/**
 * Description of RulesAutoGroup
 *
 * @author vlavlat
 */
class RulesAutoGroup {
    /**
     * @var self
     */
    private static $instance;


    /**
     * @var array
     */
    protected $rules;
    
    protected function __construct() {
        $this->rules = [
                        '1' => ['name' => 'Обычная регистрация'                                     ,'condition'    => 'easy_reg'],
                        '2' => ['name' => 'Регистрация по админской ссылке'                         ,'condition'    => 'admin_reg'],
                        '3' => ['name' => 'Внёс в систему '.  config('vlavlat.summa_auto_chlen_club'),'condition'   => 'chlen_club'],
                        '4' => ['name' => 'Покупка всех курсов'                                     ,'condition'    => 'bay_all_curs'],
                    ];
    }
    
    
    public function getIdNames() {
        $IdNames =[];
        foreach ($this->rules as $key=>$r){
            $IdNames[$key] = $key.'. '.$r['name'];
        }
        return $IdNames;
    }
    
    public function getOmlyNames() {
        $omlyNames =[];
        foreach ($this->rules as $key=>$r){
            $omlyNames[] = $key.'. '.$r['name'];
        }
        return $omlyNames;
    }
    
    /**
     * @param int $id
     * @return string
     */
    public function getName($id) {
        return !empty($this->rules[$id])?$id.'. '.$this->rules[$id]['name']:'';
    }
    
    
    public function execute(User $user) {

        $current_group_id = $user->group?$user->group->id:0;
        $new_group_id = $current_group_id;
        
        foreach ($this->rules as $key=>$rule){
            
            if (!$this->{$rule['condition']}($user)) { continue; }
            
            $group_id = $this->searchIdGroup($key);
            if (!$group_id) { continue; }
            
            $new_group_id = $group_id;
        }
        
        if ($new_group_id <= $current_group_id) { return;}
        $user->update(['group_id' => $new_group_id ]);
    }
    
    
    protected function searchIdGroup($id_rule) {
        try {
            $exc = UserGroup::where('rule_id',$id_rule)->first()->id;
        } catch (\Exception $exc) {
            $exc = \FALSE;
        }
        return $exc;
    }
    
    
    
    //Condition
    
    protected function easy_reg(User $user) {
        $parrent = RefKeys::where('user_id', $user->id)->first();

        if ($parrent) {
            return $parrent->uparent?(!$parrent->uparent->isAdmin()):\TRUE;
        }
        return \TRUE;
    }
    
    protected function admin_reg(User $user) {
        $parrent = RefKeys::where('user_id', $user->id)->first();

        if ($parrent) {
            return $parrent->uparent?($parrent->uparent->isAdmin()):  \FALSE;
        }
        return \FALSE;
    }
    
    protected function chlen_club(User $user) {
        $summa = Billing::getUserOf($user->id)->where('transfer_sum','>',0)->sum('transfer_sum');
        
        if (\config('vlavlat.summa_auto_chlen_club') <= $summa) { return \TRUE;}
        
        return FALSE;
    }
    
    protected function bay_all_curs(User $user) {
        $ids_bay = $user->coursesByu?$user->coursesByu->pluck('curses_id'):  \FALSE;
        
        if(empty($ids_bay)) { return \FALSE; }
        
        $ids_active = Curse::active()->pluck('id');
        
        if(empty($ids_active)) { return \FALSE; }
        
        $diff = $ids_active->diff($ids_bay);
        
        return (empty($diff) || $diff->isEmpty());
    }
    
    /**
     * @return self
     */
    public static function getInstance(){
        if (!self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }
}
