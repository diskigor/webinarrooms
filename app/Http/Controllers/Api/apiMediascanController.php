<?php

namespace App\Http\Controllers\Api;

use App\Models\Api\apiMediascan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;

class apiMediascanController extends Controller
{
    public function getAll(){
        $client = new Client();
        $uri = 'http://mediascan.ru/api/mobileapp/getall';                                                              //Адрес запроса по api
        $res = $client->get($uri)->getBody();                                                                           //Получаем тело запроса
        $data= json_decode($res);
        foreach ($data->items as $item){
            $this->getID($item->id);
        }
    }

    public function getID($id){
        $client = new Client();
        $uri = 'http://mediascan.ru/api/mobileapp/get?id='.$id;                                                         //Адрес запроса по api
        $res = $client->get($uri)->getBody();                                                                           //Получаем тело запроса
        $data= json_decode($res);
        $result = explode('-',$data->date);
        $media_bd = new apiMediascan();
        $media_bd->mediascan_id = $data->id;
        $media_bd->mediascan_date = $data->date;
        $media_bd->year =$result[0];
        $media_bd->month =$result[1];
        $media_bd->day =$result[2];
        $media_bd->mediascan_body = $data->body;
        $media_bd->mediascan_keywords = $data->keywords;
        $media_bd->save();
    }

    public function getNewID(){
        $last_id = apiMediascan::orderBy('id', 'DESC')->first();
        $client = new Client();
        $uri = 'http://mediascan.ru/api/mobileapp/getnew?id='.$last_id->mediascan_id;
        $res = $client->get($uri)->getBody();                                                                           //Получаем тело запроса
        $data= json_decode($res);
        if($data->itemsCount != 0){
            foreach ($data->items as $item){
                $this->getID($item->id);
            }
        }

    }
}
