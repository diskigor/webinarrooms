<?php

namespace App\Http\Controllers\Interkassa;

use App\Models\Billing\Billing;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class addBalanceController extends Controller
{
    public function apiInterkassa(Request $request)
    {
        $user_id = $request->ik_pm_no;
        $sum = $request->ik_am;
        $currency = $request->ik_cur;
        $status = ($request->ik_inv_st == 'success') ? TRUE : FALSE;
        $ik_inv_id = $request->ik_inv_id;
        $ik_desc = $request->ik_desc;

        $check_user = User::find($user_id);
        if (!$check_user) {
            return false;
        }
        if ($status === TRUE && !Billing::where('number_inv',$ik_inv_id)->first()) {
            $new_order = new Billing();
            $new_order->user_id         = $check_user->id;
            $new_order->transfer_sum    = $sum;
            $new_order->currency        = $currency;
            $new_order->number_inv      = $ik_inv_id;
            $new_order->desc            = $ik_desc;
            $new_order->save();
        }
    }

    public function intSuccess(Request $request)
    {
        $user = User::find($request->ik_pm_no);
        $status = ($request->ik_inv_st == "success") ? TRUE : FALSE;
        if ($status) {
            return redirect(route('/personal_area'))->with('success', $user->name . ' Ваш платеж успешно проведен');
        }
    }

    public function intFail(Request $request)
    {
        $user = User::find($request->ik_pm_no);
        $status = ($request->ik_inv_st == 'canceled') ? TRUE : FALSE;
        if ($status) {
            return redirect(route('/personal_area'))->with('warning', $user->name . ' Ваш платеж отменен');
        }
    }

    public function intWaiting(Request $request)
    {
        $user = User::find($request->ik_pm_no);
        $status = ($request->ik_inv_st == "waitAccept") ? TRUE : FALSE;
        if ($status) {
            return redirect(route('/personal_area'))->with('warning', $user->name . ' Ваш платеж не проведен статус - на ожидании');
        }
    }
}
