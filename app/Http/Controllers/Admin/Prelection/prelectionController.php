<?php

namespace App\Http\Controllers\Admin\Prelection;

use App\Models\Curses\Curse;
use App\Models\Curses\TransCurse;
use App\Models\Materials\Materials;
use App\Models\Materials\MaterialsLectures;
use App\Models\Prelections\Prelection;
use App\Models\Prelections\PrelectionTrans;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use File;

class prelectionController extends Controller
{
    protected $s_page = 9;


    public function __construct()
    {
        $this->middleware('lecturerOrAdmin');
    }
    
    /**
     * Метод вывода лекций
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $courses    = Curse::with('lang')->active();
        $lections   = Prelection::query();
        $curse_id   = $request->get('course_id');
        
        if (Auth::user()->isLecturer()) {
            $courses->where('user_id', Auth::user()->id);
            $lections->where('user_id', Auth::user()->id);
        } 
        
        if($curse_id>0) {
            $prelections = $lections->where('curse_id', $request->get('course_id'))
                    ->paginate($this->s_page)
                    ->appends('course_id', $request->get('course_id'));
        } else {
            $prelections = $lections->paginate($this->s_page);
        }
        
//        
//        $course_chearsh = array();
//        foreach ($courses as $cours) {
//            foreach ($cours->trans_curse as $trans) {
//                $course_chearsh[$cours->id] = $trans->name_trans;
//            }
//        }
        
        $course_chearsh = $courses->get()->pluck('lang.name_trans','id')->toArray();
        
        return view('Admin.prelection.listPrelection', compact('prelections', 'course_chearsh', 'curse_id'));
        
//        dd($prelections,$course_chearsh);
//        
//        if (isset($request->course_id)) {
//            $course_id = $request->course_id;
//            if (Auth::user()->isLecturer()) {
//                $prelections = Prelection::where('curse_id', $course_id)
//                    ->where('user_id', Auth::user()->id)
//                    ->paginate(9)
//                    ->appends('course_id', $course_id);
//            } else {
//                $prelections = Prelection::where('curse_id', $course_id)
//                    ->paginate(9)
//                    ->appends('course_id', $course_id);
//            }
//            return view('Admin.prelection.listPrelection', compact('prelections', 'course_chearsh'));
//        } else {
//            if (Auth::user()->isLecturer()) {
//                $prelections = Prelection::where('user_id', Auth::user()->id)
//                    ->paginate(9);
//            } else {
//                $prelections = Prelection::paginate(9);
//            }
//
//            return view('Admin.prelection.listPrelection', compact('prelections', 'course_chearsh'));
//        }

    }

    /**
     *Метод вывода представления для создания лекции
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
            if (Auth::user()->isLecturer()) {
                $all_cur = Curse::where('user_id', Auth::user()->id)->get(['id'])->pluck('id');
            } else {
                $all_cur = Curse::get(['id'])->pluck('id');
            }
            $local = App::getLocale();                                                                                  //Получаем все язык приложения
            $curses = TransCurse::where('lang_local', $local)->whereIn('sdo_curses_id', $all_cur)->get();                                                   //Получаем все курсы СДО
            foreach ($curses as $curs) {                                                                                //Перебираме все курсы
                $arr_curses[$curs->sdo_curses_id] = $curs->name_trans;                                                  //Записываем название и id курса
            }
            
            $languages = LaravelLocalization::getSupportedLocales();                                                    //Записываем доступные языки приложения
            foreach ($languages as $key => $value) {
                $arr_local[$key] = $value['name'];
            }
            $arr_local = array_reverse($arr_local);                                                                     //Переварачиваем массив
            return view('Admin.prelection.createPrelection',                                                      //Возвращаем представление с данными
                compact('curses', 'arr_local', 'arr_curses'));

    }

    /**
     * Метод создания лекции
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [                                                                                 //Проверка на валидность данных
            'curse_id' => 'required',
            'number' => 'required',
            'price' => 'required'
        ]);
        $languages = LaravelLocalization::getSupportedLocales();                                                    //Получаем все языки приложения
        foreach ($languages as $key => $value) {                                                                    //Перебираем массив с языками
            $this->validate($request, [                                                                             //Созадем валидатор под каждый язык
                'name_trans_' . $key => 'required',
                'content_trans_' . $key => 'required',
                'short_desc_' . $key => 'required'
            ], [
                'name_trans_' . $key . 'required' => 'Название на ' . $key . ' обязательно!',
                'content_trans_' . $key . 'required' => 'Содержимое на ' . $key . ' обязательно!',
                'short_desc_' . $key . 'required' => 'Краткое описание на ' . $key . ' обязательно!',
            ]);
        }
        $check_number = Prelection::where('curse_id', $request->curse_id)
            ->where('number', $request->number)
            ->first();
        if ($check_number) {
            return back()->withInput()->with('message', 'Номер лекции ' . $request->number . ' уже существует');
        }
        $prelection = new Prelection();                                                                             //Создание новой лекции
        $prelection->curse_id = $request->curse_id;
        $prelection->user_id = Auth::user()->id;
        $prelection->number = $request->number;
        $prelection->price = $request->price;
        $prelection->save();                                                                                        //Сохранение данных
        $created_prelection_id = $prelection->id;
        foreach ($languages as $key => $value) {
            $trans_prelection = new PrelectionTrans();
            $trans_prelection->lang_local = $key;
            $trans_prelection->sdo_lectures_id = $created_prelection_id;
            $trans_prelection->name_trans = $request->{'name_trans_' . $key};
            $trans_prelection->short_desc = $request->{'short_desc_' . $key};
            $trans_prelection->content_trans = $request->{'content_trans_' . $key};
            //-------------------Slug-------------------------------------------------------------------------------
            $slug = explode(' ', $request->{'name_trans_' . $key});
            foreach ($slug as $w => $word) {
                if (iconv_strlen($word) < 3) {
                    unset($slug[$w]);
                }
            }
            $trans_prelection->slug = implode('-', $slug);
            //------------------------------------------------------------------------------------------------------
            $trans_prelection->save();
        }
        return redirect()->route('prelection.index')->with('success', 'Лекция успешно добавлена');
    }

    /**
     * Метод вывода подробнее лекции
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $local = App::getLocale();
        $prelection = PrelectionTrans::where('sdo_lectures_id', $id)->where('lang_local', $local)->first();
        if (Auth::user()->isLecturer()) {
            $add_materials = collect(Materials::where('user_id', Auth::user()->id)->pluck('name', 'id'));
        } else {
            $add_materials = collect(Materials::pluck('name', 'id'));
        }
        
        if ($prelection) {
            return view('Admin.prelection.showPrelection', compact('prelection', 'add_materials'));
        } else {
            return back()->with('warning', 'Лекция не найдена.Ошибка при поиске');
        }
    }

    /**
     * Метод вывода представления редактирования лекции
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $local = App::getLocale();                                                                                  //Получаем язак приложения
        $prelection = Prelection::find($id);                                                                        //Получаем лекцию по id
        if (Auth::user()->isLecturer()) {
            $all_cur = Curse::where('user_id', Auth::user()->id)->get(['id'])->pluck('id');
        } else {
            $all_cur = Curse::get(['id'])->pluck('id');
        }

        $curses = TransCurse::where('lang_local', $local)->whereIn('sdo_curses_id', $all_cur)->get();                                                   //Получаем все курсы СДО
        foreach ($curses as $curs) {                                                                                //Перебираме все курсы
            $arr_curses[$curs->sdo_curses_id] = $curs->name_trans;                                                  //Записываем название и id курса
        }
        if ($prelection) {
            return view('Admin.prelection.editPrelection', compact('prelection', 'arr_curses'));
        } else {
            return back()->with('warning', 'Лекция не найдена');
        }
    }

    /**
     * Метод обновления лекции
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'curse_id' => 'required',
            'number' => 'required',
            'price' => 'required'
        ]);
        $languages = LaravelLocalization::getSupportedLocales();                                                    //Получаем все языки приложения
        foreach ($languages as $key => $value) {                                                                    //Перебираем массив с языками
            $this->validate($request, [                                                                             //Созадем валидатор под каждый язык
                'name_trans_' . $key => 'required',
                'content_trans_' . $key => 'required',
                'short_desc_' . $key => 'required'
            ]);
        }
        $check_number = Prelection::where('curse_id', $request->curse_id)
            ->where('id', '!=', $id)
            ->where('number', $request->number)
            ->first();
        if ($check_number) {
            return back()->withInput()->with('message', 'Номер лекции ' . $request->number . ' уже существует');
        }
        $prelection = Prelection::find($id);
        if ($prelection) {
            $prelection->curse_id = $request->curse_id;
            $prelection->block_number = $request->block_number;
            $prelection->number = $request->number;
            $prelection->price = $request->price;
            $prelection->save();
            $prelection_update_id = $prelection->id;
            foreach ($languages as $key => $value) {
                $trans_prelection = PrelectionTrans::where('sdo_lectures_id', $prelection_update_id)
                    ->where('lang_local', $key)->first();
                $trans_prelection->name_trans = $request->{'name_trans_' . $key};
                $trans_prelection->short_desc = $request->{'short_desc_' . $key};
                $trans_prelection->content_trans = $request->{'content_trans_' . $key};
                //-------------------Slug---------------------------------------------------------------------------
                $slug = explode(' ', $request->{'name_trans_' . $key});
                foreach ($slug as $w => $word) {
                    if (iconv_strlen($word) < 3) {
                        unset($slug[$w]);
                    }
                }
                $trans_prelection->slug = implode('-', $slug);
                $trans_prelection->save();
            }
            return redirect()->route('prelection.index')->with('success', 'Лекция успешно обновлена!');
        } else {
            return back()->withInput()->with('warning', 'Лекция не найдена');
        }
    }

    public function prelectionMaterial(Request $request, $id)
    {
        if (Auth::user()->isAdmin() or Auth::user()->isLecturer()) {
            $this->validate($request, [
                'material' => 'required'
            ]);
            $prelection_materials = MaterialsLectures::where('additional_material_id', $request->material)
                ->where('lecture_id', $id)->first();
            if ($prelection_materials) {
                return back()->with('warning', 'Данный материал уже есть в данной лекции');
            } else {
                $add_prelection_material = new MaterialsLectures();
                $add_prelection_material->lecture_id = $id;
                $add_prelection_material->additional_material_id = $request->material;
                $add_prelection_material->sort = $request->sort;
                $add_prelection_material->save();
                return back()->with('success', 'Материал успешно добавлен');
            }
        } else {
            // Реализовать отправку сообщения администратору о взломе.
            return back()->with('warning', trans('message.hack_message'));
        }
    }


    public function deletePrelectionMaterials($id)
    {
        if (Auth::user()->isAdmin() or Auth::user()->isLecturer()) {
            $delete_prel_material = MaterialsLectures::find($id);
            if ($delete_prel_material) {
                $delete_prel_material->delete();
                return back()->with('success', 'Материал успешно откреплен от лекции');
            } else {
                return back()->with('warning', 'Ошибка при поиске.');
            }
        } else {
            // Реализовать отправку сообщения администратору о взломе.
            return back()->with('warning', trans('message.hack_message'));
        }
    }

    /**
     * Метод удаления лекции
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $prelection = Prelection::findOrFail($id);

        //удаляем эту лекцию со всех прогрессов прохождения пользователей
        $progress = ($prelection->course)?$prelection->course->progress:collect();
        foreach ($progress as $p_i){
            $p_i->deleteProgressID($id);
        }


        $prelection->delete();
        return redirect()->route('prelection.index')->with('success', 'Лекция успешно удалена');
    }

    public function multipleDelete(Request $request)
    {
        foreach ($request->prelections_id as $value) {
            $lecture = Prelection::find($value);
            $lecture->delete();
        }
        return back()->with('success', 'Выбранные лекции успешно удалены!');
    }
}
