<?php

namespace App\Http\Controllers\Admin\Referals;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Models\Billing\RefKeys;
use App\Models\Billing\RefHistory;

class referalController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth'); 
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        
        if($user->isAdmin()){
            $refferals = RefKeys::all();
            return view('Admin.referals.referals_list',compact('refferals'));
        } else {
            $all_referal = $user->getMyReferalsLevel(config('vlavlat.ref_max_level'));

            $refferals = collect();

            foreach ($all_referal as $key => $level_users) {
                foreach ($level_users as $us) {
                    $us->x_referal_level = $key;
                    $us->x_referal_summa = RefHistory::where('parent_id',$user->id)->where('user_id',$us->id)->sum('operation_value');
                    $us->x_referal_bonus = RefHistory::where('parent_id',$user->id)->where('user_id',$us->id)->sum('value');
                    $refferals->push($us);
                }
            }

            $history = RefHistory::where('parent_id',$user->id)->get();
            
            $all_bonus = RefHistory::where('user_id',$user->id)->sum('value');
            
            return view('Admin.referals.referal_statistics',compact('user','refferals','history','all_bonus'));
        }

        
    }

//   public function getAnswerSuccess(Request $request){
//        dd($request);
//   }
//    /**
//     * Store a newly created resource in storage.
//     *
//     * @param  \Illuminate\Http\Request  $request
//     * @return \Illuminate\Http\Response
//     */
//    public function store(Request $request)
//    {
//        //
//    }
//
//    /**
//     * Display the specified resource.
//     *
//     * @param  int  $id
//     * @return \Illuminate\Http\Response
//     */
//    public function show($id)
//    {
//        //
//    }
//
//    /**
//     * Show the form for editing the specified resource.
//     *
//     * @param  int  $id
//     * @return \Illuminate\Http\Response
//     */
//    public function edit($id)
//    {
//        //
//    }
//
//    /**
//     * Update the specified resource in storage.
//     *
//     * @param  \Illuminate\Http\Request  $request
//     * @param  int  $id
//     * @return \Illuminate\Http\Response
//     */
//    public function update(Request $request, $id)
//    {
//        //
//    }
//
//    /**
//     * Remove the specified resource from storage.
//     *
//     * @param  int  $id
//     * @return \Illuminate\Http\Response
//     */
//    public function destroy($id)
//    {
//        //
//    }
}
