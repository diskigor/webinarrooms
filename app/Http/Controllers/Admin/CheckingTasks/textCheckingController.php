<?php

namespace App\Http\Controllers\Admin\CheckingTasks;

use App\Models\Favorites\Favorites;
use App\Models\UserAnswer\TextAnswer;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class textCheckingController extends Controller
{
    public function __construct()
    {
        $this->middleware('userNoCadet');//все кроме кадетов
    }
    
    /**
     * Метод вывода текстовых ответов пользователей
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $text_answers = TextAnswer::withCount('comments')->latest()->paginate(15);
        //dd($text_answers);
        return view('Admin.tasks.text_answer.textAnswerList', compact('text_answers'));
    }

    /**
     * Метод вывода формы проверки
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $text_answer = TextAnswer::find($id);
        if ($text_answer) {
            return view('Admin.tasks.text_answer.showAnswer', compact('text_answer'));
        }
        return back()->with('warning', 'Ошибка при поиске ответа в БД');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $text_answer = TextAnswer::find($id);
        if ($text_answer) {
            $text_answer->answer = $request->answer;
            $text_answer->save();
            return back()->with('success', 'Изменения успешно изменены');
        }
        return back()->with('warning', 'Ошибка при поиске ответа');
    }

    /**
     * Метод проверки ответа пользователя и сохранения статуса
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeStatusAnswer(Request $request, $id)
    {
        $text_answer = TextAnswer::find($id);
        $user = User::find($text_answer->user_id);
        if ($text_answer) {
            if ($request->status === '2') {
                $text_answer->status = $request->status;
                $text_answer->check_user = Auth::user()->id;
                $text_answer->save();
                return redirect(route('checking_text_tasks.index'))
                    ->with('success', 'Задание успешно отправлено на доработку пользователю');
            } elseif ($request->status === '1') {
                if (isset($request->price)) {
                    $text_answer->price = $request->price;
                    $text_answer->status = $request->status;
                    $text_answer->check_user = Auth::user()->id;
                    $text_answer->save();
//                    $user->level_point = $user->level_point + $request->price;
//                    $user->save();
                    return redirect(route('checking_text_tasks.index'))
                        ->with('success', 'Вы проверили данный ответ');
                } else {
                    return back()->with('warning', 'Оцените ответ пользователя');
                }
            }
        }
        return back()->with('warning', 'Ошибка при посике ответа пользователя на тест');
    }

    public function addFavorite($favorit_id)
    {
        $user = Auth::user();
        $check = Favorites::where('user_id', $user->id)->where('favor_user_id', $favorit_id)->first();
        if ($check) {
            return back()->with('warning', 'Данный пользователь уже помечен как избранный!');
        }
        $add_favorite = new Favorites();
        $add_favorite->user_id = $user->id;
        $add_favorite->favor_user_id = $favorit_id;
        $add_favorite->save();
        return back()->with('success', 'Работа успешно добавлена в избранное');
    }

    public function deleteFavorite($favorit_id)
    {       
        $delete_favorite = Favorites::where('favor_user_id', $favorit_id)
            ->where('user_id', Auth::user()->id)
            ->first();
        if (!$delete_favorite) {
            return back()->with('warning', 'Ошибка при поиске избранного пользователя!');
        }
        $delete_favorite->delete();
        return back()->with('success', 'Пользователь успешно удален с избранных');
    }

    public function listFavorite()
    {
        $favorites = Favorites::where('user_id', Auth::user()->id)->get();
        return view('Admin.tasks.favorites.list_favorites', compact('favorites'));
    }

    public function workFavorite($fav_id)
    {
        $text_answers = TextAnswer::where('user_id', $fav_id)->withCount('comments')->paginate(15);
        return view('Admin.tasks.text_answer.textAnswerList', compact('text_answers'));
    }

}
