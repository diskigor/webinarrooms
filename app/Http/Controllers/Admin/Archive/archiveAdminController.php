<?php

namespace App\Http\Controllers\Admin\Archive;

use App\Models\Api\apiMediascan;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class archiveAdminController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin'); 
    }
    
    public function index()
    {
        $count_sends_year = DB::table('mediascan')
            ->select('year', DB::raw('count(id) as total'))
            ->groupBy('year')
            ->get()
            ->pluck('total', 'year');
        $count_sends_year = $count_sends_year->reverse();
        $current_y =date('Y');
        $count_archive_year = apiMediascan::where('year', $current_y)->first();
        $arr_month = ['01','02','03','04','05','06','07','08','09','10','11','12'];
        return view('Admin.archive.all_list', compact(
            'count_sends_year',
            'count_archive_year',
            'current_y',
            'arr_month'));
    }
    
    public function index_first()
    {
        $mediascans = DB::table('mediascan')
            ->latest()
            ->take(20)
            ->get();
        return view('Admin.archive.first_show', compact(
            'mediascans'));
    }

    public function show($year){
        $count_sends_year = DB::table('mediascan')
            ->select('year', DB::raw('count(id) as total'))
            ->groupBy('year')
            ->get()
            ->pluck('total', 'year');
        $count_sends_year = $count_sends_year->reverse();
        $count_archive_year = apiMediascan::where('year', $year)->first();
        $arr_month = ['01','02','03','04','05','06','07','08','09','10','11','12'];
        return view('Admin.archive.show_years_archive',
            compact(
            'count_archive_year',
            'year',
            'count_sends_year',
            'arr_month'));
    }

    
    public function edit($id) {
        $api_post = apiMediascan::find($id);
        return view('Admin.archive.show', compact('api_post'));
    }
    
    
    
    public function search(Request $request) {
        //работаем со строкой поиска
        if($request->has('textsearch')){
            $mediascans = apiMediascan::where('mediascan_body','like','%'.trim($request->input('textsearch')).'%')->orderBy('id','DESC')->paginate(20)->appends($request->input());
            return view('Admin.archive.show_search', ['mediascans'=>$mediascans]);
        }
        
        return redirect()->route('admin_all_archive.index');
    }
    

//    public function getYearApi($year)
//    {
//        $count_sends_year = DB::table('mediascan')
//            ->select('year', DB::raw('count(id) as total'))
//            ->groupBy('year')
//            ->get()
//            ->pluck('total', 'year');
//        $count_sends_year = $count_sends_year->reverse();
//        $count_archive_year = apiMediascan::where('year', $year)->first();
//        $arr_month = ['01','02','03','04','05','06','07','08','09','10','11','12'];
//        return view('Admin.archive.admin_show_years_archive',
//            compact(
//                'count_archive_year',
//            'year',
//            'count_sends_year',
//            'arr_month'));
//    }
//
//    public function getApiPost($id)
//    {
//        $api_post = apiMediascan::find($id);
//        return view('Admin.archive.admin_show_post_api', compact('api_post'));
//    }
}
