<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Billing\Orders;
use App\Models\News\VideoNews;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Charts;
use App\Models\Shop\buyUsersCourses;
use App\Models\Curses\Curse;
use App\Models\Shop\Progress;

use App\Models\Roles\Role;

use App\Models\Billing\RefKeys;

use App\Http\Controllers\Admin\Users\Settings\balanceController;

class adminHomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        
        if(Auth::user()->isAdmin()){
            $orders_count = Orders::count();
            $ref_orders = RefKeys::count();
            
            //для блока кусров и пользователей.
            $block_users_courses = new \stdClass();
            $block_users_courses->courses_free      = Curse::Discount100()->count();
            $block_users_courses->courses_lecturer  = Curse::Lecturer()->count();
            $block_users_courses->courses_gilbo     = Curse::Gilbo()->count();
            $block_users_courses->roles     = Role::get();
            

            $block_users_courses->cadet_price_all = buyUsersCourses::getSummAllActiveCurses();
            
            
            return view('Admin.adminHome',compact('orders_count','ref_orders','block_users_courses'));
        }
        
        if(Auth::user()->isLecturer()) {
            return view('Admin.lecturerHome');
        }
        
        $video_news = VideoNews::orderBy('id','DESC')->first();
        
        $user_courses = buyUsersCourses::where('user_id', Auth::user()->id)->latest()->get();
        
        $full_courses = 0;
        foreach ($user_courses as $cours) {
            $procent = $cours->currentProcent();
            $full_courses   = ($procent == 100)?++$full_courses:$full_courses;
            $cours->procent_di  = $procent;
            $cours->chart   = Charts::create('percentage', 'justgage')
                        ->title('')
                        ->elementLabel('% Пройдено')
                        ->values([$procent,0,100])
                        ->responsive(true)
                        ->height(200)
                        ->width(0);
        }
        
        $all_pay_courses = Curse::where('discount','!=',100)->count();
        
        $chart_all_end =  Charts::create('pie', 'highcharts')
                ->title('Общие данные')
                ->labels(['Платных курсов', '% пройдено'])
                ->values([$all_pay_courses,$full_courses])
                ->dimensions(300,300)
                ->responsive(true);
        
        $chart_all_end = Charts::create('percentage', 'justgage')
                    ->title('Платных курсов')
                    ->elementLabel('% пройдено')
                    ->values([$full_courses/$all_pay_courses*100,0,100])
                    ->responsive(false)
                    ->height(300)
                    ->width(0);
        
        $chart_status =  Charts::create('percentage', 'justgage')
                    ->title('Статус')
                    ->elementLabel('Мой статус')
                    ->values([Auth::user()->level_point,0,10000])
                    ->responsive(false)
                    ->height(300)
                    ->width(0);
        
        $bay_all_curses = balanceController::getButtonBayAllCourses(Auth::user());
        
        //dd($user_courses,$cours,$full_courses,$all_pay_courses);
        return view('Admin.adminHome',compact('video_news','chart_all_end','chart_status','user_courses','bay_all_curses'));

    }
    
}
