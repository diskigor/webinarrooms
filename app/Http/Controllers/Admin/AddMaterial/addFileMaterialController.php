<?php

namespace App\Http\Controllers\Admin\AddMaterial;

use App\Models\Materials\Materials;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class addFileMaterialController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('lecturerOrAdmin')->except('getXURL');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arr_type = collect(['audio' => 'Audio', 'pdf' => 'PDF', 'video_link' => 'Видео', 'archives' => 'Архив RAR,ZIP']);
        $languages = LaravelLocalization::getSupportedLocales();                                                    //Записываем доступные языки приложения
        foreach ($languages as $key => $value) {                                                                    //Перезаписываем в удобном формате
            $arr_local[$key] = $value['name'];
        }
        $arr_local = array_reverse($arr_local);

        return view('Admin.add_materials.createFileMaterials',
            compact('arr_type', 'arr_local'));
    }

    /**
     * Метод вывода аудио записей
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getFile($id)
    {
        $material = Materials::find($id);
        if ($material) {
            if ($material->type === 'pdf') {
                $fileContents = Storage::disk('local')->get($material->content);
                $response = Response::make($fileContents, 200);
                $response->header('Content-Type', "application/pdf");
            } elseif($material->type == 'audio') {
                $fileContents = Storage::disk('local')->get($material->content);
                $response = Response::make($fileContents, 200);
                $response->header('Content-Type', "audio/mp3");
            }else{
                $fileContents = Storage::disk('local')->get($material->content);
                $response = Response::make($fileContents, 200);
                $response->header('Content-Type', "video/mp4");   
            }
            return $response;
        } else {
            return back()->with('warning', 'Материал не найден');
        }
    }
    
    /**
     * Метод получения урла для материалов, по ID
     * @author Vlavlat
     * @param type $id
     * @return type
     */
    public static function getXURL($id) {
        $material = Materials::find($id);
        if ($material && Storage::disk('public')->exists($material->content)) {
            return Storage::url($material->content);
        }
        return null;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [                                                                                 //Проверка данных на валидность
            'name' => 'required',
            'lang' => 'required',
            'type' => 'required',
        ], [
            'name.required' => 'Поле "Название документа" обязательно к заполнению',                                //Ввывод ошибок при не прохождении валидации
            'lang.required' => 'Поле "Язак материала" обязательно к заполнению',
            'type.required' => 'Поле "Тип файла" обязательно к заполнению',

        ]);
        if ($request->type === 'video_link') {
            $this->validate($request, [
                'file' => 'required|mimes:mp4,mov,ogg'
            ], [
                'file.required' => 'Выберите видео в формате mp4,mov,ogg',
                'file.mimes' => 'Файл не подходящего формата(mp4,mov,ogg)'
            ]);

            $path = Storage::disk('public')->putFile('files_video', $request->file);

            $add_file_material = new Materials();
            $add_file_material->name        = $request->name;
            $add_file_material->lang_local  = $request->lang;
            $add_file_material->content     = $path;
            $add_file_material->type        = $request->type;
            $add_file_material->user_id     = $request->user()->id;
            $add_file_material->save();
            return redirect(route('materials.index'))->with('success', 'Видео файл успешно добавлен');
        }

        if ($request->type === 'pdf') {                                                                             //Проверка на тип дополнительного материала(PDF)
            $this->validate($request, [
                'file' => 'required|mimes:pdf'
            ], [
                'file.required' => 'Прикрепите файл формате PDF',
                'file.mimes' => 'Добавленый файл не в формате PDF'
            ]);

            $path = Storage::disk('public')->putFile('files_pdf', $request->file);

            $add_file_material = new Materials();
            $add_file_material->name = $request->name;
            $add_file_material->lang_local = $request->lang;
            $add_file_material->content = $path;
            $add_file_material->type = $request->type;
            $add_file_material->user_id = $request->user()->id;
            $add_file_material->save();
            return redirect(route('materials.index'))->with('success', 'PDF файл успешно добавлен');
        }

        if ($request->type === 'audio') {
            if ($request->file->getClientMimeType() === "audio/mp3") {                                              //Добавления дополнительного материала Audio                   
                $path = Storage::disk('public')->putFile('files_mp3', $request->file);

                $add_file_material = new Materials();
                $add_file_material->name = $request->name;
                $add_file_material->lang_local = $request->lang;
                $add_file_material->content = $path;
                $add_file_material->type = $request->type;
                $add_file_material->user_id = $request->user()->id;
                $add_file_material->save();
                return redirect()->route('materials.index')
                    ->with('success', 'Аудио файл успешно добавлен');
            } else {
                return back()->with('warning', 'Неправильный формат аудио');
            }
        }

        if ($request->type === 'archives') {
            if (in_array($request->file->getClientMimeType(),['application/zip','application/x-rar'])) {                                              //Добавления дополнительного материала Audio                   
                $path = Storage::disk('public')->putFile('archives', $request->file);

                $add_file_material = new Materials();
                $add_file_material->name = $request->name;
                $add_file_material->lang_local = $request->lang;
                $add_file_material->content = $path;
                $add_file_material->type = $request->type;
                $add_file_material->user_id = $request->user()->id;
                $add_file_material->save();
                return redirect()->route('materials.index')
                    ->with('success', 'Архивный файл успешно добавлен');
            } else {
                return back()->with('warning', 'Неправильный формат архива'.$request->file->getClientMimeType());
            }
        }


       return back()->with('warning', 'Неизвестный формат'); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $material = Materials::find($id);
        if(!$material) { return back()->with('warning', 'Материал не найден'); }

        $languages = LaravelLocalization::getSupportedLocales();                                                //Записываем доступные языки приложения
        foreach ($languages as $key => $value) {                                                                //Перезаписываем в удобном формате
            $arr_local[$key] = $value['name'];
        }
        $arr_local = array_reverse($arr_local);
        return view('Admin.add_materials.editFileMaterials', compact('material', 'arr_local'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required',
            'lang'=>'required'
        ]);
        $material = Materials::find($id);
        if(!$material){ return back()->with('warning','Материал не найден!'); }
        
        $material->lang_local = $request->lang;
        $material->name = $request->name;

        if($request->hasFile('file')){
            Storage::disk('public')->delete($material->content);
            switch ($material->type){
                case 'video_link':
                    $material->content = Storage::disk('public')->putFile('files_video', $request->file('file'));
                    break;
                case 'pdf':
                    $material->content = Storage::disk('public')->putFile('files_pdf', $request->file('file'));
                    break;
                case 'audio':
                    $material->content = Storage::disk('public')->putFile('files_mp3', $request->file('file'));
                    break;
                default :
                    $material->content = Storage::disk('public')->putFile('archives', $request->file('file'));
                    break;
            }
        }
        
        
        
        $material->save();
        return redirect()->route('materials.index')->with('success','Материал успешно обновлен!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
