<?php

namespace App\Http\Controllers\Admin\AddMaterial;

use App\Models\Materials\Materials;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class addTextMaterialController extends Controller
{
    public function __construct()
    {
        $this->middleware('lecturerOrAdmin');
    }
    
    /**
     * Метод для добавления текстового дополнительного материала
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->isAdmin() or Auth::user()->isLecturer()) {                                                    //Проверяем роль пользователя
           // $arr_type = collect(['text' => 'Text', 'video_link' => 'Video']);                                           //Записываем возмодный тип данных
            $languages = LaravelLocalization::getSupportedLocales();                                                    //Записываем доступные языки приложения
            foreach ($languages as $key => $value) {                                                                    //Перезаписываем в удобном формате
                $arr_local[$key] = $value['name'];
            }
            $arr_local = array_reverse($arr_local);
            return view('Admin.add_materials.createTextMaterials',
                compact( 'arr_local'));                                                          //Выводим представление для создание доп.материала.
        } else {
            // Реализовать отправку сообщения администратору о взломе.
            return back()->with('error', trans('message.hack_message'));
        }
    }

    /**
     * Метод записи созданого материала в таблицу в БД
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::user()->isAdmin() or Auth::user()->isLecturer()) {
            $this->validate($request,[
                'name'=>'required',
                'desc'=>'required'
            ]);
            $new_text_material = new Materials() ;
            $new_text_material->name        = $request->name;
            $new_text_material->lang_local  = $request->lang;
            $new_text_material->content     = $request->desc;
            $new_text_material->type        = 'text';
            $new_text_material->user_id     = $request->user()->id;
            $new_text_material->save();
            return redirect()->route('materials.index')->with('success','Материал успешно добавлен');
        } else {
            // Реализовать отправку сообщения администратору о взломе.
            return back()->with('error', trans('message.hack_message'));
        }
    }

    /**
     * Метод для вывода представления для
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $material = Materials::find($id);
        //$arr_type = collect(['text' => 'Text', 'video_link' => 'Video']);                                           //Записываем возмодный тип данных
        $languages = LaravelLocalization::getSupportedLocales();                                                    //Записываем доступные языки приложения
        foreach ($languages as $key => $value) {                                                                    //Перезаписываем в удобном формате
            $arr_local[$key] = $value['name'];
        }
        $arr_local = array_reverse($arr_local);
        if($material){
            return view('Admin.add_materials.editTextMaterials',
                compact('material','arr_local'));
        }else{
            return redirect()->route('materials.index')->with('error','Данный материал не найден');
        }
    }

    /**
     * Метод обновления текстового материала
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $material = Materials::find($id);
        if($material){
            $this->validate($request,[
                'name'=>'required',
                'desc'=>'required'
            ]);

            $material->name         = $request->name;
            $material->lang_local   = $request->lang;
            $material->content      = $request->desc;
            $material->save();
            return redirect()->route('materials.index')->with('success','Материал успешно обновлен!');
        }else{
        return redirect()->route('materials.index')->with('error','Данный материал не найден сообщите администратору');
        }

    }

}
