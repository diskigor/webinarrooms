<?php

namespace App\Http\Controllers\Admin\AddMaterial;

use App\Models\Materials\Materials;
use App\Models\Materials\MaterialsLectures;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use App\Models\Curses\Curse;

class addMaterialController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('lecturerOrAdmin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $courses = Curse::with('trans_curse')->where('status', 1);
        if (Auth::user()->isLecturer()) {
            $courses =$courses->where('user_id',Auth::user()->id)->get();
        } else {
            $courses =$courses->get();
        }

        if ($request->course_id) {
            $ci = (int)$request->course_id;
            if($request->course_id == 'null') $ci = null;
        } else {
            $ci = 0;
            unset($request->course_id);
        }

        $materials = Materials::with(['prelection','prelection.course'])
                ->orderBy('id','desc')
                ->whereHas('prelection.course', function ($query) use($ci) {
                    if ($ci) $query->where('id', $ci);
                    if ($ci === null) $query->whereNull('id');
                    if (Auth::user()->isLecturer()) $query->where('user_id',Auth::user()->id);
                  })->paginate(25)->appends($request->input());

        return view('Admin.add_materials.addMaterialsList',compact(['materials','courses']));
    }

    /**
     * Метод удаления дополнительного материала
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        MaterialsLectures::where('additional_material_id',$id)->delete();
        $m = Materials::find($id);
        if($m) {
            Storage::delete($m->content);
            Storage::disk('public')->delete($m->content);
            $m->delete();
        }
        return redirect()->route('materials.index')->with('success','Материал успешно удален');
    }
}
