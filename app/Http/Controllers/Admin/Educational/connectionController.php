<?php

namespace App\Http\Controllers\Admin\Educational;

use App\Models\Settings\MentorsConnection;
use App\Models\Settings\MentorSettings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class connectionController extends Controller
{
    /**
     * Метод вывода списка связей куратора с админом
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index()
    {
        if (Auth::user()->isAdmin()) {
            $connection = MentorsConnection::all();
            return view('Admin.admin.classroom.connections.connectionMentors', compact('connection'));
        } else {
            // Реализовать отправку сообщения администратору о взломе.
            return back()->with('warning', trans('message.hack_message'));
        }
    }
// Доработать функционал
    public function addConnection($id)
    {
        if (Auth::user()->isСurator() or Auth::user()->isLecturer()) {
            $curator = Auth::user();
            $settings = MentorSettings::orderBy('count_cadets', 'ASC')->get();
            if ($settings->count() === 0) {
                return back()->with('warning', 'На данный момент вы не можете быть куратором данному курсанту');
            }
            $check_connection = MentorsConnection::where('curator_id', $curator->id)->where('cadet_id', $id)->first();
            if ($check_connection) {
                return back()->with('warning', 'Вы уже куратор данного пользователя');
            }
            if($settings->count() === 1){
                foreach ($settings as $setting){
                    if($curator->level_point >= $setting->count_point){
                        $count_cadets = MentorsConnection::where('curator_id',$curator->id)->get();
                        $res=$setting->count_cadets - $count_cadets->count();
                        if($res === 0 or $res < 0){
                            return back('warning','Вы не можете взять больше курсантов');
                        }
                        $new_connection  = new MentorsConnection();
                        $new_connection->cadet_id = $id;
                        $new_connection->curator_id = $curator->id;
                        $new_connection->status = 1;
                        $new_connection->save();
//                        $curator->level_point = $curator->level_point - $setting->count_point;
//                        $curator->save();
                        return back()->with('success','Вы успешно стали наставником пользователя');
                    }
                }
            }
            if($settings->count() > 1){
                    foreach ($settings as $setting){
                        $arr_set[$setting->count_point] = $setting->count_cadets;
                    }
                    dd($arr_set);
            }

        } else {
            // Реализовать отправку сообщения администратору о взломе.
            return back()->with('warning', trans('message.hack_message'));
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
