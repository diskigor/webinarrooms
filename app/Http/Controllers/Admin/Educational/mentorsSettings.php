<?php

namespace App\Http\Controllers\Admin\Educational;

use App\Models\Settings\MentorSettings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class mentorsSettings extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('onlyadmin');
    }
    
    /**
     * Метод вывода представления настройки балловой системы
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index()
    {
        $mentors_settings = MentorSettings::all();
        return view('Admin.admin.classroom.settings.mentorSettings', compact('mentors_settings'));
    }

    /**
     * Метод добавления настройки
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'count_point' => 'required',
            'count_cadets' => 'required'
        ]);
        $new_setting = new MentorSettings();
        $new_setting->count_point = $request->count_point;
        $new_setting->count_cadets = $request->count_cadets;
        $new_setting->save();
        return back()->with('success', 'Вы успешно добоавили настройку');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $settting = MentorSettings::find($id);
        if (!$settting) {
            return back()->with('warning', 'Ошибка при поиске');
        } else {
            $settting->delete();
            return back()->with('success','Настройка удалена');
        }
    }
}
