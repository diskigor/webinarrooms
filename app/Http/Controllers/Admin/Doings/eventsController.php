<?php

namespace App\Http\Controllers\Admin\Doings;

use App\Models\Doings\Doings;
use App\Models\Doings\DoingsTrans;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Illuminate\Support\Facades\Storage;
use File;

class eventsController extends Controller
{
    /**
     *Языки
     * @var array
     */
    protected $arr_local;
    
    /**
     * Только администраторы
     */
    public function __construct()
    {
        $this->middleware('onlyadmin');
        
        $languages = LaravelLocalization::getSupportedLocales();                                                    //Записываем доступные языки приложения
        foreach ($languages as $key => $value) {                                                                    //Перезаписываем в удобном формате
            $this->arr_local[$key] = $value['name'];
        }
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Doings::orderBy('id', 'DESC')->paginate(15);
        return view('Admin.events.events', compact('events'));
    }

    /**
     * Вывод формы добавления события.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arr_local = array_reverse($this->arr_local);
        return view('Admin.events.createEvents', compact('arr_local'));
    }

    /**
     * Запись нового события.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $languages = LaravelLocalization::getSupportedLocales();                                                    //Получаем все языки приложения
        foreach ($languages as $key => $value) {                                                                    //Перебираем массив с языками
            $this->validate($request, [                                                                             //Созадем валидатор под каждый язык
                'name_' . $key => 'required',
                'text_' . $key => 'required',
                'description_'.$key =>'required'
            ]);
        }
        
        $request->merge(['slug'=>str_slug($request->get('slug'))]);
        
        $this->validate($request, [                                                                                 //Проверяем на валидность изображения
            'image_link' => 'required|image|mimes:jpeg,png,jpg,gif',
            'slug'=>'required|unique:sdo_events,slug'
            ],[
            'slug.required' => 'Для сео обязательно',
            'slug.unique'   => 'Этот Slug "'.$request->slug.'" уже используется, нужен новый'
        ]);
        
        
        
        $add_events = new Doings();                                                                                 //Создаем новость в таблицу News
        $add_events->image_link    = Storage::disk('public')->putFile('events', $request->file('image_link'));
        $add_events->slug = $request->slug;
        $add_events->save();

        foreach ($languages as $key => $value) {                                                                    //Перебираем языки приложения
            $add_event_trans = new DoingsTrans();                                                                   //Записываем в таблицу news_trans
            $add_event_trans->sdo_events_id = $add_events->id;
            $add_event_trans->name = $request->{'name_' . $key};
            $add_event_trans->text = $request->{'text_' . $key};
            $add_event_trans->description = $request->{'description_' . $key};
            $add_event_trans->lang_local = $key;
            $add_event_trans->save();
        }
        return redirect()->route('admin_events.index')->with('success', 'Событие успешно добавлено');         //Вернеть на страницу все новости
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Метод вывода представления для редактирования события с информацией
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit($id)
    {
            $event = Doings::find($id);
            if (!$event) {
                return back()->with('warning', 'Ошибка при поиске события!');
            }
            return view('Admin.events.editEvents', compact('event'));
    }

    /**
     * Метод редактирования события
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     *
     */
    public function update(Request $request, $id)
    {
        $update_event = Doings::find($id);  
        if(!$update_event) { return back()->with('warning', 'Ошибка при обновлении события!'); }
        
        $languages = LaravelLocalization::getSupportedLocales();                                                    //Получаем все языки приложения
        foreach ($languages as $key => $value) {                                                                    //Перебираем массив с языками
            $this->validate($request, [                                                                             //Созадем валидатор под каждый язык
                'name_' . $key => 'required',
                'text_' . $key => 'required',
                'description_'.$key =>'required'
            ]);
        } 
        $request->merge(['slug'=>str_slug($request->get('slug'))]);
        
        
        $this->validate($request,[
            'slug'=>'required|unique:sdo_events,slug,'.$update_event->id
        ],[
            'slug.required' => 'Для сео обязательно',
            'slug.unique'   => 'Этот Slug "'.$request->slug.'" уже используется, нужен новый'
        ]);
        
        $update_event->slug = $request->slug;
        
        if($request->hasFile('image_link')){
            $this->validate($request,[ 'image_link'=>'required|image|mimes:jpeg,png,jpg,gif' ]);
            
            //Удаляем старое изображение
            if (file_exists(public_path($update_event->image_link))) { File::delete($update_event->image_link); }
            Storage::disk('public')->delete($update_event->image_link);
            
            $update_event->image_link = Storage::disk('public')->putFile('events', $request->file('image_link'));
        }
        
        $update_event->save();

        foreach ($languages as $key => $value) {                                                                    //Перебираем языки приложения
            $update_event_trans = DoingsTrans::where('sdo_events_id', $update_event->id)
                ->where('lang_local', $key)
                ->first();                                                                                          //Записываем в таблицу news_trans
            $update_event_trans->name = $request->{'name_' . $key};
            $update_event_trans->text = $request->{'text_' . $key};
            $update_event_trans->description = $request->{'description_' . $key};
            $update_event_trans->save();
        }
        return redirect(route('admin_events.index'))->with('success', 'Событие успешно изменино');

    }

    /**
     * Метод удаления события
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        if (Auth::user()->isAdmin()) {                                                                                  //Проверка на роль полльзователя
            $delete_event= Doings::find($id);
            if(file_exists(public_path($delete_event->image_link))){                                                    //Проверяет есть ли старая аватарка пользователя в папке.
                File::delete($delete_event->image_link);                                                                //Если есть удаляет
            }
            $delete_event->delete();
            return redirect()->route('admin_events.index')->with('success','Событие  успешно удалено!');
        } else {
            // Реализовать отправку сообщения администратору о взломе.
            return back()->with('warning', trans('message.hack_message'));
        }
    }
}
