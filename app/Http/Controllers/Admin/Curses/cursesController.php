<?php

namespace App\Http\Controllers\Admin\Curses;

use App\Models\Curses\Curse;
use App\Models\Curses\GroupPriceCurses;
use App\Models\Curses\TransCurse;
use App\Models\Prelections\Prelection;
use App\Models\Settings\UserGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Illuminate\Support\Facades\Storage;
use File;

class cursesController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('lecturerOrAdmin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->isAdmin()) {
            $courses = Curse::orderBy('ordered')->paginate(25);
        } else {
            $courses = Curse::where('user_id',Auth::user()->id)->orderBy('ordered')->paginate(25);
        } 
        return view('Admin.curses.listCurses', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $languages = LaravelLocalization::getSupportedLocales();
        foreach ($languages as $key => $value) {
            $arr_local[$key] = $value['name'];
        }
        $arr_local = array_reverse($arr_local);
        $groups = UserGroup::get();
        if ($groups->count() === 0) {
            return back()->with('warning','Для создание курсов, введите название групп пользователей для вывода поля с ценами');
        }
        return view('Admin.curses.createCurses', compact('arr_local', 'groups'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $languages = LaravelLocalization::getSupportedLocales();                                                    //Получаем все языки приложения
            foreach ($languages as $key => $value) {                                                                    //Перебираем массив с языками
                $this->validate($request, [                                                                             //Созадем валидатор под каждый язык
                    'name_' . $key => 'required',
                    'short_desc_' . $key => 'required',
                    'description_' . $key => 'required',
                ]);
            }
            $this->validate($request, [                                                                                 //Проверяем на валидность изображения
                'image' => 'required|image|mimes:jpeg,png,jpg,gif',
                'slug_curse'=>'required'
            ]);

            $add_course = new Curse();                                                                                  //Создаем новость в таблицу Curse
            
            if ($request->user()->isAdmin()) {
                $add_course->front_status = $request->input('front_status',0);
                $add_course->front_hight = $request->input('front_hight',0);
            } 
            
            $add_course->image    = Storage::disk('public')->putFile('coursesImage', $request->file('image'));
            
            $add_course->user_id = $request->user()->id;
            
            
            if ($request->discount) {
                $add_course->discount = $request->discount;
            } else {
                $add_course->discount = 0;
            }
            $add_course->status = $request->input('status',0);
            $add_course->slug = $request->slug_curse;
            $add_course->ordered = $request->input('ordered', 0);               //* сортировка отображения последовательности курсов
            $add_course->save();                                                                                        //Сохраняем нвость
            $id_created_course = $add_course->id;                                                                       //Записываем id последней новости
            $groups_price = $request->group_price;
            foreach ($groups_price as $key => $value) {
                $connection = new GroupPriceCurses();
                $connection->course_id = $id_created_course;
                $connection->group_id = $key;
                $connection->price = $value;
                $connection->save();
            }
            foreach ($languages as $key => $value) {                                                                    //Перебираем языки приложения
                $add_course_trans = new TransCurse();                                                                   //Записываем в таблицу news_trans
                $add_course_trans->lang_local = $key;
                $add_course_trans->sdo_curses_id = $id_created_course;
                $add_course_trans->name_trans = $request->{'name_' . $key};
                $add_course_trans->short_desc_trans = $request->{'short_desc_' . $key};
                $add_course_trans->description_trans = $request->{'description_' . $key};
                if ($request->user()->isAdmin()) {
                    $add_course_trans->front_descr = $request->{'front_descr_' . $key};
                } 
                $add_course_trans->save();
            }
            return redirect()->route('curses.index')->with('success', 'Курс успешно добавлен');                   //Вернеть на страницу все новости
    }
    /**
     * Метод вывода представления редактирования курса
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::user()->isAdmin()) {
            $edit_curse = Curse::find($id);
        } else {
            $edit_curse = Curse::where('user_id',Auth::user()->id)->find($id);
        } 

        if (!$edit_curse) {  return back()->with('warning', 'Курс не найден'); }

        $groups = UserGroup::get();
        if ($groups->count() === 0) {
            return back()->with('warning','Для редактирования курсов, введите название групп пользователей для вывода поля с ценами');
        }

        $list_curs = Curse::where('id','!=',$edit_curse->id)->active()->orderBy('id')->get();//для создания списка зависимых покупок.

        return view('Admin.curses.editCurse', compact('edit_curse', 'groups' , 'list_curs'));//Возвращаем представление с данными для редактирования
            
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        if (Auth::user()->isAdmin()) {
            $curse_edit = Curse::find($id);
        } else {
            $curse_edit = Curse::where('user_id',Auth::user()->id)->find($id);
        } 
        
        if (!$curse_edit) {  return back()->with('warning', 'Курс не найден'); }
        
        $languages = LaravelLocalization::getSupportedLocales();                                                    //Получаем все языки приложения
        foreach ($languages as $key => $value) {                                                                    //Перебираем массив с языками
            $this->validate($request, [                                                                             //Созадем валидатор под каждый язык
                'name_' . $key => 'required',
                'short_desc_' . $key => 'required',
                'description_' . $key => 'required',
            ]);
        }
        
        if ($request->user()->isAdmin()) {
            $curse_edit->front_status = $request->input('front_status',0);
            $curse_edit->front_hight = $request->input('front_hight',0);
        } 
        

        if($request->hasFile('image')){
            $this->validate($request,[
                'image'=>'required|image|mimes:jpeg,png,jpg,gif',
            ]);

            if (file_exists(public_path($curse_edit->image))) {                                                       //Удаляем старое изображение
                File::delete($curse_edit->image);
            }
            Storage::disk('public')->delete($curse_edit->image);
            $curse_edit->image = Storage::disk('public')->putFile('coursesImage', $request->file('image'));
        }


        $curse_edit->discount = $request->input('discount', 0);

        $curse_edit->ordered = $request->input('ordered', 0);               //* сортировка отображения последовательности курсов
        $curse_edit->slug = $request->slug_curse;
        $curse_edit->parent_buy_id = ($request->input('parent_buy_id',null) != $curse_edit->id && $request->input('parent_buy_id',null) != 0 )?$request->input('parent_buy_id',null):null;
        $curse_edit->status = $request->input('status',0);
        $curse_edit->save();
        $id_updated_course = $curse_edit->id;                                                                       //Записываем id последней новости
        $groups_price = $request->group_price;
        foreach ($groups_price as $key => $value) {
            if ($curse_edit->groupPrice($key)) {
                $change_group_price = $curse_edit->groupPrice($key);
                $change_group_price->price = $value;
                $change_group_price->save();
            } else {
                $connection = new GroupPriceCurses();
                $connection->course_id = $id_updated_course;
                $connection->group_id = $key;
                $connection->price = $value;
                $connection->save();
            }
        }
        foreach ($languages as $key => $value) {                                                                    //Перебираем языки приложения
            $edit_course_trans = TransCurse::where('sdo_curses_id', $id_updated_course)
                ->where('lang_local', $key)
                ->first();                                                                                          //Записываем в таблицу news_trans
            $edit_course_trans->name_trans = $request->{'name_' . $key};
            //-------------------Slug-------------------------------------------------------------------------------
//                $slug = explode(' ', $request->{'name_' . $key});
//                foreach ($slug as $w => $word) {
//                    if (iconv_strlen($word) < 3) {
//                        unset($slug[$w]);
//                    }
//                }
//                $result_slug = implode('-', $slug);
            //------------------------------------------------------------------------------------------------------
//                $edit_course_trans->slug = $result_slug;
            $edit_course_trans->short_desc_trans = $request->{'short_desc_' . $key};
            $edit_course_trans->description_trans = $request->{'description_' . $key};
            
            if ($request->user()->isAdmin()) {
                $edit_course_trans->front_descr = $request->{'front_descr_' . $key};
            } 

            $edit_course_trans->save();
        }
        return redirect(route('curses.index'))->with('success', 'Курс успешно изменен');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //проверка 
        if (Auth::user()->isAdmin()) { $curse = Curse::find($id);
        } else { $curse = Curse::where('user_id',Auth::user()->id)->find($id);
        } 
        if(!$curse) { return back()->with('warning', 'Курс не найден.'); }
        if($curse->id == 1) { return back()->with('warning', 'Вы не можете удалить хранилище'); }
        
        //перемещаем лекции в хранилище
        $lecturesStorage = Prelection::where('curse_id', $id)->get();
        foreach ($lecturesStorage as $lecture) {
            $lecture->curse_id = 1;
            $lecture->save();
        }
        
        //Удаляем изображение
        if (file_exists(public_path($curse->image))) {                                                            
            File::delete($curse->image);
        }
        Storage::disk('public')->delete($curse->image);
        
        $curse->delete();

        return redirect()->route('curses.index')
            ->with('success', 'Курс успешно удален,
                лекции которые входили в данный курс перемещены в хранилище');
    }
}
