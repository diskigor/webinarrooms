<?php

namespace App\Http\Controllers\Admin\Curses\Block;




use App\Models\Curses\Curse;
use App\Models\Curses\GroupPriceCurses;
use App\Models\Curses\TransCurse;
use App\Models\Prelections\Prelection;
use App\Models\Settings\UserGroup;

use App\Models\Curses\CurseBlock;
use App\Models\Curses\CurseBlockLang;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use File;




class blockController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('onlyadmin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blocks = CurseBlock::orderBy('order_block')->paginate(20);
        return view('Admin.coursesblocks.list', compact('blocks'));
    }

    /**
     * Сразу создаем запись блока.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $block = CurseBlock::latest('order_block')->first();
        if($block){
            $i = $block->order_block + 1;
        } else {
            $i = 1;
        }
        
        $newblock = new CurseBlock([
            'user_id'       => $request->user()->id,
            'order_block'   => $i,
            'status'        => 0
                ]);
        $newblock->save();
        
        return redirect()->route('admin_block.edit',$newblock->id)->with('success', 'Блок успешно создан, нужно его настроить!');
    }

    
    /**
     * Метод вывода представления редактирования block
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $block = CurseBlock::with(['lang','courses'=>function($query) {
            return $query->orderBy('sdo_courses_block_ordred');
        }])->findOrFail($id);
        $available = Curse::with(['lang'])->whereNull('sdo_courses_block_id')->orderBy('ordered')->get();
        
        foreach ($available as $cur) {
            $cur->trans_name = $cur->lang?$cur->lang->name_trans:'';
        }
        
//dd($available,$available->pluck('trans_name','id'));
        return view('Admin.coursesblocks.edit', compact('block','available'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $block = CurseBlock::with('courses')->findOrFail($id);
        $block->update([
            'order_block'   =>$request->input('order_block', 1),
            'discount'      =>$request->input('discount', 0),
            'front_status'  =>$request->input('front_status', 0),
            'front_hight'   =>$request->input('front_hight', 0),
            'ilink'         =>$request->input('ilink', ''),
            'status'        =>$request->input('status', 0),
            ]);
        
        foreach (LaravelLocalization::getSupportedLocales() as $key => $lang) {
            
            $description = CurseBlockLang::where('sdo_courses_block_id',$block->id)->firstOrNew(['language'=>$key]);
            
            $description->name          = $request->name["$key"];
            $description->shortd        = $request->shortd["$key"];
            $description->description   = $request->description["$key"];
            
            $block->lang()->save($description);
        }
        
      
        return redirect(route('admin_block.index'))->with('success', 'Блок успешно обновлён');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $block = CurseBlock::with('courses')->findOrFail($id);
        
        foreach ($block->courses as $cours){
            $cours->sdo_courses_block_id = NULL;
            $cours->sdo_courses_block_ordred = 0;
            $cours->save();
        }
        
        $old = $block->name;
        
        $block->delete();
        
        return redirect(route('admin_block.index'))->with('success', 'Блок "'.$old.'" успешно удалён');
    }
    
    
    public function add_course_to_block(Request $request, $block_id) {
        $block = CurseBlock::with('courses')->findOrFail($block_id);
        
        $cours = Curse::findOrFail($request->input('course_id'));
        
        if (!is_null($cours->sdo_courses_block_id)) { return back()->with('warning', 'Курс привязан к другому блоку'); }
        
        $maxc = Curse::where('sdo_courses_block_id',$block->id)->latest('sdo_courses_block_ordred')->first();
        $max = $maxc?($maxc->sdo_courses_block_ordred + 1):1;
        
        $cours->sdo_courses_block_id = $block->id;
        $cours->sdo_courses_block_ordred = $max;
        
        $cours->save();
        
        return redirect(route('admin_block.edit',$block->id))->with('success', 'К блоку добавлен курс');
    }
    
    
    public function del_course_to_block(Request $request, $block_id) {
        $block = CurseBlock::with('courses')->findOrFail($block_id);
        
        $cours = Curse::findOrFail($request->input('course_id'));
        
        if (is_null($cours->sdo_courses_block_id)) { return back()->with('warning', 'Курс не привзан к блокам'); }
        
        
        $cours->sdo_courses_block_id = NULL;
        $cours->sdo_courses_block_ordred = 0;
        $cours->save();
        
        return redirect(route('admin_block.edit',$block->id))->with('success', 'КУрс откреплён от блока');
    }
}
