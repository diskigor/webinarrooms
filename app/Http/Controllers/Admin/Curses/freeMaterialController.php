<?php

namespace App\Http\Controllers\Admin\Curses;


use App\User;
use App\Models\Curses\Curse;
use App\Models\Shop\buyUsersCourses;
use App\Models\Shop\Progress;
use App\Models\Curses\TransCurse;
use App\Models\Prelections\Prelection;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class freeMaterialController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth'); 
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $free_courses = Curse::Discount100()->get();
        return view('users.free.listFreeMaterials', compact('free_courses'));
    }

    public function show(Request $request, $slug)
    {
        $get_course = Curse::Discount100()->where('slug', $slug)->first();
        if (!$get_course) { return back()->with('warning', 'Ошибка! Курс не найден! '); }
        $cours = $get_course->trans_curse[0];//TODO выяснить нахуя?
        
        
        //fix псевдо покупка
        if (!$request->user()->checkBuyCourse($get_course->id)) {
            $this->buyUserCours($get_course, $request->user());
        }
        
        return view('users.free.show_free_course', compact('cours'));
    }

    /**
     * @param $slug
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function showFreeLecture($slug,$id)
    {
        $get_course = Curse::Discount100()->where('slug', $slug)->first();
        if (!$get_course) { return back()->with('warning', 'Ошибка! Курс не найден! '); }
        
        if ($get_course->groupPrice(Auth::user()->group_id) && $get_course->groupPrice(Auth::user()->group_id)->price !== 0) {
            return back()->with('warning', 'Ошибка! Эта лекция/курс, недоступны, в открытых материалах!');
        }

        $free_lecture = Prelection::find($id);
        if (!$free_lecture) { return back()->with('warning', 'Ошибка!Лекция не найдена сообщите администратору сайта!'); }
        
        if (!in_array($id, $get_course->lectures->pluck('id')->toArray())) { return back()->with('warning', 'Ошибка!Лекция от другого курса!'); }
        
        foreach ($free_lecture->test_tasks as $item) {
            if ($item->typeTest->name === 'group') {
                foreach ($item->testQuestions as $question) {
                    $arr_group_test[$question->group_number][] = $question;
                }
                $item->questions_group = $arr_group_test;
            }
        }

        return view('users.free.show_free_prelect', compact('free_lecture'));

    }

    /**
     * Запись в таблицу купленных пользователем курсов и доступных для изучения
     * @param Curse $course
     * @param User $user
     * @return buyUsersCourses
     */
    protected function buyUserCours(Curse $course, User $user) {
        $add_course_user = new buyUsersCourses();
        $add_course_user->user_id = $user->id;
        $add_course_user->curses_id = $course->id;
        $add_course_user->save();
        
        //инициализируем прохождение, купленного курса
        $this->initProgress($course, $user);
        
        return $add_course_user;
    }
    
    /**
     * Инициализация прохождения курса пользователем
     * @param Curse $course
     * @param User $user
     * @return Progress
     */
    protected function initProgress(Curse $course, User $user) {
        $progress = new Progress();
        $progress->user_id = $user->id;
        $progress->curses_id = $course->id;
        $progress->step = 0;
        $progress->save();
        return $progress;
    }
}
