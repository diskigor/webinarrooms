<?php

namespace App\Http\Controllers\Admin\Front\Project;

use App\Models\Project\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use File;
use Illuminate\Support\Facades\Storage;

class projectController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('onlyadmin');
    }
    
    /**
     * Метод вывода представления списка проектов
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $local = App::getLocale();
        $projects = Project::where('lang_local', $local)->get();
        return view('Admin.admin.project.listProject', compact('projects'));
    }

    /**
     * Метод вывода представления добавления проектов на сайт
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function create()
    {
        $languages = LaravelLocalization::getSupportedLocales();
        foreach ($languages as $key => $value) {
            $arr_local[$key] = $value['name'];
        }
        $arr_local = array_reverse($arr_local);
        return view('Admin.admin.project.createProject', compact('arr_local'));
    }

    /**
     * Метод созранения проекта СДО в таблицу
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {   
        //Проверка на валидность
        $this->validate($request, [                                                                                  
            'image' => 'required|image|mimes:jpeg,png,jpg',
            'lang' => 'required',
            'name_project' => 'required'
        ]);
        
        $add_project = new Project();
        
        $add_project->main_div      = $request->get('main',0)?$request->get('main',0):0;
        $add_project->side_bar      = $request->get('side_bar',0);
        $add_project->name          = strip_tags($request->get('name_project'));
        $add_project->href          = $request->get('href_project','')?$request->get('href_project',''):'';
        $add_project->lang_local    = $request->get('lang');
        $add_project->image_link    = Storage::disk('public')->putFile('projectImage', $request->file('image'));
        $add_project->short_desc    = $request->get('project_description');
        
        $add_project->save();
        return redirect(route('else_project.index'))->with('success', 'Проект успешно добавлен');
    }

    /**
     * Метод вывода представления для редактирования проектов
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit($id)
    {
        $languages = LaravelLocalization::getSupportedLocales();
        foreach ($languages as $key => $value) {
            $arr_local[$key] = $value['name'];
        }
        $arr_local = array_reverse($arr_local);
        $project = Project::find($id);
        if ($project) {
            return view('Admin.admin.project.editProject', compact('project', 'arr_local'));
        }
    }

    /**
     * Метод сохранения отредактированого проекта
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $project = Project::find($id);
        if (!$project) { return back()->with('warning', 'Ошибка при поиске проекта'); }
        
        //Проверка на валидность
        $this->validate($request, [ 'lang' => 'required',
                                    'name_project' => 'required']);
        

        $project->main_div      = $request->get('main',0)?$request->get('main',0):0;
        $project->side_bar      = $request->get('side_bar',0);
        $project->lang_local    = $request->get('lang');
        $project->name          = $request->get('name_project');
        $project->href          = $request->get('href_project','')?$request->get('href_project',''):'';
        
        if($request->hasFile('image')){
            //проверка
            $this->validate($request,[ 'image'=>'image|mimes:jpeg,png,jpg,gif' ]);
            //Удаляем старое изображение
            if (file_exists(public_path($project->image_link))) { File::delete($project->image_link); }
            Storage::disk('public')->delete($project->image_link);
            //пишем новое в хранилище
            $project->image_link = Storage::disk('public')->putFile('projectImage', $request->file('image'));
        }

        $project->short_desc = $request->get('project_description');
        $project->save();
        return redirect(route('else_project.index'))->with('success','Проект успешно обновлен');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Project::find($id);
        
        //Удаляем старое изображение
        if(file_exists(public_path($project->image_link))){                                                     
            File::delete($project->image_link);
        }
        Storage::disk('public')->delete($project->image_link);
        
        $project->delete();
        return redirect(route('else_project.index'))->with('success','Проект успешно удален');
    }
}
