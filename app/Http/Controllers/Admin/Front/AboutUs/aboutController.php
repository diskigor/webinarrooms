<?php

namespace App\Http\Controllers\Admin\Front\AboutUs;

use App\Models\FrontModel\AboutUs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use File;

class aboutController extends Controller
{

    /**
     * Метод вывода админу представления About Us
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function index()
    {
        if (Auth::user()->isAdmin()) {
            $about_us = AboutUs::first();
            return view('Admin.admin.about.aboutUs', compact('about_us'));
        } else {
            // Реализовать отправку сообщения администратору о взломе.
            return back()->with('warning', trans('message.hack_message'));
        }

    }

    /**
     * Метод добавления информации для страницы о нас
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if (Auth::user()->isAdmin()) {
            $about = new AboutUs();
            $this->validate($request, [
                'about_us' => 'required'
            ]);
            $about->about_us = $request->about_us;
            $this->validate($request, [
                'image_logo' => 'required|image|mimes:jpeg,png,jpg',
            ]);
            $about_image = $request->image_logo->getClientOriginalName();                                               //Получаем имя изображения
            $imageX = explode('.', $about_image);                                                              //Разбиваем строку
            $hash = hash('md5', $imageX[0]);                                                                      //Хэшируем имя картинки
            $imageX[0] = 'about_' . $hash;                                                                              //Записываем в массив
            $imageX = 'images/about/' . implode('.', $imageX);                                                    //Конкатенируем строку
            $upload = public_path('/images/about');                                                               //Записываем путь где будем хранить изображения
            $image = $request->image_logo;
            $image->move($upload, $imageX);                                                                             //Перемещение изображения в папку
            $about->image_logo = $imageX;
            $about->save();
            return back()->with('success', 'Страница о нас цуспешно обновлена');
        } else {
            // Реализовать отправку сообщения администратору о взломе.
            return back()->with('warning', trans('message.hack_message'));
        }
    }

    /**
     * Метод обновления страницы о нас
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        if (Auth::user()->isAdmin()) {
            $about = AboutUs::find($id);
            if (!$about) {
                return back()->with('warning', 'Ошибка при редактировании');
            }
            $this->validate($request, [
                'about_us' => 'required'
            ]);
            $about->about_us = $request->about_us;
            if ($request->image_logo) {
                if (file_exists(public_path($about->image_logo))) {                                                     //Удаляем старое изображение
                    File::delete($about->image_logo);
                }
                $this->validate($request, [
                    'image_logo' => 'required|image|mimes:jpeg,png,jpg',
                ]);
                $about_image = $request->image_logo->getClientOriginalName();                                           //Получаем имя изображения
                $imageX = explode('.', $about_image);                                                          //Разбиваем строку
                $hash = hash('md5', $imageX[0]);                                                                  //Хэшируем имя картинки
                $imageX[0] = 'about_' . $hash;                                                                          //Записываем в массив
                $imageX = 'images/about/' . implode('.', $imageX);                                                //Конкатенируем строку
                $upload = public_path('/images/about');                                                           //Записываем путь где будем хранить изображения
                $image = $request->image_logo;
                $image->move($upload, $imageX);                                                                         //Перемещение изображения в папку
                $about->image_logo = $imageX;
            }
            $about->save();
            return back()->with('success', 'Страница о нас цуспешно обновлена');
        } else {
            // Реализовать отправку сообщения администратору о взломе.
            return back()->with('warning', trans('message.hack_message'));
        }
    }

}
