<?php

namespace App\Http\Controllers\Admin\Front\AboutUs;

use App\Models\FrontModel\Advantages;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use File;

class advantagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $advantages = Advantages::get();
        return view('Admin.admin.about.advantages', compact('advantages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Метод добавления достижения
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if (Auth::user()->isAdmin()) {
            $this->validate($request, [
                'image_link' => 'required|image|mimes:jpeg,png,jpg',
                'advantage' => 'required'
            ]);
            $advantage_new = new Advantages();
            $advantage_new->advantage = $request->advantage;
            $image = $request->image_link->getClientOriginalName();                                                     //Получаем имя изображения
            $imageX = explode('.', $image);                                                                    //Разбиваем строку
            $hash = hash('md5', $imageX[0]);                                                                      //Хэшируем имя картинки
            $imageX[0] = 'advantage_' . $hash;                                                                          //Записываем в массив
            $imageX = 'images/advantage_/' . implode('.', $imageX);                                               //Конкатенируем строку
            $upload = public_path('/images/advantage_');                                                          //Записываем путь где будем хранить изображения
            $image = $request->image_link;
            $image->move($upload, $imageX);
            $advantage_new->image_link = $imageX;
            $advantage_new->save();
            return back()->with('success', 'Преимущество успешно добавлено');
        } else {
            // Реализовать отправку сообщения администратору о взломе.
            return back()->with('warning', trans('message.hack_message'));
        }
    }


    /**
     * Метод редактирования преимущества
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        if (Auth::user()->isAdmin()) {
            $advant = Advantages::find($id);
            if (!$advant) {
                return back()->with('warning', 'Данное преимущество не найдено');
            }
            $this->validate($request, [
                'advantage' => 'required',
            ]);
            $advant->advantage = $request->advantage;
            if ($request->new_image) {
                $this->validate($request, [
                    'new_image' => 'image|mimes:jpeg,png,jpg'
                ]);
                if (file_exists(public_path($advant->image_link))) {                                                    //Удаляем старое изображение
                    File::delete($advant->image_link);
                }
                $image = $request->new_image->getClientOriginalName();                                                  //Получаем имя изображения
                $imageX = explode('.', $image);                                                                //Разбиваем строку
                $hash = hash('md5', $imageX[0]);                                                                  //Хэшируем имя картинки
                $imageX[0] = 'advantage_' . $hash;                                                                      //Записываем в массив
                $imageX = 'images/advantage_/' . implode('.', $imageX);                                           //Конкатенируем строку
                $upload = public_path('/images/advantage_');                                                      //Записываем путь где будем хранить изображения
                $image = $request->new_image;
                $image->move($upload, $imageX);
                $advant->image_link = $imageX;
            }
            $advant->save();
            return back()->with('success', 'Преимущество успешно обновлено');

        } else {
            // Реализовать отправку сообщения администратору о взломе.
            return back()->with('warning', trans('message.hack_message'));
        }
    }

    /**
     * Метод удаления преимущетсва
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        if (Auth::user()->isAdmin()) {
            $advantage = Advantages::find($id);
            if (!$advantage) {
                return back()->with('warning', 'Не удалось найти преимущество');
            }
            if (file_exists(public_path($advantage->image_link))) {                                                     //Удаляем старое изображение
                File::delete($advantage->image_link);
            }
            $advantage->delete();
            return back()->with('success', 'Преимущество успешно удалено');
        } else {
            // Реализовать отправку сообщения администратору о взломе.
            return back()->with('warning', trans('message.hack_message'));
        }
    }
}
