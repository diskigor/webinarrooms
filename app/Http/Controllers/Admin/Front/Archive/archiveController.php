<?php

namespace App\Http\Controllers\Admin\Front\Archive;

use App\Models\Api\apiMediascan;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class archiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $count_sends_year = DB::table('mediascan')
            ->select('year', DB::raw('count(id) as total'))
            ->groupBy('year')
            ->get()
            ->pluck('total', 'year');
        $count_sends_year = $count_sends_year->reverse();
        $current_y =date('Y');
        $count_archive_year = apiMediascan::where('year', $current_y)->first();
        $arr_month = ['01','02','03','04','05','06','07','08','09','10','11','12'];
        return view('front.api.show_archive', compact(
            'count_sends_year',
                'count_archive_year',
                'current_y',
                'arr_month'));
    }

    public function getYearApi($year)
    {
        $count_sends_year = DB::table('mediascan')
            ->select('year', DB::raw('count(id) as total'))
            ->groupBy('year')
            ->get()
            ->pluck('total', 'year');
        $count_sends_year = $count_sends_year->reverse();
        $count_archive_year = apiMediascan::where('year', $year)->first();
        $arr_month = ['01','02','03','04','05','06','07','08','09','10','11','12'];
        return view('front.api.show_years_archive', compact('count_archive_year',
            'year',
            'count_sends_year',
            'arr_month'));
    }


    public function getApiPost($id)
    {
        $api_post = apiMediascan::find($id);
        return view('front.api.show_post_api', compact('api_post'));
    }
}
