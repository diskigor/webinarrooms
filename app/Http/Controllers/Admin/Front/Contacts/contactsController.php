<?php

namespace App\Http\Controllers\Admin\Front\Contacts;

use App\Models\FrontModel\Contacts;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class contactsController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('onlyadmin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = Contacts::first();
        return view('Admin.admin.contacts.contacts', compact('contacts'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contacts = new Contacts();
        if (isset($request->number_one)) {
            $contacts->number_one = $request->number_one;
        }
        if (isset($request->number_two)) {
            $contacts->number_two = $request->number_two;
        }
        if (isset($request->email_one)) {
            $contacts->email_one = $request->email_one;
        }
        if(isset($request->email_two)){
            $contacts->email_two = $request->email_two;
        }
        $contacts->save();
        return back()->with('success','Контакты успешно обновлены');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $contacts = Contacts::find($id);
        if (isset($request->number_one)) {
            $contacts->number_one = $request->number_one;
        }
        if (isset($request->number_two)) {
            $contacts->number_two = $request->number_two;
        }
        if (isset($request->email_one)) {
            $contacts->email_one = $request->email_one;
        }
        if(isset($request->email_two)){
            $contacts->email_two = $request->email_two;
        }
        $contacts->save();
        return back()->with('success','Контакты успешно обновлены');
    }
}
