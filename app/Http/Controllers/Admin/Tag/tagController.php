<?php

namespace App\Http\Controllers\Admin\Tag;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

use App\Models\Blog\Blog;

use Lecturize\Tags\Models\Tag;
use Lecturize\Tags\Models\Taggable;

class tagController extends Controller
{
    /**
     * Только админы
     */
    public function __construct()
    {
        $this->middleware('onlyadmin');
    }
    
    /**
     * список тегов
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index()
    {
        $tags = Tag::paginate(10);
        return view('Admin.admin.tag.list', compact('tags'));
    }

    /**
     * Метод вывода пердставления для создания тега
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function create()
    {
        return view('Admin.admin.tag.create');
    }

    /**
     * Метод созранения тега в таблицу БД
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'tag'=>'required|unique:tags,tag'
        ]);
        
        $model = new Tag;
        $model->tag = trim($request->input('tag'));
        $model->save();


        return redirect(route('atags.index'))->with('success','Тег успешно добавлен!');

    }

    /**
     * Метод вывода представления для редактирования тега
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit($id)
    {

        $tag = Tag::find($id);

        if(!$tag){
            return back()->with('warning','Ошибка при поиске тега!');
        }

        return view('Admin.admin.tag.edit',compact('tag'));

    }

    /**
     * Метод обновления тега
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $tag = Tag::find($id);
        if(!$tag){
            return back()->with('warning','Тег не найден!');
        }
        $this->validate($request,[
            'tag'=>'required|unique:tags,tag,'.$id
        ]);

        $tag->tag = trim($request->input('tag'));
        $tag->save();
        
        return redirect(route('atags.index'))->with('success','Тег успешно отредактирован!');
    }

    /**
     * Метод удаления тега
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $tag = Tag::find($id);
        if(!$tag){
            return back()->with('warning','Тег не найден!');
        }

        $tag->delete();
        return back()->with('success','Тег успешно удален!');
    }
}
