<?php

namespace App\Http\Controllers\Admin\Statistics;

use App\Models\Billing\Balls;
use App\DataTables\AdminBallsDataTable;

use App\Http\Controllers\Controller;



class ballsController extends Controller
{
    
    /**
     * Только админы
     */
    public function __construct()
    {
        $this->middleware('onlyadmin');
    }
    
    /**
     * Метод вывода статистики
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index()
    {
        return view('Admin.statistics.balls.index');
    }
    
    
    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAjaxData(AdminBallsDataTable $dataTable)
    {
        return $dataTable->render('dataTables.ordersStatistic');
    }

    
   
    /**
     * Метод удаления записи о баллах
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {

        $order = Balls::find($id);
        if(!$order){
            return back()->with('warning','Данная запись не найдена!');
        }
        $order->delete();
        return back()->with('success','Запись успешно удалена!');
    }

}
