<?php

namespace App\Http\Controllers\Admin\Statistics;

use App\Models\Billing\Orders;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


use App\DataTables\OrdersDataTable;

class ordersController extends Controller
{
    /**
     * Метод вывода статистики
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index()
    {
        if (Auth::user()->isAdmin() or Auth::user()->isLecturer()) {
//            if(Auth::user()->isLecturer()){
//                $orders = Orders::where('lecture_id',Auth::user()->id)->orderBy('id','desc')->paginate(15);
//            }else{
//                $orders = Orders::orderBy('id','desc')->paginate(15);
//            }
            return view('Admin.statistics.ordersStatistics',compact('orders'));
        } else {
            // Реализовать отправку сообщения администратору о взломе.
            return back()->with('warning', trans('message.hack_message'));
        }
    }
    
    
    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyData(OrdersDataTable $dataTable)
    {
        return $dataTable->render('dataTables.ordersStatistic');
    }

    
    /**
     * Метод получения лектором своих бонусов.
     * @return \Illuminate\Http\RedirectResponse
     */
    public function collectBonuses(){
        if(Auth::check() and Auth::user()->isLecturer()){
            $lector = Auth::user();
            $sum_bonus = Orders::where('lecture_id',$lector->id)->sum('lecture_sum');

            if($sum_bonus <= 0){
                return back()->with('warning','Вы уже получили бонусы!Новых нет!');
            }
            $get_bonus = new Orders();
            $get_bonus->lecture_id = $lector->id;
            $get_bonus->sum = 0;
            $get_bonus->lecture_sum = '-'.$sum_bonus;
            $get_bonus->save();
            return back()->with('success','Ваш бонус начислен на счет! ');
        }else{
            // Реализовать отправку сообщения администратору о взломе.
            return back()->with('warning', trans('message.hack_message'));
        }
    }

    /**
     * Метод удаления записи о покупке
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        if(Auth::user()->isAdmin()){
            $order = Orders::find($id);
            if(!$order){
                return back()->with('warning','Данная запись не найдена!');
            }
            $order->delete();
            return back()->with('success','Запись успешно удалена!');
        }else{
            // Реализовать отправку сообщения администратору о взломе.
            return back()->with('warning', trans('message.hack_message'));
        }
    }

    /**
     * Метод сортировки пользователей по email
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function emailSearchUser(Request $request){
        if(Auth::user()){
            if($request->email_user){
                $this->validate($request,[
                    'email_user'=>'required|email|max:255|'
                ]);
                $user = User::where('email',$request->email_user)->first();
                if(!$user){
                    return redirect(route('orders_statistics.index'))
                        ->with('warning','Пользователь с таким Email не найден');
                }
                if($request->user()->isLecturer()) {
                    $orders = Orders::where('user_id',$user->id)->where('lecture_id',$request->user()->id)
                        ->orderBy('created_at','DESC')
                        ->paginate(15); 
                } else {
                    $orders = Orders::where('user_id',$user->id)
                        ->orderBy('created_at','DESC')
                        ->paginate(15);
                }

                if(!$orders){
                    return redirect(route('orders_statistics.index'))
                        ->with('warning','Пользователь с таким Email не покупал');
                }
                return view('Admin.statistics.ordersStatistics',compact('orders'));
            }
        }else{
            return back()->with('warning', trans('message.hack_message'));
        }
    }
}
