<?php

namespace App\Http\Controllers\Admin\Statistics;

use App\Models\Billing\Billing;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\DataTables\BillingDataTable;

class bilingController extends Controller
{
    
    /**
     * Только админы
     */
    public function __construct()
    {
        $this->middleware('onlyadmin');
    }
    
    /**
     * Метод вывода данных о пополнении пользователями личных счетов
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index()
    {
//        if (Auth::user()->isAdmin() or Auth::user()->isLecturer()) {
//            $billing_orders = Billing::where('type_replenishment',0)->orderBy('id','desc')->paginate(15);
//            return view('Admin.statistics.bilingStatistics', compact('billing_orders'));
//        } else {
//            // Реализовать отправку сообщения администратору о взломе.
//            return back()->with('warning', trans('message.hack_message'));
//        }
        return view('Admin.statistics.orders.index');
    }
    
    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAjaxData(BillingDataTable $dataTable)
    {
        return $dataTable->render('dataTables.billing.index');
    }
    
    
    

    /**
     * Метод удаления записи с таблицы  о пополнении личного счета на сайте.
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {

        $billing_order = Billing::find($id);
        $billing_order->delete();
        return back()->with('success','Запись успешно удалена');

    }

    public function adminReplStatistics(){
        $billing_orders = Billing::where('type_replenishment',1)
            ->orderBy('created_at','DESC')
            ->paginate(15);
        return view('Admin.statistics.billing_replen_admin',compact('billing_orders'));
    }

    public function delReplOrder($id){
            $billing_order = Billing::find($id);
            $billing_order->delete();
            return back()->with('success','Пополнение отменено успешно!');
    }

    public function emailSearchUser(Request $request){
        if(Auth::user()){
            if($request->email_user){
                $this->validate($request,[
                    'email_user'=>'required|email|max:255|'
                ]);
                $user = User::where('email',$request->email_user)->first();
                if(!$user){
                return redirect(route('admin_repl_statistics'))->with('warning','Пользователь с таким Email не найден');
                }
                $billing_orders = Billing::where('type_replenishment',1)
                    ->where('user_id',$user->id)
                    ->orderBy('created_at','DESC')
                    ->get();
                return view('Admin.statistics.billing_replen_admin',compact('billing_orders'));
            }
        }else{
            return back()->with('warning', trans('message.hack_message'));
        }
    }
}
