<?php

namespace App\Http\Controllers\Admin\News;

use App\Models\News\VideoNews;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Illuminate\Support\Facades\Storage;

class videoNewsController extends Controller
{
    
    /**
     * только лекторы и админы
     */
    public function __construct()
    {
        $this->middleware('lecturerOrAdmin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Admin.news.video_news',[
            'video_news'=>VideoNews::all()
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $languages = LaravelLocalization::getSupportedLocales();                                                    //Записываем доступные языки приложения
        
        foreach ($languages as $key => $value) {                                                                    //Перезаписываем в удобном формате
            $arr_local[$key] = $value['name'];
        }
        $arr_local = array_reverse($arr_local);
        return view('Admin.news.createVideoNews',compact('arr_local'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $this->validate($request,[
                //'lang'=>'required',
                'short_desc'=>'required',
                'video'=>'required|mimetypes:video/avi,video/mpeg,video/quicktime,video/mp4,video/ogg',
            ]);
            $add_video_news             = new VideoNews();
            $add_video_news->lang_local = 'ru';
            $add_video_news->short_desc = strip_tags($request->input('short_desc'));
            $add_video_news->href       = Storage::disk('public')->putFile('news_video_files', $request->file('video'));
            $add_video_news->status     = $request->input('status', 0);
            $add_video_news->save();
            return redirect(route('video_news.index'))->with('success','Видео новость успешно добавлена');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $edit_video_news= VideoNews::find($id);
        if(!$edit_video_news)  { return back()->with('warning','Ошибка при поиске видео новости'); }
        
        $languages = LaravelLocalization::getSupportedLocales();                                                    //Записываем доступные языки приложения
        foreach ($languages as $key => $value) {                                                                    //Перезаписываем в удобном формате
            $arr_local[$key] = $value['name'];
        }
        $arr_local = array_reverse($arr_local);
        return view('Admin.news.editVideoNews',compact('edit_video_news','arr_local'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $update_video_news = VideoNews::find($id);
            if(!$update_video_news){ return back()->with('warning','Ошибка при поиске видео новости!'); }
            
            $this->validate($request,[
                //'lang'=>'required',
                'short_desc'=>'required',
                //'href'=>'required|url',
            ]);
            $update_video_news->lang_local = 'ru';
            $update_video_news->short_desc = strip_tags($request->input('short_desc'));
            
            if($request->hasFile('video')){
                $this->validate($request,[
                    'video'=>'required|mimetypes:video/avi,video/mpeg,video/quicktime,video/mp4,video/ogg',
                ]);
                //Удаляем старое изображение
                Storage::disk('public')->delete($update_video_news->href);
                $update_video_news->href = Storage::disk('public')->putFile('news_video_files', $request->file('video'));
            }
            $update_video_news->status = $request->input('status', 0);;
            $update_video_news->save();
            return redirect(route('video_news.index'))->with('success','Видео новость успешно обновлена!');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            $video_news = VideoNews::find($id);
            if(!$video_news){ return back()->with('warning','Ошибка при поиске видео-новости'); }
            
            //Удаляем старое изображение
            Storage::disk('public')->delete($video_news->href);
            
            $video_news->delete();
            return back()->with('success','Видео новость успешно удалена!');

    }
}
