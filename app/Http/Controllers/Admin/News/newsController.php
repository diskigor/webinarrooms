<?php

namespace App\Http\Controllers\Admin\News;

use App\Models\News\News;
use App\Models\News\NewsTrans;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Illuminate\Support\Facades\Storage;
use File;

class newsController extends Controller
{
    /**
     * только лекторы и админы
     */
    public function __construct()
    {
        $this->middleware('lecturerOrAdmin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::with('lang')->paginate(25);
        return view('Admin.news.news', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $languages = LaravelLocalization::getSupportedLocales();
        foreach ($languages as $key => $value) {
            $arr_local[$key] = $value['name'];
        }
        $arr_local = array_reverse($arr_local);
        return view('Admin.news.createNews', ['arr_local' => $arr_local]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $languages = LaravelLocalization::getSupportedLocales();                                                    //Получаем все языки приложения
        foreach ($languages as $key => $value) {                                                                    //Перебираем массив с языками
            $this->validate($request, [                                                                             //Созадем валидатор под каждый язык
                'title_' . $key => 'required',
                'article_' . $key => 'required',
            ]);
        } 
        //Проверяем на валидность изображения
        $this->validate($request, ['image' => 'required|image|mimes:jpeg,png,jpg,gif|dimensions:max_width=1200,max_height=900']);
        
        $add_news = new News();                                                                                     //Создаем новость в таблицу News
        $add_news->image_link = Storage::disk('public')->putFile('newsImages', $request->file('image'));
        $add_news->save();
        
        foreach ($languages as $key => $value) {                                                                    //Перебираем языки приложения
            $add_news_trans = new NewsTrans();                                                                      //Записываем в таблицу news_trans
            $add_news_trans->news_id = $add_news->id;
            $add_news_trans->title = $request->{'title_' . $key};
            $add_news_trans->article = $request->{'article_' . $key};
            $add_news_trans->lang_local = $key;
            $add_news_trans->slug = str_slug($request->{'title_' . $key});
            $add_news_trans->save();
        }
        return redirect()->route('news.index')->with('success', 'Новость успешно добавлена');                 //Вернеть на страницу все новости
    }

    /**
     * Метод вывода новости для редактирования
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $articles= News::with('lang')->find($id);
        if(!$articles) { return back()->with('warning','Новость не найдена'); }
        
        //Возвращаем представления с данными для редактирования
        return view('Admin.news.editNews',compact('articles'));                               
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update_news = News::find($id); 
        if(!$update_news) { return back()->with('warning','Новость не найдена'); }
        
        $languages = LaravelLocalization::getSupportedLocales();                                                    //Получаем все языки приложения
        foreach ($languages as $key => $value) {                                                                    //Перебираем массив с языками
            $this->validate($request, [                                                                             //Созадем валидатор под каждый язык
                'title_' . $key => 'required',
                'article_' . $key => 'required',
            ]);
        }

        if($request->hasFile('image')){
            $this->validate($request,[
                'image'=>'required|image|mimes:jpeg,png,jpg,gif|dimensions:max_width=1200,max_height=900',
            ]);
            //Удаляем старое изображение
            if (file_exists(public_path($update_news->image))) { File::delete($update_news->image); }
            Storage::disk('public')->delete($update_news->image);
            $update_news->image = Storage::disk('public')->putFile('newsImages', $request->file('image'));
        }
        $update_news->save(); 
        
        
        foreach ($languages as $key => $value) {                                                                    //Перебираем языки приложения
            $update_news_trans = NewsTrans::where('news_id',$update_news->id)
                ->where('lang_local',$key)
                ->first();                                                                                          //Записываем в таблицу news_trans
            $update_news_trans->title = $request->{'title_' . $key};
            $update_news_trans->article = $request->{'article_' . $key};
            $update_news_trans->slug = str_slug($request->{'title_' . $key});
            $update_news_trans->save();
        }
        return redirect()->route('news.index')->with('success','Новость успешно обновлена');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            $delete_news= News::find($id);
            if(!$delete_news) { return back()->with('warning','Новость не найдена'); }
            
            //Если есть удаляет
            if(file_exists(public_path($delete_news->image_link))){
                File::delete($delete_news->image_link);                                                                 
            }
            Storage::disk('public')->delete($delete_news->image);
            $delete_news->delete();
            return redirect()->route('news.index')->with('success','Новость успешно удалена!');
    }
}
