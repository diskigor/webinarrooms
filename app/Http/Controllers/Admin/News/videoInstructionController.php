<?php

namespace App\Http\Controllers\Admin\News;

use App\Models\News\VideoInsrtuction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class videoInstructionController extends Controller
{
    
    /**
     * только лекторы и админы
     */
    public function __construct()
    {
        $this->middleware('lecturerOrAdmin')->except('index');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->isAdmin()) {
            return view('Admin.news.instruction.index',[
                        'instructions'=>VideoInsrtuction::orderBy('order')->paginate(9)
                        ]);
        }
        
        return view('users.news.list_video_instr',[
            'instructions'=>VideoInsrtuction::orderBy('order')->paginate(9)
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.news.instruction.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $this->validate($request,[
                'name'  => 'required|min:1',
                'short' => 'required|min:1',
                'video' => 'required|mimetypes:video/avi,video/mpeg,video/quicktime,video/mp4,video/ogg',
            ]);
            
            $instruction            = new VideoInsrtuction();
            $instruction->name      = strip_tags($request->input('name'));
            $instruction->short     = strip_tags($request->input('short'));
            $instruction->status    = $request->input('status',0);
            $instruction->order     = $request->input('order',0);
            $instruction->href      = Storage::disk('public')->putFile('news_video_instruction', $request->file('video'));
            $instruction->user_id   = $request->user()->id;
            $instruction->save();
            return redirect(route('video_instruction.index'))->with('success','Видео инструкция успешно добавлена');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $instruction    = VideoInsrtuction::find($id);
        if(!$instruction)  { return back()->with('warning','Ошибка при поиске видео инструкции'); }

        return view('Admin.news.instruction.edit',[
            'instruction'   => $instruction
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $instruction    = VideoInsrtuction::find($id);
        if(!$instruction)  { return back()->with('warning','Ошибка при поиске видео инструкции'); }
        
        $this->validate($request, [
            'name'  => 'required|min:1',
            'short' => 'required|min:1',
        ]);

        $instruction->name      = strip_tags($request->input('name'));
        $instruction->short     = strip_tags($request->input('short'));
        $instruction->status    = $request->input('status',0);
        $instruction->order     = $request->input('order',0);

        if($request->hasFile('video')){
            $this->validate($request,[
                'video'=>'required|mimetypes:video/avi,video/mpeg,video/quicktime,video/mp4,video/ogg',
            ]);
            //Удаляем старое видео
            Storage::disk('public')->delete($instruction->href);
            $instruction->href = Storage::disk('public')->putFile('news_video_instruction', $request->file('video'));
        }

        $instruction->save();

        return redirect(route('video_instruction.index'))->with('success','Видео инструкция успешно обновлена!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $instruction    = VideoInsrtuction::find($id);
        if(!$instruction)  { return back()->with('warning','Ошибка при поиске видео инструкции'); }
            
        //Удаляем старое видео
        Storage::disk('public')->delete($instruction->href);
        
        $instruction->delete();
        return back()->with('success','Видео инструкция успешно удалена!');
    }
}
