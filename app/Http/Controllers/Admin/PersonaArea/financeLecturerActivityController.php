<?php

namespace App\Http\Controllers\Admin\PersonaArea;

use App\Http\Controllers\Controller;



use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;


use App\Models\Billing\Billing;
use App\Models\Billing\Orders;

class financeLecturerActivityController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $start_day = Carbon::now()->subMonth();
        if ($request->input('start_day', NULL )) {
            try{
                $start_day = Carbon::createFromFormat('Y-n-j',$request->input('start_day'))->startOfDay();
            } catch (Exception $e) {
            }
        } 
        
        $end_day = Carbon::now();
        if ($request->input('end_day', NULL )) {
            $end_day = $request->input('end_day');
            try{
                $end_day = Carbon::createFromFormat('Y-n-j',$request->input('end_day'))->endOfDay();
            } catch (Exception $e) {
            }
        } 
                
        $all_curses_buy = Orders::where('created_at','>',$start_day)
                ->where('created_at','<',$end_day)
                ->where('lecture_id',$request->user()->id)
                ->sum('lecture_sum');
        
        
        $all_vebinars_buy  = 0;
        

        

            
        $block_finance_activity = new \stdClass();
        $block_finance_activity->curses = $all_curses_buy;
        $block_finance_activity->vebinars = $all_vebinars_buy;
        $block_finance_activity->sum = $all_curses_buy+$all_vebinars_buy;
        
            
        
        return view('Admin.personal_area.block_lecturer_finance_activity',
                ['block_finance_activity'=>$block_finance_activity,
                    'start_day'=>$start_day->format('d/m/Y'),
                    'end_day'=>$end_day->format('d/m/Y')]);


    }
    
    
    
    public function create(Request $request)
    {
        
        $start_day = Carbon::now()->subMonth();
        if ($request->input('start_day', NULL )) {
            try{
                $start_day = Carbon::createFromFormat('Y-n-j',$request->input('start_day'))->startOfDay();
            } catch (Exception $e) {
            }
        } 
        
        $end_day = Carbon::now();
        if ($request->input('end_day', NULL )) {
            $end_day = $request->input('end_day');
            try{
                $end_day = Carbon::createFromFormat('Y-n-j',$request->input('end_day'))->endOfDay();
            } catch (Exception $e) {
            }
        } 
        
        $distinct_curses_buy = Orders::where('created_at','>',$start_day)
                    ->where('created_at','<',$end_day)
                    ->where('lecture_id',$request->user()->id)
                    ->distinct()
                    ->get()->unique('course_id');
        
        $statistic_curses = [];
        $statistic_sum    = 0;
        
        foreach ($distinct_curses_buy as $order) {
            $course = new \stdClass();
            $course->name = $order->course->LangName;
            $course->sum = Orders::where('created_at','>',$start_day)
                    ->where('created_at','<',$end_day)
                    ->where('lecture_id',$request->user()->id)
                    ->where('course_id',$order->course_id)
                    ->sum('lecture_sum');
            $statistic_curses[] = $course;
            $statistic_sum      += $course->sum;
        }
        
        return view('Admin.personal_area.block_lecturer_fullfinance_activity',
                ['statistic_curses'=>$statistic_curses,
                    'statistic_sum'=>$statistic_sum,
                    'start_day'=>$start_day->format('d/m/Y'),
                    'end_day'=>$end_day->format('d/m/Y')]);
        
    }
}
