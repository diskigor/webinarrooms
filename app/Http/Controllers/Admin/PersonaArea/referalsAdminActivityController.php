<?php

namespace App\Http\Controllers\Admin\PersonaArea;

use App\Http\Controllers\Controller;



use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;

use App\Models\Billing\RefKeys;
use App\Models\Billing\RefHistory;
use App\Models\Billing\Billing;

class referalsAdminActivityController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('onlyadmin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        
        $start_day = Carbon::now()->subMonth();
        if ($request->input('start_day', NULL )) {
            try{
                $start_day = Carbon::createFromFormat('Y-n-j',$request->input('start_day'))->startOfDay();
            } catch (Exception $e) {
            }
        } 
        
        $end_day = Carbon::now();
        if ($request->input('end_day', NULL )) {
            $end_day = $request->input('end_day');
            try{
                $end_day = Carbon::createFromFormat('Y-n-j',$request->input('end_day'))->endOfDay();
            } catch (Exception $e) {
            }
        } 
        
        $all_new_refs = RefKeys::where('created_at','>',$start_day)
                ->where('created_at','<',$end_day)
                ->whereNotNull('parent_id')
                ->get();
        
        
        $all_sum_kel = RefHistory::where('created_at','>',$start_day)
                ->where('created_at','<',$end_day)
                ->sum('value');
        
        $count_oplata = 0;
        $coun_neoplata= 0;
        
        foreach ($all_new_refs as $rr) {
            if ($rr->user->checkEntrance()) {
               ++$count_oplata; 
            } else {
               ++$coun_neoplata;
            }
        }
            
        $block_referal_activity = new \stdClass();
        $block_referal_activity->all = $all_new_refs->count();
        $block_referal_activity->sum = $all_sum_kel;
        $block_referal_activity->vstup = $count_oplata;
        $block_referal_activity->pret = $coun_neoplata;
            
        return view('Admin.personal_area.block_admin_referal_activity',
                ['block_referal_activity'=>$block_referal_activity,
                    'start_day'=>$start_day->format('d/m/Y'),
                    'end_day'=>$end_day->format('d/m/Y')]);


    }
}
