<?php

namespace App\Http\Controllers\Admin\PersonaArea;

use App\Http\Controllers\Controller;



use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;


use App\Models\Billing\Billing;

class financeAdminActivityController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('onlyadmin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        
        $start_day = Carbon::now()->subMonth();
        if ($request->input('start_day', NULL )) {
            try{
                $start_day = Carbon::createFromFormat('Y-n-j',$request->input('start_day'))->startOfDay();
            } catch (Exception $e) {
            }
        } 
        
        $end_day = Carbon::now();
        if ($request->input('end_day', NULL )) {
            $end_day = $request->input('end_day');
            try{
                $end_day = Carbon::createFromFormat('Y-n-j',$request->input('end_day'))->endOfDay();
            } catch (Exception $e) {
            }
        } 
                
        $all_user_pay = Billing::where('created_at','>',$start_day)
                ->where('created_at','<',$end_day)
                ->AddUser()
                ->sum('transfer_sum');
        
        
        $all_admin_pay  = Billing::where('created_at','>',$start_day)
                ->where('created_at','<',$end_day)
                ->AddAdmin()
                ->sum('transfer_sum');
        
        $all_bay  = Billing::where('created_at','>',$start_day)
                ->where('created_at','<',$end_day)
                ->BayCourse()
                ->sum('transfer_sum');
        

            
        $block_finance_activity = new \stdClass();
        $block_finance_activity->add = $all_user_pay;
        $block_finance_activity->admin = $all_admin_pay;
        $block_finance_activity->sum = $all_admin_pay+$all_user_pay;
        
        $block_finance_activity->bay = abs($all_bay);
            
        
        return view('Admin.personal_area.block_admin_finance_activity',
                ['block_finance_activity'=>$block_finance_activity,
                    'start_day'=>$start_day->format('d/m/Y'),
                    'end_day'=>$end_day->format('d/m/Y')]);


    }
}
