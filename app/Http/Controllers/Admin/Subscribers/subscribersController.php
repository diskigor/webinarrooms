<?php

namespace App\Http\Controllers\Admin\Subscribers;

use App\Models\Subscribers\Subscribers;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\DataTables\SubscibersDataTable;

class subscribersController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('onlyadmin')->only(['index','destroy','getAjaxData']);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Admin.admin.subscribers.listSubscribers',compact('subscribers'));
    }
    
    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAjaxData(SubscibersDataTable $dataTable)
    {
        return $dataTable->render('dataTables.ordersStatistic');
    }

    public function destroy($id) {
        $s = Subscribers::findOrFail($id);
        
        $old = $s->email;
        
        $s->delete();
        return back()->with('success','Вы успешно удалили подписку на Email "'.$old.'".');
    }

//    public function changeStatus(Request $request,$id){
//        $user= User::find($id);
//        if($user){
//            $subscriber         = Subscribers::firstOrNew(['email'=>$user->email]);
//            $subscriber->status = $request->input('status_subscriber',1);
//            $subscriber->name   = $user->name;
//            $subscriber->save();
//            if($subscriber->active){
//                return back()->with('success','Вы подписаны.');
//            }
//            return back()->with('success','Вы успешно отписались.');
//        }
//        return back()->with('warning','Произошла ошибка сообщите администратору сайта.');
//    }
    
    

    /**
     * Создать нового подписчика, нужен параметр '?mail='
     * @param Request $request
     * @return Subscribers
     */
    public function store(Request $request){
        //Проверка на валидность
        $this->validate($request, [                                                                                  
            'email' => 'required|email|max:255',
        ]);
        
        $type = $request->get('type',Subscribers::$FOR_MEDIASCAN);
        if(!key_exists($type, Subscribers::getAllType())) { $type = Subscribers::$FOR_MEDIASCAN; }
        
        
        self::addSubcriberEmailNameType($request->get('email'), $request->get('name',''), $type);
        
        switch ($type) {
            case Subscribers::$FOR_MEDIASCAN:
                return back()->with('success','Вы успешно подписались на Рассылку "Наука Лидерства"');
            case Subscribers::$FOR_BLOGS:
                return back()->with('success','Вы успешно подписались на новые статьи блогов ШЭЛ');
        }

        return back()->with('success','Вы успешно подписались');
    }
    
    /**
     * Метод вывода данных для изменения подписки
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {

        $subscribers = Subscribers::where('user_id', $request->user()->id)->get();
        $collection = collect();
        foreach (Subscribers::getAllType() as $key => $desc) {
            $base = $subscribers->first(function ($sub, $k) use ($key) {
                    return $sub->type == $key;
                });
            
            if(!$base) {
                $basexx = collect();
                $basexx->put('id',$key);
                $basexx->put('status',0);
                $basexx->put('uType',$desc);
            } else {
                $basexx = collect();
                $basexx->put('id',$base->id);
                $basexx->put('status',$base->status);
                $basexx->put('uType',$desc);
                
            } 
            
            $collection->push($basexx);
        }
        
        
        
        if($request->isXmlHttpRequest()) { 
            //dd($collection->first()->get('id'));
            return view('Admin.admin.subscribers._partials.edit',['subscribers'=>$collection]);
        }
        
        return back()->with('success','Вы успешны');
    }
    
    /**
     * Метод обновления данных пользователя подписки
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Проверка на валидность обязательных значений
        $this->validate($request,[                                                                      
            'x_id'=>'required',
            'c_ch'=>'required']);
        
        if ($request->get('c_ch') == 'true') {  $c_ch = 1; } else { $c_ch = 0; }
        
        if (!is_numeric($request->get('x_id'))) { return response()->json(['error'=>'только целые числа']);}
        
        $subscriber = Subscribers::where('user_id', $request->user()->id)->find($request->get('x_id'));
        if($subscriber) { 
            $subscriber->update(['status'=>$c_ch]); 
            return response()->json(['success'=>'Вы изменили статус, '.$subscriber->UType]);
        }

        if(!key_exists($request->get('x_id'), Subscribers::getAllType())) { return response()->json(['success'=>'Это капец!']); }

        self::addSubcriberEmailNameType($request->user()->email, $request->user()->fullName, $request->get('x_id'));
        
        return response()->json(['success'=>'Вы успешно подписались']);
    }

    
//    /**
//     * Проверка почты среди уже подписавшихся
//     * @param type $email
//     * @return Subscribers|NULL
//     */
//    public function check($email='')
//    {
//        return Subscribers::where('email', $email)->first();
//    }

    
//    /**
//     * Записать почту в базу подписчиков
//     * @param type $email
//     * @return Subscribers
//     */
//    public static function addSubcriber($email='sale@divotek.com')
//    {
//        $in_email = \trim(strip_tags($email));
//        // проверили по базе ответили кого нашли
//        $subscriber = Subscribers::where('email', $in_email)->first();
//        if ($subscriber) { return $subscriber;}
//        
//        $add_subscriber = new Subscribers();
//        $add_subscriber->status = 1;
//        $add_subscriber->email = $in_email;
//        $add_subscriber->save();
//        return $add_subscriber;
//    }
    


    
    /**
     * Записать почту, Тип, "Имя первичное" в базу подписчиков
     * @param type $email
     * @param type $name
     * @param type $type
     * @return type
     */
    public static function addSubcriberEmailNameType($email = 'sale@divotek.com', $name = '' , $type = 0)
    {
        //обработка входных данных
        $in_email   = \trim(strip_tags($email));
        $in_name    = \trim(strip_tags($name));
        $in_type    = array_key_exists($type,Subscribers::getAllType())?$type:Subscribers::$FOR_MEDIASCAN;
        $user       = User::where('email',$in_email)->first();
        
        //обновление по пользователю
        if($user) { Subscribers::where('email', $in_email)->update(['user_id'=>$user->id,'name'=>$user->fullName]);} 
        
        // проверили по базе подписок если нашли, отдали
        $subscribers = Subscribers::where('email', $in_email)->where('type',$in_type)->first();
        if ($subscribers) { return $subscribers;}
        
        //новая запись
        $add_subscriber = new Subscribers();
        $add_subscriber->status     = 1;
        $add_subscriber->name       = $user?$user->fullName:$in_name;
        $add_subscriber->email      = $in_email;
        $add_subscriber->type       = $in_type;
        $add_subscriber->user_id    = $user?$user->id:NULL;
        $add_subscriber->save();
        return $add_subscriber;
    }
    
    

    
    /**
     * проверить и если нет, то добавить пользователю подписку по типу
     * @param User $user
     * @param type $type
     * @return Subscribers
     */
    public static function SearchOrNewUserFromTYPE(User $user , $type = 0)
    {
        $in_type = array_key_exists($type,Subscribers::getAllType())?$type:Subscribers::$FOR_SYSTEM_EMAIL;
        
        $sub = Subscribers::GetUserOf($user->id,$in_type)->first();
        if($sub) { return $sub;}

        
        //новая запись
        $add_subscriber = new Subscribers();
        $add_subscriber->status     = 1;
        $add_subscriber->name       = $user->fullName;
        $add_subscriber->email      = $user->email;
        $add_subscriber->type       = $in_type;
        $add_subscriber->user_id    = $user->id;
        $add_subscriber->save();
        return $add_subscriber;
    }
    
}
