<?php

namespace App\Http\Controllers\Admin\Reference;

use App\Models\Settings\ReferencSettings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class referanceSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->isAdmin()){
            $referen = ReferencSettings::first();
            return view('Admin.admin.classroom.reference.referen_settings',compact('referen'));
        }else{
            // Реализовать отправку сообщения администратору о взломе.
            return back()->with('warning',trans('message.hack_message'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth::user()->isAdmin()){
            $ref = ReferencSettings::find($id);
            $this->validate($request,[
                'bonus'=>'required'
            ]);
            $ref->bonus = $request->bonus;
            $ref->save();
            return back()->with('success','Бонус успешно изменен!');
        }else{
            // Реализовать отправку сообщения администратору о взломе.
            return back()->with('warning',trans('message.hack_message'));
        }
    }

}
