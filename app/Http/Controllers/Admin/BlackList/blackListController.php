<?php

namespace App\Http\Controllers\Admin\BlackList;

use App\Models\BlackList\BlackList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class blackListController extends Controller
{
    public function __construct()
    {
        $this->middleware('onlyadmin');
    }
    
    /**
     * Метод вывода списка заблокированых пользователей
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index()
    {
        $black_lists = BlackList::with('user')->get();
        return view('Admin.admin.blackList', compact('black_lists'));
    }

    /**
     * Метод добавления пользователя в черный лист
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $add_black_list = new BlackList();
        $add_black_list->user_id = $request->user_id;
        $add_black_list->admin_id = Auth::user()->id;
        $add_black_list->reason = strip_tags($request->reason);
        $add_black_list->save();
        return back()->with('success', 'Данный пользователь успешно заблокирован');
    }


//    public function blockListMessages()
//    {
//        if (Auth::user()->isAdmin()) {
//            $block_messages = BlockMessageUsers::get();
//
//        }else{
//            // Реализовать отправку сообщения администратору о взломе.
//            return back()->with('warning', trans('message.hack_message'));
//        }
//    }


    /**
     * Метод удаляющий запись с БД для разбллокировки пользователя
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $black_list = BlackList::find($id);
        if ($black_list) {
            $black_list->delete();
            return back()->with('success', 'Пользователь успешно разблокирован');
        }
        return back()->with('warning', 'Ошибка при поиске');
    }

    /**
     * Метод вывода формы на фронте заблокированому пользователю
     * @param $message
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showFormBlock($message)
    {
        return view('front.message.blackFormMessage', compact('message'));
    }

    /**
     * Метод записи сообщения заблокированого пользователя в БД
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
//    public function messageUser(Request $request)
//    {
//        $this->validate($request, [
//            'first_name' => 'required|max:255',
//            'last_name' => 'required|max:255',
//            'email' => 'required',
//            'message' => 'required',
//        ]);
//        $new_block_message = new BlockMessageUsers();
//        $new_block_message->first_name = $request->first_name;
//        $new_block_message->last_name = $request->last_name;
//        $new_block_message->email = $request->email;
//        $new_block_message->message = $request->message;
//        $new_block_message->save();
//        $confirm_send_message = "Ваше сообщение отправлено администратору сайта.Ответ прийдет Вам на почту.";
//        return view('front.message.blackFormMessage', compact('confirm_send_message'));
//    }
}
