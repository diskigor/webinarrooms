<?php

namespace App\Http\Controllers\Admin\Search;

use App\Models\Billing\Billing;
use App\Models\Billing\Orders;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class searchController extends Controller
{
    /**
     * Метод автокомплита для поиска кадетов
     * @param Request $request
     * @return array
     */
    public function search(Request $request, $role_id)
    {
        $term = $request->term;
        $users = User::where('role_id', $role_id)->where('name', 'LIKE', '%' . $term . '%')->get();
        if ($users->count() == 0) {
            $resultSearch[] = 'С таким именем не найдено пользвателя';
        } else {
            foreach ($users as $user) {
                $resultSearch[] = $user->name;
            }
        }
        return response()->json($resultSearch);
    }

    /**
     * Метод автокомплита для поиска среди всех пользователе
     * @param Request $request
     * @return array
     */
    public function users(Request $request)
    {
        $term = $request->term;
        $users = User::where('name', 'LIKE', '%' . $term . '%')->limit(10)->get();
        if ($users->count() == 0) {
            $resultSearch[] = 'С таким именем не найдено пользвателя';
        } else {
            foreach ($users as $user) {
                $resultSearch[$user->id] = $user->name;
            }
        }
        return response()->json($resultSearch);
    }
    
    public function searchEmail(Request $request, $role_id)
    {
        $term = $request->term;
        $users = User::where('role_id', $role_id)->where('email', 'LIKE', '%' . $term . '%')->get();
        if ($users->count() === 0) {
            $resultSearch[] = 'С таким email не найдено пользвателя';

        } else {
            foreach ($users as $user) {
//                $resultSearch[$user->name] = $user->email;
                $resultSearch[] = array('value' => $user->name, 'label' => $user->email);
            }

        }
        return response()->json($resultSearch);
    }

    public function billingEmailSearch(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email'
        ]);
        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return back()->with('warning', 'Пользователь с ' . $request->email . ' не найден');
        }
        $orders = Billing::where('user_id', $user->id)->get();
        if ($orders->count() === 0) {
            return back()->with('warning', 'Данный пользователь не пополнял себе счет');
        } else {
            return view('Admin.statistics.sortBilling', compact('orders', 'user'));
        }
    }

    public function billingInvSearch(Request $request)
    {
        $this->validate($request,[
            'inv'=>'required'
        ]);
        $orders = Billing::where('number_inv',$request->inv)->get();
        if(!$orders){
            return back()->with('warning','Платежа под номером '.$request->inv.' не найдено');
        }else{
            return view('Admin.statistics.sortBilling', compact('orders'));
        }
    }
}
