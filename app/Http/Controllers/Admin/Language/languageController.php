<?php
/**
 * author Michael Terpelyvets
 *
 */

namespace App\Http\Controllers\Admin\Language;

use App\Models\Language\Language;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use File;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class languageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->isAdmin()) {
            $languages = LaravelLocalization::getSupportedLocales();
            $lang_logo = Language::get();
            return view('Admin.language.listLanguage', [
                'languages' => $languages,
                'lang_logo' => $lang_logo]);
        } else {
            // Реализовать отправку сообщения администратору о взломе.
            return back()->with('error', trans('message.hack_message'));
        }
    }

    public function store(Request $request)
    {
        if (Auth::user()->isAdmin()) {
            $this->validate($request, [
                'image' => 'required|image|mimes:jpeg,png,jpg,gif|dimensions:max_width=300,max_height=300',
            ]);
            $language_logo = new Language();
            $language_logo->name = $request->name;
            $image = $request->image->getClientOriginalName();                                                          //Получаем имя файла
            $imageX = explode('.', $image);                                                                    //Разбиваем строку
            $hash = hash('md5', $imageX[0]);                                                                      //Хэшируем имя картинки
            $imageX[0] = $hash . 'language';                                                                            //Записываем в массив
            $imageX = 'images/language/' . implode('.', $imageX);                                                 //Конкатенируем строку
            $upload = public_path('/images/language');                                                            //Записываем путь где будем хранить изображения
            $image = $request->image;
            $image->move($upload, $imageX);                                                                             //Перемещаем файл
            $language_logo->image = $imageX;
            $language_logo->save();
            return redirect()
                ->route('language.index')
                ->with('success',trans('language.message_add_language_logo'));
        } else {
            // Реализовать отправку сообщения администратору о взломе.
            return back()->with('warning', trans('message.hack_message'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($language)
    {
        if (Auth::user()->isAdmin()) {
            $language_logo = Language::where('name', $language)->first();
            if ($language_logo) {
                return view('Admin.language.editLanguage', ['language_logo' => $language_logo]);
            } else {
                return view('Admin.language.editLanguage', ['language' => $language]);
            }
        } else {
            // Реализовать отправку сообщения администратору о взломе.
            return back()->with('warning', trans('message.hack_message'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::user()->isAdmin()) {
            $language = Language::find($id);
            if ($language) {
                if ($request->image) {
                    $this->validate($request, [
                        'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:700',
                    ]);
                    $image = $request->image->getClientOriginalName();                                                  //Получаем имя файла
                    $imageX = explode('.', $image);                                                            //Разбиваем строку
                    $hash = hash('md5', $imageX[0]);                                                              //Хэшируем имя картинки
                    $imageX[0] = $hash . 'language';                                                                    //Записываем в массив
                    $imageX = 'images/language/' . implode('.', $imageX);                                         //Конкатенируем строку
                    $upload = public_path('/images/language');                                                    //Записываем путь где будем хранить изображения
                    $image = $request->image;

                    if (file_exists(public_path($language->image))) {                                                   //Проверяет есть ли старое изображение в папке.
                        File::delete($language->image);                                                                 //Если есть удаляет
                    }
                    $image->move($upload, $imageX);                                                                     //Перемещаем файл
                    $language->image = $imageX;
                }
                $language->save();
                return redirect()
                    ->route('language.index')
                    ->with('success', trans('language.message_edit_language_logo'));
            } else {
                return back()->with('warning', 'Error: not found language.');
            }
        } else {
            // Реализовать отправку сообщения администратору о взломе.
            return back()->with('warning', trans('message.hack_message'));
        }
    }

}
