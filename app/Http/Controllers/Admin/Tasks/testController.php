<?php

namespace App\Http\Controllers\Admin\Tasks;

use App\Models\Curses\Curse;
use App\Models\Tasks\Test;
use App\Models\Tasks\TestAnswer;
use App\Models\Tasks\TestAnswerTrans;
use App\Models\Tasks\TestQuestion;
use App\Models\Tasks\TestQuestionTrans;
use App\Models\Tasks\TestTrans;
use App\Models\Tasks\TypeTest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Illuminate\Support\Facades\App;
use App\Models\Prelections\Prelection;
use App\Models\Prelections\PrelectionTrans;

class testController extends Controller
{
    /**
     *Языки
     * @var array
     */
    protected $arr_local;


    public function __construct()
    {
        $this->middleware('lecturerOrAdmin');
        
        $languages = LaravelLocalization::getSupportedLocales();                                                    //Записываем доступные языки приложения
        foreach ($languages as $key => $value) {                                                                    //Перезаписываем в удобном формате
            $this->arr_local[$key] = $value['name'];
        }
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (Auth::user()->isLecturer()) {
            $courses =Curse::active()->where('user_id',Auth::user()->id)->get();
        } else {
            $courses = Curse::active()->get();
        }


        foreach ($courses as $cours) {
            foreach ($cours->trans_curse as $trans) {
                $course_chearsh[$cours->id] = $trans->name_trans;
            }
        }
        
        $arr_local = array_reverse($this->arr_local);
        
        $type_tests = TypeTest::take(2)->get();
        if ($type_tests->count() === 0) { return back()->with('warning', 'Создание тестов не возможно, таблица с типами пустая!'); }
        
        foreach ($type_tests as $type_test) {
            $type_arr[$type_test->id] = $type_test->name;
        }

        if (Auth::user()->isLecturer()) {
            $tests = Test::where('user_id',  Auth::user()->id)->get();
        } else {
            $tests = Test::all();
        }

        return view('Admin.tasks.tests.listTest',
            compact('arr_local', 'course_chearsh', 'tests', 'type_arr'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //if (!$request->user()->isAdmin() && !$request->user()->isLecturer()) { return back()->with('warning', trans('message.hack_message'));}// Реализовать отправку сообщения администратору о взломе.
        
    }

    /**
     * Матод создания теста
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->user()->isAdmin() && !$request->user()->isLecturer()) { return back()->with('warning', trans('message.hack_message'));}// Реализовать отправку сообщения администратору о взломе.
        $this->validate($request, [
            'prelection_id' => 'required',
            'lang_local' => 'required',
            'type' => 'required',
            'point' => 'required',
            'name' => 'required',
            'desc' => 'required'
        ], [
            'prelection_id.required' => 'Выберите лекцию для данного теста']);

        $new_test = new Test();
        $new_test->name = $request->input('name');
        $new_test->description = $request->input('desc');
        $new_test->point = $request->input('point');
        $new_test->lang_local = $request->input('lang_local','ru');
        $new_test->user_id = Auth::user()->id;
        $new_test->lectures_id = $request->input('prelection_id');
        $new_test->type_id = $request->input('type',1);
        $new_test->save();
        return redirect()->route('test.index')->with('success', 'Тест "'.$new_test->name .'" успешно добавлен');
    }

    /**
     * Метод вывода представления заполнения теста вопросами
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function fillTest($id)
    {
        $test = Test::find($id);
        $languages = LaravelLocalization::getSupportedLocales();                                                    //Записываем доступные языки приложения
        foreach ($languages as $key => $value) {                                                                    //Перезаписываем в удобном формате
            $arr_local[$key] = $value['name'];
        }
        $arr_local = array_reverse($arr_local);
        if ($test) {
            $test_questions = TestQuestion::where('test_id', $test->id)->get();
            return view('Admin.tasks.tests.fillTest', compact('test', 'arr_local', 'test_questions'));
        } else {
            return back()->with('warning', 'Тестовое задание не найдено');
        }
    }

    /**
     * Метод добавления вопроса с вариантами ответами и указателем правильного
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addQuestAnswer(Request $request)
    {
        if (!$request->user()->isAdmin() && !$request->user()->isLecturer()) { return back()->with('warning', trans('message.hack_message'));}// Реализовать отправку сообщения администратору о взломе.
            
        $this->validate($request, [
                'test_id'   => 'required',
                'question'  => 'required',
                'answer'    => 'required|array'
            ],[
                'question.required' => 'Задайте вопрос к тесту',
                'answer.required'   => 'Задайте ответы к вопросу',
                'answer.array'      => 'Ответы в не корректном формате'
            ]);
            
            $test = Test::find($request->test_id);
            if (!$test) { return back()->with('warning', 'Тестовое задание не найдено'); }

            $res = null;
            switch ($test->type_id) {
                case 1:
                case 2:
                    $res = $this->addBooleanQuestAnswer($request);
                    
                    break;
            }

            if ($res instanceof TestQuestion) {
                return back()->with('success', 'Ответ "'.strip_tags($res->question).'" успешно добавлен!'); 
            } elseif($res) {
                return back()->with('warning', $res); 
            }
//dd('---------- add ok ------------',$request,$test);

            $type = $test->typeTest->name;

            if ($type === "concatenation") {
                dd("concatenation",$request);
                $add_question = new TestQuestion();
                $add_question->test_id = $request->test_id;
                $add_question->question = $request->question;
                $add_question->group_number = $request->group;
                $add_question->save();
                $id_question = $add_question->id;
                $namber = $request->namber;
                foreach ($answers as $key => $value) {
                    $answer_question = new TestAnswer();
                    $answer_question->test_question_id = $id_question;
                    $answer_question->number = $namber[$key];
                    $answer_question->answer = $value;
                    $answer_question->is_right = $bal[$key];
                    $answer_question->save();
                }
                return redirect(route('fill_test', $request->test_id))->with('success', 'Вопрос с ответами успешно добавлен');
            }

    }
    
    /**
     * Метод добавления вопроса с ответами к тесту первогои второго  типа (boolean/group)
     * @param Request $request
     */
    protected function addBooleanQuestAnswer(Request $request){
        $add_question = new TestQuestion();
        $add_question->test_id = $request->input('test_id');
        $add_question->group_number = (int)$request->input('group_number',0);
        $add_question->number  = (int)$request->input('number',0);
        $add_question->question = $request->input('question','');
        $add_question->button_type = $request->input('button_type','radio');
        $add_question->save();
        foreach ($request->input('answer') as $value) {
            if(!isset($value['answer'])) continue;
            $answer_question = new TestAnswer();
            $answer_question->test_question_id = $add_question->id;
            $answer_question->number = isset($value['number'])?(int)$value['number']:0;
            $answer_question->answer = isset($value['answer'])?$value['answer']:'';
            $answer_question->is_right = isset($value['is_right'])?(int)$value['is_right']:0;
            $answer_question->save();
        }
        return $add_question;          
    }
    
    
    public function updateQuestAnswer(Request $request){
        $this->validate($request, [
            'test_id'   => 'required',
            'question'  => 'required',
            'answer'    => 'required|array'
        ],[
            'question.required' => 'Задайте вопрос к тесту',
            'answer.required'   => 'Задайте ответы к вопросу',
            'answer.array'      => 'Ответы в не корректном формате'
        ]);
        
        if (!$request->user()->isAdmin() && !$request->user()->isLecturer()) { return back()->with('warning', trans('message.hack_message'));}// Реализовать отправку сообщения администратору о взломе.
        
        $test = Test::find($request->get('test_id'));
        if (!$test) { return back()->with('warning', 'Тестовое задание не найдено'); }
        
        $question = TestQuestion::find($request->get('question_id'));
        if (!$question) { return back()->with('warning', 'Тестовый вопрос не найден'); }
        
        if($test->id != $question->test_id) { return back()->with('warning', 'Тестовый вопрос другого теста'); }
                
        $question->update(['group_number'=>$request->input('group_number',0),'number'=>$request->input('number',0),'question'=>$request->input('question'),'button_type'=>$request->input('button_type','radio')]);
 
        $res = null;
        switch ($test->type_id){
            case 1:
            case 2:
            default :
                $res = $this->updateBooleanAnswer($request, $question);
                break;
           }
        
       if ($res) {
            return back()->with('warning', $res); 
       } else {
            return back()->with('success', 'Вопрос "'.strip_tags($question->question).'" с ответами, успешно обновлён!'); 
       }
    }
           
    /**
     * Метод обновления Вопроса и ответов для первого и второго типов тестов (boolean/group)
     * @param Request $request
     * @param TestQuestion $question
     */
    protected function updateBooleanAnswer(Request $request, TestQuestion $question) {
        $dbanswers = $question->testAnswerQuestion;
        $ra = $request->input('answer');
        foreach ($ra as $k => $v) {
            
            $obj =  $dbanswers->first(function($item) use ($k){
                        return $item->id == $k;
                    });
            
            if (!$obj) {
                $obj = new TestAnswer();
                $obj->test_question_id = $question->id;
            } 
            
            $obj->number = isset($v['number'])?(int)$v['number']:0;
            $obj->answer = isset($v['answer'])?$v['answer']:'';
            $obj->is_right = isset($v['is_right'])?(int)$v['is_right']:0;
            $obj->save();
        }
    }     

    /**
     * Метод удаления вопроса  с вариантами ответа
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteQuestion($id)
    {
        if (!Auth::user()->isAdmin() && !Auth::user()->isLecturer()) { return back()->with('warning', trans('message.hack_message'));}// Реализовать отправку сообщения администратору о взломе.
        $test_question = TestQuestion::find($id);
        if (!$test_question) {
            return back()->with('warning', 'Ошибка при поиске');
        }
        $test_q = strip_tags($test_question->question);
        $test_question->delete();
        return back()->with('success', 'Вопрос "'.$test_q.'" с ответами, успешно удален');
    }
    
    /**
     * Метод вывода представления для редактирования теста
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit($id)
    {
        if (!Auth::user()->isAdmin() && !Auth::user()->isLecturer()) { return back()->with('warning', trans('message.hack_message'));}// Реализовать отправку сообщения администратору о взломе.
   
        $test = Test::with(['testQuestions'=>function($query){
            $query->orderBy('group_number', 'asc')->orderBy('number', 'asc');
            $query->with(['testAnswerQuestion'=>function($q){
                $q->orderBy('number', 'asc');
            }]);
        }])->find($id);
        if (!$test) { return back()->with('warning', 'Тестовое задание не найдено'); }
        
        if (Auth::user()->isLecturer()) {
            $courses =Curse::where('status', 1)->where('user_id',Auth::user()->id)->get();
        } else {
            $courses = Curse::where('status', 1)->get();
        }


        foreach ($courses as $cours) {
            foreach ($cours->lectures as $lec) {
                $lectures[$lec->id] = $cours->lang->name_trans.'//'. $lec->sdoLectureTrans()->name_trans;
            }
        }
        
        
        
        switch ($test->type_id){
            case 1:
                return view('Admin.tasks.tests.edit_test_boolean', compact('test','lectures'));
            case 2:
            default :
                return view('Admin.tasks.tests.edit_test_group', compact('test','lectures'));
           }
    }

    /**
     * Метод обеовления описания теста
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Auth::user()->isAdmin() && !Auth::user()->isLecturer()) { return back()->with('warning', trans('message.hack_message'));}// Реализовать отправку сообщения администратору о взломе.
        
        $test = Test::find($id);
        if (!$test) { return back()->with('warning', 'Тестовое задание не найдено'); }
        
        $sdo_lectures_id = Prelection::find($request->get('prelection_id',0));
        
        $test->update([
            'name'          => $request->input('name',''),
            'description'   => $request->input('description',''),
            'point'         => $request->input('point',0),
            'lectures_id'   => ($sdo_lectures_id?$sdo_lectures_id->id:$test->lectures_id)
            ]);

        return back()->with('success', 'Описание теста "'.strip_tags($test->name).'" успешно обновлёно!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Auth::user()->isAdmin() && !Auth::user()->isLecturer()) { return back()->with('warning', trans('message.hack_message'));}// Реализовать отправку сообщения администратору о взломе.
        
        $test = Test::find($id);
        if (!$test) { return back()->with('warning', 'Тестовое задание не найдено'); }
        
        if (Auth::user()->isLecturer() && $test->user_id != Auth::user()->id) {
            return back()->with('warning', 'Тестовое задание не возможно удалить');
        } 
        $test->delete();
        return redirect()
            ->route('test.index')
            ->with('success', 'Тест успешно удален');
        
    }

}
