<?php

namespace App\Http\Controllers\Admin\Tasks;

use App\Models\Tasks\Explanation;
use App\Models\Tasks\Test;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class explanationController extends Controller
{
    /**
     *Языки
     * @var array
     */
    protected $arr_local;
    
    public function __construct()
    {
        $this->middleware('lecturerOrAdmin');
        
        $languages = LaravelLocalization::getSupportedLocales();                                                    //Записываем доступные языки приложения
        foreach ($languages as $key => $value) {                                                                    //Перезаписываем в удобном формате
            $this->arr_local[$key] = $value['name'];
        }
    }
    
    /**
     * Метод вывода формы наполнения результатов тестов
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index($id)
    {
        $test = Test::find($id);
        if (!$test) {
            return back()->with('warning', 'Ощибка при поиске теста');
        }
        $explanations = Explanation::where('test_id', $test->id)->get();
        return view('Admin.tasks.explanation.explanationList', compact('explanations', 'test'));
    }

    /**
     * Метод добавления ответа и ключа теста
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'key_test' => 'required'
        ]);
        $add_explanation = new Explanation();
        $add_explanation->test_id = $id;
        $add_explanation->key_test = $request->key_test;
        if ($request->alias) {
            $add_explanation->alias = $request->alias;
        }
        if ($request->description) {
            $add_explanation->description = $request->description;
        }
        $add_explanation->save();
        return redirect(route('explanation_list', $id))->with('success', 'Ответ успешно добавлен');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $explanation = Explanation::find($id);
        if (!$explanation) {
            return back()->with('warning', 'Возникла ошибка при поиске результата');
        }
        $explanation->delete();
        return back()->with('success', 'Результат успешно удален');
    }
}
