<?php

namespace App\Http\Controllers\Admin\Tasks;

use App\Models\Curses\Curse;
use App\Models\Prelections\Prelection;
use App\Models\Prelections\PrelectionTrans;
use App\Models\Tasks\TextTasks;
use App\Models\Tasks\TextTasksTrans;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class textAssignmentsController extends Controller
{
    
    /**
     *Языки
     * @var array
     */
    protected $arr_local;
    
    public function __construct()
    {
        $this->middleware('lecturerOrAdmin')->except('getPrelection');
        
        $languages = LaravelLocalization::getSupportedLocales();                                                    //Записываем доступные языки приложения
        foreach ($languages as $key => $value) {                                                                    //Перезаписываем в удобном формате
            $this->arr_local[$key] = $value['name'];
        }
    }
    
    
    /**
     * Метод вывода всех текстовых заданий
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {                                                                          //Получаем все текстовые задания
        if (Auth::user()->isLecturer()) {
            $text_tasks = TextTasks::with('lang')->where('user_id',Auth::user()->id)->get();
        } else {
            $text_tasks = TextTasks::with('lang')->get();
        }

        return view('Admin.tasks.text_assignments.listTextAssignments',                                       //Возвращаем представления с информацией
            compact('text_tasks'));
    }

    /**
     * Метод вывода формы добавления текстового задания
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        $languages = LaravelLocalization::getSupportedLocales();                                                    //Записываем доступные языки приложения
//        foreach ($languages as $key => $value) {                                                                    //Перезаписываем в удобном формате
//            $arr_local[$key] = $value['name'];
//        }
        $arr_local = array_reverse($this->arr_local);                                                                     //Переварачиваем масив
        if (Auth::user()->isLecturer()) {
            $courses =Curse::active()->where('user_id',Auth::user()->id)->get();
        } else {
            $courses = Curse::active()->get();
        }
        
        foreach ($courses as $cours) {
            foreach ($cours->trans_curse as $trans) {
                $course_chearsh[$cours->id] = $trans->name_trans;
            }
        }
        return view('Admin.tasks.text_assignments.createTextAssignments',                                     //Возвращаем представление с нужной информацией
            compact('arr_local', 'course_chearsh'));
    }

    /**
     * Метод сохранения текстового задания в БД
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [                                                                                 //Проверка на пустоту переменной которая хранить униклаьный ключ лекции
            'lectures_id' => 'required',
        ]);
        $languages = LaravelLocalization::getSupportedLocales();                                                    //Получаем все языки приложения
        foreach ($languages as $key => $value) {                                                                    //Перебираем массив с языками
            $this->validate($request, [                                                                             //Созадем валидатор под каждый язык
                'name_' . $key => 'required',
                'text_' . $key => 'required',
            ]);
        }
        $new_text_tasks = new TextTasks();                                                                          //Создаем текстовое задание и записываем в БД
        $new_text_tasks->lecture_id = $request->lectures_id;
        $new_text_tasks->user_id    = $request->user()->id;
        $new_text_tasks->save();
        $id_created_tasks = $new_text_tasks->id;
        foreach ($languages as $key => $value) {                                                                    //Записываем в таблицу текстовое задания на всех языках
            $trans_add_tasks = new TextTasksTrans();
            $trans_add_tasks->sdo_text_task_id = $id_created_tasks;
            $trans_add_tasks->name_trans = $request->{'name_' . $key};
            $trans_add_tasks->content_trans = $request->{'text_' . $key};
            $trans_add_tasks->lang_local = $key;
            $trans_add_tasks->save();
        }
        //Возвращаем на список всех текстовых заданий с сообщением об успешной операции
        return redirect()->route('text_assignments.index')->with('success', 'Текстовое задание успешно добалено');
    }

    public function getPrelection(Request $request)
    {
        $course_id = $request->get('course_id');
        $prelections = Prelection::where('curse_id', $course_id)->get();
        $arr_lectures = [];
        foreach ($prelections as $value) {
            $arr_lectures[$value->id] = $value->sdoLectureTrans()->name_trans;
            //$value->sdoLectureTrans()->sdo_lectures_id   =>  $value->id
        }
        return response()->json($arr_lectures);
    }

    /**
     * Метод вывода формы редактирования текстового задания
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::user()->isAdmin()) {
            $text_tasks = TextTasks::with('lang')->find($id);
            $lections   = Prelection::with('lang')->get();
        } else {
            $text_tasks = TextTasks::with('lang')->where('user_id',Auth::user()->id)->find($id);
            $lections   = Prelection::with('lang')->where('user_id', Auth::user()->id)->get();
        }

        //базовые проверки                                                          
        if(!$text_tasks) { return back()->with('warning', 'Ошибка при поиске текстового задания'); }

//        $lang = App::getLocale();                                                                                   //Получаем язык приложения
//        $prelections = PrelectionTrans::where('lang_local', $lang)->get();                                          //Получаем лекции
//        foreach ($prelections as $value) {
//            $arr_lectures[$value->sdo_lectures_id] = $value->name_trans;
//        }
        
        $arr_lectures = $lections->isEmpty()?[]:$lections->pluck('lang.name_trans','id');

        return view('Admin.tasks.text_assignments.editTextAssignments',[
            'text_tasks'    => $text_tasks,
            'arr_lectures'  => $arr_lectures
            ]);
    }

    /**
     * Метод сохранения изменений в текстовом задании
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::user()->isAdmin()) {
            $edit_text_tasks = TextTasks::with('lang')->find($id);
            $lections   = Prelection::with('lang')->get();
        } else {
            $edit_text_tasks = TextTasks::with('lang')->where('user_id',Auth::user()->id)->find($id);
            $lections   = Prelection::with('lang')->where('user_id', Auth::user()->id)->get();
        }
        
        //базовые проверки                                                          
        if(!$edit_text_tasks) { return back()->with('warning', 'Ошибка при поиске текстового задания'); }
        if(!$request->input('lectures_id')) { return back()->with('warning', 'Обязательно выбрать лекцию !'); }
        if(!$lections->contains('id', $request->input('lectures_id'))) { return back()->with('warning', 'Вы не можете добавить текстовое задание к выбранной лекции!'); }

        $languages = LaravelLocalization::getSupportedLocales();                                                //Получаем все языки приложения
        foreach ($languages as $key => $value) {                                                                //Перебираем массив с языками
            $this->validate($request, [                                                                         //Созадем валидатор под каждый язык
                'name_' . $key => 'required',
                'text_' . $key => 'required',
            ]);
        }
        
        //Обновить запись
        $edit_text_tasks->update(['lecture_id'=>$request->input('lectures_id')]);
                                                                               

        foreach ($languages as $key => $value) {                                                                //Записываем в таблицу текстовое задания на всех языках
            $trans_edit_tasks = TextTasksTrans::where('lang_local', $key)
                ->where('sdo_text_task_id', $edit_text_tasks->id)
                ->first();
            $trans_edit_tasks->sdo_text_task_id = $edit_text_tasks->id;
            $trans_edit_tasks->name_trans = $request->{'name_' . $key};
            $trans_edit_tasks->content_trans = $request->{'text_' . $key};
            $trans_edit_tasks->save();                                                                          //Сохранения текстового задания на всех языках приложения
        }


        return redirect()->route('text_assignments.index')->with('success', 'Текстовое задание успешно изменено');
    }

    /**
     * Метод удаления текстового задания
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::user()->isAdmin()) {
            $te = TextTasks::find($id);
        } else {
            $te = TextTasks::where('user_id',Auth::user()->id)->find($id);
        }

        if (!$te) { return back()->with('warning', 'Текстовое задание не найдено, не возможно удалить'); }

        $te->getTextTasksTransEdit()->delete();
        $te->delete();                                                                            
        return redirect()
            ->route('text_assignments.index')
            ->with('success', 'Текстовое задание успешно удалено');                                                //Переадрисация на список всех текстовых заданий
    }
}
