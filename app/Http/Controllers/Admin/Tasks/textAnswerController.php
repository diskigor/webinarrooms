<?php

namespace App\Http\Controllers\Admin\Tasks;

use App\Models\Tasks\TextAnswerComment;
use App\Models\UserAnswer\TextAnswer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class textAnswerController extends Controller
{
    /**
     * Метод записи комментариев к тестовому ответу пользователя
     * @param Request $request
     * @param $text_answer_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, $text_answer_id)
    {
        if (!Auth::user()->isCadet()) {
            $com_user = Auth::user();
            $this->validate($request, [
                'comment' => 'required|min:100'
            ], [
                'comment.min' => 'Комментарий должен быть не меньше 100 символов'
            ]);
            $check_text_answer = TextAnswer::find($text_answer_id);
            if (!$check_text_answer) {
                return back()->with('warning', 'Ответ не найден сообщите администратору сайта');
            }
            if($check_text_answer->coutn_comment === 10){
                return back()->with('warning','Возможно оставить только 10 комментариев!');
            }
//            if($check_text_answer->coutn_comment===0){
//                $com_user->	level_point++;
//                $com_user->save();
//            }
            $check_text_answer->coutn_comment++;
            $check_text_answer->save();
            $new_comment = new TextAnswerComment();
            $new_comment->text_answer_id = $check_text_answer->id;
            $new_comment->user_id = $com_user->id;
            $new_comment->comment = $request->comment;
            $new_comment->save();
            return back()->with('success','Вы успешно оставили комментарий');
        } else {
            return back()->with('warning', 'Курсантам нет доступа к данному функционалу');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
