<?php

namespace App\Http\Controllers\Admin\Message;

use App\Models\Callback\Callback;
use App\Models\Chats\Chats;

use App\User;
use App\Models\Settings\UserGroup;
use App\Models\Roles\Role;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Jobs\workcreateTemaForUserjob;

class messageController extends Controller
{
    
    
    
    /**
     * Вывод списка все тем/сообщений администратору
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::user() || !Auth::user()->isAdmin()) { return back()->with('warning', trans('message.hack_message'));}// Реализовать отправку сообщения администратору о взломе.

        $messages = Callback::orderBy('status','ASC')
                ->orderBy('updated_at','DESC')
                ->paginate(20);
        return view('Admin.admin.message.list',compact('messages'));
    }
    
    
    /**
     * Форма создания сообщения от администратора пользователям.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ugroups    = UserGroup::get();
        $uroles     = Role::get();
        $uusers     = User::where('role_id','!=','1')->orderBy('name')->get();
        
       return view('Admin.admin.message.create',  compact('uusers','ugroups','uroles'));
    }
    
    /**
     * Метод создания темы для пользователей администратором
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function messageadmin(Request $request) {
        $this->validate($request, [
            'subject' => 'required',
            'message' => 'required',
            'select_userss' => 'required|in:all_users,group_users,roles_users,one_user'
        ], [
            'subject.required' => 'Не указана тема сообщения',
            'message.required' => 'Введите текст сообщения',
            'select_userss.required' => 'Нужно выбрать получателя(ей)',]);
        
        $select_userss  = $request->get('select_userss');
        $tm             = $request->only(['subject','message']);
        
        $message =[ 'status'    =>'warning',
                    'text'      =>'Конкретизируйте получателя'];  

        if ($select_userss=='all_users') { $message = $this->createTemaForAll($tm); }
        
        if ($select_userss=='group_users') {  $message = $this->createTemaForGroup($request->input('group_users'), $tm);}
        
        if ($select_userss=='roles_users') {  $message = $this->createTemaForRole($request->input('roles_users'), $tm);}
        
        if ($select_userss=='one_user') { 
            $one = User::find($request->input('one_user',-1));
            if($one){ $message = $this->createTemaForUser($one, $tm); }
        }

        
        
        if($message['status'] != 'success') { return back()->with($message['status'], $message['text'])->withInput(); }
        
        return redirect()->route('message.index')->with($message['status'], $message['text']);
    }
    
    
    
    /**
     * Метод создания темы и отправки сообщения для всех пользователей
     * @param array $param
     * @return array
     */
    protected function createTemaForAll($param = []) {
        $users = User::where('id','!=',Auth::user()->id)->orderBy('name')->get();
        if ($users->isEmpty()) {
            return [    'status'    =>'warning',
                        'text'      =>'Нет пользователей, только администраторы'];
        }
        
        foreach ($users as $user) {
            $this->createTemaForUser($user, $param);
        }
        
        return [
            'status'    =>'success',
            'text'      =>'Создана тема "'.$param['subject'].'", для всех, '.$users->count().' пользователей'];
    }
    
    /**
     * Метод создания темы и отправки сообщения для группы пользователей
     * @param int $group_users_id
     * @param array $param
     * @return array
     */
    protected function createTemaForGroup($group_users_id, $param = []) {
        $group = UserGroup::find((int)$group_users_id);
        
        if(!$group) {
            return [    'status'    =>'warning',
                        'text'      =>'Конкретизируйте группу пользователей'];
        }

        $users = User::where('group_id',$group->id)->where('id','!=',Auth::user()->id)->get();
        if ($users->isEmpty()) {
            return [    'status'    =>'warning',
                        'text'      =>'В группе "'.$group->name.'" нет пользователей'];
        }
        
        foreach ($users as $user) {
            $this->createTemaForUser($user, $param);
        }
        
        return [
            'status'    =>'success',
            'text'      =>'Создана тема "'.$param['subject'].'", для группы "'.$group->name.'", '.$users->count().' пользователей'];
    }
    
    /**
     * Метод создания темы и отправки сообщения для роли пользователей
     * @param int $role_users_id
     * @param array $param
     * @return array
     */
    protected function createTemaForRole($role_users_id, $param = []) {
        $role = Role::find((int)$role_users_id);
        
        if(!$role) {
            return [    'status'    =>'warning',
                        'text'      =>'Конкретизируйте роль пользователей'];
        }

        $users = User::where('role_id',$role->id)->where('id','!=',Auth::user()->id)->get();
        if ($users->isEmpty()) {
            return [    'status'    =>'warning',
                        'text'      =>'В группе "'.$role->name.'" нет пользователей'];
        }
        
        foreach ($users as $user) {
            $this->createTemaForUser($user, $param);
        }
        
        return [
            'status'    =>'success',
            'text'      =>'Создана тема "'.$param['subject'].'", для группы "'.$role->name.'", '.$users->count().' пользователей'];
    }
    
    
    
    /**
     * Метод создания темы и отправки сообщения для одного пользователя
     * @param User $user
     * @param array $param
     * @return array
     */
    protected function createTemaForUser(User $user, $param = []) {
        //TODO переделать на очереди, если будет необходимо
        
//        $callback = new Callback();
//        $callback->user_id  = $user->id;
//        $callback->subject  = strip_tags($param['subject']);
//        $callback->message  = strip_tags($param['message']);
//        $callback->sadmin   = 1;
//        $callback->save();
//        
//        $add_chat = new Chats();
//        $add_chat->sender_id    = Auth::id();
//        $add_chat->recipient_id = $user->id;
//        $add_chat->status       = 0;
//        $add_chat->sadmin       = 1;
//        $add_chat->message      = strip_tags($param['message']);
//        
//        $callback->chats()->save($add_chat);

        dispatch(new workcreateTemaForUserjob($user,Auth::id(),$param));
        
        return [
            'status'    =>'success',
            'text'      =>'Создана тема "'.$param['subject'].'", с пользователем '.$user->idFullNameEmail];
    }
    
    /**
     * Метод создания ответа администратором пользователю
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::user() || !Auth::user()->isAdmin()) { return back()->with('warning', trans('message.hack_message'));}// Реализовать отправку сообщения администратору о взломе.
        
        
        $this->validate($request, [
            'callback_id' => 'required',
            'text_message' => 'required',
        ], [
            'callback_id.required' => 'Не указан номер темы сообщения',
            'text_message.required' => 'Введите текст ответа на сообщение']);

        $callback = Callback::find($request->input('callback_id',0));
        if(!$callback) { return back()->with('warning', 'Неизвестная тема сообщения'); }
        
        $author = $callback->author;
        $sender = $request->user();
        
        $add_chat = new Chats();
        $add_chat->sender_id    = $sender->id;
        $add_chat->recipient_id = $author->id;
        $add_chat->status       = 0;
        $add_chat->sadmin       = 1;
        $add_chat->status_new   = 'send';
        $add_chat->message      = strip_tags($request->input('text_message',''));
        $callback->chats()->save($add_chat);
        
        if ($callback->isAdmin) {
            $callback->update(['status'=>0,'status_new'=>'send']);
        } else {
            $callback->update(['status'=>0,'status_new'=>'new']);
        }
        
        return back()->with('success', 'Ответ успешно отправлен пользователю');
    }
    
    
    /**
     * Показать переписку с пользователем по теме
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!Auth::user() || !Auth::user()->isAdmin()) { return back()->with('warning', trans('message.hack_message'));}// Реализовать отправку сообщения администратору о взломе.
        
        
        $callback = Callback::with(['author','chats'=>  function ($query) {
            $query->orderBy('created_at','asc');
        }])->find($id);
        if(!$callback) { return back()->with('warning', 'Неизвестная тема сообщения'); }
        
        //проверка что это сообщение от пользователя, определяем отвечающего админа;
        $callback->chats()->where('recipient_id',0)->update(['recipient_id'=>Auth::user()->id]);

        $callback->chats()->where('recipient_id',Auth::user()->id)->update(['status'=>1,'status_new'=>'read']);
        
        $allr = $callback->chats()->where('status_new','!=','read')->get();
        if($allr->isEmpty()) {
            $callback->update(['status'=>1,'status_new'=>'read']);
        }

        return view('Admin.admin.message.show',compact('callback'));
    }
    
    /**
     * Удалить тему переписки
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Auth::user() || !Auth::user()->isAdmin()) { return back()->with('warning', trans('message.hack_message'));}// Реализовать отправку сообщения администратору о взломе.
        
        $callback = Callback::find($id);
        if(!$callback) { return back()->with('warning', 'Неизвестная тема сообщения'); }

        $old = $callback->subject.' #'.$callback->id;
        $callback->delete();
        return back()->with('success','Тема "'.$old.'" удалена.');
    }
    
    
    public function delete_multi_message(Request $request) {
        $messagies_id = $request->input('messagies_id', []);
        
        if (!$request->user() || !$request->user()->isAdmin()) { return response()->json(['error'=>trans('message.hack_message')]); }
        
        if (empty($request->all()) || empty($messagies_id)) {
            return response()->json(['error'=>'Ничего не выбрано']);
        }
        
        foreach ($messagies_id as $id_del){
            $callback = Callback::find($id_del);
            if (!$callback) { continue; }
            $callback->delete();
        }
        
        return response()->json(['success'=>'Удалены ВСЕ выбранные сообщения']);;
        
    }
}
