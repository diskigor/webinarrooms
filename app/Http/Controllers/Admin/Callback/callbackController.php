<?php

namespace App\Http\Controllers\Admin\Callback;

use App\Models\Callback\Callback;
use App\Models\Chats\Chats;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class callbackController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth'); 
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('callback.callback');
    }

    /**
     * Метод сохранения сооющения для администратора
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'subject'=>'required',
            'message'=>'required'
        ]);
        
        $callback = new Callback();
        $callback->user_id = $request->user()->id;
        $callback->subject = strip_tags($request->input('subject',''));
        $callback->message  = strip_tags($request->input('message',''));
        $callback->save();
        
        $add_chat = new Chats();
        $add_chat->sender_id    = $request->user()->id;
        $add_chat->recipient_id = 0;
        $add_chat->status       = 0;
        $add_chat->message      = strip_tags($request->input('message',''));
        
        $callback->chats()->save($add_chat);

        return redirect(route('user_callback_chats.index'))->with(['status'=>'Сообщение отправлено', 'text'=>'*Администратор ответит в течение 24 часов']);
    }
}
