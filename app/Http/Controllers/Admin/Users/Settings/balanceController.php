<?php

namespace App\Http\Controllers\Admin\Users\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\User;
use App\Models\Shop\buyUsersCourses;

use App\Http\Controllers\Interkassa\InterkassaController;

use App\DataTables\UserHistoryTransactionsDataTable;

use App\DataTables\AdminBallsDataTable;

class balanceController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth'); 
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $bay_all_curses = self::getButtonBayAllCourses($request->user());
        
        return view('users.balance.balance',[
            'need_sum'          => $request->input('need_summ', 0),
            'need_text'         => $request->input('need_text', null),
            'bay_all_curses'    => $bay_all_curses
        ]);
    }
    
    /**
     * Метод выдаёт форму, для добавление или Интеркассы
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function getpayform(Request $request)
    {
        $need_text = $request->get('need_text', null);
        $need_summ = $request->get('need_sum', 0);

        $this->verifyAllowed($request);
        
        if (!$request->user()->checkEntrance() && $need_summ<$request->user()->MinPay) {
            $fp = $request->user()->MinPay;
            $need_summ = ($request->user()->balance<$fp)?($fp - $request->user()->balance):null;
            $need_text = 'Для первого взноса нужно внести от : ' . $need_summ . '<i class="fa fa-eur" aria-hidden="true"></i>';
        }
        
        if($request->get('action') == 'get_payment_form') {
            if ($request->get('summa') < $need_summ) { return response()->json (['errors'=>$request->get('summa').' некорректная сумма, минимум '.$need_summ . '<i class="fa fa-eur" aria-hidden="true"></i>']); }
            if ($request->get('summa') < config('vlavlat.minum_pay_add_balance')) { return response()->json (['errors'=>$request->get('summa').' некорректная сумма, минимум '.config('vlavlat.minum_pay_add_balance') . '<i class="fa fa-eur" aria-hidden="true"></i>']); }
            
            $obj_ik = new InterkassaController();
            $data_form = collect([
                'ik_co_id'  => env('INTERKASSA_ID', NULL),
                'ik_pm_no'  => $request->user()->id,
                'ik_am'     => $request->get('summa'),
                'ik_cur'    => env('INTERKASSA_CURRENCY', 'EUR'),
                'ik_desc'   => 'Пополнение баланса - '.$request->user()->name.' - '.$request->user()->email
            ]);
            $forma = $obj_ik->getFormaIK($data_form);
            return response()->json (['forms'=>$forma]);
        }
        
        
        
        return response()->view('users.balance.form_add_balance',  compact('need_summ','need_text'));
    }
    
    public function getHistoryTransaction(UserHistoryTransactionsDataTable $dataTable) {
        return $dataTable->render('dataTables.ordersStatistic');
    }
    
    public function getHistoryBalls(AdminBallsDataTable $dataTable) {
        return $dataTable->render('dataTables.ordersStatistic');
    }
    
    protected function verifyAllowed(Request $request){
        if( $request->user()->checkEntrance()) { return \TRUE;}
        
        //$fp = env('APP_FIRST_PAY', 350);//по умолчанию нужно внести 350 евро
        
        if($request->user()->balance < $request->user()->MinPay) { return \FALSE; }
        
        $request->user()->update(['first_contribution'=>1]);

        return \TRUE;
    }
    
    public static function getButtonBayAllCourses(User $user) {
        $bay_all_curses = new \stdClass();
        
        if ($user->countBuyCourse()) {
            $bay_all_curses->route      = FALSE;
            $bay_all_curses->discont    = 0;
            $bay_all_curses->summ       = buyUsersCourses::getSummAllActiveCurses()*2;
            $bay_all_curses->text       = '';
        } else {    
            $bay_all_curses->route      = route('buyAllCourses');
            $bay_all_curses->discont    = config('vlavlat.discont_for_bay_all_curs');
            $bay_all_curses->summ       = buyUsersCourses::getSummAllActiveCurses($bay_all_curses->discont);
            $bay_all_curses->text       = 'Купить все курсы за '.$bay_all_curses->summ;
            if ($bay_all_curses->discont) {
                $bay_all_curses->text  .= ' , со скидкой '.$bay_all_curses->discont.'%';
            }
        }
        
        return view('users.balance.buttonBayAllCourses',['bay_all_curses'    => $bay_all_curses ]);
    }
}
