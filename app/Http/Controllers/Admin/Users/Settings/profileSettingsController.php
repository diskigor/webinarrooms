<?php

namespace App\Http\Controllers\Admin\Users\Settings;

use App\User;
use function Couchbase\basicDecoderV1;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use File;
use Illuminate\Support\Facades\Storage;

use App\Models\Billing\Balls;

use Hash;

class profileSettingsController extends Controller
{
    protected $full_fieds = [
        'avatar_link','country','name','middle_name','surname','nickname','gender',
        'date_of_birth','country','city','education','profession','m_income'
    ];

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = User::find(Auth::user()->id);
        if ($profile) {
            return view('Admin.profileSettings', [
                'profile' => $profile,
                'full_profile_procent' => $this->getFullProfileProcent($profile)    
                    ]);
        }
        return back()->with('warning', 'Ошибка при поиске вашего профиля сообщите администратору сайта');
    }


    /**
     * Метод изменения автарки
     *
     * @return \Illuminate\Http\Response
     */
    public function changeAvatar(Request $request, $id)
    {
        $user = User::find($id);
        if ($user) {
            $this->validate($request, [
                'avatar' => 'required|image|mimes:jpeg,png,jpg,gif',
            ], [
                'avatar.dimensions' => 'Картинка большого размера. Выберите меньше',
                'avatar.mimes' => 'Формат картинки допускается только jpeg,png,jpg,gif',
            ]);
            
            //удаляем старое если есть
            if(file_exists(public_path($user->avatar_link))){ File::delete($user->avatar_link); }
            Storage::disk('public')->delete($user->avatar_link);
            $user->avatar_link = Storage::disk('public')->putFile('usavatars', $request->file('avatar'));
            
            
            $user->save();
            
            $this->addBallFromFullProfile($user);
            
            return back()->with('success', 'Фото профиля успешно изменено');
        }
        return back()->with('warning', 'Ошибка при поиске вашего профиля сообщите администратору сайта');
    }

    /**
     * Метод изменения имени
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeProf(Request $request, $id)
    {

        $user = User::find($id);
        if ($user) {
            $this->validate($request, [
                'name' => 'required',
            ]);
            $user->name = strip_tags($request->name);
            $user->surname = strip_tags($request->surname) ? $request->surname : NULL;
            $user->middle_name = strip_tags($request->middle_name) ? $request->middle_name : NULL;
            $user->nickname = strip_tags($request->nickname) ? $request->nickname : NULL; //телефон
            $user->gender = $request->gender ;
            $user->save();
            
            $this->addBallFromFullProfile($user);
            
            return back()->with('success', 'Данные успешно изменены');
        }
        return back()->with('warning', 'При поиске профиля возникла проблема');
    }
    
    public function changeUserEmail(Request $request, $id)
    {

        $user = User::find($id);
        if ($user) {
            $this->validate($request, [
                'email' => 'required',
            ]);
            $user->email = strip_tags($request->email);
            $user->save();
            
            $this->addBallFromFullProfile($user);
            
            return back()->with('success', 'Данные успешно изменены');
        }
        return back()->with('warning', 'При поиске профиля возникла проблема');
    }
    
    public function changeUserData(Request $request, $id)
    {

        $user = User::find($id);
        if ($user) {
            $user->country = strip_tags($request->country) ? $request->country : NULL;
            //$user->ind = strip_tags($request->ind) ? $request->ind : NULL;
            $user->city = strip_tags($request->city) ? $request->city : NULL;
            //$user->address = strip_tags($request->address) ? $request->address : NULL;
            $user->education = strip_tags($request->education) ? $request->education : NULL;
            //$user->scholastic_degree = strip_tags($request->scholastic_degree) ? $request->scholastic_degree : NULL;
            //$user->social_status = strip_tags($request->social_status) ? $request->social_status : NULL;
            $user->profession = strip_tags($request->profession) ? $request->profession : NULL;
            $user->m_income = strip_tags($request->m_income) ? $request->m_income : NULL;
            //$user->causes = strip_tags($request->causes) ? $request->causes : NULL;
            $user->date_of_birth = strip_tags($request->date_of_birth) ? $request->date_of_birth : NULL;
            $user->save();
            
            $this->addBallFromFullProfile($user);
            
            return back()->with('success', 'Данные успешно изменены');
        }
        return back()->with('warning', 'При поиске профиля возникла проблема');
    }

    public function deleteAvatar(){
        if(Auth::check()){
            Auth::user()->update([
                'avatar_link'=>null
            ]);
            return back()->with('success','Аватарка успешно удалена!');
        }
        return redirect('/');
    }

    public function getHashKey()
    {

        $user = Auth::user();
        if($user->hash === null){
            $key = str_random(17);
            $user->hash = $key;
            $user->save();
            return back()->with('success','Вы получили уникальный ключ');
        }


    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if ($user) {
            $this->validate($request, [
                'old_pass' => 'required',
                'new_pass' => 'required',
                'confirm_pass' => 'required',
            ]);
            if (Hash::check($request->old_pass, $user->password)) {
                if ($request->new_pass === $request->confirm_pass) {
                    $password = bcrypt($request->new_pass);
                    $user->password = $password;
                    $user->save();
                    
                    $this->addBallFromFullProfile($user);
                    
                    return back()->with('success', 'Пароль успешно изменен');
                }
                return back()->with('warning', 'Пароль подтверждения не совпадает с новым');
            }
            return back()->with('warning', 'Старый пароль введен не верно');
        }
        return back()->with('warning', 'Ошибка при поиске вашего профиля сообщите администратору сайта');
    }
    
    /**
     *  Метод удаления пользователя
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    { 
        if($request->user()->id != $id) { return back()->with('error','Error: нет такого пользователя!'); }     
        
        //return back()->with('warning','Error: Самоубийцо !!!');
        
        //удаляем старое если есть
        if(file_exists(public_path($request->user()->avatar_link))){ File::delete($request->user()->avatar_link); }
        Storage::disk('public')->delete($request->user()->avatar_link);

        $request->user()->delete();                                                                                       //Удаляем пользователя с БД
        return back()->with('success',trans('message.delete_user'));                                        //Выводим сообщение об успешном удалении
    }
    

    
    /**
     * Добавление баллов за заполненный профиль
     * @param User $user
     */
    protected function addBallFromFullProfile(User $user){
        $t = Balls::where('type', Balls::$FOR_FULL_PROFILE)->where('user_id',  $user->id)->first();
        if($t) { return; }//начисляем одни раз
        if($this->getFullProfileProcent($user) < 100) { return; }//только полностью заполненный профиль

        
        $new_ball               = new Balls();
        $new_ball->type         = Balls::$FOR_FULL_PROFILE;
        $new_ball->user_id      = $user->id;
        $new_ball->value        = abs(config('vlavlat.add_ball_from_full_profile'));
        $new_ball->description  = 'Бонус, за заполненный профиль';
        $user->balls()->save($new_ball);


    }
    
    protected function getFullProfileProcent(User $user) {
        $max = count($this->full_fieds)>0?count($this->full_fieds):1;
        $counter = 0;
        foreach ($this->full_fieds as $pole){
            if (trim($user->getAttribute($pole)) != ''){ ++$counter;}
        }
        $procent = 100/$max*$counter;
        return $procent>=100?100:round($procent,2);
    }
    
}
