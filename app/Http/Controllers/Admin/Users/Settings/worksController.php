<?php

namespace App\Http\Controllers\Admin\Users\Settings;

use App\Models\Tasks\TextAnswerComment;
use App\Models\Tasks\TextAnswerCommentLike;
use App\Models\UserAnswer\TextAnswer;
use App\Http\Controllers\Controller;
use App\Models\Billing\Balls;
use App\User;
use Illuminate\Support\Facades\Auth;

class worksController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth'); 
    }
    
    public function index()
    {
        $works = TextAnswer::where('user_id', Auth::user()->id)->orderBy('created_at','desc')->paginate(15);
        return view('users.works.worksList', compact('works'));
    }


    public function show($id)
    {
        $work = TextAnswer::where('user_id', Auth::user()->id)->where('id', $id)->first();
        if (!$work) {
            return back()->with('warning', 'Ответ не найден.Сообщите о данной проблеме');
        }
        return view('users.works.showWork', compact('work'));
    }

    /**
     * добавляет лайк к коментарию
     * @param type $id
     * @return type
     */
    public function edit($id)
    {
        $work_comment = TextAnswerComment::find($id);
        if (!$work_comment) {return back()->with('warning', 'Комментарий не найден.Сообщите о данной проблеме');}
        
        //ограничение: один лайк на комментарии к ответу
        if(TextAnswerCommentLike::where('text_answer_id',$work_comment->text_answer_id)->first()){
            return back()->with('warning', 'Возможен только один избранные комментарий к ответу');
        }
        
        //создаём запис в таблицу лайков, на лучший комментарий
        $new_like = new TextAnswerCommentLike();
        $new_like->text_answer_comment_id = $work_comment->id;
        $new_like->text_answer_id = $work_comment->text_answer_id;
        $new_like->cursant_id = Auth::user()->id;
        $new_like->like = 1;
        $new_like->save();

        //начисление баллов за лучший комментарий
        if(abs(config('vlavlat.balls_for_best_comment_text_answer'))>0) {
            $new_ball               = new Balls();
            $new_ball->type         = Balls::$FOR_BEST_COMMENT_USER_ANSWER;
            $new_ball->user_id      = $work_comment->user_id;
            $new_ball->value        = abs(config('vlavlat.balls_for_best_comment_text_answer'));
            $new_ball->description  = 'За лучший комментарий';
            $work_comment->balls()->save($new_ball);
        }
        
//        $bonus = User::find($work_comment->user_id);
//        $bonus->level_point = 5;//TODO fix
//        $bonus->save();
        
        return redirect(route('my_works.show', $work_comment->text_answer_id))
            ->with('success', 'Вы выбрали самый полезный комментарий');
    }

}
