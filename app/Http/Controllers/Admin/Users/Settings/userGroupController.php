<?php

namespace App\Http\Controllers\Admin\Users\Settings;

use App\Models\Settings\UserGroup;

use App\Http\Controllers\Rules\RulesAutoGroup;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;

class userGroupController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('onlyadmin'); 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = UserGroup::get();
        
        $rules = RulesAutoGroup::getInstance();
        
        return view('Admin.settings.user_group',compact('groups','rules'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'description'=>'required'
        ]);
        $new_group= new UserGroup();
        $new_group->name= $request->name;
        $new_group->description = $request->description;
        $new_group->save();
        return redirect(route('group_user.index'))->with('success','Группа успешно добавлена!');
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = UserGroup::find($id);
        if(!$group){ return back()->with('warning','Группа не найдена!'); }
        
        $rules = RulesAutoGroup::getInstance()->getIdNames();
        
        array_unshift($rules,'');

        return view('Admin.settings.user_group_edit',compact('group','rules'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $group = UserGroup::find($id);
        if(!$group){ return back()->with('warning','Группа не найдена!'); }
        
        $this->validate($request,[
            'name'=>'required',
            'description'=>'required'
        ]);
        
        $group->name        = $request->input('name');
        $group->description = $request->input('description');
        $group->rule_id     = $request->input('rule_id');
        $group->save();
        return redirect(route('group_user.index'))->with('success','Группа "'.$group->name.'" успешно обновлена');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = UserGroup::find($id);
        if(!$group){ return back()->with('warning','Группа не найдена!'); }

        if($group->CountUsers) {
            return back()->with('warning','Нельзя удалить группу, в неё входят '.$group->CountUsers.' пользователей');
        }

        $old = $group->name;
        $group->delete();
        return redirect(route('group_user.index'))->with('success','Группа "'.$old.'" успешно удалена');
    }
}
