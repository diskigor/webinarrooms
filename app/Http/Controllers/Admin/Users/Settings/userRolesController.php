<?php

namespace App\Http\Controllers\Admin\Users\Settings;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\Roles\Role;

class userRolesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('onlyadmin'); 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::get();
        return view('Admin.settings.user_roles',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return back()->with('warning', 'У Вас нет прав к данной настройке');
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        if(!$role){ return back()->with('warning','Роль не найдена!'); }
        
        return view('Admin.settings.user_roles_edit',compact('role'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $role = Role::find($id);
        
        if(!$role){ return back()->with('warning','Роль не найдена!'); }
        
        $this->validate($request,[
            'description'=>'required'
        ]);
        
        $role->description = $request->input('description');
        $role->save();
        return redirect(route('roles_user.index'))->with('success','Описание роли "'.$role->name.'" успешно обновлено');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return back()->with('warning', 'У Вас нет прав к данной настройке');
    }
}
