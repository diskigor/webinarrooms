<?php

namespace App\Http\Controllers\Admin\Users\Chats;

use App\Models\Callback\Callback;
use App\Models\Chats\Chats;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class chatController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth'); 
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $callbacks = Callback::where('user_id',Auth::user()->id)
                ->orderBy('status','ASC')
                ->orderBy('updated_at','DESC')
                ->paginate(20);
        return view('Admin.mail.list_user_callbacks', compact('callbacks'));
        
    }


    public function sendMails()
    {
        if(Auth::check()){
            $send_messages = Callback::where('user_id',Auth::user()->id)->get();
            return view('Admin.mail.sendersMailList',compact('send_messages'));
        }else{
            return redirect('login');
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'callback_id' => 'required',
            'text_message' => 'required',
        ], [
            'callback_id.required' => 'Не указан номер темы сообщения',
            'text_message.required' => 'Введите текст ответа на сообщение']);
        
        $callback = Callback::with(['author','chats'=>  function ($query) {
                $query->orderBy('created_at','asc');
            }])->find($request->input('callback_id',0));
            
        if(!$callback) { return back()->with('warning', 'Неизвестная тема сообщения'); }
        if($callback->user_id != $request->user()->id) { return back()->with('warning', 'Неизвестная тема сообщения, для вас'); }
        
        $add_chat = new Chats();
        $add_chat->sender_id    = $callback->user_id;
        $add_chat->recipient_id = 0;
        $add_chat->status       = 0;
        $add_chat->status_new   = 'send';
        $add_chat->message      = strip_tags($request->input('text_message',''));
        $callback->chats()->save($add_chat);
        if ($callback->isAdmin) {
            $callback->update(['status'=>0,'status_new'=>'new']);
        } else {
            $callback->update(['status'=>0,'status_new'=>'send']);
        }
        

        return back()->with('success','Ваше сообщение успешно отправлено!');

    }

    /**
     * Отобразить переписку по теме поддержки
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $callback = Callback::with(['author','chats'=>  function ($query) {
                $query->orderBy('created_at','asc');
            }])->find($id);
            
        if(!$callback) { return back()->with('warning', 'Неизвестная тема сообщения'); }
        if($callback->user_id != Auth::user()->id) { return back()->with('warning', 'Неизвестная тема сообщения, для вас'); }
        
        $callback->chats()->where('recipient_id',Auth::user()->id)->update(['status'=>1,'status_new'=>'read']);
        
        $allr = $callback->chats()->where('status_new','!=','read')->get();
        if($allr->isEmpty()) {
            $callback->update(['status'=>1,'status_new'=>'read']);
        }

        return view('Admin.mail.show_user_callback',compact('callback'));
    }

}
