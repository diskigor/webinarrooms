<?php

namespace App\Http\Controllers\Admin\Users\Lector;

use App\Models\Billing\Billing;
use App\Models\Billing\Balls;
use App\Models\Roles\Role;
use App\Models\Settings\UserGroup;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use File;
use Illuminate\Support\Facades\Storage;

class lectorController extends Controller
{
    public function __construct()
    {
        $this->middleware('onlyadmin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lectors = User::where('role_id', 2)->get();
        return view('Admin.lecturer.showLector', ['lectors' => $lectors]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lecturer = User::where('role_id', 2)->find($id);
        if(!$lecturer) { return back()->with('error','Error: нет такого лектора!'); }

        $roles = Role::get();
        foreach ($roles as $role) {
            $arr_role[$role->id]=$role->name.' ('.$role->description.')';
        }
        $groups = UserGroup::pluck('name','id');

        return view('Admin.lecturer.editLecturer', [
            'lecturer'  => $lecturer,
            'arr_role'  => $arr_role,
            'groups'    => $groups
        ]);
                
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::where('role_id', 2)->find($id);
        if(!$user) { return back()->with('error','Error: нет такго лектора!'); }

        if($request->hasFile('avatar_link')){ 
             //Проверка на валидность
            $this->validate($request,[ 'avatar_link'=> 'required|image|mimes:jpeg,png,jpg,gif|dimensions:max_width=500,max_height=500',]);

            //удаляем старое если есть
            if(file_exists(public_path($user->avatar_link))){ File::delete($user->avatar_link); }
            Storage::disk('public')->delete($user->avatar_link);
            $user->avatar_link = Storage::disk('public')->putFile('usavatars', $request->file('avatar_link'));
        }
        
        //Проверка на валидность обязательных значений
        $this->validate($request,[                                                                      
            'email'         =>'required|unique:users,email,'.$user->id,
            'name'          =>'required']
                ,[  'email.required' => 'Нет пользователей без почты',
                    'email.unique'   => 'Эта почта "'.$request->email.'" уже используется, введите другую'
        ]);
        
        $user->email    = $request->input('email'); 
        $user->name     = strip_tags($request->input('name'));   
        $user->role_id  = (int)$request->input('role',2);
        $user->group_id = (int)$request->input('group_id',1);
        $user->lecture_percent  = (int)$request->input('lecture_percent', 0);
        $user->save();
        
        //----------Запись в ордер пополнения от админа------------------------------------------------
        $nballance  = $this->addBalanceOrder($request, $user);
        
        //----------Запись пополнения БАЛЛОВ от админа------------------------------------------------
        $nballs     = $this->addBalls($request, $user);

        $errors = '';
        $errors .= is_string($nballance)?$nballance.'  ':'';
        $errors .= is_string($nballs)?$nballs:'';
        if (trim($errors) != '') { return back()->with('warning', $errors); }

        return redirect()->route('lectors.index')->with('success', trans('message.update_user'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {                
        back()->with('warning','Error: Нельзя удалить лектора!');
    }
    
    
    
    /**
     * Запись в ордер пополнения от админа
     * @param Request $request
     * @param User $user
     * @return boolean|Billing|string
     */
    protected function addBalanceOrder(Request $request, User $user) {
        if($request->input('balance')){

            if((Billing::getUserOf($user->id)->sum('transfer_sum') + $request->input('balance')) < 0 ) { return 'Отрицательный балланс не допустим!'; }

            $add_admin_replensh                     = new Billing();
            $add_admin_replensh->user_id            = $user->id;
            $add_admin_replensh->type_replenishment = Billing::TYPE_ADMIN;
            $add_admin_replensh->transfer_sum       = $request->input('balance');
            $add_admin_replensh->currency           = Billing::TYPE_CURRENCY;
            $add_admin_replensh->admin_id           = $request->user()->id;
            if($request->get('text_admin')) {
                $add_admin_replensh->desc   = $request->get('text_admin').', администратор '.$request->user()->name;
            } else {
                $add_admin_replensh->desc   = 'Без комментариев, администратор '.$request->user()->name;
            }
            $add_admin_replensh->save();
            return $add_admin_replensh;
        }
        return FALSE;

    }
    
    /**
     * Начисление баллов от админа
     * @param Request $request
     * @param User $user
     * @return Balls|boolean|string
     */
    protected function addBalls(Request $request, User $user){
        if($request->input('level_point')) {
            
            if((Balls::GetUserOf($user->id)->sum('value') + $request->input('level_point')) < 0 ){  return 'Отрицательные баллы не допустимы!'; }
            
            $new_ball           = new Balls();
            $new_ball->type     = Balls::$FROM_ADMIN;
            $new_ball->user_id  = $user->id;
            $new_ball->value    = $request->input('level_point');
            if($request->get('text_admin')) {
                $new_ball->description    = $request->get('text_admin').', администратор '.$request->user()->name;
            } else {
                $new_ball->description    = 'Без комментариев, администратор '.$request->user()->name;
            }
            $request->user()->balls()->save($new_ball);
            return $new_ball;
        }
        return FALSE;
    }
    
}
