<?php

namespace App\Http\Controllers\Admin\Blog;

use App\Models\Blog\Blog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Illuminate\Support\Facades\Storage;
use File;

use Lecturize\Tags\Models\Tag;
use Lecturize\Tags\Models\Taggable;

class blogController extends Controller
{
    /**
     * @var array
     */
    protected $all_tags;
    
    protected $nameBlog = 'Все блоги';


    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
        //$this->middleware('onlyadmin');
        
        $this->all_tags = Tag::all()->pluck('tag','tag');
        
        
    }
    /**
     * Метод вывода представления со всеми статьями
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $articles = Blog::orderBy('created_at','desc');
        if (!$request->user()->isAdmin() || $request->user()->isGilbo()) {
            $articles->where('author_id',$request->user()->id);
            $this->nameBlog = 'Блог '.$request->user()->FullFIO;
        }

        $blogs = $articles->paginate(10);
        $blogs->nameBlog = $this->nameBlog;
        
        return view('Admin.admin.blog.blogList', [
            'blogs' => $blogs
        ]);
    }

    /**
     * Метод вывода пердставления для создания статьи
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function create()
    {
        $languages = LaravelLocalization::getSupportedLocales();
        foreach ($languages as $key => $value) {
            $arr_local[$key] = $value['name'];
        }
        $arr_local = array_reverse($arr_local);
        if (!request()->user()->isAdmin() || request()->user()->isGilbo()) {
            $this->nameBlog = 'Блог '.request()->user()->FullFIO;
        }
        
        return view('Admin.admin.blog.createArticle', [
            'arr_local' => $arr_local,
            'all_tags'  => $this->all_tags,
            'nameBlog'  => $this->nameBlog
        ]);
    }

    /**
     * Метод созранения статьи в таблицу БД
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image_link'=>'required|image|mimes:jpeg,png,jpg',
            'title'=>'required',
            'slug'=>'required|unique:blog,slug',
            'sd_article'=>'required',
            'lang'=>'required'
        ]);
        
        $new_article = new Blog();
        $new_article->image_link    = Storage::disk('public')->putFile('blog_img', $request->file('image_link'));
        $new_article->title         = $request->title;
        $new_article->article       = $request->sd_article;
        $new_article->lang_local    = $request->lang;
        $new_article->slug          = str_slug($request->slug);
        $new_article->author_id     = $request->user()->id;
        $new_article->save();
        return redirect(route('ablog.index'))->with('success','Статья успешно добавлена!');

    }

    /**
     * Метод вывода представления для редактирования статьи
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit($id)
    {

        $article = Blog::find($id);
        if(!$article){
            return back()->with('warning','Ошибка при поиске статьи!');
        }
        
        if(!request()->user()->isAdmin() && $article->author_id !=  request()->user()->id){
            return back()->with('warning','Вы не можете редактировать!');
        }
             
        $languages = LaravelLocalization::getSupportedLocales();
        foreach ($languages as $key => $value) {
            $arr_local[$key] = $value['name'];
        }
        $arr_local = array_reverse($arr_local);
        
        $all_tags = Tag::all()->pluck('tag','tag');
        return view('Admin.admin.blog.editArticle',[
            'article'   => $article,
            'arr_local' => $arr_local,
            'all_tags'  => $this->all_tags
        ]);
    }

    /**
     * Метод обновления статьи
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $update_article = Blog::find($id);
        if(!$update_article){
            return back()->with('warning','Статья не найдена!');
        }
        
        if(!$request->user()->isAdmin() && $update_article->author_id !=  $request->user()->id){
            return back()->with('warning','Вы не можете редактировать!');
        }
        
        $this->validate($request,[
            'title'=>'required',
            'slug'=>'required|unique:blog,slug,'.$update_article->id,
            'sd_article'=>'required',
            'lang'=>'required'
        ]);

        if($request->hasFile('image_link')){
            $this->validate($request,[
                'image_link'=>'required|image|mimes:jpeg,png,jpg',
            ]);
            Storage::disk('public')->delete($update_article->image_link);
            $update_article->image_link = Storage::disk('public')->putFile('blog_img', $request->file('image_link'));
        }
        
        if($request->has('tags')) {
            $update_article->retag($request->input('tags'));
        }
        
        $update_article->title      = $request->title;
        $update_article->article    = $request->sd_article;
        $update_article->lang_local = $request->lang;
        $update_article->slug       = str_slug($request->slug);
        
        if($request->input('setGilboAuthor', FALSE)) {
            $update_article->author_id = config('vlavlat.personal_IdGilbo');
        }

        $update_article->save();
        return redirect(route('ablog.index'))->with('success','Статья успешно отредактирована!');
    }

    /**
     * Метод удаления статьи
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $sdo_article = Blog::find($id);
        if(!$sdo_article){
            return back()->with('warning','Блог не найден!');
        }

        if(!request()->user()->isAdmin() && $sdo_article->author_id !=  request()->user()->id){
            return back()->with('warning','Вы не можете удалить этот блог!');
        }

        Storage::disk('public')->delete($sdo_article->image_link);
        $sdo_article->delete();
        return back()->with('success','Блог успешно удален!');
    }
}
