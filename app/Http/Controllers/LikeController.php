<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Like;
use App\Models\Blog\Blog;
use App\Models\Comment;

class LikeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function likeBlog($id)
    {
        // here you can check if product exists or is valid or whatever
        $blog = Blog::find($id);
        if (!$blog) { return response()->json(['errors'=>'Нет такого блога']);}
       
        $this->handleLike(Blog::class, $id);
        
        $blog->refresh();
        
        $view = view('users.blog.likes_block', ['blog'=>$blog])->render();
        
        return response()->json(['likes_block'=>$view]);
    }
    
    
    public function likeComment($id)
    {
        // here you can check if product exists or is valid or whatever
        $comment = Comment::find($id);
        if (!$comment) { return response()->json(['errors'=>'Нет такого комментария']);}
       
        $this->handleLike(Comment::class, $id);
        
        $comment->refresh();
        
        $view = view('users.blog.likes_block_comment', ['comment'=>$comment])->render();
        
        return response()->json(['likes_block_comment'=>$view]);
    }
    
    
    protected function handleLike($type, $id)
    {
        $existing_like = Like::withTrashed()->whereLikeableType($type)->whereLikeableId($id)->whereUserId(Auth::id())->first();

        if (is_null($existing_like)) {
            Like::create([
                'user_id'       => Auth::id(),
                'likeable_id'   => $id,
                'likeable_type' => $type,
            ]);
        } else {
            if (is_null($existing_like->deleted_at)) {
                $existing_like->delete();
            } else {
                $existing_like->restore();
            }
        }
    }
}
