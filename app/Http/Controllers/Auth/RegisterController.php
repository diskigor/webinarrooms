<?php

namespace App\Http\Controllers\Auth;

use App\Models\Subscribers\Subscribers;
use App\User;
use File;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

//use App\Models\Billing\RefOrders;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = 'admin/personal_area';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        return Validator::make($data, [
            'name'      => 'required|max:255',
            'email'     => 'required|email|max:255|unique:users',
            'password'  => 'required|min:6|confirmed',
            'rules'     => 'required'
        ],[
            'email.unique'          => 'Пользователь с данной почтой уже существует!',
            'password.confirmed'    => 'Пароли не совпадают',
            'rules.required'        => '*** Обязательный пункт'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        //---------------------------------------Запись пользователя в таблицу ----------------------------------------
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'group_id'=>1,
        ]);

    }

    protected function register(Request $request){
        $input = $request->all();
        $validator = $this->validator($input);
        if($validator->passes()){
            $data = $this->create($input)->toArray();
            $data['confirm_token']= str_random(25);
            $user = User::find($data['id']);
            $user->confirm_token = $data['confirm_token'];
            $user->save();
            Mail::send('Admin.admin.mails.confirmation',$data, function ($message) use ($data){
                $message->to($data['email']);
                $message->subject('Registration Confirmation');
            });
            return redirect('/')->with('status','Для завершения регистрации подтвердите Ваш email. Письмо отправлено на ваш почтовый адрес. Обязательно проверьте папку СПАМ! Письмо может попасть туда');
        }
        return redirect('/register')->with('status','Ошибка в данных при регистрации')->withInput()->withErrors($validator);
    }

    public function confirmation($token){
        $user  = User::where('confirm_token',$token)->first();
        if(!is_null($user)){
            $user->confirmed =1;
            $user->confirm_token='';
            $user->save();
            return redirect('/')->with('status','Ваш профиль активирован');
        }
        return redirect('/')->with('status','Ошибка');
    }

}
