<?php

namespace App\Http\Controllers\Front\Contacts;

use App\Models\FrontModel\Contacts;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Settings\StaticPage;
class contactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = Contacts::first();
        $page = StaticPage::where('slug', 'oferta')->first();

        return view('front.contacts',compact('contacts','page'));
    }

}
