<?php

namespace App\Http\Controllers\Front\Home;

use App\Models\Api\apiMediascan;
use App\Models\Curses\Curse;
use App\Models\Curses\TransCurse;
use App\Models\Doings\Doings;
use App\Models\FrontModel\AboutUs;
use App\Models\FrontModel\Advantages;
use App\Models\News\VideoNews;
use App\Models\Project\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use App\Models\Curses\CurseBlock;
use Illuminate\Support\Facades\Auth;

class homeController extends Controller {

    /**
     * Метод вывода главной страницы
     * @return \Illuminate\Http\Response
     */
    public function index() {
//        $courses = Curse::active()->get();
//
//        $events = Doings::get();

        $projects = Project::forFront()->get();

        $mediascans = apiMediascan::orderBy('id', 'DESC')->take(5)->get();

        $c_blocks = CurseBlock::activeFront()->with('coursesFront.lang', 'lang')->withCount('coursesFront')->orderBy('order_block')->get();

        $vidos = VideoNews::orderBy('id', 'DESC')->take(10)->get();


        $blogs = (new \App\Http\Controllers\Users\Blogs\gilboController())->free_main_page();
        if (Auth::check()) {
            $all_blogs = route('blog.index');
        } else {
            $all_blogs = '/blog';
        }
        if (Auth::check()) {
            $all_nauka = route('admin_all_archive.index');
        } else {
            $all_nauka = route('all_archive.index');
        }

        return view('front.home', [
            'blogs'         => $blogs,
            'projects'      => $projects,
            'mediascans'    => $mediascans,
            'c_blocks'      => $c_blocks,
            'all_blogs'     => $all_blogs,
            'vidos'         => $vidos,
            'all_nauka'     => $all_nauka
        ]);
    }

    /**
     * Метод вывода представления ознакомления с курсом на фронте
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function show($slug) {
        $local = App::getLocale();
        $preview_course_slug = Curse::where('slug', $slug)->first();
        $preview_course = $preview_course_slug->trans_curse[0];
        if (is_null($preview_course)) {
            return back();
        }
        return view('front.course.preview_course', compact('preview_course'));
    }

    /**
     * Метод вывода всех курсов
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showAllCourses() {
        $courses = Curse::where('status', 1)->get();
        return view('front.course.all_front_courses', compact('courses'));
    }

    /**
     * Метод отправляющий на почту техподдержки письмо пользователя
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function supportEmail(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'subject' => 'required',
            'question' => 'required',
        ]);
        $data = $request->toArray();
        Mail::send('Admin.admin.mails.support_email', $data, function ($message) use ($data) {
            $message->to(env('SUPPORT_EMAIL', 'support@lsg.ru'));
            $message->subject('Техподдрежка пользователя!');
        });
        return back()->with('status', 'Ваше обращение будет обработано в течение 24 часов');
    }

    /**
     * Метод отправки вопроса пользователя
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function askMail(Request $request) {

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'ask_user' => 'required'
        ]);
        $data = $request->toArray();
        Mail::send('Admin.admin.mails.ask_mail', $data, function ($message) use ($data) {
            $message->to(env('ASK_MAIL', 'support@lsg.ru'));
            $message->subject('Задать вопрос?');

        });
        return back()->with('status', 'Спасибо.Ваше письмо успешно отправлено!');
    }

//    public function notFound()
//    {
//        return view('errors.404');
//    }
//
//    public function forbiddenPage()
//    {
//        return view('errors.403');
//    }
//
//    public function serviceTemporarily()
//    {
//        return view('errors.503');
//    }
}
