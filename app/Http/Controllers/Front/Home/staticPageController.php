<?php

namespace App\Http\Controllers\Front\Home;

use App\Models\Curses\Curse;
use App\Models\Curses\GroupPriceCurses;
use App\Models\Settings\StaticPage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use File;

class staticPageController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('onlyadmin')->except(['show','showPersonalArea']);
    }
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $static_pages = StaticPage::get();
        return view('Admin.admin.pages.listStaticPages', compact('static_pages'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge(['slug'=>str_slug($request->get('slug'))]);
        
        $this->validate($request, [
            'slug' => 'required|unique:static_pages,slug',
            'name' => 'required',
            'sort' => 'required',
        ],[
            'slug.required' => 'Для сео обязательно',
            'slug.unique'   => 'Этот Slug "'.$request->slug.'" уже используется, нужен новый'
        ]);
        
        $new_page = new StaticPage();
        $new_page->slug = $request->slug;
        $new_page->name = $request->name;
        if($request->banner_text){
            $new_page->banner_text = strip_tags($request->banner_text);
        }
        
        if($request->hasFile('banner_image')){
            $this->validate($request,[ 'image'=>'required|image|mimes:jpeg,png,jpg,gif', ]);
            $new_page->banner_image = Storage::disk('public')->putFile('banner_static', $request->file('banner_image'));
        }

        if (isset($request->type_href)) {
            $new_page->type_href = $request->type_href;
            if (!isset($request->href)) {
                return back()->with('warning', 'Для создания внешней ссылки вставьте ссылку!');
            } else {
                $new_page->href = 0;
            }
        }
        if (isset($request->show_menu)) {
            $new_page->show_menu = $request->show_menu;
        }
        
        $new_page->content = $request->content_page;
        $new_page->sort = $request->sort;
        $new_page->save();
        return redirect(route('static_pages.index'))->with('success', 'Страница успешно добавлена!');
    }


    public function show($slug)
    {
        $page = StaticPage::where('slug', $slug)->firstOrFail();
        
        //это для отображения бесплатных курсов
        if ($slug === 'free') {
            $free_course = GroupPriceCurses::where('price', 0)->where('group_id', 1)->get();
            return view('front.static_page', compact('page', 'free_course'));
        }
        return view('front.static_page', compact('page'));
    }

    public function showPersonalArea($slug){
        $page = StaticPage::where('slug', $slug)->first();
        
        //это для отображения бесплатных курсов
        if ($slug === 'free') {
            $free_course = GroupPriceCurses::where('price', 0)->where('group_id', 1)->get();
            return view('Admin.static_page_admin', compact('page', 'free_course'));
        }
        return view('Admin.static_page_admin', compact('page'));
    }

    public function edit($id)
    {
        $page = StaticPage::find($id);
        if (!$page) { return back()->with('warning', 'Страница не найдена!'); }
        return view('Admin.admin.pages.editStaticPage', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update_page = StaticPage::find($id);
        if (!$update_page) { return back()->with('warning', 'Статическая страница не найдена'); }
        
        $request->merge(['slug'=>str_slug($request->get('slug'))]);
        $this->validate($request, [
            'slug' => 'required|unique:static_pages,slug,'.$update_page->id,
            'name' => 'required',
            'banner_text' => 'required'
        ],[
            'slug.required' => 'Для сео обязательно',
            'slug.unique'   => 'Этот Slug "'.$request->slug.'" уже используется, нужен новый'
        ]);
        
        if($request->hasFile('banner_image')){
            //проверка
            $this->validate($request,[ 'banner_image'=>'image|mimes:jpeg,png,jpg,gif' ]);
            //Удаляем старое изображение
            if (file_exists(public_path($update_page->banner_image))) { File::delete($update_page->image_link); }
            Storage::disk('public')->delete($update_page->banner_image);
            //пишем новое в хранилище
            $update_page->banner_image = Storage::disk('public')->putFile('events', $request->file('banner_image'));
        }

        if($request->banner_text){ $update_page->banner_text = strip_tags($request->banner_text); }
        $update_page->slug = $request->slug;
        $update_page->name = $request->name;
        if (isset($request->sort) and $request->sort != 0) {
            $update_page->sort = $request->sort;
        }
        if (isset($request->show_menu)) {
            $update_page->show_menu = $request->show_menu;
        } else {
            $update_page->show_menu = 0;
        }
        if (isset($request->type_href)) {
            $update_page->type_href = $request->type_href;
            if (!isset($request->href)) {
                return back()->with('warning', 'Для создания внешней ссылки вставьте ссылку!');
            } else {
                $update_page->href = $request->href;
            }
        } else {
            $update_page->type_href = 0;
        }
        $update_page->content = $request->content_page;
        $update_page->save();
        return redirect(route('static_pages.index'))->with('success', 'Страница успешно обновленна!');
    }

    /**
     * Метод удаления статических страниц
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $page = StaticPage::find($id);
        if (!$page) { return back()->with('warning', 'Данная статическая страница не найдена!'); }
        
        //Удаляем старое изображение
        if (file_exists(public_path($page->banner_image))) { File::delete($page->image_link); }
        Storage::disk('public')->delete($page->banner_image);
        
        $old = $page->name;
        $page->delete();
        return back()->with('success', 'Страница успешно удалена');
    }
}
