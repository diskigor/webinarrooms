<?php

namespace App\Http\Controllers\Front\Doings;

use App\Models\Doings\Doings;
use App\Models\Doings\DoingsTrans;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class eventsFrontController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Doings::get();
        return view('front.events.events',compact('events'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $event = Doings::where('slug',$slug)->first();
        if(is_null($event)){
            return back();
        }
        return view('front.events.showEvent',compact('event'));
    }

}
