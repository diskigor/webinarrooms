<?php

namespace App\Http\Controllers\Front\News;

use App\Models\News\News;
use App\Models\News\VideoNews;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class newsFrontController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::get();
        return view('front.news.news',compact('news'));
    }


    /**
     * Метод вывода представления статьи подробней
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show($id)
    {
       $news = News::find($id);
       if(is_null($news)){
        return back();
       }
       return view('front.news.showNews',compact('news'));
    }

    /**
     * Метод вывода представления вывода видео новости
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function videoNews($id){
        $video_news = VideoNews::find($id);
        if(is_null($video_news)){
            return back();
        }
        return view('front.news.showVideoNews',compact('video_news'));
    }

}
