<?php
namespace App\Http\Controllers\Webinar;

use App\Models\Billing\Orders;
use App\Models\News\VideoNews;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

use Charts;
use App\Models\Shop\buyUsersCourses;
use App\Models\Curses\Curse;
use App\Models\Shop\Progress;

use App\Models\Billing\RefKeys;

use Illuminate\Support\Facades\DB;
use App\Webinar;

//use App\Http\Requests;

Class WebinarController extends Controller 
{
	public function index(){
        $webinars = DB::table('users')->join('webinars', 'users.id', '=', 'webinars.id_user')->get();
        $webinars_wallet = DB::table('users')->join('users_webinar', 'users.id', '=', 'users_webinar.user_id')->join('webinars', 'users_webinar.webinar_id', '=', 'webinars.id')->where('users_webinar.user_id', Auth::user()->id)->get();
        $posters = DB::table('webinar_poster')->pluck('image');
        $posterss = DB::table('webinar_poster')->pluck('image');
        
        return view('Admin.webinar.webinar', ['webinars' => $webinars, 'posters' => $posters, 'webinars_wallet' => $webinars_wallet, 'posterss' => $posterss]);
	}

    public function poster_webinars(){
        $url = $_SERVER['SERVER_NAME'];
        $posters = DB::table('webinar_poster')->get();
        $webinars = DB::table('webinars')->where('arhiv', 0)->count();

        return view('Admin.webinar.poster_webinar', ['posters' => $posters, 'url' => $url, 'webinars' => $webinars]);
    }

	public function create_webinar(){

		if(Auth::user()->isAdmin()){
            $orders_count = Orders::count();
            $ref_orders = RefKeys::count();
            return view('Admin.webinar.create_webinar',compact('orders_count','ref_orders'));
        }

        $users = DB::table('users')->get();
        
        return view('Admin.webinar.create_webinar')->with(['users' => $users]);
	}

    public function add_webinar(Request $request){
        $this->validate($request, [
            'name_webinar' => 'required|max:255',
            'description' => 'required',
            'date_webinar' => 'required|max:255',
            'time_start' => 'required|max:255',
            'time_stop' => 'required|max:255',
            'timezone' => 'required|max:255'
        ]);

        $data = $request->all();

        $webinar = new Webinar;
        $webinar->fill($data);

        $webinar->save();

        return redirect('/admin/my_webinars');
    }

    public function posters (){
        if(Auth::user()->isAdmin()){
            $orders_count = Orders::count();
            $ref_orders = RefKeys::count();
            return view('Admin.webinar.update_poster');
        }
    }

    public function update_poster(Request $request){
        DB::table('webinar_poster')->where('id', 1)->update(['description' => $request->get('desc')]);
        return redirect('/admin/update_poster');
    }

    public function update_poster_img(Request $request){
        $file_name = time().'.'.$request->file->getClientOriginalExtension(); 
        $request->file->move(public_path('file'), $file_name);

        DB::table('webinar_poster')->where('id', 1)->update(['image' => '/file/'.$file_name]);
        return redirect('/admin/update_poster');
    }

    public function show($id) {
        $webinar = DB::table('users')->join('webinars', 'users.id', '=', 'webinars.id_user')->where('webinars.id', '=', $id)->get();
        $wallets = DB::table('users_webinar')->whereWebinar_idAndUser_id($id, Auth::user()->id)->get();
        $count = DB::table('users_webinar')->where('webinar_id', $id)->count();
        $lektor_webinar = DB::table('webinars')->get();
        $posters = DB::table('webinar_poster')->pluck('image');

        return view('Admin.webinar.webinar-content')->with(['webinar' => $webinar, 'wallets' => $wallets, 'count' => $count, 'lektor_webinar' => $lektor_webinar, 'posters' => $posters]);
    }

    public function my_webinars() {
        $webinars = DB::table('users')->join('webinars', 'users.id', '=', 'webinars.id_user')->get();
        $posters = DB::table('webinar_poster')->pluck('image');
        $count = DB::table('users_webinar')->get();
        $moders = DB::table('webinar_moders')->where('user_email', Auth::user()->email)->get();
        $id = Auth::user()->id;
        
        return view('Admin.webinar.my_webinars', ['webinars' => $webinars, 'posters' => $posters, 'id' => $id, 'count' => $count, 'moders' => $moders]);
    }

    public function my_show($id) {
        $webinar = DB::table('users')->join('webinars', 'users.id', '=', 'webinars.id_user')->where('webinars.id', '=', $id)->get();
        $pay = DB::table('users_webinar')->where('webinar_id', $id)->get();

        return view('Admin.webinar.webinar-content-lektor')->with(['webinar' => $webinar, 'pay'=> $pay]);
    }

    public function showTranslete($id) {
        $webinars = DB::table('users')->join('webinars', 'users.id', '=', 'webinars.id_user')->where('webinars.id', '=', $id)->get();
        $id = Auth::user()->id;

        return view('Admin.webinar.my_webinars_translate', ['webinars' => $webinars, 'id' => $id]);
    }

    public function admin_webinars(){
        $webinars = DB::table('users')->join('webinars', 'users.id', '=', 'webinars.id_user')->get();
        $posters = DB::table('webinar_poster')->pluck('image');
        
        return view('Admin.webinar.admin_webinars', ['webinars' => $webinars, 'posters' => $posters]);   
    }
    //Редактор админа
    public function admin_webinars_edit ($id){
        $webinar = DB::table('webinars')->where('id', '=', $id)->first();
        $users = DB::table('users')->get();

        return view('Admin.webinar.admin_webinars_edit')->with(['webinar' => $webinar, 'users' => $users]);
    }

    public function save_edit_admin (Request $request){
        $id = $request->get('id');
        DB::table('webinars')->where('id', $id)->update(['name_webinar' => $request->get('name'), 'description' => $request->get('description'), 'date_webinar' => $request->get('date_webinar'), 'time_start' => $request->get('time_start'), 'time_stop' => $request->get('time_stop'), 'timezone' => $request->get('timezone'), 'chat' => $request->get('chat'), 'question' => $request->get('question'), 'question_sound' => $request->get('question_sound'), 'type_message' => $request->get('type_message'), 'list_people' => $request->get('list_people'), 'sum_people' => $request->get('sum_people')]);
        return redirect('/admin/admin_webinars/');
    }
    //Редактор ректора
    public function user_webinars_edit ($id){
        $webinar = DB::table('webinars')->where('id', '=', $id)->first();
        $users = DB::table('users')->get();
        $bye = DB::table('users_webinar')->where('webinar_id', '=', $id)->get();

        return view('Admin.webinar.user_webinars_edit')->with(['webinar' => $webinar, 'users' => $users, 'bye' => $bye]);
    }

    public function save_edit_user (Request $request){
        $id = $request->get('id');

        if($request->get('btn_one') != false){
            DB::table('webinars')->where('id', $id)->update(['name_webinar' => $request->get('name'), 'description' => $request->get('description'), 'date_webinar' => $request->get('date_webinar'), 'time_start' => $request->get('time_start'), 'time_stop' => $request->get('time_stop'), 'timezone' => $request->get('timezone'), 'chat' => $request->get('chat'), 'question' => $request->get('question'), 'question_sound' => $request->get('question_sound'), 'type_message' => $request->get('type_message'), 'list_people' => $request->get('list_people'), 'sum_people' => $request->get('sum_people')]);
        }

        if($request->get('btn_list') != false){
            $arr = $request->get('list_users_add');
            $ars = explode(',', $arr);
            foreach ($ars as $value) {
                if ($value != null) {
                    DB::table('webinar_pristine')->insert(['webinar_id' => $id, 'user_email' => $value]);
                }
            }

            $arrs = $request->get('list_users_add_lektor');
            $arss = explode(',', $arrs);
            foreach ($arss as $values) {
                if ($values != null) {
                    DB::table('webinar_moders')->insert(['webinar_id' => $id, 'user_email' => $values]);

                    $user_add = DB::table('users')->where('email', $values)->first();
                    DB::table('users_webinar')->insert(['webinar_id' => $id, 'user_id' => $user_add->id]);
                }
            }

            if ($request->get('text_message') != null) {
                DB::table('webinar_message')->insert(['webinar_id' => $id, 'time_1' => $request->get('message_1'), 'time_2' => $request->get('message_2'), 'message' => $request->get('text_message')]);
            }
        }

        if($request->get('update_img') != false){
            $file_name = time().'.'.$request->file->getClientOriginalExtension(); 
            $request->file->move(public_path('file'), $file_name);

            DB::table('webinars')->where('id', $id)->update(['image' => '/file/'.$file_name]);
        }
        return redirect('/admin/my_webinars/');
    }

    public function add_user_webinar (Request $request){
        DB::table('users_webinar')->insert(['webinar_id' => $request->get('webinar_id'), 'user_id' => $request->get('user_id')]);
        $balance = Auth::user()->balance - $request->get('price');
        DB::table('users')->where('id', $request->get('user_id'))->update(['balance' => $balance]);

        return redirect('/admin/webinar/' . $request->get('webinar_id'));
    }

    public function add_webinar_arhiv (Request $request){
        if($request->get('end') != false){
            DB::table('webinars')->where('id', $request->get('webinar_id'))->update(['arhiv' => '1']);
            return redirect('/admin/arhiv/');
        }else{
            return redirect('/admin/my_webinar/' . $request->get('webinar_id'));
        }
    }

    public function arhiv(){
        $webinars = DB::table('users')->join('webinars', 'users.id', '=', 'webinars.id_user')->get();
        $posters = DB::table('webinar_poster')->pluck('image');
        $count = DB::table('users_webinar')->where('user_id', Auth::user()->id)->get();
        $id = Auth::user()->id;
        
        return view('Admin.webinar.arhiv', ['webinars' => $webinars, 'posters' => $posters, 'id' => $id, 'count' => $count]);
    }
}

