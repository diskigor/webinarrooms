<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

Class Webinar extends Model
{

	protected $fillable = ['name_webinar', 'description', 'date_webinar', 'time_start', 'time_stop', 'timezone', 'chat', 'question', 'question_sound', 'type_message', 'list_people', 'sum_people', 'id_user', 'count_user', 'updated_at', 'created_at'];

}
