<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

//models for observers
use App\User;
use App\Observers\UserObserver;

//следим за изменениями пользователей : приходы, рефералы, админы накинули
use App\Models\Billing\Billing;
use App\Observers\BillingObserver;

//следим за изменениями пользователей : покупки
use App\Models\Billing\Orders;
use App\Observers\OrderObserver;

//следим за начисление баллов 
use App\Models\Billing\Balls;
use App\Observers\BallsObserver;

//следим за созданием событий/мероприятий
use App\Models\Doings\DoingsTrans;
use App\Observers\EventDoingObserver;

//следим за созданием блогов
use App\Models\Blog\Blog;
use App\Observers\BlogObserver;

class AppModelObserverProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //Observers models
        User::observe(UserObserver::class);
        
        //следим за изменениями пользователей : приходы, рефералы, админы накинули
        Billing::observe(BillingObserver::class);
        
        //следим за изменениями пользователей : покупки
        Orders::observe(OrderObserver::class);
        
        //следим за изменениями баллов 
        Balls::observe(BallsObserver::class);
        
        //следим за созданием событий/мероприятий
        DoingsTrans::observe(EventDoingObserver::class);
        
        //следим за созданием блогов
        Blog::observe(BlogObserver::class);
        
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
