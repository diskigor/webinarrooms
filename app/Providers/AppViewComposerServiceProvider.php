<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Illuminate\Support\Facades\Auth;

use App\Models\Blog\Blog;
use App\Models\Curses\Curse;
use App\Models\Language\Language;

//TODO
use App\Models\Doings\Doings;
use App\Models\FrontModel\Contacts;
use App\Models\News\VideoNews;
use App\Models\Settings\StaticPage;
use App\Models\Api\apiMediascan;
use App\Models\Project\Project;

//
use App\Models\Callback\Callback;
use App\Models\Chats\Chats;



class AppViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        
        
//        //левый садбар для пользователей
//        view()->composer('layouts.leftSiteBarUsers', function ($view) {
//            $view->with([
//                'blogs_counts'  => collect([
//                    'all'       => Blog::all()->count(),
//                    'new'       => Blog::CountNew()]),
//                
//                'courses_counts'    => collect([
//                    'all'           => Curse::active()->count(),
//                    'new_all'       => Curse::CountNew(),
//                    'new_gilbo'     => Curse::CountNew('gilbo'),
//                    'new_lecturer'  => Curse::CountNew('lecturer'),
//                        ]),
//                
//                    ]);
//        });
        
        
        //правый садбар для пользователей
        view()->composer('layouts.rightSideBarUsers', function ($view) {
            $view->with([
                'right_dates'   => collect([
                    'events'        => Doings::orderBy('id', 'DESC')->take(2)->get(),
                    'mediascans'    => apiMediascan::orderBy('id', 'DESC')->take(2)->get(),
                    'blogs'         => Blog::orderBy('id', 'DESC')->take(2)->get(),
                    'projects'      => Project::forSide()->get()
                        ]),
                    ]);
        });
        
        
        
        
        
        //TODO проверить использование
        view()->composer('*', function ($view) {
            //$new_message_from_admin = Auth::user()?Callback::where('user_id',Auth::user()->id)->NewMessage()->count():NULL;
            
            $new_message_from_admin =  NULL;

            if(Auth::check() && !Auth::user()->isAdmin()) {
                $all_mess_2 = Callback::where('user_id',Auth::user()->id)->get();
                foreach ($all_mess_2 as $message_2) {
                    if($message_2->UserStatusMessage == 'new') {++$new_message_from_admin;}
                } 
            }
            

            $new_message_from_users = NULL;
            
            if(Auth::check() && Auth::user()->isAdmin()) {
                $all_mess = Callback::NewMessage()->orWhere('status_new', 'send')->get();
                foreach ($all_mess as $message) {
                    if($message->AdminStatusMessage == 'new') {++$new_message_from_users;}
                } 
            }
            
            $view->with([
                'lg_logo'    => Language::get(),
                
                'minum_kel_balance_alert'=>  config('vlavlat.minum_kel_balance_alert'),
                
                'blogcounts' => collect([
                    'all'      => Blog::all()->count(),
                    'new'      => Blog::CountNew()]),
                
                'blogs_counts'  => collect([
                    'all'       => Blog::all()->count(),
                    'new'       => Blog::CountNew()]),
                
                'courses_counts'    => collect([
                    'all'           => Curse::active()->count(),
                    'new_all'       => Curse::CountNew(),
                    'new_gilbo'     => Curse::CountNew('gilbo'),
                    'new_lecturer'  => Curse::CountNew('lecturer'),
                        ]),
                
                'new_message_from_admin' => $new_message_from_admin>0?$new_message_from_admin:NULL,
                'new_message_from_users' => $new_message_from_users>0?$new_message_from_users:NULL,
                    ]);

            
        });
        
        
        
        
        
        
        
        // Вывод в левый сайд-бар курсов
        view()->composer('layouts.partials.left_sidebar', function ($view) {
            $video_news = VideoNews::orderBy('created_at', 'DESC')->first();
            $events = Doings::take(3)->get();
            $view->with(['video_news' => $video_news, 'events' => $events]);
        });
//        view()->composer('layouts.partials.header', function ($view) {
//            $courses = Curse::where('status', 1)->get();
//            $pages = StaticPage::orderBy('sort', 'ASC')->where('show_menu', 1)
//                ->get();
//            $view->with(['courses' => $courses, 'pages' => $pages]);
//        });
//        view()->composer('layouts.nav', function ($view) {
//            $courses = Curse::where('status', 1)->get();
//            $pages = StaticPage::orderBy('sort', 'ASC')->where('show_menu', 1)
//                ->get();
//            $view->with(['courses' => $courses, 'pages' => $pages]);
//        });
//        view()->composer('Admin.admin.leftSiteBarAdmin', function ($view) {
//            $pages = StaticPage::orderBy('sort', 'ASC')->where('show_menu', 1)
//                ->get();
//            $view->with(['pages' => $pages]);
//        });
//        view()->composer('Admin.cadet.leftSiteBarCadet', function ($view) {
//            $pages = StaticPage::orderBy('sort', 'ASC')->where('show_menu', 1)
//                ->get();
//            $view->with(['pages' => $pages]);
//        });
//        view()->composer('Admin.curator.leftSiteBarCurator', function ($view) {
//            $pages = StaticPage::orderBy('sort', 'ASC')->where('show_menu', 1)
//                ->get();
//            $view->with(['pages' => $pages]);
//        });
//        view()->composer('Admin.lecturer.leftSiteBarLecturer',function ($view){
//            $pages = StaticPage::orderBy('sort','ASC')->where('show_menu',1)
//                ->get();
//            $view->with(['pages'=>$pages]);
//        });
//        view()->composer('layouts.partials.footer', function ($view) {
//            $contacts = Contacts::first();
//            $pages = StaticPage::orderBy('sort', 'ASC')
//                ->where('show_menu', 1)
//                ->get();
//            $view->with(['contacts' => $contacts, 'pages' => $pages]);
//        });
        
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
