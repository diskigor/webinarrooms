<?php

namespace App;
//use App\Models\Billing\RefOrders;
use App\Models\BlackList\BlackList;
use App\Models\Callback\Callback;
use App\Models\Chats\Chats;
use App\Models\Favorites\Favorites;
use App\Models\Roles\Role;
use App\Models\Settings\UserGroup;
use App\Models\Shop\buyUsersCourses;
use App\Models\Shop\Progress;
use App\Models\Subscribers\Subscribers;
use App\Models\UserAnswer\TextAnswer;
use App\Models\Billing\Balls;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Collection;

use Illuminate\Support\Facades\Storage;

//РЕФЕРАЛКА
use App\Models\Billing\RefKeys;
use App\Models\Billing\RefHistory;

//use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    
    /**
     *
     * @var \Illuminate\Support\Collection
     */
    protected $main_user_for_ref;

    /**
     *
     * @var \Illuminate\Support\Collection
     */
    protected $main_user_parents;
    
    /**
     * Отношения hasOne к модели Роли пользователя
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function role()
    {
        return $this->hasOne(Role::class, 'id', 'role_id');
    }

    public function group()
    {
        return $this->hasOne(UserGroup::class, 'id', 'group_id');
    }

    /**
     * Метод проверки на роль админа
     * @return bool
     */
    public function isAdmin()
    {
        return $this->role->name === 'Admin';
    }

    /**
     * Метод проверки на Гильбо
     * @return bool
     */
    public function isGilbo()
    {
        return $this->isAdmin() && $this->id === config('vlavlat.personal_IdGilbo');
    }
    
    /**
     * Метод проверки на роль лектора
     * @return bool
     */
    public function isLecturer()
    {
        return $this->role->name === 'Lecturer';
    }

    /**
     * Метод проверки на роль куратора
     * @return bool
     */
    public function isСurator()
    {
        return $this->role->name === 'Сurator';
    }

    /**
     * Метод проверки на роль кадета
     * @return bool
     */
    public function isCadet()
    {
        return $this->role->name === 'Cadet';
    }

    /**
     * TODO
     * Подписка на сайт связь
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function subscriber()
    {
        return $this->hasOne(Subscribers::class, 'email', 'email');
    }

    public function getMessage()
    {
        
        if (Auth::user()->isAdmin()) {
            $message = Callback::where('status', 0)->get();
        } else {
            $message = Chats::where('recipient_id', Auth::user()->id)->where('status', 0)->get();
        }
        
        return $message;
    }

    public function progressCurse($course_id)
    {
        return $this->hasOne(Progress::class, 'user_id', 'id')->where('curses_id', $course_id)->first();
    }

    public function blackUser()
    {
        return $this->hasOne(BlackList::class, 'user_id', 'id');
    }

    /**
     * Проверка возможности покупать курсы пользователем
     * @return bool
     */
    public function checkEntrance()
    {
        return $this->first_contribution?  \TRUE :  \FALSE;
//        $entrance_pay = buyUsersCourses::whereIn('curses_id', [6, 10, 15])->where('user_id', $this->id)->get();
//        if ($entrance_pay->count() === 3) {
//            return true;
//        } else {
//            return false;
//        }
    }

    /**
     * Метод проверяет купил текущий пользователь курс или нет.
     * @param int $course_id
     * @return boolean
     */
    public function checkBuyCourse($course_id)
    {
        $check_buy_course = buyUsersCourses::where('curses_id', $course_id)->where('user_id', $this->id)->first();
        if (!$check_buy_course) {
            return false;
        }
        return true;
    }

    public function favoriteUser($fav_id)
    {
        $favorite = Favorites::where('user_id', $this->id)->where('favor_user_id', $fav_id)->first();
        if ($favorite) {
            return true;
        } else {
            return false;
        }
    }

    public function countBuyCourse(){
        return $this->hasMany(buyUsersCourses::class)->where('user_id', $this->id)->count();
    }

    public function countMyWork(){
        return $this->hasMany(TextAnswer::class)->where('user_id', $this->id)->count();
    }

    public function coursesByu(){
        return $this->hasMany(buyUsersCourses::class,'user_id','id');
    }
//    public function sendPasswordResetNotification($token)
//    {
//        $this->notify(new ResetPasswordNotification($token));
//    }
    
    public function getAvatarAttribute() {
        if (!$this->avatar_link || trim($this->avatar_link) == '') { return asset('images/default-avatar.png'); }
        if(file_exists(public_path($this->avatar_link))){  return asset($this->avatar_link);}
        if(Storage::disk('public')->exists($this->avatar_link)) { return Storage::url($this->avatar_link);}
        return asset('images/default-avatar.png');
    }
    
    public function getFullNameAttribute() {
        if ($this->name) {$rrr =$this->name;}
        if ($this->surname) {$rrr .=', '.$this->surname;}
        if ($this->middle_name) {$rrr .=', '.$this->middle_name;}
        return $rrr;
    }
    
    public function getFullFIOAttribute() {
        if ($this->name) {$rrr =$this->name;}
        if ($this->surname) {$rrr .=' '.mb_strtoupper(mb_substr($this->surname,0,1)).'.';}
        if ($this->middle_name) {$rrr .=' '.mb_strtoupper(mb_substr($this->middle_name,0,1)).'.';}
        return $rrr;
    }
    
    public function getIdFullNameEmailAttribute() {
        $rrr = $this->id;
        if ($this->getFullNameAttribute()) {$rrr .=', '.$this->getFullNameAttribute();}
        //if ($this->surname) {$rrr .=', '.$this->surname;}
        //if ($this->middle_name) {$rrr .=', '.$this->middle_name;}
        if ($this->email) {$rrr .=', '.$this->email;}
        return $rrr;
    }
    
//    /**
//     * Связь с таблицой рефералов пользователя
//     * @return hasMany
//     */
//    public function referals() {
//        return $this->hasMany(RefOrders::class,'user_give_id', $this->id);
//    }
//    
//    /**
//     * Получить колическтов рефералов
//     * @return int
//     */
//    public function getReferalsCount(){
//        return $this->referals()->count();
//    }
    
    //РЕФЕРАЛКА
    
    /**
     * Получить запись с реферальным ключём.
     */
    public function refkey()
    {
      return $this->hasOne(RefKeys::class,'user_id','id');
    }
    
    public function getReferalLink($route = 'main_front_page') {
        if (!$this->relationLoaded('refkey')) { $this->load('refkey'); }
        
        if (!$this->refkey){ return 'Для генерации ключа, сохраните профиль'; } 
        
        return route($route,[config('vlavlat.referalkey')=>$this->refkey->hashkey]);
    }
    
    /**
     * 
     * @param int $level
     */
    public function getMyReferalsLevel($level = 1, $users = NULL, $iter = NULL) {
        //инициализация
        $iter   = $iter?$iter:1;                //итерация
        $users  = $users?$users:collect($this->query()->where('id', $this->id)->get()); //коллекция

        //проверка
        $level  = ($level>config('vlavlat.ref_max_level') || $level<1)?1:$level;
        if(!$this->main_user_for_ref) { $this->main_user_for_ref = collect(); }
        
        if ($iter>$level) { return NULL; }
        
        $referals = collect();
        foreach ($users as $us) {
            $ref_us = $this->getMyReferals($us->id);
            if ($ref_us->isEmpty()) { continue; }
            $referals->push($ref_us); 
        }
        $flattee_referals = $referals->flatten();
        
        if ($flattee_referals->isEmpty()) { return $this->main_user_for_ref; }
        
        $this->main_user_for_ref->put($iter, $flattee_referals);
        
        if ($iter<$level) {
            return $this->getMyReferalsLevel($level, $flattee_referals, $iter+1);
        } 

        return $this->main_user_for_ref;
    }
    
    public function getMyReferals($user_id = null) {
        $user_id = $user_id?$user_id:$this->id;
        $refs_key = RefKeys::where('parent_id',  $user_id)->get();
        $res = collect();
        foreach ($refs_key as $rk) {
            $res->push($rk->user);
        }
        return $res;
    }
    
    /**
     * Получить список пригласивших, для которых нужно начислить бонусы
     * @param type $user_id
     * @param type $level
     * @return type
     */
    public function getMyParrents($user_id = null,$level = 0) {
        if(!$this->main_user_parents) { $this->main_user_parents = collect(); }
        
        if($level > config('vlavlat.ref_max_level')) { return NULL; }

        $user_id = $user_id?$user_id:$this->id;
        
        $user_key = RefKeys::where('user_id',  $user_id)->first();

        if ($user_key && $level) {
            $this->main_user_parents->put($level, $user_key);
        }
        
        if($user_key && $user_key->parent_id) {
            $this->getMyParrents($user_key->parent_id, ++$level);
        }

        return $this->main_user_parents;
    }
    
    public function getMyParrentAttribute() {
        $parrent = RefKeys::where('user_id', $this->id)->first();

        if ($parrent) {
            return $parrent->uparent?$parrent->uparent->fullFIO:NULL;
        }
        
        return NULL;
    }

    /**
     * Все бонусы текущего или выбранного пользователя
     * @param int|null $user_id
     * @return int
     */
    public function getMyReferalsAllBonus($user_id = null) {
        $user_id = $user_id?$user_id:$this->id;
        return RefHistory::where('parent_id',  $user_id)->sum('value');
    }
    
    
    /**
     * Все балы от админа/лектора и хз
     * @return type
     */
    public function balls()
    {
      return $this->morphMany(Balls::class, 'balltable');
    }
    
    
    /**
     * Получаем сумму минимального платежа, для становления членом клуба, 
     * в зависимости от того через кого зарегистрировался пользователь
     * @return int
     */
    public function getMinPayAttribute() {
        $parrent = RefKeys::where('user_id', $this->id)->first();

        if ($parrent && $parrent->uparent && $parrent->uparent->isAdmin()) {
            return config('vlavlat.admin_first_pay');
        }
        return config('vlavlat.standart_first_pay');
    }
}
/**
    public function refUserStat(){
        if(Auth::check()){
            $ref_orders = RefOrders::where('user_give_id',Auth::id())->orderBy('id','desc')->paginate(15);
            return view('users.statistics.ref_user_stat',compact('ref_orders'));
        }
        return redirect('/');
    }
 */