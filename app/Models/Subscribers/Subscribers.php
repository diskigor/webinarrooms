<?php

namespace App\Models\Subscribers;

use App\User;

use Illuminate\Database\Eloquent\Model;

class Subscribers extends Model
{
    public static $FOR_MEDIASCAN                = 10;//тип для рассылки
    public static $FOR_NEWS                     = 20;//тип для новостей
    public static $FOR_BLOGS                    = 30;//тип для блогов
    public static $FOR_SYSTEM_EMAIL             = 40;//уведомления системы

    protected $table='subscribers';
    
    protected $guarded = ['id'];
    
    
    
    
    /**
     * Связь с таблицей пользователей
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
    
    /**
     * Связь с таблицей пользователей/авторов
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author(){
        return $this->belongsTo(User::class,'author_id','id');
    }
    
    
    /**
     * Получить массив всех доступных типов
     * @return array
     */
    public static function getAllType()
    {
        return [
            self::$FOR_MEDIASCAN    => 'Подписка на рассылку',
           // self::$FOR_NEWS         => 'Подписка на новости',
            self::$FOR_BLOGS        => 'Подписка на блоги',
            self::$FOR_SYSTEM_EMAIL => 'Уведомления системы',
        ];
    }
    
    
    /**
     * Получить все модели, обладающие subscriberstable.
     */
    public function subscriberstable()
    {
      return $this->morphTo();
    }    
    

    
    /**
     * Атрибут, получить имя пользователя
     * @return string
     */
    public function getUNameAttribute()
    {
        if (!$this->relationLoaded('user')) { $this->load('user'); } 
        if ($this->user) { return $this->user->fullName; }
        return $this->name;
    } 
    
    
    /**
     * Атрибут, получить имя пользователя
     * @return string
     */
    public function getUAuthorAttribute()
    {
        if (!$this->relationLoaded('author')) { $this->load('author'); } 
        if ($this->author) { return $this->author->fullName; }
        return '';
    }
    
    /**
     * Атрибут, проверка статуса подписки
     * @return boolean
     */
    public function getUTypeAttribute()
    {
        if(array_key_exists($this->type, self::getAllType())) {
             return self::getAllType()[$this->type];
        }
        return self::getAllType()[self::$FOR_MEDIASCAN];
    } 
    
    /**
     * Атрибут, проверка статуса подписки
     * @return boolean
     */
    public function getActiveAttribute()
    {
        return $this->status;
    } 
    
    /**
     * Заготовка активных
     * @param  object        $query
     * @return object        $query
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    
    /**
     * Заготовка подписок конкретного пользователя && типа
     * @param  object        $query
     * @param  int           $user_id
     * @param  int           $type
     * @return object        $query
     */
    public function scopeGetUserOf($query, $user_id, $type = 0)
    {
        $query->where('user_id',$user_id);
        
        if(array_key_exists($type, self::getAllType())) {
            $query->where('type',$type);
        }
        
        return $query;
    }
    
    
}
