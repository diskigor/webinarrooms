<?php

namespace App\Models\Favorites;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Favorites extends Model
{
    protected  $table= "favorites_users";

    public function favoritUser(){
        return $this->belongsTo(User::class,'favor_user_id','id');
    }
}
