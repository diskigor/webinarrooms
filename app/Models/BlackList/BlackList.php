<?php

namespace App\Models\BlackList;

use App\User;
use Illuminate\Database\Eloquent\Model;

class BlackList extends Model
{
    protected  $table = "users_banlist";

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
}
