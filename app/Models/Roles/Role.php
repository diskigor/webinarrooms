<?php

namespace App\Models\Roles;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table="roles";

    public function user(){
        return $this->belongsTo(User::class);
    }
    
    
  /**
    * Получить описание роли псевдо пользователя.
    */
  public function getPseudoDescriptionAttribute()
  {     
      if ($this->name == 'Cadet' && (config('vlavlat.balls_up_role_to_pretendent') < Auth::user()->level_point)) {
          return 'Продвинутый';
      }
      return $this->description;
  }
  
  /**
    * Получить псевдо роли  пользователя.
    */
  public function getPseudoNameAttribute()
  {     
      if ($this->name == 'Cadet' && (config('vlavlat.balls_up_role_to_pretendent') < Auth::user()->level_point)) {
          return 'Pretendent';
      }
      return $this->name;
  }
    
    /**
     * Атрибут количестов пользователей конкретной роли
     * @return boolean
     */
    public function getCountUsersAttribute()
    {
        return User::where('role_id',  $this->id)->count();
    }
}
