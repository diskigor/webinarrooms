<?php

namespace App\Models\Callback;

use App\User;
use App\Models\Chats\Chats;
use Illuminate\Database\Eloquent\Model;

class Callback extends Model
{
    protected $table = 'message_admin';

    protected $guarded = ['id'];

    public function author(){
        return $this->belongsTo(User::class,'user_id','id');
    }
    
    public function chats() {
        return $this->hasMany(Chats::class, 'ma_id','id');
    }
    
    
    
    
    public function getIsAdminAttribute() {
        return $this->sadmin;
    }
    
    
    public function getAdminStatusMessageAttribute(){
        if ($this->sadmin && $this->status_new == 'send' || !$this->sadmin && $this->status_new == 'new') {
            return 'send';
        }
        
        if ($this->status_new == 'read') {
            return 'read';
        }
        
        if ($this->sadmin && $this->status_new == 'new' || !$this->sadmin && $this->status_new == 'send') {
            return 'new';
        }
        
    }

    
    public function getUserStatusMessageAttribute(){
        if (!$this->sadmin && $this->status_new == 'send' || $this->sadmin && $this->status_new == 'new') {
            return 'send';
        }
        
        if ($this->status_new == 'read') {
            return 'read';
        }
        
        if (!$this->sadmin && $this->status_new == 'new' || $this->sadmin && $this->status_new == 'send') {
            return 'new';
        }
        
    }
    

    
    
    
    
    public function scopeSendMessage($query){
        return $query->where('status_new', 'send');
    }
    
    public function scopeReadMessage($query){
        return $query->where('status_new', 'read');
    }
    
    public function scopeNewMessage($query){
        return $query->where('status_new', 'new');
    }
    
    
}
