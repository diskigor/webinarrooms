<?php

namespace App\Models\Shop;

use App\Models\Curses\Curse;
use App\Models\Roles\Role;
use Illuminate\Database\Eloquent\Model;

class buyUsersCourses extends Model
{
    protected $table = 'users_curses';

    protected $guarded = ['id'];
    

    public function course()
    {
        return $this->belongsTo(Curse::class, 'curses_id', 'id');
    }

    public function procent($course_id, $user_id)
    {
        $progress = Progress::where('user_id', $user_id)->where('curses_id', $course_id)->first();
        return $progress?$progress->currentProcent():0;
    }
    
    public function currentProcent() {
        return $this->procent($this->curses_id, $this->user_id);
    }
    
    public static function getSummAllActiveCurses($discont = 0) {
        $active     = Curse::active()->get();
        $role_cadet = Role::where('name','Cadet')->first();

        $block_users_courses = 0;

        foreach ($active as $c){
            $block_users_courses += $c->groupPrice($role_cadet->id)->price;
        }

        if ($discont>0) {
            return round($block_users_courses - $block_users_courses/100*$discont +1) ;
        }

        return $block_users_courses;
    }
}
