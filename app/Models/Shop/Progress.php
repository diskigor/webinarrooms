<?php

namespace App\Models\Shop;

use Illuminate\Database\Eloquent\Model;

use App\Models\Curses\Curse;
use Carbon\Carbon;

class Progress extends Model
{
    protected $table='sdo_progress';
    
    protected $guarded = ['id'];
    
    /**
      * Атрибуты, которые должны быть преобразованы к базовым типам.
      *
      * @var array
      */
    protected $casts = [
      'progress'    => 'array',
      'full_steps'  => 'array',
    ];
    
    public function curse()
    {
        return $this->belongsTo(Curse::class, 'curses_id', 'id');
    }

    public function getClosedAttribute(){
        return $this->fullStepsCount() === $this->allStepsCount()? \TRUE :  \FALSE;
    }
    
    public function deleteProgressID($id){
        if (in_array($id, $this->currentSteps())) {
            $s = array_search($id,$this->currentSteps());
            unset($this->progress[$s]);
        } else { return; }
        
        if (in_array($id, $this->fullSteps())) {
            $s = array_search($id,$this->fullSteps());
            unset($this->full_steps[$s]);
        }
        
        if (!$this->relationLoaded('curse')) {$this->load('curse');}
        
        //$sss = array_values($this->progress);
        foreach ($this->currentSteps() as $idx) {
            $nnex = $this->curse->nextLectionID($idx);
            if ($nnex) {
                $this->pushNextStep($nnex->id);
            } else { break; }
        }
        $this->push();
    }

    /**
     * Добавление в список полных/пройденных шагов
     * @param type $id
     * @return void
     */
    public function pushFullStep($id) {
        if (!in_array($id, $this->currentSteps())) { return; }//если нет в списке доступных
        if (in_array($id, $this->fullSteps())) { return; }//если уже есть в списке полных/пройденных шагов
        
        if(isset($this->progress[$id]['available'])) {
            $a = $this->progress[$id]['available'];
        } else {
            $a = Carbon::now()->startOfDay()->toDateTimeString();
        }
        
        $fix = $this->full_steps;
        $fix[$id] = ['available' =>  $a,'completed' =>  Carbon::now()->toDateTimeString()];
        $this->full_steps = $fix;
        
        $this->push();
    }
    
    /**
     * Получить изученные шаги/лекции
     * @return array
     */
    public function fullSteps() {
        if (is_array($this->full_steps)) {
            if (in_array(0, array_keys($this->full_steps))) { return array_keys($this->refixAddDate('full_steps'));}//fix add date steps
            return array_keys($this->full_steps);//add date steps
        }
        return array();
    }
      
    /**
     * Получить количество изученных шагов в курсе
     * @return int
     */
    public function fullStepsCount() {
        return count($this->fullSteps());
    }
    
    /**
     * Добавление доступных шагов/лекций
     * @param type $id
     * @return void
     */
    public function pushNextStep($id) {
        if (in_array($id, $this->currentSteps())) { return; }//если уже есть в списке доступных шагов/лекций
        
        $fix = $this->progress;
        $fix[$id] = ['created' =>  Carbon::now()->toDateTimeString(),'available' =>  Carbon::now()->addDay(config('vlavlat.add_day_for_lection_complete'))->startOfDay()->toDateTimeString()];//будет доступна завтра
        $this->progress = $fix;

        $this->push();
    }
    
    
    public function openStepFromBall($id) {
        if (!in_array($id, $this->currentSteps())) { return; }//если нет в списке доступных шагов/лекций
        $fix = $this->progress;
        $fix[$id]['available'] = Carbon::now()->toDateTimeString();//будет доступна сейчас
        $this->progress = $fix;
        $this->push();
        return true;
    }
    
    /**
     * 
     * @param type $id
     * @return array
     */
    public function getTimeInfoLection($id){
        $result = ['created'=> NULL,'available'=> NULL,'completed'=> NULL];
        if (!in_array($id, $this->currentSteps())) { return $result; }//если нет в списке доступных
        $progress = $this->progress;
        $result['created'] = $progress[$id]['created'];
        $result['available'] = Carbon::createFromFormat('Y-m-d H:i:s',$progress[$id]['available'])->format('d-m-Y');
        if (!in_array($id, $this->fullSteps())) { return $result; }//если нет  в списке полных/пройденных шагов
        $fulls =$this->full_steps;
        $result['completed'] = Carbon::createFromFormat('Y-m-d H:i:s',$fulls[$id]['completed'])->format('d-m-Y');
        return $result;
    }

    /**
     * Получить доступные шаги/лекции
     * @return array
     */
    public function currentSteps() {
        if (is_array($this->progress) && count($this->progress)>0) {
            if (in_array(0, array_keys($this->progress))) {return array_keys($this->refixAddDate('progress'));}//fix add date steps
            return array_keys($this->progress);
        }
        return $this->initFirstStep();
    }
    
    /**
     * Получить ID текущей лекции
     * @return int
     */
    public function currentStep() {
        $a = array_diff($this->currentSteps(), $this->fullSteps());
        //$a = array_diff( $this->fullSteps() , $this->currentSteps() );
        return (int)array_pop($a);
    }
    
    /**
     * Доступность текущей лекции
     * @return boolean
     */
    public function currentStepAvailable() {
        $c = $this->currentStep();
        if ($this->currentStep() >= 0 ) { return FALSE; }
        
        if(isset($this->progress[$c]['available'])) {
            $a = $this->progress[$c]['available'] <= Carbon::now() ;
        } else {
            $a = FALSE;
        }
        return $a;
    }
    
    /**
     * Доступность лекции в прогрессе, по ID
     * @param int $id
     * @return boolean
     */
    public function getAvailableLection($id = -1) {
        if (!in_array($id, $this->currentSteps())) { return FALSE; }//если нет в списке доступных

        if(isset($this->progress[$id]['available'])) {
            $a = $this->progress[$id]['available'] <= Carbon::now() ;
        } else {
            $a = FALSE;
        }
        return $a;
    }
    
    /**
     * Доступность лекции в прогрессе, по ID в виде даты или false
     * @param int $id
     * @return boolean|string
     */
    public function getAvailableLectionDate($id = -1) {
        if (!in_array($id, $this->currentSteps())) { return FALSE; }//если нет в списке доступных

        if(isset($this->progress[$id]['available'])) {
            $a = Carbon::parse($this->progress[$id]['available'])->toDateString();
        } else {
            $a = FALSE;
        }
        return $a;
    }
    
    
    /**
     * Получить количество пройденных шагов в курсе
     * @return int
     */
    public function currentStepsCount() {
        return count($this->currentSteps());
    }

    
    public function allStepsCount() {
        if (!$this->relationLoaded('curse')) {
            $this->load('curse');
            }
//        $all = 0;
//        if ($this->curse->count() !== 0) {
//            $all = $this->curse->lectures()->count();
//        }
        return $this->curse?$this->curse->lectures()->count():0;    
    }
    
    public function currentProcent() {
        $all = $this->allStepsCount();
        $cur = $this->fullStepsCount();
        if($all===0 || $cur===0) { return 0;}
        if($cur >= $all) { return 100;}
        return round($cur/$all*100,2);
    }
    
    
    protected function initFirstStep() {
        if (!$this->relationLoaded('curse')) {
            $this->load('curse');
            }
            
        $firststep = ['0' => ['created' =>  Carbon::now()->subDay()->startOfDay()->toDateTimeString(),'available' =>  Carbon::now()->startOfDay()->toDateTimeString()]];
        
        if ($this->curse()->count() !== 0) {
            $first = $this->curse->firstLectionID();
            $firststep = $first?[$first->id => ['created' =>  Carbon::now()->startOfDay()->toDateTimeString(),'available' =>  Carbon::now()->startOfDay()->toDateTimeString()]]:$firststep;
        }

        $this->progress = $firststep;
        $this->push();
        return array_keys($firststep);
    }
    
    
    protected function refixAddDate($type = 'full_steps') {
        $fix = [];
        
        if ($type == 'full_steps') {
            foreach ($this->full_steps as $value) {
                if(isset($value['available'])) { continue;}
                $fix[$value] = ['available' => $this->created_at->toDateTimeString(),'completed' =>  $this->updated_at->toDateTimeString()];
            }
            if (empty($fix)) { $fix = ['0' => [ 'available' =>  Carbon::now()->subDay()->startOfDay()->toDateTimeString(), 'completed' =>  Carbon::now()->startOfDay()->toDateTimeString()]];}
            $this->full_steps = $fix;
        }
        
        if ($type == 'progress') {
            foreach ($this->progress as $value) {
                if(isset($value['created'])) { continue;}
                $fix[$value] = ['created' =>  Carbon::now()->subDay()->startOfDay()->toDateTimeString(),'available' =>  Carbon::now()->startOfDay()->toDateTimeString()];
            }
            if (empty($fix)) {$fix = ['0' => ['created' =>  Carbon::now()->subDay()->startOfDay()->toDateTimeString(),'available' =>  Carbon::now()->startOfDay()->toDateTimeString()]];}
            $this->progress = $fix;
        }

        $this->push();
        return $fix;
    }
}
