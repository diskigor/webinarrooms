<?php

namespace App\Models\Blog;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

use App\User;
use App\Models\Comment;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

use Lecturize\Tags\Traits\HasTags;

class Blog extends Model
{
    use HasTags;
    
    protected $table='blog';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Атрибут картинка или заглушку
     * @return boolean
     */
    public function getImageAttribute()
    {
        if(Storage::disk('public')->exists($this->image_link)) {
            return Storage::url($this->image_link);
        }
        return asset('images/blog_pic_5.jpg');
    }
    
    
    public function author() {
        return $this->belongsTo(User::class,'author_id','id');
    }
    
    /**
     * Атрибут полное имя автора, если есть
     * @return boolean
     */
    public function getAuthorFIOAttribute()
    {
        if (!$this->relationLoaded('author')) { $this->load('author'); }
        
        if (!$this->author){
            return '';
        } 
        return $this->author->fullFIO;
    }
    
    public function likes()
    {
        return $this->morphToMany(User::class, 'likeable')->whereDeletedAt(null);
    }
    
    public function getIsLikedAttribute()
    {
        $like = $this->likes()->whereUserId(Auth::id())->first();
        return (!is_null($like)) ? true : false;
    }
    
    public function comments()
    {
     return $this->hasMany(Comment::class);
    }
    
    public function commentsParrent()
    {
     return $this->comments()->whereNull('parent_id');
    }
    
    /**
     * Проверка новизны статьи
     * @return boolean
     */
    public function getIsNewAttribute()
    {
        return $this->created_at >= Carbon::now()->subday(config('vlavlat.limit_new_blog'));
    }
    
    /**
     * Заготовка счётчика новых статей блога
     * @param  mixed    $group
     * @return int
     */
    public static function CountNew($group = null)
    {
        $result = self::query()->where('created_at','>=',Carbon::now()->subday(config('vlavlat.limit_new_blog')));
        switch ($group){
            case 'gilbo':
                $result->gilbo();
                break;
            case 'lecturer':
                $result->lecturer();
                break;
            case 'gaypark':
                $result->gaypark();
        }
        return $result->count() ;
    }
    
    
    /**
     * Заготовка автора Гильбо
     * @param  object        $query
     * @return object        $query
     */
    public function scopeGilbo($query)
    {
        return $query->where('author_id', config('vlavlat.personal_IdGilbo'));
    }
    
    /**
     * Атрибут автора Гильбо
     * @return boolean
     */
    public function getIsGilboAttribute()
    {
        return ($this->author_id == config('vlavlat.personal_IdGilbo'));
    }
    
    /**
     * Заготовка лекторы
     * @param  object        $query
     * @return object        $query
     */
    public function scopeLecturer($query)
    {
        return $query->whereHas('author', function($qa){
            $qa->whereHas('role',function($ro){
                $ro->where('name','Lecturer');
            });
        });
    }
    
    /**
     * Атрибут лектора
     * @return boolean
     */
    public function getIsLecturerAttribute()
    {
        return ($this->author->isLecturer());
    }
    
    /**
     * Заготовка для геев
     * @param  object        $query
     * @return object        $query
     */
    public function scopeGaypark($query)
    {        
        return $query->whereHas('author', function($qa){
            $qa->whereHas('role',function($ro){
                $ro->where('name','<>','Lecturer');
            })->where('author_id','<>', config('vlavlat.personal_IdGilbo'));
        });
    }
    
    /**
     * Атрибут гея
     * @return boolean
     */
    public function getIsGayparkAttribute()
    {
        return (!$this->getIsGilboAttribute() && !$this->getIsLecturerAttribute());
    }
    
    
    /**
     * Заготовка лекторы
     * @param  object        $query
     * @return object        $query
     */
    public function scopeLecturerGilbo($query)
    {
        return $query->whereHas('author', function($qa){
            $qa->whereHas('role',function($ro){
                $ro->where('name','Lecturer');
            })->orWhere('author_id', config('vlavlat.personal_IdGilbo'));
        });
    }
    
    /**
     * Атрибут счётчик просмотров
     * @return boolean
     */
    public function getCounterAttribute()
    {
        return $this->count_view;
    }
    
}
