<?php

namespace App\Models\Prelections;

use Illuminate\Database\Eloquent\Model;

class PrelectionTrans extends Model
{
    protected $table= 'sdo_lectures_trans';

    public function sdoLectures(){
        return $this->belongsTo(Prelection::class,'sdo_lectures_id','id');
    }
}
