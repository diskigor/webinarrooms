<?php

namespace App\Models\Prelections;

use App\Models\Curses\Curse;
use App\Models\Materials\MaterialsLectures;
use App\Models\Tasks\Test;
use App\Models\Tasks\TextTasks;
use App\Models\UserAnswer\TestUserAnswer;
use App\Models\UserAnswer\TextAnswer;
use App\Models\Billing\Balls;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class Prelection extends Model
{
    protected $table = 'sdo_lectures';

    /**
     * Связь для вывода лекций по языку приложения
     * @return mixed
     */
    public function sdoLectureTrans()
    {
        $local = App::getLocale();
        return $this->hasMany(PrelectionTrans::class, 'sdo_lectures_id', 'id')
            ->where('lang_local', $local)
            ->first();
    }
    /**
     * Связь для редактирования лекции
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sdoLectureTransEdit()
    {
        return $this->hasMany(PrelectionTrans::class, 'sdo_lectures_id', 'id');
    }
    
    
    /**
     * Связь для вывода лекций  на языке приложения
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function lang(){
        $local = App::getLocale();
        return $this->belongsTo(PrelectionTrans::class,'id','sdo_lectures_id')->where('lang_local',$local);
    }
    
    /**
     * Метод, accessor,
     * возвращает название название ЛЕКЦИИ на языке приложения
     * @return string
     */
    public function getNameAttribute() {
        if (!$this->relationLoaded('lang')) { $this->load('lang'); } 
        if ($this->lang) { return $this->lang->name_trans; }
        return '';
    }
    
    /**
     * Метод, accessor,
     * возвращает нкраткое название ЛЕКЦИИ на языке приложения
     * @return string
     */
    public function getLangShortDAttribute() {
        if (!$this->relationLoaded('lang')) { $this->load('lang'); } 
        if ($this->lang) { return $this->lang->short_desc; }
        return '';
    }
    
    /**
     * Метод, accessor,
     * возвращает нкраткое название ЛЕКЦИИ на языке приложения
     * @return string
     */
    public function getLangDescriptionAttribute() {
        if (!$this->relationLoaded('lang')) { $this->load('lang'); } 
        if ($this->lang) { return $this->lang->content_trans; }
        return '';
    }
    
    /**
     * Связь для вывода автора лекции
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    /**
     * Связь с курсом лекции
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo(Curse::class, 'curse_id', 'id');
    }

    /**
     * Связь с таблицей отвечающая за связь лекции с текстовыми заданиями
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function text_tasks()
    {
        return $this->hasMany(TextTasks::class, 'lecture_id');
    }
    /**
     * Связь с таблицей дополнительных материалов
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function materials()
    {
        return $this->hasMany(MaterialsLectures::class, 'lecture_id', 'id')->orderBy('sort','ASC');
    }

    /**
     * Связь с таблице тестовых заданий
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function test_tasks(){
        return $this->hasMany(Test::class,'lectures_id','id');
    }
    
    
    /**
     * Сумма баллов за все тесты лекции
     * @return int
     */
    public function getTestBallsCountAttribute()
    {
        return Test::where('lectures_id', $this->id)->sum('point');
    }
    
    /**
     * Сумма баллов за все тесты лекции
     * @return int
     */
    public function getAllBallsCountAttribute()
    {
        return $this->price + Test::where('lectures_id', $this->id)->sum('point');
    }

    /**
     * Метод проверки в лекции для вывода кнопки прохождения
     * @return bool
     */
    public function checkPrelectionAllTasks(){
        $user= Auth::user();
        $test_tasks = Test::where('lectures_id',$this->id)->get();
        if($test_tasks->count() != 0){
            foreach ($test_tasks as $test_task){
                $test_answer_user = TestUserAnswer::where('user_id',$user->id)
                    ->where('test_id',$test_task->id)
                    ->first();
                if(is_null($test_answer_user)){
                    return FALSE;
                }
                if($test_answer_user->status != 1){
                    return FALSE;
                }
            }
        }
        $text_tasks = TextTasks::where('lecture_id',$this->id)->get();
        if($text_tasks->count()!= 0){
            foreach ($text_tasks as $text_task){
                $text_answer_task = TextAnswer::where('task_id',$text_task->id)->where('user_id',$user->id)->first();
                if(is_null($text_answer_task)){
                    return FALSE;
                }
                if($text_answer_task->status != 1){
                    return FALSE;
                }
            }
        }
        return TRUE;
    }
    
    
    /**
     * Все балы за прохождение лекции
     * @return type
     */
    public function balls()
    {
      return $this->morphMany(Balls::class, 'balltable');
    }
}
