<?php

namespace App\Models\Billing;

use App\User;
use Illuminate\Database\Eloquent\Model;

class __RefOrders extends Model
{
    protected $table='ref_statistics';

    public function userGive(){
        return $this->belongsTo(User::class,'user_give_id','id');
    }

    public function userUse(){
        return $this->belongsTo(User::class,'user_use_id','id');
    }
}
