<?php

namespace App\Models\Billing;

use App\User;
use App\Models\Billing\RefRules;
use App\Models\Billing\Billing;
use Illuminate\Database\Eloquent\Model;

class RefHistory extends Model
{
    protected $table='ref_history';
    
    protected $guarded = ['id'];

    /**
      * Атрибуты, которые должны быть преобразованы к базовым типам.
      *
      * @var array
      */
    protected $casts = [
      'rules_desc'    => 'array'
    ];
    
    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function uparent(){
        return $this->belongsTo(User::class,'parent_id','id');
    }
    
    public function rule(){
        return $this->belongsTo(RefRules::class,'rules_id','id');
    }
    
    public function getNameRuleAttribute(){
        if (!$this->relationLoaded('rule')) { $this->load('rule'); }
        
        if (!$this->rule){
            return '';
        } 
        return $this->rule->nameRule;
    }
    
    public function getValueCurrencyAttribute() {
        return $this->value.' '.Billing::TYPE_CURRENCY;
    }
    
    public function getOperationValueCurrencyAttribute() {
        return $this->operation_value.' '.Billing::TYPE_CURRENCY;
    }
}
