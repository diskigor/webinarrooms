<?php

namespace App\Models\Billing;

use App\Models\Curses\Curse;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Balls extends Model
{
    public static $FROM_ADMIN       = 1;//тип от админа
    public static $FOR_TEST         = 10;//тип для тестов
    public static $FOR_LECTION      = 20;//тип для лекций
    public static $FOR_BALLS_TO_MONEY   = 30;//тип для конвертаций
    public static $FOR_BEST_COMMENT_USER_ANSWER = 40;//тип для комментариев к текстовому заданию
    public static $FOR_OPEN_LECTURE = 50;//тип для Открытие лекции за балы
    public static $FOR_FULL_PROFILE = 60;//тип для баллов за зполненный профиль
    
    protected $table= 'sdo_balls';

    protected $guarded = ['id'];
    
    /**
     * Получить массив всех доступных типов
     * @return array
     */
    public static function getAllType()
    {
        return [
            self::$FROM_ADMIN                   => 'Админские',
            self::$FOR_TEST                     => 'Тесты',
            self::$FOR_LECTION                  => 'Лекции',
            self::$FOR_BALLS_TO_MONEY           => 'Конвертация',
            self::$FOR_BEST_COMMENT_USER_ANSWER => 'Комментарий к текстовым заданиям',
            self::$FOR_OPEN_LECTURE             => 'Открытие лекции за балы',
            self::$FOR_FULL_PROFILE             => 'Балы за заполненный профиль',
        ];
    }
    
    
    /**
     * Связь с таблицей пользователей
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    /**
     * Получить все модели, обладающие balltable.
     */
    public function balltable()
    {
      return $this->morphTo();
    }
    
    
    /**
     * Заготовка баллов конкретного пользователя && типа
     * @param  object        $query
     * @param  int           $user_id
     * @param  int           $type
     * @return object        $query
     */
    public function scopeGetUserOf($query, $user_id, $type = 0)
    {
        $query->where('user_id',$user_id);
        
        if(array_key_exists($type, self::getAllType())) {
            $query->where('type',$type);
        }
        
        return $query;
    }
    
    /**
     * Заготовка активных
     * @param  object        $query
     * @return object        $query
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    
}
