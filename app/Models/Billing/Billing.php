<?php

namespace App\Models\Billing;

use App\User;
use App\Models\Billing\Balls;
use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
    protected $table='sdo_billing_orders';

    protected $guarded = ['id'];
    
    /**
     * Пополнение пользователем, через кассу
     */
    const TYPE_USER     = 0;
    
    /**
     * Пополнение администратором
     */
    const TYPE_ADMIN    = 1;
    
    /**
     * Признак реферальных начислений
     */
    const TYPE_REFERAL  = 10;
    
    /**
     * доход лектора (автора курса)
     */
    const TYPE_LECTURE  = 20; 
    
    /**
     * Признак операции по конвертации баллов во внутреннюю валюту системы
     */
    const TYPE_BALLS_TO_MONEY  = 30; 
    
    /**
     * покупка
     */
    const TYPE_BUY_COURSE  = 40; 
    
    /**
     * Внутренняя валюта системы
     */
    const TYPE_CURRENCY = 'КЭЛ';

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function admin(){
        return $this->belongsTo(User::class,'admin_id','id');
    }
    
    /**
     * Заготовка поплнений пользователя
     * @param  object        $query
     * @return object        $query
     */
    public function scopeAddUser($query)
    {
        return $query->where('type_replenishment', self::TYPE_USER);
    }
    
    /**
     * Заготовка поплнений конкретного пользователя
     * @param  object        $query
     * @param  int           $user_id
     * @param  boolean       $all
     * @return object        $query
     */
    public function scopeGetUserOf($query, $user_id, $all = TRUE)
    {
        if($all) {
            return $query->where('user_id',$user_id);
        } else {
            return $query->where('type_replenishment', self::TYPE_USER)->where('user_id',$user_id);
        }
        
    }
    
    
    /**
     * Атрибут проверка поплнения пользователем
     * @param  object        $query
     * @return object        $query
     */
    public function getIsUserAttribute()
    {
        return $this->type_replenishment == self::TYPE_USER;
    }
    
    /**
     * Заготовка поплнений админом
     * @param  object        $query
     * @return object        $query
     */
    public function scopeAddAdmin($query)
    {
        return $query->where('type_replenishment', self::TYPE_ADMIN);
    }

    /**
     * Атрибут проверка поплнения админом
     * @param  object        $query
     * @return object        $query
     */
    public function getIsAdminAttribute()
    {
        return $this->type_replenishment == self::TYPE_ADMIN;
    } 
            
    /**
     * Заготовка поплнений referal
     * @param  object        $query
     * @return object        $query
     */
    public function scopeAddReferal($query)
    {
        return $query->where('type_replenishment', self::TYPE_REFERAL);
    }
    
    /**
     * Атрибут проверка поплнения referal
     * @param  object        $query
     * @return object        $query
     */
    public function getIsReferalAttribute()
    {
        return $this->type_replenishment == self::TYPE_REFERAL;
    }
    
    
    /**
     * Все балы от конвертации
     * @return type
     */
    public function balls()
    {
      return $this->morphMany(Balls::class, 'balltable');
    }
    
    
    /**
     * Заготовка поплнений пользователя
     * @param  object        $query
     * @return object        $query
     */
    public function scopeBayCourse($query)
    {
        return $query->where('type_replenishment', self::TYPE_BUY_COURSE);
    }
    
}
