<?php

namespace App\Models\Billing;

use App\User;
use Illuminate\Database\Eloquent\Model;

class RefKeys extends Model
{
    protected $table='ref_keys';
    
    protected $guarded = ['id'];

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function uparent(){
        return $this->belongsTo(User::class,'parent_id','id');
    }
    
    /**
     * 
     * @param int $user_id
     * @param mixed $level
     */
    public function getParrents($user_id, $level = 1) {
        
        
    }
}
