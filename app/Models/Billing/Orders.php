<?php

namespace App\Models\Billing;

use App\Models\Curses\Curse;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected  $table= 'sdo_orders';

    /**
     * Связь с таблицый пользователей
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    /**
     * Связь с таблицей курсов
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course(){
        return $this->belongsTo(Curse::class,'course_id','id');
    }

}
