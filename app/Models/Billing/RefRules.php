<?php

namespace App\Models\Billing;

use App\User;
use App\Models\Billing\Billing;
use Illuminate\Database\Eloquent\Model;

class RefRules extends Model
{
    protected $table='ref_rules';
    
    protected $guarded = ['id'];
    
 
    /**
     * Заготовка включенных правил
     * @param  object        $query
     * @return object        $query
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
    
    
    
    public function getNameRuleAttribute(){
        return $this->description.' '.$this->value.(($this->type == 'procent')?'%':Billing::TYPE_CURRENCY);
    }
    
    public static function calculateOfRule($inval, $level = 1, $onlyClub = false){
        $res = 0;
        
        if ($onlyClub) {
            $rule = self::query()->active()->where('level', $level)->where('onlyclub')->first();
        } else {
            $rule = self::query()->active()->where('level', $level)->first();
        }
        
        if (!$rule) { return $res; }

        switch ($rule->type) {
            case 'procent':
                $res = $inval * $rule->value / 100;
                break;
            case 'fixed':
                $res = $rule->value;
        }
        
        return $res;
    }
}
