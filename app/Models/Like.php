<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Blog\Blog;
use App\Models\Comment;

class Like extends Model
{
    use SoftDeletes;

    protected $table = 'likeables';

    protected $fillable = [
        'user_id',
        'likeable_id',
        'likeable_type',
    ];
    
    /**
     * Get all of the products that are assigned this like.
     */
    public function blogs()
    {
        return $this->morphedByMany(Blog::class, 'likeable');
    }
    
    /**
     * Get all of the products that are assigned this like.
     */
    public function comments()
    {
        return $this->morphedByMany(Comment::class, 'likeable');
    }
}
