<?php

namespace App\Models\Chats;

use App\User;
use App\Models\Callback\Callback;
use Illuminate\Database\Eloquent\Model;

class Chats extends Model
{
    protected $table ="sdo_chats";
    protected $guarded = ['id'];

    public function sender(){
        return $this->belongsTo(User::class,'sender_id','id');
    }
    
    public function recipient(){
        return $this->belongsTo(User::class,'recipient_id','id');
    }
    
    public function tema() {
        return $this->belongsTo(Callback::class,'ma_id','id');
    }
    
    
    
    
    
    
    public function getIsAdminAttribute() {
        return $this->sadmin;
    }
    
    public function getIsReadAttribute() {
        return $this->status;
    }
    
}
