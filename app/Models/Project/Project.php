<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Storage;

class Project extends Model {

    protected $table = "sdo_projects";

    /**
     * "Загружающий" метод модели.
     *
     * @return void
     */
    protected static function boot() {
        parent::boot();

        //всегда сортировать
        static::addGlobalScope(function (Builder $builder) {
            $builder->orderBy('main_div');
        });
    }

    /**
     * Атрибут картинка
     * @return boolean
     */
    public function getImageAttribute() {
        if (Storage::disk('public')->exists($this->image_link)) {
            return Storage::url($this->image_link);
        }

        if (file_exists(public_path($this->image_link))) {
            return asset($this->image_link);
        }

        return FALSE;
    }

    
    
    /**
     * Заготовка для фронта
     * @param  object        $query
     * @return object        $query
     */
    public function scopeForFront($query)
    {
        return $query->where('side_bar', 0);
    }
    
    /**
     * Проверка только для фронта
     * @return boolean
     */
    public function getForFrontAttribute()
    {
        return $this->side_bar?FALSE:TRUE;
    }
    
    
    
    /**
     * Заготовка для сйдбара
     * @param  object        $query
     * @return object        $query
     */
    public function scopeForSide($query)
    {
        return $query->where('side_bar', 1);
    }

    /**
     * Проверка только для сайдбара
     * @return boolean
     */
    public function getForSideAttribute()
    {
        return $this->side_bar?TRUE:FALSE;
    }
}
