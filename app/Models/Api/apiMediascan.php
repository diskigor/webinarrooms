<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Model;

class apiMediascan extends Model
{
    protected $table="mediascan";

    public function getMonthDay($year,$month){

        return $days = apiMediascan::where('year',$year)->where('month',$month)->get();
    }

}
