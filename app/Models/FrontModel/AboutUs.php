<?php

namespace App\Models\FrontModel;

use Illuminate\Database\Eloquent\Model;

class AboutUs extends Model
{
    protected $table='sdo_about_us';
}
