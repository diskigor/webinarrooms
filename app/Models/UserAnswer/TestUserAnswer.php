<?php

namespace App\Models\UserAnswer;

use Illuminate\Database\Eloquent\Model;

class TestUserAnswer extends Model
{
    protected $table='sdo_test_user_answer';
    
    /**
     * Атрибуты, для которых запрещено массовое назначение.
     *
     * @var array
     */
    protected $guarded = ['id'];
    
    public function getJsonAnswerAttribute() {
        try {
            $res = json_decode($this->descr_answer, true);
        } catch (Exception $exc) {
            $res = null;
        }
        return $res;
    }
}
