<?php

namespace App\Models\UserAnswer;

use App\Models\Tasks\TextAnswerComment;
use App\Models\Tasks\TextAnswerCommentLike;
use App\Models\Tasks\TextTasks;
use App\User;
use Illuminate\Database\Eloquent\Model;

class TextAnswer extends Model
{
    protected $table = "sdo_text_user_answer";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
    
    /**
     * Свзяь с таблицей пользователей
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function student()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Связь с таблицу текстовых заданий
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function text_task()
    {
        return $this->belongsTo(TextTasks::class, 'task_id', 'id');
    }

    public function checkUser()
    {
        return $this->belongsTo(User::class, 'check_user', 'id');
    }

    public function comments()
    {
        return $this->hasMany(TextAnswerComment::class, 'text_answer_id', 'id');
    }

    public function check_comment(){
        return $this->hasOne(TextAnswerCommentLike::class,'text_answer_id','id');
    }

}
