<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;
use App\User;

class UserGroup extends Model
{
    protected $table = "users_group";
        
    /**
     * Атрибут количестов пользователей конкретной группы
     * @return boolean
     */
    public function getCountUsersAttribute()
    {
        return User::where('group_id',  $this->id)->count();
    }
    
    
    public function getRuleIdsAttribute() {
        return $this->rule_id;
    }
}
