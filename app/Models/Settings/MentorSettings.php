<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

class MentorSettings extends Model
{
    protected $table='mentor_settings';
}
