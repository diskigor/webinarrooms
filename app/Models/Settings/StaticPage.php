<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Storage;

class StaticPage extends Model
{
    protected $table = "static_pages";
    
    /**
     * Атрибут картинка
     * @return boolean
     */
    public function getImageAttribute()
    {
        if(Storage::disk('public')->exists($this->banner_image)) {
            return Storage::url($this->banner_image);
        }
        
        if (file_exists(public_path($this->banner_image))) { 
            return asset($this->banner_image);
        }
        
        return FALSE;
    }
}
