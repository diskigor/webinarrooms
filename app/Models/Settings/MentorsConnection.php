<?php

namespace App\Models\Settings;

use App\User;
use Illuminate\Database\Eloquent\Model;

class MentorsConnection extends Model
{
    protected $table= "mentors_connection";

    public function cadet(){
        return $this->belongsTo(User::class,'cadet_id','id');
    }

    public function curator(){
        return $this->belongsTo(User::class,'curator_id','id');
    }
}
