<?php

namespace App\Models\Doings;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

use Illuminate\Support\Facades\Storage;

class Doings extends Model
{
    protected $table='sdo_events';

    public function event_trans(){
        $local = App::getLocale();
        return $this->hasMany(DoingsTrans::class,'sdo_events_id','id')
            ->where('lang_local',$local)->first();
    }

    public function relationForEditEvent(){
        return $this->hasMany(DoingsTrans::class,'sdo_events_id','id');
    }
    
    /**
     * Связь для вывода события на языке приложения
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function lang(){
        $local = App::getLocale();
        return $this->belongsTo(DoingsTrans::class,'id','sdo_events_id')->where('lang_local',$local);
        
    }
    
    /**
     * Атрибут картинка
     * @return boolean
     */
    public function getImageAttribute()
    {
        if(Storage::disk('public')->exists($this->image_link)) {
            return Storage::url($this->image_link);
        }
        
        if (file_exists(public_path($this->image_link))) { 
            return asset($this->image_link);
        }
        return asset('images/blog_pic_5.jpg');
    }
    
}
