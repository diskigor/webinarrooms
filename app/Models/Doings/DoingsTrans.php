<?php

namespace App\Models\Doings;

use Illuminate\Database\Eloquent\Model;

class DoingsTrans extends Model
{
    protected $table='sdo_events_trans';

    public function event(){
        return $this->belongsTo(Doings::class,'sdo_events_id','id');
    }
}
