<?php

namespace App\Models\Curses;

use App\Models\Curses\CurseBlock;
use Illuminate\Support\Facades\App;

use Illuminate\Database\Eloquent\Model;

class CurseBlockLang extends Model
{
    protected $table ='sdo_courses_block_lang';
    protected $guarded = ['id'];

    /**
     * Обратная связь с таблицей блоков
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function block(){
        return $this->belongsTo(CurseBlock::class,'sdo_courses_block_id','id');
    }
    
    /**
    * Заготовка запроса текущего языка.
    *
    * @param \Illuminate\Database\Eloquent\Builder $query
    * @return \Illuminate\Database\Eloquent\Builder
    */
   public function scopeCurrent($query, $local = null)
   {
        if(!$local) { $local = App::getLocale(); }
        return $query->where('language',$local);
   }
}

