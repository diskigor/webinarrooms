<?php

namespace App\Models\Curses;

use Illuminate\Database\Eloquent\Model;

class TransCurse extends Model
{
    protected $table = 'sdo_curses_trans';

    /**
     * Связь с таблицей курсов
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function curse(){

        return $this->belongsTo(Curse::class,'sdo_curses_id','id');
    }
}
