<?php

namespace App\Models\Curses;

use App\Models\Prelections\Prelection;
use App\Models\Shop\Progress;
use App\Models\Curses\CurseBlock;
use App\Models\Shop\buyUsersCourses;
use App\User;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use File;

use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\Model;

class Curse extends Model
{
    protected $table ='sdo_curses';
    
    /**
     * Связь с автором курса
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author() {
        return $this->belongsTo(User::class,'user_id','id');
    }
    
    /**
     * Атрибут картинка
     * @return boolean
     */
    public function getImagexAttribute()
    {
        if(Storage::disk('public')->exists($this->image)) {
            return Storage::url($this->image);
        }
        
        if (file_exists(public_path($this->image))) { 
            return asset($this->image);
        }
        return asset('images/blog_pic_5.jpg');
    }
    
    /**
     * Заготовка автора Гильбо
     * @param  object        $query
     * @return object        $query
     */
    public function scopeGilbo($query)
    {
        if(config('app.env') == 'local') {
            return $query->where('user_id', 1)->where('discount','<', 100);//для тестирования
        }
        return $query->where('user_id', config('vlavlat.personal_IdGilbo'))->where('discount','<', 100);
    }
    
    /**
     * Заготовка лекторы
     * @param  object        $query
     * @return object        $query
     */
    public function scopeLecturer($query)
    {
        return $query->whereHas('author', function($qa){
            $qa->whereHas('role',function($ro){
                $ro->where('name','Lecturer');
            })->where('discount','<', 100);
        });
    }
    
    
    /**
     * Связь для вывода курса на языке приложения
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function trans_curse(){
        $local = App::getLocale();
        return $this->hasMany(TransCurse::class,'sdo_curses_id','id')->where('lang_local',$local);
    }
    
    /**
     * Связь для вывода курса на языке приложения
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lang(){
        $local = App::getLocale();
        return $this->belongsTo(TransCurse::class,'id','sdo_curses_id')->where('lang_local',$local);
    }
    
    /**
     * Название курса на языке приложения
     * @return boolean
     */
    public function getLangNameAttribute()
    {
        if (!$this->relationLoaded('lang')) {
            $this->load('lang');
        }
        
        if($this->lang()->count() || $this->lang) {
            return $this->lang->name_trans;
        }

        return 'no name';
    }
    
    
    /**
     * Краткое описание курса на языке приложения
     * @return boolean
     */
    public function getLangShortDAttribute()
    {
        if (!$this->relationLoaded('lang')) {
            $this->load('lang');
        }
        
        if($this->lang()->count() || $this->lang) {
            return $this->lang->short_desc_trans;
        }

        return 'no short desc';
    }
    
    
    /**
     * Полное описание курса на языке приложения
     * @return boolean
     */
    public function getLangDescriptionAttribute()
    {
        if (!$this->relationLoaded('lang')) {
            $this->load('lang');
        }
        
        if($this->lang()->count() || $this->lang) {
            return $this->lang->description_trans;
        }

        return 'no description';
    }
    

    /**
     * Связь для редактирования курса
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getTransCurseEdit(){
        return $this->hasMany(TransCurse::class,'sdo_curses_id','id');
    }

    /**
     * Связь для получения лекций которые принадлежат курсу
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lectures(){
        return $this->hasMany(Prelection::class)->orderBy('number','ASC');
    }

    /**
     * Полное описание курса на языке приложения
     * @return int
     */
    public function getLecturesBallsCountAttribute()
    {
        if (!$this->relationLoaded('lectures')) {
            $this->load('lectures');
        }
        
        $res = 0;
        
        if($this->lectures()->count() || $this->lectures) {
            foreach ($this->lectures as $lec ) {
                $res+= $lec->allBallsCount;
                //dump($lec->price.' ---- '.$lec->testBallsCount);
            }
        }

        return $res;
    }
    
    
    
    public function groupPrice($id_group){
        return $this->hasMany(GroupPriceCurses::class,'course_id','id')->where('group_id',$id_group)->first();
    }

    public function freeMaterialRelation(){
        return $this->hasMany(GroupPriceCurses::class,'course_id','id');
    }
    
    
    /**
     * Свзяь с прогресами всех пользователей данного курса
     */
    public function progress() {
        return $this->hasMany(Progress::class, 'curses_id', 'id');
    }
    
    public function progressCurse($user_id = 0)
    {
        return $this->hasOne(Progress::class, 'curses_id', 'id')->where('user_id', $user_id)->first();
    }
    
    public function currentProcent($user_id = 0) {
        return $this->progressCurse($user_id)?$this->progressCurse($user_id)->currentProcent():0;
    }
    
    /**
     * Номер Последней лекции курса
     * @return int
     */
    public function lastLection(){
        $max_lecture = Prelection::where('curse_id',$this->id)->max('number');
        return $max_lecture;
    }
    
    /**
     * ID Первой лекции курса
     * @return Prelection
     */
    public function firstLectionID($col = '*'){
        $min_id = Prelection::where('curse_id',$this->id)->orderBy('number','ASC')->first([$col]);
        return $min_id;
    }
    
    /**
     * ID Последней лекции курса
     * @return Prelection
     */
    public function lastLectionID($col = '*'){
        $max_id = Prelection::where('curse_id',$this->id)->orderBy('number','DESC')->first([$col]);
        return $max_id;
    }
    
    /**
     * ID Последней лекции курса
     * @return int
     */
    public function verifyLastLectionID($id){
        $last_lection = $this->lastLectionID();
        return ($last_lection && $last_lection->id == $id)? \TRUE : \FALSE;
    }
    
    /**
     * Следующая лекция курса
     * @param int $current_lection_id
     * @return Prelection
     */
    public function nextLectionID($current_lection_id = 0){
        $curr = Prelection::where('curse_id',$this->id)->find((int)$current_lection_id);
        if (!$curr) { return null;}
        $next = Prelection::where('curse_id',$this->id)
                ->where('number','>',$curr->number)
                ->where('id','!=',(int)$current_lection_id)
                ->orderBy('number','ASC')
                ->first();
        return $next;
    }


    /**
   * Заготовка запроса включенных курсов.
   *
   * @param \Illuminate\Database\Eloquent\Builder $query
   * @return \Illuminate\Database\Eloquent\Builder
   */
    public function scopeActive($query)
    {
      return $query->where('status', 1);
    }
  
    /**
     * Обратная связь с таблицей блоков
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function block(){
        return $this->belongsTo(CurseBlock::class,'sdo_courses_block_id','id');
    }
    
    
    /**
     * Проверка новизны курса
     * @return boolean
     */
    public function getIsNewAttribute()
    {
        return $this->created_at >= Carbon::now()->subday(config('vlavlat.limit_new_course'));
    }
    
    /**
     * Заготовка счётчика новых курсов
     * @param  mixed    $group
     * @return int
     */
    public static function CountNew($group = null)
    {
        $result = self::query()->where('created_at','>=',Carbon::now()->subday(config('vlavlat.limit_new_course')));
        switch ($group){
            case 'gilbo':
                $result->gilbo();
                break;
            case 'lecturer':
                $result->lecturer();
        }
        return $result->count() ;
    }
    
    /**
     * Проверка новизны курса
     * @return boolean
     */
    public function getForViewFrontAttribute()
    {
        return $this->front_status>0;
    }

    /**
     * Заготовка запроса курсов для отображения на фронте.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActiveFront($query)
    {
      return $query->where('front_status', 1)->orderBy('sdo_courses_block_ordred','ASC')->orderBy('ordered','ASC');
    }
    
    /**
     * Заготовка запроса скидка 100%.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDiscount100($query)
    {
      return $query->where('discount','>=', 100)->where('status', 1);
    }
    
    
    /**
     * Проверка новизны курса
     * @return boolean
     */
    public function getIsFreeAttribute()
    {
        return $this->discount >= 100 && $this->status == 1;
    }
    
    /**
     * Сколько куплено данных курсов
     * @return boolean
     */
    public function getUserBuy()
    {
        return buyUsersCourses::where('curses_id' , $this->id)->count();
    }
}

