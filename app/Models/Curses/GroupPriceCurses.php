<?php

namespace App\Models\Curses;

use App\Models\Settings\UserGroup;
use Illuminate\Database\Eloquent\Model;

class GroupPriceCurses extends Model
{
  protected  $table="sdo_course_group_price";

  public function course(){
      return $this->belongsTo(Curse::class,'course_id','id');
  }

  public function group(){
      return $this->belongsTo(UserGroup::class,'group_id','id');
  }
}
