<?php

namespace App\Models\Curses;

use App\Models\Curses\CurseBlockLang;
use App\Models\Curses\Curse;
use App\User;

use Illuminate\Support\Facades\App;

use Illuminate\Database\Eloquent\Model;

class CurseBlock extends Model
{
    protected $table ='sdo_courses_block';
    protected $guarded = ['id'];

    
    /**
     * Обратная связь с таблицей пользователей
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
    
    /**
     * Метод, accessor,
     * возвращает название название block на языке приложения||на первом найденном языке
     * @return string
     */
    public function getAuthorAttribute() {

        if (is_null($this->user)) { $this->load('user'); }
            
        if ($this->user) { return $this->user->name; }
        
        return '-no author-';
    }
    
    
    /**
     * Связь для block lang
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lang(){
        return $this->hasMany(CurseBlockLang::class,'sdo_courses_block_id','id');
    }

    
    
    /**
     * Метод, accessor,
     * возвращает название название block на языке приложения||на первом найденном языке
     * @return string
     */
    public function getNameAttribute() {
        
        if (!$this->relationLoaded('lang')) {
            $this->load(['lang'=>function($querry){
                        return $querry->current();
                    }]);
        } elseif(count($this->lang)>1) {
            $this->relations['lang'] = $this->lang->where('language',App::getLocale());
        }

        if (is_null($this->lang)) { $this->load('lang'); }
            
        if ($this->lang && $this->lang->first()) { return $this->lang->first()->name; }
        
        return '-no block name-';
    }
    
    
    /**
     * Метод, accessor,
     * возвращает short description block на языке приложения||на первом найденном языке
     * @return string
     */
    public function getShortDAttribute() {
        
        if (!$this->relationLoaded('lang')) {
            $this->load(['lang'=>function($querry){
                        return $querry->current();
                    }]);
        } elseif(count($this->lang)>1) {
            $this->relations['lang'] = $this->lang->where('language',App::getLocale());
        }

        if (is_null($this->lang)) { $this->load('lang'); }
            
        if ($this->lang && $this->lang->first()) { return $this->lang->first()->shortd; }
        
        return '-no block shortD-';
    }
    
    /**
     * Метод, accessor,
     * возвращает description block на языке приложения||на первом найденном языке
     * @return string
     */
    public function getDescriptionAttribute() {
        
        if (!$this->relationLoaded('lang')) {
            $this->load(['lang'=>function($querry){
                        return $querry->current();
                    }]);
        } elseif(count($this->lang)>1) {
            $this->relations['lang'] = $this->lang->where('language',App::getLocale());
        }

        if (is_null($this->lang)) { $this->load('lang'); }
            
        if ($this->lang && $this->lang->first()) { return $this->lang->first()->description; }
        
        return '-no block description-';
    }
    
    
    
    
    
    
    
    /**
     * Связь для курсов в блоке
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courses(){
        return $this->hasMany(Curse::class,'sdo_courses_block_id','id');
    }

    /**
     * Метод, accessor,
     * возвращает количество курсов в блоке
     * @return string
     */
    public function getCoursesCountAttribute() {
        return $this->courses()->count();
    }
    

    /**
     * Связь для курсов в блоке
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function coursesFront(){
        return $this->hasMany(Curse::class,'sdo_courses_block_id','id')->activeFront();
    }
    
    
    
    /**
     * Метод, accessor,
     * возвращает название название block на языке приложения||на первом найденном языке
     * @return string
     */
    public function getImageAttribute() {
        
        
        return '-no image-';
    }
    
    /**
     * Заготовка запроса включенных Блоков курсов.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
      return $query->where('status', 1);
    }
    
    /**
     * Заготовка запроса Блоков курсов для отображения на фронте.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActiveFront($query)
    {
      return $query->where('front_status', 1);
    }
    
    /**
     * Метод, accessor,
     * возвращает description block для фронта
     * @return string
     */
    public function getFrontNameAttribute() {

        if (empty($this->ilink)) {
            return $this->getNameAttribute();
        }
        
        return $this->ilink;
    }
}

