<?php

namespace App\Models\Language;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $table='language_logo';

    protected $fillable=['name','image'];
}
