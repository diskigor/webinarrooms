<?php

namespace App\Models\Materials;

use Illuminate\Database\Eloquent\Model;

class Materials extends Model
{
    protected $table = "additional_materials";
    
    
    
    public function prelection() {
        return $this->belongsToMany(\App\Models\Prelections\Prelection::class, 'matterials_lectures', 'additional_material_id', 'lecture_id')->withPivot('sort', 'created_at');
        
    }
    
    public function getNameTypeAttribute() {
        $res='Текст';
        switch ($this->type){
            case'video_link':
                $res='Видео';
                break;
            case'pdf':
                $res='PDF';
                break;
            case'audio':
                $res='Аудио';
                break;
            case'archives':
                $res='Архив RAR,ZIP';
                break;
        }
        return $res;
    }
}
