<?php

namespace App\Models\Materials;

use Illuminate\Database\Eloquent\Model;

class MaterialsLectures extends Model
{
        protected $table="matterials_lectures";

        public function material(){
            return $this->belongsTo(Materials::class,'additional_material_id','id');
        }
}
