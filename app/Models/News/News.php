<?php

namespace App\Models\News;

use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Storage;

class News extends Model
{
    protected $table='news';

    /**
     * Связь для вывода новости на языке приложения
     * @return mixed
     */
    public function news_trans(){
        $local = App::getLocale();
        return $this->hasMany(NewsTrans::class)->where('lang_local',$local);
    }
    
    /**
     * Связь для вывода новости на языке приложения
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lang(){
        $local = App::getLocale();
        return $this->belongsTo(NewsTrans::class,'id','news_id')->where('lang_local',$local);
    }

    /**
     * Связь для редактирования новости
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function relationForEdit(){
        return $this->hasMany(NewsTrans::class);
    }
    
    
    /**
     * Атрибут картинка
     * @return boolean
     */
    public function getImageAttribute()
    {
        if(Storage::disk('public')->exists($this->image_link)) {
            return Storage::url($this->image_link);
        }
        
        if (file_exists(public_path($this->image_link))) { 
            return asset($this->image_link);
        }
        
        return asset('images/blog_pic_5.jpg');
    }
}
