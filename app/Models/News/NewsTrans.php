<?php

namespace App\Models\News;

use Illuminate\Database\Eloquent\Model;

class NewsTrans extends Model
{
    protected $table="news_trans";

    public function news(){
        return $this->belongsTo(News::class);
    }
}
