<?php

namespace App\Models\News;

use Illuminate\Support\Facades\Storage;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class VideoNews extends Model
{
    protected $table='sdo_video_news';
    
    protected $guarded = ['id'];
    
    /**
     * "Загружающий" метод модели.
     *
     * @return void
     */
    protected static function boot() {
        parent::boot();

        //всегда сортировать
        static::addGlobalScope(function (Builder $builder) {
            $builder->orderBy('created_at','desc');
        });
    }
    
    /**
     * Атрибут видео
     * @return boolean
     */
    public function getVideoAttribute()
    {
        if(Storage::disk('public')->exists($this->href)) {
            return Storage::url($this->href);
        }

        return asset('images/blog_pic_5.jpg');
    }
    
    /**
     * Заготовка активных
     * @param  object        $query
     * @return object        $query
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
}
