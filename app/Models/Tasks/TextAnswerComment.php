<?php

namespace App\Models\Tasks;

use App\Models\UserAnswer\TextAnswer;
use App\Models\Billing\Balls;
use App\User;
use Illuminate\Database\Eloquent\Model;

class TextAnswerComment extends Model
{
    protected $table="text_answer_comment";
    
    protected $guarded = ['id'];

    public function text_answer(){
        return $this->belongsTo(TextAnswer::class,'text_answer_id','id');
    }

    public function author_comment(){
        return $this->belongsTo(User::class,'user_id','id');
    }
    
    public function like_com(){
        return $this->hasOne(TextAnswerCommentLike::class,'text_answer_comment_id','id');
    }
    
    /**
     * Все балы за комментарии к тектовым заданиям
     * @return type
     */
    public function balls()
    {
      return $this->morphMany(Balls::class, 'balltable');
    }
}
