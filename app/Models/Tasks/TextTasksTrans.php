<?php

namespace App\Models\Tasks;

use Illuminate\Database\Eloquent\Model;

class TextTasksTrans extends Model
{
    protected $table= "sdo_text_tasks_trans";

    public function textTasks(){
        return $this->belongsTo(TextTasks::class,'sdo_text_task_id','id');
    }
}
