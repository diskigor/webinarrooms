<?php

namespace App\Models\Tasks;

use Illuminate\Database\Eloquent\Model;

class TestQuestion extends Model
{
    protected $table = 'sdo_test_questions';
    
    protected $guarded = ['id'];

    /**
     * Связь один ко многим с таблицей ответов на вопрос
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function testAnswerQuestion(){
        return $this->hasMany(TestAnswer::class,'test_question_id','id');
    }

    public function test(){
        return $this->belongsTo(Test::class,'test_id','id');
    }
}
