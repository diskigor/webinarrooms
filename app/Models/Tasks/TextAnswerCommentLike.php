<?php

namespace App\Models\Tasks;

use Illuminate\Database\Eloquent\Model;

class TextAnswerCommentLike extends Model
{
    protected $table = "text_answer_comment_like";
    
    protected $guarded = ['id'];

    public function comment(){
        return $this->belongsTo(TextAnswerComment::class,'text_answer_comment_id','id');
    }
}
