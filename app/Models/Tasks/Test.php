<?php

namespace App\Models\Tasks;

use App\Models\Prelections\Prelection;
use App\Models\UserAnswer\TestUserAnswer;
use App\Models\Billing\Balls;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class Test extends Model
{
    protected $table="sdo_test";

    protected $guarded = ['id'];

    /**
     * Связь с таблицей пользователей
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    /**
     * Связь с лекциями прилодения
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function prelection(){
        return $this->belongsTo(Prelection::class,'lectures_id','id');
    }

    /**
     * Связь с таблицей вопросов к тесту
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function testQuestions(){
        return $this->hasMany(TestQuestion::class,'test_id','id');
    }

    public function userTestAnswer(){
        return $this->hasOne(TestUserAnswer::class,'test_id','id')->where('user_id',Auth::user()->id);
    }

    public function typeTest(){
        return $this->belongsTo(TypeTest::class,'type_id','id');
    }
    
    /**
     * Все балы за тесты
     * @return type
     */
    public function balls()
    {
      return $this->morphMany(Balls::class, 'balltable');
    }
    
}
