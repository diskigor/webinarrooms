<?php

namespace App\Models\Tasks;

use App\Models\Prelections\Prelection;
use App\Models\UserAnswer\TextAnswer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class TextTasks extends Model
{
    protected $table = "sdo_text_tasks";
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
    
    public function textTasksTrans()
    {
        $local = App::getLocale();
        return $this->hasMany(TextTasksTrans::class, 'sdo_text_task_id', 'id')
            ->where('lang_local', $local)
            ->first();
    }
    
    /**
     * Связь для вывода текстового задания на языке приложения
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function lang(){
        $local = App::getLocale();
        return $this->belongsTo(TextTasksTrans::class,'id','sdo_text_task_id')->where('lang_local',$local);
        
    }

    /**
     * Метод, accessor,
     * возвращает название название текстового задания на языке приложения
     * @return string
     */
    public function getNameAttribute() {
        if (!$this->relationLoaded('lang')) { $this->load('lang'); } 
        if ($this->lang) { return $this->lang->name_trans; }
        return '';
    }
    
    /**
     * Метод, accessor,
     * возвращает нкраткое название текстового задания на языке приложения
     * @return string
     */
    public function getDescriptionAttribute() {
        if (!$this->relationLoaded('lang')) { $this->load('lang'); } 
        if ($this->lang) { return $this->lang->content_trans; }
        return '';
    }
    
    
    
    public function prelection()
    {
        return $this->belongsTo(Prelection::class, 'lecture_id', 'id');
    }

    public function getTextTasksTransEdit()
    {
        return $this->hasMany(TextTasksTrans::class, 'sdo_text_task_id', 'id');
    }

    public function answerUserText(){
        return $this->hasOne(TextAnswer::class,'task_id','id')->where('user_id',Auth::user()->id);
    }




}
