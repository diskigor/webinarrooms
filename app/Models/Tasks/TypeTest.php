<?php

namespace App\Models\Tasks;

use Illuminate\Database\Eloquent\Model;

class TypeTest extends Model
{
    protected $table = "sdo_test_type";
}
