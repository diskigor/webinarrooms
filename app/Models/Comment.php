<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

use App\Models\Blog\Blog;
use App\User;

class Comment extends Model
{
    protected $table='comments';
    
    protected $guarded = [];
	
    public function blog()
	{
            return $this->belongsTo(Blog::class);
	}
	
    public function user()
	{
            return $this->belongsTo(User::class);
	}
        
    public function likes()
    {
        return $this->morphToMany(User::class, 'likeable')->whereDeletedAt(null);
    }
    
    public function getIsLikedAttribute()
    {
        $like = $this->likes()->whereUserId(Auth::id())->first();
        return (!is_null($like)) ? true : false;
    }

    /**
     * @return bool
     */
    public function getIsTopAttribute()
    {
        return (!$this->parent_id) ? TRUE : FALSE;
    }
    

    /**
     * @return bool
     */
    public function getIsSetChildrenAttribute()
    {
        return (Comment::where('parent_id',  $this->id)->first()) ? TRUE : FALSE;
    }
    
    
    public function getAllCildrenComments($id = NULL) {
        if(!$id) { $id= $this->id; }
        
        $all = Comment::where('parent_id',  $id)->get();
        foreach ($all as $com) {
            $all = $all->merge($this->getAllCildrenComments($com->id));
        }
        return $all;
    }
}