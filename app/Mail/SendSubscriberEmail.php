<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use App\Models\Blog\Blog;
use App\Models\Subscribers\Subscribers;

class SendSubscriberEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var Subscribers 
     */
    protected $subscriber;
    
    /**
     * @var Blog 
     */
    protected $blog;

    
    /**
     * Конструктор
     * @param User $user
     * @param Blog $blog
     */
    public function __construct(Subscribers $subscriber, Blog $blog)
    {
        $this->subscriber = $subscriber;
        $this->blog = $blog;
        $this->subject('Новая статья блога "'.$blog->title.'"');
        $this->to($subscriber->email, $subscriber->UName);
    }

    /**
     * Строим письмо.
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.subscribers.sendBlog')->with([
                        'subscriber'    => $this->subscriber,
                        'blog'          => $this->blog,
                    ]);
    }
}
