<?php
//Route::group(
//    [
//        'prefix' => LaravelLocalization::setLocale(),
//        'middleware' => ['localeSessionRedirect', 'localizationRedirect']
//    ],
//    function () {

Auth::routes();
Route::get('/user/confirmation/{token}', 'Auth\RegisterController@confirmation')->name('confirmation');

Route::get('block_user/{message}', 'Admin\BlackList\blackListController@showFormBlock')->name('block_user');
Route::get('block_user_message', 'Admin\BlackList\blackListController@messageUser')->name('block_user_message');//TODO
//--------------------------------------Маршруты для пользователя пополнения------------------------------------
Route::get('int_success', 'Interkassa\addBalanceController@intSuccess')->name('int_success');
Route::get('int_fail', 'Interkassa\addBalanceController@intFail')->name('int_fail');
Route::get('int_waiting', 'Interkassa\addBalanceController@intWaiting')->name('int_waiting');


//--------------------- Вебинары -----------------------------------------------------------------------------
    Route::get('/catalog_webinars', 'Webinar\WebinarController@index')->name('catalog_webinars');//Каталог вебинаров
    Route::get('/poster_webinars', 'Webinar\WebinarController@poster_webinars')->name('poster_webinars');//Афиша вебинаров
    Route::get('/create_webinar', 'Webinar\WebinarController@create_webinar')->name('create_webinar');//Создать вебинар
    Route::post('/create_webinar', 'Webinar\WebinarController@add_webinar')->name('articleAdd');//Запись в базу вебинара
    Route::get('/update_poster', 'Webinar\WebinarController@posters')->name('update_poster');//Страница с информацией об афише для Админа
    Route::post('/update_poster', 'Webinar\WebinarController@update_poster')->name('updatePoster');//Обновления в базе информации
    Route::post('/update_poster', 'Webinar\WebinarController@update_poster_img')->name('updatePosterimg');//Обновления в базе картинки
    Route::get('/webinar/{id}', 'Webinar\WebinarController@show')->name('webinarShow');//Страница конкретного вебинара
    Route::get('/my_webinars', 'Webinar\WebinarController@my_webinars')->name('my_webinars');//Страница с списком всех вебинаров отдельного лектора
    Route::get('/my_webinar/{id}', 'Webinar\WebinarController@my_show')->name('MywebinarShow');//Страница конкретного вебинара лектора
    Route::get('/translete_webinar/{id}', 'Webinar\WebinarController@showTranslete')->name('webinarShowTranslete');//Страница конкретного вебинара трансляция
    Route::get('/admin_webinars', 'Webinar\WebinarController@admin_webinars')->name('admin_webinars');//Страница с списком всех вебинаров для админа
    Route::get('/admin_webinars_edit/{id}', 'Webinar\WebinarController@admin_webinars_edit')->name('EditWebinarAdmin');//Страница для редактирования конкретного вебинара админом
    Route::post('/admin_webinars_edit', 'Webinar\WebinarController@save_edit_admin')->name('SaveEdit');//Обновления в базе информации
    Route::get('/user_webinars_edit/{id}', 'Webinar\WebinarController@user_webinars_edit')->name('EditWebinarUser');//Страница для редактирования конкретного вебинара ректором
    Route::post('/user_webinars_edit', 'Webinar\WebinarController@save_edit_user')->name('SavedEditWebinar');//Обновления в базе информации
    Route::post('/webinar', 'Webinar\WebinarController@add_user_webinar')->name('AddUserWebinar');//Обновления в базе информации
    Route::post('/my_webinar', 'Webinar\WebinarController@add_webinar_arhiv')->name('AddWebinarArgiv');//Переносим вебинар в архив
    Route::get('/arhiv', 'Webinar\WebinarController@arhiv')->name('arhiv');//Архив

//-------------------------------------Маршруты фронта----------------------------------------------------------
//
//Route::get('/pageNotFound', 'Front\Home\homeController@notFound')->name('notfound');
//Route::get('/forbiddenPage', 'Front\Home\homeController@forbiddenPage')->name('forbiddenPage');
//Route::get('/service_temporarily', 'Front\Home\homeController@serviceTemporarilyserviceTemporarily')
//    ->name('service_temporarily');


Route::get('front_test', 'Front\Home\frontTestController@show')->name('front_test');
Route::post('front_test_answer', 'Front\Home\frontTestController@answer')->name('front_test_answer');
//----------------------------------------Временнная заглушка
Route::get('/', 'Front\Home\homeController@index')->name('main_front_page');
//Route::get('/', function () {
//    return view('stub');
//});

//-----------------Путь для теста почты
Route::get('test_email', 'Users\Courses\studiesController@testEmail');
//------------------------------
Route::get('show_preview_course/{slug}', 'Front\Home\homeController@show')->name('show_preview_course');
Route::get('show_all_courses', 'Front\Home\homeController@showAllCourses')->name('show_all_courses');
Route::resource('about', 'Front\About\aboutController');
Route::resource('contacts', 'Front\Contacts\contactsController');

Route::get('news_sdo', 'Front\News\newsFrontController@index')->name('news_sdo');
Route::get('show_news/{id}', 'Front\News\newsFrontController@show')->name('show_news');



Route::get('video_show/{id}', 'Front\News\newsFrontController@videoNews')->name('video_show');
Route::get('events_sdo', 'Front\Doings\eventsFrontController@index')->name('events_sdo');
Route::get('show_event/{slug}', 'Front\Doings\eventsFrontController@show')->name('show_event');

//статические страницы
Route::get('page/{slug}', 'Front\Home\staticPageController@show')->name('page');

Route::resource('all_archive', 'Admin\Front\Archive\archiveController');
Route::get('show_years_archive/{year}', 'Admin\Front\Archive\archiveController@getYearApi')
    ->name('show_years_archive');
Route::get('show_post/{id}', 'Admin\Front\Archive\archiveController@getApiPost')
    ->name('show_post');

//Рассылка в админке
Route::get('admin_all_archive/search', 'Admin\Archive\archiveAdminController@search')->name('admin_all_archive.search');
Route::get('admin_all_archive/first_show', 'Admin\Archive\archiveAdminController@index_first')->name('all_archive_first_show');
Route::resource('admin_all_archive', 'Admin\Archive\archiveAdminController');


Route::post('support_send_email', 'Front\Home\homeController@supportEmail')->name('support_send_email');
Route::post('question_user', 'Front\Home\homeController@askMail')->name('question_user');
//        Route::get('/home', 'HomeController@index');
        
//Маруты для админа сайта
Route::get('blog', 'Users\blogController@free_index')->name('front_blog_index');
Route::get('blog/{slug}', 'Users\blogController@free_show')->name('front_blog_show');
Route::get('blog_search', 'Users\blogController@search')->name('front_blog_search');

//Gilbo blog
Route::get('blogi_gilbo','Users\Blogs\gilboController@free_index')->name('front_blog_gilbo');
Route::get('blogi_gilbo/{slug}', 'Users\Blogs\gilboController@free_show')->name('front_blog_gilbo_show');
Route::get('blogi_gilbo_search', 'Users\Blogs\gilboController@search' )->name('front_blog_gilbo_search');

//Lecturer blog
Route::get('blogi_lecturer','Users\Blogs\lecturerController@free_index')->name('front_blog_lecturer');
Route::get('blogi_lecturer/{slug}', 'Users\Blogs\lecturerController@free_show')->name('front_blog_lecturer_show');
Route::get('blogi_lecturer_search', 'Users\Blogs\lecturerController@search' )->name('front_blog_lecturer_search');

//GayPark blog
Route::get('gaidpark','Users\Blogs\gayparkController@free_index')->name('front_blog_gaypark');
Route::get('gaidpark/{slug}', 'Users\Blogs\gayparkController@free_show')->name('front_blog_gaypark_show');
Route::get('gaidpark_search', 'Users\Blogs\gayparkController@search' )->name('front_blog_gaypark_search');

//конкретно для фронта
Route::post('add_new_subscribers','Admin\Subscribers\subscribersController@store')->name('add_new_subscribers');

Route::get('comments_load/{id_blog}', ['uses' => 'CommentController@comments_load', 'as' => 'comments_load']);

Route::group(['middleware' => 'admin', 'prefix' => 'admin'], function () {
    //--------------------- Личный кабинет -----------------------------------------------------------------------------
    
    Route::any('/', function () { return redirect()->route('/personal_area'); });//fix Рабочий кабинет
    Route::get('/personal_area', 'Admin\adminHomeController@index')->name('/personal_area');//Рабочий кабинет
    
    Route::post('change_profile_avatar/{id}', 'Admin\Users\Settings\profileSettingsController@changeAvatar')
        ->name('change_profile_avatar');
    Route::post('change_prof/{id}', 'Admin\Users\Settings\profileSettingsController@changeProf')
        ->name('change_prof');
    Route::post('change_user_data/{id}', 'Admin\Users\Settings\profileSettingsController@changeUserData')
        ->name('change_user_data');
    Route::post('change_user_email/{id}', 'Admin\Users\Settings\profileSettingsController@changeUserEmail')
        ->name('change_user_email');
    //--------------------------------DELETE AVATAR---------------------------------------------------------------------
    Route::post('delete_avatar', 'Admin\Users\Settings\profileSettingsController@deleteAvatar')->name('delete_avatar');
    //------------------------------------------------------------------------------------------------------------------
    Route::resource('connection_mentors', 'Admin\Educational\connectionController');
    Route::get('add_connect_mentor/{id}', 'Admin\Educational\connectionController@addConnection')
        ->name('add_connect_mentor');
    Route::get('show_mail', 'Admin\Users\Chats\chatController@index')->name('show_mail');
    Route::get('send_mail_user', 'Admin\Users\Chats\chatController@sendMails')->name('send_mail_user');
    Route::get('more_mail/{id}', 'Admin\Users\Chats\chatController@show')->name('more_mail');
    Route::post('answer_users', 'Admin\Users\Chats\chatController@store')->name('answer_users');
    
    Route::resource('user_callback_chats', 'Admin\Users\Chats\chatController');//общение с администратором
    
//    Route::get('create_mail_user', 'Admin\Users\Chats\chatController@create')->name('create_mail_user');


    //----------------------------Balance-------------------------------------------------------------------------------
    Route::get('get_add_balance_form','Admin\Users\Settings\balanceController@getpayform')->name('get_add_balance_form');
    
    Route::get('get_history_user_transaction','Admin\Users\Settings\balanceController@getHistoryTransaction')->name('get_history_user_transaction');
    Route::get('get_history_user_balls','Admin\Users\Settings\balanceController@getHistoryBalls')->name('get_history_user_balls');
    
    //Route::post('orders_s_anydata/export','Admin\Statistics\ordersController@anyData');
    
    Route::resource('mybalance', 'Admin\Users\Settings\balanceController');
    
    //---------------------------------Рефералка------------------------------------------------------------------------
    Route::resource('refsystem', 'Admin\Referals\referalController');
    Route::resource('refrules', 'Admin\Referals\referalRulesController');
    Route::resource('refhistory', 'Admin\Referals\referalHistoryController');
    
    //------------------------------------------------------------------------------------------------------------------
    Route::get('get_all_api', 'Api\apiMediascanController@getNewID');
    // -------------------- Все пути  Админа ---------------------------------------------------------------------------
    
    //
    Route::resource('all_admin_referalsActivity', 'Admin\PersonaArea\referalsAdminActivityController');
    Route::resource('all_admin_financeActivity', 'Admin\PersonaArea\financeAdminActivityController');
    
    
    Route::resource('all_lecturer_financeActivity', 'Admin\PersonaArea\financeLecturerActivityController');
    
    //Управление кадетами
    Route::resource('cadet', 'Admin\Users\Cadet\cadetController'); 
    
    Route::resource('curator', 'Admin\Users\Curator\curatorController');                                               
                                                         
    Route::resource('lectors', 'Admin\Users\Lector\lectorController');
    Route::resource('curses', 'Admin\Curses\cursesController');
    Route::resource('language', 'Admin\Language\languageController');
    
    Route::resource('message', 'Admin\Message\messageController');
    Route::post('message_admin', 'Admin\Message\messageController@messageadmin')->name('message_admin');
    Route::post('delete_multi_message', 'Admin\Message\messageController@delete_multi_message')->name('delete_multi_message');
    
    Route::resource('profile', 'Admin\Users\Settings\profileSettingsController');
    Route::resource('mentor_settings', 'Admin\Educational\mentorsSettings');
    Route::resource('group_user', 'Admin\Users\Settings\userGroupController');
    Route::resource('roles_user', 'Admin\Users\Settings\userRolesController');
       
    //-------------------------------------------- блоки курсов * новое
    Route::post('add_course_to_block/{block_id}','Admin\Curses\Block\blockController@add_course_to_block')->name('add_course_to_block');
    Route::post('del_course_to_block/{block_id}','Admin\Curses\Block\blockController@del_course_to_block')->name('del_course_to_block');
    Route::resource('admin_block', 'Admin\Curses\Block\blockController', ['except' => [ 'store']]);
    
    
    Route::resource('ablog', 'Admin\Blog\blogController');
    Route::resource('atags', 'Admin\Tag\tagController');
    
    Route::post('blog/like/{id}', ['as' => 'blog.like', 'uses' => 'LikeController@likeBlog']);
    
    //Gilbo blog
    Route::get('blog_gilbo/search', ['as' => 'ablog_gilbo.search', 'uses' => 'Users\Blogs\gilboController@search']  );
    Route::resource('blog_gilbo','Users\Blogs\gilboController');
    
    //Lecturer blog
    Route::get('blog_lecturer/search', ['as' => 'ablog_lecturer.search', 'uses' => 'Users\Blogs\lecturerController@search']  );
    Route::resource('blog_lecturer','Users\Blogs\lecturerController');
    
    //Gaypark blog
    Route::get('blog_gaidpark/search', ['as' => 'ablog_gaypark.search', 'uses' => 'Users\Blogs\gayparkController@search']  );
    Route::resource('blog_gaidpark','Users\Blogs\gayparkController');
    
    //MAIN USER BLOG controller
    Route::get('blog/search', ['as' => 'ablog.search', 'uses' => 'Users\blogController@search']  );
    Route::resource('blog', 'Users\blogController');
    
    Route::post('comment/like/{id}', ['as' => 'comment.like', 'uses' => 'LikeController@likeComment']);
    Route::get('comment_list/{id_blog}', ['uses' => 'CommentController@index', 'as' => 'comment_list']);
    //
    Route::post('comment_delete/{id_blog}/delete/{id_comment}', ['uses' => 'CommentController@destroy', 'as' => 'comment_delete']);
    Route::post('comment', ['uses' => 'CommentController@store', 'as' => 'comment']);
    
    //лекции для админа и лектора
    Route::resource('prelection', 'Admin\Prelection\prelectionController');
    Route::post('multiple_delete_prelection', 'Admin\Prelection\prelectionController@multipleDelete')
        ->name('multiple_delete_prelection');
    Route::post('prelection_destr/{id}','Admin\Prelection\prelectionController@destroy')
        ->name('prelection_destr');
    //--------------------------Новости---------------------------------------------------------------------------------
    Route::resource('news', 'Admin\News\newsController');
    Route::resource('video_news', 'Admin\News\videoNewsController');
    Route::resource('video_instruction', 'Admin\News\videoInstructionController');
    
    
    // новостные статьи для авторизированных пользователей
    Route::get('unewsv/{id}','Users\newsController@videoNews')->name('unewsv');
    Route::resource('unews','Users\newsController', ['only' => ['index','show']]);
    
    //------------------------------------------------------------------------------------------------------------------
    //-------------------------------Меропреятия и семинары ------------------------------------------------------------
    Route::resource('admin_events', 'Admin\Doings\eventsController');
    //------------------------------------------------------------------------------------------------------------------
    
    //Billing statistic
    Route::get('biling_statistics/getAjaxData','Admin\Statistics\bilingController@getAjaxData')->name('biling_statistics.getAjaxData');
    Route::post('biling_statistics/getAjaxData/export','Admin\Statistics\bilingController@getAjaxData');
    Route::resource('biling_statistics', 'Admin\Statistics\bilingController');

        Route::get('admin_repl_statistics', 'Admin\Statistics\bilingController@adminReplStatistics')
        ->name('admin_repl_statistics');

    Route::post('search_email_repl_admin','Admin\Statistics\bilingController@emailSearchUser')
        ->name('search_email_repl_admin');

    Route::post('del_order_admin_prel/{id}','Admin\Statistics\bilingController@delReplOrder')->name('del_order_admin_prel');
    
    
    //Order Statistic
    
    Route::resource('orders_statistics', 'Admin\Statistics\ordersController');

    Route::get('orders_s_anydata','Admin\Statistics\ordersController@anyData')->name('orders_s_anydata');
    Route::post('orders_s_anydata/export','Admin\Statistics\ordersController@anyData');
    
    Route::post('search_email_orders_admin','Admin\Statistics\ordersController@emailSearchUser')
        ->name('search_email_orders_admin');
    
    
    //balls statistic
    Route::get('balls_statistics/getAjaxData','Admin\Statistics\ballsController@getAjaxData')->name('balls_statistics.getAjaxData');
    Route::post('balls_statistics/getAjaxData/export','Admin\Statistics\ballsController@getAjaxData');
    Route::resource('balls_statistics', 'Admin\Statistics\ballsController');
    
    //CONVERTERS
    Route::post('form_converter_ball_to_kell','Users\Converters\ballToKelController@forInclude')->name('form_converter_ball_to_kell');
    
    
    
    //учебная аудитория
    Route::resource('educational_audience', 'Users\Educational\audienceController');
    
    //каталог курсов
    Route::resource('lectures_catalog', 'Users\Cataloglectures\cataloglecturesController');
    
    //вебинарная комната
    Route::resource('webinars_room', 'Users\Webinars\webinarsController');
    //семинары
    Route::resource('advice_room', 'Users\Advice\adviceController');
    //faq
    Route::resource('faq', 'Users\Faq\faqController');

//    Route::resource('reference_statistics', 'Admin\Statistics\referenceStatisticsController');
//    Route::post('search_ref_orders_admin','Admin\Statistics\referenceStatisticsController@emailSearchUser')
//        ->name('search_ref_orders_admin');

    
    //Работаем с подписчиками ----------------------------------------------------------------------------------------
        //проверить наличие подписчика по email
        Route::post('subscribers/check/{mail}', 'Admin\Subscribers\subscribersController@check')->name('subscribers.check');
//        //сменить статус подписки
//        Route::post('subscribers/change_status/{id}', 'Admin\Subscribers\subscribersController@changeStatus')->name('subscribers.change_status');
        //SubscibersDataTable
        Route::get('subscribers/getAjaxData','Admin\Subscribers\subscribersController@getAjaxData')->name('subscribers.getAjaxData');
        Route::post('subscribers/getAjaxData/export','Admin\Subscribers\subscribersController@getAjaxData');
        //основной роутер
        Route::resource('subscribers', 'Admin\Subscribers\subscribersController');
    
    
    //----------------------Test----------------------------------------------------------------------------------------
    Route::resource('test', 'Admin\Tasks\testController');
    Route::get('fill_test/{id}', 'Admin\Tasks\testController@fillTest')->name('fill_test');
    Route::get('delete_question/{id}', 'Admin\Tasks\testController@deleteQuestion')->name('delete_question');
    Route::post('add_question_answer', 'Admin\Tasks\testController@addQuestAnswer')->name('add_question_answer');
    
    Route::post('update_question_answer', 'Admin\Tasks\testController@updateQuestAnswer')->name('update_question_answer');
    
    
    Route::post('answer_test_task/{id}', 'Users\Courses\studiesController@addAnswerTestTaskUser')
        ->name('answer_test_task');
    Route::post('test_concat_answer/{id}', 'Users\Courses\studiesController@addAnswerConcatTest')
        ->name('test_concat_answer');
    Route::post('test_group_answer/{id}', 'Users\Courses\studiesController@addAnswerGroupTest')
        ->name('test_group_answer');
    
    
    
    //текстовые задания - админка
    Route::resource('text_assignments', 'Admin\Tasks\textAssignmentsController');
    
    
    
    Route::get('explanation_list/{id}', 'Admin\Tasks\explanationController@index')->name('explanation_list');
    Route::post('explanation_add/{id}', 'Admin\Tasks\explanationController@store')->name('explanation_add');
    Route::get('explanation_delete/{id}', 'Admin\Tasks\explanationController@destroy')->name('explanation_delete');
    //--------------------Route Add Materials---------------------------------------------------------------------------
    Route::resource('materials', 'Admin\AddMaterial\addMaterialController');
    Route::resource('text_materials', 'Admin\AddMaterial\addTextMaterialController');
    Route::resource('file_materials', 'Admin\AddMaterial\addFileMaterialController');
    Route::get('get_file/{id}', 'Admin\AddMaterial\addFileMaterialController@getFile')->name('getFile');
    //------------------------------------------------------------------------------------------------------------------
    //------------------------Prelection Materials----------------------------------------------------------------------
    Route::post('prelection_material/{id}', 'Admin\Prelection\prelectionController@prelectionMaterial')
        ->name('prelection_material');
    Route::get('delete_prelection_materail/{id}',
        'Admin\Prelection\prelectionController@deletePrelectionMaterials')
        ->name('delete_prelection_materail');
    //------------------------------------------------------------------------------------------------------------------
    //
    //------------------------Route For Cadet and Curator --------------------------------------------------------------
    Route::get('partner_curses', 'Users\Courses\userCourseController@indexpartners')->name('partner_curses');
    
    Route::resource('user_curses', 'Users\Courses\userCourseController');                                               //Вывод курсов для покупки кадетами и кураторами
    
    //---------------------------------------Покупка курсов-----------------------------------------------------
    Route::get('buy_curses/{id}', 'Users\Courses\userCourseController@buyCourse')->name('buy_curses');
    Route::post('buyBlockCourses/{id}', 'Users\Courses\userCourseController@buyBlockCourses')->name('buyBlockCourses');
    Route::post('buyAllCourses', 'Users\Courses\userCourseController@buyAllCourses')->name('buyAllCourses');
    
    
    //Путь для вывода статистики пользователю
    Route::resource('user_statistics', 'Users\StatisticsUser\ordersUserController');  
    
    Route::get('replenishment_user_statistic','Users\StatisticsUser\ordersUserController@replenishmentUserStatistic')
        ->name('replenishment_user_statistic');
//    Route::get('ref_user_statistics','Users\StatisticsUser\ordersUserController@refUserStat')->name('ref_user_statistics');
    
    //------------------------------------------------------------------------------------------------------------------
    //Путь отвечающий за вывод всех купленых курсов и стадию учебы
    Route::get('learn_prelection/{id}', 'Users\Courses\studiesController@showLecture')
        ->name('studies.learn_prelection');
    Route::resource('studies', 'Users\Courses\studiesController');                                                      

    //Рефакторинг обучения по курсам
        //получить список дополнительных материалов для лекции
        Route::get('get_studies_materials/{id}','Users\Courses\studiesMaterialsController@show')->name('get_studies_materials');
        //получить текстовые задания для лекции
        Route::resource('studies_texts','Users\Courses\studiesTextController');
        //получить ТЕСТЫ для лекции
        Route::resource('studies_tests','Users\Courses\studiesTestController');
        
    Route::post('open_lection_from_balls', 'Users\Courses\mainStudiesController@openLectionFromBalls')->name('openLectionFromBalls');    
    Route::resource('main_studies', 'Users\Courses\mainStudiesController'); 
    
    Route::resource('free_studies', 'Users\Courses\freeStudiesController'); 
    
    Route::resource('lecture_studies', 'Users\Courses\lectureStudiesController'); 
    
    
    
    
    Route::get('finish_prelection/{id}', 'Users\Courses\studiesController@finishPrelection')
        ->name('finish_prelection');
    
    
    Route::get('get_key', 'Admin\Users\Settings\profileSettingsController@getHashKey')
        ->name('get_key');
    
    //-------- мои работы
    Route::resource('my_works', 'Admin\Users\Settings\worksController');
    
//    Route::get('show_my_work/{id}', 'Admin\Users\Settings\worksController@show')
//        ->name('show_my_work');
//    
//    Route::get('chose_interesting_comment/{id}', 'Admin\Users\Settings\worksController@add_like')
//        ->name('chose_interesting_comment');
    
    //------------- Бесплатные материалы
    Route::get('free_materials/{slug}/prelection/{id}', 'Admin\Curses\freeMaterialController@showFreeLecture')
        ->name('free_materials.prelection');
    Route::resource('free_materials', 'Admin\Curses\freeMaterialController');//открытые материалы
    
//    Route::get('free_show_prelection/{id}', 'Admin\Curses\freeMaterialController@showFreeLecture')
//        ->name('free_show_prelection');
    
    
    Route::get('instruction', function () { return view('Admin.instruction'); })->name('instruction');
    //----------------------------Пути для автокомплита при поиске по пользователям-----------------------------
    //TODO
    Route::get('search/{role_id}', 'Admin\Search\searchController@search');
    Route::get('searchEmail/{role_id}', 'Admin\Search\searchController@searchEmail');
    Route::get('search_email_billig', 'Admin\Search\searchController@billingEmailSearch')->name('search_email_billig');
    Route::get('search_inv_billig', 'Admin\Search\searchController@billingInvSearch')->name('search_inv_billig');
    Route::get('search_users', 'Admin\Search\searchController@users')->name('search_users');
    //--------------------------------Пути домашних заданий-----------------------------------------------------
    Route::post('answer_text_tasks/{id}', 'Users\Courses\studiesController@addAnswerTextTaskUser')
        ->name('answer_text_tasks');
    Route::post('edit_answer/{id}', 'Users\Courses\studiesController@editAnswerTextTaskUser')
        ->name('edit_answer');
    
    // работы курсантов, проверка тестовых ответов
    Route::resource('checking_text_tasks', 'Admin\CheckingTasks\textCheckingController');
    Route::get('add_favorit/{id}', 'Admin\CheckingTasks\textCheckingController@addFavorite')
        ->name('add_favorit');
    Route::get('delete_favorite/{id}', 'Admin\CheckingTasks\textCheckingController@deleteFavorite')
        ->name('delete_favorite');
    Route::get('favorite_list', 'Admin\CheckingTasks\textCheckingController@listFavorite')
        ->name('favorite_list');
    Route::get('show_favorite_work/{id}', 'Admin\CheckingTasks\textCheckingController@workFavorite')
        ->name('show_favorite_work');
    Route::post('change_status_text_answer/{id}', 'Admin\CheckingTasks\textCheckingController@changeStatusAnswer')
        ->name('change_status_text_answer');
    
    

    //----------------------------------------------------------------------------------------------------------
    //----------------------------------TEXT ANSWER COMMENT AND LIKE--------------------------------------------
    Route::post('add_comment_text_answer/{id}', 'Admin\Tasks\textAnswerController@store')
        ->name('add_comment_text_answer');
    //----------------------------------------------------------------------------------------------------------
    //-------------------------------Callback-------------------------------------------------------------------
    Route::resource('callback', 'Admin\Callback\callbackController');
    //----------------------------------------------------------------------------------------------------------
    //-------------------------------Balck List ----------------------------------------------------------------
    Route::resource('black_list', 'Admin\BlackList\blackListController');
    //----------------------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------------------
    Route::get('collect_bonuses_lecture', 'Admin\Statistics\ordersController@collectBonuses')
        ->name('collect_bonuses_lecture');
    
    
    // ---------------------------------- Пути для изменения данных фронта--------------------------------------
    //статические страницы
    Route::resource('static_pages', 'Front\Home\staticPageController');
    Route::get('page_admin/{slug}', 'Front\Home\staticPageController@showPersonalArea')->name('page_admin');
    
    
    Route::resource('admin_about_us', 'Admin\Front\AboutUs\aboutController');
    Route::resource('admin_contacts', 'Admin\Front\Contacts\contactsController');
    Route::resource('else_project', 'Admin\Front\Project\projectController');//проекты
    Route::resource('advantages', 'Admin\Front\AboutUs\advantagesController');

    //----------------------------------------------------------------------------------------------------------
});
//Route::get('/answer_add_balance/success', 'Admin\Users\Settings\balanceController@getAnswerSuccess');
//    });
Route::get('/ajax-course', 'Admin\Tasks\textAssignmentsController@getPrelection');
Route::match(['get', 'post'], 'interkassa', 'Interkassa\InterkassaController@api')->name('interkassa');

Route::get('/logout', 'Auth\LoginController@logout')->name('logout' );