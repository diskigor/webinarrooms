<?php

use Illuminate\Database\Seeder;

class ReferalRulesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('ref_rules')->truncate();


        DB::table('ref_rules')->updateOrInsert(['id'=>1],[
            'id'                => 1,
            'status'            => 1,
            'description'       => 'Первый уровень',
            'level'             => 1,
            'onlyclub'          => 0,
            'type'              => 'procent',
            'value'             => 5,
        ]);
        
        DB::table('ref_rules')->updateOrInsert(['id'=>2],[
            'id'                => 2,
            'status'            => 1,
            'description'       => 'Второй уровень',
            'level'             => 2,
            'onlyclub'          => 0,
            'type'              => 'procent',
            'value'             => 2,
        ]);
        
        DB::table('ref_rules')->updateOrInsert(['id'=>3],[
            'id'                => 3,
            'status'            => 1,
            'description'       => 'Третий уровень',
            'level'             => 3,
            'onlyclub'          => 0,
            'type'              => 'procent',
            'value'             => 0.5,
        ]);
        
        DB::table('ref_rules')->updateOrInsert(['id'=>4],[
            'id'                => 4,
            'status'            => 0,
            'description'       => 'Уровень XXX',
            'level'             => 4,
            'onlyclub'          => 0,
            'type'              => 'fixed',
            'value'             => 1,
        ]);
    }
}

/**
        DB::table('balance_operations')->updateOrInsert(['id'=>3],[
            'id'        => 3,
            'code'      => 'donate',
            'temp_name' => 'Поддержка проекта без награды'
        ]);
 * 
 *          $table->boolean('status')->default(1);
            $table->string('description')->nullable()->default('add description');
            $table->integer('level')->default(1)->index('level');
            $table->boolean('onlyclub')->default(0);
            $table->string('type', 20)->nullable()->default('procent');//procent || fixed
            $table->float('value', 8, 2)->default(0);
 */