<?php

use Illuminate\Database\Seeder;

class ResetUserBuy extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        //курсы
        DB::table('users_curses')->truncate();
        DB::table('sdo_progress')->truncate();
        
        //текстовые ответы пользователей при прохождении лекций, лайки и комменты
        DB::table('text_answer_comment_like')->truncate();
        DB::table('text_answer_comment')->delete();
        DB::table('sdo_text_user_answer')->delete();
        
        //ответы на тесты
        DB::table('sdo_test_user_answer')->truncate();
            
        //реферальная статистика
        DB::table('ref_history')->truncate();
        
        //покупки курсов
        DB::table('sdo_orders')->truncate();
        
        //финансовый учёт
        DB::table('sdo_billing_orders')->delete();
        
        
        //обнуляем баллы и балансы пользователей
        DB::table('users')->update([
            'balance'   => 0,
            'first_contribution'   => 0,
            'level_point'   => 0
            ]);
    }
    
    
    /*
     * DB::table('users_deposit')->delete();
     *  DB::table('users_curses')->truncate();
     *         DB::table('project_types')->updateOrInsert(['id'=>2],[
            'id'        => 2,
            'code'      => 'timeout'
        ]);
     */
}
