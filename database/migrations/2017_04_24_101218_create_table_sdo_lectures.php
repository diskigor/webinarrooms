<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSdoLectures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sdo_lectures', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('curse_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->string('image');
            $table->integer('number');
            $table->integer('price');
            $table->foreign('curse_id')
                ->references('id')
                ->on('sdo_curses')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('sdo_lectures_trans',function (Blueprint $table){
            $table->increments('id');
            $table->integer('sdo_lectures_id')->unsigned()->index();
            $table->string('slug');
            $table->string('name_trans');
            $table->text('short_desc ');
            $table->string('lang_local');
            $table->text('content_trans');
            $table->foreign('sdo_lectures_id')
                ->references('id')
                ->on('sdo_lectures')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sdo_lectures');
        Schema::dropIfExists('sdo_lectures_trans');
    }
}
