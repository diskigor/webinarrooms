<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesBlockSystem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sdo_courses_block', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable()->unsigned()->index('user');
            
            $table->boolean('status')->default(1)->index('status'); 
            $table->string('code')->nullable()->index('code');
            $table->string('ilink')->nullable()->index('ilink');
            $table->integer('order_block')->default(1)->index();
            $table->timestamps();
            
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });	
            
        Schema::create('sdo_courses_block_lang', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sdo_courses_block_id')->nullable()->unsigned()->index('up_id');
            $table->char('language')->default('ru')->index('language');
            
            $table->string('name')->nullable();
            $table->string('shortd')->nullable();
            $table->text('description')->nullable();
            
            $table->timestamps();

            $table->index(['sdo_courses_block_id', 'language']);
            
            $table->foreign('sdo_courses_block_id')
                    ->references('id')
                    ->on('sdo_courses_block')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
        
        
        //Расширяем таблицу курсов, для инмплементации привязки к блокам
        Schema::table('sdo_curses', function (Blueprint $table) {
            $table->integer('sdo_courses_block_id')->nullable()->unsigned()->index();
            $table->integer('sdo_courses_block_ordred')->default(1)->index('sdo_courses_block_ordred');
            
            $table->foreign('sdo_courses_block_id')
                ->references('id')
                ->on('sdo_courses_block')
                ->onUpdate('cascade')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('sdo_curses', function (Blueprint $table) {
            $table->dropForeign(['sdo_courses_block_id']);
            $table->dropColumn('sdo_courses_block_id');
            $table->dropColumn('sdo_courses_block_ordred');
        });
        
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('sdo_courses_block_lang');
        Schema::dropIfExists('sdo_courses_block');
        
    }
}
