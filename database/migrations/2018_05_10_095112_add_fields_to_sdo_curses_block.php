<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToSdoCursesBlock extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Расширяем таблицу блоков курсов
        Schema::table('sdo_courses_block', function (Blueprint $table) {
            $table->integer('front_status')->nullable()->default(0);
            $table->integer('front_hight')->nullable()->default(0);
            $table->integer('discount')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Урезаем таблицу блоков курсов
        Schema::table('sdo_courses_block', function (Blueprint $table) {
            $table->dropColumn('front_status');
            $table->dropColumn('front_hight');
            $table->dropColumn('discount');
        });
    }
}
