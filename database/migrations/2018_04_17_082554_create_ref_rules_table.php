<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_rules', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('status')->default(1);
            $table->string('description')->nullable()->default('add description');
            $table->integer('level')->default(1)->index('level');
            $table->boolean('onlyclub')->default(0);
            $table->string('type', 20)->nullable()->default('procent');//procent || fixed
            $table->float('value', 8, 2)->default(0);
            
            $table->index(['status', 'level', 'onlyclub']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_rules');
    }
}
