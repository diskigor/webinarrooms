<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsProgressToSdoProgress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Расширяем таблицу пользователя
        Schema::table('sdo_progress', function (Blueprint $table) {
            $table->text('progress')->nullable();
            $table->text('full_steps')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Расширяем таблицу пользователя
        Schema::table('sdo_progress', function (Blueprint $table) {
            $table->dropColumn('progress');
            $table->dropColumn('full_steps');
        });
    }
}
