<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldForSupportCarts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        
        //Расширяем таблицу переписки пользователей
        Schema::table('sdo_chats', function (Blueprint $table) {
            $table->index('recipient_id', 'my_recipient_id');
            $table->index('sender_id', 'my_sender_id');
            
            $table->integer('ma_id')->unsigned()->nullable()->index('ma_id');
            
            $table->foreign('ma_id')
                  ->references('id')->on('message_admin')
                  ->onDelete('cascade');
        });
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Расширяем таблицу переписки пользователей
        Schema::table('sdo_chats', function (Blueprint $table) {
            $table->dropForeign(['ma_id']);
            $table->dropColumn('ma_id');
        });
    }
}
