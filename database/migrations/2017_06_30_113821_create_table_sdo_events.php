<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSdoEvents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sdo_events',function (Blueprint $table){
            $table->increments('id');
            $table->string('image_link');
            $table->timestamps();
        });
        Schema::create('sdo_events_trans',function (Blueprint $table){
            $table->increments('id');
            $table->integer('sdo_events_id')->unsigned()->index();
            $table->string('name');
            $table->text('text');
            $table->string('lang_local');
            $table->string('slug')->unique();
            $table->foreign('sdo_events_id')
                ->references('id')
                ->on('sdo_events')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sdo_events');
        Schema::dropIfExists('sdo_events_trans');
    }
}
