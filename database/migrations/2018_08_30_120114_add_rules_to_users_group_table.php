<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRulesToUsersGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Расширяем таблицу blog
        Schema::table('users_group', function (Blueprint $table) {
            $table->integer('rule_id')->default(0)->index('rule_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Схлопываем таблицу blog
        Schema::table('users_group', function (Blueprint $table) {
            $table->dropColumn('rule_id');
        });
    }
}
