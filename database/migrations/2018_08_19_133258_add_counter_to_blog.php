<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCounterToBlog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Расширяем таблицу blog
        Schema::table('blog', function (Blueprint $table) {
            $table->integer('count_view')->default(0)->index('count_view');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Схлопываем таблицу blog
        Schema::table('blog', function (Blueprint $table) {
            $table->dropColumn('count_view');
        });
    }
}
