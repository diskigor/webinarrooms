<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSdoTextTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sdo_text_tasks',function (Blueprint $table){
            $table->increments('id');
            $table->integer('lecture_id')->unsigned()->index();
            $table->foreign('lecture_id')
            ->references('id')
            ->on('sdo_lectures')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('sdo_text_user_answer',function (Blueprint $table){
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('task_id')->unsigned()->index();
            $table->boolean('status',2)->default(0);
            $table->integer('check_user')->nullable();
            $table->integer('price')->nullabe();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->foreign('task_id')
            ->references('id')
                ->on('sdo_text_tasks')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('sdo_text_tasks_trans',function (Blueprint $table){
            $table->increments('id');
            $table->integer('sdo_text_task_id')->unsigned()->index();
            $table->string('name_trans');
            $table->text('content_trans');
            $table->string('lang_local');
            $table->foreign('sdo_text_task_id')
                ->references('id')
                ->on('sdo_text_tasks')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sdo_text_tasks');
        Schema::dropIfExists('sdo_text_user_answer');
        Schema::dropIfExists('sdo_text_tasks_trans');
    }
}
