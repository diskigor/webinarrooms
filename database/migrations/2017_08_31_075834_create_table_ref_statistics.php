<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRefStatistics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('ref_statistics',function (Blueprint $table){
            $table->increments('id');
            $table->integer('user_give_id')->unsigned()->index();
            $table->integer('user_use_id')->unsigned()->index();
            $table->integer('ref_sum');
           $table->foreign('user_give_id')
               ->references('id')
               ->on('users')
               ->onUpdate('cascade')
               ->onDelete('cascade');
           $table->foreign('user_use_id')
               ->references('id')
               ->on('users')
               ->onUpdate('cascade')
               ->onDelete('cascade');
            $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('ref_statistics');
    }
}
