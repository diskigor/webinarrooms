<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToSdoCurses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Расширяем таблицу курсов
        Schema::table('sdo_curses', function (Blueprint $table) {
            $table->integer('ordered')->default(0)->index('ordered');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Расширяем таблицу курсов
        Schema::table('sdo_curses', function (Blueprint $table) {
            $table->dropColumn('ordered');
        });
    }
}
