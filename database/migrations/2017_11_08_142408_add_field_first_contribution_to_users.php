<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldFirstContributionToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Расширяем таблицу пользователя
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('first_contribution')->default(0)->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Расширяем таблицу пользователя
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('first_contribution');
        });
    }
}
