<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldForDependenciesBayCourse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Расширяем таблицу курсов
        Schema::table('sdo_curses', function (Blueprint $table) {
            $table->integer('parent_buy_id')->unsigned()->nullable()->index('parent_buy_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Расширяем таблицу курсов
        Schema::table('sdo_curses', function (Blueprint $table) {
            $table->dropColumn('parent_buy_id');
        });
    }
}
