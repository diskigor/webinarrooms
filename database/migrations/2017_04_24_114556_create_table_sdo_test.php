<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSdoTest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sdo_test_type',function (Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->timestamps();
        });

        Schema::create('sdo_test', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->integer('point');
            $table->string('lang_local');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('lectures_id')->unsigned()->index();
            $table->integer('type_id')->unsigned()->index();
            $table->foreign('type_id')
                ->references('id')
                ->on('sdo_test_type')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('lectures_id')
                ->references('id')
                ->on('sdo_lectures')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('sdo_test_questions',function (Blueprint $table){
            $table->increments('id');
            $table->integer('group_number')->default(0);
            $table->integer('test_id')->unsigned()->index();
            $table->text('question');
            $table->integer('number')->default(0);
            $table->foreign('test_id')
                ->references('id')
                ->on('sdo_test')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('sdo_test_answers',function (Blueprint $table){
                $table->increments('id');
                $table->integer('test_question_id')->unsigned()->index();
                $table->integer('number');
                $table->text('answer');
                $table->integer('is_right');
                $table->foreign('test_question_id')
                    ->references('id')
                    ->on('sdo_test_questions')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
                $table->timestamps();
        });

        Schema::create('explanation',function (Blueprint $table){
            $table->increments('id');
            $table->integer('test_id')->unsigned()->index();
            $table->text('key_test');
            $table->string('alias')->nullable();
            $table->text('description')->nullable();
            $table->foreign('test_id')
                ->references('id')
                ->on('sdo_test')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('sdo_test_user_answer',function (Blueprint $table){
            $table->increments('id');
            $table->text('answer')->nullable();
            $table->integer('user_id')->unsigned()->index();
            $table->integer('test_id')->unsigned()->index();
            $table->integer('prelection_id')->unsigned()->index();
            $table->boolean('status')->default(0);
            $table->foreign('prelection_id')
                ->references('id')
                ->on('sdo_lectures')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('test_id')
                ->references('id')
                ->on('sdo_test')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sdo_test_type');
        Schema::dropIfExists('sdo_test');
        Schema::dropIfExists('sdo_tests_question');
        Schema::dropIfExists('sdo_tests_answer');
        Schema::dropIfExitst('explanation');
        Schema::dropIfExists('sdo_test_user_answer');
    }
}
