<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSdoBallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sdo_balls', function (Blueprint $table) {
            $table->increments('id');
            
            $table->boolean('status')->default(1)->index('status');
            $table->integer('type')->unsigned()->default(1)->index('type');
            
            $table->integer('user_id')->nullable()->unsigned()->index('user');
            $table->float('value', 8, 2)->default(0);
            
            $table->string('description')->nullable()->default('-- no description --');
            
            
            $table->nullableMorphs('balltable');
            
            $table->timestamps();
            
            $table->index(['status', 'user_id']);
            $table->index(['type', 'user_id']);
            $table->index(['status', 'type', 'user_id']);

            
            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('set null');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sdo_balls');
    }
}
