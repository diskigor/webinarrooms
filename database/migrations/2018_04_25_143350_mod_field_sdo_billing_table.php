<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModFieldSdoBillingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sdo_billing_orders', function (Blueprint $table) {
            $table->float('transfer_sum',8,2)->default(0)->change();
            $table->string('desc')->nullable()->change();
        });
        
        Schema::table('sdo_orders', function (Blueprint $table) {
            $table->float('sum',8,2)->default(0)->change();
            $table->float('lecture_sum',8,2)->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
