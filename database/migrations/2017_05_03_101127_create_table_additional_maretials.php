<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAdditionalMaretials extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('additional_materials',function (Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('lang_local');
            $table->text('content');
            $table->integer('sort');
            $table->enum('type',['audio','pdf','text','video_link']);
            $table->timestamps();
        });
        Schema::create('matterials_lectures',function (Blueprint $table){
            $table->increments('id');
            $table->integer('lecture_id');
            $table->integer('additional_material_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     Schema::dropIfExists('additional_materials');
     Schema::dropIfExists('matterials_lectures');
    }
}
