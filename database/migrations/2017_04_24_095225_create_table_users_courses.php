<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsersCourses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sdo_curses',function (Blueprint $table){
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->boolean('status')->default(0);
            $table->string('image');
            $table->integer('discount')->default(0);
            $table->integer('price')->default(0);
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('users_curses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('curses_id')->unsigned()->index();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('curses_id')
                ->references('id')
                ->on('sdo_curses')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('sdo_curses_trans',function (Blueprint $table){
            $table->increments('id');
            $table->string('lang_local');
            $table->string('slug');
            $table->integer('sdo_curses_id')->unsigned()->index();
            $table->string('name_trans');
            $table->string('short_desc_trans');
            $table->text('description_trans');
            $table->foreign('sdo_curses_id')
                ->references('id')
                ->on('sdo_curses')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sdo_curses');
        Schema::dropIfExists('users_curses');
        Schema::dropIfExists('sdo_curses_trans');
    }
}
