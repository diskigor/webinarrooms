<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_history', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('user_id')->nullable()->unsigned()->index('user');
            $table->integer('parent_id')->nullable()->unsigned()->index('parent');
            
            $table->integer('referal_level')->nullable()->index('referal_level');
            
            $table->integer('rules_id')->nullable()->unsigned()->index('rules');
            $table->string('rules_desc')->nullable()->default('[]');
            
            $table->float('operation_value', 8, 2)->default(0);
            $table->float('value', 8, 2)->default(0);
            
            $table->integer('billing_orders_id')->nullable()->unsigned()->index('billing_orders_id');

            $table->timestamps();
            
            
            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('set null');
            
            $table->foreign('parent_id')
                    ->references('id')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('set null');

            $table->foreign('rules_id')
                    ->references('id')
                    ->on('ref_rules')
                    ->onUpdate('cascade')
                    ->onDelete('set null');
            
            $table->foreign('billing_orders_id')
                    ->references('id')
                    ->on('sdo_billing_orders')
                    ->onUpdate('cascade')
                    ->onDelete('set null');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('ref_history');
    }
}
