<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFialdToQestion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Расширяем таблицу вопросов сортировкой
        Schema::table('sdo_test_questions', function (Blueprint $table) {
            $table->integer('ordered')->default(0)->nullable()->index('ordered');
            $table->enum('button_type', ['radio','checkbox'])->default('radio')->index('button_type');
        });
        
        //Расширяем таблицу ответов сортировкой
        Schema::table('sdo_test_answers', function (Blueprint $table) {
            $table->integer('ordered')->default(0)->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Расширяем таблицу вопросов сортировкой
        Schema::table('sdo_test_questions', function (Blueprint $table) {
            $table->dropColumn('ordered');
            $table->dropColumn('button_type');
        });
        
        //Расширяем таблицу ответов сортировкой
        Schema::table('sdo_test_answers', function (Blueprint $table) {
            $table->dropColumn('ordered');
        });
    }
}
