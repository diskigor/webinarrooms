<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToSdoCurses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Расширяем таблицу курсов
        Schema::table('sdo_curses', function (Blueprint $table) {
            $table->integer('front_status')->nullable()->default(0);
            $table->integer('front_hight')->nullable()->default(0);
        });
        
        //Расширяем таблицу курсов, описание
        Schema::table('sdo_curses_trans', function (Blueprint $table) {
            $table->text('front_descr')->nullable();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Урезаем таблицу курсов
        Schema::table('sdo_curses', function (Blueprint $table) {
            $table->dropColumn('front_status');
            $table->dropColumn('front_hight');
        });
        
        //Урезаем таблицу курсов, описание
        Schema::table('sdo_curses_trans', function (Blueprint $table) {
            $table->dropColumn('front_descr');
        });
        
    }
}
