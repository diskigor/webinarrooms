<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSdoProject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sdo_projects',function (Blueprint $table){
            $table->increments('id');
            $table->boolean('main_div');
            $table->boolean('side_bar');
            $table->string('image_link');
            $table->string('name',255);
            $table->text('short_desc');
            $table->string('href');
            $table->string('lang_local');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sdo_projects');
    }
}
