<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserTextAnswerComment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     Schema::create('text_answer_comment',function (Blueprint $table){
        $table->increments('id');
        $table->integer('text_answer_id')->unsigned()->index();
        $table->integer('user_id')->unsigned()->index();
        $table->text('comment');
         $table->foreign('user_id')
             ->references('id')
             ->on('users')
             ->onDelete('cascade')
             ->onUpdate('cascade');
         $table->foreign('text_answer_id')
             ->references('id')
             ->on('sdo_text_user_answer')
             ->onDelete('cascade')
             ->onUpdate('cascade');
        $table->timestamps();
     });
     Schema::create('text_answer_comment_like',function (Blueprint $table){
         $table->increments('id');
         $table->integer('text_answer_id')->unsigned()->index();
         $table->integer('text_answer_comment_id')->unsigned()->index();
         $table->integer('cursant_id')->unsigned()->index();
         $table->boolean('like');
         $table->foreign('cursant_id')
             ->references('id')
             ->on('users')
             ->onDelete('cascade')
             ->onUpdate('cascade');
         $table->foreign('text_answer_comment_id')
             ->references('id')
             ->on('text_answer_comment')
             ->onDelete('cascade')
             ->onUpdate('cascade');
         $table->foreign('text_answer_id')
             ->references('id')
             ->on('sdo_text_user_answer')
             ->onDelete('cascade')
             ->onUpdate('cascade');
         $table->timestamps();
     });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('text_answer_comment');
        Schema::dropIfExists('text_answer_comment_like');
    }
}
