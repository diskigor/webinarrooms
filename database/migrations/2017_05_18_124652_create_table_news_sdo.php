<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNewsSdo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news',function (Blueprint $table){
            $table->increments('id');
            $table->string('image_link');
            $table->timestamps();
        });
        Schema::create('news_trans',function (Blueprint $table){
            $table->increments('id');
            $table->integer('news_id')->unsigned()->index();
            $table->string('title')->unique();
            $table->text('article');
            $table->string('lang_local');
            $table->string('slug')->unique();
            $table->foreign('news_id')
                ->references('id')
                ->on('news')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
        Schema::dropIfExists('news_trans');
    }
}
