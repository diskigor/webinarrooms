<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSdoContacts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sdo_contacts',function (Blueprint $table){
            $table->increments('id');
            $table->text('number_one')->nullable();
            $table->text('number_two')->nullable();
            $table->string('email_one')->unique()->nullable();
            $table->string('email_two')->unique()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sdo_contacts');
    }
}
