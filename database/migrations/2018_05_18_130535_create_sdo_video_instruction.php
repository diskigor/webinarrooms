<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSdoVideoInstruction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sdo_video_instruction', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('order')->nullable()->unsigned()->default(0)->index('order');
            
            $table->boolean('status')->nullable()->unsigned()->default(0)->index('status');
            
            $table->integer('type')->nullable()->unsigned()->default(0)->index('type');
            
            $table->integer('parent_id')->nullable()->unsigned()->index('parent');
            
            $table->integer('user_id')->nullable()->unsigned()->index('user');
            
            $table->string('href')->nullable();
            
            $table->string('name')->nullable();
            
            $table->string('short')->nullable();
            
            $table->text('desc')->nullable();
            
            $table->nullableMorphs('instructiontable','in_morphs');
            
            $table->timestamps();
            
            $table->index(['order','status'],'o_s');
            $table->index(['order','status', 'user_id'],'o_s_u');
            $table->index(['order','type', 'user_id'],'o_t_u');
            $table->index(['order','status', 'type', 'user_id'],'o_s_t_u');
            
            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('set null');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sdo_video_instruction');
    }
}
