<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldBlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Расширяем таблицу blog
        Schema::table('blog', function (Blueprint $table) {
            $table->string('mainauthor')->nullable()->default('Гильбо Е.В.')->index('mainauthor');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Схлопываем таблицу blog
        Schema::table('blog', function (Blueprint $table) {
            $table->dropColumn('mainauthor');
        });
    }
}
