<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AadFieldForMessageAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        
        //Расширяем таблицу переписки пользователей
        Schema::table('message_admin', function (Blueprint $table) {
            $table->boolean('sadmin')->default(0)->index();
            $table->char('status_new',10)->default('send')->index();

        });
        
        //Расширяем таблицу переписки пользователей
        Schema::table('sdo_chats', function (Blueprint $table) {
            $table->boolean('sadmin')->default(0)->index();
            $table->char('status_new',10)->default('send')->index();

        });
        
        

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        //Схлопываем таблицу переписки пользователей
        Schema::table('sdo_chats', function (Blueprint $table) {
            $table->dropColumn('sadmin');
            $table->dropColumn('status_new');
        });
        
        //Схлопываем таблицу переписки пользователей
        Schema::table('message_admin', function (Blueprint $table) {
            $table->dropColumn('sadmin');
            $table->dropColumn('status_new');
        });
    }
}
