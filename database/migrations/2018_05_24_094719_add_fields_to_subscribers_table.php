<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToSubscribersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Расширяем таблицу подписчиков
        Schema::table('subscribers', function (Blueprint $table) {
            $table->integer('type')->nullable()->unsigned()->index('type');
            $table->string('group')->nullable()->index();
            $table->string('other')->nullable()->index();
            $table->integer('user_id')->nullable()->unsigned()->index('user');
            $table->integer('author_id')->nullable()->unsigned()->index('author');
            $table->nullableMorphs('subscriberstable','in_morphs');
            
            $table->index('email','email_and');
            $table->index('status','status_and');
            $table->index(['type','status'],'t_s');
            $table->index(['type','status', 'user_id'],'t_s_u');
            $table->index(['type','status', 'author_id'],'t_s_a');
            $table->index(['type','status', 'user_id', 'author_id'],'t_s_u_a');
            
            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('set null');
            
            $table->foreign('author_id')
                    ->references('id')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('set null');
            
            $table->dropUnique(['email']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Возрат таблицы подписчиков
        Schema::disableForeignKeyConstraints();
        Schema::table('subscribers', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropColumn('user_id');
            $table->dropForeign(['author_id']);
            $table->dropColumn('author_id');
            $table->dropColumn('type');
            $table->dropColumn('group');
            $table->dropColumn('other');
            $table->dropColumn('subscriberstable_type');
            $table->dropColumn('subscriberstable_id');
            $table->dropIndex('email_and');
            $table->dropIndex('status_and');
            $table->dropIndex('t_s');
            $table->dropIndex('t_s_u');
            $table->dropIndex('t_s_a');
            $table->dropIndex('t_s_u_a');
            
            $table->unique(['email']);
        });
    }
}
