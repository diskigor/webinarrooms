<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users',function (Blueprint $table){
            $table->increments('id');
            $table->string('name');
//----
            $table->string('nickname')->nullable();
            $table->string('surname')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('date_of_birth')->nullable();
            $table->enum('gender',['woman','man'])->nullable();
            $table->string('country')->nullable();
            $table->string('ind')->nullable();
            $table->string('city')->nullable();
            $table->string('address')->nullable();
            $table->string('education')->nullable();
            $table->string('scholastic_degree')->nullable();
            $table->string('social_status')->nullable();
            $table->string('profession')->nullable();
            $table->integer('m_income')->nullable();
            $table->string('causes');
            $table->integer('group_id')->nullable();
//----
            $table->integer('level_point')->default(0);
            $table->string('email')->unique();
            $table->string('password');
            $table->integer('role_id')->default(4);
            $table->string('avatar_link')->nullable();
            $table->integer('balance')->default(0);
            $table->string('hash',32)->nullable();
            $table->string('google_id')->nullable();
            $table->string('facebook_id')->nullable();
            $table->string('twitter_id')->nullable();
            $table->string('vkontakte_id')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
        Schema::create('roles',function (Blueprint $table){
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('description');
            $table->timestamps();
        });
        Schema::create('language_logo',function (Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('roles');
        Schema::dropIfExists('users_roles');
        Schema::dropIfExists('language_logo');
    }
}
